/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: TemplateRepository.java
 * Created on: 2016/12/09, 03:39:52 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository;

import com.gml.simpc.api.entity.OptionsTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repositorio de plantillas.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface TemplateRepository extends JpaRepository<OptionsTemplate, Long> {

}
