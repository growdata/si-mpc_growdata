/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Sesion.java
 * Created on: 2016/10/21, 01:38:02 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entidad para manejo de sesiones.
 *
 * @author <a href="mailto:julianm@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
@Table(name = "SESIONES")
@Entity
public class Session implements Serializable {

    /**
     * ID de la entidad.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator =
        "session_seq_gen")
    @SequenceGenerator(name = "session_seq_gen", sequenceName =
        "SESSION_SEQ", allocationSize = 1, initialValue= 1)
    private Long id;
    /**
     * Nombre de usuario en sesi&ocute;n.
     */
    @ManyToOne
    @JoinColumn(name = "USUARIO", nullable = false)
    private User user;

    /**
     * Fecha de la ultima operacion
     */
    @Column(name = "ULTIMA_OPERACION")
    private Date lastOperation;
    
    /**
     * Fecha del ultimo ping desde cliente
     */
    @Column(name = "ULTIMO_PING")
    private Date lastPing;
    
    /*--------------------------------------------------------*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getLastOperation() {
        return lastOperation;
    }

    public void setLastOperation(Date lastOperation) {
        this.lastOperation = lastOperation;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getLastPing() {
        return lastPing;
    }

    public void setLastPing(Date lastPing) {
        this.lastPing = lastPing;
    }
    /*--------------------------------------------------------*/

    @Override
    public String toString() {
        return "Session{" + "id=" + id + ", user=" + user + ", lastOperation=" +
            lastOperation + ", lastPing=" + lastPing + '}';
    }
    
}
