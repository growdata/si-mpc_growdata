/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: basicInformation.java
 * Created on: 2016/12/27, 04:23:26 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Leonardo Rocha</a>
 */
public class IntermediationDto {

    private String documentType;
    private String documentNumber;
    private String company;
    private String vacancyCode;
    private String applicationDate;
    private String applicationStatus;
    private String declineReason;
    private String declineDate;
    private String processStatus;
    private String referralDate;
    private String companyRejectionReason;
    private String salaryProposal;
    private String companyResponseDate;
    private String justification;
    private Long cargaId;


    /*
     * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public IntermediationDto() {

    }

    public IntermediationDto(String documentType, String documentNumber,
        String company, String vacancyCode, String applicationDate,
        String applicationStatus, String declineReason, String declineDate,
        String processStatus, String referralDate, String companyRejectionReason,
        String salaryProposal, String companyResponseDate, String justification,
        Long cargaId) {
        this.documentType = documentType;
        this.documentNumber = documentNumber;
        this.company = company;
        this.vacancyCode = vacancyCode;
        this.applicationDate = applicationDate;
        this.applicationStatus = applicationStatus;
        this.declineReason = declineReason;
        this.declineDate = declineDate;
        this.processStatus = processStatus;
        this.referralDate = referralDate;
        this.companyRejectionReason = companyRejectionReason;
        this.salaryProposal = salaryProposal;
        this.companyResponseDate = companyResponseDate;
        this.justification = justification;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getVacancyCode() {
        return vacancyCode;
    }

    public void setVacancyCode(String vacancyCode) {
        this.vacancyCode = vacancyCode;
    }

    public String getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(String applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    public String getDeclineReason() {
        return declineReason;
    }

    public void setDeclineReason(String declineReason) {
        this.declineReason = declineReason;
    }

    public String getDeclineDate() {
        return declineDate;
    }

    public void setDeclineDate(String declineDate) {
        this.declineDate = declineDate;
    }

    public String getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(String processStatus) {
        this.processStatus = processStatus;
    }

    public String getReferralDate() {
        return referralDate;
    }

    public void setReferralDate(String referralDate) {
        this.referralDate = referralDate;
    }

    public String getCompanyRejectionReason() {
        return companyRejectionReason;
    }

    public void setCompanyRejectionReason(String companyRejectionReason) {
        this.companyRejectionReason = companyRejectionReason;
    }

    public String getSalaryProposal() {
        return salaryProposal;
    }

    public void setSalaryProposal(String salaryProposal) {
        this.salaryProposal = salaryProposal;
    }

  



    public String getCompanyResponseDate() {
        return companyResponseDate;
    }

    public void setCompanyResponseDate(String companyResponseDate) {
        this.companyResponseDate = companyResponseDate;
    }

    public String getJustification() {
        return justification;
    }

    public void setJustification(String justification) {
        this.justification = justification;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("tipo Documento=").append(documentType).
            append("~&~Numero de documento=").append(documentNumber).
            append("~&~Empresa=").append(company).
            append("~&~Codigo de la vacante=").append(vacancyCode).
            append("~&~fecha de postulaci�n=").append(applicationDate).
            append("~&~Estado de postulaci�n=").append(applicationStatus).
            append("~&~Raz�n por la que Declin�=").append(declineReason).
            append("~&~Fecha de Declinaci�n=").append(declineDate).
            append("~&~Estado del proceso=").append(processStatus).
            append("~&~Fecha de Remisi�n=").append(referralDate).
            append("~&~Raz�n De rechazo de la empresa=").append(companyRejectionReason).
            append("~&~Propuesta Salarial=").append(salaryProposal).
            append("~&~Fecha de Respuesta de la empresa=").append(companyResponseDate).
            append("~&~justificaci�n=").append(justification).
            append("~&~cargaId=").append(cargaId);
        return stringBuilder.toString();
    }

    public Long getCargaId() {
        return cargaId;
    }

    public void setCargaId(Long cargaId) {
        this.cargaId = cargaId;
    }

}
