/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: ConsolidantPostulantDto.java
 * Created on: 2017/01/24, 08:18:39 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public class ConsolidantPostulantDto {
    
    /*
     *Mes  de los datos recogidos 
     */
     private String month;
     
     /**
      * Postulantes al subsidio  (sin respuesta) 
      */
     private int order;
     
     /**
      * Postulantes al subsidio  (sin respuesta) 
      */
     private int postulants;
     
     /*
      * Solicitudes aprobadas 
      */
     private int approvedApplications;
     
     /**
      * Solicitudes denegadas
      */
     private int requestsDenied;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getPostulants() {
        return postulants;
    }

    public void setPostulants(int postulants) {
        this.postulants = postulants;
    }

    public int getApprovedApplications() {
        return approvedApplications;
    }

    public void setApprovedApplications(int approvedApplications) {
        this.approvedApplications = approvedApplications;
    }

    public int getRequestsDenied() {
        return requestsDenied;
    }

    public void setRequestsDenied(int requestsDenied) {
        this.requestsDenied = requestsDenied;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getOrder() {
        return order;
    }

    @Override
    public String toString() {
        return "ConsolidantPostulantDto{" + "month=" + month + ", postulants=" +
            postulants + ", approvedApplications=" + approvedApplications +
            ", requestsDenied=" + requestsDenied + '}';
    }
     
     
}
