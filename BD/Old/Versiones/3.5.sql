DROP TABLE IND_VARIABLES;
DROP TABLE IND_FUENTES;
DROP TABLE IND_INDICADORES;

CREATE TABLE IND_FUENTES 
   (	
    ID NUMBER(19,0), 
    NOMBRE VARCHAR2(255), 
    TABLA VARCHAR2(255)
   );
ALTER TABLE IND_FUENTES ADD PRIMARY KEY (ID);

CREATE TABLE IND_VARIABLES 
   (	
    IND_FUENTE_ID NUMBER(19,0),
    ID NUMBER(19,0), 
    CAMPO VARCHAR2(255),
    DESCRIPCION VARCHAR2(255),
    TIPO VARCHAR2(255),
    DETALLE VARCHAR2(255)
   );
ALTER TABLE IND_VARIABLES ADD PRIMARY KEY (ID);
ALTER TABLE IND_VARIABLES ADD CONSTRAINT FK_IND_VARIABLES 
    FOREIGN KEY (IND_FUENTE_ID) REFERENCES IND_FUENTES (ID); 

CREATE TABLE IND_INDICADORES 
   (	
    ID NUMBER(19,0), 
    NOMBRE VARCHAR2(255),
    DESCRIPCION VARCHAR2(255),
    IND_FUENTE_ID NUMBER(19,0),
    TIPO_GRAFICO VARCHAR2(50), 
    IND_VARIABLE_X_ID NUMBER(19,0),
    IND_VARIABLE_SERIE_ID NUMBER(19,0),
    IND_VARIABLE_Y_ID NUMBER(19,0),
    FUNCION VARCHAR2(50) ,
 CONDICION clob
   );
ALTER TABLE IND_INDICADORES ADD PRIMARY KEY (ID);

Insert into IND_FUENTES (ID,NOMBRE,TABLA) values ('1','Prestaciones Economicas','MD_F_FOSFEC');
Insert into IND_FUENTES (ID,NOMBRE,TABLA) values ('2','Capacitaci�n','MD_F_CAP_PROGRAMA_MODULO');
Insert into IND_FUENTES (ID,NOMBRE,TABLA) values ('3','Detalle de capacitaci�n','MD_VIEW_PORTAL_CAP');
Insert into IND_FUENTES (ID,NOMBRE,TABLA) values ('4','Valores percapita en salud y pension','VW_PER_CAPITA_SALUD_PENSION');
Insert into IND_FUENTES (ID,NOMBRE,TABLA) values ('5','Valores percapita en alimentacion','VW_PERCAPITA_ALIMENTACION');
Insert into IND_FUENTES (ID,NOMBRE,TABLA) values ('6','Valores percapita ahorro de cesantias','VW_AHORRO_CESANTIAS_PERCAPITA');
Insert into IND_FUENTES (ID,NOMBRE,TABLA) values ('7','Valores percapita cuota monetaria','VW_CUOTA_MON_PERCAPITA');
Insert into IND_FUENTES (ID,NOMBRE,TABLA) values ('8','Valores percapita prestaciones economicas','VW_PRESTACIONES_PERCAPITA');
Insert into IND_FUENTES (ID,NOMBRE,TABLA) values ('9','Valores percapita por cuota monetaria por dependientes ','VW_CUOTA_DEPENDIETES_PERCAPITA');
--------------------------------------------------------

commit;


Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('261','Valor Per-capita de Salud  (Barras x CCF)','1','B','7','6','11','COUNT','Total Liquidado en Salud Sobre el total de beneficiarios
',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('72','Beneficiarios de Liquidados (salud y pension)(Lineas x Periodo)','1','L','25','7','13','COUNT','N�mero de Beneficiarios liquidadados  por periodo',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('82','Beneficiarios cuota monetaria  (Lineas Periodo)','1','L','25','7','10','COUNT','N�mero de Beneficiarios liquidados en Cuota Monetaria por periodo',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('271','Valor Per-capita de  Pensi�n  (Barras x CCF)','1','B','7','6','11','COUNT','Total Liquidado en Pensi�n Sobre el total de beneficiarios
',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('331','Capacitaciones realizadas en la agce','2','B','224','221','225','COUNT','Numero de capacitaciones realizadas por la ccf. (Barras x CCF)',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('341','Capacitaciones realizadas por tipo de Instituci�n','2','B','226','221','225','COUNT','Numero de capacitaciones realizas por tipo de instituci�n',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('351','Capacitaciones realizadas por tipo de formaci�n','2','B','223','221','225','COUNT','Numero de capacitaciones realizadas por tipo de formaci�n',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('361','Capacitaciones realizadas por  cine a.c.','2','B','227','221','225','COUNT','Numero de Capacitaciones realizadas por  cine a.c.',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('381','Por tipo de certificaci�n de programa','2','B','221','228','225','COUNT','Numero de capacitaciones realizadas por tipo de certificaci�n de programa',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('401','Personas ocupadas remitidas a este curso','2','B','221','228','225','COUNT','Personas ocupadas remitidas a este curso',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('411','Personas que se remitieron al curso','3','B','331','333','338','COUNT','N�mero total de personas que se remitieron al curso',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('421','Personas que desertaron','3','B','331','339','338','COUNT','n�mero total de personas que desertaron',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('431','Asistencia a la capacitaci�n','3','B','331','340','338','AVG','% de asistencia de las personas ',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('451','Costo total del subsidio de transporte por tipo de programa ','3','B','331','332','342','SUM','Costo total del subsidio de transporte por tipo de programa ',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('461','Otros costos asociados al curso por tipo de programa','3','B','331','332','343','SUM','Otros costos asociados al curso por tipo de programa',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('21','Postulados No Aceptados
  (Barra x CCF)
','1','B','7','8','4','COUNT','Corresponde a las solicitudes Denegadas por CCF','verificacion_requisitos=2');
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('31','Beneficiarios (Postulados aceptados al Mecanismo)
  (Barras x CCF)
','1','B','7','8','4','COUNT','Se obtiene de las solicitudes Aprobadas por CCF','verificacion_requisitos=1');
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('41','Proporci�n de beneficiarios sobre el total de postulados (Circulo)','1','D','1','7','4','COUNT','Porcentaje de solicitudes aprobadas sobre radicadas',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('51','Proporci�n de beneficiarios salientes del total de beneficiarios (Circulo)','1','D','8','7','26','COUNT','Porcentaje de beneficios finalizados por periodo de liquidaci�n de beneficios.','verificacion_requisitos=1');
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('71','Beneficiarios de Liquidados (salud y pension)( Barras x CCF)','1','B','7','25','13','COUNT','N�mero de Beneficiarios liquidadados  por periodo',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('91','Beneficiarios de incentivo de ahorro (Barras x CCF)','1','B','7','25','11','COUNT','N�mero de Beneficiarios liquidados en cesant�as por periodo',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('101','N�mero de Beneficiarios de Bono de Alimentaci�n (Barras x CCF)','1','B','7','25','12','COUNT','N�mero de Beneficiarios liquidados en bono de alimentaci�n por periodo',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('111','Beneficiarios del incentivo de ahorro del total de beneficiarios del Mecanismo de Protecci�n al Cesante.  (Circulo)','1','D','25','7','27',null,'Porcentaje de beneficios liquidados por ahorro de cesant�as sobre el total de beneficios liquidados por CCF','verificacion_requisitos=1');
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('201','Total liquidado en Salud (Barras x CCF)','1','B','7','25','13','SUM','Valor total liquidado en Salud, para cada CCF o consolidado por rango de periodos.',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('211','Total Liquidado en Pensi�n  (Barras x CCF)','1','B','7','25','14','SUM','Valor total liquidado en Pensi�n, para cada CCF o consolidado por rango de periodos.',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('221','Total Liquidado en Cuota Monetaria  (Barras x CCF)','1','B','7','25','10','SUM','Valor total liquidado en Cuota Monetaria, para cada CCF o consolidado por rango de periodos',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('231','Total Liquidado en Bono de Alimentaci�n  (Barras x CCF)','1','B','7','25','12','SUM','Valor total liquidado en Bono de Alimentaci�n, para cada CCF o consolidado por rango de periodos',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('241','Total Liquidado Por Ahorro de Cesant�as  (Barras x CCF)','1','B','7','25','11','SUM','Valor total liquidado por Ahorro de Cesant�as, para cada CCF o consolidado por rango de periodos',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('251','Total Prestaciones Econ�micas  (Barras x CCF)','1','B','7','25','27','SUM','Valor total de liquidaci�n  prestaciones econ�micas',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('281','Valor Per-capita de  Cuota    (Barras x CCF)','1','B','7','6','11','COUNT','Total Liquidado en Cuota Monetaria Sobre el total de beneficiarios de cuota monetaria
',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('291','Valor Per-capita de  Bono de Alimentaci�n  (Barras x CCF)','1','B','7','6','11','COUNT','Total Liquidado en Ahorro de Cesant�as Sobre el total de beneficiarios de ahorro de cesant�as
',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('301','Valor Per-capita Liquidaci�n  Ahorro de Cesant�as  (Barras x CCF)','1','B','7','6','11','COUNT','Total Liquidado en Ahorro de Cesant�as Sobre el total de beneficiarios de ahorro de cesant�as
',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('321','Valor Per-capita de Cuota Monetaria por dependientes econ�micos   (Barras x CCF)','1','B','7','6','11','COUNT','Valor per-capita de cuota monetaria Sobre el total de dependientes econ�micos del beneficiario del subsidio
',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('11','Postulados al Mecanismo (Barra x CCF)','1','B','7','1','3','COUNT','Corresponde a las solicitudes radicadas en el sistema por CCF',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('22','Postulados No Aceptados
  (Lineas x Periodo)

','1','L','8','7','4','COUNT','Corresponde a las solicitudes Denegadas por periodo','verificacion_requisitos=2');
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('32','Beneficiarios (Postulados aceptados al Mecanismo)  (Linea x Periodo)','1','L','8','7','4','COUNT','Se obtiene de las solicitudes Aprobadas por periodo','verificacion_requisitos=1');
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('102','N�mero de Beneficiarios de Bono de Alimentaci�n  (Lineas x Periodo)','1','L','25','7','12','COUNT','N�mero de Beneficiarios liquidados en bono de alimentaci�n por periodo',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('131','Porcentaje de ahorro para cesant�as (Circulo)','1','D','25','7','15','COUNT','Porcentaje de ahorro para cesant�as (Circulo)',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('141','Beneficiarios de las Agencias de Gesti�n y Colocaci�n (todos los inscritos a agencia)','1','D','7','11','15','COUNT','N�mero de beneficiarios que se inscribieron  a la Agencia de Gesti�n y Colocaci�n.
',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('161','Proporci�n de beneficiarios de incentivo sobre el total de beneficiarios','1','D','7','11','15','COUNT','Proporci�n de los beneficiarios que se inscribieron a la Agencia de Gesti�n y Colocaci�n (B2). La variable B corresponde a los beneficios liquidados en Zenith por periodo) 
',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('181','Beneficiarios por tipo de curso de capacitaci�n','1','D','7','11','15','COUNT','Proporci�n de beneficiarios por tipo de curso del total de beneficiarios que asisten a un curso, donde i=[t�cnico laboral, validaci�n, educaci�n continua, alfabetizaci�n
',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('191','Beneficiarios de capacitaci�n que terminaron','1','D','7','11','15','COUNT','Proporci�n de beneficiarios que finalizaron el curso de capacitaci�n
',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('202','Total liquidado en Salud (Lineas x Periodo)','1','L','25','7','13','SUM','Valor total liquidado en Salud, para cada CCF o consolidado por rango de periodos.',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('212','Total Liquidado en Pensi�n (Lineas x Periodo)','1','L','25','7','14','SUM','Valor total liquidado en Pensi�n, para cada CCF o consolidado por rango de periodos.',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('222','Total Liquidado en Cuota Monetaria (Lineas x Periodo)','1','L','25','7','10','SUM','Valor total liquidado en Cuota Monetaria, para cada CCF o consolidado por rango de periodos',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('232','Total Liquidado en Bono de Alimentaci�n (Lineas x Periodo)','1','L','25','7','12','SUM','Valor total liquidado en Bono de Alimentaci�n, para cada CCF o consolidado por rango de periodos',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('242','Total Liquidado Por Ahorro de Cesant�as  (Lineas x Periodo)','1','L','25','7','11','SUM','Valor total liquidado por Ahorro de Cesant�as, para cada CCF o consolidado por rango de periodos',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('252','Total Prestaciones Econ�micas (Lineas x Periodo)','1','L','25','7','27','SUM','Valor total de liquidaci�n  prestaciones econ�micas',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('262','Valor Per-capita de Salud  (Lineas x Periodo)','1','B','7','6','11','COUNT','Total Liquidado en Salud Sobre el total de beneficiarios
',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('272','Valor Per-capita de  Pensi�n  (Lineas x Periodo)','1','L','6','7','10','COUNT','Total Liquidado en Pensi�n Sobre el total de beneficiarios
',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('282','Valor Per-capita de  Cuota Monetaria (Lineas x Periodo)','1','L','6','7','10','COUNT','Total Liquidado en Cuota Monetaria Sobre el total de beneficiarios de cuota monetaria
',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('292','Valor Per-capita de  Bono de Alimentaci�n','1','L','6','7','10','COUNT','Total Liquidado en Ahorro de Cesant�as Sobre el total de beneficiarios de ahorro de cesant�as',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('302','Valor Per-capita Liquidaci�n  Ahorro de Cesant�as','1','L','6','7','10','COUNT','Total Liquidado en Ahorro de Cesant�as Sobre el total de beneficiarios de ahorro de cesant�as

',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('312','Valor Per-capita Prestaciones Econ�micas','1','L','6','7','10','COUNT','Total Liquidado por prestaciones econ�micas Sobre el total de beneficiarios',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('322','Valor Per-capita de Cuota Monetaria por dependientes econ�micos ','1','L','6','7','10','COUNT','Valor per-capita de cuota monetaria Sobre el total de dependientes econ�micos del beneficiario del subsidio',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('371','Por necesidad de formaci�n la capacitaci�n','2','B','228','221','225','COUNT','Numero de capacitaciones realizadas por necesidad de formaci�n ',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('441','Costo total de la matricula por tipo de programa','3','B','331','332','341','SUM','Costo total de la matricula por tipo de programa',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('12','Postulados al Mecanismo (Lineas x Periodo)','1','L','1','7','3','COUNT','Corresponde a las solicitudes radicadas en el sistema por periodo',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('81','Beneficiarios cuota monetaria ( Barras x CCF)','1','B','7','25','10','COUNT','N�mero de Beneficiarios liquidados en Cuota Monetaria por periodo',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('121','Proporci�n Beneficiarios de cuota monetaria ( Circulo)','1','D','25','7','15','COUNT','Porcentaje de Beneficiarios que se les liquidada Cuota Monetaria sobre le total de beneficios liquidados por CCF por periodo y CCF
',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('311','Valor Per-capita Prestaciones Econ�micas  (Barras x CCF)','1','B','7','6','11','COUNT','Total Liquidado por prestaciones econ�micas Sobre el total de beneficiarios
',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('92','Beneficiarios de incentivo de ahorro (Lineas x Periodo)','1','L','25','7','11','COUNT','N�mero de Beneficiarios liquidados en cesant�as por periodo',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('151','Beneficiarios de capacitaci�n (si una persona esta en 3 se reporta una sola vez por persona)','1','D','7','11','15','COUNT','N�mero de beneficiarios que reciben los servicios de capacitaci�n.
',null);
Insert into IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION) values ('171','Beneficiarios de Capacitaci�n del total de beneficiarios','1','D','7','11','15','COUNT','Proporci�n de beneficiarios que reciben los servicios de capacitaci�n del total de beneficiarios. (La variable B corresponde a los beneficios liquidados en Zenith por periodo)
',null);
-----------------------------------------



-------------

truncate table IND_VARIABLES;


Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','21','campo',null,'TIPO_IDENTIFICACION','Tipo de identificacion');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','22','campo',null,'numero_identificacion','N�mero de identificaci�n');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('2','221','periodo','yyyy-MM','fecha_inicio','Fecha de inicio');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('2','222','periodo','yyyy-MM','fecha_fin','Fecha de fin');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('2','223','tabla','tipos_programa','tipo_curso','Tipo de formaci�n');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('2','224','tabla','vw_cajas_compensacion','codigo_ccf','Caja de compensaci�n atravez del cual se dicta la capacitacion');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('2','225','campo',null,'codigo_programa','C�digo de programa');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('2','226','tabla','vw_tipos_institucion','codigo_inst','Tipo de instituci�n');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('4','18','campo',null,'periodo','Periodo del cual se quiere ver el valor percapita');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('4','19','campo',null,'valor_percapita_salud','Valor percapita salud');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('4','20','campo',null,'valor_percapita_pension','Valor percapita pension');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('2','227','tabla','vw_cine','cine','C�digo cine');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('2','228','tabla','vw_necesidad','necesidad','Necesidad');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','331','periodo','yyyy-MM','fecha_fin','Fecha de fin');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','332','tabla','tipos_programa','tipo_cap_id','Tipo de formaci�n');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','333','tabla','vw_cajas_compensacion','ccf_code','Caja de compensaci�n');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','334','campo',null,'cod_programa','C�digo de programa');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','335','tabla','vw_tipos_institucion','tipo_inst_id','Tipo de instituci�n');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','336','tabla','vw_cine','cine','C�digo cine');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','337','tabla','vw_necesidad','necesidad','Necesidad');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','338','campo',null,'documento','Documento de identificaci�n');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','339','tabla','vw_estados_asistencia_cap','estado','Estado');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','340','campo',null,'porcentaje_asistencia','Porcentaje de asistencia');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','341','campo',null,'costo_matricula','Costo de la matr�cula');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','342','campo',null,'COSTO_TRANSPORTE','Costo del subsidio de transporte');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','343','campo',null,'OTROS_COSTOS','Otros costos');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','1','periodo','yyyy-MM','fecha_postulacion_mpc','Fecha que se radica la solucitud');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values (null,'3','campo',null,'TIPO_IDENTIFICACION || numero_identificacion ','N�mero de identificaci�n');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','4','campo',null,'verificacion_requisitos','Estado de la solicitud  ');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','5','tabla','vw_estados_beneficio','terminacion_beneficio','Terminaci�n del beneficio');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','6','campo',null,'periodo_beneficio_liquidado','Periodo de liquidaci�n');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','7','tabla','vw_cajas_compensacion','codigo_ccf','Caja de compensacion a la  que pertenece  el postulante');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','8','periodo','yyyy-MM','fecha_verificacion_postu','Fecha de verificaci�n de la  solicitud');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','9','periodo','yyyy-MM','fecha_terminacion_beneficio','Corresponde a la fecha en la cual se aplico la ultima o sexta liquidacion al beneficio');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','10','campo',null,'monto_liquidado_cuota','Corresponde al monto liquidado al cesante por cuota monetaria para el periodo  de beneficio reportado. ');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','11','campo',null,'Monto_Liquidado_Cesantias','Corresponde al monto liquidado al cesante por ahorro de cesantias por periodo liquidado. ');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','12','campo',null,'Monto_Liquidado_Alimentacion','Corresponde al monto liquidado al cesante por cuota monetaria para el periodo  de beneficio reportado. ');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','13','campo',null,'monto_liquidado_salud','Monto liquidado en salud');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','14','campo',null,'monto_liquidado_pension','Monto liquidado en pensi�n');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','15','campo',null,'valor_total_prestaciones','Valor total de  prestaciones');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','16','campo',null,'beneficiario_del_mecanismo','Beneficiario del mecanismo');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','23','campo',null,'inscripcion_servicio_empleo','inscripcion al servicio de empleo');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','24','campo',null,'tipo_vinculacion_ccf','tipo de  vinculacion ccf');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','25','periodo','yyyy-MM','fecha_liquidacion_beneficio','fecha liquidacion beneficio');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','26','campo',null,'TERMINACION_BENEFICIO','terminacion de beneficio');
Insert into IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','27','campo',null,'valor_total_prestaciones','Valor total de prestaciiones');
--------------------------------------------------------




--Creaci�n de vista para cajas de compensacion
create or replace view vw_cajas_compensacion as
    select codigo id,nombre_corto descripcion
    from fundaciones 
    where es_ccf = 'V'
;   
commit;
--Creaci�n de vista para tipos de institucion
create or replace view vw_tipos_institucion as
    select 
    t.id,t.DESCRIPCION
    from MD_F_CAP_PROGRAMA_MODULO md, INSTITUCIONES i,TIPOS_INSTITUCION t
    where md.codigo_inst = i.id and t.id = i.TIPO_INSTITUCION_ID
    group by t.id,t.DESCRIPCION
;

commit;




--Creaci�n de vista para codigos cine
create or replace view vw_cine as
    select id,valor descripcion
    from cine
;  
commit;
--Creacio de vista para necesidad
create or replace view vw_necesidad as
select 
necesidad id,
case when necesidad = '1' then 'Necesidad de la empresa.'
 when necesidad = '2' then 'Necesidad del buscador.'
 when necesidad = '3' then 'Necesidad del sector productivo.'
 when necesidad = '4' then 'Ninguna de las anteriores'
end descripcion
from MD_F_CAP_PROGRAMA_MODULO
group by necesidad;
commit;
--Creaci�n de vista para estado de aprobacion en fosfec
create or replace view vw_estados_aprobacion as
    select 
    verificacion_requisitos id,
    case when verificacion_requisitos =1 then  'APROBADO'
    when  verificacion_requisitos =2 then 'DENEGADO'
    when verificacion_requisitos=3 then 'DESISTIDO'
    when  verificacion_requisitos =4 then 'CANCELADO'
    when  verificacion_requisitos =5 then 'EN PROCESO DE VERIFICACI�N'
    end descripcion
    from MD_F_FOSFEC group by verificacion_requisitos;
  commit;  
--Creaci�n de vista para estado de beneficio en fosfec
create or replace view vw_estados_beneficio as
    select 
    terminacion_beneficio id,
    case when terminacion_beneficio =1 then  'FINALIZADO'
    when  terminacion_beneficio =2 then 'NO FINALIZADO'
    end descripcion
    from MD_F_FOSFEC group by terminacion_beneficio;    
commit;
--Creaci�n de vista para estado de asistencia a capacitacion
create or replace view vw_estados_asistencia_cap as
select estado id,
case when estado = '1' then 'Finalizado'
 when estado = '2' then 'En curso'
 when estado = '3' then 'Desertado'
 when estado = '4' then 'Inscrito'
 end descripcion
 from MD_F_PROGRAMA_DETALLE
 group by estado;

commit;
ALTER TABLE MD_F_CAP_PROGRAMA_MODULO ADD EN_DOMINIO NUMBER(1) DEFAULT 0;

INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('24',
    'Asunto que se env�a en los correos para indicar que fallo la confirmaci�n de los programas',
    'COMMIT_PROGRAMS','Error al confirmar los programas cargados','CONFIRMACION DE PROGRAMAS');
COMMIT;
