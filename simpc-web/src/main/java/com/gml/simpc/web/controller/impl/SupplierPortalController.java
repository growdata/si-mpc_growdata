/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: SupplierPortalController.java
 * Created on: 2017/01/10, 05:32:58 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gml.simpc.api.dto.GraphicDataDto;
import com.gml.simpc.api.enums.GraphicType;
import com.gml.simpc.api.services.SupplierBankService;
import com.gml.simpc.api.utilities.Util;
import static com.gml.simpc.api.utilities.Util.FAILED_ALERT;
import com.gml.simpc.web.controller.ControllerDefinition;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
@Controller
public class SupplierPortalController implements ControllerDefinition {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER
            = Logger.getLogger(SupplierPortalController.class);

    /**
     * Servicio para manipular modulos.
     */
    @Autowired
    private SupplierBankService supplierBankService;

    /**
     * Muestra la informacion incial del portal Banco de oferentes.
     */
    @RequestMapping("banco-oferentes-portal")
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
            HttpServletResponse response) {
        ModelAndView modelAndView
                = new ModelAndView("banco-oferentes-portal");
        modelAndView.addObject("tab", "tabInstituciones");
        init(modelAndView, true);
        return modelAndView;

    }

    /**
     * Muestra el resultado segun institucion de la consulta segun el filtro.
     * 
     * @param request
     * @param response
     * 
     * @return 
     */
    @RequestMapping(value = "bancoInstituciones",
            method = RequestMethod.GET)
    public ModelAndView displayResultInstitution(HttpServletRequest request,
            HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView("banco-oferentes-portal");

        try {

            String institutionType = request.getParameter("tipoInstitucion");
            String city = Util.nullValue(request.getParameter("municipioModal"));
            String state = request.getParameter("departamentoModal");

            List institutions;

            institutions = this.supplierBankService.
                    getInstitutionsByTypeAndCityAndState(institutionType, city,
                            state);

            modelAndView.addObject("institutionList", institutions);
            modelAndView.addObject("tab", "tabInstituciones");
            graphicInstitution(modelAndView, institutionType, city, state);

            modelAndView.addObject("instSelected", institutionType);
            if (!"%".equals(city)) {
                modelAndView.addObject("citySelected", city);
            }
            modelAndView.addObject("stateSelected", state);
            modelAndView.addObject("activateBtn", true);
        } catch (Exception uEx) {
            LOGGER.error("Error ", uEx);
            modelAndView.addObject("msg",
                    "ERROR en la consulta");
            modelAndView.addObject("msgType", FAILED_ALERT);
        } finally {
            LOGGER.info("finally save in portal de   banco de oferente");
        }
        init(modelAndView, true);
        return modelAndView;

    }

    /**
     * Muestra el grafico de intituciones segun los datos despleagados.
     * 
     * @param modelAndView
     * @param institutionType
     * @param city
     * @param state 
     */
    private void graphicInstitution(ModelAndView modelAndView,
        String institutionType,
        String city, String state) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            List<GraphicDataDto> resultGraphic = this.supplierBankService.
                graphicInstitutionsByTypeAndCityAndState(institutionType, city,
                    state);
            modelAndView.addObject("graphicData", mapper.writeValueAsString(
                resultGraphic));
            modelAndView.addObject("graphicType", GraphicType.B);
            modelAndView.addObject("tab", "tabInstituciones");
            modelAndView.addObject("titulografica",
                "CANTIDAD DE TIPOS DE INSTITUCIONES");
            modelAndView.addObject("titulo", "CANTIDAD DE INSTITUCIONES");

            String xKey = "axisX";
            List<String> yKeys = new ArrayList();
            List<String> labels = new ArrayList();
            List<String> tableg = new ArrayList();
            List<String> barColors = new ArrayList();

            yKeys.add("axisY");
            labels.add("Cantidad de Instituciones");
            barColors.add("blue");

            tableg.add("CAJAS DE COMPENSACION");
            tableg.add("CANTIDAD POR TIPO DE INSTITUCIÓN");

            modelAndView.addObject("graphicList", resultGraphic);
            modelAndView.addObject("xKey", xKey);
            modelAndView.addObject("yKeys", yKeys);
            modelAndView.addObject("labels", labels);
            modelAndView.addObject("titleList", tableg);
            modelAndView.addObject("barColors", barColors);

            

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msgType", FAILED_ALERT);

        } finally {
            LOGGER.info("Finaliza realiacion de  graficos");
        }
    }

    /**
     * Muestra el resultado segun sede de la consulta segun el filtro.
     * 
     * @param request
     * @param response
     * 
     * @return 
     */
    @RequestMapping(value = "bancoSedes",
            method = RequestMethod.GET)
    public ModelAndView displayResultHeadQuarter(HttpServletRequest request,
            HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView("banco-oferentes-portal");

        try {

            String institutionType = request.getParameter(
                    "tipoInstitucion");
            String city = Util.
                    nullValue(request.getParameter("municipioModal2"));
            String state = request.getParameter("departamentoModal2");

            List headQuarters;
            LOGGER.info(
                    "Opciones seleccionadas tipo" + institutionType + "ciudad"
                    + city + "Estado"
                    + state);
            headQuarters = this.supplierBankService.
                    getHeadquarterByTypeAndCityAndState(institutionType, city, state);

            modelAndView.addObject("headQuarterList", headQuarters);
            modelAndView.addObject("instSelected2", institutionType);
            modelAndView.addObject("tab", "tabSedes");
            graphicHeadQuarter(modelAndView, institutionType, city, state);
            modelAndView.addObject("instSelected", institutionType);
            if (!"%".equals(city)) {
                modelAndView.addObject("citySelected", city);
            }
            modelAndView.addObject("stateSelected", state);
            modelAndView.addObject("activateBtn", true);

        } catch (Exception uEx) {
            LOGGER.error("Error ", uEx);
            modelAndView.addObject("msg",
                    "ERROR en la consulta");
            modelAndView.addObject("msgType", FAILED_ALERT);
        } finally {
            LOGGER.info("finally save in portal de   banco de oferente");
        }
        init(modelAndView, true);
        return modelAndView;

    }

    /**
     * Muestra el grafico de sedes y municipios.
     * 
     * @param modelAndView
     * @param institutionType
     * @param city
     * @param state 
     */
    private void graphicHeadQuarter(ModelAndView modelAndView,
        String institutionType,
        String city, String state) {
        ObjectMapper mapper = new ObjectMapper();

        try {
            List<GraphicDataDto> resultGraphic = this.supplierBankService.
                graphicHeadQuartersByTypeAndCityAndState(institutionType, city,
                    state);

            String xKey = "axisX";
            List<String> yKeys = new ArrayList();
            List<String> labels = new ArrayList();
            List<String> tableg = new ArrayList();
            List<String> barColors = new ArrayList();

            yKeys.add("axisY");
            labels.add("Cantidad de sedes");
            barColors.add("blue");

            tableg.add("CAJAS DE COMPENSACION");
            tableg.add("CANTIDAD POR DEPARTAMENTO");

            modelAndView.addObject("graphicData", mapper.writeValueAsString(
                resultGraphic));
            modelAndView.addObject("graphicList", resultGraphic);
            modelAndView.addObject("xKey", xKey);
            modelAndView.addObject("yKeys", yKeys);
            modelAndView.addObject("labels", labels);
            modelAndView.addObject("titleList", tableg);
            modelAndView.addObject("barColors", barColors);
            modelAndView.addObject("titulografica",
                "CANTIDAD DE SEDES POR DEPARTAMENTO");
            modelAndView.addObject("graphicType", GraphicType.B);
            modelAndView.addObject("tab", "tabSedes");
            modelAndView.addObject("titulo", "SEDES POR DEPARTAMENTO ");

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msgType", FAILED_ALERT);

        } finally {
            LOGGER.info("Finaliza realiacion de  graficos");
        }
    }

    /**
     * Muestra el resultado segun sede de la consulta segun el filtro.
     * 
     * @param request
     * @param response
     * 
     * @return 
     */
    @RequestMapping(value = "bancoProgramas",
            method = RequestMethod.GET)
    public ModelAndView displayResultPrograms(HttpServletRequest request,
            HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView("banco-oferentes-portal");

        try {

            String formationType = Util.nullValue(request.getParameter(
                    "formacion"));
            String duration = Util.nullValue(request.getParameter("duracion"));
            String moduleName = Util.likeValue(request.getParameter("nombreModulo"));

            List programs;


            programs = this.supplierBankService.
                    getProgramByTypeAndDurationAndModule(formationType, duration,
                            moduleName);

            modelAndView.addObject("programsList", programs);
            modelAndView.addObject("tab", "tabProgramas");
            modelAndView.addObject("formationSelected", formationType);
            modelAndView.addObject("durationSelected", duration);
            modelAndView.addObject("nameSelected", request.getParameter("nombreModulo"));

        } catch (Exception uEx) {
            LOGGER.error("Error ", uEx);
            modelAndView.addObject("msg",
                    "ERROR en la consulta");
            modelAndView.addObject("msgType", FAILED_ALERT);
        } finally {
            LOGGER.info("finally save in portal de   banco de oferente");
        }
        init(modelAndView, true);
        return modelAndView;

    }

    /**
     * Inicia los objetos para el controler.
     * 
     * @param modelAndView
     * @param creationError 
     */
    private void init(ModelAndView modelAndView, boolean creationError) {
        try {
            modelAndView.addObject("institutionTypes", this.supplierBankService.
                    getInstitutionTypes());
            modelAndView.
                    addObject("stateList", this.supplierBankService.getStates());
            modelAndView.addObject("citiesList", this.supplierBankService.
                    getCities());
            modelAndView.addObject("formationType", this.supplierBankService.
                    getFormationType());

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msgType", FAILED_ALERT);

        } finally {
            LOGGER.info("Init Portal Banco de  oferente");
        }
    }
}
