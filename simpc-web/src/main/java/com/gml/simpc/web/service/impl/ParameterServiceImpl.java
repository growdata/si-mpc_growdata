/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ResourcesServiceImpl.java
 * Created on: 2016/10/26, 12:28:33 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.entity.Parameter;
import com.gml.simpc.api.services.ParameterService;
import com.gml.simpc.commons.annotations.AnnotationInterceptor;
import com.gml.simpc.web.repository.ParameterRepository;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Servicio para administrar parametros.
 *
 * @author <a href="mailto:jonathanp@gmlsoftware.com">Jonathan Ponton</a>
 */
@Service
public class ParameterServiceImpl implements ParameterService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(ParameterServiceImpl.class);
    /**
     * Repositorio para usuarios.
     */
    @Autowired
    private ParameterRepository parameterRepository;
    /**
     * Objeto de bloqueo.
     */
    private final Object LOCK = new Object();

    /**
     * Genera la lista de todos los p&acute;rametros.
     * 
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar Todos los " +
        "Parámetros", entityTableName = "PARAMETROS")
    @Override
    public List<Parameter> getAll() {
        LOGGER.info("Ejecutando metodo [ getAll ]");
        return this.parameterRepository.findAll();
    }

    /**
     * Genera lista de p&acute;rametros seg&ucute;un parte del nombre.
     * 
     * @param name
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar Por Nombre",
        entityTableName = "PARAMETROS")
    @Override
    public List<Parameter> findBySpNameLikeIgnoreCase(String name) {
        LOGGER.info("Ejecutando metodo [ findByName ]");
        return this.parameterRepository.findBySpNameLikeIgnoreCase(name);
    }

    /**
     * Genera lista de p&acute;rametros seg&ucute;un el nombre.
     * 
     * @param name
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar Por Nombre",
        entityTableName = "PARAMETROS")
    @Override
    public Parameter findByName(String name) {
        LOGGER.info("Ejecutando metodo [ findByName ]");
        return this.parameterRepository.findByName(name);
    }

    /**
     * Busca un parametro seg&ucute;n su id.
     * 
     * @param id
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar Por Id",
        entityTableName = "PARAMETROS")
    @Override
    public Parameter findById(Long id) {
        LOGGER.info("Ejecutando metodo [ findById ]");
        return this.parameterRepository.findOne(id);
    }

    /**
     * Guarda p&acute;rametros.
     * 
     * @param parameter 
     */
    @Transactional
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Guardar",
        entityTableName = "PARAMETROS")
    @Override
    public void save(Parameter parameter) {
        LOGGER.info("Ejecutando metodo [ save de parametros]");
        this.parameterRepository.saveAndFlush(parameter);

        LOGGER.info("Se creo la institucion: " +
            parameter.getId());
    }

    /**
     * Actualiza p&acute;rametros.
     * 
     * @param parameter 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Actualizar",
        entityTableName = "PARAMETROS")
    @Override
    public void update(Parameter parameter) {
        LOGGER.info("Ejecutando metodo [ update de instituciones]");
        this.parameterRepository.saveAndFlush(parameter);
    }
}
