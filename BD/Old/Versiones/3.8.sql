ALTER TABLE TIPOS_CAPACITACION add  NOMBRE varchar2(200);

update TIPOS_CAPACITACION set nombre = 'Competencias' where id = 1;
update TIPOS_CAPACITACION set nombre = 'Tics' where id = 2;
update TIPOS_CAPACITACION set nombre = 'Alf. o bach.' where id = 3;
update TIPOS_CAPACITACION set nombre = 'Entrenamiento' where id = 4;
update TIPOS_CAPACITACION set nombre = 'Tec. Lab.' where id = 5;
commit;