<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster.bundle.min.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster-sideTip-shadow.min.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
    </head>
    <body>
        <c:if test="${not empty msg}">
            <br/>
            <div id="alert-${msgType}" class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        <h1>Par&aacute;metros del Sistema</h1>
        <div class="panel panel-default">
            <div class="panel-heading"><h2>B&uacute;squeda</h2></div>
            <div class="panel-body">
                <legend>
                    <form:form method="post" action="/masterdata/buscarParametros.htm" modelAttribute="searchParameter">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="nombreParametro" class="control-label">Nombre del Par&aacute;metro</label>
                                <form:input type="text" class="form-control" id="nombreParametro" 
                                            placeholder="Nombre del Par�metro" maxlength="50"
                                            path="name"/>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-offset-4 col-sm-8" style="text-align:right">
                            <button type="submit" class="btn btn-success">Buscar</button>
                        </div>
                    </form:form>
                </legend>
                <div class="clearfix"></div>
            </div>
        </div>

        <br/>
        <div class="panel panel-default">
            <div class="panel-heading"><h2>Detalles Parametrizaci&oacute;n</h2></div>
            <div class="panel-body">
                <div class=" table-responsive">
                    <table id="parameterList" class="table table-striped table-bordered" 
                           cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nombre del Par�metro</th>
                                <th>Descripci&oacute;n</th>
                                <th>Valor</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Nombre del Par�metro</th>
                                <th>Descripci&oacute;n</th>
                                <th>Valor</th>
                                <th>Acciones</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:set var="search" value="\\" />
                            <c:set var="replace" value="/" />
                            <c:if test="${not empty parameterList}">
                                <c:forEach var="item" items="${parameterList}">
                                    <tr>
                                        <td>${item.spName}</td>
                                        <td>${item.description}</td>
                                        <td>${item.value}</td>
                                        <td>
                                            <div class="btn-group" role="group" >
                                                <button id="editBtn${item.name}" type="button" class="btn btn-warning btn-xs"
                                                        onclick="setValues(${item.id}, '${item.name}', '${item.spName}', '${item.description}', '${fn:replace(item.value, search, replace)}')">
                                                        <i class="fa fa-id-card-o" aria-hidden="true"></i> Editar
                                                </button>  
                                            </div>
                                         </td>
                                    </tr>
                                </c:forEach>
                            </c:if>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        
        <div class="modal fade" id="editModal" role="dialog" 
            aria-labelledby="editModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                       </button>
                       <h4 class="modal-title" id="creationModalLabel">Modificar Par&aacute;metro</h4>
                    </div>

                    <div class="modal-body">
                        <form:form modelAttribute="selectedParameter" enctype="multipart/form-data"
                            method="post" action="/masterdata/guardarParametro.htm"
                            id="creationForm">
                            <div class="col-sm-12">
                                <h1>Datos del Par&aacute;metro</h1>
                            </div>
                            <input type="hidden" name="id" id="id"/>
                            <input type="hidden" name="name" id="name"/>
                            <input type="hidden" name="spName" id="spName"/>
                            <input type="hidden" name="description" id="description"/>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="control-label">Nombre:</label><br/>
                                    <p class="form-control-static" id="pname">${item.spName}</p>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="description" class="control-label">Descripci&oacute;n:</label><br/>
                                    <p class="form-control-static" id="pdescription">${item.description}</p>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="valor" class="control-label">Valor:</label><br/>
                                    <input type="text" name="value" id="value" required="required"
                                           class="form-control" width="500px" placeholder="Valor del Par�metro"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-12" style="text-align:right">
                                <br/>
                                <button id="saveButton" type="submit" class="btn btn-success"
                                        data-btn-ok-label="Confirmar" data-btn-cancel-label="Cancelar"
                                            data-title="Est� seguro?" 
                                            data-toggle="confirmation" data-placement="top" 
                                            confirmation-callback>Guardar</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                                <div class="clearfix"></div>
                            </div>
                        </form:form>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>

        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/dataTables.buttons.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-confirmation.js"></script>
        <script src="resources/js/jquery.validate.min.js"></script>
        <script src="resources/js/tooltipster.bundle.js"></script>
        <script src="resources/js/custom-functionalities.js"></script>
        <script>
            $(document).ready(function () {
                $('#parameterList').DataTable({
                    "language": {
                        "url": "resources/js/Spanish.json"
                    },
                    "bFilter": false,
                    "bInfo": false
                });
                
                $('#saveButton').confirmation({
                    onConfirm: function (event, element) {
                        if (validateForm("creationForm")) {
                            $('#creationForm').submit();
                        }
                    }
                });
            });
            
            function loadParameterDetail(id){
                $('#detailArea').attr("src",'detalle-parametro.htm?id='+id);
            }
            
            /**
             * Modifica los datos del formulario para edici�n.
             * @param {type} id
             * @param {type} name
             * @param {type} esp_name
             * @param {type} description
             * @param {type} value
             * @returns {undefined}
             */
            function setValues(id, name, esp_name, description, value) {
                
                $('#editModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                
                $("#id").val(id);
                $("#name").val(name);
                $("#spName").val(esp_name);
                $("#description").val(description);
                $("#value").val(value);
                $("#pname").text(esp_name);
                $("#pdescription").text(description);
            }
        </script>                                           
    </body>
</html>    
      
