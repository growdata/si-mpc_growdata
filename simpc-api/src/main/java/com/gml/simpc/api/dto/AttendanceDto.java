/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: AttendanceDto.java
 * Created on: 2016/12/20, 05:49:22 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * Objective: map the required fields into a load of training or orientation
 * programs for detail data.
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

/**
 * Class to map attendance to training and orientation for load data. 
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica Jim&eacute;nez Rozo</a>
 */
public class AttendanceDto {
    
    private String moduleCode;
    private String documentType;
    private String document;
    private String status;
    private String attendancePercent;
    private String transportCost;
    private String justification;
    
    /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

    public AttendanceDto() {
    }

    public AttendanceDto(String moduleCode, String documentType, String document,
        String status, String attendancePercent, String transportCost,
        String justification) {
        this.moduleCode = moduleCode;
        this.documentType = documentType;
        this.document = document;
        this.status = status;
        this.attendancePercent = attendancePercent;
        this.transportCost = transportCost;
        this.justification = justification;
    }
        
    /*************************************************************************/

    public String getModuleCode() {
        return moduleCode;
    }

    public void setModuleCode(String moduleCode) {
        this.moduleCode = moduleCode;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAttendancePercent() {
        return attendancePercent;
    }

    public void setAttendancePercent(String attendancePercent) {
        this.attendancePercent = attendancePercent;
    }

    public String getTransportCost() {
        return transportCost;
    }

    public void setTransportCost(String transportCost) {
        this.transportCost = transportCost;
    }

    public String getJustification() {
        return justification;
    }

    public void setJustification(String justification) {
        this.justification = justification;
    }
    
    /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

    @Override
    public String toString() {
        return "AttendanceDto{" + "moduleCode=" + moduleCode + ", documentType=" +
            documentType + ", document=" + document + ", status=" + status +
            ", attendancePercent=" + attendancePercent + ", transportCost=" +
            transportCost + ", justification=" + justification + '}';
    }
}
