/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ActionLogServiceImpl.java
 * Created on: 2016/11/15, 10:50:35 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.entity.HistoryAccess;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.services.HistoryAccessService;
import com.gml.simpc.api.utilities.Util;
import com.gml.simpc.commons.annotations.AnnotationInterceptor;
import com.gml.simpc.web.repository.HistoryAccessRepository;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Service(value = "historyAccessService")
public class HistoryAccessServiceImpl implements HistoryAccessService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(
        HistoryAccessServiceImpl.class);
    /**
     * Repositorio del log de accesos.
     */
    @Autowired
    private HistoryAccessRepository historyAccessRepository;

    /**
     * Genera todo el listado de historial de accesos.
     * 
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", entityTableName = "HISTORICO_ACCESOS",
        methodName = "Consultar Todos Los Históricos de accesos")
    @Override
    public List<HistoryAccess> getAll() {
        LOGGER.info("Ejecutando metodo [ getAll ] de HistoryAccessServiceImpl");
        return this.historyAccessRepository.findAll();
    }

    /**
     * Guarda historiales de accesos.
     * 
     * @param historyAcces 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", entityTableName = "HISTORICO_ACCESOS",
        methodName = "Guardar")
    @Override
    public void save(HistoryAccess historyAcces) {
        LOGGER.info("Ejecutando metodo [ save ] de HistoryAccessServiceImpl");
        this.historyAccessRepository.save(historyAcces);
    }

    /**
     * Actualiza historiales de accesos.
     * 
     * @param historyAccess 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", entityTableName = "HISTORICO_ACCESOS",
        methodName = "Actualizar")
    @Override
    public void update(HistoryAccess historyAccess) {
        LOGGER.info("Ejecutando metodo [ update ] de HistoryAccessServiceImpl");
        this.historyAccessRepository.saveAndFlush(historyAccess);
    }

    /**
     * Elimina historiales de accesos.
     * 
     * @param historyAccess 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", entityTableName = "HISTORICO_ACCESOS",
        methodName = "Borrar")
    @Override
    public void remove(HistoryAccess historyAccess) {
        LOGGER.info("Ejecutando metodo [ remove ] de HistoryAccessServiceImpl");
        this.historyAccessRepository.delete(historyAccess);
    }

    /**
     * Genera listado de historial de accesos segun los filtros ingresados.
     * 
     * @param user
     * @param initDate
     * @param finishDate
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", entityTableName = "HISTORICO_ACCESOS",
        methodName = "Consultar Por Filtros")
    @Override
    public List<HistoryAccess> findHistoricAccessUser(User user, Date initDate,
        Date finishDate) {
        LOGGER.info("Ejecutando metodo [ findHistoricAccessUser ] in " +
            "HistoryAccessServiceImpl");
        String idCCf = "";
        if (user.getCcf() != null) {
            idCCf = user.getCcf().getCode();
        }
        return this.historyAccessRepository.getHistoricAccessUser(initDate,
            finishDate,
            Util.nullValue(user.getIdentificationNumber()), Util.nullValue(
            user.getUsername()), Util.nullValue(idCCf));

    }

}
