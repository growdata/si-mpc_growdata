/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IndicatorController.java
 * Created on: 2016/12/12, 11:18:44 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gml.simpc.api.dto.GraphicDataDto;
import com.gml.simpc.api.dto.Page;
import com.gml.simpc.api.dto.PieGraphicDto;
import com.gml.simpc.api.dto.TableDataDto;
import com.gml.simpc.api.entity.IndicatorConfiguration;
import com.gml.simpc.api.entity.IndicatorSource;
import com.gml.simpc.api.entity.IndicatorVariable;
import com.gml.simpc.api.entity.valuelist.Foundation;
import com.gml.simpc.api.services.IndicatorConfigurationService;
import com.gml.simpc.api.services.IndicatorSourceService;
import com.gml.simpc.api.services.IndicatorVariableService;
import com.gml.simpc.api.services.ParameterService;
import com.gml.simpc.api.utilities.Util;
import com.gml.simpc.web.constants.WebConstants;
import com.gml.simpc.web.controller.ControllerDefinition;
import com.gml.simpc.web.utilities.IntersectionFilesCreator;
import com.gml.simpc.web.utilities.ValueListService;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_001_UNKNOW;
import com.gml.simpc.api.entity.valuelist.Cine;
import com.gml.simpc.api.entity.valuelist.DeliveredCertification;
import com.gml.simpc.api.entity.valuelist.InstitutionType;
import com.gml.simpc.api.entity.valuelist.TrainingType;
import static com.gml.simpc.api.utilities.Util.FAILED_ALERT;

/**
 * Controller para manejar la parte de indicadores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Controller
public class IndicatorController implements ControllerDefinition {

    /**
     * Nombre de archivo de cruce de pila.
     */
    public static final String INDICADOR_FILE_NAME
            = "DATOS_PLANOS";

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER
            = Logger.getLogger(IndicatorController.class);

    /**
     * Servicio para manipular indicadores.
     */
    @Autowired
    private IndicatorConfigurationService indicatorConfigurationService;
    /**
     * Servicio de Fuente de datos, indica los campos de cual tabla se debe
     * mostrar planos.
     */
    @Autowired
    private IndicatorSourceService indicatorSourceService;
    /**
     * Servicio de variable para buscar lista en caso que sea necesario.
     */
    @Autowired
    private IndicatorVariableService indicatorVariableService;

    /**
     * Componente de creaci&oacute;n de archivos de pdf,xls,cvs.
     */
    @Autowired
    private IntersectionFilesCreator intersectionFilesCreator;
    /**
     * Repositorio de par&aacute;metros.
     */
    @Autowired
    private ParameterService parameterService;
    /**
     * Servicio para listas de valores.
     */
    @Autowired
    private ValueListService valueListService;
    private IndicatorConfiguration indicatorConfig;

    /**
     * M&eacute;todo para generar listas de indicadores.
     * 
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = "listas", method = RequestMethod.GET)
    public ModelAndView listas(HttpServletRequest request,
            HttpServletResponse response) {

        ModelAndView modelAndView = new ModelAndView("indicadores");

        try {
            //Obteniendo el indicador seleccionado 
            String indicatorSel = request.getParameter("indicatorSel");
            String year = request.getParameter("anio");
            String month = request.getParameter("mes");
            String year2 = request.getParameter("anio2");
            String month2 = request.getParameter("mes2");
            init(modelAndView);

            modelAndView.addObject("indicatorSelected", indicatorSel);
            this.indicatorConfig
                    = (IndicatorConfiguration) this.valueListService.
                            getIndicatorsMap().get(indicatorSel);

            List<Map<String, Object>> variableList = new ArrayList();
            IndicatorVariable xVariable = indicatorConfig.getxVar();
            IndicatorVariable sVariable = indicatorConfig.getSerieVar();

            if ("tabla".equals(xVariable.getType())) {

                variableList = this.indicatorVariableService.getVariableList(
                        xVariable);
            } else if ("tabla".equals(sVariable.getType())) {

                variableList = this.indicatorVariableService.getVariableList(
                        sVariable);
            }
            modelAndView.addObject("variableList", variableList);
            modelAndView.addObject("yearSelected", year);
            modelAndView.addObject("monthSelected", month);
            modelAndView.addObject("yearSelected2", year2);
            modelAndView.addObject("monthSelected2", month2);

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msgType", FAILED_ALERT);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {

            LOGGER.info("Finaliza onDisplay IndicatorController");
        }

        return modelAndView;
    }

    /**
     * M&eacute;todo para generar la p&aacute;gina inicial de indicadores.
     * 
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = "indicadores", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
            HttpServletResponse response) {
        LOGGER.info("Ejecutando metodo [ onDisplay IndicatorController ]");

        ModelAndView modelAndView = new ModelAndView("indicadores");

        init(modelAndView);

        return modelAndView;
    }

    /**
     * M&eacute;todo para obtener una p&aacute;gina.
     * 
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = "obtenerPagina", method = RequestMethod.GET)
    public @ResponseBody
    String getPage(HttpServletRequest request,
            HttpServletResponse response) {

        try {
            //Fetch the page number from client
            Integer start = 1;
            Integer draw = 1;
            Integer length = 10;

            if (request.getParameter("start") != null) {
                start
                        = (Integer.valueOf(request.getParameter("start")) + 10) / 10;
            }

            if (request.getParameter("draw") != null) {
                draw = Integer.valueOf(request.getParameter("draw"));
            }

            if (request.getParameter("length") != null) {
                length = Integer.valueOf(request.getParameter("length"));
            }

            LOGGER.info("pagina: " + start + ", longitud: " + length);

            //Create page list data
            Page<TableDataDto> tablePage = this.indicatorConfigurationService.
                    getDataTablePage(this.indicatorConfig, start,
                            length);
            tablePage.setDraw(draw);

            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(tablePage);

            return json;
        } catch (Exception ex) {
            LOGGER.error("Error", ex);
        }

        return null;
    }

    /**
     * M&eacute;todo para generar los resultados de un indicador.
     * 
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = "generarResultados", method = RequestMethod.GET)
    public ModelAndView getResults(HttpServletRequest request,
            HttpServletResponse response) {

        ModelAndView modelAndView = new ModelAndView("indicadores-resultados");

        try {

            //Obteniendo el indicador seleccionado 
            String indicatorSel = request.getParameter("indicatorSel");

            this.indicatorConfig
                    = (IndicatorConfiguration) this.valueListService.
                            getIndicatorsMap().get(indicatorSel);

            //Obteniendo Tabla Fuente del  Indicador seleccionado
            IndicatorSource indicatorSource
                    = indicatorConfig.getIndicatorSource();
            String graphicTypeSel = indicatorConfig.getGraphicType();

            //Creando variables
            IndicatorVariable xVariable = indicatorConfig.getxVar();
            IndicatorVariable seriesVariable = indicatorConfig.getSerieVar();
            IndicatorVariable yVariable = indicatorConfig.getyVar();

            modelAndView.addObject("variableList", indicatorSource.
                    getVariables());

            //Valores seleccionados por el usuario
            String xyear = request.getParameter("anio");
            String xmonth = request.getParameter("mes");
            String xyear2 = request.getParameter("anio2");
            String xmonth2 = request.getParameter("mes2");

            String rango = (xyear.concat("/").concat(xmonth)).concat(" - ").
                    concat(xyear2).concat("/").concat(xmonth2);

            String serieVariableLista
                    = Util.nullValue(request.getParameter("serieVariableLista"));
            String yVariableSel = null;

            LOGGER.info(serieVariableLista + "lA SELECCION DE LA SERIE ");
            LOGGER.info(graphicTypeSel + " EL  GRAFICO ********");

            //Seteando  los valores a  cada  variable filtros
            switch (graphicTypeSel) {
                case "L":
                case "D":
                    xVariable.setVariable(xyear, xmonth, xyear2, xmonth2,
                            null, null);
                    seriesVariable.setVariable(null, null, null, null, null,
                            serieVariableLista);
                    break;
                case "B":
                    seriesVariable.setVariable(xyear, xmonth, xyear2, xmonth2,
                            null, null);
                    xVariable.setVariable(null, null, null, null, null,
                            serieVariableLista);
                    break;
            }

            if (indicatorConfig.getyVar() != null) {
                if ("D".equals(graphicTypeSel)
                        && "tabla".equals(yVariable.getType())) {
                    yVariable.setVariable(null, null, null, null, yVariableSel,
                            serieVariableLista);
                } else {
                    yVariableSel = indicatorConfig.getyVar().getField();
                    yVariable.setVariable(null, null, null, null, yVariableSel,
                            null);
                }
            }

            //Obteniendo datos para gr&aacute;ficas
            if ("L".equals(graphicTypeSel) || "B".equals(graphicTypeSel)) {
                List<GraphicDataDto> graphicData
                        = (List<GraphicDataDto>) this.indicatorConfigurationService.
                                getDataGraphic(indicatorConfig);
                graphicLabelsXY(modelAndView, graphicData, indicatorConfig,
                        serieVariableLista, rango);
            } else {

                List<PieGraphicDto> graphicData
                        = (List<PieGraphicDto>) this.indicatorConfigurationService.
                                getDataGraphic(indicatorConfig);
                graphicLabelsPie(modelAndView, graphicData, indicatorConfig,
                        serieVariableLista, rango);
                if ("171".equals(indicatorSel) && "0".equals(graphicData.get(0).getValue().toString())
                        && "0".equals(graphicData.get(1).getValue().toString())) {
                    graphicData.clear();
                }
            }
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msgType", FAILED_ALERT);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("Finaliza getResults IndicatorController");
        }

        return modelAndView;
    }

    /**
     * Genera el grafico del indicador (Linea o barras).
     * 
     * @param modelAndView
     * @param data
     * @param indicatorConfig
     * @param ccfSerie
     * @param rango 
     */
    private void graphicLabelsXY(ModelAndView modelAndView,
            List<GraphicDataDto> data, IndicatorConfiguration indicatorConfig,
            String ccfSerie, String rango) {

        String name = indicatorConfig.getDescription();
        String unitMeasurement = indicatorConfig.getUnitMeasurement();
        String xVariable = indicatorConfig.getxVar().getDescription();
        String graphicTypeSel = indicatorConfig.getGraphicType();

        modelAndView.addObject("graphic", graphicTypeSel);
        ObjectMapper mapper = new ObjectMapper();
        try {
            String xKey = xVariable;
            List<String> yKeys = new ArrayList();
            List<String> labels = new ArrayList();
            List<String> tableg = new ArrayList();

            yKeys.add("axisY");
            labels.add("Valor");
            tableg.add(xVariable);
            tableg.add("VALOR");

            modelAndView.addObject("graphicData",
                    mapper.writeValueAsString(data));

            modelAndView.addObject("graphicList", data);
            modelAndView.addObject("xKey", xKey);
            modelAndView.addObject("yKeys", yKeys);
            modelAndView.addObject("labels", labels);
            modelAndView.addObject("titleList", tableg);
            modelAndView.addObject("rango", rango);
            modelAndView.addObject("titulografica", name);
            modelAndView.addObject("unidadMedida", unitMeasurement);
            modelAndView.addObject("totalpie", 1);

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msgType", FAILED_ALERT);

        } finally {
            LOGGER.info("Finaliza creacion de labels graficos");
        }
        if (!"%".equals(ccfSerie)) {
            if ("CF".equals(indicatorConfig.getValueListInd())) {
                Map ccfs = valueListService.getFoundations();
                Foundation ccf = (Foundation) ccfs.get(ccfSerie);
                modelAndView.addObject("titulo", indicatorConfig.getId() + " - "
                        + indicatorConfig.getName().concat(" - ").
                                concat(ccf.getShortName()));
            } else if ("CA".equals(indicatorConfig.getValueListInd())) {
                switch (ccfSerie) {
                    case "1":
                        modelAndView.addObject("titulo", indicatorConfig.
                                getId() + " - " + indicatorConfig.getName().
                                        concat(" - ").
                                        concat("Programas y certificaciones genéricas"));
                        break;
                    case "2":
                        modelAndView.addObject("titulo", indicatorConfig.
                                getId() + " - " + indicatorConfig.getName().
                                        concat(" - ").
                                        concat("Educación"));
                        break;
                    case "3":
                        modelAndView.addObject("titulo", indicatorConfig.
                                getId() + " - " + indicatorConfig.getName().
                                        concat(" - ").
                                        concat("Artes y Humanidades"));
                        break;
                    case "4":
                        modelAndView.addObject("titulo", indicatorConfig.
                                getId() + " - " + indicatorConfig.getName().
                                        concat(" - ").
                                        concat("Ciencias Sociales, periodismo e información"));
                        break;
                    case "5":
                        modelAndView.addObject("titulo", indicatorConfig.
                                getId() + " - " + indicatorConfig.getName().
                                        concat(" - ").
                                        concat("Administración de Empresas y Derecho"));
                        break;
                    case "6":
                        modelAndView.addObject("titulo", indicatorConfig.
                                getId() + " - " + indicatorConfig.getName().
                                        concat(" - ").
                                        concat("Ciencias Naturales, matemáticos y estadísticas"));
                        break;
                    case "7":
                        modelAndView.addObject("titulo", indicatorConfig.
                                getId() + " - " + indicatorConfig.getName().
                                        concat(" - ").
                                        concat("Universitario o Equivalente"));
                        break;
                    case "8":
                        modelAndView.addObject("titulo", indicatorConfig.
                                getId() + " - " + indicatorConfig.getName().
                                        concat(" - ").
                                        concat("Ingeniería, Industria y Construcción"));
                        break;
                    case "9":
                        modelAndView.addObject("titulo", indicatorConfig.
                                getId() + " - " + indicatorConfig.getName().
                                        concat(" - ").
                                        concat("Agricultura, Silvicultura, Pesca y Veterinaria"));
                        break;
                    case "10":
                        modelAndView.addObject("titulo", indicatorConfig.
                                getId() + " - " + indicatorConfig.getName().
                                        concat(" - ").
                                        concat("Salud y Bienestar"));
                        break;
                    case "11":
                        modelAndView.addObject("titulo", indicatorConfig.
                                getId() + " - " + indicatorConfig.getName().
                                        concat(" - ").
                                        concat("Servicios"));
                        break;
                }
            } else if ("NF".equals(indicatorConfig.getValueListInd())) {
                switch (ccfSerie) {
                    case "1":
                        modelAndView.addObject("titulo", indicatorConfig.
                                getId() + " - " + indicatorConfig.getName().
                                        concat(" - ").
                                        concat("Necesidad de la empresa"));
                        break;
                    case "2":
                        modelAndView.addObject("titulo", indicatorConfig.
                                getId() + " - " + indicatorConfig.getName().
                                        concat(" - ").
                                        concat("Necesidad del buscador"));
                        break;
                    case "3":
                        modelAndView.addObject("titulo", indicatorConfig.
                                getId() + " - " + indicatorConfig.getName().
                                        concat(" - ").
                                        concat("Necesidad del sector productivo"));
                        break;
                    case "4":
                        modelAndView.addObject("titulo", indicatorConfig.
                                getId() + " - " + indicatorConfig.getName().
                                        concat(" - ").
                                        concat("Ninguna de las anteriores"));
                        break;
                }
            } else if ("CE".equals(indicatorConfig.getValueListInd())) {
                Map certs = valueListService.getDeliveredCertificationMap();
                DeliveredCertification cert = (DeliveredCertification) certs.
                        get(ccfSerie);
                modelAndView.addObject("titulo", indicatorConfig.getId() + " - "
                        + indicatorConfig.getName().concat(" - ").
                                concat(cert.getCode()));
            } else if ("TF".equals(indicatorConfig.getValueListInd())) {
                Map forms = valueListService.getTrainingTypeMap();
                TrainingType form = (TrainingType) forms.get(ccfSerie);
                modelAndView.addObject("titulo", indicatorConfig.getId() + " - "
                        + indicatorConfig.getName().concat(" - ").
                                concat(form.getValue()));
            } else if ("TI".equals(indicatorConfig.getValueListInd())) {
                switch (ccfSerie) {
                    case "1":
                        modelAndView.addObject("titulo", indicatorConfig.
                                getId() + " - " + indicatorConfig.getName().
                                        concat(" - ").
                                        concat("Cajas de compensación Familiar"));
                        break;
                    case "2":
                        modelAndView.addObject("titulo", indicatorConfig.
                                getId() + " - " + indicatorConfig.getName().
                                        concat(" - ").
                                        concat("Formación para el trabajo"));
                        break;
                    case "3":
                        modelAndView.addObject("titulo", indicatorConfig.
                                getId() + " - " + indicatorConfig.getName().
                                        concat(" - ").
                                        concat("Unidades vocacionales de aprendizaje de empresa"));
                        break;
                    case "4":
                        modelAndView.addObject("titulo", indicatorConfig.
                                getId() + " - " + indicatorConfig.getName().
                                        concat(" - ").
                                        concat("SENA"));
                        break;
                }
            }
        } else {
            modelAndView.addObject("titulo", indicatorConfig.getId() + " - " + indicatorConfig.getName().concat(" - ").concat("Todas"));
        }

    }

    /**
     * Genera los parametros verticales del gr&aacute;fico.
     * 
     * @param modelAndView
     * @param data
     * @param indicatorConfig
     * @param ccfSerie
     * @param rango 
     */
    private void graphicLabelsPie(ModelAndView modelAndView,
            List<PieGraphicDto> data, IndicatorConfiguration indicatorConfig,
            String ccfSerie, String rango) {

        String unitMeasurement = indicatorConfig.getUnitMeasurement();
        String name = indicatorConfig.getId() + " - " + indicatorConfig.getName();
        String pieParts = indicatorConfig.getyVar().getDescription();
        String graphicTypeSel = indicatorConfig.getGraphicType();
        BigDecimal totalpie = BigDecimal.ZERO;

        for (PieGraphicDto pieGraphicDto : data) {
            totalpie = totalpie.add(pieGraphicDto.getValue());
        }

        modelAndView.addObject("unidadMedida", unitMeasurement);
        modelAndView.addObject("graphic", graphicTypeSel);
        modelAndView.addObject("totalpie", totalpie);
        modelAndView.addObject("rango", rango);
        ObjectMapper mapper = new ObjectMapper();
        try {

            List<String> tableg = new ArrayList();

            tableg.add(pieParts);
            tableg.add("VALOR");

            modelAndView.addObject("graphicData", mapper.
                    writeValueAsString(data));
            modelAndView.addObject("dataPie", data);
            modelAndView.addObject("titleList", tableg);
            modelAndView.addObject("titulografica", indicatorConfig.getDescription());

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msgType", FAILED_ALERT);

        } finally {
            LOGGER.info("Finaliza realiacion de  graficos");
        }
        if (!"%".equals(ccfSerie)) {
            if ("CF".equals(indicatorConfig.getValueListInd())) {
                Map ccfs = valueListService.getFoundations();
                Foundation ccf = (Foundation) ccfs.get(ccfSerie);
                modelAndView.addObject("titulo", name.concat(" - ").
                        concat(ccf.getShortName()));
            } else if ("CA".equals(indicatorConfig.getValueListInd())) {
                switch (ccfSerie) {
                    case "1":
                        modelAndView.addObject("titulo", name.concat(" - ").
                                concat("Programas y certificaciones genéricas"));
                        break;
                    case "2":
                        modelAndView.addObject("titulo", name.concat(" - ").
                                concat("Educación"));
                    case "3":
                        modelAndView.addObject("titulo", name.concat(" - ").
                                concat("Artes y Humanidades"));
                        break;
                    case "4":
                        modelAndView.addObject("titulo", name.concat(" - ").
                                concat("Ciencias Sociales, periodismo e información"));
                        break;
                    case "5":
                        modelAndView.addObject("titulo", name.concat(" - ").
                                concat("Administración de Empresas y Derecho"));
                        break;
                    case "6":
                        modelAndView.addObject("titulo", name.concat(" - ").
                                concat("Ciencias Naturales, matemáticos y estadísticas"));
                        break;
                    case "7":
                        modelAndView.addObject("titulo", name.concat(" - ").
                                concat("Universitario o Oquivalente"));
                        break;
                    case "8":
                        modelAndView.addObject("titulo", name.concat(" - ").
                                concat("Ingeniería, Industria y Construcción"));
                        break;
                    case "9":
                        modelAndView.addObject("titulo", name.concat(" - ").
                                concat("Agricultura, Silvicultura, Pesca y Veterinaria"));
                        break;
                    case "10":
                        modelAndView.addObject("titulo", name.concat(" - ").
                                concat("Salud y Bienestar"));
                        break;
                    case "11":
                        modelAndView.addObject("titulo", name.concat(" - ").
                                concat("Servicios"));
                        break;
                }
            } else if ("NF".equals(indicatorConfig.getValueListInd())) {
                switch (ccfSerie) {
                    case "1":
                        modelAndView.addObject("titulo", name.concat(" - ").
                                concat("Necesidad de la empresa"));
                        break;
                    case "2":
                        modelAndView.addObject("titulo", name.concat(" - ").
                                concat("Necesidad del buscador"));
                        break;
                    case "3":
                        modelAndView.addObject("titulo", name.concat(" - ").
                                concat("Necesidad del sector productivo"));
                        break;
                    case "4":
                        modelAndView.addObject("titulo", name.concat(" - ").
                                concat("Ninguna de las anteriores"));
                        break;
                }
            } else if ("CE".equals(indicatorConfig.getValueListInd())) {
                Map certs = valueListService.getDeliveredCertificationMap();
                DeliveredCertification cert = (DeliveredCertification) certs.
                        get(ccfSerie);
                modelAndView.addObject("titulo", name.concat(" - ").
                        concat(cert.getCode()));
            } else if ("TF".equals(indicatorConfig.getValueListInd())) {
                Map forms = valueListService.getTrainingTypeMap();
                TrainingType form = (TrainingType) forms.get(ccfSerie);
                modelAndView.addObject("titulo", name.concat(" - ").
                        concat(form.getValue()));
            } else if ("TI".equals(indicatorConfig.getValueListInd())) {
                switch (ccfSerie) {
                    case "1":
                        modelAndView.addObject("titulo", name.concat(" - ").
                                concat("Cajas de compensación Familiar"));
                        break;
                    case "2":
                        modelAndView.addObject("titulo", name.concat(" - ").
                                concat("Formación para el trabajo"));
                        break;
                    case "3":
                        modelAndView.addObject("titulo", name.concat(" - ").
                                concat("Unidades vocacionales de aprendizaje de empresa"));
                        break;
                    case "4":
                        modelAndView.addObject("titulo", name.concat(" - ").
                                concat("SENA"));
                        break;
                }
            }
        } else {
            modelAndView.addObject("titulo", name.concat(" - ").concat("Todas"));
        }

    }

    /**
     * Inicia los objetos para el controler.
     *
     * @param modelAndView
     */
    private void init(ModelAndView modelAndView) {
        try {
            List<IndicatorConfiguration> indicatorList
                    = this.indicatorConfigurationService.findAll();
            List<IndicatorSource> indicatorSourceList
                    = this.indicatorSourceService.findAll();
            List<Integer> years = new ArrayList<>();
            Calendar c1 = Calendar.getInstance();
            for (int i = 2014; i <= c1.get(Calendar.YEAR); i++) {
                years.add(i);
            }

            modelAndView.addObject("anios", years);
            modelAndView.addObject("indicatorList", indicatorList);
            modelAndView.addObject("indicatorSourceList", indicatorSourceList);

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msgType", FAILED_ALERT);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("Finaliza onDisplay IndicatorController");
        }
    }

    /**
     * Pagina las tablas de los resultados de un indicador.
     * 
     * @param request
     * @return
     * @throws IOException 
     */
    @RequestMapping(value = "springPaginationDataTables", method
            = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String springPaginationDataTables(HttpServletRequest request) throws
            IOException {

        //Fetch the page number from client
        Integer pageNumber = 1;
        if (request.getParameter("page") != null) {
            pageNumber = Integer.valueOf(request.getParameter("page"));
        }

        Integer pageDisplayLength = 100;

        LOGGER.info("pagina" + pageNumber + "longitud" + pageDisplayLength);
        //Create page list data
        Page<TableDataDto> tablePage = this.indicatorConfigurationService.
                getDataTablePage(this.indicatorConfig, pageNumber, pageDisplayLength);

        ObjectMapper mapper = new ObjectMapper();

        return mapper.writeValueAsString(tablePage);
    }

    /**
     * Descarga todos los archivos generados en un cruce.
     *
     * @param request
     * @param response
     */
    @RequestMapping(value = "descargar-indicadorxls", method = RequestMethod.GET)
    public void downloadxls(HttpServletRequest request,
            HttpServletResponse response) {

        try {
            ServletOutputStream sos = response.getOutputStream();
            response.setContentType("application/xlsx");
            response.setHeader("Content-Disposition",
                    "attachment;filename=DATOSPLANOSxls.xlsx");

            sos.write(createXlsFile());
            sos.flush();

        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(IndicatorController.class.
                    getName()).
                    log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Descarga todos los archivos generados en un cruce.
     *
     * @param request
     * @param response
     */
    @RequestMapping(value = "descargar-indicadorcvs", method = RequestMethod.GET)
    public void downloadCVS(HttpServletRequest request,
            HttpServletResponse response) {

        try {
            ServletOutputStream sos = response.getOutputStream();
            response.setContentType("application/csv");
            response.setHeader("Content-Disposition",
                    "attachment;filename=DATOSPLANOSsv.csv");

            sos.write(createCVSFile());
            sos.flush();

        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(IndicatorController.class.
                    getName()).
                    log(Level.SEVERE, null, ex);
        }

    }

    /**
     * M&eacute;todo para crear archivo de excel con los resultados de las
     * consultas.
     * 
     * @return 
     */
    public byte[] createXlsFile() {

        List<TableDataDto> listRows = this.indicatorConfigurationService.
                getDataForFiles(this.indicatorConfig);

        String nombreHoja1 = "Hoja1";//nombre de la hoja1

        XSSFWorkbook libroTrabajo = new XSSFWorkbook();
        XSSFSheet hoja1 = libroTrabajo.createSheet(nombreHoja1);

        //iterando numero de filas (r)
        for (int r = 0; r < listRows.size(); r++) {
            XSSFRow row = hoja1.createRow(r);
            TableDataDto row2 = listRows.get(r);
            //iterando numero de columnas (c)
            for (int c = 0; c < row2.getColumns().size(); c++) {
                XSSFCell cell = row.createCell(c);
                cell.setCellValue(row2.getColumns().get(c));
            }
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {

            //escribir este libro en un OutputStream.
            libroTrabajo.write(baos);

            return baos.toByteArray();

        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(IndicatorController.class.
                    getName()).
                    log(Level.SEVERE, null, ex);
        }

        return null;
    }

    /**
     * M&eacute;todo para crear archivo de excel con los resultados de las
     * consultas.
     * 
     * @return 
     */
    public byte[] createCVSFile() {

        List<TableDataDto> listRows = this.indicatorConfigurationService.
                getDataForFiles(this.indicatorConfig);

        StringBuilder lineas = new StringBuilder();

        for (int i = 0; i < listRows.size(); i++) {
            StringBuilder row1 = new StringBuilder();

            int size = listRows.get(i).getColumns().size();

            for (int j = 0; j < size; j++) {
                row1.append(listRows.get(i).getColumns().get(j));

                if (i != size - 1) {
                    row1.append(WebConstants.COMA_SEPARATOR);
                }
            }

            lineas.append(row1);
            lineas.append("\n");
        }

        return lineas.toString().getBytes();
    }

}
