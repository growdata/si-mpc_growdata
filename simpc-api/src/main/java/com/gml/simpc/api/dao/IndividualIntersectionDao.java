/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: PilaDao.java
 * Created on: 2016/12/27, 04:06:52 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dao;

import com.gml.simpc.api.dto.BasicInformationDto;
import com.gml.simpc.api.dto.DependentDto;
import com.gml.simpc.api.dto.EducationLevelDto;
import com.gml.simpc.api.dto.InformalEducationDto;
import com.gml.simpc.api.dto.IntermediationDto;
import com.gml.simpc.api.dto.LanguageDto;
import com.gml.simpc.api.dto.OtherKnowledgeDto;
import com.gml.simpc.api.dto.PilaDto;
import com.gml.simpc.api.dto.PilaEmploymentServiceDto;
import com.gml.simpc.api.dto.ProfitDto;
import com.gml.simpc.api.dto.ProgramDto;
import com.gml.simpc.api.dto.WorkExperienceDto;
import java.util.List;

/**
 * Interface que define el DAO para consultar informaci&oacute;n de
 * la tabla MD_F_PILA
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public interface IndividualIntersectionDao {

    /**
     * Obtiene registros pila por n&uacute;mero y tipo de documento
     *
     *
     * @return List<PilaDto>
     */
    List<PilaDto> findPilaByDocumentAndNumberType(String documentType,
        String numberIdentification);

    /**
     * Obtiene registros de dependientes por n&uacute;mero y tipo de documento
     *
     *
     * @return List<DependentDto>
     */
    List<DependentDto> findDependentsByDocumentAndNumberType(String documentType,
        String numberIdentification);

    /**
     * Obtiene registros de orientac&oacute;n y capacitaci&oacute;n por
     * n&uacute;mero y tipo de documento
     *
     *
     * @return List<ProgramDto>
     */
    List<ProgramDto> findTrainingsByDocumentAndNumberType(String documentType,
        String numberIdentification);

    /**
     * Obtiene registros de orientac&oacute;n y capacitaci&oacute;n
     *
     *
     * @return List<ProgramDto>
     */
    List<ProgramDto> findTrainings();

    /**
     * Obtiene registros pila por n&uacute;mero y tipo de documento para
     * usuarios del servicio publico de empleo
     *
     *
     * @return List<PilaDto>
     */
    List<PilaEmploymentServiceDto> findPilaByDocumentAndNumberTypeEmploymentService(
        String documentType, String numberIdentification);

    /**
     * Obtiene registros fosfec por n&uacute;mero y tipo de documento
     *
     *
     * @return List<ProfitDto>
     */
    List<ProfitDto> findFosfecByDocumentAndNumberType(String documentType,
        String numberIdentification);

    /**
     * Obtiene registros intermediacion por n&uacute;mero y tipo de documento
     *
     *
     * @return List<IntermediationDto>
     */
    List<IntermediationDto> findIntermediationByDocumentAndNumberType(
        String documentType, String numberIdentification);

    /**
     * Obtiene registros de basic Information por n&uacute;mero y tipo de
     * documento
     *
     *
     * @return List<BasicInformationDto>
     */
    List<BasicInformationDto> findBasicInformationByDocumentAndNumberType(
        String documentType, String numberIdentification);

    /**
     * Obtiene registros de educationLevel por n&uacute;mero y tipo de
     * documento
     *
     *
     * @return List<EducationLevelDto>
     */
    List<EducationLevelDto> findEducationLevelByDocumentAndNumberType(
        String documentType, String numberIdentification);

    /**
     * Obtiene registros de workExperience por n&uacute;mero y tipo de
     * documento
     *
     *
     * @return List<WorkExperienceDto>
     */
    List<WorkExperienceDto> findWorkExperienceByDocumentAndNumberType(
        String documentType, String numberIdentification);

    /**
     * Obtiene registros de InformalEducation por n&uacute;mero y tipo de
     * documento
     *
     *
     * @return List<InformalEducationDto>
     */
    List<InformalEducationDto> findInformalEducationByDocumentAndNumberType(
        String documentType, String numberIdentification);

    /**
     * Obtiene registros de Language por n&uacute;mero y tipo de
     * documento
     *
     *
     * @return List<LanguageDto>
     */
    List<LanguageDto> findLanguageByDocumentAndNumberType(
        String documentType, String numberIdentification);

    /**
     * Obtiene registros de Language por n&uacute;mero y tipo de
     * documento
     *
     *
     * @return List<OtherKnowledgeDto>
     */
    List<OtherKnowledgeDto> findOtherKnowledgeByDocumentAndNumberType(
        String documentType, String numberIdentification);

    /**
     * Actualiza la columna <b>EN_DOMINIO</b> en la de la tabla
     * <b>MD_F_CAP_PROGRAMA_MODULO</b>.
     *
     * @param from
     * @param to
     */
    void updateInDomainColumn(int from, int to);
}
