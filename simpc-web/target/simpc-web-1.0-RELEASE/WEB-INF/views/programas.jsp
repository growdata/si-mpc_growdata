<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>MECANISMO DE PROTECCI�N AL CESANTE "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster.bundle.min.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster-sideTip-shadow.min.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <style>
            input[type=number]::-webkit-outer-spin-button,
            input[type=number]::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            input[type=number] {
                -moz-appearance:textfield;
            }
        </style>
    </head>

    <body>
        <c:if test="${not empty msg}">
            <div id="alert-${msgType}" class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>


        <h1> Administraci&oacute;n de Programas</h1>
        <div class="panel panel-default">
            <div class="panel-heading"><h2>B&uacute;squeda</h2></div>
            <div class="panel-body">
                <legend>
                    <div id="alert-area" ></div>
                    <form:form method="post" action="/masterdata/buscarProgramas.htm" modelAttribute="searchProgram" id="valid_form"> 


                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="nombrePrograma" class="control-label">Nombre del programa</label>
                                <form:input type="text" class="form-control validado" id="nombrePrograma" 
                                            placeholder="Nombre del Programa" maxlength="50"
                                            path="name"/>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="codigoPrograma" class="control-label">C�digo del Programa</label>
                                <form:input type="text" class="form-control validado" id="codigoPrograma" 
                                            placeholder="C�digo del Programa" maxlength="50" value=""
                                            path="code"/>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="tipoCapacitacion" class=" control-label">Tipo de Formaci&oacute;n</label>
                                <form:select class="form-control validado" id="tipoCapacitacion" path="type.id" >
                                    <form:option value="0">Seleccione...</form:option>
                                    <c:if test="${not empty tiposPrograma}">
                                        <c:forEach var="item" items="${tiposPrograma}">
                                            <form:option label="${item.name}" value="${item.id}">
                                                ${item.name}
                                            </form:option>
                                        </c:forEach>
                                    </c:if>
                                </form:select>
                                <div class="clearfix"></div>
                            </div>
                        </div>




                        <div class="clearfix"></div>

                        <div class="col-sm-offset-4 col-sm-8" style="text-align:right">
                            <button id="nuevoBtn"  type="button" class="btn btn-success" onclick="prepararForm()">Nuevo</button>
                            <button id="buscarBtn" type="button" class="btn btn-success" onclick="validarCampos()">Buscar</button>


                        </div>
                    </form:form>


                </legend>
                <div class="clearfix"></div>
                <br>

            </div>
        </div>

        <br/>
        <div class="panel panel-default">
            <div class="panel-heading"><h2>Detalle Consulta Programas</h2></div>
            <div class="panel-body">
                <div class=" table-responsive">
                    <table id="programs" class="table table-striped table-bordered" 
                           cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>C&oacute;digo del Programa</th>
                                <th>Nombre del Programa</th>
                                <th>Tipo de Formaci&oacute;n</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>C&oacute;digo del Programa</th>
                                <th>Nombre del Programa</th>
                                <th>Tipo de Formaci&oacute;n</th>

                                <th>Acciones</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:if test="${not empty programList}">

                                <c:forEach var="item" items="${programList}">
                                    <tr>
                                        <td>${item.code}</td>
                                        <td>${item.name}</td>
                                        <td>${item.type.name}</td>

                                        <td>
                                <center>
                                    <div class="btn-group" role="group" >
                                        <table>
                                            <tr>
                                                <td>
                                                    <button type="button" class="btn btn-info btn-xs"
                                                            data-toggle="modal" data-target="#detailModal"
                                                            onclick="loadProgramDetail(${item.id})">
                                                        <i class="fa fa-id-card-o" aria-hidden="true"></i> Ver
                                                    </button>
                                                </td> 
                                                <td>
                                                    <button type="button" class="btn btn-warning btn-xs" onclick="editProgram(${item.id})">
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> 
                                                        Editar
                                                    </button>
                                                </td>
                                                <td>
                                                    <form:form method="post" action="/masterdata/borrarPrograma.htm"> 
                                                        <input type="hidden" name="id" value="${item.id}"/>
                                                            <button class="btn btn-danger btn-xs elimProg" type="submit"
                                                                    data-btn-ok-label="Confirmar" data-btn-cancel-label="Cancelar"
                                                                    data-title="Est� seguro?" 
                                                                    data-toggle="confirmation" data-placement="top" 
                                                                    confirmation-callback  >
                                                                <i class="fa fa-times-circle-o" aria-hidden="true"></i>Eliminar
                                                            </button>
                                                        </form:form>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </center>
                                </td>
                                </tr>
                            </c:forEach>

                        </c:if>
                        </tbody>
                    </table>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>

        <div class="modal fade" id="creationModal" role="dialog" 
             aria-labelledby="creationModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="creationModalLabel">Registro de Programas</h4>
                    </div>

                    <div class="modal-body">
                        <div id="alert-area2" ></div>
                        <c:if test="${not empty internalMsg}">
                            <div class="alert alert-${msgType} alert-dismissible" role="alert" id="messageCreationProgram">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-label="Close">
                                    <span aria-hidden="true">�</span>
                                </button>
                                <strong>${internalMsg}</strong>
                            </div>
                        </c:if>
                        <div class="col-sm-12">
                            <h1>Datos del Programa</h1>
                            <br/> 
                        </div>
                        <form:form id="creationProgram" modelAttribute="creationProgram"
                                   method="post" action="/masterdata/guardarPrograma.htm">

                            <form:input type="hidden" path="id" name="creationId" id="creationId" value="${creationProgram.id}"/>
                            
                            <input type="hidden" name="captureID" id="captureID" value=""/>
                            
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="typeid" class=" control-label">Tipo de Formaci&oacute;n(*)</label>
                                    <form:select class="form-control" onchange="activarTecnico()" id="typeid" path="type.id" required="required" name="tipoFormacion">
                                        <option value="">Seleccione...</option>
                                        <c:if test="${not empty tiposPrograma}">
                                            <c:forEach var="item" items="${tiposPrograma}">
                                                <option label="${item.name}" value="${item.id}">
                                                    ${item.name}
                                                </option>
                                            </c:forEach>
                                        </c:if>
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 divCod">
                                <div class="form-group">
                                    <label for="id" class="control-label">Codigo del Programa(*)</label>
                                    <div class="input-box">
                                        <form:input type="text" class="form-control"  id="id" 
                                                    required="required"
                                                    path="id" name="id"/>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                        
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="control-label">Nombre del Programa(*)</label>
                                    <div class="input-box">
                                        <form:input type="text" class="form-control"  id="name" 
                                                    placeholder="Nombre del Programa" required="required" autofocus="autofocus"
                                                    path="name" name="nombrePrograma"/>
                                        <span class="unit mascara">T�cnico Laboral en </span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                        
                            <form:input type="hidden" path="deliveredCertification.id" id="certificacionEmitidaV" value=""/>   
                                        
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="certificacionEmitida" class="control-label">Certificaci�n Emitida(*)</label>
                                        <input type="text" class="form-control" id="certificacionEmitida" 
                                                readonly="true" value=""/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>  
                                        
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="hours" class="control-label">Duraci&oacute;n Total en horas(*)</label>
                                    <form:input type="text" class="form-control" id="hours" placeholder="Duraci�n" required="required"
                                                path="hours" maxlength="6"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                    
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="cineid" class=" control-label">Niveles CINE(*)</label>
                                    <form:select class="form-control" id="cineid" path="cine.id" required="required">
                                        <form:option value="">Seleccione...</form:option>
                                        <c:if test="${not empty cines}">
                                            <c:forEach var="item" items="${cines}">
                                                <form:option label="${item.value}" value="${item.id}"/>
                                            </c:forEach>
                                        </c:if>
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                    
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="necessity" class="control-label">Necesidad del Curso(*)</label>
                                    <form:select class="form-control" id="necessity" path="necessity" required="required">
                                        <form:option value="">Seleccione...</form:option>
                                        <c:if test="${not empty necesidades}">
                                            <c:forEach var="item" items="${necesidades}">
                                                <form:option label="${item.label}" value="${item}"/>
                                            </c:forEach>
                                        </c:if>
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>  
                                    
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="qualityCertificate" class=" control-label">Certificado de Calidad (*)</label>
                                    <form:select class="form-control" onchange="activarEmision()" id="qualityCertificate" path="qualityCertificate" required="required">
                                        <option value="">Seleccione...</option>
                                        <form:option value="SI">SI</form:option>
                                        <form:option value="NO" selected="selected">NO</form:option>
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 divNomCerti">
                                <div class="form-group">
                                    <label for="certificationid" class="control-label">Nombre Certificado de Calidad(*)</label>
                                    <form:select class="form-control" onchange="certifVal()" id="certificationid"
                                                 path="certification.id" required="required">
                                        <form:option id="select" value="">Seleccione...</form:option>
                                        <c:if test="${not empty certificationsList}">
                                            <c:forEach var="item" items="${certificationsList}">
                                                <form:option label="${item.code}" value="${item.id}"/>
                                            </c:forEach>
                                        </c:if>
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 divCual">
                                <div class="form-group">
                                    <label for="whichName" class="control-label">�Cu�l?(*)</label>
                                    <div class="input-box">
                                        <form:input type="text" class="form-control" id="whichName" 
                                                    placeholder="Nombre del Certificado de Calidad" required="required"
                                                    path="whichName" name="whichName" maxlength="80"/>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            
                            <c:if test="${not empty disableCodeEdit}" >
                                <div class="col-sm-6">
                                    <div class="form-group" id="codigoProgramaDiv">
                                        <label for="nit" class="control-label">C&oacute;digo del Programa</label>
                                        <form:input type="text" class="form-control" id="nit" 
                                                    placeholder="C�digo del programa" required="required" onkeypress="return isNumberKey(event)"
                                                    readonly="${disableCodeEdit}"
                                                    path="code" maxlength="10" />
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </c:if>
                            
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="observacioness" class="form-control-label msnLabel">Observaciones:</label>
                                    <form:textarea class="form-control"  name="observaciones" id="observaciones" path="observations" maxlength="200"></form:textarea>
                                    </div>
                            </div>
                                    
                            <div class="clearfix"></div>
                            
                            <div class="col-sm-12" style="text-align:right">
                                <br/>
                                <button id="saveButtonProgram" class="btn btn-success"
                                        data-btn-ok-label="Confirmar" data-btn-cancel-label="Cancelar"
                                        data-title="Est� seguro?"
                                        data-toggle="confirmation" data-placement="top" 
                                        confirmation-callback >Guardar</button>            
                                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="resetForm()">Cancelar</button>
                            </div>
                            
                        </form:form>

                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="detailModal" role="dialog" 
             aria-labelledby="creationModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="creationModalLabel">Detalle del Programa</h4>
                    </div>

                    <div class="modal-body" style="height: 100vh">

                        <iframe id="detailArea" width="100%" height="100%" style="border: 0">

                        </iframe>
                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>    

        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/dataTables.buttons.min.js"></script>
        <script src="resources/js/buttons.flash.min.js"></script>
        <script src="resources/js/jszip.min.js"></script>
        <script src="resources/js/pdfmake.min.js"></script>
        <script src="resources/js/vfs_fonts.js"></script>
        <script src="resources/js/buttons.html5.min.js"></script>
        <script src="resources/js/buttons.print.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/jquery.ui.widget.js"></script>
        <script src="resources/js/jquery.iframe-transport.js"></script>
        <script src="resources/js/bootstrap-confirmation.js"></script>
        <script src="resources/js/jquery.fileupload.js"></script>
        <script src="resources/js/jquery.validate.min.js"></script>
        <script src="resources/js/tooltipster.bundle.js"></script>
        <script src="resources/js/custom-functionalities.js"></script>
        <script>

            $(document).ready(function () {
                
                $(".elimProg").each(function () {
                    $(this).confirmation();
                });
                
                
                $(".divCual").hide();
                
                var code = "<c:out value="${creationProgram.qualityCertificate}"/>";
                
                if($("#creationId").val() === "" || code === "NO"){
                    $(".divNomCerti").hide();
                    $(".divCual").hide();
                }
                
                if(code === "SI"){
                    $("#qualityCertificate").val("SI");
                }else{
                    $("#qualityCertificate").val("NO");
                }
                
                var certifEmit = "<c:out value="${creationProgram.deliveredCertification.code}"/>";
                
                $('#certificacionEmitida').val(certifEmit);
                
                if($("#certificationid").val() === '7'){
                    $(".divCual").show();
                }else{
                    $(".divCual").hide();
                }

                $("#codigoPrograma").keypress(function (e) {
                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                              return false;
                    }
                })

                $("#hours").keypress(function (e) {
                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                              return false;
                    }
                });

                $('#programs').DataTable({
                    "language": {
                        "url": "resources/js/Spanish.json"
                    }, "initComplete": function (settings, json) {
                        $(".dt-buttons").each(function (index) {
                            console.log(index + ": " + $(this).text());
                            if ($(this).find('.exportar').length === 0) {
                                $(this).prepend('<label class="exportar">Exportar a:&nbsp;&nbsp;</label>');
                            }
                        });
                    },
                    "bFilter": false,
                    "bInfo": false,
                    dom: 'Blfrtip',
                    buttons: [
                        {
                            extend: 'csv',
                            fieldBoundary: '',
                            footer: false,
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'CSV',
                            exportOptions: {
                                columns: [0, 1, 2]
                            }
                        },
                        {
                            extend: 'excel',
                            footer: false,
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'EXCEL',
                            exportOptions: {
                                columns: [0, 1, 2]
                            }
                        },
                        {
                            extend: 'pdf',
                            footer: false,
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'PDF',
                            exportOptions: {
                                columns: [0, 1, 2]
                            }
                        },
                        {
                            extend: 'print',
                            footer: false,
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'COPY',
                            exportOptions: {
                                columns: [0, 1, 2]
                            }
                        }
                    ]
                });

            <c:if test="${not empty creationProgram.id}">
                    
                $('#creationModal').modal('show');
                $('#typeid').val(${creationProgram.type.id});
                $('#cineid').val(${creationProgram.cine.id});
                $('#necessity').val("${creationProgram.necessity}");
                $('#observaciones').val("${creationProgram.observations}");
                $('#observaciones').val("${creationProgram.observations}");
                $('#observaciones').val("${creationProgram.observations}");
                $('#hours').val("${creationProgram.hours}");
                $('.mascara').hide();
                $('#name').attr("placeholder", "Nombre del Programa");
                $('#name').removeClass('input2');

            </c:if>
            <c:if test="${empty creationProgram.id}">

                <c:if test="${not empty creationProgram.observations}">
                                            $('#observaciones').val(${creationProgram.observations});
                </c:if>

                <c:if test="${not empty creationProgram.type.id}">
                                            $('#typeid').val(${creationProgram.type.id});
                                            activarTecnico();

                </c:if>
                <c:if test="${not empty creationProgram.cine.id}">
                                            $('#cineid').val(${creationProgram.cine.id});
                </c:if>
                <c:if test="${not empty creationProgram.necessity.code}">
                                            $('#necessity').val("${creationProgram.necessity}");
                </c:if>
                <c:if test="${not empty creationProgram.qualityCertificate}">
                                            $('#qualityCertificate').val("${creationProgram.qualityCertificate}");
                </c:if>
                <c:if test="${not empty internalMsg}">
                                            $('#creationModal').modal('show');
                </c:if>
            </c:if>

            });

            function isNumberKey(event) {
                var keycode = event.which;

                if (!(event.shiftKey === false && (keycode === 8 ||
                        keycode === 37 || keycode === 39 ||
                        (keycode >= 48 && keycode <= 57)))) {
                    event.preventDefault();
                }
            }

            function prepararForm() {
                
                $('#creationModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });

                $('#certificacionEmitida').val("");
                $(".divCual").hide();
                $(".divCod").hide();
                $(".divNomCerti").hide();
                $('#messageCreationProgram').hide();
                $('.mascara').hide();
                $("#creationId").val(null);
                $("#name").val("");
                $("#name").text("");
                $("#hours").val(null);
                $("#cineid").val(null);
                $("#qualityCertificate").val(null);
                $('#typeid').val("");
                $("#observaciones").val("");
                $("#necessity").val(null);
                $("#codigoProgramaDiv").hide();

            }

            function loadProgramDetail(id) {
                $('#detailArea').attr("src",
                        'detalle-programa.htm?id=' + id);
            }

            $('#saveButtonProgram').confirmation({
                onConfirm: function (event, element) {
                    switch ($('#typeid').val()) {
                        case "1":
                            $('#certificacionEmitida').val("Constancia de Asistencia");
                            $('#certificacionEmitidaV').val("4");
                            break;
                        case "2":
                            $('#certificacionEmitida').val("Constancia de Asistencia");
                            $('#certificacionEmitidaV').val("4");
                            break;
                        case "3":
                            $('#certificacionEmitida').val("T�tulo de alfabetizaci�n o bachillerato");
                            $('#certificacionEmitidaV').val("3");
                            break;
                        case "4":
                            $('#certificacionEmitida').val("Certificaci�n T�cnico Laboral");
                            $('#certificacionEmitidaV').val("1");
                            break;
                        case "5":
                            $('#certificacionEmitida').val("Certificaci�n de conocimientos acad�micos");
                            $('#certificacionEmitidaV').val("2");
                            break;
                        case "6":
                            $('#certificacionEmitida').val("Constancia de Asistencia");
                            $('#certificacionEmitidaV').val("4");
                    }
                    
                    $('#captureID').val($('#id').val());
                    
                    if (validateForm("creationProgram")) {
                        validarDuracion();
                        $('#creationProgram').submit();
                    }
                }
            });


            function activarEmision() {

                $(".divNomCerti").show();

                if ($('#qualityCertificate').val() === "NO" || $('#qualityCertificate').val() === "") {
                    $(".divNomCerti").hide();
                    $(".divCual").hide();
                } else if ($('#qualityCertificate').val() === "SI" && $("#certificationid").val() === '7') 
                    $(".divCual").show();
                else {
                    $(".divNomCerti").show();
                    $("#select").attr("selected", "selected");
                }
            }
            
            function certifVal(){
                if($("#certificationid").val() === '7'){
                    $(".divCual").show();
                } else {
                    $("#whichName").val("");
                    $(".divCual").hide();
                }
            }

            function validarDuracion() {

                if ($('#typeid').val() === '4') {
                    if ($('#hours').val() < 600) {

                        $("#hours").val(null);
                        $("#alert-area2").append($("<div class='alert alert-message alert-danger" +
                                " fade in' data-alert><p> La duracion debe ser mayor a 600 </p></div>"));
                        $(".alert-message").delay(3000).fadeOut("slow", function () {
                            $(this).remove();
                        });

                    }
                } else if ($('#typeid').val() === '1') {
                    if ($('#hours').val() < 40) {

                        $("#hours").val(null);

                        $("#alert-area2").append($("<div class='alert alert-message alert-danger" +
                                " fade in' data-alert><p> La duracion debe ser mayor a 40</p></div>"));
                        $(".alert-message").delay(3000).fadeOut("slow", function () {
                            $(this).remove();
                        });
                    }
                } else if ($('#hours').val() < 1) {

                    $("#hours").val(null);
                    $("#alert-area2").append($("<div class='alert alert-message alert-danger" +
                            " fade in' data-alert><p> La duracion debe ser mayor a 0</p></div>"));
                    $(".alert-message").delay(3000).fadeOut("slow", function () {
                        $(this).remove();
                    });

                }

            }

            function activarTecnico() {

                if ($('#typeid').val() === '4') {

                    $('.mascara').show();
                    $('#name').removeAttr('placeholder');
                    $('#name').addClass('input2');
                } else {
                    $('.mascara').hide();
                    $('#name').attr("placeholder", "Nombre del Programa");
                    $('#name').removeClass('input2');

                }
                
                switch ($('#typeid').val()) {
                    case "1":
                        $('#certificacionEmitida').val("Constancia de Asistencia");
                        $('#certificacionEmitidaV').val("4");
                        break;
                    case "2":
                        $('#certificacionEmitida').val("Constancia de Asistencia");
                        $('#certificacionEmitidaV').val("4");
                        break;
                    case "3":
                        $('#certificacionEmitida').val("T�tulo de alfabetizaci�n o bachillerato");
                        $('#certificacionEmitidaV').val("3");
                        break;
                    case "4":
                        $('#certificacionEmitida').val("Certificaci�n T�cnico Laboral");
                        $('#certificacionEmitidaV').val("1");
                        break;
                    case "5":
                        $('#certificacionEmitida').val("Certificaci�n de conocimientos acad�micos");
                        $('#certificacionEmitidaV').val("2");
                        break;
                    case "6":
                        $('#certificacionEmitida').val("Constancia de Asistencia ");
                        $('#certificacionEmitidaV').val("4");

                }
                
            }

            function resetForm() {
                creationProgram.reset();
            }
            
            function validarCampos() {

                var programa = document.getElementById("nombrePrograma").value;
                var programacode = document.getElementById("codigoPrograma").value;
                var capacitacion = document.getElementById("tipoCapacitacion").value;



                if ((programa === null || programa.length === 0 || /^\s+$/.test(programa)) &&
                        (programacode === null || programacode.length === 0 || /^\s+$/.test(programacode)) &&
                        (capacitacion === '0')

                        ) {

                    $("#alert-area").append($("<div  class='alert alert-message alert-danger" +
                            " fade in' data-alert><font size=3> Debe seleccionar al menos un filtro </font></div>"));
                    $(".alert-message").delay(3000).fadeOut("slow", function () {
                        $(this).remove();
                    });
                } else {

                    $('#valid_form').submit();

                }

            }
            
            function editProgram(id) {
                
                $('#creationModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                
                onEdit("/masterdata/obtener-programa.htm?id=" + id,
                        "creationModal", afterEdit);
            }
            
            function afterEdit() {
                $("#id").attr("readonly", "readonly");
                $(".divCod").hide();
                if($.trim($("#typeid option:selected").text()) === "Comp claves y transversales" || 
                        $.trim($("#typeid option:selected").text()) === "M�dulo TICS" ||
                        $.trim($("#typeid option:selected").text()) === "Reentrenamiento t�cnico"){
                    $("#certificacionEmitida").val("Constancia de Asistencia");
                } else if ($.trim($("#typeid option:selected").text()) === "Validaci�n bachillerato"){
                    $("#certificacionEmitida").val("T�tulo de alfabetizaci�n o bachillerato");
                } else if ($.trim($("#typeid option:selected").text()) === "T�cnico laboral"){
                    $("#certificacionEmitida").val("Certificaci�n T�cnico Laboral");
                } else if ($.trim($("#typeid option:selected").text()) === "Certificaci�n de competencias"){
                    $("#certificacionEmitida").val("Certificaci�n de conocimientos acad�micos");
                } 
                
                if($("#qualityCertificate option:selected").text() === "SI"){
                    $(".divNomCerti").show();
                } else {
                    $(".divNomCerti").hide();
                }
                if($("#certificationid option:selected").text() === "OTRA"){
                    $(".divCual").show();
                } else {
                    $(".divCual").hide();
                }
                $('.mascara').hide();
                
            }
                                        
        </script>
        <style>
            .buttons-csv{
                display: inline-block;
                background-image:url(resources/images/csv.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-csv span{
                opacity: 0;
            }
            .buttons-excel{
                display: inline-block;
                background-image:url(resources/images/excel.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-excel span{
                opacity: 0;
            }
            .buttons-pdf{
                display: inline-block;
                background-image:url(resources/images/pdf.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-pdf span{
                opacity: 0;
            }
            .buttons-print{
                display: inline-block;
                background-image:url(resources/images/print.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-print span{
                opacity: 0;
            }

            div.dt-buttons {
                float: right;
                margin-left:10px;
            }

            .input-box { position: relative; }

            .input2 { display: block; padding: 10px 10px 10px 125px; }

            .unit { position: absolute; display: block; left: 5px; top: 7px; z-index: 9; }
        </style>
    </body>
</html>
    