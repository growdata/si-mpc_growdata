<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
    </head>
    <body>
        <c:if test="${not empty msg}">
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        <div class="col-sm-12">
            <h1>Detalles de la Instituci�n</h1>
            <br/>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="tipoInsitucion" class=" control-label">Tipo de Instituci&oacute;n</label>
                    <p class="form-control-static">${parentInstitution.type.name}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="nombreInstitucion" class="control-label">Nombre de la Instituci&oacute;n</label>
                    <p class="form-control-static">${parentInstitution.institutionName}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="nombreInstitucion" class="control-label">Codigo de la Instituci&oacute;n</label>
                    <p class="form-control-static">${parentInstitution.id}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="nombreInstitucion" class="control-label">Tipo de Documento</label>
                    <p class="form-control-static">${parentInstitution.documentType.name}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="nit" class="control-label">N�mero de Documento</label>
                    <p class="form-control-static">${parentInstitution.nit}</p>
                </div>
            </div>
            <c:if test="${parentInstitution.dv != null}">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="dv" class="control-label">D&iacute;gito de Verificaci&oacute;n</label>
                        <p class="form-control-static">${parentInstitution.dv}</p>
                    </div>
                </div>
            </c:if>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="origen" class=" control-label">Origen</label>
                    <p class="form-control-static">${parentInstitution.origin.label}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="naturalezaLegal" class=" control-label">Naturaleza Jur&iacute;dica</label>
                    <p class="form-control-static">${parentInstitution.legalNature.label}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="telefonoContacto" class="control-label">Tel&eacute;fono de Contacto</label>
                    <p class="form-control-static">${parentInstitution.contactPhone}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="emailContacto" class="control-label">Email de Contacto</label>
                    <p class="form-control-static">${parentInstitution.contactEmail}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="emailContacto" class="control-label">Estado de Activaci�n</label>
                    <p class="form-control-static">${parentInstitution.status.label}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="emailContacto" class="control-label">Estado de Aprobaci�n</label>
                    <p class="form-control-static">${parentInstitution.approvalStatus.label}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="emailContacto" class="control-label">Tipo de Certificaci�n Emitida</label>
                    <p class="form-control-static">${parentInstitution.typeQualityCertificate.code}</p>
                </div>
            </div>
            <c:if test="${parentInstitution.qualityCertificate != null}">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="emailContacto" class="control-label">Fecha de Vencimiento de la Certificaci&oacute;n</label>
                        <p class="form-control-static">${parentInstitution.dateToShow}</p>
                    </div>
                </div>
            </c:if>
            <div class="col-sm-4" style="vertical-align: bottom">
                <div class="form-group">
                    <form method="post" action="/masterdata/descargarCertificado.htm" target="_blank">
                        <input type="hidden" name="id" value="${parentInstitution.id}"/>
                        <button type="submit" class="btn btn-success" 
                                <c:if test="${parentInstitution.dateToShow == null}">
                                    disabled="true"
                                </c:if>
                                >Descargar Certificaci&oacute;n de Calidad </button>
                    </form>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <br/>
        <c:if test="${notSedes!=true}">
            <div class="col-sm-12">
                <h1>Sedes Asociadas</h1>
                <br/>
                <div class="panel panel-default">
                    <div class="panel-heading"><h2>B&uacute;squeda</h2></div>
                    <div class="panel-body">
                        <legend>
                            <form:form method="get" action="/masterdata/buscarSedes.htm" modelAttribute="searchHeadquarter">
                                <form:input type="hidden" path="institutionId" value="${parentInstitution.id}"/>
                                <form:input type="hidden" path="institution.id" value="${parentInstitution.id}"/>
                                <form:input type="hidden" path="institution.type.id" value="${parentInstitution.type.id}"/>
                                <form:input type="hidden" path="institution.type.name" value="${parentInstitution.type.name}"/>
                                <form:input type="hidden" path="institution.nit" value="${parentInstitution.nit}"/>
                                <form:input type="hidden" path="institution.dv" value="${parentInstitution.dv}"/>
                                <form:input type="hidden" path="institution.institutionName" value="${parentInstitution.institutionName}"/>
                                <form:input type="hidden" path="institution.origin.code" value="${parentInstitution.origin.code}"/>
                                <form:input type="hidden" path="institution.status.code" value="${parentInstitution.status.code}"/>
                                <form:input type="hidden" path="institution.legalNature.code" value="${parentInstitution.legalNature.code}"/>
                                <form:input type="hidden" path="institution.contactPhone" value="${parentInstitution.contactPhone}"/>
                                <form:input type="hidden" path="institution.contactEmail" value="${parentInstitution.contactEmail}"/>
                                <form:input type="hidden" path="institution.dateToShow" value="${parentInstitution.dateToShow}"/>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="nombreSede" class="control-label">Nombre de la Sede</label>
                                        <form:input type="text" class="form-control" id="nombreSede" 
                                                    placeholder="Nombre de la Sede" maxlength="50"
                                                    path="name"/>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="departamento" class=" control-label">Departamento</label>
                                        <form:select onchange="mostrarMunicipio()" class="form-control" id="departamento" 
                                                     path="state.id">
                                            <option value="" id="defectoDeparta">Seleccione...</option>
                                        </form:select>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4 mostrarMunicipioBusq">
                                    <div class="form-group">
                                        <label for="municipio" class=" control-label">Municipio</label>
                                        <form:select class="form-control" id="municipio" 
                                                     path="city.id">
                                            <option value="" id="defectoMunicipio">Seleccione...</option>
                                        </form:select>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="col-sm-offset-4 col-sm-8" style="text-align:right">
                                    <button type="button" class="btn btn-success" 
                                            data-toggle="modal" data-target="#creationModal"
                                            onclick="loadCreationForm(${parentInstitution.id})">Nuevo</button>
                                    <button type="submit" class="btn btn-success">Buscar</button>
                                </div>
                            </form:form>
                        </legend>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading"><h2>Detalle de las Sedes</h2></div>
                    <div class="panel-body">
                        <div class=" table-responsive">
                            <table id="institutions" class="table table-striped table-bordered" 
                                   cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>C�digo de la Sede</th>
                                        <th>Nombre de la Sede</th>
                                        <th>Direcci&oacute;n</th>
                                        <th>Municipio</th>
                                        <th>Departamento</th>
                                        <th>Principal</th>
                                        <th style="width: 150px; text-align: center">Acciones</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>C�digo de la Sede</th>
                                        <th>Nombre de la Sede</th>
                                        <th>Direcci&oacute;n</th>
                                        <th>Municipio</th>
                                        <th>Departamento</th>
                                        <th>Principal</th>
                                        <th>Acciones</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <c:if test="${not empty headquarterList}">
                                        <c:forEach var="item" items="${headquarterList}">
                                            <tr>
                                                <td>${item.codigo}</td>
                                                <td>${item.name}</td>
                                                <td>${item.address}</td>
                                                <td>${item.state.name}</td>
                                                <td>${item.city.name}</td>
                                                <td>
                                                    <c:if test="${item.principal==true}">
                                                        <input type="checkbox" name="dance"
                                                               checked disabled readonly/>
                                                    </c:if>
                                                </td>
                                                <td>
                                                    <c:if test="${parentInstitution.status == 'A'}">
                                            <center>
                                                <div class="btn-group" role="group" >
                                                    <button type="button" class="btn btn-warning btn-xs" 
                                                            data-toggle="modal" data-target="#creationModal"
                                                            onclick="loadCreationForm(${parentInstitution.id}, ${item.id})">
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button>
                                                </div>
                                            </center>
                                        </c:if>
                                        </td>
                                        </tr>
                                    </c:forEach>
                                </c:if>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal fade" id="creationModal" role="dialog" 
                         aria-labelledby="creationModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title" id="creationModalLabel">Registro de Sedes</h4>
                                </div>

                                <div class="modal-body" style="height: 80vh">
                                    <iframe id="creationArea" width="100%" height="100%" style="border: 0">
                                    </iframe>
                                </div>
                                <div class="modal-footer">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </c:if>
        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/dataTables.buttons.min.js"></script>
        <script src="resources/js/buttons.flash.min.js"></script>
        <script src="resources/js/jszip.min.js"></script>
        <script src="resources/js/pdfmake.min.js"></script>
        <script src="resources/js/vfs_fonts.js"></script>
        <script src="resources/js/buttons.html5.min.js"></script>
        <script src="resources/js/buttons.print.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-confirmation.js"></script>
        <script>
                                                                $(document).ready(function () {

                                                                    $('.mostrarMunicipioBusq').hide();

                                                                    $('#institutions').DataTable({
                                                                        "language": {
                                                                            "url": "resources/js/Spanish.json"
                                                                        }, "initComplete": function (settings, json) {
                                                                            $(".dt-buttons").each(function (index) {
                                                                                console.log(index + ": " + $(this).text());
                                                                                if ($(this).find('.exportar').length === 0) {
                                                                                    $(this).prepend('<label class="exportar">Exportar a:&nbsp;&nbsp;</label>');
                                                                                }
                                                                            });
                                                                        },
                                                                        "bFilter": false,
                                                                        "bInfo": false,
                                                                        dom: 'Blfrtip',
                                                                        buttons: [
                                                                            {
                                                                                extend: 'csv',
                                                                                fieldBoundary: '',
                                                                                footer: false,
                                                                                text: '<i class="fa fa-files-o"></i>',
                                                                                titleAttr: 'CSV',
                                                                                exportOptions: {
                                                                                    columns: [0, 1, 2]
                                                                                }
                                                                            },
                                                                            {
                                                                                extend: 'excel',
                                                                                footer: false,
                                                                                text: '<i class="fa fa-files-o"></i>',
                                                                                titleAttr: 'EXCEL',
                                                                                exportOptions: {
                                                                                    columns: [0, 1, 2]
                                                                                }
                                                                            },
                                                                            {
                                                                                extend: 'pdf',
                                                                                footer: false,
                                                                                text: '<i class="fa fa-files-o"></i>',
                                                                                titleAttr: 'PDF',
                                                                                exportOptions: {
                                                                                    columns: [0, 1, 2]
                                                                                }
                                                                            },
                                                                            {
                                                                                extend: 'print',
                                                                                footer: false,
                                                                                text: '<i class="fa fa-files-o"></i>',
                                                                                titleAttr: 'COPY',
                                                                                exportOptions: {
                                                                                    columns: [0, 1, 2]
                                                                                }
                                                                            }
                                                                        ]
                                                                    });
                                                                    loadStates("departamento", "municipio");
                                                                    loadCitiesByState("departamento", "municipio");
                                                                    loadStates("departamentoModal", "municipioModal");
                                                                    loadCitiesByState("departamentoModal", "municipioModal");

                                                                    $('#eliminarSedeForm').find('[data-toggle="confirmation"]').confirmation({
                                                                        onConfirm: function () {
                                                                            $('#eliminarSedeForm').submit();
                                                                        }
                                                                    });

                                                                    $('#defectoDeparta').attr("selected", "true");
                                                                    $('#defectoMunicipio').attr("selected", "true");
                                                                });

                                                                function loadStates(deptoId, mnpoId) {
            <c:if test="${not empty stateList}">
                <c:forEach var="item" items="${stateList}" varStatus="iter">
                    <c:if test="${iter.index == 0}">
                                                                    jQuery("<option>").attr("value", '${item.id}').
                                                                            text('${item.name}').appendTo("#" + deptoId);
                                                                    loadCities("${item.id}", deptoId, mnpoId);
                    </c:if>
                    <c:if test="${iter.index > 0}">
                                                                    jQuery("<option>").attr("value", '${item.id}').
                                                                            text('${item.name}').appendTo("#" + deptoId);
                    </c:if>
                </c:forEach>
            </c:if>
                                                                }

                                                                function loadCitiesByState(deptoId, mnpoId) {
                                                                    jQuery("#" + deptoId).change(function () {
                                                                        jQuery("#" + mnpoId).html('');
                                                                        var stateCode = jQuery("#" + deptoId).val();

                                                                        loadCities(stateCode, deptoId, mnpoId);
                                                                    });
                                                                }

                                                                function loadCities(stateCode, deptoId, mnpoId) {
                                                                    jQuery("#" + mnpoId).find('option').remove();
                                                                    jQuery("<option>").attr("value", '').
                                                                            text('Seleccione...').appendTo("#" + mnpoId);
            <c:if test="${not empty citiesList}">
                <c:forEach items="${citiesList}" var="item" >
                                                                    var code = "<c:out value="${item.state.id}"/>";
                                                                    if (code === stateCode) {
                                                                        jQuery("<option>").attr("value", '${item.id}').
                                                                                text('${item.name}').appendTo("#" + mnpoId);
                                                                    }
                </c:forEach>
            </c:if>
                                                                }

                                                                function loadInstitutionDetail(id) {
                                                                    $('#detailArea').attr("src",
                                                                            'detalle-institucion-oferente.htm?id=' + id);
                                                                }

                                                                function loadCreationForm(parentId, id) {
                                                                    if (id !== undefined) {
                                                                        $('#creationArea').attr("src", 'crear-sede.htm?parentId=' +
                                                                                parentId + '&id=' + id);
                                                                    } else {
                                                                        $('#creationArea').attr("src", 'crear-sede.htm?parentId=' +
                                                                                parentId);
                                                                    }
                                                                }

                                                                window.closeModal = function () {
                                                                    $('#creationModal').modal('hide');
                                                                };

                                                                function mostrarMunicipio() {
                                                                    $('.mostrarMunicipioBusq').show();
                                                                }

        </script>
        <style>
            .buttons-csv{
                display: inline-block;
                background-image:url(resources/images/csv.png);
                cursor:pointer !important;

                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-csv span{
                opacity: 0;
            }
            .buttons-excel{
                display: inline-block;
                background-image:url(resources/images/excel.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-excel span{
                opacity: 0;
            }
            .buttons-pdf{
                display: inline-block;
                background-image:url(resources/images/pdf.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-pdf span{
                opacity: 0;
            }
            .buttons-print{
                display: inline-block;
                background-image:url(resources/images/print.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-print span{
                opacity: 0;
            }
            div.dt-buttons {
                float: right;
                margin-left:10px;
            }

        </style>    
    </body>
</html>
