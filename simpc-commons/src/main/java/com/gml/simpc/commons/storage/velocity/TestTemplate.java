/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: TestTemplate.java
 * Created on: 2016/12/20, 01:34:44 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.commons.storage.velocity;

import com.gml.simpc.api.dto.TemplateDto;
import com.gml.simpc.api.entity.Headquarter;
import com.gml.simpc.api.entity.Institution;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.entity.exeption.SystemException;
import com.gml.simpc.api.entity.valuelist.City;
import com.gml.simpc.api.entity.valuelist.Foundation;
import com.gml.simpc.api.entity.valuelist.InstitutionType;
import com.gml.simpc.api.entity.valuelist.State;
import com.gml.simpc.api.enums.ApprovalInstitutionStatus;
import com.gml.simpc.api.enums.LegalNature;
import com.gml.simpc.api.enums.Origin;
import com.gml.simpc.api.enums.UserStatus;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_015_TEMPLATE_FAIL;

/**
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
public class TestTemplate {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(TestTemplate.class);

    /**
     * M&eacute;todo que verifica si una plantilla esta bien formada.
     *
     * @param template
     *
     * @return
     */
    public boolean testTemplate(String template) {
        Map<String, Object> params = new HashMap<>();
        TemplateDto information = this.getDummy();
        params.put("datos", information);

        try {
            VelocityExecutor.applyTemplate(template, params);
            return true;
        } catch (SystemException e) {
            LOGGER.error("Error", e);

            throw new SystemException(ERR_015_TEMPLATE_FAIL);
        }

    }

    /**
     * M&eacute;todo que devuelve un template dummy.
     *
     * @return
     */
    public TemplateDto getDummy() {

        Institution example = new Institution();
        //Datos de la institution
        example.setContactEmail("example@yahoo.com");
        example.setContactPhone("00202020");
        example.setQualityCertificateExpiration(new Date());
        example.setLegalNature(LegalNature.N);
        example.setApprovalStatus(ApprovalInstitutionStatus.N);
        example.setOrigin(Origin.R);
        example.setInstitutionName("elnombre");
        example.setDv(Integer.SIZE);
        example.setNit("Identificacion");
        example.setType(new InstitutionType());

        //Datos del consultor
        User newuser = new User();
        newuser.setFirstName("Elnombre");
        newuser.setId(Long.MIN_VALUE);
        newuser.setFirstSurname("lastname");
        newuser.setStatus(UserStatus.A);
        newuser.setPhoneNumber("392183");
        newuser.setEmail("email");
        newuser.setId(Long.MIN_VALUE);
        newuser.setIdentificationNumber("Identificacion");
        Foundation newFoundation = new Foundation();
        newFoundation.setCode("sdasd");
        newFoundation.setNit("321312");
        newFoundation.setName("laCCF");
        newuser.setCcf(newFoundation);

        example.setCreationUserId(newuser);
        example.setModificationUserId(newuser);

        Headquarter newOne = new Headquarter();
        newOne.setName("LaSede");
        newOne.setAddress("No 4 kr 232");
        newOne.setCity(new City());
        newOne.setState(new State());
        newOne.setPrincipal(true);

        List<Headquarter> headquarters = new ArrayList();
        headquarters.add(newOne);

        return new TemplateDto(example, "unejemplo", headquarters);
    }
}
