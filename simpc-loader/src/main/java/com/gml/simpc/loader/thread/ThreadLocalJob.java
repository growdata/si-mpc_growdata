/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: LocalThreadMessage.java
 * Created on: 2017/01/03, 11:56:31 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.thread;

import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.dto.ValidationErrorDto;
import java.util.List;

/**
 * Clase para almacenar en thread local el resultado de la ejecuci�n de un
 * job de carga
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica Jim&eacute;nez Rozo</a>
 */
public final class ThreadLocalJob {
    
    /**
     * Objeto thread local.
     */
    public static final ThreadLocal<JobResultDto> THREAD  = new ThreadLocal<>();
 
    /**
     * Constructor privado para impedir instacias de la clase.
     */
    private ThreadLocalJob() {}
 
    /**
     * Inicializa en el thread local un nuevo objeto de tipo JobResultDto.
     */
    public static void init() {
        JobResultDto result = new JobResultDto();
        THREAD.set(result);
    }
    
    /**
     * Incrementa el n�mero actual de l�neas procesadas.
     *
     */
    public static void setProcesed() {
        JobResultDto result =THREAD.get();
        int cont = result.getProcesedLines();
        cont++;
        result.setProcesedLines(cont);
        THREAD.set(result);
    }
    
    /**
     * Actualiza el estado actual del job.
     * @param valor
     *
     */
    public static void setStatus(String status) {
        JobResultDto result =THREAD.get();
        result.setStatus(status);
        THREAD.set(result);
    }
    
    /**
     * Adiciona un error de validaci�n al resultado.
     * @param message
     *
     */
    public static void setError(String message) {
        JobResultDto result =THREAD.get();
        List <ValidationErrorDto> errorList = result.getErrorList();
        ValidationErrorDto error = new ValidationErrorDto(result.getProcesedLines(),message);
        errorList.add(error);
        result.setErrorList(errorList);
        THREAD.set(result);
    }
 
    /**
     * Elimina el valor del thread.
     */
    public static void delete() {
        THREAD.remove();
    }
 
    /**
     * Obtiene el resultado actual del job.
     * @return JobResultDto
     */
    public static JobResultDto getResult() {
        return THREAD.get();
    }
    
}
