/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IRepository.java
 * Created on: 2016/10/24, 09:59:11 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository;

import com.gml.simpc.api.entity.Profile;
import com.gml.simpc.api.enums.ProfileStatus;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author <a href="mailto:jonathanp@gmlsoftware.com">Jonathan Pont�n</a>
 */
public interface ProfileRepository extends JpaRepository<Profile, Long> {

    /**
     * Consulta por ID.
     *
     * @param id
     *
     * @return
     */
    public Profile findById(Long id);

    /**
     * Query necesario para filtrar las instituciones
     *
     * @param name
     * @param status
     *
     * @return
     */
    @Query(value = "SELECT DISTINCT p.* FROM perfiles p INNER JOIN recursos r" +
        " ON r.perfil_id = p.id WHERE UPPER(p.nombre) LIKE UPPER(?1) AND " +
        "p.estado LIKE ?2 AND r.funcionalidad_id LIKE ?3",
        nativeQuery = true)
    List<Profile> findByFilters(String name, String status,
        String functionalityId);

    /**
     * M&eacute;todo para consultar perfiles por nombre.
     *
     * @param name
     * @param status
     *
     * @return
     */
    Profile findByName(String name, ProfileStatus status);
}
