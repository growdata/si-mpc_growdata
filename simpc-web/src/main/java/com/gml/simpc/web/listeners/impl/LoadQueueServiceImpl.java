/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ExpirationTokenServiceImpl.java
 * Created on: 2016/11/21, 04:12:49 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.listeners.impl;

import com.gml.simpc.api.entity.LoadProcess;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.entity.exeption.UserException;
import com.gml.simpc.api.entity.valuelist.LoadType;
import com.gml.simpc.api.enums.ProcessStatus;
import com.gml.simpc.api.services.LoadProcessService;
import static com.gml.simpc.api.utilities.Util.PARAMETER_LOAD_QUEUE;
import com.gml.simpc.commons.mail.sender.Notificator;
import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.dto.ValidationErrorDto;
import com.gml.simpc.loader.file.FileErrorWritter;
import com.gml.simpc.loader.utilities.BatchJobNames;
import com.gml.simpc.web.batch.JobService;
import com.gml.simpc.web.batch.ValidatorLocator;
import com.gml.simpc.web.listeners.LoadQueueService;
import com.gml.simpc.web.utilities.ValueListService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Clase para procesar cargas de informaci�n a partir de la validaci�n de
 * estructura hasta la notificaci�n al usuario
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "loadQueueService")
public class LoadQueueServiceImpl implements LoadQueueService {

    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(
            LoadQueueServiceImpl.class);
    private static final String NEWLINE = "\n";

    /**
     * Plantilla JDBC para consultar las propiedades.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Mensaje de confirmaci�n de finalizacion de carga
     */
    private static final String NOTIFICATION_MESSAGE
            = "Se adelant� el procedimiento de carga de informaci�n de %s.<br>"
            + "El detalle de la carga es el siguiente:<br>"
            + "Id de carga: %s<br>"
            + "Fecha de carga: %s<br>"
            + "Duraci�n del proceso: %s ms<br>"
            + "Peso del archivo: %s<br>"
            + "N�mero total de procesos presentados: %s<br>"
            + "N�mero total de registros que cumplen: %s<br>"
            + "N�mero total de registros que no cumplen: %s";

    /**
     * Servicio para manejar procesos de carga desde repositorio jpa
     */
    @Autowired
    private LoadProcessService loadProcessService;

    /**
     * Servicio para el lanzamiento de jobs
     */
    @Autowired
    private JobService jobService;

    /**
     * Servicio para ubicaci�n de validadores de negocio
     */
    @Autowired
    private ValidatorLocator validatorLocatorService;

    /**
     * Servicio de notificaci&oacute;n de correos
     */
    @Autowired
    private Notificator notificator;

    /**
     * Servicio para consulta de listas de valor
     */
    @Autowired
    private ValueListService valueListService;

    /**
     * M&ecute;todo asincrono para procesar carges de informaci�n al
     * administrador en caso de ser necesario
     */
    @Scheduled(fixedRate = 60000)
    @Override
    public void init() {
        //TODO: para convertir este m�todo en fases.
        LoadProcess nextProcess = getNextProcess();
        if (nextProcess != null) {
            updateStatus(nextProcess, ProcessStatus.CARGANDO_STAGE);
            String message = "Process " + nextProcess.getId();
            List<ValidationErrorDto> errorsList = new ArrayList<>();
            String errorListStr;
            BatchJobNames batchJobNames = new BatchJobNames();
            //Stage cleaning
            String tableName = batchJobNames.getStageJobTableNames().
                    get(nextProcess.getLoadType());
            this.jdbcTemplate.execute("truncate table " + tableName);
            //Stage load job
            String jobName = batchJobNames.getStageJobNames()
                    .get(nextProcess.getLoadType());
            JobResultDto launchJobResult = launchJob(nextProcess, jobName);
            if ("COMPLETED".equals(launchJobResult.getStatus())) {
                updateStatus(nextProcess, ProcessStatus.CARGADO_STAGE);
                message = message + " passed to " + nextProcess.getStatus();
                //BussinessValidation
                errorsList = evaluate(
                        nextProcess.getLoadType(), nextProcess.getId());
                if (errorsList.isEmpty()) {
                    updateStatus(nextProcess, ProcessStatus.NEGOCIO_VALIDADO);
                    message = message + ". Passed to " + nextProcess.getStatus();
                    jobName = batchJobNames.getMasterJobNames()
                            .get(nextProcess.getLoadType());
                    //Master load job
                    launchJobResult = launchJob(nextProcess, jobName);
                    if ("COMPLETED".equals(launchJobResult.getStatus())) {
                        updateStatus(nextProcess, ProcessStatus.CARGADO_MASTER);
                        message = message + ". Passed to " + nextProcess.
                                getStatus();
                    } else {
                        updateStatus(nextProcess,
                                ProcessStatus.FALLA_PROCESAMIENTO);
                        message = message + ". Error on launch master job ";
                    }

                } else {
                    errorListStr = getErrorsString(errorsList);
                    int failRows = evaluateErrors(errorsList);
                    nextProcess.setDetail(errorListStr);
                    nextProcess.setFailRows(failRows);
                    updateStatus(nextProcess, ProcessStatus.ERRORES_NEGOCIO);
                    message = message + ". finished with bussiness errors: "
                            + errorListStr;
                }
            } else {
                updateStatus(nextProcess, ProcessStatus.FALLA_PROCESAMIENTO);
                message = message + ". Error on launch stage job";
            }
            LOGGER.info(message);
            notify(nextProcess, errorsList);
            writeBussinessErrorFile(nextProcess, errorsList);
        }

    }

    /**
     * M&ecute;todo para obtener siguiente registro a procesar
     */
    private LoadProcess getNextProcess() {
        LoadProcess nextProcess = loadProcessService.findFirstValidated();
        return nextProcess;
    }

    /**
     * M&ecute;todo para lanzar job
     */
    private JobResultDto launchJob(LoadProcess nextProcess, String jobName) {
        LOGGER.info("Launching " + jobName);
        //Launching batch job
        JobResultDto launchJobResult = this.jobService.launchJob(
                nextProcess.getNewFileRoot() + nextProcess.getNewFilePath()
                + nextProcess.getNewFileName(), jobName, nextProcess.getId(),
                nextProcess.getWriteType());
        return launchJobResult;

    }

    /**
     * M&ecute;todo para actualizar estado de proceso
     */
    private void updateStatus(LoadProcess nextProcess, ProcessStatus newStatus) {
        nextProcess.setStatus(newStatus);
        Date date = new Date();
        nextProcess.setEndDate(date);
        nextProcess.setTime();
        nextProcess.setSuccessRows();
        switch (newStatus) {
            case CARGANDO_STAGE:
                nextProcess.setInitDateStage(date);
                break;
            case CARGADO_STAGE:
                nextProcess.setInitDateBussiness(date);
                break;
            case NEGOCIO_VALIDADO:
                nextProcess.setInitDateMasterData(date);
                break;
        }
        try {
            this.loadProcessService.save(nextProcess);
        } catch (UserException ex) {
            LOGGER.error("Error updating load process status", ex);
        }
    }

    private List<ValidationErrorDto> evaluate(String name, Long id) {
        List<ValidationErrorDto> errorsList = this.validatorLocatorService.
                evaluate(name, id);
        return errorsList;
    }

    private String getErrorsString(List<ValidationErrorDto> errorsList) {
        StringBuilder buffer = new StringBuilder();
        boolean first = true;

        for (ValidationErrorDto validationError : errorsList) {
            if (!first) {
                buffer.append("; ");
            }
            buffer.append("Error en l�nea ");
            buffer.append(validationError.getLine());
            buffer.append(": ");
            buffer.append(validationError.getMessage());
            first = false;
        }
        return buffer.toString();
    }

    /**
     * M&ecute;todo para notificar fin de carga
     */
    private void notify(LoadProcess nextProcess,
            List<ValidationErrorDto> errorsList) {
        Map<String, LoadType> map = this.valueListService.getValueList()
                .getLoadTypes();
        LoadType loadType = map.get(nextProcess.getLoadType());
        String message = String.format(NOTIFICATION_MESSAGE,
                loadType.getDescription(),
                nextProcess.getId(),
                nextProcess.getInitDate(),
                nextProcess.getTime(),
                nextProcess.getSize(),
                nextProcess.getTotalRows(),
                nextProcess.getSuccessRows(),
                nextProcess.getFailRows()
        );

        User sendUser = nextProcess.getConsultant();
        LOGGER.info("Sending load process notification for "
                + nextProcess.getNotifyEmail());
        this.notificator.sendNotification(nextProcess.getNotifyEmail(), message,
                sendUser.getFirstName().concat(" ").concat(sendUser.getFirstSurname()).
                        concat(" - ").concat(sendUser.getCcf().getName()), PARAMETER_LOAD_QUEUE);
    }

    private void writeBussinessErrorFile(LoadProcess loadProcess,
            List<ValidationErrorDto> errorsList) {
        FileErrorWritter fileErrWr = new FileErrorWritter();
        fileErrWr.writeErrorFile(loadProcess, errorsList, "bussiness");
    }

    private Integer evaluateErrors(List<ValidationErrorDto> errorsList) {

        List<Integer> errorListEva = new ArrayList<>();
        
        for (ValidationErrorDto validationError : errorsList) {
            errorListEva.add(validationError.getLine());
        }

    List<Object> errorListResult = errorListEva.stream().distinct().collect(Collectors.toList());

        System.out.println(errorListResult.size() + " = TAMANIO");

        return errorListResult.size();

    }
}
