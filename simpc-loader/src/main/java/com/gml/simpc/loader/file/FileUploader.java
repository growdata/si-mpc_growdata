/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: FileUploader.java
 * Created on: 2017/01/24, 10:07:23 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.file;

import com.gml.simpc.api.entity.LoadProcess;
import com.gml.simpc.api.entity.exeption.FileException;
import static com.gml.simpc.api.entity.exeption.code.FileErrorCode.FILE_ERR_008_INPUT_OUTPUT_ERROR;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

/**
 * Clase para cargar archivos de carga al servidor
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica
 * Jim&eacute;nez Rozo</a>
 */
public class FileUploader {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(FileUploader.class);

    public void sendFile(MultipartFile multFile, LoadProcess loadProcess,
        HashMap ftpParams) {

        LOGGER.info("Sending multfile " + multFile.getOriginalFilename() +
            " to " + loadProcess.getNewFileRoot() +
            loadProcess.getNewFilePath() + loadProcess.getNewFileName());

        String ext;
        long size;

        try {
            ext = multFile.getOriginalFilename()
                .substring(multFile.getOriginalFilename().lastIndexOf('.') + 1);
            size = multFile.getSize();

            byte[] bytes = multFile.getBytes();
            FileValidator.validateFile(ext, size, loadProcess.getSendType(),
                bytes);
            loadProcess.setFileSended(true);

            loadProcess.setFileInfo(ext, multFile.getOriginalFilename(),
                size);

            if ("web".equals(loadProcess.getSendType())) {
                uploadFile(bytes, loadProcess);
            }
        } catch (FileException ex) {
            loadProcess.setFileSended(false);
            LOGGER.error("Error uploading multfile ", ex);
            loadProcess.setErrorCode(ex.getFileErrorCode());
        } catch (Exception ex) {
            loadProcess.setFileSended(false);
            LOGGER.error("Error uploading multfile ", ex);
            loadProcess.setErrorCode(FILE_ERR_008_INPUT_OUTPUT_ERROR);
        }
    }

    public void sendFile(File file, LoadProcess loadProcess,
        HashMap ftpParams) {

        LOGGER.info(("Sending file " + file.getPath() + " to " +
            loadProcess.getNewFileRoot() + loadProcess.getNewFilePath() +
            loadProcess.getNewFileName()));

        String ext;
        long size;

        try {
            ext = file.getName().substring(file.getName().lastIndexOf('.') + 1);
            size = file.length();

            Path path = Paths.get(file.getPath());
            byte[] bytes = Files.readAllBytes(path);

            FileValidator.validateFile(ext, size, loadProcess.getSendType(),
                bytes);
            loadProcess.setFileSended(true);

            loadProcess.setFileInfo(ext, file.getName(), size);

            if ("web".equals(loadProcess.getSendType())) {
                uploadFile(bytes, loadProcess);
            }
        } catch (FileException ex) {
            loadProcess.setFileSended(false);
            LOGGER.error("Error uploading multfile ", ex);
            loadProcess.setErrorCode(ex.getFileErrorCode());
        } catch (Exception ex) {
            loadProcess.setFileSended(false);
            LOGGER.error("Error uploading multfile " + ex);
            loadProcess.setErrorCode(FILE_ERR_008_INPUT_OUTPUT_ERROR);
        }
    }

    public LoadProcess uploadFile(byte[] bytes, LoadProcess loadProcess) {

        LOGGER.info(("Uploading file " + " to " +
            loadProcess.getNewFileRoot() + loadProcess.getNewFilePath() +
            loadProcess.getNewFileName()));

        if (loadProcess.getFileSended() == true) {
            try {
                File directory = new File(loadProcess.getNewFileRoot() +
                    loadProcess.getNewFilePath());
                directory.mkdirs();
                File newFile = new File(directory.getAbsolutePath() +
                    System.getProperty("file.separator") + loadProcess.
                    getNewFileName());
                try (BufferedOutputStream stream = new BufferedOutputStream(
                    new FileOutputStream(newFile))) {
                    stream.write(bytes);
                }
                loadProcess.setFileSended(true);
                LOGGER.info("File succesfully uploadeded in " + newFile.
                    getPath());
            } catch (IOException ex) {
                loadProcess.setFileSended(false);
                LOGGER.error("Error uploading file ", ex);
                loadProcess.setErrorCode(FILE_ERR_008_INPUT_OUTPUT_ERROR);
            }
        }
        return loadProcess;
    }
}
