/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ApprovalServiceImpl.java
 * Created on: 2016/11/30, 01:13:11 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.entity.Approval;
import com.gml.simpc.api.entity.Institution;
import com.gml.simpc.api.entity.OptionsTemplate;
import com.gml.simpc.api.services.ApprovalService;
import com.gml.simpc.commons.annotations.AnnotationInterceptor;
import com.gml.simpc.web.repository.ApprovalRepository;
import com.gml.simpc.web.repository.TemplateRepository;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Service(value = "approvalService")
public class ApprovalServiceImpl implements ApprovalService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(HeadquarterServiceImpl.class);

    /**
     * Repositorio para las aprobaciones.
     */
    @Autowired
    private ApprovalRepository repository;

    /**
     * Repositorio para las aprobaciones.
     */
    @Autowired
    private TemplateRepository templateRepository;

    /**
     * Encontrar lista de Causales por instituci&oacute;n.
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Encontrar aprobación" +
            " Por Institución",entityTableName = "APROBACIONES")
    @Override
    public List<Approval> findByInstitution(Institution institution) {
        LOGGER.info("Ejecutando metodo [ findByInstitution de  aprobaciones ]");
        return this.repository.findByInstitution(institution.getId());
        
    }
    
    /**
     * 
     * @param approval 
     */
    @Transactional
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Guardar Aprobación",
        entityTableName = "APROBACIONES")
    @Override
    public void save(Approval approval) {
        
        this.repository.saveAndFlush(approval);
        
    }

    /**
     * Encontrar Guardar plantilla de correo.
     */
    @Transactional
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Actualizar Plantilla",
        entityTableName = "PLANTILLAS")
    @Override
    public void saveTemplate(OptionsTemplate template) {
        template.setId(1L);
        this.templateRepository.saveAndFlush(template);
        
    }

    /**
     * Buscar plantilla de aprobacion.
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Encontrar Plantilla" +
            "por Id 1",
        entityTableName = "PLANTILLAS")
    @Override
    public OptionsTemplate findTemplate() {
        return this.templateRepository.findOne(1L);
    }
}
