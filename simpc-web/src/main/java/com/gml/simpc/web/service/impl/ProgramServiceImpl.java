/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserServiceImpl.java
 * Created on: 2016/10/19, 02:11:43 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.entity.Program;
import com.gml.simpc.api.services.ProgramService;
import com.gml.simpc.api.utilities.Util;
import com.gml.simpc.commons.annotations.AnnotationInterceptor;
import com.gml.simpc.web.repository.ProgramRepository;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.gml.simpc.api.utilities.Util.likeValue;

/**
 * Servicio para administrar programas.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Service(value = "programService")
public class ProgramServiceImpl implements ProgramService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(ProgramServiceImpl.class);
    /**
     * Repositorio para los programas.
     */
    @Autowired
    private ProgramRepository programRepository;

    /**
     * Genera la lista de todos los programas.
     * 
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar Todos Los" +
        " Programas", entityTableName = "PROGRAMAS")
    @Override
    public List<Program> getAll() {
        LOGGER.info("Ejecutando metodo [ getAll de programas ]");
        return this.programRepository.findAll();
    }

    /**
     * Genera una lista de programas seg&ucute;n los filtros ingresados.
     *
     * @param program
     *
     * @return
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar Por Filtros",
        entityTableName = "PROGRAMAS")
    @Override
    public List<Program> findByFilters(Program program) {
        LOGGER.info("Ejecutando metodo [ findByFilters de programas ]");
        String programType = Util.ALL_VALUES;

        LOGGER.info("VALOR DEL NOMBRE " + likeValue(program.getName()));
        
        if (program.getType().getId() != null) {
            programType = Util.nullValue(program.getType().getId().toString());
        }
                return this.programRepository.
                    findByFilters(likeValue(program.getName()),
                        likeValue(program.getCode()), programType);
    }
    
    /**
     * Guarda programas.
     * 
     * @param program 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Guardar",
        entityTableName = "PROGRAMAS")
    @Override
    public void save(Program program) {
        
        if("NO".equals(program.getQualityCertificate())){
            program.setCertification(null); 
            program.setWhichName(null);
        }
        this.programRepository.save(program);
        if (program.getId() != null) {
            program.setCode(String.valueOf(program.getId()));
            this.programRepository.saveAndFlush(program);
        }
    }
    
    /**
     * Actualiza programas.
     * 
     * @param program 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Guardar",
        entityTableName = "PROGRAMAS")
    @Override
    public void saveUpdate(Program program) {
        this.programRepository.save(program);
    }

    /**
     * Guarda programas.
     * 
     * @param program 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Actualizar",
        entityTableName = "PROGRAMAS")
    @Override
    public void update(Program program) {
        this.programRepository.saveAndFlush(program);
    }

    /**
     * Elimina programas.
     * 
     * @param program 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Borrar",
        entityTableName = "PROGRAMAS")
    @Override
    public void remove(Program program) {
        this.programRepository.delete(program);
    }

    /**
     * Encuentra un programa seg&ucute;n su id.
     * 
     * @param id
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar Por Id",
        entityTableName = "PROGRAMAS")
    @Override
    public Program findById(Long id) {
        LOGGER.info("Ejecutando metodo [ findById ]");
        return this.programRepository.findOne(id);
    }

    /**
     * Encuentra un programa seg&ucute;n su codigo.
     * 
     * @param code
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Encontrar por c�digo",
        entityTableName = "PROGRAMAS")
    @Override
    public Program findByCode(String code) {
        LOGGER.info("Ejecutando metodo [ findByCode ]");
        return this.programRepository.findByCode(code);
    }
}
