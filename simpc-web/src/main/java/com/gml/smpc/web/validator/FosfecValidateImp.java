/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Light.java
 * Created on: 2017/01/18, 04:30:10 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.smpc.web.validator;

import com.gml.simpc.loader.dto.ValidationErrorDto;
import com.gml.simpc.loader.validator.ValidateCommand;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

/**
 * Clase para implementar validaciones particulares de negocio en carges de
 * informaci�n
 * en cuanto a Fosfec
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
@Service(value = "fosfecValidateService")
public class FosfecValidateImp implements ValidateCommand {

    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(
        FosfecValidateImp.class);

    /**
     * Consulta para encontrar programas de instituciones inactivas
     */
    private static final String RULE_QUERY = "Select GI.linea as LINEA,  " +
"        ' No existe registro  de la persona con  tipo y n�mero de  " +
"        documento '|| GI.TIPO_IDENTIFICACION||' - '||GI.NUMERO_IDENTIFICACION ||  " +
"        ' en el maestro de datos b�sicos de hoja de vida.'  as MENSAJE  " +
"        FROM ST_FOSFEC GI left join MD_F_DATOS_BASICOS MDB  " +
"        ON (GI.TIPO_IDENTIFICACION = MDB.TIPO_DOCUMENTO AND  " +
"        GI.NUMERO_IDENTIFICACION=MDB.NUMERO_DOCUMENTO) where  " +
"        MDB.NUMERO_DOCUMENTO is null and GI.CARGA_ID=?";

    /**
     * Plantilla JDBC para consultar las propiedades.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Realiza la validaci�n.
     *
     * @param loadId
     *
     * @return
     */
   @Override
    public List<ValidationErrorDto> validate(Long loadId) {

        List<ValidationErrorDto> errorsList =
            this.jdbcTemplate.query(RULE_QUERY,new Long[]{loadId}, new RowMapper() {
                @Override
                public ValidationErrorDto mapRow(ResultSet rs, int rownumber)
                    throws SQLException {
                    ValidationErrorDto e = new ValidationErrorDto(
                        Integer.parseInt(rs.getString("LINEA")),
                        rs.getString("MENSAJE"));

                    return e;
                }
            });
        LOGGER.info("Validating fosfec  for load ... " + loadId);

        return errorsList;
    }

    /**
     * Verifica si es del tipo que le corresponde.
     *
     * @param loadType
     *
     * @return
     */
    @Override
    public boolean verify(String loadType) {
        return "PES-1".equals(loadType);
    }
}
