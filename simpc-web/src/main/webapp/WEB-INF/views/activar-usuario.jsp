<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>RECUPERACI&Oacute;N CONTRASE&Ntilde;A  "SIMPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster.bundle.min.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster-sideTip-shadow.min.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
    </head>
    <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <div class="col-sm-3" style="padding:0px;">
                        <img src="resources/images/logo_mpc.png" width="100%" alt=""/>
                    </div>
                    <div class="col-sm-8">
                        <a class="navbar-brand" href="/masterdata/index.htm">Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</a>
                    </div>
                </div>
            </div>
        </nav>
    <!--    <body style="background-color: rgba(53,52,138,1.00)">-->
    <c:if test="${not empty msg}">
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert"
                    aria-label="Close">
                <span aria-hidden="true">�</span>
            </button>
            <strong>${msg}</strong>
        </div>
    </c:if>
    <c:if test="${empty noToken}">
        <div class="panel panel-default" style="width: 70%; position: absolute;top:23%;left:15%;">
            <div class="panel-heading"><h2>Confirmaci�n Usuario</h2></div>
            <br/>
            <div class="container">
                <c:choose>
                    <c:when test="${!mintrabUser}">
                        <div class="alert alert-warning alert-dismissible" id="myAlert">
                            <font size=3><strong>�Atenci�n!</strong> La contrase�a debe tener m�nimo 8 caracteres, al menos una letra may�scula, una min�scula, un d�gito y un car�cter especial. (Ej: ?, #, /, & )</font>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="alert alert-warning alert-dismissible" id="myAlert">
                            <font size=3><strong>�Atenci�n!</strong>Para activar su usuario, por favor ingrese su nombre de usuario y contrase�a correspondientes al directorio activo del Ministerio. (* Por esta opci�n no podr� recuperar su contrase�a, por favor comun�quese con el administrador.)</font>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="panel-body">
                <legend>
                    <form:form id="editionPassword" method="post" action="/masterdata/modificarClave.htm" modelAttribute="editionPassword">   
                        <input type="hidden" name="userId" value="${user.id}"/>
                        <input type="hidden" name="token" value="${token}"/>
                        <input type="hidden" name="older" value="${isOldUser}"/>
                        <input type="hidden" name="mintrab" value="${mintrabUser}"/>
                        
                        <c:choose>
                            <c:when test="${!mintrabUser}">
                                <c:if test="${!isOldUser}">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="user-name-usuario" class="control-label">Nombre de Usuario(*)</label>
                                            <form:input type="text" class="form-control" id="userName" 
                                                        placeholder="Nombre de Usuario" required="required"
                                                        path="userName" autocomplete="off" />
                                        </div>
                                    </div>
                                </c:if>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="user-pass-usuario" class="control-label">Contrase�a(*)</label>
                                        <form:input type="password" class="form-control" id="user-pass-usuario" 
                                                    placeholder="Contrase�a de Usuario" required="required"
                                                    data-rule-passrule="true" path="newPassword" autocomplete="off"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="user-pass-usuario" class="control-label">Confirmaci�n Contrase�a(*)</label>
                                        <form:input type="password" class="form-control" id="user-pass-usuario2" 
                                                    placeholder="Contrase�a de Usuario" required="required"
                                                    data-rule-passrule="true" path="confirmationPassword" autocomplete="off"/>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="modal-footer">                        
                                    <div class="modal-footer">                        
                                        <button id="saveButton" class="btn btn-success" type="submit"
                                                data-btn-ok-label="Confirmar" data-btn-cancel-label="Cancelar"
                                                data-title="Est� seguro?" 
                                                data-toggle="confirmation" data-placement="left" 
                                                confirmation-callback>Guardar   
                                        </button>
                                    </div>     
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="user-name-usuario" class="control-label">Nombre de Usuario(*)</label>
                                        <form:input type="text" class="form-control" id="userName" 
                                                    placeholder="Nombre de Usuario" required="required"
                                                    path="userName" autocomplete="off" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="user-pass-usuario" class="control-label">Contrase�a(*)</label>
                                        <form:input type="password" class="form-control" id="user-pass-usuario" 
                                                    placeholder="Contrase�a de Usuario" required="required"
                                                    path="newPassword" autocomplete="off"/>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="modal-footer">                        
                                    <div class="modal-footer">                        
                                        <button id="saveButton" class="btn btn-success" type="submit"
                                                data-btn-ok-label="Confirmar" data-btn-cancel-label="Cancelar"
                                                data-title="Est� seguro?" 
                                                data-toggle="confirmation" data-placement="left" 
                                                confirmation-callback>Activar   
                                        </button>
                                    </div>     
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </form:form>
                </legend>
                <div class="clearfix"></div>
            </div>
        </div>
    </c:if>
    <div class="clearfix"></div>
    <div class="footer">
        <img src="resources/images/bg_banderin.jpg" width="100%" style="position: fixed;bottom: 0%;margin-bottom: 5px;"/> 
        <div class="clearfix"></div> 
    </div><!--/.page-footer-->
    <div class="clearfix"></div> 
    <script src="resources/js/jquery-1.12.3.js"></script>
    <script src="resources/js/jquery.dataTables.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/dataTables.buttons.min.js"></script>
    <script src="resources/js/buttons.flash.min.js"></script>
    <script src="resources/js/jszip.min.js"></script>
    <script src="resources/js/pdfmake.min.js"></script>
    <script src="resources/js/vfs_fonts.js"></script>
    <script src="resources/js/buttons.html5.min.js"></script>
    <script src="resources/js/buttons.print.min.js"></script>
    <script src="resources/js/dataTables.bootstrap.min.js"></script>
    <script src="resources/js/bootstrap-datepicker.js"></script>
    <script src="resources/js/bootstrap-confirmation.js"></script>
    <script src="resources/js/jquery.validate.min.js"></script>
    <script src="resources/js/tooltipster.bundle.js"></script>
    <script src="resources/js/custom-functionalities.js"></script>
    <script>
        
        $('#saveButton').confirmation({
            onConfirm: function (event, element) {
                if (validateForm("editionPassword")) {
                    $('#editionPassword').submit();
                }
            }
        });
        
        $.validator.addMethod('passrule', function (val, elem) {
            var filter = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^a-zA-Z0-9])[A-Za-z\d^a-zA-Z0-9]{8,15}/;

            if (!filter.test(val)) {
                return false;
            } else {
                return true;
            }
        }, 'Por favor, ingrese la contrase�a seg�n los par�metros indicados.');
        
    </script>
</body>
</html>