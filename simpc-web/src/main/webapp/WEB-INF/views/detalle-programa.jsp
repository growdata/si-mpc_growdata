<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
    </head>
    <body>
        <c:if test="${not empty msg}">
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        <div >
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="nit" class="control-label">C�digo del Programa</label>
                    <p class="form-control-static">${programDetailed.code}</p>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label for="tipoInsitucion" class=" control-label">Tipo de Capacitaci&oacute;n</label>
                    <p class="form-control-static">${programDetailed.type.name}</p>
                </div>
            </div>
               

            <div class="col-sm-6">
                <div class="form-group">
                    <label for="nombrePrp" class=" control-label">Nombre del Programa</label>
                    <p class="form-control-static">${programDetailed.name}</p>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label for="duracion" class="control-label">Duracion en horas</label>
                    <p class="form-control-static">${programDetailed.hours}</p>
                </div>
            </div>
       
    
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="dv" class="control-label">Nivel CINE</label>
                    <p class="form-control-static">${programDetailed.cine.value}</p>
                </div>
            </div>

           
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="certiCalidad" class="control-label">Certificaci�n de Calidad</label>
                    <p class="form-control-static">${programDetailed.qualityCertificate}</p>
                </div>
            </div>    
       
            <c:if test="${programDetailed.qualityCertificate != 'NO' && programDetailed.certification.id != '7'}">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="nomCerti" class="control-label">Nombre Certificado de Calidad</label>
                        <p class="form-control-static">${programDetailed.certification.code}</p>
                    </div>
                </div>
            </c:if>
            <c:if test="${programDetailed.qualityCertificate != 'NO' && programDetailed.certification.id == '7'}">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="nomCerti" class="control-label">Nombre Certificado de Calidad</label>
                        <p class="form-control-static">${programDetailed.whichName}</p>
                    </div>
                </div>
            </c:if>
                
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="necesidad" class="control-label">Necesidad del Curso</label>
                    <p class="form-control-static">${programDetailed.necessity}</p>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label for="observaciones" class="control-label">Observaciones</label>
                    <p class="form-control-static">${programDetailed.observations}</p>
                </div>
            </div>
        </div>


    <br/>
    <script src="resources/js/jquery-1.12.3.js"></script>
    <script src="resources/js/jquery.dataTables.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            
//            var certifEmit = "<c:out value="${programDetailed.certification.code}"/>";
//            
//            alert(certifEmit);
            
            $('#institutions').DataTable({
                "language": {
                    "url": "resources/js/Spanish.json"
                },
                "bFilter": false,
                "bInfo": false
            });

            loadStates("departamento", "municipio");
            loadStates("departamentoModal", "municipioModal");
        });

        function loadStates(deptoId, mnpoId) {
        <c:if test="${not empty stateList}">
            <c:forEach var="item" items="${stateList}" varStatus="iter">
                <c:if test="${iter.index == 0}">
            jQuery("<option>").attr("value", '${item.id}').
                    text('${item.name}').attr("selected", "true").
                    appendTo("#" + deptoId);

            loadCities("${item.id}", deptoId, mnpoId);
                </c:if>
                <c:if test="${iter.index > 0}">
            jQuery("<option>").attr("value", '${item.id}').
                    text('${item.name}').appendTo("#" + deptoId);
                </c:if>
            </c:forEach>
        </c:if>
        }

        function loadCitiesByState(deptoId, mnpoId) {
            jQuery("#" + deptoId).change(function () {
                jQuery("#" + mnpoId).html('');
                var stateCode = jQuery("#" + deptoId).val();

                loadCities(stateCode, deptoId, mnpoId);
            });
        }

        function loadCities(stateCode, deptoId, mnpoId) {
            jQuery("#" + mnpoId).find('option').remove();

        <c:if test="${not empty citiesList}">
            <c:forEach items="${citiesList}" var="item" >
            var code = "<c:out value="${item.state.id}"/>";

            if (code === stateCode) {
                jQuery("<option>").attr("value", '${item.id}').
                        text('${item.name}').appendTo("#" + mnpoId);
            }
            </c:forEach>
        </c:if>
        }

        function loadInstitutionDetail(id) {
            $('#detailArea').attr("src",
                    'detalle-institucion-oferente.htm?id=' + id);
        }

        function loadCreationForm(parentId, id) {
            if (id !== undefined) {
                $('#creationArea').attr("src", 'crear-sede.htm?parentId=' +
                        parentId + '&id=' + id);
            } else {
                $('#creationArea').attr("src", 'crear-sede.htm?parentId=' +
                        parentId);
            }
        }

        window.closeModal = function () {
            $('#creationModal').modal('hide');
        };
    </script>
</body>
</html>
