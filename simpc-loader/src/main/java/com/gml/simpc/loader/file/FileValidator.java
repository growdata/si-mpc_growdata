/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: FileValidator.java
 * Created on: 2017/01/24, 07:53:47 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.file;

import com.gml.simpc.api.entity.exeption.FileException;
import static com.gml.simpc.api.entity.exeption.code.FileErrorCode.FILE_ERR_001_EMPTY_FILE;
import static com.gml.simpc.api.entity.exeption.code.FileErrorCode.FILE_ERR_002_FILE_SIZE_INVALID;
import static com.gml.simpc.api.entity.exeption.code.FileErrorCode.FILE_ERR_003_FILE_TYPE_INVALID;
import static com.gml.simpc.api.entity.exeption.code.FileErrorCode.FILE_ERR_014_ENCODING;
import static com.gml.simpc.api.entity.exeption.code.FileErrorCode.FILE_ERR_015_FILE_TYPE_INVALID;
import static com.gml.simpc.api.utilities.Util.FTP_LOAD_MAX_SIZE;
import static com.gml.simpc.api.utilities.Util.LOAD_VALID_EXT;
import static com.gml.simpc.api.utilities.Util.LOAD_VALID_EXT_PDF;
import static com.gml.simpc.api.utilities.Util.WEB_LOAD_MAX_SIZE;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.tika.parser.txt.CharsetDetector;
import org.apache.tika.parser.txt.CharsetMatch;

/**
 * Clase para hacer validaciones sobre archivos, en cuanto a tama�o u extensi�n
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica
 * Jim&eacute;nez Rozo</a>
 */
public final class FileValidator {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(FileValidator.class);

    /**
     * Constructor privado para impedir instancias de la clase.
     */
    private FileValidator() {
    }

    /**
     * Metodo que valida el archivo en cuanto a tamano y tipo
     *
     * @param file
     * @param loadProcess
     *
     * @return
     */
    public static void validateFile(String ext, long size,
        String sendType, byte[] file) {
        LOGGER.info("Inicia la validacion del archivo.");
        
        validateEmptyFile(size);
        validateMaxSize(sendType, size);
        validateExtention(ext);
        validateEncoding(file);
    }

    /**
     * Valida que el archivo no este vac&iacute;o.
     *
     * @param size
     */
    private static void validateEmptyFile(long size) {
        LOGGER.info("Se valida que el archivo no este vacio.");
        
        if (size == 0) {
            throw new FileException(FILE_ERR_001_EMPTY_FILE);
        }
    }

    /**
     * Valida que no sea excedido el taman&tilde;o m&aacute;ximo.
     *
     * @param sendType
     * @param size
     */
    private static void validateMaxSize(String sendType, long size) {
        LOGGER.info("Se valida que el archivo no exceda el tamano maximo.");
        
        long maxSize = "web".equals(sendType) ? WEB_LOAD_MAX_SIZE :
            FTP_LOAD_MAX_SIZE;

        if (size > maxSize) {
            throw new FileException(FILE_ERR_002_FILE_SIZE_INVALID);
        }
    }

    /**
     * Verifica que la extensi&oacute;n sea v&aacute;lida.
     *
     * @param ext
     */
    private static void validateExtention(String ext) {
        LOGGER.info("Se valida la extension del archivo.");
        
        if (StringUtils.countMatches(LOAD_VALID_EXT, ext) == 0) {
            throw new FileException(FILE_ERR_003_FILE_TYPE_INVALID);
        }
    }
    
    public static void validateExtentionPDF(String ext) {
        LOGGER.info("Se valida la extension del archivo.");
        
        if (StringUtils.countMatches(LOAD_VALID_EXT_PDF, ext) == 0) {
            throw new FileException(FILE_ERR_015_FILE_TYPE_INVALID);
        }
    }

    private static void validateEncoding(byte[] file) {
        LOGGER.info("Se valida el encoding del archivo.");
        CharsetDetector detector = new CharsetDetector();
        
        detector.setText(file);
        CharsetMatch match = detector.detect();
        
        
        if (!"UTF-8".equalsIgnoreCase(match.getName())) {
            throw new FileException(FILE_ERR_014_ENCODING);
        }
    }
    
     
}
