--Insercion de usuario para pruebas
INSERT INTO USUARIOS (ID,DIRECCION,ADMINISTRADOR,FECHA_CREACION,CORREO_ELECTRONICO,PRIMER_NOMBRE,
PRIMER_APELLIDO,NUMERO_IDENTIFICACION,FECHA_MODIFICACION,PASSWORD_USUARIO,NUMERO_TELEFONO,SEGUNDO_NOMBRE,
SEGUNDO_APELLIDO,ESTADO,NOMBRE_USUARIO,CAJA_COMPENSACION,ID_USUARIO_CREACION,ID_USUARIO_MODIFICACION,
PERFIL,ULTIMO_CAMBIO_CONTRASENA,id_departamento,id_municipio,tipo_documento) 

VALUES (USER_SEQ.NEXTVAL,'cLL 64 sUR nO. 37 B 10','1',current_timestamp,
'hectorm@gmlsoftware.com','Hector','Martibnez','1024484521',null,'39dfa55283318d31afe5a3ff4a0e3253e2045e43','3057777422','Leonardo','Beltr�n','A',
'hectorm','CCF02','1',null,'1',null,'5','167',2);

COMMIT;

--Insercion de instituciones
INSERT INTO INSTITUCIONES (id,correo_electronico,fecha_creacion,digito_verificacion,nombre_institucion,naturaleza_juridica,nit,origen,estado,id_usuario_creacion,tipo_institucion_id,TIPO_DOCUMENTO)
VALUES (FOUNDATION_SEQ.NEXTVAL,'luzj@asopagos.com',CURRENT_TIMESTAMP ,'0','Institucion 1',1,'11111111111','R','A',1,1,1);
INSERT INTO INSTITUCIONES (id,correo_electronico,fecha_creacion,digito_verificacion,nombre_institucion,naturaleza_juridica,nit,origen,estado,id_usuario_creacion,tipo_institucion_id,TIPO_DOCUMENTO)
VALUES (FOUNDATION_SEQ.NEXTVAL,'luzj@asopagos.com',CURRENT_TIMESTAMP ,'1','Institucion 2',1,'11111111112','R','I',1,1,1);

--Insercion de sedes
INSERT INTO SEDES (ID,DIRECCION,FECHA_CREACION,ID_USUARIO_CREACION,ID_INSTITUCION,NOMBRE,PRINCIPAL,ID_MUNICIPIO,ID_DEPARTAMENTO,CODIGO)
VALUES(SEDE_SEQ.NEXTVAL,'Calle 49 B No. 63 - 21 Edificio Camacol Piso 2',CURRENT_TIMESTAMP,1,1,'Sede1',1,(select id from municipios where divipola = '91001'),(select id from departamentos where id = (select id from municipios where divipola = '91001')),'SED1');
INSERT INTO SEDES (ID,DIRECCION,FECHA_CREACION,ID_USUARIO_CREACION,ID_INSTITUCION,NOMBRE,PRINCIPAL,ID_MUNICIPIO,ID_DEPARTAMENTO,CODIGO)
VALUES(SEDE_SEQ.NEXTVAL,'Carrera 50 No. 53 - 43 Piso 15',CURRENT_TIMESTAMP,1,1,'Sede2',1,(select id from municipios where divipola = '91263'),(select id from departamentos where id = (select id from municipios where divipola = '91263')),'SED2');

COMMIT;

--Datos de prueba para indicadores
Insert into INDICADORES (ID,NOMBRE,DESCRIPCION,VARIABLE,COLUMNAS) values (INDICATOR_SEQ.NEXTVAL,'Indicador 1','Indicador de prueba INDICATOR_SEQ.NEXTVAL','prueba.tipo=A','VALOR:VALOR,TIPO:TIPO,CCF:CCF');
Insert into GRAFICOS_INDICADOR (ID,NOMBRE,TIPO_GRAFICO,INDICADOR,PERIODICIDAD,NUMERO_PERIODOS,ETIQUETAS,VARIABLE_FECHA,VARIABLE_AGRUPACION,ETIQUETA_VARIABLE_AGRUPACION,CAMPO_AGRUPACION,FUNCION_AGREGACION)
    values (INDICATOR_GRAPH_SEQ.NEXTVAL,'Indicador 1 - Gr�fico 1','L',INDICATOR_SEQ.CURRVAL,'M',12,'','FECHA',null,null,null,'S');
Insert into GRAFICOS_INDICADOR (ID,NOMBRE,TIPO_GRAFICO,INDICADOR,PERIODICIDAD,NUMERO_PERIODOS,ETIQUETAS,VARIABLE_FECHA,VARIABLE_AGRUPACION,ETIQUETA_VARIABLE_AGRUPACION,CAMPO_AGRUPACION,FUNCION_AGREGACION)
    values (INDICATOR_GRAPH_SEQ.NEXTVAL,'Indicador 1 - Gr�fico 2','C',INDICATOR_SEQ.CURRVAL,'A',1,'T�tulo 1,T�tulo 2','FECHA',null,null,null,'S');

Insert into INDICADORES (ID,NOMBRE,DESCRIPCION,VARIABLE,COLUMNAS) values (INDICATOR_SEQ.NEXTVAL,'Indicador 2','Indicador de prueba INDICATOR_SEQ.NEXTVAL','prueba.tipo=A','VALOR:VALOR,TIPO:TIPO,CCF:CCF');
Insert into GRAFICOS_INDICADOR (ID,NOMBRE,TIPO_GRAFICO,INDICADOR,PERIODICIDAD,NUMERO_PERIODOS,ETIQUETAS,VARIABLE_FECHA,VARIABLE_AGRUPACION,ETIQUETA_VARIABLE_AGRUPACION,CAMPO_AGRUPACION,FUNCION_AGREGACION)
    values (INDICATOR_GRAPH_SEQ.NEXTVAL,'Indicador 2 - Gr�fico 3','B',INDICATOR_SEQ.CURRVAL,'M',6,null,'FECHA',null,null,null,'S');
Insert into GRAFICOS_INDICADOR (ID,NOMBRE,TIPO_GRAFICO,INDICADOR,PERIODICIDAD,NUMERO_PERIODOS,ETIQUETAS,VARIABLE_FECHA,VARIABLE_AGRUPACION,ETIQUETA_VARIABLE_AGRUPACION,CAMPO_AGRUPACION,FUNCION_AGREGACION)
    values (INDICATOR_GRAPH_SEQ.NEXTVAL,'Indicador 12- Gr�fico 4','B',INDICATOR_SEQ.CURRVAL,'M',12,null,'FECHA',null,null,null,'S');



DROP TABLE PRUEBA;
CREATE TABLE PRUEBA 
   (	
    ID NUMBER(10,0) NOT NULL ENABLE, 
	VALOR NUMBER(18,2) NOT NULL ENABLE, 
	TIPO CHAR(1 BYTE) DEFAULT 'A' NOT NULL ENABLE, 
	CCF VARCHAR2(50 BYTE) NOT NULL ENABLE, 
	FECHA TIMESTAMP (6) NOT NULL ENABLE
   ) ;


Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (1,'1000000','A','CCF001',to_timestamp('01/10/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (2,'1200000','A','CCF001',to_timestamp('01/10/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (3,'500000','B','CCF001',to_timestamp('01/10/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (4,'1700000','B','CCF001',to_timestamp('01/10/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (5,'1900000','A','CCF001',to_timestamp('01/10/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (6,'400000','B','CCF002',to_timestamp('01/10/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (7,'540000','B','CCF002',to_timestamp('01/10/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (8,'45000','A','CCF002',to_timestamp('01/10/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (9,'550000','B','CCF002',to_timestamp('01/10/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (10,'3450000','B','CCF002',to_timestamp('01/10/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (11,'63400000','A','CCF003',to_timestamp('01/10/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (12,'340000','B','CCF003',to_timestamp('01/10/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (13,'554300000','B','CCF003',to_timestamp('01/10/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (14,'3400000','A','CCF003',to_timestamp('01/10/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (15,'540000','B','CCF003',to_timestamp('01/10/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (16,'3400000','B','CCF004',to_timestamp('01/10/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (17,'45400000','A','CCF004',to_timestamp('01/10/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (18,'4400000','B','CCF004',to_timestamp('01/10/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (19,'4600000','B','CCF004',to_timestamp('01/10/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (20,'4800000','A','CCF004',to_timestamp('01/10/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (21,'5000000','A','CCF001',to_timestamp('01/11/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (22,'5200000','A','CCF001',to_timestamp('01/11/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (23,'5400000','B','CCF001',to_timestamp('01/11/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (24,'5600000','B','CCF001',to_timestamp('01/11/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (25,'5800000','A','CCF001',to_timestamp('01/11/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (26,'6000000','B','CCF002',to_timestamp('01/11/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (27,'6200000','B','CCF002',to_timestamp('01/11/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (28,'6400000','A','CCF002',to_timestamp('01/11/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (29,'6600000','B','CCF002',to_timestamp('01/11/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (30,'6800000','B','CCF002',to_timestamp('01/11/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (31,'7000000','A','CCF003',to_timestamp('01/11/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (32,'7200000','B','CCF003',to_timestamp('01/11/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (33,'7400000','B','CCF003',to_timestamp('01/11/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (34,'7600000','A','CCF003',to_timestamp('01/11/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (35,'7800000','B','CCF003',to_timestamp('01/11/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (36,'8000000','B','CCF004',to_timestamp('01/11/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (37,'8200000','A','CCF004',to_timestamp('01/11/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (38,'8400000','B','CCF004',to_timestamp('01/11/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (39,'8600000','B','CCF004',to_timestamp('01/11/16','DD/MM/RR'));
Insert into PRUEBA (ID,VALOR,TIPO,CCF,FECHA) 
values (40,'8800000','A','CCF004',to_timestamp('01/11/16','DD/MM/RR'));

COMMIT;