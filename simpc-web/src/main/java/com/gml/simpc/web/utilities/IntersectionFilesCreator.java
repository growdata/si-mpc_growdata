/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IntersectionFilesCreator.java
 * Created on: 2017/02/15, 10:40:27 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.utilities;

import com.gml.simpc.api.utilities.Util;
import com.gml.simpc.web.constants.WebConstants;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.List;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.stereotype.Service;

/**
 * Componente para creaci&oacute;n de archivos de cruce.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Service
public class IntersectionFilesCreator {

    /**
     * Logger de la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(IntersectionFilesCreator.class);

    /**
     * M&eacute;todo para crear archivo de excel con los resultados de las
     * consultas.
     *
     * @param listaDtos
     * @param dto
     * @param fileName
     * @param path
     */
    public void createXlsFile(List<List<?>> listaDtos, List<Object> dto, String fileName,
        String path) {
        LOGGER.info("entra a createXlsFile");

        try {
            File file = new File(path + "/" + fileName.
                concat(WebConstants.XLS_EXTENSION_FILE));
            file.getParentFile().mkdirs();

            boolean fileExist = file.exists();

            if (!fileExist) {
                fileExist = file.createNewFile();
            }

            if (fileExist) {
                try (FileOutputStream fileOut = new FileOutputStream(file)) {
                    HSSFWorkbook workbook = new HSSFWorkbook();
                    HSSFSheet worksheet = workbook.createSheet(fileName);
                    addHeader(dto, worksheet, workbook,
                        WebConstants.XLS_EXTENSION_FILE, null);
                    addValues(listaDtos, worksheet,
                        WebConstants.XLS_EXTENSION_FILE,
                        null);
                    workbook.write(fileOut);
                    fileOut.flush();
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ex);
        }
    }

    /**
     * M&eacute;todo para crear archivo txt con los resultados de las consultas.
     *
     * @param listaDtos
     * @param dto
     * @param fileName
     * @param path
     */
    public void createTxtFile(List<List<?>> listaDtos, List<Object> dto, String fileName,
        String path) {
        LOGGER.info("entra a createTxtFile");

        try {
            File file = new File(path + "/" + fileName.
                concat(WebConstants.TXT_EXTENSION_FILE));
            file.getParentFile().mkdirs();

            boolean fileExist = file.exists();

            if (!fileExist) {
                fileExist = file.createNewFile();
            }

            if (fileExist) {
                FileOutputStream fileOut = new FileOutputStream(file);
                try (BufferedWriter bw = new BufferedWriter(
                    new OutputStreamWriter(
                        fileOut))) {
                    addHeader(dto, null, null, WebConstants.TXT_EXTENSION_FILE,
                        bw);
                    addValues(listaDtos, null, WebConstants.TXT_EXTENSION_FILE,
                        bw);
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ex);
        }
    }

    /**
     * M&eacute;todo para crear archivos csv con los resultados de las
     * consultas.
     *
     * @param listaDtos
     * @param dto
     * @param fileName
     * @param path
     */
    public void createCsvFile(List<List<?>> listaDtos, List<Object> dto, String fileName,
        String path) {
        

        try {
            File file = new File(path + "/" + fileName.
                concat(WebConstants.CSV_EXTENSION_FILE));
            file.getParentFile().mkdirs();

            boolean fileExist = file.exists();

            if (!fileExist) {
                fileExist = file.createNewFile();
            }

            if (fileExist) {
                FileOutputStream fileOut = new FileOutputStream(file);
                try (BufferedWriter bw = new BufferedWriter(
                    new OutputStreamWriter(fileOut))) {
                    addHeader(dto, null, null, WebConstants.CSV_EXTENSION_FILE,
                        bw);
                    addValues(listaDtos, null, WebConstants.CSV_EXTENSION_FILE,
                        bw);
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ex);
        }
    }

    /**
     * M&eacute;todo para anadir cabecera al archivo.
     *
     * @param dto
     */
    private void addHeader(List<Object> dto, HSSFSheet worksheet,
        HSSFWorkbook workbook, String fileType, BufferedWriter file)
        throws IOException {

        //List<Object> dtos = (List) dto;

        switch (fileType) {
            case WebConstants.XLS_EXTENSION_FILE:
                int contador = 0;
                HSSFRow row1 = worksheet.createRow((short) 0);
                HSSFCellStyle cellStyle = workbook.createCellStyle();
                cellStyle.setFillForegroundColor(HSSFColor.GOLD.index);
                cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                for (Object object : dto) {
                    LOGGER.info("CREANDO HEADER PARA: " + object.getClass().getName());
                    List<String> nombresColumna =
                        Arrays.asList(object.toString().
                            split(WebConstants.RECORD_SEPARATOR));
                    for (String nombre : nombresColumna) {
                        HSSFCell cellA1 = row1.createCell((short) contador);
                        cellA1.setCellValue(nombre.
                            split(WebConstants.EQUAL_STRING)[0]);
                        cellA1.setCellStyle(cellStyle);
                        contador++;
                    }
                }
                break;
            case WebConstants.TXT_EXTENSION_FILE:
            case WebConstants.CSV_EXTENSION_FILE:
                for (Object object : dto) {
                    List<String> nombresColumna = Arrays.asList(object.
                        toString().split(
                            WebConstants.RECORD_SEPARATOR));
                    for (String nombre : nombresColumna) {
                        file.write(nombre.split(WebConstants.EQUAL_STRING)[0]);
                        if (nombresColumna.indexOf(nombre) < nombresColumna.
                            size() - 1 || dto.indexOf(object) <
                            dto.size() - 1) {
                            file.write(WebConstants.EXPORT_SEPARATOR);
                        }
                    }
                }
                break;

        }

    }

    /**
     * M&eacute;todo para agregar cabecera al archivo.
     *
     * @param dto
     */
    private void addValues(List<List<?>> listaDtos, HSSFSheet worksheet,
        String fileType, BufferedWriter file) throws IOException {
        //List<List<?>> listas = (List) listaDtos;
        int contadorX2 = 0;
        int contadorX = 0;
        int contadorCampoGeneral = 0;
        StringBuilder[] lineas;

        switch (fileType) {
            case WebConstants.XLS_EXTENSION_FILE:
                for (List<?> listaValor : listaDtos) {
                    int contadorY = 1;
                    for (Object object : listaValor) {
                        LOGGER.info("CREANDO VALORES PARA: " + object.getClass().getName());
                        contadorX = contadorX2;
                        List<String> valoresColumna = Arrays.asList(object.
                            toString().
                            split(WebConstants.RECORD_SEPARATOR));
                        HSSFRow row1;
                        if (worksheet.getRow((short) contadorY) != null) {
                            row1 = worksheet.getRow((short) contadorY);
                        } else {
                            row1 = worksheet.createRow((short) contadorY);
                        }
                        for (String nombre : valoresColumna) {
                            HSSFCell cellA1 = row1.createCell((short) contadorX);
                            String value = nombre.split(WebConstants.EQUAL_STRING)[1];
                            if ("null".equals(value))
                                value = Util.EMPTY_TEXT;
                            cellA1.setCellValue(value);
                            contadorX++;
                        }
                        contadorY++;
                    }
                    contadorX2 = contadorX;
                }
                break;
            case WebConstants.TXT_EXTENSION_FILE:
            case WebConstants.CSV_EXTENSION_FILE:
                lineas = new StringBuilder[largerSize(listaDtos)];
                for (List<?> listaValor : listaDtos) {
                    int contadorY = 0;
                    for (Object object : listaValor) {

                        List<String> valoresColumna = Arrays.asList(object.
                            toString().split(WebConstants.RECORD_SEPARATOR));
                        StringBuilder row1;
                        if (lineas[contadorY] != null &&
                            !lineas[contadorY].toString().
                            equals(Util.EMPTY_TEXT)) {
                            row1 = lineas[contadorY];
                        } else {
                            row1 = new StringBuilder();
                            row1.append(Util.EMPTY_TEXT);
                        }
                        int contadorCampo = row1.toString().split(
                            WebConstants.RECORD_SEPARATOR).length;
                        for (int i = contadorCampo; i <
                            (contadorCampoGeneral - valoresColumna.size());
                            i++) {
                            row1.append(WebConstants.EXPORT_SEPARATOR);
                        }
                        for (String nombre : valoresColumna) {
                            String value = nombre.split(WebConstants.EQUAL_STRING)[1];
                            if ("null".equals(value))
                                value = Util.EMPTY_TEXT;
                            row1.append(value);
                            if (contadorY == 1) {
                                contadorCampoGeneral++;
                            }
                            if (valoresColumna.indexOf(nombre) < valoresColumna.
                                size() - 1 || listaDtos.indexOf(listaValor) <
                                listaDtos.size() - 1) {
                                row1.append(WebConstants.EXPORT_SEPARATOR);
                            }
                        }
                        lineas[contadorY] = row1;
                        contadorY++;
                    }
                }
                file.newLine();
                if (lineas != null) {
                    for (StringBuilder stringB : lineas) {
                        file.write(stringB.toString());
                        file.newLine();
                    }
                }
                break;
        }
    }

    /**
     * M&eacute;todo para sacar el mayor size de una lsita de arrays.
     */
    private int largerSize(List<List<?>> listas) {
        int largerSize = 0;
        for (List<?> listaValor : listas) {
            if (listaValor.size() > largerSize) {
                largerSize = listaValor.size();
            }
        }

        return largerSize;
    }

    /**
     * Crea los archivos segun el cruce.
     *
     * @param fileName
     * @param path
     * @param dtoList
     * @param emptyDtoList
     */
    public void createFiles(String fileName, String path, List<List<?>> dtoList,
        List<Object> emptyDtoList) {
        createXlsFile(dtoList, emptyDtoList, fileName, path);
        createTxtFile(dtoList, emptyDtoList, fileName, path);
        createCsvFile(dtoList, emptyDtoList, fileName, path);
    }
}
