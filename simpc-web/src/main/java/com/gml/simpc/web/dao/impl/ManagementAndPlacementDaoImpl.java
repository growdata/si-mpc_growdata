/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: GraphicDaoImpl.java
 * Created on: 2016/12/27, 04:07:32 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.dao.impl;

import com.gml.simpc.api.dao.ManagementAndPlacementDao;
import com.gml.simpc.api.dto.ManagementAndPlacementDto;
import com.gml.simpc.api.utilities.Util;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * Dao que define la forma en que se consulta la informaci&oacute;n de
 * gestion y colocacion
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Repository
public class ManagementAndPlacementDaoImpl implements ManagementAndPlacementDao {

    /**
     * Logger de la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(ManagementAndPlacementDaoImpl.class);
    /**
     * Plantilla Jdbc para operar la base de datos.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Consulta para gestion y colocacion
     */
    private static final String FIND_CONSOLIDATED_POSTULANTS =
        " select  GI.estado_postulacion as ESTADO_POSTULACION, \n" +
        "sum(decode(db.sexo,'F',1,0)) MUJERES, sum(decode(db.sexo,'M',1,0))" +
        " HOMBRES FROM MD_F_GESTION_INTERMEDIACION GI INNER JOIN " +
        "MD_F_DATOS_BASICOS DB ON (GI.NUMERO_DOCUMENTO = DB.NUMERO_DOCUMENTO" +
        " AND GI.TIPO_DOCUMENTO=DB.TIPO_DOCUMENTO)  WHERE " +
        "GI.FECHA_POSTULACION >= ? and GI.FECHA_POSTULACION <= ? " +
        "and GI.ESTADO_POSTULACION LIKE ? GROUP BY " +
        "GI.ESTADO_POSTULACION ORDER BY GI.ESTADO_POSTULACION";

    @Override
    public List<ManagementAndPlacementDto> findManagementAndPlacementResults(
        Date init,
        Date finish, String postulationStatus) {

        String query = FIND_CONSOLIDATED_POSTULANTS;
        return this.jdbcTemplate.query(query, new RowMapper() {
            @Override
            public ManagementAndPlacementDto mapRow(ResultSet rs, int rownumber)
                throws SQLException {
                ManagementAndPlacementDto e = new ManagementAndPlacementDto();
                e.setPostulationStatus(Util.POSTULATION_STATUS.get(rs.getInt(
                    "ESTADO_POSTULACION") - 1));
                e.setWomen(rs.getInt("MUJERES"));
                e.setMen(rs.getInt("HOMBRES"));
                return e;
            }
        }, init, finish, Util.nullValue(postulationStatus));
    }

}
