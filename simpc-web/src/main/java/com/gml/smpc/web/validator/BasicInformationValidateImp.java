/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Light.java
 * Created on: 2017/01/18, 04:30:10 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.smpc.web.validator;

import com.gml.simpc.loader.dto.ValidationErrorDto;
import com.gml.simpc.loader.validator.ValidateCommand;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

/**
 * Clase para implementar validaciones particulares de negocio en carges de
 * información
 * en cuanto a hojas de vida datos basicos
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Leonardo Rocha</a>
 */
@Service(value = "basicInformationValidateService")
public class BasicInformationValidateImp implements ValidateCommand {

    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(
        BasicInformationValidateImp.class);

    /**
     * Consulta para las reglas de negocio
     */
    private static final String RULE_QUERY = "select 0 as LINEA, '' as " +
        "MENSAJE FROM ST_DATOS_BASICOS WHERE CODIGO_CCF ='INEXISTENTE'";

    /**
     * Plantilla JDBC para consultar las propiedades.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Realiza la validación.
     *
     * @param loadId
     *
     * @return
     */
    @Override
    public List<ValidationErrorDto> validate(Long loadId) {

        List<ValidationErrorDto> errorsList =
            this.jdbcTemplate.query(RULE_QUERY, new RowMapper() {
                @Override
                public ValidationErrorDto mapRow(ResultSet rs, int rownumber)
                    throws
                    SQLException {
                    ValidationErrorDto e = new ValidationErrorDto(
                        Integer.parseInt(rs.getString("LINEA")),
                        rs.getString("MENSAJE"));

                    return e;
                }
            });
        LOGGER.info("Validating basicInformation  for load ... " + loadId);

        return errorsList;
    }

    /**
     * Verifica si es del tipo que le corresponde.
     *
     * @param loadType
     *
     * @return
     */
    @Override
    public boolean verify(String loadType) {
        return "HDV-1".equals(loadType);
    }
}
