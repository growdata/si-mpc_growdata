/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IndicatorGraphicRepository.java
 * Created on: 2016/12/22, 03:40:04 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository;

import com.gml.simpc.api.entity.IndicatorGraphic;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repositorio de Gr&aacute;ficos de indicadores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface IndicatorGraphicRepository
    extends JpaRepository<IndicatorGraphic, Long> {

}
