/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: FileValidatorWebTest.java
 * Created on: 2017/02/08, 09:58:17 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.file;

import com.gml.simpc.api.entity.LoadProcess;
import com.gml.simpc.loader.utilities.FileUtils;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import static com.gml.simpc.api.entity.exeption.code.FileErrorCode.FILE_ERR_001_EMPTY_FILE;
import static com.gml.simpc.api.entity.exeption.code.FileErrorCode.FILE_ERR_003_FILE_TYPE_INVALID;
import static org.junit.Assert.assertTrue;

/**
 * Pruebas para validacion de archivos
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica</a>
 */
@Ignore
public class FileValidatorWebTest {

    /**
     * Logeer para la clase.
     */
    private static final org.apache.log4j.Logger LOGGER =
        org.apache.log4j.Logger.getLogger(FileValidatorWebTest.class);

    /**
     * Servicio para ejecutar jobs en modo pruebas
     */
    public FileValidatorWebTest() {
    }

    /**
     * Caso de prueba para archivos correctos
     */
    @Test
    public void testWebValidationSuccessCase() {

        LOGGER.info(
            "Running FileValidatorWebTest - testWebValidationSuccessCase");
        String fileName =
            "loadTestFiles/program/training/program-code-error.txt";
        String loadPath = "C:/SIMPC/UPLOADS/";
        File file = new File(fileName);
        FileUtils.readFile(file);
        LoadProcess loadProcess = new LoadProcess(loadPath, "web", "CAP-1", "A");
        String ext = file.getName().substring(file.getName().lastIndexOf(".") +
            1);
        Long size = file.length();
        FileValidator.validateFile(ext, size, loadProcess.getSendType(), 
                null);
        LOGGER.info(loadProcess.toString());
        assertTrue(loadProcess.getFileSended() == true);
        assertTrue(loadProcess.getErrorCode() == null);
    }

    /**
     * Caso de prueba para archivos que no existen
     */
    @Test
    public void testWebValidationNoFoudndFileCase() {
        LOGGER.info(
            "Running FileValidatorWebTest - testWebUpLoadNoFoudndFileCase");
        String fileName = "loadTestFiles/program/training/non-exists.txt";
        String loadPath = "C:/SIMPC/UPLOADS/";
        File file = new File(fileName);
        LoadProcess loadProcess = new LoadProcess(loadPath, "web", "CAP-1", "A");
        String ext = file.getName().substring(file.getName().lastIndexOf(".") +
            1);
        Long size = file.length();
        FileValidator.validateFile(ext, size, loadProcess.getSendType(), 
                null);
        LOGGER.debug(loadProcess.toString());
        assertTrue(loadProcess.getFileSended() == false);
        assertTrue(loadProcess.getErrorCode() == FILE_ERR_001_EMPTY_FILE);

    }

    /**
     * Caso de prueba para archivos vacios
     */
    @Test
    public void testWebValidationEmptyCase() {

        LOGGER.info("Running FileValidatorWebTest - testWebValidationEmptyCase");
        
        String fileName = "loadTestFiles/program/training/empty-file.txt";
        String loadPath = "C:/SIMPC/UPLOADS/";
        File file = new File(fileName);
        FileUtils.readFile(file);
        LoadProcess loadProcess = new LoadProcess(loadPath, "web", "CAP-1", "A");
        String ext = file.getName().substring(file.getName().lastIndexOf(".") +
            1);
        Long size = file.length();
        FileValidator.validateFile(ext, size, loadProcess.getSendType(), 
                null);
        assertTrue(loadProcess.getFileSended() == false);
        assertTrue(loadProcess.getErrorCode() == FILE_ERR_001_EMPTY_FILE);
    }

    /**
     * Caso de prueba para archivos de tipo incorrecto
     */
    @Test
    public void testWebValidationInvalTypeCase() {

        LOGGER.info(
            "Running FileValidatorWebTest - testWebValidationInvalTypeCase");
        
        String fileName =
            "loadTestFiles/program/training/training-test-file.csv";
        String loadPath = "C:/SIMPC/UPLOADS/";
        File file = new File(fileName);
        FileUtils.readFile(file);
        LoadProcess loadProcess = new LoadProcess(loadPath, "web", "CAP-1", "A");
        String ext = file.getName().substring(file.getName().lastIndexOf(".") +
            1);
        Long size = file.length();
        FileValidator.validateFile(ext, size, loadProcess.getSendType(), 
                null);
        assertTrue(loadProcess.getFileSended() == false);
        assertTrue(loadProcess.getErrorCode() == FILE_ERR_003_FILE_TYPE_INVALID);
    }

    /**
     * Caso de prueba para archivos correctos a partir de un MultipartFile
     */
    @Test
    public void testWebValidationSuccessMultCase() {

        LOGGER.info(
            "Running FileValidatorWebTest - testWebValidationSuccessMultCase");
        
        String fileName =
            "loadTestFiles/program/training/program-code-error.txt";
        String loadPath = "C:/SIMPC/UPLOADS/";
        File testFile = new File(fileName);
        FileUtils.readFile(testFile);
        MultipartFile multFile = fileToMockMultipartFile(testFile);
        LoadProcess loadProcess = new LoadProcess(loadPath, "web", "CAP-1", "A");
        String ext = multFile.getOriginalFilename()
            .substring(multFile.getOriginalFilename().lastIndexOf(".") + 1);
        Long size = multFile.getSize();
        FileValidator.validateFile(ext, size, loadProcess.getSendType(), 
                null);
        assertTrue(loadProcess.getFileSended() == true);
        assertTrue(loadProcess.getErrorCode() == null);

    }

    /**
     * M�todo privado que convierte un File en MultipartFile usando
     * MockMultipartFile
     *
     * @param file
     *
     * @return
     */
    public static MultipartFile fileToMockMultipartFile(File file) {

        Path path = Paths.get(file.getPath());
        String contentType = "text/plain";
        byte[] content = null;
        try {
            content = Files.readAllBytes(path);
        } catch (final IOException e) {
        }
        MultipartFile result = new MockMultipartFile(file.getName(),
            file.getName(), contentType, content);
        return result;
    }
}
