package com.gml.simpc.loader.batch;

import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import com.gml.simpc.loader.thread.ThreadLocalValueList;
import com.gml.simpc.loader.utilities.ValueListTestService;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-batch.xml",
    "classpath:batch_jobs/curriculumVitae/otherKnowledge/otherKnowledgeJob.xml"})
public class OtherKnowledgeLoadJobTest {

    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(OtherKnowledgeLoadJobTest.class);

    /**
     * Servicio para ejecutar jobs en modo pruebas
     */
    @Autowired
    JobLauncherTestUtils jobLauncherTestUtils;

    /**
     * Test  para probar carga de hojas de vida - otros cursos
     */
    @Test
    public void OtherKnowledgeTestSucces() throws Exception {

        LOGGER.info("Running OtherKnowledgeTestSucces");

        //Inicializamos local thread
        ThreadLocalJob.init();

        //Inicializamos listas de valores
        ValueListTestService valService = new ValueListTestService();
        valService.updateAll();
        ThreadLocalValueList.set(valService.getValueList());

        // Inicializamos el job.
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath",
            "file:loadTestFiles/curriculum vitae/otherKnowledge/" +
            "otherKnowledgeTestFile.txt");

        // Lanzamos el job.
        JobExecution jobExecution = jobLauncherTestUtils.
            launchJob(jobParametersBuilder.toJobParameters());

        for (Throwable t : jobExecution.getAllFailureExceptions()) {
            t.printStackTrace();
        }

        JobResultDto jobResult = ThreadLocalJob.getResult();
        LOGGER.info(jobResult.toString());

        if (!jobResult.getErrorList().isEmpty()) {
            LOGGER.info(jobResult.getErrorsString());
             
        }

        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().
            getExitCode());

        Assert.assertTrue(jobResult.getErrorList().isEmpty());

        ThreadLocalJob.delete();
    }
    
    /**
     * Test  para probar carga de hojas de vida - otros cursos error tipo
     */
    @Test
    public void OtherKnowledgeTestOtherKnowledgeError() throws Exception {

        LOGGER.info("Running OtherKnowledgeTestOtherKnowledgeError");

        //Inicializamos local thread
        ThreadLocalJob.init();

        //Inicializamos listas de valores
        ValueListTestService valService = new ValueListTestService();
        valService.updateAll();
        ThreadLocalValueList.set(valService.getValueList());

        // Inicializamos el job.
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath",
            "file:loadTestFiles/curriculum vitae/otherKnowledge/" +
            "otherKnowledgeTestFile-otherKnowledgeError.txt");

        // Lanzamos el job.
        JobExecution jobExecution = jobLauncherTestUtils.
            launchJob(jobParametersBuilder.toJobParameters());

        for (Throwable t : jobExecution.getAllFailureExceptions()) {
            t.printStackTrace();
        }

        JobResultDto jobResult = ThreadLocalJob.getResult();
        LOGGER.info(jobResult.toString());
        LOGGER.info(jobResult.toString());

        if (!jobResult.getErrorList().isEmpty()) {
            LOGGER.info(jobResult.getErrorsString());
             
        }

        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().
            getExitCode());

        Assert.assertTrue(jobResult.getErrorList().size()==1);

        ThreadLocalJob.delete();
    }
}
