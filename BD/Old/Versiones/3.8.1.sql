
ALTER TABLE md_f_fosfec MODIFY MONTO_LIQUIDADO_SALUD DEFAULT 0;
ALTER TABLE md_f_fosfec MODIFY MONTO_LIQUIDADO_PENSION DEFAULT 0;
ALTER TABLE md_f_fosfec MODIFY MONTO_LIQUIDADO_CESANTIAS DEFAULT 0;
ALTER TABLE md_f_fosfec MODIFY MONTO_LIQUIDADO_ALIMENTACION DEFAULT 0;
ALTER TABLE md_f_fosfec MODIFY VALOR_TOTAL_PRESTACIONES DEFAULT 0;

ALTER TABLE md_fosfec MODIFY MONTO_LIQUIDADO_SALUD DEFAULT 0;
ALTER TABLE md_fosfec MODIFY MONTO_LIQUIDADO_PENSION DEFAULT 0;
ALTER TABLE md_fosfec MODIFY MONTO_LIQUIDADO_CESANTIAS DEFAULT 0;
ALTER TABLE md_fosfec MODIFY MONTO_LIQUIDADO_ALIMENTACION DEFAULT 0;
ALTER TABLE md_fosfec MODIFY VALOR_TOTAL_PRESTACIONES DEFAULT 0;
 commit;



create or replace view VW_SALUD_PENSION_PERCAPITA as
select FECHA_LIQUIDACION_BENEFICIO periodo,CODIGO_CCF
,((sum(monto_liquidado_salud)) / (count(distinct(numero_identificacion||TIPO_IDENTIFICACION))))valor_percapita_salud
,((sum(monto_liquidado_pension)) / (count(distinct(numero_identificacion||TIPO_IDENTIFICACION))))valor_percapita_pension
from MD_F_FOSFEC

where monto_liquidado_salud <> 0
group by FECHA_LIQUIDACION_BENEFICIO ,CODIGO_CCF;

commit;


create or replace view VW_PRESTACIONES_PERCAPITA as
select FECHA_LIQUIDACION_BENEFICIO periodo,CODIGO_CCF
,((sum(VALOR_TOTAL_PRESTACIONES)) / (count(distinct(numero_identificacion||TIPO_IDENTIFICACION)))) valor_percapita_prestaciones

from MD_F_FOSFEC

where VALOR_TOTAL_PRESTACIONES <> 0
group by FECHA_LIQUIDACION_BENEFICIO ,CODIGO_CCF;

commit;

create or replace view VW_CUOTA_MON_PERCAPITA as
select FECHA_LIQUIDACION_BENEFICIO periodo,CODIGO_CCF
,((sum( MONTO_LIQUIDADO_CUOTA)) / (count(distinct(numero_identificacion||TIPO_IDENTIFICACION)))) valor_percapita_cuota_mon

from MD_F_FOSFEC

where MONTO_LIQUIDADO_CUOTA <> 0
group by FECHA_LIQUIDACION_BENEFICIO ,CODIGO_CCF;

commit;


create or replace view VW_CUOTA_DEPENDIETES_PERCAPITA as
select FECHA_LIQUIDACION_BENEFICIO periodo,CODIGO_CCF
,((sum( MONTO_LIQUIDADO_CUOTA)) / (count(distinct(numero_identificacion||TIPO_IDENTIFICACION)))) valor_percapita_dependientes

from MD_F_FOSFEC

where MONTO_LIQUIDADO_CUOTA <> 0
group by FECHA_LIQUIDACION_BENEFICIO ,CODIGO_CCF;

commit;


create or replace view VW_ALIMENTACION_PERCAPITA as
select FECHA_LIQUIDACION_BENEFICIO periodo,CODIGO_CCF
,((sum( MONTO_LIQUIDADO_ALIMENTACION)) / (count(distinct(numero_identificacion||TIPO_IDENTIFICACION)))) valor_percapita_alimentacion

from MD_F_FOSFEC

where MONTO_LIQUIDADO_ALIMENTACION <> 0
group by FECHA_LIQUIDACION_BENEFICIO ,CODIGO_CCF;

commit;
create or replace view VW_AHORRO_CESANTIAS_PERCAPITA as
select FECHA_LIQUIDACION_BENEFICIO periodo,CODIGO_CCF
,((sum( MONTO_LIQUIDADO_CESANTIAS)) / (count(distinct(numero_identificacion||TIPO_IDENTIFICACION)))) valor_percapita_cesantias

from MD_F_FOSFEC

where  MONTO_LIQUIDADO_CESANTIAS <> 0
group by FECHA_LIQUIDACION_BENEFICIO ,CODIGO_CCF;