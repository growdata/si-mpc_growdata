/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Approval.java
 * Created on: 2016/11/30, 11:35:44 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 * Entidad que representa las aprobaciones
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Quilindo</a>
 */
@Table(name = "APROBACIONES")
@Entity
public class Approval implements Serializable {

    /**
     * ID de la aprobacion
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator =
        "approval_seq_gen")
    @SequenceGenerator(name = "approval_seq_gen", sequenceName =
        "APPROVAL_SEQ", allocationSize = 1, initialValue= 1)
    private Long id;

    /**
     * Institucion que se esta aprobando
     */
    @Column(name = "ID_INSTITUCION", nullable = false)
    private Long institution;
    /**
     * Id de Usuario de modificaci&oacute;n.Campo de Auditor&iacute;a.
     */

    @ManyToOne
    @JoinColumn(name = "ID_User", nullable = false)
    private User modificationUser;

    /**
     * fecha de modificaci&oacute:n. Campo de Auditor&iacute;a.
     */
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modificationDate;
    /**
     * Causal de Rechazo
     */
    @NotNull(message = "Debe escribir la causa de rechazo")
    @Column(name = "CAUSALRECHAZO", nullable = false, length = 2000)
    private String rejectionCause;

    /**
     * Constructor por defecto.
     */
    public Approval() {
        this.id = null;
    }

    /**
     * Constructor con par&aacute;metros.
     *
     * @param institution
     * @param modificationUser
     * @param modificationDate
     * @param rejectionCause
     */
    public Approval(Long institution, User modificationUser,
        Date modificationDate, String rejectionCause) {
        this.institution = institution;
        this.modificationUser = modificationUser;
        this.modificationDate = modificationDate;
        this.rejectionCause = rejectionCause;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRejectionCause() {
        return rejectionCause;
    }

    public void setRejectionCause(String rejectionCause) {
        this.rejectionCause = rejectionCause;
    }

    public void setModificationUser(User modificationUser) {
        this.modificationUser = modificationUser;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Long getId() {
        return id;
    }

    public Long getInstitution() {
        return institution;
    }

    public void setInstitution(Long institution) {
        this.institution = institution;
    }

    public User getModificationUser() {
        return modificationUser;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Aprobación{").append("id=").append(id).
            append(", Id de la institución=").append(institution).
            append(", Usuario Modificador=").append(modificationUser.
            getUsername()).
            append(", Fecha de modificación=").append(modificationDate).
            append(", Causa de rechazo=").append(rejectionCause).
            append('}');
        return stringBuilder.toString();
    }

}
