/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Util.java
 * Created on: 2017/03/15, 02:21:49 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.desktop.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gml.simpc.api.dto.ResponseDto;
import org.apache.log4j.Logger;

/**
 * Utileria para el Desktop app.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public final class Util {
    
    /**   
     * host Dominio;Url de creaci&oacute;n de procesos de carga
     */
    public static final String DOMINIO_PROCESO_CARGA = "localhost:7001";
                                                        //localhost:7001
                                                        //simpc.mintrabajo.gov.co:7001
     /**   
     * host Dominio;Url conexion ssh FTP
     */
      public static final String DOMINIO_CONEXION_SSH = "ftp://192.168.2.116/";
  //    public static final String DOMINIO_CONEXION_SSH = "simpc.mintrabajo.gov.co";

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(Util.class);
    /**
     * Constructor privado para impedir instacias de la clase.
     */
    private Util() {
        // Do Nothing.
    }

    /**
     * Obtiene un ResponseDto a partir de un Json.
     *
     * @param response
     *
     * @return
     */
    public static ResponseDto getResponse(String response) {
        ObjectMapper mapper = new ObjectMapper();

        try {
            return mapper.readValue(response, ResponseDto.class);
        } catch (Exception ex) {
            LOGGER.error("Error", ex);
        }

        return null;
    }
}
