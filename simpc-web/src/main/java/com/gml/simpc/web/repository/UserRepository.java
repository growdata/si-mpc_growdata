/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserRepository.java
 * Created on: 2016/10/19, 02:03:51 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository;

import com.gml.simpc.api.entity.User;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repositorio para usuarios.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Transactional
public interface UserRepository extends JpaRepository<User, String> {

    /**
     * Este query filtra los usuarios
     *
     * @param ccfName
     * @param identificationNumber
     * @param firstName
     * @param profile
     * @param status
     *
     * @return
     */
    @Query(value
            = "SELECT * FROM USUARIOS WHERE CAJA_COMPENSACION LIKE ?1"
            + " AND NUMERO_IDENTIFICACION LIKE ?2 AND (UPPER(PRIMER_NOMBRE)"
            + " LIKE UPPER(?3) OR UPPER(SEGUNDO_NOMBRE) LIKE UPPER(?3)"
            + " OR UPPER(PRIMER_APELLIDO) LIKE UPPER(?3) OR"
            + " UPPER(SEGUNDO_APELLIDO) LIKE UPPER(?3)) AND ESTADO LIKE ?4"
            + " AND PERFIL LIKE ?5",
            nativeQuery = true)
    List<User> findByFilters(String ccfName, String identificationNumber,
            String firstName, String status, String profile);

    /**
     * Este query filtra los usuarios que no estan con una ccf
     *
     * @param ccfName
     * @param identificationNumber
     * @param firstName
     * @param status
     * @param profile
     *
     * @return
     */
    @Query(value
            = "SELECT * FROM USUARIOS WHERE CAJA_COMPENSACION = ?1"
            + " AND NUMERO_IDENTIFICACION LIKE ?2 AND (UPPER(PRIMER_NOMBRE)"
            + " LIKE UPPER(?3) OR UPPER(SEGUNDO_NOMBRE) LIKE UPPER(?3)"
            + " OR UPPER(PRIMER_APELLIDO) LIKE UPPER(?3) OR"
            + " UPPER(SEGUNDO_APELLIDO) LIKE UPPER(?3)) AND ESTADO LIKE ?4"
            + " AND PERFIL LIKE ?5",
            nativeQuery = true)
    List<User> findByFiltersNotCcf(String ccfName, String identificationNumber,
            String firstName, String status, String profile);

    /**
     * M&eacute;todo para verificar la existencia del usuario
     *
     * @param userName Nombre del usuario suministrado por el administrador
     * @param email Correo del usuario suministrado por el administrador
     *
     * @return
     *
     */
    User findByUsernameIgnoreCaseOrEmailIgnoreCase(String userName,
            String email);

    /**
     * Consulta un usuario por email, contrase&ntilde;a y estado de perfil.
     *
     * @param username
     * @param email
     * @param password
     * @param statusProfile
     *
     * @return
     */
    @Query(value = "SELECT * FROM USUARIOS JOIN PERFILES "
            + "ON USUARIOS.PERFIL = PERFILES.ID WHERE "
            + "(UPPER(NOMBRE_USUARIO) = UPPER(?1) OR "
            + "UPPER(CORREO_ELECTRONICO) = UPPER(?2)) "
            + "AND USUARIOS.PASSWORD_USUARIO = ?3 "
            + "AND PERFILES.ESTADO = ?4", nativeQuery = true)
    User searchUserLogin(String username, String email, String password,
            String statusProfile);

    /**
     * Este query trae los usuarios administradores
     *
     * @param isAdmin
     *
     * @return
     */
    List<User> findByAdmin(boolean isAdmin);

    /**
     *
     * @param userName Nombre del usuario que se desea asignar
     * @param email Por validaci&ocute;n de interfaz este parametro recibe el
     * mail del ususario
     * @param userPass Clave de acceso asignada por el usuario
     */
    @Modifying
    @Query(value = "UPDATE USUARIOS SET PASSWORD_USUARIO = ?3, "
            + "CORREO_ELECTRONICO = ?2, ESTADO = A, ULTIMO_CAMBIO_CONTRASENA = "
            + "CURRENT_TIMESTAMP WHERE UPPER(NOMBRE_USUARIO) = UPPER(?1)",
            nativeQuery = true)
    void updateCredentials(String userName, String email, String userPass);

    /**
     *
     * @param UserId
     *
     * @return
     */
    public User findById(Long id);

    /**
     * M&eacute;todo para actualizar los datos base de un usuario.
     *
     * @param firstName
     * @param secondName
     * @param firstSurname
     * @param secondSurname
     * @param phoneNumber
     * @param address
     * @param email
     * @param username
     * @param documentType
     * @param identificationNumber
     * @param stateId
     * @param cityId
     * @param ccf
     * @param status
     * @param Id
     */
    @Modifying
    @Query(value
            = "UPDATE USUARIOS SET PRIMER_NOMBRE = ?1, SEGUNDO_NOMBRE = ?2,"
            + " PRIMER_APELLIDO = ?3, SEGUNDO_APELLIDO = ?4, NUMERO_TELEFONO = ?5,"
            + " DIRECCION = ?6, CORREO_ELECTRONICO = ?7, NOMBRE_USUARIO = ?8,"
            + " TIPO_DOCUMENTO = ?9, NUMERO_IDENTIFICACION = ?10,"
            + " CAJA_COMPENSACION = ?11, ID_DEPARTAMENTO = ?12, ID_MUNICIPIO = ?13,"
            + " ESTADO = ?14 WHERE ID = ?15",
            nativeQuery = true)
    void updateUser(String firstName, String secondName, String firstSurname,
            String secondSurname, String phoneNumber, String address, String email,
            String username, Long documentType, String identificationNumber,
            String ccf, Long stateId, Long cityId, String status, Long Id);

    /**
     * M&eacute;todo para actualizar los datos base de un usuario.
     *
     * @param status
     * @param password
     * @param LastPasswordChange
     * @param Id
     */
    @Modifying
    @Query(value
            = "UPDATE USUARIOS SET ESTADO = ?1, PASSWORD_USUARIO = ?2,"
            + " ULTIMO_CAMBIO_CONTRASENA = ?3 WHERE ID = ?4",
            nativeQuery = true)
    void updateUserOld(String status, String password, Date LastPasswordChange,
            Long Id);

    /**
     * Obtiene todos los usuarios que ya haya expirado el tiempo de uso de la
     * contrase&ntilde;a.
     *
     * @return
     */
    @Query(nativeQuery = true, value = "SELECT * FROM usuarios WHERE "
            + "(trunc(CURRENT_TIMESTAMP) - trunc(ultimo_cambio_contrasena) ) > "
            + "(SELECT TO_NUMBER(VALOR) FROM PARAMETROS WHERE ID = 19) AND "
            + "ultimo_cambio_contrasena IS NOT NULL AND estado = 'A' "
            + "AND caja_compensacion <> '2'")
    List<User> findUsersToExpire();

    /**
     * Obtiene todos los usuarios pr�ximos a expirar
     *
     * @return
     */
    @Query(nativeQuery = true, value = "SELECT * FROM usuarios WHERE "
            + " ultimo_cambio_contrasena IS NOT NULL AND estado = 'A' "
            + " AND caja_compensacion <> '2' "
            + " AND ((trunc(CURRENT_TIMESTAMP)-trunc(ultimo_cambio_contrasena) ) >= "
            + " ((SELECT TO_NUMBER(VALOR)FROM PARAMETROS WHERE ID = 19) - "
            + " (SELECT TO_NUMBER(VALOR)FROM PARAMETROS WHERE ID = 23))) "
            + " AND ((trunc(CURRENT_TIMESTAMP) - trunc(ultimo_cambio_contrasena) ) < "
            + " (SELECT TO_NUMBER(VALOR) "
            + " FROM PARAMETROS WHERE ID = 19)) ")
    List<User> findUsersToExpirePrev();

}
