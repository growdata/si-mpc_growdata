/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ActionLogService.java
 * Created on: 2016/11/15, 10:46:55 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.entity.ActionLog;
import com.gml.simpc.api.entity.User;
import java.util.Date;
import java.util.List;

/**
 *Servicio  para obtener el log de las acciones
 * 
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public interface ActionLogService {

    /**
     * Obtiene todos los logs registrados.
     *
     * @return
     */
    List<ActionLog> getAll();

    /**
     * Servicio para guardar un actionLog.
     *
     * @param actionLog
     */
    void save(ActionLog actionLog);

    /**
     * Servicio para actualizar un actionLog.
     *
     * @param actionLog
     */
    void update(ActionLog actionLog);

    /**
     * Elimina un actionLog.
     *
     * @param actionLog
     */
    void remove(ActionLog actionLog);
    
    /**
     * Consultar por filtros
     * @param user
     * @param initDate
     * @param finishDate
     * @return 
     */
    List<ActionLog> getActionsByFilters(User user, Date initDate,
        Date finishDate);
    
}
