/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IndicatorSourceServiceImpl.java
 * Created on: 2016/12/16, 10:31:31 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.entity.IndicatorSource;
import com.gml.simpc.api.services.IndicatorSourceService;
import com.gml.simpc.web.repository.IndicatorSourceRepository;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Servicio para manipular indicadores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Service
public class IndicatorSourceServiceImpl implements IndicatorSourceService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(IndicatorSourceServiceImpl.class);

    /**
     * Repositorio de indicadores.
     */
    @Autowired
    private IndicatorSourceRepository indicatorSourceRepository;

    /**
     * Genera la lista de indicadores.
     * 
     * @return 
     */
    @Override
    public List<IndicatorSource> findAll() {
        LOGGER.info("Ejecutando metodo [ findAll ]");
        List<IndicatorSource> indicatorSourceList =
            this.indicatorSourceRepository.findAll();
        return indicatorSourceList;
    }

    /**
     * Genera un indicador espec&icute;fico.
     * 
     * @param indicatorSourceSel
     * @return 
     */
    @Override
    public IndicatorSource findOne(Long indicatorSourceSel) {
        IndicatorSource indicatorSource = this.indicatorSourceRepository.
            findOne(indicatorSourceSel);
        return indicatorSource;
    }
    
}
