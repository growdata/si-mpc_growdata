<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); 
    response.setHeader("Pragma", "no-cache"); 
    response.setDateHeader("Expires", 0); 
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <!-- <link rel="bootstrap.bundle.js"> --> 
    </head>

    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <div class="col-sm-3" style="padding:0px;">
                        <img src="resources/images/logo_mpc.png" width="100%" alt=""/>
                    </div>
                    <div class="col-sm-8">
                        <a class="navbar-brand" href="/masterdata/index.htm">Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</a>
                    </div>
                </div>
                <div class="navbar-nav navbar-form credenciales pull-right">
                    <table>
                        <tr>
                            <td>
                                <strong>Bienvenido:</strong> <div style="float: right; padding-left: 2px;" id="userUsername"><c:out value="${userNames}"/></div>
                            </td>
                            <td style="width: 120px; text-align: right">
                                <form:form method="post" action="/masterdata/logout.htm"> 
                                    <button id="btnCerrarSesion" type="submit" class="btn btn-warning btn-xs">Cerrar Sesi�n</button>
                                    <button onclick="loadPage('descargaAyuda.pdf')" type="button" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="right" title="Ayudas en linea"> 
                                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    </button>               
                                </form:form>
                            </td>
                        </tr>
                    </table>
                </div> 
                <center>
                    <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target=".sidebar-nav">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </center>
            </div>
        </nav>
        <div class="container">
            <div class="row row-offcanvas row-offcanvas-left">
                <!-- sidebar -->
                <div class="col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation" style="height: 77.856vh;overflow: auto">
                    <ul class="nav">
                        <c:if test="${not empty funtionalities}">
                            <c:forEach var="item" items="${funtionalities}">
                                <c:if test="${empty item.parentId}">
                                    <c:if test="${not empty setParentId and item.id != setParentId}">
                                            </ul>
                                        </li>
                                    </c:if>
                                    <li><a id="funcCategoryLnk${item.id}" data-toggle="collapse" data-target="#sub${item.id}"><i class="fa ${item.icon}" aria-hidden="true"></i> ${item.label} <span class="caret"></span></a>
                                        <ul class="nav collapse" id="sub${item.id}">
                                    <c:set var="setParentId" scope="request" value="${item.id}"/>
                                </c:if>
                                <c:if test="${not empty item.parentId}">
                                    <li><a id="funcLnk${item.id}" href="#" onclick="loadPage('${item.path}')" data-toggle="tooltip" data-placement="right" title="${item.popup}">${item.label}</a></li>
                                </c:if>
                            </c:forEach>
                        </c:if>
                    </ul>
                </div>

                <!-- main area -->
                <div class="col-xs-12 col-sm-9" style="height: 77.856vh">
                    <iframe id="mainArea" width="100%" height="100%" style="border: 0">
                    </iframe>
                </div>
            </div>
        </div>
        <div class="footer">
            <img src="resources/images/bg_banderin.jpg" width="100%"/> V 3.8
            <div class="clearfix"></div> 
        </div>
        <div class="clearfix"></div> 

        <script>
            $(document).ready(function () {
                
                $('[data-toggle=offcanvas]').click(function () {
                    $('.row-offcanvas').toggleClass('active');
                });

                // TODO: cambiar p�gina de inicio para pruebas
                // Por defecto welcome.jsp
                $('#mainArea').attr("src", "welcome.jsp");
                
                sendSesionPing("sessionPing.htm");
                setInterval(function() {sendSesionPing();}, 30000);
            });

            function sendSesionPing() {
                $.ajax({
                    url: "sessionPing.htm",
                    type: "POST",
                    processData: false
                });
            }

            function loadPage(path) {
              
                $.ajax({
                    url: "setUserName.htm",
                    data: "",
                    type: "GET",
                    contentType: "application/json",
                    dataType:"json" , 

                    success: function(user) {
                        $('#userUsername').html(user.firstName +" "+ user.firstSurname);      
                    }
                });
               
                $('#mainArea').attr("src", path);
            }
        </script>
    </body>
</html>

