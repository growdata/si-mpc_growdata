/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IService.java
 * Created on: 2016/10/24, 11:31:09 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.entity.Profile;
import com.gml.simpc.api.entity.Resource;
import java.util.List;

/**
 *
 * @author <a href="mailto:jonathanp@gmlsoftware.com">Jonathan Pont�n</a>
 */
public interface ProfileService {

    /**
     * Obtiene todos los perfil creados.
     *
     * @return
     */
    List<Profile> getAll();

    /**
     * Servicio para guardar perfil.
     *
     * @param profile
     */
    void save(Profile profile);

    /**
     * Servicio para actualizar perfil.
     *
     * @param profile
     */
    void update(Profile profile);

    /**
     * Elimina un perfil.
     *
     * @param profile
     */
    void remove(Profile profile);

    /**
     * Metodo que consulta por filtros
     *
     * @param name
     * @param status
     *
     * @return
     */
    List<Profile> findByFilters(String name, String status,
        String functionalityId);

    /**
     * Consulta los perfiles por id.
     *
     * @param id
     *
     * @return
     */
    Profile findById(Long id);

    /**
     * Actualiza las funcionalidades de un perfil.
     *
     * @param profileId
     * @param resources
     */
    void updateProfileFunctionalities(Long profileId, List<Resource> resources);
}
