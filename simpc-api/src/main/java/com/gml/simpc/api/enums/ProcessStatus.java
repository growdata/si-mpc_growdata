/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ProcessStatus.java
 * Created on: 2017/01/31, 10:52:35 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.enums;

/**
 * Enumeración para estado de procesos de carga
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica
 * Jim&eacute;nez Rozo</a>
 */
public enum ProcessStatus {
    ARCHIVO_CARGADO,ERRORES_ESTRUCTURA,FALLA_VALIDACION,ESTRUCTURA_VALIDADA,
    FALLA_PROCESAMIENTO,CARGANDO_STAGE, CARGADO_STAGE,
    ERRORES_NEGOCIO,NEGOCIO_VALIDADO,CARGANDO_MASTER,CARGADO_MASTER;





}
