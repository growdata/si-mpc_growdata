/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserException.java
 * Created on: 2016/10/21, 02:39:07 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity.exeption;

import com.gml.simpc.api.entity.exeption.code.ErrorCode;

import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_001_UNKNOW;

/**
 * Excepcion que se utiliza para encapsular errores del sistema y presentarlos
 * al usuario.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class UserException extends Exception {
    
    /**
     * C&oacute;digo de error.
     */
    private final ErrorCode errorCode;

    /**
     * Constructor por defecto.
     */
    public UserException() {
        this.errorCode = ERR_001_UNKNOW;
    }

    /**
     * @param errorCode
     */
    public UserException(ErrorCode errorCode) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
    }

    /**
     * @param errorCode
     * @param params 
     */
    public UserException(ErrorCode errorCode, Object ... params) {
        super(String.format(errorCode.getMessage(), params));
        this.errorCode = errorCode;
    }

    /**
     * @param errorCode
     * @param cause
     */
    public UserException(ErrorCode errorCode, Throwable cause) {
        super(errorCode.getMessage(), cause);
        this.errorCode = errorCode;
    }

    /**
     * @param cause
     */
    public UserException(Throwable cause) {
        super(cause);

        this.errorCode = ERR_001_UNKNOW;
    }

    /**
     * @param message
     */
    public UserException(String message) {
        super(message);

        this.errorCode = ERR_001_UNKNOW;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
