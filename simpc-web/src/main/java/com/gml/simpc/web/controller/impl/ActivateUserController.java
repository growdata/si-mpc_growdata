/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ActivateUserController.java
 * Created on: 2016/11/21, 11:52:13 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.gml.simpc.api.dto.PasswordChangeDto;
import com.gml.simpc.api.entity.Activate;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.enums.UserStatus;
import com.gml.simpc.api.services.ActivationService;
import com.gml.simpc.api.services.UserService;
import com.gml.simpc.api.utilities.Util;
import com.gml.simpc.web.controller.ControllerDefinition;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_001_UNKNOW;
import static com.gml.simpc.api.utilities.Util.FAILED_ALERT;

/**
 * Controlador para la activaci&ocute;n de cuentas de usuario.
 *
 * @author <a href="mailto:julianm@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
@Controller
public class ActivateUserController implements ControllerDefinition {

    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(ActivateUserController.class);

    /**
     * Servicio para adminisraci&oacute;n de usuarios.
     */
    @Autowired
    private UserService userService;

    /**
     * Servicio para adminisraci&oacute;n de activaciones.
     */
    @Autowired
    private ActivationService activateService;

    /**
     * M&eacute;todo que se ejecuta al abrir la p&aacute;gina.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "/activar-usuario.htm", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response) {
        LOGGER.info("Ejecutando metodo [ onDisplay ActivateUserController ]");

        ModelAndView modelAndView = new ModelAndView("activar-usuario");
        LOGGER.info("Token: " + request.getParameter("key"));
        String token = request.getParameter("key");

        Activate activate = this.activateService.findByUserToken(token);
        try {
            if (activate != null) {
                LOGGER.info("Ejecutando metodo [ onDisplay " +
                    "ActivateUserController ]");
                boolean isOldUser = activate.getUser().
                    getLastPasswordChange() != null;
                
                LOGGER.info("AQUIII" + activate.getUser().getCcf().getCode());
                
                boolean mintrabUser = "2".
                        equals(activate.getUser().getCcf().getCode());

                if (!isUserOfMinistryOfLabor(activate.getUser()) &&
                    activate.getUser().getStatus().equals(UserStatus.T)) {
                    modelAndView.addObject("userMinistryOfLabor", 1);
                }
                LOGGER.info(" user id es  igual a "+activate.getUser().getId());
                modelAndView.addObject("user", activate.getUser());
                modelAndView.addObject("editionPassword", new PasswordChangeDto());
                modelAndView.addObject("token", token);
                modelAndView.addObject("isOldUser", isOldUser);
                modelAndView.addObject("mintrabUser", mintrabUser);
                
            } else {
                LOGGER.info("No se pudo validar la existencia del token");

                init(modelAndView, true);
                modelAndView.addObject("noToken", 1);
                modelAndView.addObject("editionPassword", new PasswordChangeDto());
                modelAndView.addObject("msg", "El token no existe o ha expirado.");
            }
            
        }catch (Exception ex) {
            
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
            modelAndView.addObject("msgType", FAILED_ALERT);
        }
        
        return modelAndView;
    }

    /**
     * Activa el usuario la primera vez que entra al sistema.
     * 
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = "/activar.htm", method = RequestMethod.POST)
    public ModelAndView activateUser(HttpServletRequest request,
        HttpServletResponse response) {
        String token = request.getParameter("token");
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        try {
            Activate activate = this.activateService.findByUserToken(token);

            if (activate != null && activate.getUser().getEmail().equals(email)) {
                userService.updateCredentials(activate.getUser().getUsername(),
                    email, Util.calculateSha1(password));
                this.activateService.remove(activate);

                return new ModelAndView("activar-usuario", "msg",
                    "Sus credenciales fueron actualizadas");
            }

            return new ModelAndView("activar-usuario", "msg",
                "Error al realizar la activación. Intente nuevamente.");
        } catch (Exception e) {
            LOGGER.error("Error", e);

            return new ModelAndView("activar-usuario", "msg",
                ERR_001_UNKNOW.getMessage());
        }
    }

    /**
     * Inicia los objetos para el controler.
     * 
     * @param modelAndView
     * @param tokenError 
     */
    private void init(ModelAndView modelAndView, boolean tokenError) {
        LOGGER.info("Ejecutando metodo [ init ]");
        if (tokenError) {
            modelAndView.addObject("user", new User());
        }
    }

    /**
     * M&eacute;todo que valida si es usuario del ministerio de trabajo.
     * 
     * @param user
     * @return 
     */
    private Boolean isUserOfMinistryOfLabor(User user) {
        return user.getCcf().getNit().equals(Util.NIT_ENTIDAD_MINISTRY_OF_LABOR);
    }
}
