/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: EpsRepository.java
 * Created on: 2017/03/03, 11:17:41 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository.valuelist;

import com.gml.simpc.api.entity.valuelist.Eps;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface EpsRepository extends JpaRepository<Eps, String> {

}
