/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: GraphicDao.java
 * Created on: 2016/12/27, 04:06:52 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dao;

import com.gml.simpc.api.dto.ConsolidantPostulantDto;
import java.util.List;

/**
 * Interface que define el DAO para consultar informaci&oacute;n del portal
 * de beneficios economicos
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public interface EconomicBenefitsDao {

    /**
     * Obtiene consulta consolidada postulantes
     *
     *
     * @param idCcf
     * @param year
     * @param month
     * @return List<ConsolidantPostulantDto>
     */
    List<ConsolidantPostulantDto> getConsolidatedPostulants(String idCcf, 
        String year, String month);
    
     /**
     * Obtiene consulta detallada postulantes
     *
     *
     * @param idCcf
     * @param year
     * @param month
     * @return List<ConsolidantPostulantDto>
     */
    List<ConsolidantPostulantDto> getDetailedPostulants(String idCcf, 
        String year, String month);
    
    /**
     * Obtiene consulta consolidada beneficiarios
     *
     *
     * @param idCcf
     * @param year
     * @param month
     * @return List<ConsolidantPostulantDto>
     */
    List<ConsolidantPostulantDto> getConsolidatedBenefits(String idCcf, 
        String year, String month);
    
    /**
     * Obtiene consulta consolidada beneficiarios
     *
     *
     * @param idCcf
     * @param year
     * @param month
     * @return List<ConsolidantPostulantDto>
     */
    List<ConsolidantPostulantDto> getDetailedBenefits(String idCcf, 
        String year, String month);

}
