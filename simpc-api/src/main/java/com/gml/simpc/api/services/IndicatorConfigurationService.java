/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IndicatorService.java
 * Created on: 2016/12/16, 10:28:47 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.dto.Page;
import com.gml.simpc.api.dto.TableDataDto;
import com.gml.simpc.api.entity.IndicatorConfiguration;
import java.util.List;

/**
 * Interface que define el servicio de indicadores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface IndicatorConfigurationService {

    /**
     * Obtiene todos los indicadores del sistema.
     *
     * @return
     */
    List<IndicatorConfiguration> findAll();

    /**
     * Obtiene el indicador seleccionado
     *
     * @return
     */
    IndicatorConfiguration findOne(Long id);

    /**
     * Obtiene los datos de indicador para graficarlo
     *
     * @return
     */
    Object getDataGraphic(IndicatorConfiguration indicator);

    /**
     * Obtiene los datos para la tabla
     *
     * @return
     */
    List<TableDataDto> getData(IndicatorConfiguration indicatorConfiguration);

    /**
     * Obtiene los nombres de la columnas para la tabla
     *
     * @return
     */
    List<TableDataDto> getColumnName(
        IndicatorConfiguration indicatorConfiguration);

    /**
     * Obtiene los datos por pagina
     *
     * @return
     */
    Page<TableDataDto> getDataTablePage(
        IndicatorConfiguration indicatorConfiguration, int pageNo, int pageSize);

    /**
     * Obtiene los datos completos para el archivo en excel
     *
     * @return
     */
    List<TableDataDto> getDataForFiles(
        IndicatorConfiguration indicatorConfiguration);
}
