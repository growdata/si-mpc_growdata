/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: MailHistoryServiceImpl.java
 * Created on: 2017/01/13, 03:00:16 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.entity.MailHistory;
import com.gml.simpc.api.services.MailHistoryService;
import com.gml.simpc.web.repository.MailHistoryRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Servicio para historial de emails.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Service
public class MailHistoryServiceImpl implements MailHistoryService {

    /**
     * Logger de la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(MailHistoryServiceImpl.class);
    /**
     * Repositorio de mails.
     */
    @Autowired
    private MailHistoryRepository mailHistoryRepository;

    /**
     * Guarda historiales de correos.
     * 
     * @param mailHistory 
     */
    @Override
    public void save(MailHistory mailHistory) {
        LOGGER.info("Ejecutando metodo [ save ]");
        
        this.mailHistoryRepository.saveAndFlush(mailHistory);
    }
}
