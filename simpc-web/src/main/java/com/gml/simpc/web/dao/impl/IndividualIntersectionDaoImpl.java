/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: PilaDaoImpl.java
 * Created on: 2016/12/27, 04:07:32 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.dao.impl;

import com.gml.simpc.api.dao.IndividualIntersectionDao;
import com.gml.simpc.api.dto.BasicInformationDto;
import com.gml.simpc.api.dto.DependentDto;
import com.gml.simpc.api.dto.EducationLevelDto;
import com.gml.simpc.api.dto.InformalEducationDto;
import com.gml.simpc.api.dto.IntermediationDto;
import com.gml.simpc.api.dto.LanguageDto;
import com.gml.simpc.api.dto.OtherKnowledgeDto;
import com.gml.simpc.api.dto.PilaDto;
import com.gml.simpc.api.dto.PilaEmploymentServiceDto;
import com.gml.simpc.api.dto.ProfitDto;
import com.gml.simpc.api.dto.ProgramDto;
import com.gml.simpc.api.dto.WorkExperienceDto;
import com.gml.simpc.web.dao.mapper.BasicInformationDtoRowMapper;
import com.gml.simpc.web.dao.mapper.DependentDtoRowMapper;
import com.gml.simpc.web.dao.mapper.EducationLevelDtoRowMapper;
import com.gml.simpc.web.dao.mapper.InformalEducationDtoRowMapper;
import com.gml.simpc.web.dao.mapper.IntermediationDtoRowMapper;
import com.gml.simpc.web.dao.mapper.LanguageDtoRowMapper;
import com.gml.simpc.web.dao.mapper.OtherKnowledgeDtoRowMapper;
import com.gml.simpc.web.dao.mapper.PilaDtoRowMapper;
import com.gml.simpc.web.dao.mapper.PilaEmploymentServiceDtoRowMapper;
import com.gml.simpc.web.dao.mapper.ProfitDtoRowMapper;
import com.gml.simpc.web.dao.mapper.ProgramDtoRowMapper;
import com.gml.simpc.web.dao.mapper.WorkExperienceDtoRowMapper;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Dao que define la manera en que se consultan los registros para los
 * cruces individuales
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Repository
public class IndividualIntersectionDaoImpl
    implements IndividualIntersectionDao {

    /**
     * Logger de la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(IndividualIntersectionDaoImpl.class);
    /**
     * Plantilla Jdbc para operar la base de datos.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Consulta para encontrar registros de la tabla MD_F_PILA
     * filtrando por tipo y n&uacute;mero de documento.
     */
    private static final String FIND_PILA = "SELECT * FROM MD_F_PILA p " +
        "INNER JOIN md_f_datos_basicos b ON p.TIPODOCUMENTO = b.tipo_documento " +
        "AND p.NUMERODOCUMENTO = b.numero_documento WHERE p.TIPODOCUMENTO = ? " +
        "and p.NUMERODOCUMENTO = ?";
    /**
     * Consulta para encontrar registros de la tabla MD_F_DEPENDIENTES
     * filtrando por tipo y n&uacute;mero de documento.
     */
    private static final String FIND_DEPENDENTS = "SELECT * FROM " +
        " MD_F_DEPENDIENTES d INNER JOIN md_f_datos_basicos b ON " +
        "d.TIPO_IDENTIFICACION = b.tipo_documento AND d.NUMERO_IDENTIFICACION = " +
        "b.numero_documento WHERE d.TIPO_IDENTIFICACION = ? AND " +
        "d.NUMERO_IDENTIFICACION = ?";
    /**
     * Consulta para encontrar registros de la tabla MD_F_CAP_PROGRAMA_MODULO y
     * MD_F_ORI_PROGRAMA_MODULO filtrando por tipo y n&uacute;mero de documento.
     */
    private static final String FIND_ORIENTATION = "SELECT CAP.CARGA_ID, " +
        "CAP.CODIGO_PROGRAMA ,CAP.NOMBRE_PROGRAMA ,CAP.TIPO_PROGRAMA ," +
        "CAP.TIPO_CURSO ,CAP.CODIGO_CCF  ,CAP.CODIGO_INST ,CAP.CODIGO_SEDE ," +
        "CAP.CODIGO_DEPARTAMENTO ,CAP.CODIGO_MUNICIPIO ,CAP.CERTIFICADA ," +
        "CAP.NOMBRE_CERTIFICACION ,CAP.OTRO_NOMBRE_CERT ,CAP.CERTIFICACION_EMITIDA ," + 
        "CAP.CINE, CAP.NECESIDAD,CAP.CODIGO_MODULO," +
        "CAP.NOMBRE_MODULO ,CAP.DURACION ,CAP.TIPO_HORARIO ," +
        "CAP.TIPO_COSTO_MATRICULA ,CAP.COSTO_MATRICULA,CAP.TIPO_OTROS_COSTOS," +
        "CAP.OTROS_COSTOS ,CAP.FECHA_INICIO ,CAP.FECHA_FIN, 'N/A' AS " +
        "AREA_DESEMPENO FROM MD_F_CAP_PROGRAMA_MODULO CAP INNER JOIN " +
        "MD_F_PROGRAMA_DETALLE DETA ON (CAP.CODIGO_MODULO=DETA.CODIGO_MODULO) " +
        "INNER JOIN md_f_datos_basicos b ON DETA.tipo_documento = " +
        "b.tipo_documento AND DETA.documento = b.numero_documento " +
        "WHERE  DETA.TIPO_DOCUMENTO = ? AND DETA.DOCUMENTO= ? UNION " +
        "SELECT ORI.CARGA_ID, ORI.CODIGO_PROGRAMA ,ORI.NOMBRE_PROGRAMA ," +
        "ORI.TIPO_PROGRAMA ,ORI.TIPO_CURSO ,ORI.CODIGO_CCF  ,ORI.INSTITUCION" +
        " AS CODIGO_INST , 'N/A' AS CODIGO_SEDE ,ORI.CODIGO_DEPARTAMENTO ," +
        "ORI.CODIGO_MUNICIPIO ,'N/A' AS CERTIFICADA ,'N/A' AS " +
        "NOMBRE_CERTIFICACION ,'N/A' AS OTRO_NOMBRE_CERT ," +
        "'N/A' AS CERTIFICACION_EMITIDA ,'N/A' AS CINE ,'N/A' AS NECESIDAD ," +
        "ORI.CODIGO_MODULO ,ORI.NOMBRE_MODULO ,ORI.DURACION,ORI.TIPO_HORARIO," +
        "'N/A' AS TIPO_COSTO_MATRICULA ,'N/A' AS COSTO_MATRICULA ,'N/A' AS" +
        " TIPO_OTROS_COSTOS ,'N/A' AS OTROS_COSTOS ,ORI.FECHA_INICIO ," +
        "ORI.FECHA_FIN, ORI. AREA_DESEMPENO FROM MD_F_ORI_PROGRAMA_MODULO ORI" +
        " INNER JOIN MD_F_PROGRAMA_DETALLE DETA2 ON (ORI.CODIGO_MODULO=" +
        "DETA2.CODIGO_MODULO) INNER JOIN md_f_datos_basicos b ON " +
        "DETA2.tipo_documento = b.tipo_documento AND " +
        "DETA2.documento = b.numero_documento WHERE DETA2.TIPO_DOCUMENTO = ? AND " +
        "DETA2.DOCUMENTO= ?";
    /**
     * Consulta para encontrar registros de la tabla MD_F_CAP_PROGRAMA_MODULO y
     * MD_F_ORI_PROGRAMA_MODULO.
     */
    private static final String FIND_PROGRAMAS_IN_MDF = "SELECT CAP.CARGA_ID," +
        " CAP.CODIGO_PROGRAMA ,CAP.NOMBRE_PROGRAMA ,CAP.TIPO_PROGRAMA, " +
        "CAP.TIPO_CURSO, CAP.CODIGO_CCF, CAP.CODIGO_INST, CAP.CODIGO_SEDE, " +
        "CAP.CODIGO_DEPARTAMENTO, CAP.CODIGO_MUNICIPIO, CAP.CERTIFICADA, " +
        "CAP.NOMBRE_CERTIFICACION, CAP.OTRO_NOMBRE_CERT, CAP.CERTIFICACION_EMITIDA, " +
        "CAP.CINE, CAP.NECESIDAD, CAP.CODIGO_MODULO," +
        "CAP.NOMBRE_MODULO, CAP.DURACION, CAP.TIPO_HORARIO, " +
        "CAP.TIPO_COSTO_MATRICULA, CAP.COSTO_MATRICULA, CAP.TIPO_OTROS_COSTOS, " +
        "CAP.OTROS_COSTOS, CAP.FECHA_INICIO, CAP.FECHA_FIN, 'N/A' AS " +
        "AREA_DESEMPENO FROM MD_F_CAP_PROGRAMA_MODULO CAP WHERE EN_DOMINIO = -1";
    /**
     * Consulta para encontrar registros de la tabla MD_F_PILA
     * filtrando por tipo y n&uacute;mero de documento para usuarios
     * de servicio publico de empleo.
     */
    private static final String FIND_PILA_EMPLOYMENT_SERVICE = "SELECT " +
        "ID_CARGA, TIPODOCUMENTO, NUMERODOCUMENTO, TIPOIDAPORTANTEU, " +
        "NUMIDAPORTANTEU, NOMBREAPORTANTEU, MAXFECHAU, PRIMERPERIODOU " +
        "FROM MD_F_PILA p INNER JOIN md_f_datos_basicos b ON p.TIPODOCUMENTO = " +
        "b.tipo_documento AND p.NUMERODOCUMENTO = b.numero_documento " +
        "WHERE p.TIPODOCUMENTO = ? and p.NUMERODOCUMENTO = ?";
    /**
     * Consulta para encontrar registros de la tabla MD_F_FOSFEC
     * filtrando por tipo y n&uacute;mero de documento.
     */
    private static final String FIND_FOSFEC = "SELECT * FROM MD_F_FOSFEC f " +
        "INNER JOIN md_f_datos_basicos b ON f.TIPO_IDENTIFICACION = " +
        "b.tipo_documento AND f.NUMERO_IDENTIFICACION = b.numero_documento " +
        "WHERE f.TIPO_IDENTIFICACION = ? and f.NUMERO_IDENTIFICACION  = ?";
    /**
     * Consulta para encontrar registros de la tabla MD_F_GESTION_INTERMEDIACION
     * filtrando por tipo y n&uacute;mero de documento.
     */
    private static final String FIND_INTERMEDIATION = "SELECT * FROM " +
        "MD_F_GESTION_INTERMEDIACION i INNER JOIN md_f_datos_basicos b ON " +
        "i.TIPO_DOCUMENTO = b.tipo_documento AND i.NUMERO_DOCUMENTO = " +
        "b.numero_documento WHERE i.TIPO_DOCUMENTO = ? and i.NUMERO_DOCUMENTO = ?";
    /**
     * Consulta para encontrar registros de la tabla MD_F_DATOS_BASICOS
     * filtrando por tipo y n&uacute;mero de documento.
     */
    private static final String FIND_BASIC_INFORMATION = "SELECT * FROM " +
        "MD_F_DATOS_BASICOS WHERE TIPO_DOCUMENTO = ? and NUMERO_DOCUMENTO = ?";
    /**
     * Consulta para encontrar registros de la tabla MD_F_NIVEL_EDUCATIVO
     * filtrando por tipo y n&uacute;mero de documento.
     */
    private static final String FIND_EDUCATION_LEVEL = "SELECT * FROM " +
        "MD_F_NIVEL_EDUCATIVO n INNER JOIN md_f_datos_basicos b ON " +
        "n.TIPO_DOCUMENTO = b.tipo_documento AND n.NUMERO_DOCUMENTO = " +
        "b.numero_documento WHERE n.TIPO_DOCUMENTO = ? and n.NUMERO_DOCUMENTO = ?";
    /**
     * Consulta para encontrar registros de la tabla MD_F_EXPERIENCIA_LABORAL
     * filtrando por tipo y n&uacute;mero de documento.
     */
    private static final String FIND_WORK_EXPERIENCE = "SELECT * FROM " +
        "MD_F_EXPERIENCIA_LABORAL l INNER JOIN md_f_datos_basicos b ON " +
        "l.TIPO_DOCUMENTO = b.tipo_documento AND l.NUMERO_DOCUMENTO = " +
        "b.numero_documento WHERE l.TIPO_DOCUMENTO = ? and l.NUMERO_DOCUMENTO = ?";
    /**
     * Consulta para encontrar registros de la tabla MD_F_EDUCACION_INFORMAL
     * filtrando por tipo y n&uacute;mero de documento.
     */
    private static final String FIND_INFORMAL_EDUCATION = "SELECT * FROM " +
        "MD_F_EDUCACION_INFORMAL e INNER JOIN md_f_datos_basicos b ON " +
        "e.TIPO_DOCUMENTO = b.tipo_documento AND e.NUMERO_DOCUMENTO = " +
        "b.numero_documento WHERE e.TIPO_DOCUMENTO = ? and e.NUMERO_DOCUMENTO = ?";
    /**
     * Consulta para encontrar registros de la tabla MD_F_IDIOMAS
     * filtrando por tipo y n&uacute;mero de documento.
     */
    private static final String FIND_LANGUAGES = "SELECT i.* FROM " +
        "MD_F_IDIOMAS i INNER JOIN md_f_datos_basicos b ON i.TIPO_DOCUMENTO = " +
        "b.tipo_documento AND i.NUMERO_DOCUMENTO = b.numero_documento " +
        "WHERE i.TIPO_DOCUMENTO = ? and i.NUMERO_DOCUMENTO = ? ";
    /**
     * Consulta para encontrar registros de la tabla MD_F_OTROS_CONOCIMIENTOS
     * filtrando por tipo y n&uacute;mero de documento.
     */
    private static final String FIND_OTHER_KNOWLEDGES = "SELECT * FROM " +
        "MD_F_OTROS_CONOCIMIENTOS o INNER JOIN md_f_datos_basicos b ON " +
        "o.TIPO_DOCUMENTO = b.tipo_documento AND o.NUMERO_DOCUMENTO = " +
        "b.numero_documento WHERE o.TIPO_DOCUMENTO = ? and o.NUMERO_DOCUMENTO = ?";
    /**
     * Actualiza el estado del programa indicando si ya esta en el dominio o no;
     * 0: No esta en el dominio, 1: Esta en el dominio, -1: En transición.
     */
    private static final String UPDATE_IN_DOMIN_COLUMN = "UPDATE " +
        "MD_F_CAP_PROGRAMA_MODULO SET EN_DOMINIO = ? WHERE EN_DOMINIO = ?";

    @Override
    public List<PilaDto> findPilaByDocumentAndNumberType(String documentType,
        String identificationNumber) {
        LOGGER.info("Consultando información de PILA.");

        return this.jdbcTemplate.query(FIND_PILA, new PilaDtoRowMapper(),
            documentType, identificationNumber);
    }

    @Override
    public List<DependentDto> findDependentsByDocumentAndNumberType(
        String documentType, String numberIdentification) {
        LOGGER.info("Consultando información de DEPENDIENTES.");

        return this.jdbcTemplate.query(FIND_DEPENDENTS,
            new DependentDtoRowMapper(), documentType, numberIdentification);
    }

    @Override
    public List<ProgramDto> findTrainingsByDocumentAndNumberType(
        String documentType, String numberIdentification) {
        LOGGER.info("Consultando información de orientación  y capacitación.");

        return this.jdbcTemplate.query(FIND_ORIENTATION,
            new ProgramDtoRowMapper(), documentType, numberIdentification,
            documentType, numberIdentification);
    }

    @Override
    public List<ProgramDto> findTrainings() {
        LOGGER.info("Consultando información de orientación  y capacitación.");

        return this.jdbcTemplate.query(FIND_PROGRAMAS_IN_MDF,
            new ProgramDtoRowMapper());
    }

    @Override
    public List<PilaEmploymentServiceDto>
        findPilaByDocumentAndNumberTypeEmploymentService(String documentType,
            String numberIdentification) {
        LOGGER.info("Consultando información de PILA para servicio publico" +
            "de empleo");
        return this.jdbcTemplate.query(FIND_PILA_EMPLOYMENT_SERVICE,
            new PilaEmploymentServiceDtoRowMapper(), documentType,
            numberIdentification);
    }

    @Override
    public List<ProfitDto> findFosfecByDocumentAndNumberType(String documentType,
        String numberIdentification) {
        LOGGER.info("Consultando información de FOSFEC.");

        return this.jdbcTemplate.query(FIND_FOSFEC, new ProfitDtoRowMapper(),
            documentType, numberIdentification);
    }

    @Override
    public List<IntermediationDto> findIntermediationByDocumentAndNumberType(
        String documentType, String identificationNumber) {
        LOGGER.info("Consultando información de intermedicacion.");

        return this.jdbcTemplate.query(FIND_INTERMEDIATION,
            new IntermediationDtoRowMapper(), documentType, identificationNumber);
    }

    @Override
    public List<BasicInformationDto> findBasicInformationByDocumentAndNumberType(
        String documentType, String identificationNumber) {
        LOGGER.info("Consultando información básica.");
        LOGGER.info("Tipo documento: " + documentType);
        LOGGER.info("Numero documento: " + identificationNumber);

        List<BasicInformationDto> list = this.jdbcTemplate.
            query(FIND_BASIC_INFORMATION, new BasicInformationDtoRowMapper(),
                documentType, identificationNumber);

        LOGGER.info("INFORMACION BASICA: " + list);

        return list;
    }

    @Override
    public List<EducationLevelDto> findEducationLevelByDocumentAndNumberType(
        String documentType, String identificationNumber) {
        LOGGER.info("Consultando información de niveles educativos.");

        return this.jdbcTemplate.query(FIND_EDUCATION_LEVEL,
            new EducationLevelDtoRowMapper(), documentType,
            identificationNumber);

    }

    @Override
    public List<WorkExperienceDto> findWorkExperienceByDocumentAndNumberType(
        String documentType, String identificationNumber) {
        LOGGER.info("Consultando información de experiencia laboral.");

        return this.jdbcTemplate.query(FIND_WORK_EXPERIENCE,
            new WorkExperienceDtoRowMapper(), documentType,
            identificationNumber);
    }

    @Override
    public List<InformalEducationDto> findInformalEducationByDocumentAndNumberType(
        String documentType, String identificationNumber) {
        LOGGER.info("Consultando información de educacion informal.");

        return this.jdbcTemplate.query(FIND_INFORMAL_EDUCATION,
            new InformalEducationDtoRowMapper(), documentType,
            identificationNumber);
    }

    @Override
    public List<LanguageDto> findLanguageByDocumentAndNumberType(
        String documentType, String identificationNumber) {
        LOGGER.info("Consultando información de idiomas.");

        return this.jdbcTemplate.query(FIND_LANGUAGES,
            new LanguageDtoRowMapper(), documentType, identificationNumber);
    }

    @Override
    public List<OtherKnowledgeDto> findOtherKnowledgeByDocumentAndNumberType(
        String documentType, String identificationNumber) {
        LOGGER.info("Consultando información de otros conocimientos.");

        return this.jdbcTemplate.query(FIND_OTHER_KNOWLEDGES,
            new OtherKnowledgeDtoRowMapper(), documentType,
            identificationNumber);
    }

    @Override
    public void updateInDomainColumn(int from, int to) {
        this.jdbcTemplate.update(UPDATE_IN_DOMIN_COLUMN, to, from);
    }
}
