/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: basicInformation.java
 * Created on: 2016/12/27, 04:23:26 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Leonardo Rocha</a>
 */
public class InformalEducationDto {

    private String documentType;
    private String numberIdentification;
    private String trainingType;
    private String institution;
    private String status;
    private String certificationDate;
    private String programName;
    private String country;
    private String hours;
    private Long cargaId;


    /*
     * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public InformalEducationDto() {

    }

    public InformalEducationDto(String documentType, String numberIdentification,
        String trainingType, String institution,
        String status, String certificationDate, String programName,
        String country, String hours, Long cargaId) {

        this.documentType = documentType;
        this.numberIdentification = numberIdentification;
        this.trainingType = trainingType;
        this.institution = institution;
        this.status = status;
        this.certificationDate = certificationDate;
        this.programName = programName;
        this.country = country;
        this.hours = hours;
        this.cargaId = cargaId;
    }

    public Long getCargaId() {
        return cargaId;
    }

    public void setCargaId(Long cargaId) {
        this.cargaId = cargaId;
    }

    public String getTrainingType() {
        return trainingType;
    }

    public void setTrainingType(String trainingType) {
        this.trainingType = trainingType;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCertificationDate() {
        return certificationDate;
    }

    public void setCertificationDate(String certificationDate) {
        this.certificationDate = certificationDate;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getNumberIdentification() {
        return numberIdentification;
    }

    public void setNumberIdentification(String numberIdentification) {
        this.numberIdentification = numberIdentification;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Tipo de capacitación=").append(trainingType).
            append("~&~institución=").append(institution).
            append("~&~estado=").append(status).
            append("~&~Fecha de Certificación=").append(certificationDate).
            append("~&~Nombre del Programa=").append(programName).
            append("~&~País=").append(country).
            append("~&~Duración en horas =").append(hours);
        return stringBuilder.toString();
    }

}
