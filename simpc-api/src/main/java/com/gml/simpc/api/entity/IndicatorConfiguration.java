/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Indicator.java
 * Created on: 2016/12/12, 03:28:43 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entidad que define la estructura de los indicadores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Table(name = "IND_INDICADORES")
@Entity
public class IndicatorConfiguration implements Serializable {

    /**
     * ID del indicador.
     */
    @Id
    @Column(name = "ID", length = 10)
    private Long id;
    
     
    /**
     * Nombre del tipo de indicador.
     */
    @Column(name = "NOMBRE", length = 50)
    private String name;
    
    /**
     * Descripci&oacute;n.
     */
    @Column(name = "DESCRIPCION", length = 200)
    private String description;
    
    /**
     * Unidad de medida.
     */
    @Column(name = "UNIDAD_MEDIDA", length = 200)
    private String unitMeasurement;

    /**
     * Fuente de datos a la cual pertenece.
     */
    @ManyToOne
    @JoinColumn(name = "IND_FUENTE_ID")
    private IndicatorSource indicatorSource;

    /**
     * Descripci&oacute;n.
     */
    @Column(name = "TIPO_GRAFICO", length = 200)
    private String graphicType;
    
    /**
     * Fuente de datos a la cual pertenece.
     */
    @ManyToOne
    @JoinColumn(name = "IND_VARIABLE_X_ID")
    private IndicatorVariable xVar;
    
     /**
     * Fuente de datos a la cual pertenece.
     */
    @ManyToOne
    @JoinColumn(name = "IND_VARIABLE_SERIE_ID")
    private IndicatorVariable serieVar;
    
    /**
     * Fuente de datos a la cual pertenece.
     */
    @ManyToOne
    @JoinColumn(name = "IND_VARIABLE_Y_ID")
    private IndicatorVariable yVar;
    
    /**
     * Lista de valor que maneja el indicador seg�n su variable de b�squeda.
     */
    @Column(name = "IND_LISTA_VALOR")
    private String valueListInd;

    /**
     * Descripci&oacute;n.
     */
    @Column(name = "FUNCION", length = 200)
    private String function;
    
    
    /**
     * Variable para agregar mas conciones al indicador
     */
    @Column(name = "CONDICION", length = 2000)
    private String conditions;
    
    /**
     * Variable para agregar mas conciones al indicador
     */
     @ManyToOne
    @JoinColumn(name = "VAR_FUNCION")
    private IndicatorVariable varfuntion;
    
    
    
    /**
     * Constructor por defecto.
     */
    public IndicatorConfiguration() {
        this.id = null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getUnitMeasurement() {
        return unitMeasurement;
    }

    public void setUnitMeasurement(String unitMeasurement) {
        this.unitMeasurement = unitMeasurement;
    }

    public IndicatorSource getIndicatorSource() {
        return indicatorSource;
    }

    public void setIndicatorSource(IndicatorSource indicatorSource) {
        this.indicatorSource = indicatorSource;
    }

    public String getGraphicType() {
        return graphicType;
    }

    public void setGraphicType(String graphicType) {
        this.graphicType = graphicType;
    }

    public IndicatorVariable getxVar() {
        return xVar;
    }

    public void setxVar(IndicatorVariable xVar) {
        this.xVar = xVar;
    }

    public IndicatorVariable getSerieVar() {
        return serieVar;
    }

    public void setSerieVar(IndicatorVariable serieVar) {
        this.serieVar = serieVar;
    }

    public IndicatorVariable getyVar() {
        return yVar;
    }

    public void setyVar(IndicatorVariable yVar) {
        this.yVar = yVar;
    }
    
    public String getValueListInd() {
        return valueListInd;
    }

    public void setValueListInd(String valueListInd) {
        this.valueListInd = valueListInd;
    }

    public IndicatorVariable getVarfuntion() {
        return varfuntion;
    }

    public void setVarfuntion(IndicatorVariable varfuntion) {
        this.varfuntion = varfuntion;
    }

   

    public String getFunction() {
        return function;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    

    public void setFunction(String function) {
        this.function = function;
    }

    @Override
    public String toString() {
        return "Indicator{" + "id=" + id + ", name=" + name + ", description=" + description + ", indicatorSource=" + indicatorSource + ", graphicType=" + graphicType + ", xVar=" + xVar + ", serieVar=" + serieVar + ", yVar=" +
            yVar + ", function=" + function + ", condiciones=" + conditions + '}';
    }

    

   
}
