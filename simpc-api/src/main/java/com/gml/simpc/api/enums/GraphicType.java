/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: GraphicType.java
 * Created on: 2016/12/12, 04:26:39 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.enums;

/**
 * Tipo de gr&aacute;fico de indicadores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public enum GraphicType {

    L("L�neas"),
    C("Circulos"),
    B("Columnas");
    /**
     * Label del tipo de gr&aacute;fico.
     */
    private final String label;

    private GraphicType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
