/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: DesktopSession.java
 * Created on: 2017/03/07, 11:03:37 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entidad que maneja las sesiones de escritorio.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Table(name = "SESIONES_ESCITORIO")
@Entity
public class DesktopSession implements Serializable {

    /**
     * Id de la entidad.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator =
        "desktop_seq_gen")
    @SequenceGenerator(name = "desktop_seq_gen", sequenceName =
        "DESKTOP_SEQ", allocationSize = 1, initialValue= 1)
    private Long id;
    /**
     * Usuario en sesi&oacute;n.
     */
    @ManyToOne
    @JoinColumn(name = "USUARIO", nullable = false)
    private User user;

    /**
     * Token generado en la creaci&ocute;n.
     */
    @Column(name = "TOKEN")
    private String userToken;

    /**
     * Fecha de creaci&ocute;n del usuario.
     */
    @Column(name = "FECHA_SESION")
    private Date userDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public Date getUserDate() {
        return userDate;
    }

    public void setUserDate(Date userDate) {
        this.userDate = userDate;
    }
}
