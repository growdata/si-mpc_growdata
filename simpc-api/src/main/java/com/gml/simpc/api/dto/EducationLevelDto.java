/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: basicInformation.java
 * Created on: 2016/12/27, 04:23:26 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Leonardo Rocha</a>
 */
public class EducationLevelDto {

    private String documentType;
    private String numberIdentification;
    private String educationLevel;
    private String performanceArea;
    private String nucleusKnowledge;
    private String degreeObtained;
    private String degreeHomologated;
    private String degreeCountry;
    private String university;
    private String status;
    private String finishDate;
    private String professionalCard;
    private String professionalCardNumber;
    private String professionalCardExpeditionDate;
    private String observations;
    private String practice;
    private Long cargaId;


    /*
     * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public EducationLevelDto() {

    }

    public EducationLevelDto(String documentType, String numberIdentification,
        String educationLevel, String performanceArea,
        String nucleusKnowledge, String degreeObtained, String degreeHomologated,
        String degreeCountry, String university, String status,
        String finishDate, String professionalCard,
        String professionalCardNumber, String professionalCardExpeditionDate,
        String observations, String practice, Long cargaId) {
        this.documentType = documentType;
        this.numberIdentification = numberIdentification;
        this.educationLevel = educationLevel;
        this.performanceArea = performanceArea;
        this.nucleusKnowledge = nucleusKnowledge;
        this.degreeObtained = degreeObtained;
        this.degreeHomologated = degreeHomologated;
        this.degreeCountry = degreeCountry;
        this.university = university;
        this.status = status;
        this.finishDate = finishDate;
        this.professionalCard = professionalCard;
        this.professionalCardNumber = professionalCardNumber;
        this.professionalCardExpeditionDate = professionalCardExpeditionDate;
        this.observations = observations;
        this.practice = practice;
        this.cargaId = cargaId;
    }

    public Long getCargaId() {
        return cargaId;
    }

    public void setCargaId(Long cargaId) {
        this.cargaId = cargaId;
    }

    public String getEducationLevel() {
        return educationLevel;
    }

    public void setEducationLevel(String educationLevel) {
        this.educationLevel = educationLevel;
    }

    public String getPerformanceArea() {
        return performanceArea;
    }

    public void setPerformanceArea(String performanceArea) {
        this.performanceArea = performanceArea;
    }

    public String getNucleusKnowledge() {
        return nucleusKnowledge;
    }

    public void setNucleusKnowledge(String nucleusKnowledge) {
        this.nucleusKnowledge = nucleusKnowledge;
    }

    public String getDegreeObtained() {
        return degreeObtained;
    }

    public void setDegreeObtained(String degreeObtained) {
        this.degreeObtained = degreeObtained;
    }

    public String getDegreeHomologated() {
        return degreeHomologated;
    }

    public void setDegreeHomologated(String degreeHomologated) {
        this.degreeHomologated = degreeHomologated;
    }

    public String getDegreeCountry() {
        return degreeCountry;
    }

    public void setDegreeCountry(String degreeCountry) {
        this.degreeCountry = degreeCountry;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public String getProfessionalCard() {
        return professionalCard;
    }

    public void setProfessionalCard(String professionalCard) {
        this.professionalCard = professionalCard;
    }

    public String getProfessionalCardNumber() {
        return professionalCardNumber;
    }

    public void setProfessionalCardNumber(String professionalCardNumber) {
        this.professionalCardNumber = professionalCardNumber;
    }

    public String getProfessionalCardExpeditionDate() {
        return professionalCardExpeditionDate;
    }

    public void setProfessionalCardExpeditionDate(
        String professionalCardExpeditionDate) {
        this.professionalCardExpeditionDate = professionalCardExpeditionDate;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public String getPractice() {
        return practice;
    }

    public void setPractice(String practice) {
        this.practice = practice;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getNumberIdentification() {
        return numberIdentification;
    }

    public void setNumberIdentification(String numberIdentification) {
        this.numberIdentification = numberIdentification;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.
            append("Nivel Educativo=").append(educationLevel).
            append("~&~�rea de Desempe�o=").append(performanceArea).
            append("~&~N�cleo Del conocimiento=").append(nucleusKnowledge).
            append("~&~T�tulo obtenido=").append(degreeObtained).
            append("~&~T�tulo Homologado=").append(degreeHomologated).
            append("~&~Pa�s Del T�tulo=").append(degreeCountry).
            append("~&~Instituci�n=").append(university).
            append("~&~Estado=").append(status).
            append("~&~Fecha de finalizaci�n=").append(finishDate).
            append("~&~Tarjeta Profesional=").append(professionalCard).
            append("~&~N�mero de tarjeta profesional=").append(professionalCardNumber).
            append("~&~Fecha de expedici�n de tarjet profesinal=").append(professionalCardExpeditionDate).
            append("~&~Observaciones=").append(observations).
            append("~&~Interesado en pr�cticas empresariales=").append(practice);

        return stringBuilder.toString();
    }

}
