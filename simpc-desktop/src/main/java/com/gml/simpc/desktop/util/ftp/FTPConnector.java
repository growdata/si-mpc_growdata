/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: FTPConnector.java
 * Created on: 2017/04/27, 03:28:37 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.desktop.util.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

/**
 * Clase encargada de establecer conexi&oacute;n y ejecutar comandos FTP.
 *
 * @author Manuel Mart�nez
 */
public class FTPConnector {

    /**
     * Sesi&oacute;n FTP establecida.
     */
    private FTPClient session;

    /**
     * Establece una conexi&oacute;n FTP.
     *
     * @param username Nombre de usuario.
     * @param password Contrase&ntilde;a.
     * @param host Host a conectar.
     * @param port Puerto del Host.
     *
     * @throws IllegalAccessException
     * @throws IOException
     */
    public void connect(String username, String password, String host, int port)
            throws IllegalAccessException, IOException {
        if (this.session == null || !this.session.isConnected()) {
            this.session = new FTPClient();

            this.session.connect(host, port);
            this.session.login(username, password);
            this.session.enterLocalPassiveMode();
            this.session.setFileType(FTP.BINARY_FILE_TYPE);
        } else {
            throw new IllegalAccessException("Sesion FTP ya iniciada.");
        }
    }

    /**
     * A&ntilde;ade un archivo al directorio FTP usando el protocolo SFTP.
     *
     * @param ftpPath Path del FTP donde se agregar&aacute; el archivo.
     * @param filePath Directorio donde se encuentra el archivo a subir en
     * disco.
     * @param fileName Nombre que tendra el archivo en el destino.
     * @param file Archivo para carga.
     *
     * @throws IllegalAccessException
     * @throws IOException
     */
    public final void addFile(String ftpPath, String filePath,
            String fileName, File file) throws IllegalAccessException, IOException {
        if (this.session != null && this.session.isConnected()) {

            try (InputStream inputStream = new FileInputStream(file)) {
                this.session.makeDirectory(ftpPath);
                this.session.storeFile(ftpPath + fileName, inputStream);
            }
        } else {
            throw new IllegalAccessException("No existe sesion FTP iniciada.");
        }
    }

    /**
     * Cierra la sesi&oacute;n FTP.
     *
     * @param pathname
     * @throws java.io.IOException
     */
    public void makeDirectory(String pathname) throws IOException {
        String[] parts = pathname.split("/");
        String dir = "";
        for (String d : parts) {
            dir = dir + "/" + d;
            this.session.makeDirectory(dir);
        }
    }

    /**
     * Cierra la sesi&oacute;n FTP.
     *
     * @throws java.io.IOException
     */
    public final void disconnect() throws IOException {
        this.session.disconnect();
    }
}
