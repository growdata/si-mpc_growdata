/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CommonsValidator.java
 * Created on: 2016/12/11, 12:33:45 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 * Objetive: Test program massive upload: training detail records
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.batch;

/**
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import com.gml.simpc.loader.thread.ThreadLocalValueList;
import com.gml.simpc.loader.utilities.ValueListTestService;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-batch.xml", 
    "classpath:batch_jobs/program/training/training-attendance-job.xml" })
public class TrainingAttendanceLoadJobTest {
    
    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER = 
        Logger.getLogger(TrainingAttendanceLoadJobTest.class);
    
    /**
     * Servicio para ejecutar jobs en modo pruebas
    */
    @Autowired
    JobLauncherTestUtils jobLauncherTestUtils;
    
    /*
     * Case for validate invalid programCode field
     */
    @Test
    public void moduleCodeErrorCaseJob() throws Exception {
        
        LOGGER.info("Running TrainingAttendanceLoadJobTest - " +
            "moduleCodeErrorCaseJob");

        //Inicializamos local thread
        ThreadLocalJob.init();
 
        //Inicializamos listas de valores
        ValueListTestService valService = new ValueListTestService();
        valService.updateAll();
        ThreadLocalValueList.set(valService.getValueList());
        
        // Inicializamos el job.
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath", 
            "file:loadTestFiles/program/training/attendance/module-code-error.txt"); 
        
        JobExecution jobExecution = jobLauncherTestUtils.
            launchJob(jobParametersBuilder.toJobParameters());
        
        for (Throwable t : jobExecution.getAllFailureExceptions()) {
            t.printStackTrace();
        }
        
        JobResultDto jobResult = ThreadLocalJob.getResult();
        LOGGER.info(jobResult.toString());
        
        if (jobResult.getErrorList().size()!= 0){
            LOGGER.info(jobResult.getErrorsString());
            
        }
        
        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().getExitCode());
        Assert.assertTrue(jobResult.getErrorList().size()== 1);
        
        ThreadLocalJob.delete();
    }
    
    /*
     * Case for validate invalid endDate field
     */
    @Test
    public void documentErrorCaseJob() throws Exception {
        
        LOGGER.info("Running TrainingAttendanceLoadJobTest - " +
            "documentErrorCaseJob");

        //Inicializamos local thread
        ThreadLocalJob.init();
 
        //Inicializamos listas de valores
        ValueListTestService valService = new ValueListTestService();
        valService.updateAll();
        ThreadLocalValueList.set(valService.getValueList());
        
        // Inicializamos el job.
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath", 
            "file:loadTestFiles/program/training/attendance/document-error.txt"); 
        
        JobExecution jobExecution = jobLauncherTestUtils.
            launchJob(jobParametersBuilder.toJobParameters());
        
        for (Throwable t : jobExecution.getAllFailureExceptions()) {
            t.printStackTrace();
        }
        
        JobResultDto jobResult = ThreadLocalJob.getResult();
        LOGGER.info(jobResult.toString());
        
        if (jobResult.getErrorList().size()!= 0){
            LOGGER.info(jobResult.getErrorsString());
            System.out.println(jobResult.getErrorsString());
        }
        
        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().getExitCode());
        Assert.assertTrue(jobResult.getErrorList().size()== 2);
        
        ThreadLocalJob.delete();
    }
    
    /*
     * Case for validate invalid endDate field
     */
    @Test
    public void documentTypeErrorCaseJob() throws Exception {
        
        LOGGER.info("Running TrainingAttendanceLoadJobTest - " +
            "documentTypeErrorCaseJob");

        //Inicializamos local thread
        ThreadLocalJob.init();
        
        //Inicializamos listas de valores
        ValueListTestService valService = new ValueListTestService();
        valService.updateAll();
        ThreadLocalValueList.set(valService.getValueList());
 
        // Inicializamos el job.
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath", 
            "file:loadTestFiles/program/training/attendance/document-type-error.txt"); 
        
        JobExecution jobExecution = jobLauncherTestUtils.
            launchJob(jobParametersBuilder.toJobParameters());
        
        for (Throwable t : jobExecution.getAllFailureExceptions()) {
            t.printStackTrace();
        }
        
        JobResultDto jobResult = ThreadLocalJob.getResult();
        LOGGER.info("Resultado del job "+jobResult.toString());
        
        if (jobResult.getErrorList().size()!= 0){
            LOGGER.info(jobResult.getErrorsString());
        }
        
        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().getExitCode());
        Assert.assertTrue(jobResult.getErrorList().size()== 2);
        
        ThreadLocalJob.delete();
    }
    
    /*
     * Case for validate success fields
     */
    @Test
    public void successCaseJob() throws Exception {
        
        LOGGER.info("Running TrainingAttendanceLoadJobTest - successCaseJob");

        //Inicializamos local thread
        ThreadLocalJob.init();
        
        //Inicializamos listas de valores
        ValueListTestService valService = new ValueListTestService();
        valService.updateAll();
        ThreadLocalValueList.set(valService.getValueList());
 
        // Inicializamos el job.
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath", 
            "file:loadTestFiles/program/training/attendance/success.txt"); 
        
        JobExecution jobExecution = jobLauncherTestUtils.
            launchJob(jobParametersBuilder.toJobParameters());
        
        for (Throwable t : jobExecution.getAllFailureExceptions()) {
            t.printStackTrace();
        }
        
        JobResultDto jobResult = ThreadLocalJob.getResult();
        LOGGER.info(jobResult.toString());
        
        if (jobResult.getErrorList().size()!= 0){
            LOGGER.info(jobResult.getErrorsString());
        }
        
        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().getExitCode());
        Assert.assertTrue(jobResult.getErrorList().size()== 0);
        
        ThreadLocalJob.delete();
    }
}
