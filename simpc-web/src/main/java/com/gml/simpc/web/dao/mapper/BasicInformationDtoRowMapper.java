/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: BasicInformationDtoRowMapper.java
 * Created on: 2017/02/13, 11:43:35 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.dao.mapper;

import com.gml.simpc.api.dto.BasicInformationDto;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 * Row Mapper para informaci&oacute;n b&aacute;sica.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class BasicInformationDtoRowMapper
    implements RowMapper<BasicInformationDto> {

    @Override
    public BasicInformationDto mapRow(ResultSet rs, int i) throws SQLException {
        BasicInformationDto dto = new BasicInformationDto();
        dto.setCargaId(rs.getLong("CARGA_ID"));
        dto.setCcfCode(rs.getString("CODIGO_CCF"));
        dto.setAgencyCode(rs.getString("AGENCIA_GESTION_Y_COLOCACION"));
        dto.setRegisterDate(rs.getString("FECHA_REGISTRO"));
        dto.setLastModificationDate(rs.getString("FECHA_ULTIMA_ACTUALIZACION"));
        dto.setDocumentType(rs.getString("TIPO_DOCUMENTO"));
        dto.setNumberIdentification(rs.getString("NUMERO_DOCUMENTO"));
        dto.setEmail(rs.getString("CORREO_ELECTRONICO"));
        dto.setFirstName(rs.getString("PRIMER_NOMBRE"));
        dto.setSecondName(rs.getString("SEGUNDO_NOMBRE"));
        dto.setFirstSurName(rs.getString("PRIMER_APELLIDO"));
        dto.setSecondSurName(rs.getString("SEGUNDO_APELLIDO"));
        dto.setBirthDate(rs.getString("FECHA_NACIMIENTO"));
        dto.setGender(rs.getString("SEXO"));
        dto.setMilitaryCard(rs.getString("LIBRETA_MILITAR"));
        dto.setMilitaryCardType(rs.getString("TIPO_LIBRETA"));
        dto.setMilitaryCardNumber(rs.getString("NUMERO_LIBRETA"));
        dto.setPhoneContact(rs.getString("TELEFONO_CONTACTO"));
        dto.setCivilStatus(rs.getString("ESTADO_CIVIL"));
        dto.setCountry(rs.getString("PAIS_NACIMIENTO"));
        dto.setNationality(rs.getString("NACIONALIDAD"));
        dto.setStateDivipola(rs.getString("DEPARTAMENTO_NACIMIENTO"));
        dto.setCityDivipola(rs.getString("MUNICIPIO_NACIMIENTO"));
        dto.setHouseBoss(rs.getString("JEFE_HOGAR"));
        dto.setTargetPopulation(rs.getString("POBLACION_FOCALIZADA"));
        dto.setEthnicGroup(rs.getString("GRUPO_ETNICO"));
        dto.setPopulationType(rs.getString("TIPO_POBLACION"));
        dto.setDisability(rs.getString("CONDICIONES_DISCAPACIDAD"));
        dto.setDisabilityType(rs.getString("TIPO_DISCAPACIDAD"));
        dto.setResidenceCountry(rs.getString("PAIS_RESIDENCIA"));
        dto.setResidenceState(rs.getString("DEPARTAMENTO_RESIDENCIA"));
        dto.setResidenceCity(rs.getString("MUNICIPIO_RESIDENCIA"));
        dto.setAddres(rs.getString("DIRECCION_RESIDENCIA"));
        dto.setNeighborhood(rs.getString("BARRIO_RESIDENCIA"));
        dto.setOtherPhone(rs.getString("OTRO_TELEFONO"));
        dto.setObservations(rs.getString("OBSERVACIONES"));
        dto.setWageAspiration(rs.getString("ASPIRACION_SALARIAL"));
        dto.setTravelPossibility(rs.getString("POSIBILIDAD_VIAJAR"));
        dto.setMovePossibility(rs.getString("POSIBILIDAD_TRASLADARSE"));
        dto.setDistanceJobOffersInterest(rs.
            getString("INTERES_OFERTAS_TELETRABAJO"));
        dto.setEmploymentStatus(rs.getString("SITUACION_LABORAL_ACTUAL"));
        dto.setVehicleOwner(rs.getString("PROPIETARIO_MEDIO_TRANSPORTE"));
        dto.setDrivingLicenseCar(rs.getString("TIENE_LICENCIA_CARRO"));
        dto.setCategoryLicenseCar(rs.getString("CATEGORIA_LICENCIA_CARRO"));
        dto.setDrivingLicenseMotorcycle(rs.getString("TIENE_LICENCIA_MOTO"));
        dto.setCategoryLicenseMotorcycle(rs.
            getString("CATEGORIA_LICENCIA_MOTO"));
        
        return dto;
    }
}
