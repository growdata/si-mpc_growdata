/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IndicatorRepository.java
 * Created on: 2016/12/16, 10:26:35 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository;

import com.gml.simpc.api.entity.IndicatorVariable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Repositorio de indicadores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface IndicatorVariableRepository extends JpaRepository<IndicatorVariable, Long> {

    @Query(value =
        "select * from IND_VARIABLES v  " +
        " where IND_FUENTE_ID = ?1 AND  Campo = ?2 ", nativeQuery = true)
    public IndicatorVariable findByIndicatorIdAndField(Long id, String field);
    

}
