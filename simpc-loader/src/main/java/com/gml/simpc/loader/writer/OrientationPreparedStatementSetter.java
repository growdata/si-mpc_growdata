/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: OrientationPreparedStatementSetter.java
 * Created on: 2017/01/20, 12:16:58 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 * Objetive: se prepara el statement para cada campo para las tablas de stage y master
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.writer;

import com.gml.simpc.api.dto.ProgramDto;
import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.batch.item.database.ItemPreparedStatementSetter;

/**
 * Transfiere la informacion de los Beneficios Economicos (Archivo Fosfec)
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
public class OrientationPreparedStatementSetter
    implements ItemPreparedStatementSetter<ProgramDto> {

    @Override
    public void setValues(ProgramDto programDto, PreparedStatement ps)
        throws SQLException {
        ThreadLocalJob.setProcesed();
        JobResultDto jobResult = ThreadLocalJob.getResult();
        int line = jobResult.getProcesedLines();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        ps.setString(1, programDto.getProgramName());
        ps.setString(2, programDto.getProgramCode());
        ps.setString(3, programDto.getModuleCode());
        ps.setString(4, programDto.getModuleName());
        ps.setString(5, programDto.getProgramType());
        ps.setString(6, programDto.getCcfCode());
        ps.setString(7, programDto.getInstCode());
        ps.setString(8, programDto.getStateCode());
        ps.setString(9, programDto.getCityCode());
        ps.setString(10, programDto.getPerformance());
        ps.setString(11, programDto.getCourseType());
        ps.setString(12, programDto.getTotalHours());
        ps.setString(13, programDto.getSchedule());
        try {
            Date startDate = dateFormat.parse(programDto.getStartDate());
            Date endDate = dateFormat.parse(programDto.getEndDate());
            ps.setDate(14, new java.sql.Date(startDate.getTime()));
            ps.setDate(15, new java.sql.Date(endDate.getTime()));
        } catch (ParseException ex) {
            Logger.getLogger(TrainingPreparedStatementSetter.class.getName()).
                log(Level.SEVERE, null, ex);
        }
        ps.setString(16, Integer.toString(line));

    }

}
