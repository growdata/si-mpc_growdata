/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IService.java
 * Created on: 2016/10/24, 11:31:09 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.entity.Parameter;
import com.gml.simpc.api.entity.exeption.UserException;
import java.util.List;

/**
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
public interface ParameterService {

    /**
     * Obtiene todas los perfiles oferentes creadas.
     *
     * @return
     */
    List<Parameter> getAll();

    /**
     * Metodo que consulta por nombre sin importar si es mayuscula o minuscula.
     *
     * @param name
     *
     * @return
     */
    List<Parameter> findBySpNameLikeIgnoreCase(String name);

    /**
     * Metodo que consulta por nombre.
     *
     * @param name
     *
     * @return
     */
    Parameter findByName(String name);
    
    /**
     * Consulta una instituci&oacute;n por ID.
     *
     * @param id
     *
     * @return
     */
    Parameter findById(Long id);
    /**
     * Servicio para guardar una instituci&oacute;n oferente.
     *
     * @param parameter
     */
    void save(Parameter parameter) throws UserException;

    /**
     * Servicio para actualizar instituciones oferentes.
     *
     * @param parameter
     */
    void update(Parameter parameter);
}
