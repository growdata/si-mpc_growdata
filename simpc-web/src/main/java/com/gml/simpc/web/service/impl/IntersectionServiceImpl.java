/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IntersectionServiceImpl.java
 * Created on: 2017/02/07, 04:12:19 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.entity.Intersection;
import com.gml.simpc.api.entity.Parameter;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.entity.exeption.UserException;
import com.gml.simpc.api.entity.exeption.code.ErrorCode;
import com.gml.simpc.api.enums.IntersectionType;
import com.gml.simpc.api.services.IntersectionService;
import com.gml.simpc.commons.annotations.AnnotationInterceptor;
import com.gml.simpc.commons.file.FileUtil;
import com.gml.simpc.web.batch.ApplicationContextAwareImp;
import com.gml.simpc.web.batch.IntersectionExecutor;
import com.gml.simpc.web.repository.IntersectionRepository;
import com.gml.simpc.web.repository.ParameterRepository;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.gml.simpc.api.enums.IntersectionStatus.CREATED;
import static com.gml.simpc.api.enums.IntersectionStatus.DONE;
import static com.gml.simpc.api.utilities.Util.INTERSECTION_FILES_PATH;

/**
 * Servicio para cruces.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Service
public class IntersectionServiceImpl implements IntersectionService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(IntersectionServiceImpl.class);
    /**
     * Servicio de par&aacute;metros.
     */
    @Autowired
    private ParameterRepository parameterRepository;
    /**
     * Repositorio de cruces.
     */
    @Autowired
    private IntersectionRepository intersectionRepository;
    /**
     * Componente para obtener din&aacute;micamente beans desde el contexto.
     */
    @Autowired
    private ApplicationContextAwareImp appContextAwareImp;

    /**
     * Inicializa el cruce de informaci&ocute;n.
     * 
     * @param user
     * @param file
     * @param originalName
     * @throws UserException 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Inicia Cruce Masivo",
        entityTableName = "INSTITUCIONES")
    @Override
    public void startIntersection(User user, byte[] file, String originalName)
        throws UserException {
        LOGGER.info("Ejecutando metodo [ startIntersection ]");

        try {
            String fileName = buildFileName(user);
            writeFile(fileName, file);

            boolean validFile = checkFile(fileName);
            if (validFile) {
                Intersection intersection = this.intersectionRepository.
                    saveAndFlush(buildIntersection(user, originalName));
                intersection.setLoadFile(fileName);

                IntersectionExecutor executor = this.appContextAwareImp.
                    getBean(IntersectionExecutor.class);

                executor.setIntersection(intersection);

                ExecutorService executorService =
                    Executors.newSingleThreadExecutor();
                executorService.execute(executor);
                executorService.shutdown();
            } else {
                throw new UserException(ErrorCode.ERR_057_FILE_MISSAHAPEN);
            }
        } catch (Exception ex) {
            LOGGER.error("Error", ex);

            throw new UserException(ErrorCode.ERR_057_FILE_MISSAHAPEN);

        }
    }

    /**
     * Guarda los cruces individuales.
     * 
     * @param user
     * @param path 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Inicia Cruce Individual",
        entityTableName = "CRUCES")
    @Override
    public void saveIndividualIntersection(User user, String path) {

        try {

            Intersection intersection = new Intersection();
            intersection.setStatus(DONE);
            intersection.setUser(user);
            intersection.setCreationDate(new Date());
            intersection.setIntersectionType(IntersectionType.I);
            intersection.setEndDate(new Date());
            intersection.setResultPath(path);
            intersection.setSuccessRecords(1);
            intersection.setTotalOfRecors(1);

            this.intersectionRepository.save(intersection);

        } catch (Exception ex) {
            LOGGER.error("Error", ex);

        }
    }

    /**
     * Busca cruces por id.
     * 
     * @param id
     * @return 
     */
    @Override
    public Intersection findById(Long id) {
        return this.intersectionRepository.findOne(id);
    }

    /**
     * Constuye el nombre a asignar al archivo que se esta cargando.
     * 
     * @param userSession
     * @return 
     */
    private String buildFileName(User userSession) {
        LOGGER.info("Ejecutando metodo [ buildFileName ]");

        Parameter path = this.parameterRepository.
            findByName(INTERSECTION_FILES_PATH);

        long time = Calendar.getInstance().getTimeInMillis();

        StringBuilder builder = new StringBuilder();
        builder.append(path.getValue());
        builder.append(time);
        builder.append(" - ");
        builder.append(userSession.getUsername());
        builder.append(".txt");

        return builder.toString();
    }

    /**
     * Valida que la estructura del archivo de cruce masivo sea el correcto.
     * 
     * @param nameFile
     * @return 
     */
    private boolean checkFile(String nameFile) {
        LOGGER.info("Ejecutando metodo [ buildFileName ]");

        boolean validFile = true;

        try {

            File f = new File(nameFile);
            BufferedReader b = new BufferedReader(new FileReader(f));
            String readLine = "";

            while ((readLine = b.readLine()) != null) {
                String[] split = readLine.split(",");
                if (split.length != 3) {
                    validFile = false;
                }
            }

        } catch (IOException e) {
            validFile = false;
            e.printStackTrace();
        }
        return validFile;
    }

    /**
     * Escribe el archivo en disco.
     *
     * @param fileName
     * @param file
     */
    private void writeFile(String fileName, byte[] file) throws IOException {
        LOGGER.info("Ejecutando metodo [ writeFile ]");

        FileUtil.writeFile(fileName, file);
    }

    /**
     * Construye el objeto para cruces.
     *
     * @param user
     * @param originalName
     *
     * @return
     */
    private Intersection buildIntersection(User user, String originalName) {
        Intersection intersection = new Intersection();

        intersection.setFileName(originalName);
        intersection.setStatus(CREATED);
        intersection.setUser(user);
        intersection.setCreationDate(new Date());
        intersection.setIntersectionType(IntersectionType.M);

        return intersection;
    }

    /**
     * Consulta los archivos generados en la fecha.
     *
     * @param startDate
     * @param endDate
     * @param searchType
     * @param ccf
     *
     * @return
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Cruce filtrando",
        entityTableName = "CRUCES")
    @Override
    public List<Intersection> findByEndDateRange(Date startDate, Date endDate,
        String searchType, String ccf) {
        return this.intersectionRepository.findByEndDateRange(startDate,
            endDate, searchType, ccf);
    }
}
