/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: InstitutionService.java
 * Created on: 2016/10/19, 02:10:35 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.entity.Headquarter;
import com.gml.simpc.api.entity.Institution;
import java.util.List;

/**
 * Interface que define el servicio de las sedes
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public interface HeadquarterService {

    /**
     * Obtiene todas las sedes
     *
     * @return
     */
    List<Headquarter> getAll();

    /**
     * Servicio para guardar una sede
     *
     * @param headquarter
     */
    void save(Headquarter headquarter);

    /**
     * Elimina una sede
     *
     * @param headquarter
     */
    void remove(Headquarter headquarter);

    /**
     * Elimina una lista de sedes
     *
     * @param headquarters
     */
    void remove(List<Headquarter> headquarters);

    /**
     * Encongtrar lista de sedes por instituci&oacute;n.
     *
     * @param institution
     *
     * @return
     */
    List<Headquarter> findByInstitution(Institution institution);

    /**
     * Metodo que consulta por filtros
     *
     * @param headquarter
     *
     * @return
     */
    List<Headquarter> findByFilters(Headquarter headquarter);

    /**
     * Consulta por Id.
     *
     * @param id
     *
     * @return
     */
    Headquarter findById(Long id);

    /**
     * Consulta una sede principal por instituci&oacute;n.
     *
     * @param institution
     *
     * @return
     */
    Headquarter findPrincipalByInstitution(Institution institution);
}
