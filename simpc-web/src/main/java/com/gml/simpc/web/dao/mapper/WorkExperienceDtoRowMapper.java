/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: WorkExperienceDtoRowMapper.java
 * Created on: 2017/02/13, 11:48:29 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.dao.mapper;

import com.gml.simpc.api.dto.WorkExperienceDto;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 * Row Mapper para experiencia laboral.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class WorkExperienceDtoRowMapper implements RowMapper<WorkExperienceDto> {

    @Override
    public WorkExperienceDto mapRow(ResultSet rs, int i) throws SQLException {
        WorkExperienceDto dto = new WorkExperienceDto();
        dto.setDocumentType(rs.getString("TIPO_DOCUMENTO"));
        dto.setNumberIdentification(rs.getString("NUMERO_DOCUMENTO"));
        dto.setWorkExperienceType(rs.getString("TIPO_EXPERIENCIA_LABORAL"));
        dto.setProduct(rs.getString("PRODUCTO_O_SERVICIO"));
        dto.setPeopleAmount(rs.getString("PERSONAS_TRABAJAN_CON_USTED"));
        dto.setCompanyName(rs.getString("NOMBRE_EMPRESA"));
        dto.setSector(rs.getString("SECTOR"));
        dto.setCompanyPhone(rs.getString("TELEFONO_EMPRESA"));
        dto.setCountry(rs.getString("PAIS"));
        dto.setPosition(rs.getString("CARGO"));
        dto.setEquivalentPosition(rs.getString("CARGO_EQUIVALENTE"));
        dto.setAdmisionDate(rs.getString("FECHA_INGRESO"));
        dto.setCurrentlyWorking(rs.getString("TRABAJA_ACTUALMENTE"));
        dto.setRetirementDate(rs.getString("FECHA_RETIRO"));
        dto.setCargaId(rs.getLong("CARGA_ID"));

        return dto;
    }
}
