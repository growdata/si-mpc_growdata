/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CommonsValidator.java
 * Created on: 2016/12/11, 12:33:45 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 * Objetive: Test program massive upload: dependent records
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.batch;

/**
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import com.gml.simpc.loader.thread.ThreadLocalValueList;
import com.gml.simpc.loader.utilities.ValueListTestService;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-batch.xml", 
    "classpath:batch_jobs/profit/dependent/dependent-job.xml" })
public class DependentLoadJobTest {
    
    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER = 
        Logger.getLogger(DependentLoadJobTest.class);
    
    /**
     * Servicio para ejecutar jobs en modo pruebas
    */
    @Autowired
    JobLauncherTestUtils jobLauncherTestUtils;

    /*
     * Case for validate invalid programCode field
     */
    @Test
    public void ErrorCaseJob() throws Exception {
        
        LOGGER.info("Running DependentLoadJobTest - ErrorCaseJob");

        //Inicializamos local thread
        ThreadLocalJob.init();
 
        //Inicializamos listas de valores
        ValueListTestService valService = new ValueListTestService();
        valService.updateAll();
        ThreadLocalValueList.set(valService.getValueList());
      
        // Inicializamos el job.
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath", 
            "file:loadTestFiles/profit/dependent/dependiente-Error.txt"); 
        
        // Lanzamos el job.
        JobExecution jobExecution = jobLauncherTestUtils.
            launchJob(jobParametersBuilder.toJobParameters());
        
        for (Throwable t : jobExecution.getAllFailureExceptions()) {
            t.printStackTrace();
        }
        
        JobResultDto jobResult = ThreadLocalJob.getResult();
        LOGGER.info(jobResult.toString());
        
        if (!jobResult.getErrorList().isEmpty()){
            LOGGER.info(jobResult.getErrorsString());
        }
        
        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().getExitCode());
        Assert.assertTrue(!jobResult.getErrorList().isEmpty());
        
        ThreadLocalJob.delete();
    }
    
    
    /*
     * Case for validate success fields
     */
    @Test
    public void successCaseJob() throws Exception {
        
        LOGGER.info("Running DependentLoadJobTest - successCaseJob");

       //Inicializamos local thread
        ThreadLocalJob.init();
 
        //Inicializamos listas de valores
        ValueListTestService valService = new ValueListTestService();
        valService.updateAll();
        ThreadLocalValueList.set(valService.getValueList());
       
        
        // Inicializamos el job.
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath", 
            "file:loadTestFiles/profit/dependent/dependiente-Exitoso.txt"); 
        
        JobExecution jobExecution = jobLauncherTestUtils.
            launchJob(jobParametersBuilder.toJobParameters());
        
        for (Throwable t : jobExecution.getAllFailureExceptions()) {
            t.printStackTrace();
        }
        
        JobResultDto jobResult = ThreadLocalJob.getResult();
        LOGGER.info(jobResult.toString());
        
        if (!jobResult.getErrorList().isEmpty()){
            LOGGER.info(jobResult.getErrorsString());
            System.out.println(jobResult.getErrorsString());
        }
        
        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().getExitCode());
        Assert.assertTrue(jobResult.getErrorList().isEmpty());
        
        ThreadLocalJob.delete();
    }

 }
