/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Institution.java
 * Created on: 2016/11/19, 11:47:45 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import com.gml.simpc.api.utilities.Util;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Entidad que representa la instituci&oacute;n.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Table(name = "IND_VARIABLES")
@Entity
public class IndicatorVariable implements Serializable {

    /**
     * ID.
     */
    @Id
    @Column(name = "ID", length = 10)
    private Long id;

    /**
     * Fuente de datos a la cual pertenece.
     */
    @ManyToOne
    @JoinColumn(name = "IND_FUENTE_ID")
    private IndicatorSource indicatorSource;

    /**
     * Campo
     */
    @Column(name = "CAMPO")
    private String field;

    /**
     * Tipo
     */
    @Column(name = "TIPO")
    private String type;

    /**
     * Descripcion
     */
    @Column(name = "DESCRIPCION")
    private String description;

    /**
     * Tipo
     */
    @Column(name = "DETALLE")
    private String detail;

    /**
     * Transiente para filtros
     */
    @Transient
    private String filter;

    public void setFilter(String filter) {
        this.filter = filter;
    }

    /**
     * Constructor por defecto.
     */
    public IndicatorVariable() {
        this.id = null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public IndicatorSource getIndicatorSource() {
        return indicatorSource;
    }

    public void setIndicatorSource(IndicatorSource indicatorSource) {
        this.indicatorSource = indicatorSource;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getFilter() {
        return filter;
    }

    public void setVariable(String xyear, String xmonth, String xyear2,
        String xmonth2, String campo, String lista) {
        StringBuilder filterStr = new StringBuilder("");

        switch (type) {
            case "periodo":
                setVariableByPeriod(filterStr, xyear, xmonth, xyear2, xmonth2);
                break;
            case "campo":
                setVariableByField(campo, filterStr);
                break;
            case "tabla":
                setVariableByTable(filterStr, lista);
                break;
            default:
                break;
        }
    }

    private void setVariableByPeriod(StringBuilder filterStr, String xyear,
        String xmonth, String xyear2, String xmonth2) {
        filterStr.append("TO_CHAR(");
        filterStr.append(field);
        filterStr.append(",'yyyy-mm' ) >= '");
        filterStr.append(xyear);
        filterStr.append("-");
        filterStr.append(Util.nullValue(xmonth));
        filterStr.append("'  AND TO_CHAR(");
        filterStr.append(field);
        filterStr.append(",'yyyy-mm' ) <= '");
        filterStr.append(xyear2);
        filterStr.append("-");
        filterStr.append(Util.nullValue(xmonth2));
        filterStr.append("'");
        
        this.filter = filterStr.toString();
    }

    private void setVariableByTable(StringBuilder filterStr, String lista) {
        filterStr.append(field);
        filterStr.append(" like '");
        filterStr.append(Util.nullValue(lista));
        filterStr.append("'");
        this.filter = filterStr.toString();
    }

    private void setVariableByField(String campo, StringBuilder filterStr) {
        if (campo != null && !"0".equals(campo) && campo.length() != 0) {
            filterStr.append(field);
            filterStr.append(" = '");
            filterStr.append(campo);
            filterStr.append("'");
            this.filter = filterStr.toString();
        }
    }

    @Override
    public String toString() {
        return "IndicatorVariable{" + "id=" + id + ", indicatorSource=" +
            indicatorSource + ", field=" + field + ", type=" + type +
            ", description=" + description + ", detail=" + detail + '}';
    }
}
