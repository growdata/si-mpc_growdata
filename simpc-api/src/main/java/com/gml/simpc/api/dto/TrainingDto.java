/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ModuleDao.java
 * Created on: 2016/12/14, 03:28:20 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

import java.math.BigDecimal;
import java.util.Date;


/**
 * DAO para consultar modulos.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class TrainingDto {

    private Long tipoCapId;
    private String tipoCapNombre;
    private Long tipoInstId;
    private String tipoInstNombre;
    private String ccfCode;
    private String ccfName;
    private Date fechaFin;
    private Date fechaIni;
    private String codProgMod;
    private int contModules;
    private BigDecimal enrollmentCost;
    private BigDecimal transportCost;
    private BigDecimal otherCost;
    private BigDecimal totalCost;
    private Long cargaId;

    public TrainingDto() {
        this.enrollmentCost = new java.math.BigDecimal("0.0");
        this.transportCost = new java.math.BigDecimal("0.0");
        this.otherCost = new java.math.BigDecimal("0.0");
        this.totalCost = new java.math.BigDecimal("0.0");
    }

    
    
    public Long getTipoCapId() {
        return tipoCapId;
    }

    public void setTipoCapId(Long tipoCapId) {
        this.tipoCapId = tipoCapId;
    }

    public String getTipoCapNombre() {
        return tipoCapNombre;
    }

    public void setTipoCapNombre(String tipCapNombre) {
        this.tipoCapNombre = tipCapNombre;
    }

    public Long getTipoInstId() {
        return tipoInstId;
    }

    public void setTipoInstId(Long tipoInstId) {
        this.tipoInstId = tipoInstId;
    }

    public String getTipoInstNombre() {
        return tipoInstNombre;
    }

    public void setTipoInstNombre(String tipoInstNombre) {
        this.tipoInstNombre = tipoInstNombre;
    }

    public String getCcfCode() {
        return ccfCode;
    }

    public void setCcfCode(String ccfCode) {
        this.ccfCode = ccfCode;
    }

    public String getCcfName() {
        return ccfName;
    }

    public void setCcfName(String ccfName) {
        this.ccfName = ccfName;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getFechaIni() {
        return fechaIni;
    }

    public String getCodProgMod() {
        return codProgMod;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }


    public void setCodProgMod(String codProgMod) {
        this.codProgMod = codProgMod;
    }

    public int getContModules() {
        return contModules;
    }

    public void setContModules(int contModules) {
        this.contModules = contModules;
    }

    public BigDecimal getEnrollmentCost() {
        return enrollmentCost;
    }

    public BigDecimal getTransportCost() {
        return transportCost;
    }

    public BigDecimal getOtherCost() {
        return otherCost;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setEnrollmentCost(BigDecimal enrollmentCost) {
        this.enrollmentCost = enrollmentCost;
    }

    public void setTransportCost(BigDecimal transportCost) {
        this.transportCost = transportCost;
    }

    public void setOtherCost(BigDecimal otherCost) {
        this.otherCost = otherCost;
    }


     public void setTotalCost() {
        
        this.totalCost=this.enrollmentCost.add(this.transportCost).add(
            otherCost);
    }

  

    

    public Long getCargaId() {
        return cargaId;
    }

    public void setCargaId(Long cargaId) {
        this.cargaId = cargaId;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(", cargaId=").append(cargaId).
            append(", tipoCapId=").append(tipoCapId).
            append(", tipoCapNombre=").append(tipoCapNombre).
            append(", tipoInstId=").append(tipoInstId).
            append(", tipoInstNombre=").append(tipoInstNombre).
            append(", ccfCode=").append(ccfCode).
            append(", ccfName=").append(ccfName).
            append(", fechaFin=").append(fechaFin).
            append(", codProgMod=").append(codProgMod).
            append(", contModules=").append(contModules).
            append(", enrollmentCost=").append(enrollmentCost).
            append(", transportCost=").append(transportCost).
            append(", otherCost=").append(otherCost).
            append(", fechaIni=").append(fechaIni).
            append(", totalCost=").append(totalCost);
        return stringBuilder.toString();
    }

}
