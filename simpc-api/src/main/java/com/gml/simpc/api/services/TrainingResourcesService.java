/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: SupplierBankService.java
 * Created on: 2017/01/12, 03:17:00 PM
 * Project: MPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.dto.TrainingDto;
import java.util.List;

/**
 * Interface que define el servicio para manipular las consultas del portal
 * de capacitaciones
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo Celis</a>
 */
public interface TrainingResourcesService {

    /**
     * Obtiene los contadores de modulo por tipo de capacitacion
     * para un tipo de institucion espcifico
     *
     * @param ccfSel
     *
     * @return
     */
    public List<TrainingDto> getCostsByCcf(String ccf,String year,String month, String costType);

    public List<TrainingDto> getCostsByPeriod(String ccf, String year,
        String month);

}
