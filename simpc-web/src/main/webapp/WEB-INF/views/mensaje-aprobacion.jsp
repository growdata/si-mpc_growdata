<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-toggle.min.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <link href="resources/css/editor.css" rel="stylesheet" />
    </head>
    <body>
        <c:if test="${not empty msg}">
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        <form:form id="templateForm" name="templateForm"   method="post" action='guardar-configuracion-template.htm'  >    
            <h1 >Respuesta de Aprobaci&oacute;n</h1>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 nopadding">
                        <textarea id="txtEditor"  name="txtEditor" class="form-control" path="txtEditor.text"  ></textarea>
                        <input type='hidden' id= 'htmlValor' name='htmlValor' value='' />
                        <input type='hidden' id= 'tipo' name='tipo' value='A' />
                    </div>
                </div>
            </div>
            <br>
        <center>
            <button type="submit" id="enviar" name="enviar" class="btn btn-success btn-group-lg"  >Guardar</button>
        </center>

    </form:form > 

    <!-- jQuery -->
    <script src="resources/js/jquery-1.12.3.js"></script>
    <script src="resources/js/jquery.dataTables.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/dataTables.bootstrap.min.js"></script>
    <script src="resources/js/editor.js"></script>
    <script>

        $(document).ready(function () {
            $("#txtEditor").Editor();
            $("#txtEditor").Editor("setText", '${configure.approvalTemplate }');

            $("#enviar").click(function () {
                var parrafo = $("#txtEditor").Editor("getText");
                document.getElementById('htmlValor').value = parrafo;
                document.forms["templateForm"].submit();
            });
        });
    </script>
</body>
</html>

