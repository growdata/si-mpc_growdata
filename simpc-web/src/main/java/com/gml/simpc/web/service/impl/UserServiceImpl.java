/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserServiceImpl.java
 * Created on: 2016/10/19, 02:11:43 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.entity.exeption.SystemException;
import com.gml.simpc.api.entity.exeption.UserException;
import com.gml.simpc.api.services.UserService;
import com.gml.simpc.api.utilities.Util;
import com.gml.simpc.commons.annotations.AnnotationInterceptor;
import com.gml.simpc.commons.mail.sender.Notificator;
import com.gml.simpc.web.repository.SessionRepository;
import com.gml.simpc.web.repository.UserRepository;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_021_PROFILE_USED;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_023_PROFILE_USED;
import static com.gml.simpc.api.utilities.Util.PARAMETER_USER_MESSAGE;
import static com.gml.simpc.api.utilities.Util.likeValue;
import static com.gml.simpc.api.utilities.Util.nullValue;

/**
 * Servicio para administrar usuarios.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Service(value = "userService")
public class UserServiceImpl implements UserService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(UserServiceImpl.class);

    /**
     * Repositorio para usuarios.
     */
    @Autowired
    private UserRepository userRepository;

    /**
     * Servicio para manejo de sesiones.
     */
    @Autowired
    private SessionRepository sesionRepository;

    /**
     * Servicio de notificaci&oacute;n.
     */
    @Autowired
    private Notificator notificator;

    /**
     * Mensaje de creaci&ocute;n de instituciones a los usuarios administradores.
     */
    private static final String MENSAJE_EDICION_USUARIO
            = "Se ha modificado la información basica de usuario o "
            + "se ha actualizado la contraseña";

    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
            afterExecutionValue = "DESPUES", methodName = "Consultar Todos los"
            + " Usuarios",
            entityTableName = "USUARIOS")
    @Override
    public List<User> getAll() {
        LOGGER.info("Ejecutando metodo [ getAll ]");
        return this.userRepository.findAll();
    }

    /**
     * M&ecute;todo para guardar un usuario.
     * 
     * @param user
     * @throws UserException 
     */
    @Override
    public void save(User user) throws UserException {

        try {
            this.userRepository.saveAndFlush(user);

        } catch (DataIntegrityViolationException ex) {
            LOGGER.error("Error Saving user: ");
            LOGGER.error(user.toString());
            LOGGER.error(ex);
            String constraint = ex.getLocalizedMessage();

            throw validateConstraint(constraint);
        }
    }

    /**
     * M&ecute;todo para actualizar un usuario.
     * 
     * @param user 
     */
    @Override
    public void update(User user) {
        LOGGER.info("Ejecutando metodo [ update in user]");
        this.notificator.sendNotification(user.getEmail(),
                String.format(MENSAJE_EDICION_USUARIO), user.getFirstName().concat(" ").
                concat(user.getFirstSurname()), PARAMETER_USER_MESSAGE);
        this.userRepository.saveAndFlush(user);
    }

    /**
     * M&ecute;todo para eliminar un usuario.
     * 
     * @param user 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
            afterExecutionValue = "DESPUES", methodName = "Borrar Usuario",
            entityTableName = "USUARIOS")
    @Override
    public void remove(User user) {
        LOGGER.info("Ejecutando metodo [ remove ]");

        this.userRepository.delete(user);
    }

    /**
     * M&ecute;todo que administra el logueo de usuarios.
     * 
     * @param email
     * @param userName
     * @param password
     * @param statusProfile
     * @return 
     */
    @Override
    public User searchUserLogin(String email, String userName, String password,
            String statusProfile) {
        LOGGER.info(
                "Ejecutando metodo [ searchUserLogin ]");
        try {
            return this.userRepository.
                    searchUserLogin(email, email, Util.calculateSha1(password),
                            statusProfile);
        } catch (Exception e) {
            LOGGER.error("Error", e);
            throw new SystemException("No se ha podido realizar la consulta");
        }
    }

    /**
     * M&ecute;todo para actualizar la sesi&ocute;n de un usuario.
     * 
     * @param userId
     * @return 
     */
    @Override
    public Integer updateUserSession(Long userId) {
        LOGGER.info(" entra a  updateUserSession en  UserService " + userId);

        try {
            Integer respuesta = this.sesionRepository.updateSession(userId);
            LOGGER.info("ya se corrio el update y la respuesta fue "
                    + respuesta);

            return respuesta;
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            return 0;
        }
    }

    /**
     * M&ecute;todo para buscar un usuario seg&ucute;n su correo.
     * 
     * @param userName
     * @param mail
     * @return 
     */
    @Override
    public User findByUsernameOrEmail(String userName, String mail) {
        LOGGER.info("Ejecutando metodo [ findByUsernameOrEmail ]");

        return this.userRepository.
                findByUsernameIgnoreCaseOrEmailIgnoreCase(userName, mail);
    }

    /**
     * M&ecute;todo para actualizar las credenciales de un usuario.
     * 
     * @param userName
     * @param email
     * @param userPass 
     */
    @Override
    public void updateCredentials(String userName, String email,
            String userPass) {
        this.userRepository.updateCredentials(userName, userName, userPass);
    }

    /**
     * M&ecute;todo para buscar usuarios seg&ucute;n su id.
     * 
     * @param id
     * @return 
     */
    @Override
    public User findById(Long id) {
        LOGGER.info("Ejecutando metodo [ findByName ]");
        User user = this.userRepository.findById(id);
        return user;
    }

    /**
     * M&ecute;todo para buscar una lista de usuarios segun los filtros 
     * especificos.
     * 
     * @param user
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
            afterExecutionValue = "DESPUES", methodName = "Encontrar Usuarios",
            entityTableName = "USUARIOS")
    @Override
    public List<User> findByFilters(User user) {
        LOGGER.info("Ejecutando metodo [ findByFilters ]");

        String code = String.valueOf(user.getStatus() == null ? ""
                : user.getStatus().name());
        
        LOGGER.info("CCF DE LA BUSQUEDA = " + user.getCcf());

        if ("1".equals(user.getCcf().getCode()) || "2".equals(user.
                getCcf().getCode()) || "3".equals(user.getCcf().getCode())) {

            return this.userRepository.
                    findByFiltersNotCcf(user.getCcf().getCode(),
                            nullValue(user.getIdentificationNumber()),
                            likeValue(user.getFirstName()), likeValue(code),
                            nullValue(user.getProfile().getId()));

        } else {

            return this.userRepository.
                    findByFilters(likeValue(user.getCcf().getCode()), nullValue(user.
                            getIdentificationNumber()),
                            likeValue(user.getFirstName()), likeValue(code),
                            nullValue(user.getProfile().getId()));
        }
    }

    /**
     * M&ecute;todo para actualizar usuarios.
     * 
     * @param creationUser
     * @throws UserException 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
            afterExecutionValue = "DESPUES", methodName = "Actualizar Usuario",
            entityTableName = "USUARIOS")
    @Override
    public void updateUser(User creationUser) throws UserException {
        LOGGER.info("Ejecutando metodo [ updateUser ]");

        try {
            this.userRepository.updateUser(
                    creationUser.getFirstName(),
                    creationUser.getSecondName(), creationUser.
                    getFirstSurname(), creationUser.getSecondSurname(),
                    creationUser.getPhoneNumber(), creationUser.
                    getAddress(), creationUser.getEmail(),
                    creationUser.getUsername(),
                    creationUser.getDocumentType().getId(),
                    creationUser.getIdentificationNumber(),
                    creationUser.getCcf().getCode(),
                    creationUser.getState().getId(),
                    creationUser.getCity().getId(),
                    String.valueOf(creationUser.getStatus().getCode()),
                    creationUser.getId());
        } catch (DataIntegrityViolationException ex) {
            LOGGER.error("Error", ex);
            String constraint = ex.getLocalizedMessage();

            throw validateConstraint(constraint);
        }
    }
    
    /**
     * M&ucute;todo de control para usuarios antiguos.
     * 
     * @param creationUser
     * @throws UserException 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
            afterExecutionValue = "DESPUES", methodName = "Actualizar Usuario",
            entityTableName = "USUARIOS")
    @Override
    public void updateUserOld(User creationUser) throws UserException {
        LOGGER.info("Ejecutando metodo [ updateUser ]");

        try {
            this.userRepository.updateUserOld(
                    String.valueOf(creationUser.getStatus().getCode()),
                    creationUser.getPassword(),
                    creationUser.getLastPasswordChange(),
                    creationUser.getId());
        } catch (DataIntegrityViolationException ex) {
            LOGGER.error("Error", ex);
            String constraint = ex.getLocalizedMessage();

            throw validateConstraint(constraint);
        }
    }

    /**
     * M&ecute;todo que valida los usuarios administradores del sistema.
     * 
     * @param isAdmin
     * @return 
     */
    @Override
    public List<User> findByAdmin(boolean isAdmin) {
        return this.userRepository.findByAdmin(isAdmin);
    }

    /**
     * M&ecute;todo que valida los usuarios inactivos del sistema.
     * 
     * @return 
     */
    @Override
    public List<User> findUsersToExpire() {
        return this.userRepository.findUsersToExpire();
    }

    /**
     * M&ecute;todo que evalua los usuarios proximos a estar inactivos. 
     * 
     * @return 
     */
    @Override
    public List<User> findUsersToExpirePrev() {
        return this.userRepository.findUsersToExpirePrev();
    }

    /**
     * M&eacute;todo para evaluar el constraint que dispara la base de datos.
     *
     * @param constraint
     *
     * @return
     */
    private UserException validateConstraint(String constraint) {
        if (constraint.contains(User.UQ_USUARIOS_CORREO)) {
            return new UserException(ERR_021_PROFILE_USED,
                    "Correo Electrónico");
        } else if (constraint.contains(User.UQ_USUARIOS_IDENTIFICACION)) {
            return new UserException(ERR_021_PROFILE_USED,
                    "Número de Identificación");
        } else if (constraint.contains(User.UQ_USUARIOS_USUARIO)) {
            return new UserException(ERR_021_PROFILE_USED,
                    "Nombre de Usuario");
        }

        return new UserException(ERR_023_PROFILE_USED);
    }
}
