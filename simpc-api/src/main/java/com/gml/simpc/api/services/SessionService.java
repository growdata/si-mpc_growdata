/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserService.java
 * Created on: 2016/10/19, 02:10:35 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.entity.Session;
import com.gml.simpc.api.entity.User;
import java.util.List;

/**
 * Interface que define el servicio de usuarios.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface SessionService {

    /**
     * Obtiene todos las sesiones creadas.
     *
     * @return
     */
    List<Session> getAll();

    /**
     * Servicio para guardar sesiones.
     *
     * @param session
     */
    void save(Session session);

    /**
     * Servicio para actualizar session.
     *
     * @param session
     */
    void update(Session session);

    /**
     * Elimina una session.
     *
     * @param session
     */
    void remove(Session session);

    /**
     *
     * @param user
     *
     * @return
     */
    Session findByUser(User user);

    /**
     * Expira las sesiones que tiene un tiempo de inactividad determinado
     * por parametros del sistema
     *
     * @return
     *         Entero que indica el borrado
     */
    int expireSessions();
    
    /**
     * Actualiza la fecha de ultimo ping de cliente
     *
     * @return
     *         Entero que indica el borrado
     */
    public Integer sessionPing(Session session);
}
