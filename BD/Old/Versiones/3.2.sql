create or replace view VW_MD_F_FOSFEC as
select fecha_postulacion_mpc,
    (TIPO_IDENTIFICACION||'-'||numero_identificacion) numero_identificacion,
    verificacion_requisitos,
    terminacion_beneficio,
    periodo_beneficio_liquidado,
    codigo_ccf,
fecha_verificacion_postu,
fecha_terminacion_beneficio,
monto_liquidado_cuota,
Monto_Liquidado_Cesantias,
Monto_Liquidado_Alimentacion,
monto_liquidado_salud,
monto_liquidado_pension,
valor_total_prestaciones,
beneficiario_del_mecanismo




    from MD_F_FOSFEC;

------
create or replace view VW_ESTADOS_APROBACION as
select 
    verificacion_requisitos id,
    case when verificacion_requisitos =1 then  'APROBADO'
    when  verificacion_requisitos =2 then 'DENEGADO'
    when verificacion_requisitos=3 then 'DESISTIDO'
    when  verificacion_requisitos =4 then 'CANCELADO'
    when  verificacion_requisitos =5 then 'EN PROCESO DE VERIFICACIÓN'
    end descripcion
    from MD_F_FOSFEC group by verificacion_requisitos;

------
create or replace view VW_BENEFICIARIO as
select 
  beneficiario_del_mecanismo id,
    case when beneficiario_del_mecanismo =1 or beneficiario_del_mecanismo =2 then  'SI'
     when beneficiario_del_mecanismo =3 or beneficiario_del_mecanismo =4  then 'NO'
   
    end descripcion
    from MD_F_FOSFEC group by beneficiario_del_mecanismo;