<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<link media="all" type="text/css" href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet" />
<link media="all" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet" />
<link media="all" type="text/css" href="<c:url value="/resources/css/dataTables.bootstrap.min.css" />" rel="stylesheet" />
<link media="all" type="text/css" href="<c:url value="/resources/css/bootstrap-theme.css" />" rel="stylesheet" />
<link media="all" type="text/css" href="<c:url value="/resources/font-awesome-4.7.0/css/font-awesome.css" />" rel="stylesheet" />

<script type="text/javascript" src="<c:url value="/resources/js/jquery-1.12.3.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.dataTables.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js"  />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/dataTables.bootstrap.min.js" />"></script>
<script>
    $(document).ready(function () {
        $('#institutions').DataTable({
            "language": {
                "url": "resources/js/Spanish.json"
            },
            "bFilter": false,
            "bInfo": false
        });
    });
</script>
<style>
    input[type=number]::-webkit-outer-spin-button,
    input[type=number]::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    input[type=number] {
        -moz-appearance:textfield;
    }
</style>



