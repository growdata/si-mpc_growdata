/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ErrorCode.java
 * Created on: 2016/05/31, 09:34:34 AM
 * Project: Twitter Master
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity.exeption.code;

/**
 * Enumeraci&oacute;n con los c&oacute;digos de error que utiliza la
 * aplicaci&oacute;n.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public enum FileErrorCode {

    FILE_ERR_001_EMPTY_FILE(
        "Error 001: El archivo est� vac�o."),
    FILE_ERR_002_FILE_SIZE_INVALID(
        "Error 002: El tama�o del archivo supera el m�ximo permitido"),
    FILE_ERR_003_FILE_TYPE_INVALID(
        "Error 003: El archivo debe ser un archivo plano con extensi�n txt"),
    FILE_ERR_004_ILEGAL_STATE_ERROR("Error 004: Error desconocido validando archivo. " +
        "Consulte al administrador."),
    FILE_ERR_005_INPUT_OUTPUT_ERROR("Error 005: Error desconocido validando archivo. " +
        "Consulte al administrador."),
    FILE_ERR_006_UNKNOW_ERROR("Error 006: Error desconocido validando archivo. " +
        "Consulte al administrador."),
    FILE_ERR_007_ILEGAL_STATE_ERROR("Error 007: Error desconocido cargando archivo. " +
        "Consulte al administrador."),
    FILE_ERR_008_INPUT_OUTPUT_ERROR("Error 008: Error desconocido cargando archivo. " +
        "Consulte al administrador."),
    FILE_ERR_009_UNKNOW_ERROR("Error 009: Error desconocido cargando archivo. " +
        "Consulte al administrador."),
    FILE_ERR_010_PROCESS_CREATION_ERROR("Error 010: Error desconocido creando carga. " +
        "Consulte al administrador."),
    FILE_ERR_011_VAL_JOB_CREATION_ERROR("Error 011: Error creando validaci�n de estructura. " +
        "Revise que el n�mero de columnas sea el adecuado para el tipo de carga seleccionado." +
        "Si el problema persiste consulte al administrador."),
    FILE_ERR_012_FTP_CONNECTION("Error 012: Error desconocido cargando archivo. " +
        "Consulte al administrador."),
    FILE_ERR_013_FTP_LOCATION("Error 013: Error desconocido cargando archivo. " +
        "Consulte al administrador."),
    FILE_ERR_014_ENCODING("Error 014: El archivo tiene caracteres especiales " +
        "y el Encoding esperado es UTF-8 ."),
    FILE_ERR_015_FILE_TYPE_INVALID(
        "Error 015: El archivo a cargar debe tener extensi�n pdf"),;


    /**
     * Mensaje de error.
     */
    private final String message;

    private FileErrorCode(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return message;
    }
    
    
}
