/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: User.java
 * Created on: 2016/12/0, 01:58:45 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gml.simpc.api.entity.exeption.code.FileErrorCode;
import com.gml.simpc.api.enums.ProcessStatus;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.joda.time.DateTime;
import org.joda.time.Period;

import static com.gml.simpc.api.utilities.Util.ERROR_FILE_EXT;
import static com.gml.simpc.api.utilities.Util.ERROR_FILE_SUFIX_BUSSINESS;
import static com.gml.simpc.api.utilities.Util.ERROR_FILE_SUFIX_STRUCTURE;

/**
 * Entidad que representa los posibles tipos de carga de archivos
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Table(name = "WF_CARGA_PROCESO")
@Entity
public class LoadProcess implements Serializable {

    /**
     * ID del proceso carga.
     */
    @Id
    @Column(name = "ID", length = 10)
    @GeneratedValue(generator = "wf_carga_seq")
    @SequenceGenerator(name = "wf_carga_seq", sequenceName = "WF_CARGA_SEQ",
        allocationSize = 1, initialValue= 1)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "ESTADO",
        nullable = false)
    private ProcessStatus status;

    /**
     * Tipo de carga
     */
    @Column(name = "TIPO_CARGA", nullable = false)
    private String loadType;

    /**
     * Tipo de carga
     */
    @Column(name = "TIPO_ENVIO", nullable = false)
    private String sendType;

    /**
     * Nombre original del archivo cargado
     */
    @Column(name = "NOMBRE_ARCHIVO_ORIGINAL", nullable = false)
    private String originalFileName;

    /**
     * Ruta del archivo en disco
     */
    @Column(name = "BASE_ARCHIVO_CARGADO", nullable = false)
    private String newFileRoot;

    /**
     * Ruta del archivo en disco
     */
    @Column(name = "RUTA_ARCHIVO_CARGADO", nullable = false)
    private String newFilePath;

    /**
     * Nombre del archivo en disco
     */
    @Column(name = "NOMBRE_ARCHIVO_CARGADO", nullable = false)
    private String newFileName;

    /**
     * Nombre del archivo de errores de estructura en disco
     */
    @Column(name = "ARCHIVO_ERR_EST", nullable = false)
    private String structureErrorFileName;

    /**
     * Nombre del archivo de errores de negocio en disco
     */
    @Column(name = "ARCHIVO_ERR_NEG", nullable = false)
    private String bussinessErrorFileName;

    /**
     * Total de registros del archivo
     */
    @Column(name = "registros_totales", nullable = false)
    private int totalRows;

    /**
     * Total de registros correctosdel archivo
     */
    @Column(name = "registros_exitosos", nullable = false)
    private int successRows;

    /**
     * Total de registros fallidos del archivo
     */
    @Column(name = "registros_fallidos", nullable = false)
    private int failRows;

    /**
     * Detalle de la carga
     */
    @Column(name = "detalle", nullable = true, columnDefinition = "CLOB")
    private String detail;

    /**
     * Tama�o del archivo
     */
    @Column(name = "peso", nullable = false)
    private Long size;

    /**
     * Consultor que realiza la carga
     */
    @ManyToOne
    @JoinColumn(name = "consultor", nullable = false)
    private User consultant;

    /**
     * Fecha de inicio de proceso de carga
     */
    @Column(name = "fecha_inicio", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date initDate;

    /**
     * Get the value of endDate
     */
    @Column(name = "fecha_fin", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    /**
     * Duraci�n del proceso
     */
    @Column(name = "duracion", nullable = true)
    private int time;

    /**
     * Tipo de escritura de la informacion
     */
    @Column(name = "TIPO_ESCRITURA", nullable = false)
    private String writeType;

    /**
     * Usuario que ejecuta la carga
     */
    @Column(name = "EMAIL_NOTIFICACION", nullable = false)
    private String notifyEmail;

    /**
     * Codigo de error
     */
    @Column(name = "ERROR_CODE", nullable = true)
    private FileErrorCode errorCode;

    /**
     * Fecha de inicio de proceso de carga
     */
    @Column(name = "fecha_ini_val_est", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date initDateStrVal;

    /**
     * Fecha de inicio de proceso de carga
     */
    @Column(name = "fecha_ini_stage", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date initDateStage;

    /**
     * Fecha de inicio de proceso de carga
     */
    @Column(name = "fecha_ini_val_neg", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date initDateBussiness;

    /**
     * Fecha de inicio de proceso de carga
     */
    @Column(name = "fecha_ini_master", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date initDateMasterData;

    /**
     * Propiedad para cargar el archivo desde la vista
     */
    @JsonIgnore
    @Transient
    private Object file;

    /**
     * Propiedad para saber si el archivo se cargo satisfactoriamente
     */
    @Transient
    private boolean fileSended;

    /**
     * Tama�o del archivo en string
     */
    @Transient
    private String strSize;

    /**
     * Formato para fecha.
     */
    @JsonIgnore
    @Transient
    private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Formato para hora.
     */
    @JsonIgnore
    @Transient
    private final DateFormat timeFormat = new SimpleDateFormat("HHmmssS");
    /**
     * Cabecera del archivo.
     */
    @Column(name = "CABECERA", length = 500)
    private String header;

    /*
     * --------------------------------------------------------------------
     */
    public LoadProcess() {
    }

    public LoadProcess(String newFileRoot, String sendType, String loadType,
        String writeType) {
        this.newFileRoot = newFileRoot;
        this.sendType = sendType;
        this.loadType = loadType;
        this.writeType = writeType;
    }

    public LoadProcess(String loadType, String sendType, String originalFileName,
        String newFileRoot, String newFilePath, String newFileName,
        String structureErrorFileName, String bussinessErrorFileName, Long size,
        User consultant, String writeType, String notifyEmail) {
        this.loadType = loadType;
        this.sendType = sendType;
        this.originalFileName = originalFileName;
        this.newFileRoot = newFileRoot;
        this.newFilePath = newFilePath;
        this.newFileName = newFileName;
        this.structureErrorFileName = structureErrorFileName;
        this.bussinessErrorFileName = bussinessErrorFileName;
        this.size = size;
        this.consultant = consultant;
        this.writeType = writeType;
        this.notifyEmail = notifyEmail;
    }

    public User getConsultant() {
        return consultant;
    }

    public boolean isFileSended() {
        return fileSended;
    }

    public DateFormat getDateFormat() {
        return dateFormat;
    }

    /*
     * ------------------------------------------------------------------------
     */
    public DateFormat getTimeFormat() {
        return timeFormat;
    }

    public Long getId() {
        return id;
    }

    public void setStructureErrorFileName(String structureErrorFileName) {
        this.structureErrorFileName = structureErrorFileName;
    }

    public void setBussinessErrorFileName(String bussinessErrorFileName) {
        this.bussinessErrorFileName = bussinessErrorFileName;
    }

    public void setConsultant(User consultant) {
        this.consultant = consultant;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProcessStatus getStatus() {
        return status;
    }

    public void setStatus(ProcessStatus status) {
        this.status = status;
    }

    public String getLoadType() {
        return loadType;
    }

    public void setLoadType(String loadType) {
        this.loadType = loadType;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public String getNewFilePath() {
        return newFilePath;
    }

    public void setNewFilePath(String newFilePath) {
        this.newFilePath = newFilePath;
    }

    public String getNewFileName() {
        return newFileName;
    }

    public void setNewFileName(String newFileName) {
        this.newFileName = newFileName;
    }

    public String getStructureErrorFileName() {
        return structureErrorFileName;
    }

    public void setStructureErrorFileName() {
        String name = newFileName.substring(0, newFileName.lastIndexOf('.'));
        this.structureErrorFileName =
            name + ERROR_FILE_SUFIX_STRUCTURE + "." + ERROR_FILE_EXT;
    }

    public String getBussinessErrorFileName() {
        return bussinessErrorFileName;
    }

    public void setBussinessErrorFileName() {
        String name = newFileName.substring(newFileName.lastIndexOf('\\') + 1,
            newFileName.lastIndexOf('.'));
        this.bussinessErrorFileName =
            name + ERROR_FILE_SUFIX_BUSSINESS + "." + ERROR_FILE_EXT;

    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public int getSuccessRows() {
        return successRows;
    }

    public void setSuccessRows(int successRows) {
        this.successRows = successRows;
    }

    public void setSuccessRows() {
        this.successRows = totalRows - failRows;
    }

    public int getFailRows() {
        return failRows;
    }

    public void setFailRows(int failRows) {
        this.failRows = failRows;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getTime() {
        return time;
    }

    public void setTime() {
        DateTime startTime = new DateTime(initDate);
        DateTime endTime = new DateTime(endDate);
        Period p = new Period(startTime, endTime);
        time = p.getMillis();
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Long getSize() {
        return size;
    }

    public Object getFile() {
        return file;
    }

    public void setFile(Object file) {
        this.file = file;
    }

    public void setNotifyEmail(String notifyEmail) {
        this.notifyEmail = notifyEmail;
    }

    public String getNotifyEmail() {
        return notifyEmail;
    }

    public String getSendType() {
        return sendType;
    }

    public void setSendType(String sendType) {
        this.sendType = sendType;
    }

    public FileErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(FileErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public boolean getFileSended() {
        return fileSended;
    }

    public void setFileSended(boolean fileSended) {
        this.fileSended = fileSended;
    }

    public String getStrSize() {
        return strSize;
    }

    public void setStrSize(String strSize) {
        this.strSize = strSize;
    }

    public String getWriteType() {
        return writeType;
    }

    public void setWriteType(String writeType) {
        this.writeType = writeType;
    }

    public Date getInitDateStrVal() {
        return initDateStrVal;
    }

    public void setInitDateStrVal(Date initDateStrVal) {
        this.initDateStrVal = initDateStrVal;
    }

    public Date getInitDateStage() {
        return initDateStage;
    }

    public void setInitDateStage(Date initDateStage) {
        this.initDateStage = initDateStage;
    }

    public Date getInitDateBussiness() {
        return initDateBussiness;
    }

    public void setInitDateBussiness(Date initDateBussiness) {
        this.initDateBussiness = initDateBussiness;
    }

    public Date getInitDateMasterData() {
        return initDateMasterData;
    }

    public void setInitDateMasterData(Date initDateMasterData) {
        this.initDateMasterData = initDateMasterData;
    }

    public String getNewFileRoot() {
        return newFileRoot;
    }

    public void setNewFileRoot(String newFileRoot) {
        this.newFileRoot = newFileRoot;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    /*
     * ------------------------------------------------------------------------
     */
    public void setFileInfo(String ext, String fileName, Long fileSize) {
        Date date = new Date();

        newFilePath = dateFormat.format(date) + System.getProperty(
            "file.separator") + loadType + System.getProperty("file.separator");
        newFileName = timeFormat.format(date) + "." + ext;
        originalFileName = fileName;
        setStructureErrorFileName();
        setBussinessErrorFileName();
        size = fileSize;

        String sizeToString = this.size + " Bytes";

        if (this.size < 1000) {
            sizeToString = this.size + " Bytes";
        } else if (this.size < 1000000) {
            sizeToString = this.size / 1000 + " Kilobytes";
        } else if (this.size < 1000000000) {
            sizeToString = this.size / 1000000 + " Megabytes";
        }
        strSize = sizeToString;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Proceso de carga{").
            append("id=").append(id).
            append(", Estado=").append(status).
            append(", Tipo de Carga=").append(loadType).
            append(", Tipo de Env�o=").append(sendType).
            append(", Nombre del Archivo Original=").append(originalFileName).
            append(", Base Nueva del Archivo=").append(newFileRoot).
            append(", Ruta Nueva del Archivo=").append(newFilePath).
            append(", Nuevo Nombre del Archivo=").append(newFileName).
            append(", Archivo de Errores de Estructura=").append(
            structureErrorFileName).
            append(", Archivo de Errores de Negocio=").append(
            bussinessErrorFileName).
            append(", Total de Filas=").append(totalRows).
            append(", N�mero de Filas Exitosamente Cargadas=").append(
            successRows).
            append(", N�mero de Filas con Error=").append(failRows).
            append(", Detalle=").append(detail).
            append(", Tama�o del Archivo=").append(size).
            append(", Usuario Que inicio el Cargue=").append(consultant).
            append(", Fecha de inicio=").append(initDate).
            append(", Fecha de finalizaci�n=").append(endDate).
            append(", Duraci�n=").append(time).
            append(", Tipo de Escritura=").append(writeType).
            append(", Fecha de inicio con Formato=").append(initDateStrVal).
            append(", Fecha de inicio cargue a Stage=").append(initDateStage).
            append(", Fecha de inicio Validaciones de Negocio=").append(
            initDateBussiness).
            append(", Fecha de inicio Cargue a SIMPC=").append(
            initDateMasterData).
            append(", Correo de Notificaci�n=").append(notifyEmail).
            append(", C�digo de Error=").append(errorCode).append("}");
        return stringBuilder.toString();
    }
}
