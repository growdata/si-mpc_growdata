
# README 

Aplicaci�n para el ministerio de trabajo a trav�s de asopagos V 3.0

## CONFIGURACI�N DE ENTORNO DE DESARROLLO

El entorno se configura en un ambiente windows.

### DESCARGA E INSTALACI�N DE APLICACIONES NECESARIAS

1. 	Instalar jdk 1.8: [https://www.java.com/es/download/](https://www.java.com/es/download/ "Descargar instalador").

2. 	Instalar jdk 1.7: [http://www.oracle.com](http://www.oracle.com/technetwork/java/javase/downloads/java-archive-downloads-javase7-521261.html#jdk-7u80-oth-JPR "Descargar instalador").

3. 	Instalar Netbeans: [https://netbeans.org/downloads/](https://netbeans.org/downloads/ "Descargar instalador").

4. 	Instalar Git: [https://git-scm.com/download/win](https://git-scm.com/download/win "Descargar instalador"). Se recomienda realizar el [Tutorial recomendado](http://learngitbranching.js.org/ "Ir al tutorial").

5. 	Instalar Oracle express: [http://www.oracle.com/](http://www.oracle.com/technetwork/database/database-technologies/express-edition/downloads/index.html "Descargar instalador"). Definir como usuario system y pasword simpc2016.
		
6. 	Instalar Sql developer: [http://www.oracle.com/](http://www.oracle.com/technetwork/developer-tools/sql-developer/downloads/index.html "Descargar instalador").

7. 	Instalar weblogic usando java 8: [http://www.oracle.com/](http://www.oracle.com/technetwork/middleware/weblogic/downloads/wls-main-097127.html "Descargar instalador"). 

El comando de instalaci�n es el siguiente. Debe ejecutarse en ventana de comandos, ejecutada como administrador:

* "C:\Program Files\Java\jdk1.8.0_111\bin\java" -jar "C:\Users\Usuario\Downloads\instaladores\fmw_12.2.1.2.0_wls_Disk1_1of1\fmw_12.2.1.2.0_wls.jar"

Tener en cuenta personalizar ubicaci�n de java y ubicaci�n de jar.
	

### PREPARACI�N DE SERVIDOR
 
1. 	Crear dominio a traves del Configuration Wizard. Tener presente usuario y password. Usar Java 8: 

		* La ruta del dominio ser� algo como C:\Oracle\Middleware\Oracle_Home\user_projects\domains\base_domain.

		* Definir usuario develop, password simpc2016.

		
2. 	Correr servidor:

        * Ejecutar archivo C:\Oracle\Middleware\Oracle_Home\user_projects\domains\base_domain\bin\startWebLogic.cmd.

3. Ver que suba correctamente.
 
4. Parametrizar conexi�n a bd ingresando a la consola de weblogic [http://localhost:7001/console](http://localhost:7001/console).

		* En el �rbol de opciones abrimos Servicios > Origenes de datos.

		* Seleccionamos Nuevo > Origen de datos gen�rico.

		* Definir:
			* Nombre: SIMPC
			* Nombre del JNDI: jdbc/simpc
			* Tipo de base de datos: Oracle
			* Nombre de la base XE
			* Nombre de host: localhost
			* Usuario system y password simpc2016

		* En Controlador de bases de datos seleccionar *Oracle's Driver (Thin) for Service connections; Versions:Any .

                * Luego de creada la conexi�n, entrar a Destinos y seleccionar el servidor AdminServer


5. Detener servidor		
 
### PREPARACI�N DE APLICACI�N
 
1. 	Configurar plantillas de Netbeans.

2. 	En Netbeans, en ventana de prestaciones adicionar servidor Web logic con el dominio creado anteriormente.
 
3. 	Checkout del proyecto Git en Netbeans: 

		* Entrar por team/git/clone.

		* Clonar de bitbucket https://usuario@bitbucket.org/gmlasopagos/repo_simpc.git

		* Clonar en C:Glm/Proyectos/simpc/develop/repo_simpc/

        * Configurar datos de acceso de git por terminal:

            * cd C:Glm/Proyectos/simpc/develop/repo_simpc/
            * git config --global user.name "YOUR NAME"
            * git config --global user.email "YOUR EMAIL ADDRESS"


4. 	Habilitar vista de proyectos

5. 	Configurar en cada uno de los cuatro proyectos compilacion con java 1.7 y quitar opcion compile on save.

6.	Ejecutar Build whith dependencies en cada uno de los cuatro proyectos.

7.  Parametrizar dominio	

		* Copiar los archivos de la carpeta config desde la raiz del proyecto a la ubicaci�n del disco C:\glm\config\simpc: config.properties, log4j.properties y correo.html.

                * Personalizar ruta de logs en archivo log4j.properties

		* Abrir C:\Oracle\Middleware\Oracle_Home\user_projects\domains\base_domain\nodemanager\nodemanager.properties y verificar que la propiedad weblogic.StartScriptEnabled se encuentre en true.

		* Tomar el nombre que se encuentra en la propiedad weblogic.StartScriptName del archivo anterior (Puede ser startWebLogic.cmd o startWebLogic.sh).

		* Editar dicho archivo en la carpeta bin del dominio

		* Adicionar en la l�nea 80 set JAVA_OPTIONS="-DAPP_CONFIG=C:\config" "-DUseSunHttpHandler=true", es decir, ruta configurada en el primer aparte de esta secci�n. Se puede revisar como accede la aplicaci�n a estos archivos en el archivo simpc-web\src\main\webapp\WEB-INF\applicationContext-jpa.xml del proyecto.

		
8. 	Ejecutar aplicaci�n con weblogic, ver que suba en [http://localhost:7001/simpc/](http://localhost:7001/simpc/ "Ir a sitio local").


### PREPARACI�N DE BASE DE DATOS

1. Crear conexi�n en sqldeveloper.

2. Filtrar tablas por fecha de creacion > fecha actual.

3. Ejecutar archivo del proyecto BD/2-script_modificaciones_estructura.sql. Adiciona comentarios y secuencias.

4. Ejecutar archivo del proyecto BD/3-script_datos_parametricos.sql. Inserta datos de funcionamiento param�tricos.

5. Ejecutar archivo del proyecto BD/4-script_datos de prueba. Inserta datos de prueba en cuanto a usuario e instituciones.

6. Ingresar a la aplicaci�n con el usuario desarrollo y el password 0000.