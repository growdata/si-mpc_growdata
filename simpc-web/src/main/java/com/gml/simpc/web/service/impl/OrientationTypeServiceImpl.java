/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: OrientationTypeServiceImpl.java
 * Created on: 2017/01/23, 04:12:15 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.entity.valuelist.OrientationType;
import com.gml.simpc.api.services.OrientationTypeService;
import com.gml.simpc.commons.annotations.AnnotationInterceptor;
import com.gml.simpc.web.repository.valuelist.OrientationTypeRepository;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
@Service(value = "orientationTypeService")
public class OrientationTypeServiceImpl implements OrientationTypeService {


    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(OrientationTypeServiceImpl.class);

    /**
     * Repositorio para tipos de programas.
     */
    @Autowired
    private OrientationTypeRepository orientationTypeRepository;

    /**
     * Genera la lista con todos los tipos de programa.
     * 
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar Todos",
        entityTableName = "TIPOS_ORIENTACION")
    @Override
    public List<OrientationType> getAll() {
        LOGGER.info("Ejecutando metodo [ getAll de tipos instituciones ]");
        return this.orientationTypeRepository.findAll();
    }

    /**
     * Guarda tipos de programa.
     * 
     * @param orientationType 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Guardar",
        entityTableName = "TIPOS_ORIENTACION")
    @Override
    public void save(OrientationType orientationType) {
        LOGGER.info("Ejecutando metodo [ save de tipos de instituciones]");
        this.orientationTypeRepository.saveAndFlush(orientationType);
    }

    /**
     * Actualiza tipos de programa.
     * 
     * @param program 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Actualizar",
        entityTableName = "TIPOS_ORIENTACION")
    @Override
    public void update(OrientationType program) {
        LOGGER.info("Ejecutando metodo [ update de instituciones]");
        this.orientationTypeRepository.saveAndFlush(program);
    }
    
    /**
     * Elimina tipos de programa.
     * 
     * @param program 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Borrar",
        entityTableName = "TIPOS_ORIENTACION")
    @Override
    public void remove(OrientationType program) {
        LOGGER.info("Ejecutando metodo [ remove de instituciones ]");
        this.orientationTypeRepository.delete(program);
    }

}

