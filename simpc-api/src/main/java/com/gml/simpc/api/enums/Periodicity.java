/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Periodicity.java
 * Created on: 2016/12/12, 03:39:01 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.enums;

/**
 * Enumeraci&oacute;n para la periodicidad con la que se muestran los
 * indicadores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public enum Periodicity {

    M("MENSUAL"),
    A("ANUAL");

    /**
     * Etiqueta de la periodicidad.
     */
    private final String label;

    private Periodicity(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }
}
