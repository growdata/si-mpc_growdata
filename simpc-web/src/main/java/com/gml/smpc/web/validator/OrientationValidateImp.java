/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Light.java
 * Created on: 2017/01/18, 04:30:10 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.smpc.web.validator;

import com.gml.simpc.loader.dto.ValidationErrorDto;
import com.gml.simpc.loader.validator.ValidateCommand;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

/**
 * Clase para implementar validaciones particulares de negocio en carges de
 * información
 * en cuanto a Orientaciones
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
@Service(value = "orientationValidateService")
public class OrientationValidateImp implements ValidateCommand {

    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(
        OrientationValidateImp.class);

    /**
     * Consulta para encontrar programas de instituciones inactivas
     */
    private static final String RULE_QUERY = "";

    /**
     * Plantilla JDBC para consultar las propiedades.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Realiza la validación.
     *
     * @param loadId
     *
     * @return
     */
    @Override
    public List<ValidationErrorDto> validate(Long loadId) {

        List<ValidationErrorDto> errorsList = new ArrayList<>();
        LOGGER.info("Validating orientation  for load ... " + loadId);

        return errorsList;
    }

    /**
     * Verifica si es del tipo que le corresponde.
     *
     * @param loadType
     *
     * @return
     */
    @Override
    public boolean verify(String loadType) {
        return "ORI-1".equals(loadType);
    }
}
