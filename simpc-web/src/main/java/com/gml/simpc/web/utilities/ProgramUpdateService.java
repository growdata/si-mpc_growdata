/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ValueListService.java
 * Created on: 2017/01/16, 02:45:08 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.utilities;

import com.gml.simpc.api.dao.IndividualIntersectionDao;
import com.gml.simpc.api.dto.ProgramDto;
import com.gml.simpc.api.entity.LoadProcess;
import com.gml.simpc.api.entity.Program;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.entity.valuelist.Certifications;
import com.gml.simpc.api.entity.valuelist.Cine;
import com.gml.simpc.api.entity.valuelist.DeliveredCertification;
import com.gml.simpc.api.entity.valuelist.Foundation;
import com.gml.simpc.api.entity.valuelist.ProgramType;
import com.gml.simpc.api.enums.ProgramNecessity;
import com.gml.simpc.api.services.ProgramService;
import com.gml.simpc.web.repository.LoadProcessRepository;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Componente para actualizar programas
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Service("programUpdateService")
public class ProgramUpdateService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER
            = Logger.getLogger(ProgramUpdateService.class);
    /**
     * Constante para datos en master data.
     */
    private static final int MASTER_DATA = 0;
    /**
     * Constante para datos en transici&oacute;n.
     */
    public static final int LOCKED = -1;
    /**
     * Constante para datos en el dominio.
     */
    private static final int DOMAIN = 1;
    /**
     * Constante para datos en error.
     */
    public static final int ERROR = 2;

    /**
     * Dao para acceso de informaci&oacute;n en la Master Data.
     */
    @Autowired
    private IndividualIntersectionDao individualIntersectionDao;
    /**
     * Servicio para adminisraci&oacute;n de programas intermediario para la
     * comunicaci&oacute;n con la persistencia.
     */
    @Autowired
    private ProgramService programService;

    /**
     * Servicio de cargas.
     */
    @Autowired
    private LoadProcessRepository loadProcessRepository;

    /**
     * Repositorio de fundaciones
     */
    @Autowired
    private ValueListService valueListService;
    /**
     * Mapa de usuarios por carga.
     */
    private Map<Long, User> usersByLoads;

    /**
     * Actualiza periodicamente los programas cargados a los de la aplicacion la
     * base de datos.
     */
    @Transactional
    public void moveProgramsFromMasterDataToDomain() {
        LOGGER.info("Ejecutando metodo [ moveProgramsFromMasterDataToDomain ]");

        List<ProgramDto> programList = getProgramList();
        processList(programList);
        releaseProcessedPrograms();
    }

    /**
     * Coloca los programas a trabajar en transici&oacute;n.
     */
    public void lockObjetiveResultSet() {
        LOGGER.info("Ejecutando metodo [ lockObjetiveResultSet ]");

        this.individualIntersectionDao.updateInDomainColumn(MASTER_DATA,
                LOCKED);
    }

    /**
     * Coloca los programas a trabajar en error.
     */
    public void discardObjetiveResultSet() {
        LOGGER.info("Ejecutando metodo [ discardObjetiveResultSet ]");

        this.individualIntersectionDao.
                updateInDomainColumn(ProgramUpdateService.LOCKED,
                        ProgramUpdateService.ERROR);
    }

    /**
     * Obtiene la lista de programas en transici&oacute;n.
     *
     * @return
     */
    private List<ProgramDto> getProgramList() {
        LOGGER.info("Ejecutando metodo [ getProgramList ]");
        return this.individualIntersectionDao.findTrainings();
    }

    /**
     * Procesa la lista de programas.
     *
     * @param programsFromMDF
     */
    private void processList(List<ProgramDto> programsFromMDF) {
        LOGGER.info("Ejecutando metodo [ processList ]");

        validateLoadList();

        for (ProgramDto program : programsFromMDF) {
            addLoadToList(program);

            Program oldProgram = this.programService.
                    findByCode(program.getProgramCode());

            oldProgram = initProgram(oldProgram, program);
            mergeProgramData(program, oldProgram);

            this.programService.saveUpdate(oldProgram);
        }
    }

    /**
     * Mezcla la data de los programas.
     *
     * @param program
     * @param oldProgram
     */
    private void mergeProgramData(ProgramDto program, Program oldProgram) {
        LOGGER.info("Ejecutando metodo [ mergeProgramData ]");

        try {

            Foundation foundation = this.valueListService.getFoundations().
                    get(program.getCcfCode());
            oldProgram.setCcf(foundation);
            oldProgram.setCode(program.getProgramCode());
            oldProgram.setHours(Integer.parseInt(program.getTotalHours()));
            oldProgram.setName(program.getProgramName());
            oldProgram.setNecessity(ProgramNecessity.
                    getByCode(Integer.parseInt(program.getNecesity())));
            if ("1".equals(program.getCertified())) {
                oldProgram.setQualityCertificate("SI");
            } else {
                oldProgram.setQualityCertificate("NO");
            }

            Certifications certification = this.valueListService.
                    getCertifications().get(Integer.parseInt(program.
                            getCertName()) - 1);
            oldProgram.setCertification(certification);

            oldProgram.setWhichName(program.getOtherCertName());

            ProgramType programType = this.valueListService.getProgramTypes().
                    get(program.getCourseType());
            oldProgram.setType(programType);

            Cine cine = this.valueListService.getCine().get(program.getCine());
            oldProgram.setCine(cine);

            DeliveredCertification deliveredCertification
                    = new DeliveredCertification();

            if(null!=program.getCourseType())switch (program.getCourseType()) {
                case "1":
                case "2":
                case "6":
                    deliveredCertification.setId(Long.parseLong("4"));
                    break;
                case "3":
                    deliveredCertification.setId(Long.parseLong("3"));
                    break;
                case "4":
                    deliveredCertification.setId(Long.parseLong("1"));
                    break;
                case "5":
                    deliveredCertification.setId(Long.parseLong("2"));
                    break;
                default:
                    break;
            }

            oldProgram.setDeliveredCertification(deliveredCertification);

        } catch (Exception e) {
            LOGGER.error("Error", e);
            LOGGER.info("NO SE PUDO CONVERTIR ALGUN PROGRAMA");
        }
    }

    /**
     * Inicializa el programa con los datos minimos requeridos.
     *
     * @param oldProgram
     * @param program
     *
     * @return
     */
    private Program initProgram(Program oldProgram, ProgramDto program) {
        LOGGER.info("Ejecutando metodo [ initProgram ]");

        Program programToUse = oldProgram;

        if (oldProgram == null) {
            programToUse = new Program();
            programToUse.setCreationDate(new Date());
            programToUse.setCreationUserId(this.usersByLoads.
                    get(program.getCargaId()));
        } else {
            programToUse.setModificationDate(new Date());
            programToUse.setModificationUserId(this.usersByLoads.
                    get(program.getCargaId()));
        }

        return programToUse;
    }

    /**
     * Agrega una carga si es nueva a la lista de cargas.
     *
     * @param program
     */
    private void addLoadToList(ProgramDto program) {
        LOGGER.info("Ejecutando metodo [ addLoadToList ]");
        if (this.usersByLoads.get(program.getCargaId()) == null) {
            LoadProcess loadProcess
                    = this.loadProcessRepository.findOne(program.getCargaId());

            this.usersByLoads.put(program.getCargaId(),
                    loadProcess.getConsultant());
        }
    }

    /**
     * Valida la lista de cargas en memoria.
     */
    private void validateLoadList() {
        LOGGER.info("Ejecutando metodo [ validateLoadList ]");
        if (this.usersByLoads == null || this.usersByLoads.size() > 100) {
            this.usersByLoads = new HashMap<>();
        }
    }

    /**
     * Coloca los programas trabajados en dominio.
     */
    private void releaseProcessedPrograms() {
        LOGGER.info("Ejecutando metodo [ releaseProcessedPrograms ]");

        this.individualIntersectionDao.updateInDomainColumn(LOCKED,
                DOMAIN);
    }
}
