/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IndicatorGraphicServiceImpl.java
 * Created on: 2016/12/22, 03:36:56 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.dao.GraphicDao;
import com.gml.simpc.api.dto.GraphicDataDto;
import com.gml.simpc.api.dto.IndicatorFilterDto;
import com.gml.simpc.api.dto.TableDataDto;
import com.gml.simpc.api.entity.IndicatorGraphic;
import com.gml.simpc.api.services.IndicatorGraphicService;
import com.gml.simpc.web.repository.IndicatorGraphicRepository;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Servicio para manejar gr&aacute;ficos de indicadores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>.
 */
@Service
public class IndicatorGraphicServiceImpl implements IndicatorGraphicService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = 
        Logger.getLogger(IndicatorGraphicServiceImpl.class);
    /**
     * Repositorio de gr&aacute;fico de indicadores.
     */
    @Autowired
    private IndicatorGraphicRepository indicatorGraphicRepository;
    /**
     * Dao para gr&aacute;ficos.
     */
    @Autowired
    private GraphicDao graphicDao;

    /**
     * Busca el indicador por id.
     * 
     * @param id
     * @return 
     */
    @Override
    public IndicatorGraphic findById(Long id) {
        LOGGER.info("Ejecutando metodo [ findById ]");
        
        return this.indicatorGraphicRepository.findOne(id);
    }

    /**
     * Busca la informaci&ocute;n del grafico seg&ucute;n los filtros ingresados.
     * 
     * @param filter
     * @return 
     */
    @Override
    public List<GraphicDataDto> getGraphicData(IndicatorFilterDto filter) {
        LOGGER.info("Ejecutando metodo [ getGraphicData ]");
        
        return this.graphicDao.getGraphicData(filter);
    }

    /**
     * Genera la informaci&ocute;n de la tabla seg&ucute;n los filtros ingresados.
     * 
     * @param filter
     * @return 
     */
    @Override
    public List<TableDataDto> getTableData(IndicatorFilterDto filter) {
        LOGGER.info("Ejecutando metodo [ getTableData ]");
        
        return this.graphicDao.getTableData(filter);
    }
}
