/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: SuppliersDaoImpl.java
 * Created on: 2017/01/10, 11:13:59 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.dao.impl;

import com.gml.simpc.api.dao.SupplierDao;
import com.gml.simpc.api.dto.GraphicDataDto;
import com.gml.simpc.api.dto.TableDataDto;
import com.gml.simpc.api.dto.ValueListDto;
import com.gml.simpc.api.entity.Institution;
import com.gml.simpc.api.entity.valuelist.InstitutionType;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * Dao define la forma en que se mostrara infomacion del banco de oferentes en
 * los portales
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
@Repository
public class SuppliersDaoImpl implements SupplierDao {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(SuppliersDaoImpl.class);

    /**
     * Consulta para encontrar los nombres de los mudulos
     */
    private static final String FIND_ORIENTATION_TYPE =
        "SELECT CODIGO,DESCRIPCION FROM TIPO_ORIENTACION";

    /**
     * Consulta para encontrar los nombres de los mudulos
     */
    private static final String FIND_DURATION =
        "SELECT CODIGO,DURACION FROM PROGRAMAS GROUP BY CODIGO,DURACION";
    /**
     * Consulta para encontrar los nombres de los mudulos
     */
    private static final String FIND_FORMATION_TYPE =
        "SELECT ID,NOMBRE FROM TIPOS_PROGRAMA";
    /**
     * Parte de consulta para generar segun seleccione el filtro del portal de
     * instituciones
     */
    private static final String FIND_INSTITUTION_BY =
        "SELECT i.ID,nombre_institucion, t.Descripcion FROM ( instituciones i  " +
        "INNER JOIN sedes s ON i.id = s.id_institucion) inner join tipos_institucion t" +
        " on i.tipo_institucion_id= t.id WHERE  " +
        "(s.id_municipio like ?) AND (? = 0 OR s.id_departamento = ?) " +
        "AND (? = 0 OR i.tipo_institucion_id = ?) AND  " +
        "i.estado = 'A' AND i.ESTADO_APROBACION ='A' AND s.estado='A'" +
        " group by i.ID,nombre_institucion,t.Descripcion ";

    /**
     * Parte de consulta para generar el grafico del portal de oferentes para
     * instituciones
     */
    private static final String COUNT_INSTITUTION_TYPE =
        "SELECT count(Distinct i.ID) cantidad, t.nombre  TIPO FROM  instituciones i " +
        "INNER JOIN sedes s ON i.id = s.id_institucion inner join tipos_institucion t" +
        " on i.tipo_institucion_id= t.id WHERE  (s.id_municipio like ?) AND " +
        "(? = 0 OR s.id_departamento = ?) AND (? = 0 OR i.tipo_institucion_id = ?) AND " +
        " i.estado = 'A' AND i.ESTADO_APROBACION='A' AND s.estado='A' group by  t.nombre";

    /**
     * Consulta para generar informacion de sedes segun seleccione el filtro del
     * portal
     *
     */
    private static final String FIND_HEADQUARTER_BY =
        "SELECT s.CODIGO ,i.NOMBRE_INSTITUCION,s.NOMBRE nombre_sede,s.DIRECCION," +
        "d.NOMBRE departamento,m.NOMBRE ciudad  FROM  SEDES s " +
        "INNER JOIN INSTITUCIONES i ON i.id = s.id_institucion " +
        "INNER JOIN DEPARTAMENTOS d ON s.id_departamento=d.ID " +
        "INNER JOIN MUNICIPIOS m ON s.id_municipio=m.ID " +
        "WHERE  ( s.id_municipio like ?) AND (? = 0 OR s.id_departamento = ?) " +
        "AND (? = 0 OR i.tipo_institucion_id = ?) " +
        "AND i.estado = 'A' AND  s.estado='A'  and i.ESTADO_APROBACION='A' ";
    /**
     * Parte de consulta para generar el grafico del portal de oferentes para
     * sedes
     */
    private static final String COUNT_HEADQUARTER =
        "SELECT  COUNT(s.id) cantidad_sedes,d.NOMBRE departamento  FROM  (SEDES s  " +
        "INNER JOIN INSTITUCIONES i ON i.id = s.id_institucion " +
        "INNER JOIN DEPARTAMENTOS d ON s.id_departamento=d.ID " +
        "INNER JOIN MUNICIPIOS m ON s.id_municipio=m.ID) " +
        "WHERE (s.id_municipio like ?) AND (? = 0 OR s.id_departamento = ?) " +
        "AND (? = 0 OR i.tipo_institucion_id = ?) " +
        "AND  i.estado = 'A' AND s.estado='A'  AND i.ESTADO_APROBACION='A'  group by d.NOMBRE ";

    private static final String FIND_PROGRAMS_BY =
        "SELECT  DISTINCT   " +
        "decode(cap.en_dominio,0,cap.codigo_programa,p.codigo) pcodigo, " +
        "decode(cap.en_dominio,0,cap.nombre_programa,p.nombre)  programa," +
        "   cap.duracion duraciont," +
        "  t.nombre tformacion," +
        "  f.NOMBRE ccf ," +
        "  d.nombre departamento," +
        "  mu.NOMBRE municipio," +
        "  cert.codigo ccalidad," +
        "  i.NOMBRE_INSTITUCION institucion," +
        "  s.NOMBRE sede,  " +
        "           cap.NOMBRE_MODULO modulo,   " +
        "           cap.FECHA_INICIO finicio," +
        "           cap.FECHA_FIN ffin,    " +
        "          cap.TIPO_HORARIO horario," +
        "          cap.duracion duracionm   ," +
        "          cap.tipo_curso , p.tipo_capacitacion " +
        "         FROM md_f_cap_programa_modulo cap     LEFT  JOIN programas p     " +
        "         ON cap.codigo_programa=p.codigo    " +
        "Left JOIN certificaciones cert on cert.id=" +
        "(decode(cap.en_dominio,0,TO_NUMBER(cap.NOMBRE_CERTIFICACION),TO_NUMBER(p.CERTIFICACION)))"+
        "         INNER JOIN sedes s on (s.codigo=cap.codigo_sede)      " +
        "         INNER JOIN instituciones i on (s.id_institucion=i.id )   " +
        "         INNER JOIN FUNDACIONES f on (f.codigo=cap.CODIGO_CCF or f.codigo=p.CODIGO_CCF)    " +
        "         INNER JOIN DEPARTAMENTOS d on (d.id=s.ID_DEPARTAMENTO )    " +
        "         INNER JOIN MUNICIPIOS mu on (mu.id=s.ID_MUNICIPIO)    " +
        "         INNER JOIN tipos_programa t ON ( decode(cap.en_dominio,0,cap.tipo_curso,p.tipo_capacitacion)=t.id)  " +
        "          WHERE  " +
        "         decode(cap.en_dominio,0,cap.tipo_curso,p.tipo_capacitacion) like ?  and" +
        "         CONVERT(LOWER (cap.nombre_modulo),'US7ASCII') like   CONVERT(LOWER ( ? ) ,'US7ASCII')  and    " +
        "         i.ESTADO='A' AND i.ESTADO_APROBACION='A' AND s.ESTADO='A' ";

    /**
     * Plantilla JDBC para consultar las propiedades.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * consulta de tipos de aprobadas por tipo y ciudad
     *
     * @param type
     * @param city
     * @param state
     * @return
     */
    @Override
    public List<Institution> getInstitutionsByTypeAndCityAndState(String type,
        String city, String state) {

        return this.jdbcTemplate.query(FIND_INSTITUTION_BY, new RowMapper() {
            @Override
            public Institution mapRow(ResultSet rs, int rownumber) throws
                SQLException {
                Institution e = new Institution();
                InstitutionType iType = new InstitutionType();
                iType.setDescription(rs.getString("DESCRIPCION"));

                e.setType(iType);
                e.setInstitutionName(rs.getString("NOMBRE_INSTITUCION"));
                e.setId(Long.parseLong(rs.getString("ID")));

                return e;
            }
        }, city, state, state, type, type);

    }

    @Override
    public List<GraphicDataDto> graphicInstitutionsByTypeAndCityAndState(
        String type, String city, String state) {

        return this.jdbcTemplate.query(COUNT_INSTITUTION_TYPE, new RowMapper() {
            @Override
            public GraphicDataDto mapRow(ResultSet rs, int rownumber) throws
                SQLException {
                GraphicDataDto g = new GraphicDataDto();
                g.setAxisX(rs.getString("TIPO"));
                g.setAxisY(rs.getInt("CANTIDAD"));

                return g;
            }
        }, city, state, state, type, type);

    }

    @Override
    public List<ValueListDto> getOrientationType() {

        return this.jdbcTemplate.query(FIND_ORIENTATION_TYPE, new RowMapper() {
            @Override
            public ValueListDto mapRow(ResultSet rs, int rownumber) throws
                SQLException {
                ValueListDto e = new ValueListDto();
                e.setCode(rs.getString("CODIGO"));
                e.setDescription(rs.getString("DESCRIPCION"));
                return e;
            }
        });

    }

    @Override
    public List<ValueListDto> getDuration() {

        return this.jdbcTemplate.query(FIND_DURATION, new RowMapper() {
            @Override
            public ValueListDto mapRow(ResultSet rs, int rownumber) throws
                SQLException {
                ValueListDto e = new ValueListDto();
                e.setCode(rs.getString("CODIGO"));
                e.setDescription(rs.getString("DURACION"));
                return e;
            }
        });
    }

    @Override
    public List<ValueListDto> getFormationType() {

        return this.jdbcTemplate.query(FIND_FORMATION_TYPE, new RowMapper() {
            @Override
            public ValueListDto mapRow(ResultSet rs, int rownumber) throws
                SQLException {
                ValueListDto e = new ValueListDto();
                e.setCode(rs.getString("ID"));
                e.setDescription(rs.getString("NOMBRE"));
                return e;
            }
        });
    }

    @Override
    public List<TableDataDto> getHeadquarterByTypeAndCityAndState(String type,
        String city, String state) {
        LOGGER.info("****SEDES******" + city + "*********" + state + type);

        return this.jdbcTemplate.query(FIND_HEADQUARTER_BY, new RowMapper() {
            @Override
            public TableDataDto mapRow(ResultSet rs, int rownumber) throws
                SQLException {
                TableDataDto dto = new TableDataDto();
                dto.addColumn(rs.getString("CODIGO"));
                dto.addColumn(rs.getString("NOMBRE_INSTITUCION"));
                dto.addColumn(rs.getString("NOMBRE_SEDE"));
                dto.addColumn(rs.getString("DIRECCION"));
                dto.addColumn(rs.getString("DEPARTAMENTO"));
                dto.addColumn(rs.getString("CIUDAD"));

                return dto;
            }
        }, city, state, state, type, type);

    }

    @Override
    public List<GraphicDataDto> graphicHeadquarterByTypeAndCityAndState(
        String type, String city, String state) {
        return this.jdbcTemplate.query(COUNT_HEADQUARTER, new RowMapper() {
            @Override
            public GraphicDataDto mapRow(ResultSet rs, int rownumber) throws
                SQLException {
                GraphicDataDto g = new GraphicDataDto();
                g.setAxisX(rs.getString("DEPARTAMENTO"));
                g.setAxisY(rs.getInt("CANTIDAD_SEDES"));

                return g;
            }
        }, city, state, state, type, type);

    }

    @Override
    public List<TableDataDto> getProgramByTypeAndDurationAndModule(
        String formationType, String duration, String module) {
        String query = FIND_PROGRAMS_BY;
        if ("10".equals(duration)) {
            query = FIND_PROGRAMS_BY.concat("  and ( cap.duracion< 10 )    ");
        } else if ("40".equals(duration)) {
            query = FIND_PROGRAMS_BY.concat(
                "  and ( cap.duracion > 10 and  cap.duracion < 40  )    ");
        } else if ("41".equals(duration)) {
            query = FIND_PROGRAMS_BY.concat("  and ( cap.duracion > 40   )    ");
        }
        LOGGER.info("QUERY  ****:" + query);

        return this.jdbcTemplate.query(query, new RowMapper() {
            @Override
            public TableDataDto mapRow(ResultSet rs, int rownumber)
                throws SQLException {
                TableDataDto dto = new TableDataDto();

                dto.addColumn(rs.getString("PCODIGO"));
                dto.addColumn(rs.getString("PROGRAMA"));
                dto.addColumn(rs.getString("DURACIONT"));
                dto.addColumn(rs.getString("TFORMACION"));
                dto.addColumn(rs.getString("CCF"));
                dto.addColumn(rs.getString("DEPARTAMENTO"));
                dto.addColumn(rs.getString("MUNICIPIO"));
                dto.addColumn(rs.getString("CCALIDAD"));
                dto.addColumn(rs.getString("INSTITUCION"));
                dto.addColumn(rs.getString("MODULO"));
                dto.addColumn(rs.getString("SEDE"));
                dto.addColumn(rs.getString("FINICIO"));
                dto.addColumn(rs.getString("FFIN"));
                dto.addColumn(rs.getString("HORARIO"));
                dto.addColumn(rs.getString("DURACIONM"));
                return dto;

            }

        }, formationType, module);

    }

}
