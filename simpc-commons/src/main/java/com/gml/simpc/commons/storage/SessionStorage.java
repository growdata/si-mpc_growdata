/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: SessionStorage.java
 * Created on: 2016/12/05, 03:55:30 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.commons.storage;

import com.gml.simpc.api.entity.Session;

/**
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public final class SessionStorage {

    /**
     * Objeto thread local.
     */
    public static final ThreadLocal<Session> THREAD = new ThreadLocal<>();

    /**
     * Constructor privado para impedir instacias de la clase.
     */
    private SessionStorage() {
    }

    /**
     * Adiciona el valor.
     *
     * @param valor
     *
     */
    public static void addValue(Session valor) {
        THREAD.set(valor);
    }

    /**
     * Elimina el valor del hilo, esto se hace porque es posible estar
     * trabajando en un pool de hilos reutilizables.
     */
    public static void removeValue() {
        THREAD.remove();
    }

    /**
     * Obtiene el valor.
     *
     * @return
     */
    public static Session getValue() {
        return THREAD.get();
    }
}
