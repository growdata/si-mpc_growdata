/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ResultBeneficEconomicPortalDto.java
 * Created on: 2016/12/20, 05:49:22 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 * Objective: map the required fields into a load of training or orientation
 * programs for detail data.
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

/**
 * Class to map resulto of portal benefic economic to training and 
 * orientation for load data.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Leonardo Rocha</a>
 */
public class ResultBeneficEconomicPortalDto {

    private String month;
    private String subsidyApplicants;
    private String requestsApproved;
    private String deniedApplications;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getSubsidyApplicants() {
        return subsidyApplicants;
    }

    public void setSubsidyApplicants(String subsidyApplicants) {
        this.subsidyApplicants = subsidyApplicants;
    }

    public String getRequestsApproved() {
        return requestsApproved;
    }

    public void setRequestsApproved(String requestsApproved) {
        this.requestsApproved = requestsApproved;
    }

    public String getDeniedApplications() {
        return deniedApplications;
    }

    public void setDeniedApplications(String deniedApplications) {
        this.deniedApplications = deniedApplications;
    }

    public ResultBeneficEconomicPortalDto(String month, String subsidyApplicants,
        String requestsApproved, String deniedApplications) {
        this.month = month;
        this.subsidyApplicants = subsidyApplicants;
        this.requestsApproved = requestsApproved;
        this.deniedApplications = deniedApplications;
    }

    @Override
    public String toString() {
        return "ResultBeneficEconomicPortalDto{" + "month=" + month +
            ", subsidyApplicants=" + subsidyApplicants + ", requestsApproved=" +
            requestsApproved + ", deniedApplications=" + deniedApplications +
            '}';
    }

}
