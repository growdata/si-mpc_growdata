/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IntersectionStatus.java
 * Created on: 2017/02/07, 04:34:44 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.enums;

/**
 * Enumeraci&oacute;n que define los estados de un cruce.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public enum IntersectionStatus {

    CREATED, LOADED, ERROR, DONE;
}
