/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: InstitutionService.java
 * Created on: 2016/10/19, 02:10:35 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.entity.LoadProcess;
import com.gml.simpc.api.entity.exeption.UserException;
import java.util.Date;
import java.util.List;

/**
 * Interface que define el servicio de los procesos de carga
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
public interface LoadProcessService {

    /**
     * Servicio para guardar un proceso
     *
     * @param loadProcess
     *
     * @throws com.gml.simpc.api.entity.exeption.UserException
     */
    void save(LoadProcess loadProcess) throws UserException;

    /**
     * Servicio para actualizar programas
     *
     * @param loadProcess
     */
    void update(LoadProcess loadProcess);

    /**
     * Elimina un proceso
     *
     * @param loadProcess
     */
    void remove(LoadProcess loadProcess);

    /**
     * Trae todos los procesos
     *
     * @return
     */
    List<LoadProcess> getAll();

    /**
     * Trae un programa por el id
     *
     * @param id
     *
     * @return
     */
    LoadProcess findById(Long id);

    /**
     * Trae los registros en un rango de fechas y por tipo de archivo
     *
     * @param start
     * @param end
     * @param fileId
     *
     * @return
     */
    List<LoadProcess> findByDateAndFile(Date start, Date end, String fileId,
        String ccf);

    /**
     * Trae los registros en un perido y tipo de archivo
     *
     * @param month
     * @param year
     * @param file
     *
     * @return
     */
    List<LoadProcess> findByperiodAndFile(int year, String month, String file,
        String consultant);

    /**
     * Trae el primer registro por fecha de inicio y estado estructura_validada
     *
     * @return
     */
    LoadProcess findFirstValidated();

    /**
     * Trae carga por cabecera.
     *
     * @param header
     *
     * @return
     */
    LoadProcess getLoadProcessByHeader(String header);
}
