<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Banco de Oferentes</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster.bundle.min.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster-sideTip-shadow.min.css" rel="stylesheet"/>
        <link href="resources/css/morris.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
    </head>

    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <div class="col-sm-3" style="padding:0px;">
                        <img src="resources/images/logo_mpc.png" width="100%" alt=""/>
                    </div>
                    <div class="col-sm-8">
                        <a class="navbar-brand" href="/masterdata/index.htm">Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</a>
                    </div>
                </div>
                <div class="navbar-nav navbar-form credenciales pull-right">
                    <table>
                        <tr>
                            <td>
                                <strong>Bienvenido</strong> 
                            </td>
                            <td style="width: 120px; text-align: right">
                                <a id="" href="/masterdata/index.htm" class="btn btn-warning btn-xs">Volver al inicio</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </nav>
        <div class="container">
            <h1>Banco de Oferentes</h1>


            <!-- main area -->
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Filtros de B&uacute;squeda</h2></div>
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li><a data-toggle="tab" href="#instituciones" id="tabInstituciones" onclick="borraGrafico()">Instituciones</a></li>
                        <li><a data-toggle="tab" href="#sedes"  id="tabSedes" onclick="borraGrafico()">Sedes</a></li>
                        <li><a data-toggle="tab" href="#programas" id="tabProgramas" onclick="borraGrafico()">Programas</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="alert-area" ></div>
                        <div id="instituciones" class="tab-pane fade in ">


                            <div class="panel-body">
                                <form:form  action="/masterdata/bancoInstituciones.htm" method="GET"  >

                                    <input type="hidden" id="activateBtn" value="${activateBtn}">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="tipoInstitucionLabel" class="control-label">Tipo de Instituci&oacute;n</label>     
                                            <select class="form-control" name="tipoInstitucion" id="tipoInstitucion" > 
                                                <option value='0' label="Seleccione ..." selected="selected">Seleccione ...</option>
                                                <c:if test="${not empty institutionTypes}">     
                                                    <c:forEach items="${institutionTypes}" var="item" begin="0">

                                                        <option <c:if test="${item.id eq instSelected}">selected="selected"</c:if>  value="${item.id}">${item.name}</option>                                     
                                                    </c:forEach>  
                                                </c:if>
                                            </select>   
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="departamentoLabel" class="control-label">Departamento</label>
                                            <select onchange="activaMunicipio()" class="form-control" id="departamentoModal" name="departamentoModal">
                                                <option value="0">Todos los Departamentos</option>
                                            </select>
                                            <div class="clearfix"></div>                   
                                        </div>
                                    </div>
                                    <div class="col-sm-4 activaMunicipio">
                                        <div class="form-group">
                                            <label for="municipioSedeLabel" class="control-label">Municipio</label>
                                            <select class="form-control" id="municipioModal" name="municipioModal" onchange="setTransCityId()" >
                                            </select>

                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12" style="text-align:right">
                                        <button type="submit" class="btn btn-success">Buscar</button>    
                                    </div>


                                </form:form>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading"><h2>Resultado de la B&uacute;squeda</h2></div>
                                <div class="panel-body">

                                    <div class=" table-responsive col-md-12">
                                        <center>
                                            <button type="button" class="btn btn-default desableBtn"
                                                    data-toggle="modal" data-target="#graphicModal">  
                                                <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                                                Ver Gr&aacute;fico
                                            </button>
                                        </center>
                                        <table id="tabla1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>C&oacute;digo Instituci&oacute;n</th>
                                                    <th>Nombre de la Instituci&oacute;n</th>
                                                    <th>Tipo de Instituci&oacute;n</th>

                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Codigo Instituci&oacute;n</th>
                                                    <th>Nombre de la Instituci&oacute;n</th>
                                                    <th>Tipo de Instituci&oacute;n</th>

                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <c:if test="${not empty institutionList}">
                                                    <c:forEach items="${institutionList}" var="institucion" begin="0">

                                                        <tr> 
                                                            <td>${institucion.getId()}</td>
                                                            <td>${institucion.getInstitutionName()}</td>
                                                            <td>${institucion.type.description}</td>

                                                        </tr>   
                                                    </c:forEach>  
                                                </c:if>
                                            </tbody> 
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="sedes" class="tab-pane fade">
                            <div class="panel-body">
                                <form id="sedesForm" name="sedesOferentes" action="/masterdata/bancoSedes.htm" >


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="tipoInstitucionLabel" class="control-label">Tipo de Instituci&oacute;n</label>
                                            <select class="form-control" name="tipoInstitucion" id="tipoInstitucion" > 
                                                <option value='0' label="Seleccione..." selected>Seleccione...</option>
                                                <c:if test="${not empty institutionTypes}">     
                                                    <c:forEach items="${institutionTypes}" var="item" begin="0">

                                                        <option <c:if test="${item.id eq instSelected}">selected="selected"</c:if> value="${item.id}">${item.name}</option>                                     
                                                    </c:forEach>  
                                                </c:if>
                                            </select>   
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="departamentoLabel" class="control-label">Departamento</label>
                                            <select onchange="activaMunicipio2()" class="form-control" id="departamentoModal2" name="departamentoModal2">
                                                <option value="0">Todos los Departamentos</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group activaMunicipio2">
                                            <label for="municipioSedeLabel" class="control-label">Municipio</label>
                                            <hidden id="transCityId" name="transCityId" val=""/>
                                            <select class="form-control" id="municipioModal2" name="municipioModal2" >
                                            </select>
                                        </div>


                                    </div>   
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12" style="text-align:right">
                                        <button type="submit" class="btn btn-success">Buscar</button>    
                                    </div>
                                </form>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading"><h2>Resultado de la B&uacute;squeda</h2></div>
                                <div class="panel-body">

                                    <div class=" table-responsive col-md-12">
                                        <center>
                                            <button type="button" class="btn btn-default desableBtn"
                                                    data-toggle="modal" data-target="#graphicModal">  
                                                <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                                                Ver gr&aacute;fico
                                            </button>
                                        </center>
                                        <table id="tabla2" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Codigo de la sede</th>
                                                    <th>Nombre de la Instituci&oacute;n</th>
                                                    <th>Nombre de la sede</th>
                                                    <th>Direcci&oacute;n</th>
                                                    <th>Departamento</th>
                                                    <th>Municipio</th>

                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Codigo de la sede</th>
                                                    <th>Nombre de la Instituci&oacute;n</th>
                                                    <th>Nombre de la sede</th>
                                                    <th>Direcci&oacute;n</th>
                                                    <th>Departamento</th>
                                                    <th>Municipio</th>

                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <c:if test="${not empty headQuarterList}">
                                                    <c:forEach items="${headQuarterList}" var="sede" begin="0">

                                                        <tr> 
                                                            <td>${sede.getColumn(0)}</td>
                                                            <td>${sede.getColumn(1)}</td>
                                                            <td>${sede.getColumn(2)}</td>
                                                            <td>${sede.getColumn(3)}</td>
                                                            <td>${sede.getColumn(4)}</td>
                                                            <td>${sede.getColumn(5)}</td>

                                                        </tr>   
                                                    </c:forEach>  
                                                </c:if>
                                            </tbody> 

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="programas" class="tab-pane fade ">
                            <div class="panel-body">
                                <legend>           

                                    <form id="programasForm" name="programasOferentes" action="bancoProgramas.htm" >
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="tipoFormacionLabel" class="control-label">Tipo de Formaci&oacute;n</label>     
                                                <select class="form-control" id="fomation" name="formacion" > 
                                                    <option value='' label="Seleccione..." selected>Seleccione...</option>
                                                    <c:if test="${not empty formationType}">
                                                        <c:forEach items="${formationType}" var="item" begin="0">

                                                            <option <c:if test="${item.id eq formationSelected}">selected="selected"</c:if> value="${item.id}">${item.description}</option>                                     
                                                        </c:forEach>  
                                                    </c:if>
                                                </select>   
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="DuracionLabel" class="control-label">Duraci&oacute;n</label>
                                                <select class="form-control" id="duracion" name="duracion"  > 
                                                    <option <c:if test="${durationSelected == ''}">selected="selected"</c:if>value='' label="Todos" selected></option>
                                                    <option <c:if test="${durationSelected == '10'}">selected="selected"</c:if> value='10' label="menos de 10 Horas" ></option>
                                                    <option <c:if test="${durationSelected == '40'}">selected="selected"</c:if>value='40' label="  entre  10  y 40 Horas" ></option>
                                                    <option <c:if test="${durationSelected == '41'}">selected="selected"</c:if>value='41' label="Mayor a 40" ></option>

                                                    </select> 

                                                </div>
                                            </div>

                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="nombreModuloLabel" class="control-label">Nombre  del M&oacute;dulo </label>
                                                    <input value="<c:out value="${nameSelected}"/>" type="text" id="nombreModulo" name="nombreModulo" class="form-control"  placeholder="Escriba aqui nombre del M&oacute;dulo">

                                            </div>

                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-sm-12" style="text-align:right">
                                            <button type="submit" class="btn btn-success">Buscar</button>    
                                        </div>
                                    </form>

                                </legend>



                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading"><h2>Resultado de la B&uacute;squeda</h2></div>
                                <div class="panel-body">


                                    <div class=" table-responsive col-md-12">
                                        <table id="tabla3" class="table table-striped table-bordered" cellspacing="0" width="100%">

                                            <thead>
                                                <tr>
                                                    <th>C&oacute;digo del programa</th>
                                                    <th>Nombre del programa</th>
                                                    <th>Duraci&oacute;n total en horas</th>
                                                    <th>Tipo de Formaci&oacute;n</th>
                                                    <th>CCF</th>
                                                    <th>Departamento</th>
                                                    <th>Municipio</th>
                                                    <th>Certificaci&oacute;n de Calidad</th>
                                                    <th>Intituci&oacute;n</th>

                                                    <th>M&oacute;dulo</th>
                                                    <th>Sede</th>
                                                    <th>Fecha Inicio</th>
                                                    <th>Fecha Final</th>
                                                    <th>Horario</th>
                                                    <th>Duraci&oacute;n del M&oacute;dulo</th>


                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>C&oacute;digo del programa</th>
                                                    <th>Nombre del programa</th>
                                                    <th>Duraci&oacute;n total en horas</th>
                                                    <th>Tipo de Formaci&oacute;n</th>
                                                    <th>CCF</th>
                                                    <th>Departamento</th>
                                                    <th>Municipio</th>
                                                    <th>Certificaci&oacute;n de Calidad</th>
                                                    <th>Intituci&oacute;n</th>

                                                    <th>M&oacute;dulo</th>
                                                    <th>Sede</th>
                                                    <th>Fecha Inicio</th>
                                                    <th>Fecha Final</th>
                                                    <th>Horario</th>
                                                    <th>Duraci&oacute;n del M&oacute;dulo</th>


                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <c:if test="${not empty programsList}">
                                                    <c:forEach items="${programsList}" var="programa" begin="0">

                                                        <tr> 
                                                            <td>${programa.getColumn(0)}</td>
                                                            <td>${programa.getColumn(1)}</td>
                                                            <td align="right">${programa.getColumn(2)}</td>
                                                            <td>${programa.getColumn(3)}</td>
                                                            <td>${programa.getColumn(4)}</td>
                                                            <td>${programa.getColumn(5)}</td>
                                                            <td>${programa.getColumn(6)}</td>
                                                            <td>${programa.getColumn(7)}</td>
                                                            <td>${programa.getColumn(8)}</td>  
                                                            <td>${programa.getColumn(9)}</td>
                                                            <td>${programa.getColumn(10)}</td>

                                                            <td>${programa.getColumn(11)}</td>
                                                            <td>${programa.getColumn(12)}</td>
                                                            <td align="right">${programa.getColumn(13)}</td>
                                                            <td align="right">${programa.getColumn(14)}</td>


                                                        </tr>   
                                                    </c:forEach>  
                                                </c:if>
                                            </tbody> 
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>





                </div>   

            </div><!-- /.col-xs-12 main -->


            <div class="clearfix"></div> 

            <div class="modal fade" id="graphicModal" tabindex="-1" role="dialog" aria-labelledby="graphicModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12  " id="graphicContainer2">
                                    <h4 class="modal-title" id="graphicModalLabel"><Strong><center>${titulo}</center></Strong></h4>
                                    <div id="graphicContainer" ></div>
                                    <br>


                                    <div id="legend" class="graphic-legend inline-block"></div>
                                    <br> <br> <br>
                                    <Strong> ${titulografica}</Strong>
                                    <div class=" table-responsive col-md-12">
                                        <table id="tablag" class=" table-striped table-bordered " cellspacing="0" width="100%">
                                            <thead>

                                                <tr>
                                                    <c:if test="${not empty titleList}">
                                                        <c:forEach items="${titleList}" var="info" begin="0">

                                                            <th>${info}</th>

                                                        </c:forEach>  
                                                    </c:if>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <c:if test="${not empty graphicList}">
                                                    <c:forEach items="${graphicList}" var="info" begin="0">

                                                        <tr> 
                                                            <td>${info.axisX}</td>
                                                            <td>${info.axisY}</td>


                                                        </tr>   
                                                    </c:forEach>  
                                                </c:if>

                                            </tbody> 
                                        </table>
                                    </div>

                                </div>

                            </div>
                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" onclick="printGraphic()">Exportar PDF</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--Footer -->
            <div class="footer">
                <img src="resources/images/bg_banderin.jpg" width="100%"/> 
                <div class="clearfix"></div> 
            </div>
            <!--Ends footer -->

            <!-- jQuery -->
            <script src="resources/js/jquery-1.12.3.js"></script>
            <script src="resources/js/jquery.dataTables.min.js"></script>
            <script src="resources/js/bootstrap.min.js"></script>
            <script src="resources/js/dataTables.buttons.min.js"></script>
            <script src="resources/js/buttons.flash.min.js"></script>
            <script src="resources/js/jszip.min.js"></script>
            <script src="resources/js/pdfmake.min.js"></script>
            <script src="resources/js/vfs_fonts.js"></script>
            <script src="resources/js/buttons.html5.min.js"></script>
            <script src="resources/js/buttons.print.min.js"></script>
            <script src="resources/js/dataTables.bootstrap.min.js"></script>
            <script src="resources/js/bootstrap-datepicker.js"></script>
            <script src="resources/js/bootstrap-confirmation.js"></script>
            <script src="resources/js/jquery.validate.min.js"></script>
            <script src="resources/js/tooltipster.bundle.js"></script>
            <script src="resources/js/custom-functionalities.js"></script>
            <script src="resources/js/raphael.min.js"></script>
            <script src="resources/js/morris.js"></script>
            <script src="resources/js/xepOnline.js"></script>


            <script>

                                $(document).ready(function () {

                                    $(${tab}).tab('show');
                                    
                                    if($("#activateBtn").val() === ""){
                                        $(".desableBtn").attr("disabled", "desabled");
                                    } else {
                                        $(".desableBtn").removeAttr("disabled");
                                    }

                                    loadStates("departamentoModal", "municipioModal");
                                    loadCitiesByState("departamentoModal", "municipioModal");
                                    loadStates("departamentoModal2", "municipioModal2");
                                    loadCitiesByState("departamentoModal2", "municipioModal2");

                                    $('#departamentoModal > option[value="${stateSelected}"]').attr('selected', 'selected');
                                    $('#departamentoModal2 > option[value="${stateSelected}"]').attr('selected', 'selected');


                                    $('.table').DataTable({
                                        "language": {
                                            "url": "resources/js/Spanish.json"
                                        },
                                        "initComplete": function (settings, json) {
                                            $(".dt-buttons").each(function (index) {
                                                console.log(index + ": " + $(this).text());
                                                if ($(this).find('.exportar').length === 0) {
                                                    $(this).prepend('<label class="exportar">Exportar a:&nbsp;&nbsp;</label>');
                                                }
                                            });
                                        },
                                        "bFilter": true,
                                        "bInfo": false,
                                        dom: 'Blfrtip',
                                        buttons: [
                                            {
                                                extend: 'csv',
                                                fieldBoundary: '',
                                                text: '<i class="fa fa-file-text-o"></i>',
                                                titleAttr: 'CSV'

                                            },
                                            {
                                                extend: 'excel',
                                                text: '<i class="fa fa-file-excel-o"></i>',
                                                titleAttr: 'EXCEL'
                                            },
                                            {
                                                extend: 'pdf',
                                                text: '<i class="fa fa-file-pdf-o"></i>',
                                                titleAttr: 'PDF'

                                            },
                                            {
                                                extend: 'print',
                                                text: '<i class="fa fa-files-o"></i>',
                                                titleAttr: 'COPY'
                                            }

                                        ]

                                    });

                                    $('#tablag').DataTable({
                                        "language": {
                                            "url": "resources/js/Spanish.json"
                                        },
                                        dom: 'lt',
                                    });
                <c:if test="${not empty internalMsg}">
                                    $('#creationModal').modal('show');
                </c:if>

                                    $('#graphicModal').on('shown.bs.modal', function () {
                                        $(function () {
                                            var json = '<c:out value="${graphicData}" escapeXml="false"/>';
                                            var type = '<c:out value="${graphicType}" escapeXml="false"/>';
                                            var ykeys = [];
                                            var labels = [];

                                            var barColors = [];
                <c:forEach items="${yKeys}" var="yKey" begin="0">
                                            ykeys.push('<c:out value="${yKey}" escapeXml="false"/>');
                </c:forEach>
                <c:forEach items="${labels}" var="labels" begin="0">
                                            labels.push('<c:out value="${labels}" escapeXml="false"/>');
                </c:forEach>
                <c:forEach items="${barColors}" var="barColors" begin="0">
                                            barColors.push('<c:out value="${barColors}" escapeXml="false"/>');
                </c:forEach>
                                            $("#graphicContainer").empty();
                                            $('#legend').empty();

                                            if (type === 'B') {
var y='<c:out value="${xKey}" escapeXml="false"/>';
                                                var browsersChart = Morris.Bar({
                                                    element: 'graphicContainer',
                                                    data: jQuery.parseJSON(json),
                                                    xkey: '<c:out value="${xKey}" escapeXml="false"/>',
                                                    ykeys: ykeys,
                                                    labels: labels,
                                                    stacked: true,
                                                    hideHover: 'auto',
                                                    barGap: 4,
                                                    barColors: barColors,
                                                    barSizeRatio: 0.55,
                                                    xLabelAngle: 45,
                                                    resize: true,
                                                    yLabelFormat: function(y){return y !== Math.round(y)?'':y;}
                                                });
                                                browsersChart.options.data.forEach(function (labels, i) {
                                                    var legendItem = $('<span></span>').text(browsersChart.options.labels[i]).prepend('<br><span>&nbsp;</span>');
                                                    legendItem.find('span')
                                                            .css('backgroundColor', browsersChart.options.barColors[i])
                                                            .css('width', '20px')
                                                            .css('display', 'inline-block')
                                                            .css('margin', '5px');
                                                    $('#legend').append(legendItem)
                                                });


                                            } else if (type === 'L') {
                                                Morris.Line({
                                                    element: 'graphicContainer',
                                                    data: jQuery.parseJSON(json),
                                                    xkey: 'axisX',
                                                    ykeys: ['axisY'],
                                                    labels: ['A�os'],
                                                    stacked: true,
                                                    resize: true
                                                });
                                            } else {
                                                Morris.Donut({
                                                    element: 'graphicContainer',
                                                    data: jQuery.parseJSON(json),
                                                    stacked: true,
                                                    resize: true
                                                });
                                            }
                                        });
                                    });

                                    loadCitiesAndSelect($("#departamentoModal").val(),"departamentoModal", "municipioModal","transCityId");
                                    loadCitiesAndSelect($("#departamentoModal2").val(),"departamentoModal2", "municipioModal2","transCityId");
                                    $('#municipioModal > option[value="${citySelected}"]').attr('selected', 'selected');
                                    $('#municipioModal2 > option[value="${citySelected}"]').attr('selected', 'selected');
                                });

            
            function loadCitiesAndSelect(stateCode, deptoId, mnpoId,transId){
                var transValue = $("#"+transId).val();
                jQuery("#" + mnpoId).find('option').remove();
                jQuery("<option>").attr("value", '').
                        text('Seleccione...').appendTo("#" + mnpoId);
                <c:if test="${not empty citiesList}">
                    <c:forEach items="${citiesList}" var="item" >
                        var code = "<c:out value="${item.state.id}"/>";
                        if (code === stateCode) {
                            if ('${item.id}' === transValue) {
                               jQuery("<option>").attr("value", '${item.id}').
                                    attr("selected", "false").
                                    text('${item.name}').appendTo("#" + mnpoId);
                            }  
                            if ('${item.id}' != transValue) {
                               jQuery("<option>").attr("value", '${item.id}').
                                    text('${item.name}').appendTo("#" + mnpoId);
                            }  
                            
                        }
                    </c:forEach>
                </c:if>
            }

                                function loadStates(deptoId, mnpoId) {
                                    
                <c:if test="${not empty stateList}">
                    <c:forEach var="item" items="${stateList}" varStatus="iter">
                        <c:if test="${iter.index == 0}">
                                jQuery("<option>").attr("value", '${item.id}').
                                        text('${item.name}').appendTo("#" + deptoId);

                                loadCities("${item.id}", mnpoId);
                        </c:if>
                        <c:if test="${iter.index > 0}">
                                jQuery("<option>").attr("value", '${item.id}').
                                        text('${item.name}').appendTo("#" + deptoId);
                                $('#municipioModal > option[value="${citySelected}"]').attr('selected', 'selected');
                        </c:if>
                    </c:forEach>
                </c:if>
                                }

                                function loadCitiesByState(deptoId, mnpoId) {
                                    jQuery("#" + deptoId).change(function () {
                                        jQuery("#" + mnpoId).html('');
                                        var stateCode = jQuery("#" + deptoId).val();

                                        loadCities(stateCode, mnpoId);
                                    });
                                }

                                function loadCities(stateCode, mnpoId) {
                                    jQuery("#" + mnpoId).find('option').remove();
                                    jQuery("<option>").attr("value", '').
                                            text('Seleccione...').appendTo("#" + mnpoId);
                <c:if test="${not empty citiesList}">
                    <c:forEach items="${citiesList}" var="item" >
                                    var code = "<c:out value="${item.state.id}"/>";
                                    if (code === stateCode) {
                        <c:if test="${not empty creationInstitution.headquarter.city.id}">
                            <c:if test="${item.id == creationInstitution.headquarter.city.id}">
                                        jQuery("<option>").attr("value", '${item.id}').text('${item.name}').
                                                appendTo("#" + mnpoId);
                            </c:if>
                            <c:if test="${item.id != creationInstitution.headquarter.city.id}">
                                        jQuery("<option>").attr("value", '${item.id}').
                                                text('${item.name}').appendTo("#" + mnpoId);
                            </c:if>
                        </c:if>
                        <c:if test="${empty creationInstitution.headquarter.city.id}">
                                        jQuery("<option>").attr("value", '${item.id}').
                                                text('${item.name}').appendTo("#" + mnpoId);
                        </c:if>
                                    }
                    </c:forEach>
                </c:if>
                                }

                                function activaMunicipio() {
                                    $('.activaMunicipio').show();
                                }

                                function activaMunicipio2() {
                                    $('.activaMunicipio2').show();
                                }

                                function borraGrafico() {
                                    $("#graphicContainer2").empty();
                                    $(".desableBtn").attr("disabled", "desabled");
                                }

                                /**
                                 * Imprime el grafico en un pdf.
                                 */
                                function printGraphic() {
                                    var objeto = document.getElementById('graphicContainer2');  //obtenemos el objeto a imprimir
                                    var ventana = window.open('', '_blank');  //abrimos una ventana vac�a nueva
                                    ventana.document.write(objeto.innerHTML);  //imprimimos el HTML del objeto en la nueva ventana
                                    ventana.document.close();  //cerramos el documento
                                    ventana.print();  //imprimimos la ventana
                                    ventana.close();  //cerramos la ventana

                                }

            </script>
            <style>
                .buttons-csv{
                    display: inline-block;
                    background-image:url(resources/images/csv.png);
                    cursor:pointer !important;
                    width: 32px !important;
                    height: 32px !important;
                    border: none !important;

                }
                .buttons-csv span{
                    opacity: 0;
                }
                .buttons-excel{
                    display: inline-block;
                    background-image:url(resources/images/excel.png);
                    cursor:pointer !important;
                    width: 32px !important;
                    height: 32px !important;
                    border: none !important;

                }
                .buttons-excel span{
                    opacity: 0;
                }
                .buttons-pdf{
                    display: inline-block;
                    background-image:url(resources/images/pdf.png);
                    cursor:pointer !important;
                    width: 32px !important;
                    height: 32px !important;
                    border: none !important;

                }
                .buttons-pdf span{
                    opacity: 0;
                }
                .buttons-print{
                    display: inline-block;
                    background-image:url(resources/images/print.png);
                    cursor:pointer !important;
                    width: 32px !important;
                    height: 32px !important;
                    border: none !important;



                }
                .buttons-print span{
                    opacity: 0;

                }
                div.dt-buttons {
                    float: right;
                    margin-left:10px;


                }
                .graphic-legend > span {
                    display: inline-block;
                    margin-right: 25px;
                    margin-bottom: 10px;
                    font-size: 13px;


                }
                .graphic-legend > span:last-child {
                    margin-right: 0;
                }

                #graphicContainer {
                    max-height: 280px;
                    margin-top: 20px;
                    margin-bottom: 20px;


                }


                .graphic-legend > span > i {
                    display: inline-block;
                    width: 15px;
                    height: 15px;
                    margin-right: 7px;
                    margin-top: -3px;
                    vertical-align: middle;
                    border-radius: 1px;
                }

                #browsers_chart {
                    max-height: 280px;
                    margin-top: 20px;
                    margin-bottom: 20px;
                }

            </style>


    </body>
</html>
