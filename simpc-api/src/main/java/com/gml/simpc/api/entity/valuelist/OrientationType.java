/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ProgramType.java
 * Created on: 2016/10/12, 11:47:45 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity.valuelist;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad que representa la instituci&oacute;n.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Table(name = "TIPOS_ORIENTACION")
@Entity
public class OrientationType implements Serializable {

    /**
     * ID del tipo de ORIENTACION
     */
    @Id
    @Column(name = "ID", length = 10)
    private Long id;
    /**
     * nombre del TIPO DE ORIENTACION
     */
    @Column(name = "CODIGO", length = 50)
    private String code;

    /**
     * descripci&oacute;no
     */
    @Column(name = "DESCRIPCION", length = 200)
    private String description;

    /**
     * Constructor por defecto.
     */
    public OrientationType() {
        this.id = null;
    }
    /**
     * Constructor por id y codigo.
     *
     * @param name
     */
    public OrientationType(Long id, String code) {
        this.id = id;
        this.code = code;
    }

    /**
     * Constructor por codigo
     *
     * @param code
     */
    public OrientationType(String code) {
        this.code = code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Tipo de Orientaci�n{" + "id=" + id + ", c�digo=" + code +
            ", Descripci�n=" + description + '}';
    }


    

}
