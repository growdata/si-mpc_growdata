<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css/morris.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
    </head>
    <body >
        <c:if test="${not empty msg}">
            <br/>
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
            <br/>
            <br/>
        <div id="buttonsDiv" class="col-sm-offset-4 col-sm-8" style="text-align:right">
             <a href="indicadores.htm" type="submit" class="btn btn-success">Volver a selecci�n de indicador</a>
            <br/>
            <br/>
        </div>  
        <div class="clearfix"></div>
        <div class="panel panel-default">
            <c:choose> 
                <c:when test="${( empty graphicList) and ( empty dataPie)}">
                    <div class="row">
                    </div>
                    <h1 class="modal-title" id="graphicModalLabel"><Strong><center>${titulo}</center></Strong></h1>
                    <h4><center> <Strong> ${titulografica}</Strong> </center></h4>
                    <h4><center> <Strong>Periodo: ${rango}</Strong> </center></h4>
                    <br/>
                    <br/>
                    <h4><center><strong><font color="blue">No hay datos para este indicador en el rango seleccionado</font></strong></center></h4><br>
                </c:when>
                <c:otherwise>
                    <div  id="graphicContainer2">
                        <br/>
                        <br/>
                        <h1 class="modal-title" id="graphicModalLabel"><Strong><center>${titulo}</center></Strong></h1>
                        <h4><center> <Strong> ${titulografica}</Strong> </center></h4>
                        <h4><center> <Strong>Periodo: ${rango}</Strong> </center></h4>
                        <br/>
                        <br/>
                        <div id="graphicContainer"></div>
                        <br>
                        <div id="legend" class="graphic-legend inline-block"></div>
                        <br/>
                        <br/>
                        <div style="margin-left:15px"><font size="3">Unidad de Medida: ${unidadMedida}</font></div>
                        <br/>
                        <div class=" table-responsive col-md-12">
                            <table id="tablag" class=" table-striped table-bordered " cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <c:if test="${not empty titleList}">
                                            <c:forEach items="${titleList}" var="info" begin="0">
                                                <th   align="left">${info}</th>
                                            </c:forEach>  
                                        </c:if>
                                    </tr>
                                </thead>

                                <tbody>
                                    <c:if test="${not empty graphicList}">
                                        <c:forEach items="${graphicList}" var="grafico" begin="0">
                                            <tr> 
                                                <td  align="left">${grafico.axisX}</td>
                                                <td  align="right">${grafico.axisY}</td>
                                            </tr> 
                                        </c:forEach> 
                                    </c:if>
                                    <c:if test="${not empty dataPie}">
                                        <div id="legend" class="graphic-legend inline-block"></div>
                                        <c:forEach items="${dataPie}" var="grafico" begin="0">
                                            <tr> 
                                                <td  align="left">${grafico.label}</td>
                                                <td  align="right">${grafico.value}</td>
                                            </tr> 
                                        </c:forEach> 
                                    </c:if>
                                </tbody> 
                            </table>
                        </div>
                    </div>
                    <center><button type="button" class="btn btn-success" onclick="printGraphic()">Exportar PDF</button></center>
                </c:otherwise>
            </c:choose> 
        </div>

        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/dataTables.buttons.min.js"></script>
        <script src="resources/js/buttons.flash.min.js"></script>
        <script src="resources/js/jszip.min.js"></script>
        <script src="resources/js/pdfmake.min.js"></script>
        <script src="resources/js/vfs_fonts.js"></script>
        <script src="resources/js/buttons.html5.min.js"></script>
        <script src="resources/js/buttons.print.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/raphael.min.js"></script>
        <script src="resources/js/morris.js"></script>
        <script src="resources/js/xepOnline.js"></script>
        <script>
            
            $('#tablag').DataTable({
                    "language": {
                        "url": "resources/js/Spanish.json"
                    },
                    "bFilter": false,
                    "bInfo": false,
                    dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'csv',
                        fieldBoundary: '',
                        text: '<i class="fa fa-file-text-o"></i>',
                        titleAttr: 'CSV'

                    },
                    {
                        extend: 'excel',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        titleAttr: 'EXCEL'
                    },
                    {
                        extend: 'pdf',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        titleAttr: 'PDF'

                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-files-o"></i>',
                        titleAttr: 'COPY'
                    }

                ]
                    
                });
            
            

                $(function ()
                {

                    var json = '<c:out value="${graphicData}" escapeXml="false"/>';
                    var type = '<c:out value="${graphic}" escapeXml="false"/>';
                    var y = '<c:out value="${xKey}" escapeXml="false"/>';
                    var ykeys = [];
                    var labels = [];

            <c:forEach items="${yKeys}" var="yKey" begin="0">
                    ykeys.push('<c:out value="${yKey}" escapeXml="false"/>');
            </c:forEach>
            <c:forEach items="${labels}" var="labels" begin="0">
                    labels.push('<c:out value="${labels}" escapeXml="false"/>');
            </c:forEach>


                    if (type === 'D') {
                       var browsersChart =  Morris.Donut({
                            element: 'graphicContainer',
                            data: jQuery.parseJSON(json),
                             colors:['blue','Turquoise','Crimson','green','yellow','#7FFFD4','PURPLE',
                                    'indigo','red','pink','Lavender', 'Cyan','violet'],
                            formatter: function (value, data) {
                                var numero = ((value * 100) /${totalpie});
                                return  Number(numero.toFixed(2)) + '%';
                            }
                        });
                        
                         browsersChart.options.data.forEach(function (data, i) {
                                                var legendItem = $('<span></span>').text((browsersChart.options.data[i].label)+" - "+((browsersChart.options.data[i].value*100/${totalpie}).toFixed(2))+"%").prepend('<br><span>&nbsp;</span>');
                                                legendItem.find('span')
                                                        .css('backgroundColor', browsersChart.options.colors[i])
                                                        .css('width', '20px')
                                                        .css('display', 'inline-block')
                                                        .css('margin', '5px');
                                                $('#legend').append(legendItem);
                                            });
                    }

                    if (type === 'B') {
                        Morris.Bar({
                            element: 'graphicContainer',
                            data: jQuery.parseJSON(json),
                            xkey: 'axisX',
                            ykeys: ykeys,
                            labels: labels,
                            stacked: true,
                            hideHover: 'auto',
                            barGap: 2,
                            barSizeRatio: 0.75,
                            xLabelAngle: 75,
                            resize: true,
                            gridTextSize: 10,
                            yLabelFormat: function (y) {
                                return y !== Math.round(y) ? '' : y;
                            }
                            
                        });
                    }

                    if (type === 'L') {
                        Morris.Line({
                            element: 'graphicContainer',
                            data: jQuery.parseJSON(json),
                            xkey: 'axisX',
                            ykeys: ykeys,
                            labels: labels,
                            stacked: true,
                            hideHover: 'auto',
                            barGap: 4,
                            barSizeRatio: 0.55,
                            resize: true,
                            xLabelAngle: 20,
                            yLabelFormat: function (y) {
                                return y !== Math.round(y) ? '' : y;
                            },
                            parseTime: false
                        });
                    }

                });



                /**
                 * Imprime el grafico en un pdf.
                 */
                function printGraphic() {
                    var objeto = document.getElementById('graphicContainer2'); //obtenemos el objeto a imprimir
                    var ventana = window.open('', '_blank'); //abrimos una ventana vac�a nueva
                    ventana.document.write(objeto.innerHTML); //imprimimos el HTML del objeto en la nueva ventana
                    ventana.document.close(); //cerramos el documento
                    ventana.print(); //imprimimos la ventana
                    ventana.close(); //cerramos la ventana
                }
        </script>
        <style>
            .buttons-csv{
                display: inline-block;
                background-image:url(resources/images/csv.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-csv span{
                opacity: 0;
            }
            .buttons-excel{
                display: inline-block;
                background-image:url(resources/images/excel.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }

            .buttons-excel span{
                opacity: 0;
            }
            .buttons-pdf{
                display: inline-block;
                background-image:url(resources/images/pdf.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-pdf span{
                opacity: 0;
            }
            .buttons-print{
                display: inline-block;
                background-image:url(resources/images/print.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-print span{
                opacity: 0;
            }

            div.dt-buttons {
                float: right;
                margin-left:10px;
            }

            #graphicContainer {
      
      
               max-height: 280px;
               
                max-width:40800px;
                margin-top: 20px;
                margin-bottom: 20px;
                text-align: justify;



            }
            .mbox {   
                display: inline-block;
                width: 10px;
                height: 10px;
                margin: 10px 55px 10px 25px;
                padding-left: 4px;
            }
        </style>
    </body>
</html>
