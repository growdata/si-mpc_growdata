/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IRepository.java
 * Created on: 2016/10/24, 09:59:11 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository;

import com.gml.simpc.api.entity.Functionality;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author <a href="mailto:julianm@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
public interface FunctionalityRepository
    extends JpaRepository<Functionality, Long> {

    /**
     * Encuentra las funcionalidades a partir del nombre de usuario.
     *
     * @param username
     *
     * @return
     */
    @Query(nativeQuery = true, value = "SELECT fu.* FROM (SELECT * FROM " +
        "funcionalidades f START WITH f.padre_id IS NULL CONNECT BY PRIOR " +
        "f.id = f.padre_id ORDER SIBLINGS BY orden ASC) fu INNER JOIN " +
        "recursos r ON fu.id = r.funcionalidad_id INNER JOIN perfiles p ON " +
        "p.id = r.perfil_id INNER JOIN usuarios u ON u.perfil = p.id " +
        "WHERE u.correo_electronico = ?1")
    List<Functionality> findByUsername(String username);

    /**
     * Encuentra las funcionalidades a partir del id del recurso.
     *
     * @param id
     *
     * @return
     */
    @Query(nativeQuery = true, value = "SELECT fu.* FROM funcionalidades fu " +
        "INNER JOIN recursos r ON fu.id = r.funcionalidad_id INNER JOIN " +
        "perfiles p ON p.id = r.perfil_id WHERE p.id = ?1 AND fu.padre_id IS " +
        "NOT NULL AND administrativa = 'F'")
    List<Functionality> getFunctionalitiesByProfile(Long id);

    /**
     * Encuentra las funcionalidades a partir del id de las funcionalidades
     * omitidas.
     *
     * @param id
     *
     * @return
     */
    @Query(nativeQuery = true, value = "SELECT f.* FROM funcionalidades f " +
        "WHERE f.id NOT IN (SELECT fu.id FROM funcionalidades fu " +
        "INNER JOIN recursos r ON fu.id = r.funcionalidad_id INNER JOIN " +
        "perfiles p ON p.id = r.perfil_id WHERE p.id = ?1 AND fu.padre_id IS " +
        "NOT NULL) AND f.padre_id IS NOT NULL AND administrativa = 'F'")
    List<Functionality> getFunctionalitiesByNotProfile(Long id);

    /**
     * Obtiene las funcionalidades que cuentan con padre.
     *
     * @return
     */
    @Query(nativeQuery = true, value = "SELECT * FROM " +
        "funcionalidades f WHERE f.padre_id IS NOT NULL")
    List<Functionality> getChildrenFunctionalities();
}
