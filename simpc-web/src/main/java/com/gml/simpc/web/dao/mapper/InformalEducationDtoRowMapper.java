/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: InformalEducationDtoRowMapper.java
 * Created on: 2017/02/13, 11:51:03 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.dao.mapper;

import com.gml.simpc.api.dto.InformalEducationDto;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 * Row Mapper para educaci&oacute;n informal.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class InformalEducationDtoRowMapper 
    implements RowMapper<InformalEducationDto> {

    @Override
    public InformalEducationDto mapRow(ResultSet rs, int i) throws SQLException {
        InformalEducationDto dto = new InformalEducationDto();
        dto.setDocumentType(rs.getString("TIPO_DOCUMENTO"));
        dto.setNumberIdentification(rs.getString("NUMERO_DOCUMENTO"));
        dto.setTrainingType(rs.getString("TIPO_CAPACITACION"));
        dto.setInstitution(rs.getString("INSTITUCION"));
        dto.setStatus(rs.getString("ESTADO"));
        dto.setCertificationDate(rs.getString("FECHA_CERTIFICACION"));
        dto.setProgramName(rs.getString("NOMBRE_PROGRAMA"));
        dto.setCountry(rs.getString("PAIS"));
        dto.setHours(rs.getString("DURACION"));
        dto.setCargaId(rs.getLong("CARGA_ID"));
        
        return dto;
    }
}
