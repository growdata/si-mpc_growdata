/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ValueList.java
 * Created on: 2017/01/16, 02:15:16 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.utilities;

import com.gml.simpc.api.entity.IndicatorConfiguration;
import com.gml.simpc.api.entity.valuelist.CarLicenseCategory;
import com.gml.simpc.api.entity.valuelist.Cine;
import com.gml.simpc.api.entity.valuelist.City;
import com.gml.simpc.api.entity.valuelist.CivilStatus;
import com.gml.simpc.api.entity.valuelist.Degree;
import com.gml.simpc.api.entity.valuelist.DisabilityType;
import com.gml.simpc.api.entity.valuelist.DocumentType;
import com.gml.simpc.api.entity.valuelist.EducationalLevel;
import com.gml.simpc.api.entity.valuelist.EmploymentExperience;
import com.gml.simpc.api.entity.valuelist.EmploymentStatus;
import com.gml.simpc.api.entity.valuelist.Eps;
import com.gml.simpc.api.entity.valuelist.EquivalentPosition;
import com.gml.simpc.api.entity.valuelist.EthnicGroup;
import com.gml.simpc.api.entity.valuelist.Foundation;
import com.gml.simpc.api.entity.valuelist.InstitutionType;
import com.gml.simpc.api.entity.valuelist.Certifications;
import com.gml.simpc.api.entity.valuelist.DeliveredCertification;
import com.gml.simpc.api.entity.valuelist.KnowledgeCore;
import com.gml.simpc.api.entity.valuelist.Language;
import com.gml.simpc.api.entity.valuelist.LoadType;
import com.gml.simpc.api.entity.valuelist.MotorcycleLicenseCategory;
import com.gml.simpc.api.entity.valuelist.OrientationType;
import com.gml.simpc.api.entity.valuelist.OtherKnowledge;
import com.gml.simpc.api.entity.valuelist.PensionFunds;
import com.gml.simpc.api.entity.valuelist.PerformanceArea;
import com.gml.simpc.api.entity.valuelist.PerformanceAreaOrientation;
import com.gml.simpc.api.entity.valuelist.Population;
import com.gml.simpc.api.entity.valuelist.ProgramType;
import com.gml.simpc.api.entity.valuelist.Schedule;
import com.gml.simpc.api.entity.valuelist.Sector;
import com.gml.simpc.api.entity.valuelist.SeveranceFund;
import com.gml.simpc.api.entity.valuelist.State;
import com.gml.simpc.api.entity.valuelist.TrainingType;
import com.gml.simpc.api.entity.valuelist.TypeQualityCertificate;
import com.gml.simpc.api.enums.CCFMode;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * Clase que define las listas de valor param�tricas usadas en el sistema.
 * Incluye m�todos para obtener y setear los datos, incluyendo busquedas por id
 * y codigo.
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica
 * Jim&eacute;nez Rozo</a>
 */
public class ValueList {

    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER
            = Logger.getLogger(ValueList.class);

    /*
     * -----------------------------------------------------------------------
     */
    /**
     * **************** VALORES PARA <b>LoadType</b> *************************
     */
    private Map<String, LoadType> loadTypes;
    private List<LoadType> loadTypeList;

    /**
     * **************** VALORES PARA <b>ProgramType</b> **********************
     */
    private Map<String, ProgramType> programTypes;
    private List<ProgramType> programTypeList;

    /**
     * **************** VALORES PARA <b>orientationType</b>
     * **********************
     */
    private Map<String, OrientationType> orientationType;
    private List< OrientationType> orientationTypeList;

    /**
     * **************** VALORES PARA <b>Foundation</b> ***********************
     */
    private Map<String, Foundation> foundations;
    private List<Foundation> foundationList;
    private List<Foundation> allFoundations;

    /**
     * ******************* VALORES PARA <b>State</b> *************************
     */
    private Map<String, State> states;
    private List<State> stateList;

    /**
     * ******************** VALORES PARA <b>City</b> *************************
     */
    private Map<String, City> cities;
    private Map<Long, City> citiesById;
    private List<City> cityList;

    /**
     * ***************** VALORES PARA <b>DocumentType</b> ********************
     */
    private Map<String, DocumentType> documentTypes;
    private Map<String, DocumentType> documentTypesFull;
    private List<DocumentType> documentTypeList;

    /**
     * *************** VALORES PARA <b>InstitutionType</b> *******************
     */
    private Map<String, InstitutionType> institutionTypes;
    private List<InstitutionType> institutionTypeList;

    /**
     * ***************** VALORES PARA <b>CivilStatus</b> *********************
     */
    private List<CivilStatus> civilStatusList;
    private Map<String, CivilStatus> civilStatusMap;

    /**
     * ******************* VALORES PARA <b>Degree</b> ************************
     */
    private List<Degree> degreeList;
    private Map<String, Degree> degreeMap;

    /**
     * **************** VALORES PARA <b>DisabilityType</b> *******************
     */
    private List<DisabilityType> disabilityTypeList;
    private Map<String, DisabilityType> disabilityTypeMap;

    /**
     * **************** VALORES PARA <b>EducationalLevel</b> ******************
     */
    private List<EducationalLevel> educationalLevelList;
    private Map<String, EducationalLevel> educationalLevelMap;

    /**
     * **************** VALORES PARA <b>EmploymentStatus</b> ******************
     */
    private List<EmploymentStatus> employmentStatusList;
    private Map<String, EmploymentStatus> employmentStatusMap;

    /**
     * ************* VALORES PARA <b>EquivalentPosition</b> ******************
     */
    private List<EquivalentPosition> equivalentPositionList;
    private Map<String, EquivalentPosition> equivalentPositionMap;

    /**
     * ****************** VALORES PARA <b>EthnicGroup</b> ********************
     */
    private List<EthnicGroup> ethnicGroupList;
    private Map<String, EthnicGroup> ethnicGroupMap;

    /**
     * ****************** VALORES PARA <b>KnowledgeCore</b> ******************
     */
    private List<KnowledgeCore> knowledgeCoreList;
    private Map<String, KnowledgeCore> knowledgeCoreMap;

    /**
     * ******************** VALORES PARA <b>Language</b> *********************
     */
    private List<Language> languageList;
    private Map<String, Language> languageMap;

    /**
     * *************** VALORES PARA <b>OtherKnowledge</b> ********************
     */
    private List<OtherKnowledge> otherKnowledgeList;
    private Map<String, OtherKnowledge> otherKnowledgeMap;

    /**
     * *************** VALORES PARA <b>PerformanceArea</b> *******************
     */
    private List<PerformanceArea> performanceAreaList;
    private Map<String, PerformanceArea> performanceAreaMap;

    /**
     * *************** VALORES PARA <b>PerformanceArea</b> *******************
     */
    private List<Sector> sectorList;
    private Map<String, Sector> sectorMap;

    /**
     * ***************** VALORES PARA <b>TrainingType</b> ********************
     */
    private List<TrainingType> trainingTypeList;
    private Map<String, TrainingType> trainingTypeMap;

    /**
     * ************* VALORES PARA <b>EmploymentExperience</b> ****************
     */
    private List<EmploymentExperience> employmentExperienceList;
    private Map<String, EmploymentExperience> employmentExperienceMap;

    /**
     * ************* VALORES PARA <b>ObtainedTitle</b> ****************
     */
    private List<Degree> obtainedTitleList;
    private Map<String, Degree> obtainedTitleMap;

    /**
     * ************* VALORES PARA <b>CarLicenseCategory</b> ****************
     */
    private List<CarLicenseCategory> carLicenseCategoryList;
    private Map<String, CarLicenseCategory> carLicenseCategoryMap;

    /**
     * ************* VALORES PARA <b>MotorcycleLicenseCategory</b>
     * *************
     */
    private List<MotorcycleLicenseCategory> motorcycleLicenseCategoryList;
    private Map<String, MotorcycleLicenseCategory> motorcycleLicenseCategoryMap;

    /**
     * ************* VALORES PARA <b>Niveles Cine</b> *************
     */
    private List<Cine> cineList;
    private Map<String, Cine> cineMap;

    /**
     * ************* VALORES PARA <b>Horarios</b> *************
     */
    private List<Schedule> scheduleList;
    private Map<String, Schedule> scheduleMap;

    /**
     * ************* VALORES PARA <b>Poblacion</b> *************
     */
    private List<Population> populationList;
    private Map<String, Population> populationMap;

    /**
     * ************* VALORES PARA <b>Administradoras de Pension</b>
     * *************
     */
    private List<PensionFunds> pensionFundsList;
    private Map<String, PensionFunds> pensionFundsMap;

    /**
     * ************* VALORES PARA <b>Administradoras de salud</b> *************
     */
    private List<Eps> epsList;
    private Map<String, Eps> epsMap;

    /**
     * ************* VALORES PARA <b>certificaciones</b> *************
     */
    private List<Certifications> certificationsList;
    private Map<String, Certifications> certificationsMap;

    /**
     * **** VALORES PARA <b>tipos de certificaciones de calidad</b> **********
     */
    private List<TypeQualityCertificate> typeQualityCertificateList;
    private Map<String, TypeQualityCertificate> typeQualityCertificateMap;

    /**
     * ************* VALORES PARA <b>certificaciones emitidas</b> *************
     */
    private List<DeliveredCertification> deliveredCertificationList;
    private Map<String, DeliveredCertification> deliveredCertificationMap;

    /**
     * ************* VALORES PARA <b>Administradoras de cesantias</b>
     * *************
     */
    private List<SeveranceFund> severanceList;
    private Map<String, SeveranceFund> severanceMap;

    /**
     * ************* VALORES PARA <b>Indicadores</b> *************
     */
    private List<IndicatorConfiguration> indicatorsList;
    private Map<String, IndicatorConfiguration> indicatorsMap;

    /**
     * *************** VALORES PARA <b>PerformanceArea</b> *******************
     */
    private List<PerformanceAreaOrientation> performanceAreaOrientationList;
    private Map<String, PerformanceAreaOrientation> performanceAreaOrientationMap;

    /**
     * M&eacute:todo para incializar la lista de valor de ciudades.
     *
     * @param cityList
     */
    public void setCities(List<City> cityList) {
        LOGGER.info("... updating Cities");

        this.cityList = cityList;
        this.cities = new HashMap<>();
        this.citiesById = new HashMap<>();

        for (City item : this.cityList) {
            this.cities.put(item.getDivipola(), item);
            this.citiesById.put(item.getId(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de estados civiles.
     *
     * @param civilStatusList
     */
    public void setCivilStatus(List<CivilStatus> civilStatusList) {
        LOGGER.info("... updating CivilStatus");

        this.civilStatusList = civilStatusList;
        this.civilStatusMap = new HashMap<>();

        for (CivilStatus item : this.civilStatusList) {
            this.civilStatusMap.put(item.getCode(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de grados.
     *
     * @param degreeList
     */
    public void setDegrees(List<Degree> degreeList) {
        LOGGER.info("... updating Degree");

        this.degreeList = degreeList;
        this.degreeMap = new HashMap<>();

        for (Degree item : this.degreeList) {
            this.degreeMap.put(item.getCode(), item);
        }
    }

    /**
     * M&eacute:todo para incializar los tipos de incapacidad.
     *
     * @param disabilityTypeList
     */
    public void setDisabilityTypes(List<DisabilityType> disabilityTypeList) {
        LOGGER.info("... updating DisabilityType");

        this.disabilityTypeList = disabilityTypeList;
        this.disabilityTypeMap = new HashMap<>();

        for (DisabilityType item : this.disabilityTypeList) {
            this.disabilityTypeMap.put(item.getCode(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de valor de tipos de documento.
     *
     * @param documentTypeList
     */
    public void setDocumentTypes(List<DocumentType> documentTypeList) {
        LOGGER.info("... updating DocumentType");

        this.documentTypeList = documentTypeList;
        this.documentTypes = new HashMap<>();
        this.documentTypesFull = new HashMap<>();

        for (DocumentType item : this.documentTypeList) {
            this.documentTypesFull.put(item.getName(), item);
            if (!"NIUP".equals(item.getName()) || !"RC".equals(item.getName())) {
                this.documentTypes.put(item.getName(), item);
            }
        }
    }

    /**
     * M&eacute:todo para incializar la lista de niveles educativos.
     *
     * @param educationalLevelList
     */
    public void setEducationalLevels(List<EducationalLevel> educationalLevelList) {
        LOGGER.info("... updating EducationalLevel");

        this.educationalLevelList = educationalLevelList;
        this.educationalLevelMap = new HashMap<>();

        for (EducationalLevel item : this.educationalLevelList) {
            this.educationalLevelMap.put(item.getCode(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de experiencias laborales.
     *
     * @param employmentExperienceList
     */
    public void setEmploymentExperience(
            List<EmploymentExperience> employmentExperienceList
    ) {
        LOGGER.info("... updating EmploymentExperience");

        this.employmentExperienceList = employmentExperienceList;
        this.employmentExperienceMap = new HashMap<>();

        for (EmploymentExperience item : this.employmentExperienceList) {
            this.employmentExperienceMap.put(item.getCode(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de estados de empleo.
     *
     * @param employmentStatusList
     */
    public void setEmploymentStatus(List<EmploymentStatus> employmentStatusList) {
        LOGGER.info("... updating EmploymentStatus");

        this.employmentStatusList = employmentStatusList;
        this.employmentStatusMap = new HashMap<>();

        for (EmploymentStatus item : this.employmentStatusList) {
            this.employmentStatusMap.put(item.getCode(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de posiciones equivalentes.
     *
     * @param equivalentPositionList
     */
    public void setEquivalentPositions(
            List<EquivalentPosition> equivalentPositionList
    ) {
        LOGGER.info("... updating EquivalentPosition");

        this.equivalentPositionList = equivalentPositionList;
        this.equivalentPositionMap = new HashMap<>();

        for (EquivalentPosition item : this.equivalentPositionList) {
            this.equivalentPositionMap.put(item.getCode(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de grupos etnicos.
     *
     * @param ethnicGroupList
     */
    public void setEthnicGroups(List<EthnicGroup> ethnicGroupList) {
        LOGGER.info("... updating EthnicGroup");

        this.ethnicGroupList = ethnicGroupList;
        this.ethnicGroupMap = new HashMap<>();

        for (EthnicGroup item : this.ethnicGroupList) {
            this.ethnicGroupMap.put(item.getCode(), item);
        }
    }

    /**
     * M�todo privado para setear cajas de compensaci�n desde los repositorios
     * jpa.
     *
     * @param foundationList
     */
    public void setFoundations(List<Foundation> foundationList) {
        LOGGER.info("... updating Foundation");

        this.allFoundations = foundationList;
        this.foundations = new HashMap<>();

        for (Foundation item : foundationList) {
            this.foundations.put(item.getCode(), item);
        }

        this.foundationList = new LinkedList<>();

        for (Foundation item : this.allFoundations) {
            if (item.getIsCCF() == CCFMode.V) {
                this.foundationList.add(item);
            }
        }
    }

    /**
     * M&eacute:todo para incializar la lista de valor de tipos de
     * instituci&oacute;n.
     *
     * @param institutionTypeList
     */
    public void setInstitutionTypes(List<InstitutionType> institutionTypeList) {
        LOGGER.info("... updating InstitutionType");

        this.institutionTypeList = institutionTypeList;
        this.institutionTypes = new HashMap<>();

        for (InstitutionType item : this.institutionTypeList) {
            this.institutionTypes.put(item.getId().toString(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de n&uacute;cleos de conocimiento.
     *
     * @param knowledgeCoreList
     */
    public void setKnowledgeCores(List<KnowledgeCore> knowledgeCoreList) {
        LOGGER.info("... updating KnowledgeCore");

        this.knowledgeCoreList = knowledgeCoreList;
        this.knowledgeCoreMap = new HashMap<>();

        for (KnowledgeCore item : this.knowledgeCoreList) {
            this.knowledgeCoreMap.put(item.getCode(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de idiomas.
     *
     * @param languageList
     */
    public void setLanguages(List<Language> languageList) {
        LOGGER.info("... updating Language");

        this.languageList = languageList;
        this.languageMap = new HashMap<>();

        for (Language item : this.languageList) {
            this.languageMap.put(item.getCode(), item);
        }
    }

    /**
     * M�todo privado para setear tipos de carga desde los repositorios jpa.
     *
     * @param loadTypeList
     */
    public void setLoadTypes(List<LoadType> loadTypeList) {

        this.loadTypeList = loadTypeList;
        this.loadTypes = new HashMap<>();

        for (LoadType item : this.loadTypeList) {
            this.loadTypes.put(item.getName(), item);
        }
        LOGGER.info("... LoadTypes loaded  successfully: " + this.loadTypes.
                size());
    }

    /**
     * M�todo privado para setear tipos de programa desde los repositorios jpa.
     *
     * @param orientationTypeList
     */
    public void setOrientationType(List< OrientationType> orientationTypeList) {
        LOGGER.info("... updating OrientationTypes");

        this.orientationTypeList = orientationTypeList;
        this.orientationType = new HashMap<>();

        for (OrientationType item : this.orientationTypeList) {
            this.orientationType.put(item.getCode(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de otros conocimientos.
     *
     * @param otherKnowledgeList
     */
    public void setOtherKnowledges(List<OtherKnowledge> otherKnowledgeList) {
        LOGGER.info("... updating OtherKnowledge");

        this.otherKnowledgeList = otherKnowledgeList;
        this.otherKnowledgeMap = new HashMap<>();

        for (OtherKnowledge item : this.otherKnowledgeList) {
            this.otherKnowledgeMap.put(item.getCode(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de &aacute;reas de
     * desempe&ntilde;o.
     *
     * @param performanceAreaList
     */
    public void setPerformanceAreas(List<PerformanceArea> performanceAreaList) {
        LOGGER.info("... updating PerformanceArea");

        this.performanceAreaList = performanceAreaList;
        this.performanceAreaMap = new HashMap<>();

        for (PerformanceArea item : this.performanceAreaList) {
            this.performanceAreaMap.put(item.getCode(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de &aacute;reas de
     * desempe&ntilde;o de orientacion
     *
     * @param performanceAreaOrientationList
     */
    public void setPerformanceAreasOrientation(
            List<PerformanceAreaOrientation> performanceAreaOrientationList) {
        LOGGER.info("... updating PerformanceAreaOrientation");

        this.performanceAreaOrientationList = performanceAreaOrientationList;
        this.performanceAreaOrientationMap = new HashMap<>();

        for (PerformanceAreaOrientation item
                : this.performanceAreaOrientationList) {
            this.performanceAreaOrientationMap.put(item.getCode(), item);
        }
    }

    /**
     * M�todo privado para setear tipos de programa desde los repositorios jpa.
     *
     * @param programTypeList
     */
    public void setProgramTypes(List<ProgramType> programTypeList) {
        LOGGER.info("... updating ProgramType");

        this.programTypeList = programTypeList;
        this.programTypes = new HashMap<>();

        for (ProgramType item : this.programTypeList) {
            this.programTypes.put(item.getId().toString(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de sectores.
     *
     * @param sectorList
     */
    public void setSectors(List<Sector> sectorList) {
        LOGGER.info("... updating Sector");

        this.sectorList = sectorList;
        this.sectorMap = new HashMap<>();

        for (Sector item : this.sectorList) {
            this.sectorMap.put(item.getCode(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de valor de estados.
     *
     * @param stateList
     */
    public void setStates(List<State> stateList) {
        LOGGER.info("... updating State");

        this.stateList = stateList;
        this.states = new HashMap<>();

        for (State item : this.stateList) {
            this.states.put(item.getDivipola(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de tipos de capacitaci&oacute;n.
     *
     * @param trainingTypeList
     */
    public void setTrainingTypes(List<TrainingType> trainingTypeList) {
        LOGGER.info("... updating TrainingType");

        this.trainingTypeList = trainingTypeList;
        this.trainingTypeMap = new HashMap<>();

        for (TrainingType item : this.trainingTypeList) {
            this.trainingTypeMap.put(item.getCode(), item);
        }
    }

    public void setEmploymentExperienceList(
            List<EmploymentExperience> employmentExperienceList) {
        this.employmentExperienceList = employmentExperienceList;
        this.employmentExperienceMap = new HashMap<>();
        for (EmploymentExperience item : this.employmentExperienceList) {
            this.employmentExperienceMap.put(item.getCode(), item);
        }
    }

    public void setObtainedTitleList(List<Degree> obtainedTitleList) {
        this.obtainedTitleList = obtainedTitleList;
        this.obtainedTitleMap = new HashMap<>();

        for (Degree item : this.obtainedTitleList) {
            this.obtainedTitleMap.put(item.getCode(), item);
        }

    }

    public void setCarLicenseCategoryList(
            List<CarLicenseCategory> carLiceseCategoryList) {
        this.carLicenseCategoryList = carLiceseCategoryList;
        this.carLicenseCategoryMap = new HashMap<>();

        for (CarLicenseCategory item : this.carLicenseCategoryList) {
            this.carLicenseCategoryMap.put(item.getCode(), item);
        }

    }

    public void setMotorcycleLicenseCategoryList(
            List<MotorcycleLicenseCategory> motorcycleLiceseCategoryList) {
        this.motorcycleLicenseCategoryList = motorcycleLiceseCategoryList;
        this.motorcycleLicenseCategoryMap = new HashMap<>();

        for (MotorcycleLicenseCategory item : this.motorcycleLicenseCategoryList) {
            this.motorcycleLicenseCategoryMap.put(item.getCode(), item);
        }

    }

    /**
     * M&eacute:todo para incializar la lista de Niveles Cine
     *
     * @param cineList
     */
    public void setCine(List<Cine> cineList) {
        LOGGER.info("... updating Cine");

        this.cineList = cineList;
        this.cineMap = new HashMap<>();

        for (Cine item : this.cineList) {
            this.cineMap.put(item.getCode(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de Schedule
     *
     * @param scheduleList
     */
    public void setSchedule(List<Schedule> scheduleList) {
        LOGGER.info("... updating Schedule");

        this.scheduleList = scheduleList;
        this.scheduleMap = new HashMap<>();

        for (Schedule item : this.scheduleList) {
            this.scheduleMap.put(item.getCode(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de valor de tipos de
     * instituci&oacute;n.
     *
     * @param populationList
     */
    public void setPopulation(List<Population> populationList) {
        LOGGER.info("... updating Population");

        this.populationList = populationList;
        this.populationMap = new HashMap<>();

        for (Population item : this.populationList) {
            this.populationMap.put(item.getCode(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de valor de administradoras de
     * pension
     *
     * @param pensionFundsList
     */
    public void setPensionFunds(List<PensionFunds> pensionFundsList) {
        LOGGER.info("... updating administradoras de pension");

        this.pensionFundsList = pensionFundsList;
        this.pensionFundsMap = new HashMap<>();

        for (PensionFunds item : this.pensionFundsList) {
            this.pensionFundsMap.put(item.getCode(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de valor de administradoras de
     * pension
     *
     * @param epsList
     */
    public void setEps(List<Eps> epsList) {
        LOGGER.info("... updating administradoras de salu");

        this.epsList = epsList;
        this.epsMap = new HashMap<>();

        for (Eps item : this.epsList) {
            this.epsMap.put(item.getCode(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de valor de certificaciones
     *
     * @param certificationsList
     */
    public void setCertifications(List<Certifications> certificationsList) {
        LOGGER.info("... updating certificaciones");

        this.certificationsList = certificationsList;
        this.certificationsMap = new HashMap<>();

        for (Certifications item : this.certificationsList) {
            this.certificationsMap.put(String.valueOf(item.getId()), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de valor de certificaciones
     * emitidas
     *
     * @param deliveredCertificationList
     */
    public void setDeliveredCertification(List<DeliveredCertification> deliveredCertificationList) {
        LOGGER.info("... updating certificaciones emitidas");

        this.deliveredCertificationList = deliveredCertificationList;
        this.deliveredCertificationMap = new HashMap<>();

        for (DeliveredCertification item : this.deliveredCertificationList) {
            this.deliveredCertificationMap.put(String.valueOf(item.getId()), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de valor de los tipos de
     * certificacion
     *
     * @param typeQualityCertificateList
     */
    public void setTypeQualityCertificate(List<TypeQualityCertificate> typeQualityCertificateList) {
        LOGGER.info("... updating tipo de certificaci�n");

        this.typeQualityCertificateList = typeQualityCertificateList;
        this.typeQualityCertificateMap = new HashMap<>();

        for (TypeQualityCertificate item : this.typeQualityCertificateList) {
            this.typeQualityCertificateMap.put(item.getCode(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de valor de administradoras de
     * pension
     *
     * @param severanceList
     */
    public void setSeveranceFund(List<SeveranceFund> severanceList) {

        this.severanceList = severanceList;
        this.severanceMap = new HashMap<>();

        for (SeveranceFund item : this.severanceList) {
            this.severanceMap.put(item.getCode(), item);
        }
    }

    /**
     * M&eacute:todo para incializar la lista de valor de indicadores.
     *
     * @param indicatorsList
     */
    public void setInticators(List<IndicatorConfiguration> indicatorsList) {

        this.indicatorsList = indicatorsList;
        this.indicatorsMap = new HashMap<>();

        for (IndicatorConfiguration item : indicatorsList) {
            this.indicatorsMap.put(item.getId().toString(), item);
        }
    }


    /*
     * -----------------------------------------------------------------------
     */
    public List<Schedule> getScheduleList() {
        return scheduleList;
    }

    public Map<String, Population> getPopulationMap() {
        return populationMap;
    }

    public List<SeveranceFund> getSeveranceList() {
        return severanceList;
    }

    public Map<String, SeveranceFund> getSeveranceMap() {
        return severanceMap;
    }

    public List<IndicatorConfiguration> getIndicatorsList() {
        return indicatorsList;
    }

    public Map<String, IndicatorConfiguration> getIndicatorsMap() {
        return indicatorsMap;
    }

    public List<EmploymentExperience> getEmploymentExperienceList() {
        return employmentExperienceList;
    }

    public Map<String, EmploymentExperience> getEmploymentExperienceMap() {
        return employmentExperienceMap;
    }

    public List<PensionFunds> getPensionFundsList() {
        return pensionFundsList;
    }

    public Map<String, PensionFunds> getPensionFundsMap() {
        return pensionFundsMap;
    }

    public List<Cine> getCineList() {
        return cineList;
    }

    public List<Certifications> getCertifications() {
        return certificationsList;
    }

    public List<TypeQualityCertificate> getTypeQualityCertificate() {
        return typeQualityCertificateList;
    }

    public List<DeliveredCertification> getDeliveredCertification() {
        return deliveredCertificationList;
    }

    public Map<String, Cine> getCineMap() {
        return cineMap;
    }

    public Map<String, Certifications> getCertificationsMap() {
        return certificationsMap;
    }

    public Map<String, DeliveredCertification> getDeliveredCertificationMap() {
        return deliveredCertificationMap;
    }

    public Map<String, Schedule> getScheduleMap() {
        return scheduleMap;
    }

    public Map<String, LoadType> getLoadTypes() {
        return loadTypes;
    }

    public List<LoadType> getLoadTypeList() {
        return loadTypeList;
    }

    public Map<String, Eps> getEpsMap() {
        return epsMap;
    }

    public List<Eps> getEpsList() {
        return epsList;
    }

    public List<Certifications> getCertificationsList() {
        return certificationsList;
    }

    public List<TypeQualityCertificate> getTypeQualityCertificateList() {
        return typeQualityCertificateList;
    }

    public List<DeliveredCertification> getDeliveredCertificationList() {
        return deliveredCertificationList;
    }

    public Map<String, ProgramType> getProgramTypes() {
        return programTypes;
    }

    public List<ProgramType> getProgramTypeList() {
        return programTypeList;
    }

    public Map<String, OrientationType> getOrientationType() {
        return orientationType;
    }

    public List<OrientationType> getOrientationTypeList() {
        return orientationTypeList;
    }

    public Map<String, Foundation> getFoundations() {
        return foundations;
    }

    public List<Foundation> getFoundationList() {
        return foundationList;
    }

    public Map<String, Population> getPopulation() {
        return this.populationMap;
    }

    public List<Population> getPopulationList() {
        return populationList;
    }

    public Map<String, State> getStates() {
        return states;
    }

    public List<State> getStateList() {
        return stateList;
    }

    public Map<String, City> getCities() {
        return cities;
    }

    public List<City> getCityList() {
        return cityList;
    }

    public Map<String, DocumentType> getDocumentTypes() {
        return documentTypes;
    }

    public Map<String, DocumentType> getDocumentTypesFull() {
        return documentTypesFull;
    }

    public List<DocumentType> getDocumentTypeList() {
        return documentTypeList;
    }

    public Map<String, InstitutionType> getInstitutionTypes() {
        return institutionTypes;
    }

    public List<InstitutionType> getInstitutionTypeList() {
        return institutionTypeList;
    }

    public List<CivilStatus> getCivilStatusList() {
        return civilStatusList;
    }

    public Map<String, CivilStatus> getCivilStatusMap() {
        return civilStatusMap;
    }

    public List<Degree> getObtainedTitleList() {
        return obtainedTitleList;
    }

    public Map<String, Degree> getObtainedTitleMap() {
        return obtainedTitleMap;
    }

    public List<Degree> getDegreeList() {
        return degreeList;
    }

    public Map<String, Degree> getDegreeMap() {
        return degreeMap;
    }

    public List<DisabilityType> getDisabilityTypeList() {
        return disabilityTypeList;
    }

    public Map<String, DisabilityType> getDisabilityTypeMap() {
        return disabilityTypeMap;
    }

    public List<EducationalLevel> getEducationalLevelList() {
        return educationalLevelList;
    }

    public Map<String, EducationalLevel> getEducationalLevelMap() {
        return educationalLevelMap;
    }

    public List<EmploymentStatus> getEmploymentStatusList() {
        return employmentStatusList;
    }

    public Map<String, EmploymentStatus> getEmploymentStatusMap() {
        return employmentStatusMap;
    }

    public List<EquivalentPosition> getEquivalentPositionList() {
        return equivalentPositionList;
    }

    public Map<String, EquivalentPosition> getEquivalentPositionMap() {
        return equivalentPositionMap;
    }

    public List<EthnicGroup> getEthnicGroupList() {
        return ethnicGroupList;
    }

    public Map<String, EthnicGroup> getEthnicGroupMap() {
        return ethnicGroupMap;
    }

    public List<KnowledgeCore> getKnowledgeCoreList() {
        return knowledgeCoreList;
    }

    public Map<String, KnowledgeCore> getKnowledgeCoreMap() {
        return knowledgeCoreMap;
    }

    public List<Language> getLanguageList() {
        return languageList;
    }

    public Map<String, Language> getLanguageMap() {
        return languageMap;
    }

    public List<OtherKnowledge> getOtherKnowledgeList() {
        return otherKnowledgeList;
    }

    public Map<String, OtherKnowledge> getOtherKnowledgeMap() {
        return otherKnowledgeMap;
    }

    public List<PerformanceArea> getPerformanceAreaList() {
        return performanceAreaList;
    }

    public List<PerformanceAreaOrientation> getPerformanceAreaOrientationList() {
        return performanceAreaOrientationList;
    }

    public Map<String, PerformanceArea> getPerformanceAreaMap() {
        return performanceAreaMap;
    }

    public Map<String, PerformanceAreaOrientation> getPerformanceAreaOrientationMap() {
        return performanceAreaOrientationMap;
    }

    public List<Sector> getSectorList() {
        return sectorList;
    }

    public Map<String, Sector> getSectorMap() {
        return sectorMap;
    }

    public List<TrainingType> getTrainingTypeList() {
        return trainingTypeList;
    }

    public Map<String, TrainingType> getTrainingTypeMap() {
        return trainingTypeMap;
    }

    public List<CarLicenseCategory> getCarLicenseCategoryList() {
        return carLicenseCategoryList;
    }

    public List<MotorcycleLicenseCategory> getMotorcycleLicenseCategoryList() {
        return motorcycleLicenseCategoryList;
    }

    public List<Foundation> getAllFoundations() {
        return allFoundations;
    }

    public List<SeveranceFund> getSeveranceFundList() {
        return severanceList;
    }

    public Map<String, SeveranceFund> getSeveranceFundMap() {
        return severanceMap;
    }

    /*
     * -----------------------------------------------------------------------
     */
    public void setLoadTypes(Map<String, LoadType> loadTypes) {
        this.loadTypes = loadTypes;
    }

    public void setOrientationTypes(
            Map<String, OrientationType> orientationTypes) {
        this.orientationType = orientationTypes;

    }

    public void setProgramTypes(Map<String, ProgramType> programTypes) {
        this.programTypes = programTypes;

    }

    public void setFoundations(Map<String, Foundation> foundations) {
        this.foundations = foundations;
    }

    public void setDocumentType(Map<String, DocumentType> documentType) {
        this.documentTypes = documentType;
    }

    public void setDocumentTypeFull(Map<String, DocumentType> documentType) {
        this.documentTypesFull = documentType;
    }

    public void setStates(Map<String, State> states) {
        this.states = states;
    }

    public void setCities(Map<String, City> cities) {
        this.cities = cities;
    }

    public void setCineMap(Map<String, Cine> cine) {
        this.cineMap = cine;
    }

    public void setscheduleMap(Map<String, Schedule> schedule) {
        this.scheduleMap = schedule;
    }

    public void setCertificationsMap(Map<String, Certifications> certifications) {
        this.certificationsMap = certifications;
    }

    public void setDeliveredCertificationMap(Map<String, DeliveredCertification> deliveredCertification) {
        this.deliveredCertificationMap = deliveredCertification;
    }

    public void setDocumentTypes(Map<String, DocumentType> documentTypes) {
        this.documentTypes = documentTypes;
    }

    public void setCivilStatusMap(Map<String, CivilStatus> civilStatusMap) {
        this.civilStatusMap = civilStatusMap;
    }

    public void setDegreeMap(Map<String, Degree> degreeMap) {
        this.degreeMap = degreeMap;
    }

    public void setDisabilityTypeMap(
            Map<String, DisabilityType> disabilityTypeMap) {
        this.disabilityTypeMap = disabilityTypeMap;
    }

    public void setEducationalLevelMap(
            Map<String, EducationalLevel> educationalLevelMap) {
        this.educationalLevelMap = educationalLevelMap;
    }

    public void setEmploymentStatusMap(
            Map<String, EmploymentStatus> employmentStatusMap) {
        this.employmentStatusMap = employmentStatusMap;
    }

    public void setEquivalentPositionMap(
            Map<String, EquivalentPosition> equivalentPositionMap) {
        this.equivalentPositionMap = equivalentPositionMap;
    }

    public void setEthnicGroupMap(Map<String, EthnicGroup> ethnicGroupMap) {
        this.ethnicGroupMap = ethnicGroupMap;
    }

    public void setKnowledgeCoreMap(
            Map<String, KnowledgeCore> knowledgeCoreMap) {
        this.knowledgeCoreMap = knowledgeCoreMap;
    }

    public void setLanguageMap(Map<String, Language> languageMap) {
        this.languageMap = languageMap;
    }

    public void setOtherKnowledgeMap(
            Map<String, OtherKnowledge> otherKnowledgeMap) {
        this.otherKnowledgeMap = otherKnowledgeMap;
    }

    public void setPerformanceAreaMap(
            Map<String, PerformanceArea> performanceAreaMap) {
        this.performanceAreaMap = performanceAreaMap;
    }

    public void setPerformanceAreaOrientationMap(
            Map<String, PerformanceAreaOrientation> performanceAreaOrientationMap) {
        this.performanceAreaOrientationMap = performanceAreaOrientationMap;
    }

    public void setSectorMap(Map<String, Sector> sectorMap) {
        this.sectorMap = sectorMap;
    }

    public void setTrainingTypeMap(Map<String, TrainingType> trainingTypeMap) {
        this.trainingTypeMap = trainingTypeMap;
    }

    public void setEmploymentExperienceMap(
            Map<String, EmploymentExperience> employmentExperienceMap) {
        this.employmentExperienceMap = employmentExperienceMap;
    }

    public void setObtainedTitleMap(
            Map<String, Degree> obtainedTitleMap) {
        this.obtainedTitleMap = obtainedTitleMap;
    }

    public Map<Long, City> getCitiesById() {
        return citiesById;
    }

    public void setCitiesById(Map<Long, City> citiesById) {
        this.citiesById = citiesById;
    }

    public Map<String, CarLicenseCategory> getCarLicenseCategoryMap() {
        return carLicenseCategoryMap;
    }

    public void setCarLicenseCategoryMap(
            Map<String, CarLicenseCategory> carLicenseCategoryMap) {
        this.carLicenseCategoryMap = carLicenseCategoryMap;
    }

    public Map<String, MotorcycleLicenseCategory> getMotorcycleLicenseCategoryMap() {
        return motorcycleLicenseCategoryMap;
    }

    public void setMotorcycleLicenseCategoryMap(
            Map<String, MotorcycleLicenseCategory> motorcycleLicenseCategoryMap) {
        this.motorcycleLicenseCategoryMap = motorcycleLicenseCategoryMap;
    }

    public void setPensionFundsMap(
            Map<String, PensionFunds> pensionFundsMap) {
        this.pensionFundsMap = pensionFundsMap;
    }

    public void setEpsMap(
            Map<String, Eps> epsMap) {
        this.epsMap = epsMap;
    }

    public void setSeveranceFundMap(
            Map<String, SeveranceFund> severanceMap) {
        this.severanceMap = severanceMap;
    }
}
