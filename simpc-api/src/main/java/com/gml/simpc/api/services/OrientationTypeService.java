/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: OrientationTypeService.java
 * Created on: 2017/01/23, 04:15:06 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.entity.valuelist.OrientationType;
import java.util.List;

/**
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface OrientationTypeService {

    /**
     * Obtiene todos los  tipos de programas creadas.
     *
     * @return
     */
    List<OrientationType> getAll();

    /**
     * Servicio para guardar un tipo de programas.
     *
     * @param orientationType
     */
    void save(OrientationType orientationType);

    /**
     * Servicio para actualizar tipos de programas
     *
     * @param orientationType
     */
    void update(OrientationType orientationType);

    /**
     * Elimina un tipo de programas
     *
     * @param orientationType
     */
    void remove(OrientationType orientationType);
   
}