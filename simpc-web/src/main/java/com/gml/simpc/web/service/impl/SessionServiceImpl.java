/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserServiceImpl.java
 * Created on: 2016/10/19, 02:11:43 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.entity.Session;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.services.SessionService;
import com.gml.simpc.web.repository.SessionRepository;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Servicio para administrar usuarios.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Service(value = "sessionService")
public class SessionServiceImpl implements SessionService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(SessionServiceImpl.class);
    /**
     * Repositorio para usuarios.
     */
    @Autowired
    private SessionRepository repository;

    /**
     * Genera lista de todas las sesiones.
     * 
     * @return 
     */
    @Override
    public List<Session> getAll() {
        LOGGER.info("Ejecutando metodo [ getAll ]");
        return this.repository.findAll();
    }

    /**
     * Guarda sesiones.
     * 
     * @param session 
     */
    @Override
    public void save(Session session) {
        LOGGER.info("Ejecutando metodo [ save ]");
        
        this.repository.saveAndFlush(session);
    }

    /**
     * Actualiza sesiones.
     * 
     * @param session 
     */
    @Override
    public void update(Session session) {
        LOGGER.info("Ejecutando metodo [ update ]");

        this.repository.saveAndFlush(session);
    }

    /**
     * Elimina sesiones.
     * 
     * @param session 
     */
    @Override
    public void remove(Session session) {
        LOGGER.info("Ejecutando metodo [ remove ]");
        this.repository.delete(session);
    }

    /**
     * Busca sesiones seg&ucute;n un usuario espec&icute;fico.
     * 
     * @param user
     * @return 
     */
    @Override
    public Session findByUser(User user) {
        LOGGER.info("Ejecutando metodo [ findByUser ]");
        
        return this.repository.findByUser(user);
    }
    
    /**
     * Expira sesiones.
     * 
     * @return 
     */
    @Override
    public int expireSessions() {
        
        LOGGER.info("Ejecutando metodo [ expireSessions ]");
        return this.repository.expireSessions();
    }

    /**
     * Administra el ping de sesiones.
     * 
     * @param session
     * @return 
     */
    @Override
    public Integer sessionPing(Session session) {
        
        try {
            LOGGER.info("Ejecutando metodo [ sessionPing ] for sesion" + 
                session.toString());
            return this.repository.sessionPing(session.getId());
        } catch (Exception e) {
            LOGGER.error("Sesion nula", e);
            
            return 0;
        }
    }

}
