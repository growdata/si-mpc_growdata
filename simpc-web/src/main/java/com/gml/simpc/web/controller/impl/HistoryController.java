/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: HistoryController.java
 * Created on: 2017/02/09, 12:08:45 PM
 * Project: MASTER DATA - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.gml.simpc.api.entity.Intersection;
import com.gml.simpc.api.entity.Session;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.services.IntersectionService;
import com.gml.simpc.api.utilities.Util;
import com.gml.simpc.commons.annotations.FunctionalityInterceptor;
import com.gml.simpc.web.controller.ControllerDefinition;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import static com.gml.simpc.api.utilities.Util.SESSION_USER;

/**
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 * Muestra el historial de cruces
 */
@FunctionalityInterceptor(name = "historial-cruces.htm")
@Controller
public class HistoryController implements ControllerDefinition {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(HistoryController.class);
    /**
     * Servicio para acceder a las consultas de los cruces.
     */
    @Autowired
    private IntersectionService intersectionService;

    /**
     * Inicializa el binder para formatear fechas.
     *
     * @param binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class,
            new CustomDateEditor(sdf, true));
    }

    /**
     * Llama a la ventana de historial de cruces mostrando los del dia en curso.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "historial-cruces",
        method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response) {
        Session session = (Session) request.getSession().
            getAttribute(SESSION_USER);
        User userSession = session.getUser();
        String searchType = Util.nullValue(request.getParameter("searchType"));
        ModelAndView modelAndView = new ModelAndView("historial-cruces");
        String ccf = userSession.getCcf().getCode();
        if (userSession.isAdmin()) {
            modelAndView.addObject("isAdmin", "y");
            ccf = "%";
        }
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date startDate = dateFormat.parse(dateFormat.format(new Date()));
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startDate);
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            Date endDate = calendar.getTime();

            modelAndView.addObject("ListaCruces", this.intersectionService.
                findByEndDateRange(startDate, endDate, searchType, ccf));

        } catch (Exception ex) {
            LOGGER.info("Exception", ex);
            modelAndView.addObject("msg", ex);
        } finally {
            LOGGER.info("finally de Historial de cruces.");
        }
        return modelAndView;

    }

    /**
     * Descarga todos los archivos generados en un cruce.
     *
     * @param request
     * @param response
     * @param id
     * @throws java.io.IOException
     */
    @RequestMapping(value = "descargar-cruces", method = RequestMethod.GET)
    public void downloadZip(HttpServletRequest request,
        HttpServletResponse response, 
        @RequestParam(name = "id") Long id) throws IOException {

        Intersection intersection = this.intersectionService.findById(id);
        
        File directory = new File(intersection.getResultPath());
        String[] files = directory.list();

        byte[] zip = zipFiles(directory, files);

        ServletOutputStream sos = response.getOutputStream();
        response.setContentType("application/zip");
        response.setHeader("Content-Disposition",
            "attachment;filename=DATA.ZIP");

        sos.write(zip);
        sos.flush();
    }

    /**
     * Comprime los archivos de un directorio dado.
     * 
     * @param directory
     * @param files
     * @return 
     */
    private byte[] zipFiles(File directory, String[] files) {

        try {

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ZipOutputStream zos = new ZipOutputStream(baos);
            byte bytes[] = new byte[2048];

            for (String fileName : files) {
                writeFile(directory, fileName, zos, bytes);
            }
            zos.flush();
            baos.flush();
            zos.close();
            baos.close();

            return baos.toByteArray();
        } catch (Exception e) {
            LOGGER.info(e);
        }
        return null;
    }

    /**
     * Escribe en un archivo.
     *
     * @param directory
     * @param fileName
     * @param zos
     * @param bytes
     *
     * @throws IOException
     */
    private void writeFile(File directory, String fileName, ZipOutputStream zos,
        byte[] bytes) throws IOException {
        try (FileInputStream fis = new FileInputStream(directory.getPath() +
            System.getProperty("file.separator") + fileName);
            BufferedInputStream bis = new BufferedInputStream(fis)) {

            zos.putNextEntry(new ZipEntry(fileName));

            int bytesRead;
            while ((bytesRead = bis.read(bytes)) != -1) {
                zos.write(bytes, 0, bytesRead);
            }

            zos.closeEntry();
        }
    }

    /**
     * Descarga todos los archivos generados en un cruce.
     *
     * @param request
     * @param response
     * @param startDate
     * @param finalDate
     *
     * @return
     */
    @RequestMapping(value = "rango-cruces", method = RequestMethod.GET)
    public ModelAndView searchIntersection(HttpServletRequest request,
        HttpServletResponse response,
        @RequestParam(name = "fechaInicio") Date startDate,
        @RequestParam(name = "fechaFin") Date finalDate
    ) {
        ModelAndView modelAndView = new ModelAndView("historial-cruces");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(finalDate);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date endDate = calendar.getTime();
        Session session = (Session) request.getSession().
            getAttribute(SESSION_USER);
        User userSession = session.getUser();
        String searchType = Util.nullValue(request.getParameter("searchType"));
        String ccf = userSession.getCcf().getCode();
        if (userSession.isAdmin()) {
            modelAndView.addObject("isAdmin", "y");
            ccf = "%";
        }

        try {
            LOGGER.info("FECHA INCIO" + startDate + "FEHCA FIN" + endDate +
                "TIPODE BUSQUEDA" + searchType + "CCF" + ccf);

            modelAndView.addObject("ListaCruces", this.intersectionService.
                findByEndDateRange(startDate, endDate, searchType, ccf));

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg",
                "Error al realizar la consulta. " +
                "Por favor comuniquese con el administrador");
        } finally {
            LOGGER.info("Finaliza la invocacion de [ listProcess ].");
        }
        return modelAndView;
    }

}
