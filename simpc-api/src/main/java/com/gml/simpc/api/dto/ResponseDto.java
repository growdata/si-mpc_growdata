/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ResponseDto.java
 * Created on: 2017/03/07, 10:16:17 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

import com.gml.simpc.api.entity.DesktopSession;
import java.io.Serializable;

/**
 * DTO para respuestas por servicios REST.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class ResponseDto implements Serializable {

    /**
     * Estado final de la petici&oacute;n.
     */
    private boolean ok;
    /**
     * Mensaje de respuesta.
     */
    private String message;
    /**
     * Sesi&oacute;n.
     */
    private DesktopSession session;

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DesktopSession getSession() {
        return session;
    }

    public void setSession(DesktopSession session) {
        this.session = session;
    }
}
