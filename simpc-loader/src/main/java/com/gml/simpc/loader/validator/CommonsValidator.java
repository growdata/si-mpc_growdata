/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CommonsValidator.java
 * Created on: 2016/11/11, 10:33:45 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 * Objetive: Apply xml validation rules to any item. Retunrs error list.
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.validator;

import com.gml.simpc.api.dto.ProfitDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.batch.item.validator.ValidationException;
import org.springframework.batch.item.validator.Validator;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

/**
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class CommonsValidator<T> implements Validator<T> {

    private static final Logger LOGGER = Logger.
        getLogger(CommonsValidator.class);
    private org.springframework.validation.Validator validator;
    private MessageSource messageSource;
    /**
     * Indica el tipo de carga, si es actualizaci&oacute;n o creaci&oacute;n
     */
    private String writeType;

    /**
     * Valida un registro contra el xml de reglas
     */
    @Override
    public void validate(T item) throws ValidationException {

        ThreadLocalJob.setProcesed();
        String message;
        LOGGER.debug("Validating item " + item.toString());

        applySpecialValidations(item);

        if (!validator.supports(item.getClass())) {
            message = "Fall� la validaci�n para [ " + item +
                " ] : " + item.getClass().getName() + " Clase no soportada " +
                "para validaciones.";

            ThreadLocalJob.setError(message);

            return;
        }

        BeanPropertyBindingResult errors =
            new BeanPropertyBindingResult(item, item.getClass().toString());

        validator.validate(item, errors);
        LOGGER.debug("Result " + errors);
        if (errors.hasErrors()) {
            message = errorsToString(errors);
            ThreadLocalJob.setError(message);
        }
    }

    /**
     * Generar cadena de lista de errores de validacion de un item.
     */
    private String errorsToString(Errors errors) {
        List errorList = errors.getAllErrors();
        StringBuilder buffer = new StringBuilder();

        boolean first = true;

        for (Object object : errorList) {
            if (object instanceof MessageSourceResolvable) {
                MessageSourceResolvable r = (MessageSourceResolvable) object;
                if (!first) {
                    buffer.append(", ");
                }
                buffer.append(messageSource.getMessage(r, null));

                first = false;
            }
        }
        return buffer.toString();
    }

    /**
     * Aplica validaciones especiales de acuerdo al DTO.
     *
     * @param item
     */
    private void applySpecialValidations(T item) {
        if (item instanceof ProfitDto) {
            applyFosfecValidations((ProfitDto) item);
        }
    }

    /**
     * Aplica validaciones especiales al DTO de fosfec.
     *
     * @param dto
     */
    private void applyFosfecValidations(ProfitDto dto) {
        dto.setWriteType(this.writeType);
    }

    public void setValidator(org.springframework.validation.Validator validator) {
        this.validator = validator;
    }

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public MessageSource getMessageSource() {
        return messageSource;
    }

    public void setWriteType(String writeType) {
        this.writeType = writeType;
    }
}
