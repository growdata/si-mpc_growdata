/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: basicInformation.java
 * Created on: 2016/12/27, 04:23:26 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Leonardo Rocha</a>
 */
public class LanguageDto {

    private String documentType;
    private String numberIdentification;
    private String languages;
    private String level;
    private Long cargaId;


    /*
     * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public LanguageDto() {

    }

    public LanguageDto(String documentType, String numberIdentification,
        String languages, String level, Long cargaId) {
        this.documentType = documentType;
        this.numberIdentification = numberIdentification;
        this.languages = languages;
        this.level = level;
        this.cargaId = cargaId;
    }

    public Long getCargaId() {
        return cargaId;
    }

    public void setCargaId(Long cargaId) {
        this.cargaId = cargaId;
    }

    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getNumberIdentification() {
        return numberIdentification;
    }

    public void setNumberIdentification(String numberIdentification) {
        this.numberIdentification = numberIdentification;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(" Idioma=").append(languages).
            append("~&~Nivel=").append(level);
        return stringBuilder.toString();
    }

}
