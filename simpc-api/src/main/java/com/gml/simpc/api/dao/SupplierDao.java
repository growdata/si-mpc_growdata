/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: SupplierDao.java
 * Created on: 2017/01/10, 11:04:21 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dao;

import com.gml.simpc.api.dto.GraphicDataDto;
import com.gml.simpc.api.dto.TableDataDto;
import com.gml.simpc.api.dto.ValueListDto;
import com.gml.simpc.api.entity.Institution;
import java.util.List;

/**
 * Interfaz que define el DAO para consultar la informacion del banco de
 * oferentes
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface SupplierDao {

    /**
     * Obtiene los tipos de formacion para un programa-capacitacion
     *
     * @return
     */
    List<ValueListDto> getFormationType();

    /**
     * Obtiene los tipos de formacion para un programa-orientacion
     *
     * @return
     */
    List<ValueListDto> getOrientationType();

    /**
     * Obtiene lista de duraciones de programas
     *
     *
     * @return
     */
    List<ValueListDto> getDuration();

    /**
     * Obtiene lista de instituciones segun el filtro
     *
     * @return
     */
    List<Institution> getInstitutionsByTypeAndCityAndState(String type,
        String city, String state);

    /**
     * Obtiene Grafico de instituciones segun el filtro
     *
     * @return
     */
    List<GraphicDataDto> graphicInstitutionsByTypeAndCityAndState(String type,
        String city, String state);

    /**
     * Obtiene lista de sedes segun el filtro
     *
     * @return
     */
    List<TableDataDto> getHeadquarterByTypeAndCityAndState(String type,
        String city, String state);

    /**
     * Obtiene Grafico de Sedes segun el filtro
     *
     * @return
     */
    List<GraphicDataDto> graphicHeadquarterByTypeAndCityAndState(String type,
        String city, String state);

    /**
     * Obtiene lista de modulos y programas de capacitaciones segun el filtro
     *
     * @return
     */
    List<TableDataDto> getProgramByTypeAndDurationAndModule(
        String formationType, String duration, String module);
}
