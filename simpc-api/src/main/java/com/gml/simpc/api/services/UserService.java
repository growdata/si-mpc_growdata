/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserService.java
 * Created on: 2016/10/19, 02:10:35 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.entity.exeption.UserException;
import java.util.List;

/**
 * Interface que define el servicio de usuarios.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface UserService {

   /**
     * Obtiene todos los usuarios segun los filtros adquiridos.
     *
     * @param user
     * @return
     */
    List<User> findByFilters(User user);

    /**
     * Obtiene todos los usuarios creados.
     *
     * @return
     */
    List<User> getAll();

    /**
     * Servicio para guardar usuarios.
     *
     * @param user
     *
     * @throws UserException
     */
    void save(User user) throws UserException;

    /**
     * Servicio para actualizar usuarios.
     *
     * @param user
     */
    void update(User user) throws UserException;

    /**
     * Elimina un usuario.
     *
     * @param user
     */
    void remove(User user);

    /**
     * Cnsulta por email, contrase&ntilde;a y estado de perfil.
     *
     * @param email
     * @param userName
     * @param password
     * @param statusProfile
     *
     * @return
     */
    User searchUserLogin(String email, String userName, String password,
            String statusProfile);

    /**
     * M&eacute;todo que actualiza la sesi&oacute;n del usuario
     *
     * @param userId
     *
     * @return
     */
    Integer updateUserSession(Long userId);

    /**
     * M&eacute;todo para verificar la existencia del usuario
     *
     * @param userName Nombre del usuario suministrado por el administrador
     *
     * @return
     *
     */
    User findByUsernameOrEmail(String userName, String mail);

    /**
     *
     * @param userName Nombre del usuario que se desea asignar
     * @param email Por validaci&ocute;n de interfaz este parametro recibe el
     * mail del ususario
     * @param userPass Clave de acceso asignada por el usuario
     */
    void updateCredentials(String userName, String email, String userPass);

    /**
     * Consulta los perfiles por id.
     *
     * @param id
     * @param userId
     * @return 
     */
    User findById(Long id);

    /**
     * M&eacute;todo para actualizar los datos base de un usuario.
     *
     * @param user
     *
     * @throws UserException
     */
    void updateUser(User user) throws UserException;

    /**
     * M&eacute;todo para activar y modificar contrase�a de usuarios antiguos.
     *
     * @param user
     *
     * @throws UserException
     */
    void updateUserOld(User user) throws UserException;
    
    /**
     * Este query trae los usuarios administradores
     *
     * @param isAdmin
     *
     * @return
     */
    List<User> findByAdmin(boolean isAdmin);

    /**
     * Obtiene todos los usuarios que ya haya expirado el tiempo de uso de la
     * contrase&ntilde;a.
     *
     * @return
     */
    List<User> findUsersToExpire();
    
    /**
     * Obtiene todos los usuarios pr�ximos a expirar
     *
     * @return
     */
    List<User> findUsersToExpirePrev();

}
