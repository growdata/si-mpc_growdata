<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <style>
            input[type=number]::-webkit-outer-spin-button,
            input[type=number]::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            input[type=number] {
                -moz-appearance:textfield;
            }
        </style>
    </head>
    <body>
        <c:if test="${not empty msg}">
            <br/>
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        
        <h1>Historial de Cruces</h1>
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading"><h2>Historial de Cruces</h2></div>
            <div class="panel-body">


                <div class="tab-content">
                    <div id="alert-area" ></div>


                    <legend>           

                        <form id="rangoForm" name="rangoForm" action="rango-cruces.htm" >

                            <div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="fechaInicio" class="control-label ">Fecha Inicio</label>
                                        <input id="fechaInicio" name="fechaInicio" class="form-control "
                                               data-date-format="dd-mm-yyyy"   path="fechaInicio" required="true" />
                                        <fmt:formatDate value="${fechaInicio}" pattern="dd-MM-yyyy" />
                                        <div class="clearfix"></div>
                                    </div>
                                </div>  
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="fechaFin" class="control-label ">Fecha Fin</label>
                                        <input id="fechaFin" name="fechaFin"  class="form-control "
                                               data-date-format="dd-mm-yyyy"   path="fechaFin" required="true"/>
                                        <fmt:formatDate value="${fechaFin}" pattern="dd-MM-yyyy" />
                                        <div class="clearfix"></div>
                                    </div>
                                </div> 
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="searchType" class="control-label">Tipo De Consulta</label>
                                        <select class="form-control" id="searchType" name="searchType" >
                                            <option  value="" selected>Todas</option>
                                            <option  value="I" >Individuales</option>
                                            <option  value="M" >Masivas</option>
                                        </select>
                                    </div>
                                </div> 

                                <div class="col-sm-12" style="text-align:right">
                                    <button type="button" class="btn btn-success" id="rango" name="rango">Buscar</button>    
                                </div>
                            </div>
                        </form>
                    </legend>
                </div>


            </div>
        </div>


        <div class="panel panel-default">
            <div class="panel-heading"><h2>Resultado de la Busqueda</h2></div>
            <div id="fileMessage" class="alert alert-danger alert-dismissible" role="alert" hidden>
                <button type="button" class="close" onclick="$('#fileMessage').hide()"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>Archivo no encontrado.</strong>
            </div>
            <div class="panel-body">

                <div class=" table-responsive">
                    <table id="loads" class="table table-striped table-bordered" 
                           cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Identificador del Cruce</th>
                                <th>Fecha de carga</th>
                                <th>Nombre del  archivo</th>
                                 <th>Tipo de Cruce</th>
                                
                                <th>Total de Registros</th>
                                <th>Descripcion</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tfoot> 
                            <tr> 
                                <th>Identificador del Cruce</th>
                                <th>Fecha de carga</th>
                                <th>Nombre del  archivo</th>
                                 <th>Tipo de Cruce</th>
                               
                                 <th>Total de Registros</th>
                                <th>Descripcion</th>
                                <th>Acciones</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:if test="${not empty ListaCruces}">
                                <c:forEach var="item" items="${ListaCruces}">
                                    <tr>
                                        <td>

                                            <c:if test="${item.status=='LOADED'}">
                                                <c:set var="altype" value="info" />
                                            </c:if>
                                            <c:if test="${item.status=='CREATED'}">
                                                <c:set var="altype" value="info" />
                                            </c:if>
                                            <c:if test="${item.status=='ERROR'}">
                                                <c:set var="altype" value="danger" />
                                            </c:if>   

                                            <label class="alert alert-${altype}">${item.id}</label>
                                        </td>
                                        <td>${item.endDate}</td>

                                        <td>
                                            ${item.fileName} 
                                        </td>
                                        <td>
                                            ${item.intersectionType.label} 
                                        </td>
                                        
                                        <td>
                                            ${item.totalOfRecors}</td>
                                        <td>
                                            ${item.detail}</td>

                                        <td>
                                            <form id="downloadBussForm${item.id}" name="downloadBussForm${item.id}" action="descargar-cruces.htm" >
                                                <input type="hidden" name="id" id="loadId" value="${item.id}">
                                                <button type="button"  class="btn btn-success btnDownloadBuss" id="btnDownloadBuss" name="btnDownloadBuss" 
                                                        itemId="${item.id}"
                                                        onclick="downloadFile(${item.id})">
                                                    <i class="fa fa-download" aria-hidden="true"></i>
                                                    Descargar
                                                </button>    
                                            </form>   
                                        </td>

                                    </tr>

                                </c:forEach>
                            </c:if>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>



        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/jquery.ui.widget.js"></script>
        <script src="resources/js/jquery.iframe-transport.js"></script>
        <script src="resources/js/jquery.fileupload.js"></script>
        <script src="resources/js/jquery.validate.min.js"></script>
        <script src="resources/js/tooltipster.bundle.js"></script>
        <script src="resources/js/custom-functionalities.js"></script>
         <script src="resources/js/moment.js"></script>
        <script>
            $(document).ready(function () {
//                                               

                $('#loads').DataTable({
                    "language": {
                        "url": "resources/js/Spanish.json"
                    },
                    "bFilter": false,
                    "bInfo": false,
                    dom: 'Blfrtip',
                });
                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Mi�rcoles", "Jueves", "Viernes", "S�bado", "Domingo"],
                    daysShort: ["Dom", "Lun", "Mar", "Mi�", "Jue", "Vie", "S�b", "Dom"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Hoy"
                };

                $('#fechaFin').datepicker({language: "es", autoclose: true, startDate: new Date(2014,0,0)});
                $('#fechaInicio').datepicker({language: "es", autoclose: true, startDate:new Date(2014,0,0)});
                
                
             

                $("#rango").click(function () {
                    var fechaInicio = document.getElementById("fechaInicio").value;
                var fechaFin = document.getElementById("fechaFin").value;
                    
                    if(moment(fechaInicio,"DD-MM-YYYY").isAfter(moment(fechaFin,"DD-MM-YYYY"))){
                        
                         newAlert('alert-danger', '<strong>ATENCION!</strong> La fecha final debe ser mayor a la incial');
                        
                    } else {
                        if (validateForm("rangoForm")) {
                            $('#rangoForm').submit();
                        }
                       
                    }
                });

                function newAlert(type, message) {
                    $("#alert-area").append($("<div class='alert alert-message " +
                            type + " fade in' data-alert><p> " + message + " </p></div>"));
                    $(".alert-message").delay(3000).fadeOut("slow", function () {
                        $(this).remove();
                    });
                }
            });
            
            function downloadFile(id) {
                $.ajax({
                    url: 'descargar-cruces.htm?id=' + id,
                    responseType: 'arraybuffer',
                    success: function(data) {
                        var link = document.createElement('a');
                        link.href = 'descargar-cruces.htm?id=' + id;
                        link.download = "DATA.zip";
                        link.click();
                    },
                    error: function() {
                        $("#fileMessage").show();
                    }
                });
            }

        </script> 

    </body>
</html>    

