/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: FosfecPortalController.java
 * Created on: 2017/01/31, 04:05:55 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gml.simpc.api.dto.FosfecResourcesDto;
import com.gml.simpc.api.enums.GraphicType;
import com.gml.simpc.api.services.FosfecPortalService;
import com.gml.simpc.api.utilities.Util;
import static com.gml.simpc.api.utilities.Util.FAILED_ALERT;
import com.gml.simpc.web.controller.ControllerDefinition;
import com.gml.simpc.web.utilities.ValueListService;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
@Controller
public class FosfecPortalController implements ControllerDefinition {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(FosfecPortalController.class);

    /**
     * Servicio para manipular listas de valor.
     */
    @Autowired
    private ValueListService valueListService;

    /**
     * Servicio para acceder a las consultas de Fosfec.
     */
    @Autowired
    private FosfecPortalService fosfecPortalService;

    /**
     * Muestra la informacion incial del portal FOSFEC.
     */
    @RequestMapping("banco-fosfec-portal")
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response) {
        ModelAndView modelAndView =
            new ModelAndView("banco-fosfec-portal");
        modelAndView.addObject("tab", "tabRecursos");
        init(modelAndView);
        return modelAndView;

    }

    /**
     * Muestra el resultado de recursos Fosfec de la consulta segun el filtro.
     * 
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = "recursosFosfec",
        method = RequestMethod.GET)
    public ModelAndView displayResultFosfecResources(HttpServletRequest request,
        HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView("banco-fosfec-portal");

        try {

            LOGGER.info("*****  entra al mentodo de recursos  fosfec");
            String ccf = Util.likeValue(request.getParameter("ccf"));
            String ccfVal = request.getParameter("ccf");
            String year = Util.nullValue(request.getParameter("anio"));
            String month = Util.likeValue(request.getParameter("mes"));
            List information;
            LOGGER.
                info("*****  a�o" + year + "***ccf" + ccf + "****mes" + month);

            information = this.fosfecPortalService.getFosfecResources(ccf, year,
                month);

            modelAndView.addObject("informationList", information);
            modelAndView.addObject("ccfSelected", ccfVal);
            modelAndView.addObject("yearSelected", year);
            modelAndView.addObject("monthSelected", month);
            modelAndView.addObject("tab", "tabRecursos");
            modelAndView.addObject("activateBtn", true);
            graphicResources(modelAndView, ccf, year, month);

        } catch (Exception uEx) {
            LOGGER.error("Error ", uEx);
            modelAndView.addObject("msg", "ERROR en la consulta");
            modelAndView.addObject("msgType", FAILED_ALERT);
        } finally {
            LOGGER.info("finally consolidados  portal fosfeccur");
        }
        init(modelAndView);
        return modelAndView;

    }

    /**
     * Muestra el grafico segun filtro.
     * 
     * @param modelAndView
     * @param ccf
     * @param year
     * @param month 
     */
    private void graphicResources(ModelAndView modelAndView,
        String ccf, String year, String month) {
        try {

            ObjectMapper mapper = new ObjectMapper();
            List<FosfecResourcesDto> resultgraphics = this.fosfecPortalService.
                graphicFosfecResources(ccf, year, month);

            List<String> tableg = new ArrayList();
            tableg.add("MES");
            tableg.add("SALUD");
            tableg.add("PENSI�N");
            tableg.add("CUOTA MONETARIA");
            tableg.add("BONO");

            modelAndView.addObject("graphicData", mapper.writeValueAsString(
                resultgraphics));
            modelAndView.addObject("titulo",
                "CONSULTA CONSOLIDADA  EJECUCI�N DE RECURSOS");
            modelAndView.addObject("titulografica", "RECURSOS");
            modelAndView.addObject("graphicType", GraphicType.L);
            modelAndView.addObject("graphicList", resultgraphics);
            modelAndView.addObject("titleList", tableg);

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msgType", FAILED_ALERT);

        } finally {
            LOGGER.info("Finaliza realiacion de  graficos");
        }

    }

    /**
     * Muestra el resulto de los recursos fosfec en detalle por ccf.
     * 
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = "detalleCcf",
        method = RequestMethod.GET)
    public ModelAndView displayResultFosfecDetail(HttpServletRequest request,
        HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView("banco-fosfec-portal");

        try {

            String ccf = Util.nullValue(request.getParameter("ccftab2"));
            String year = Util.nullValue(request.getParameter("aniotab2"));
            String month = Util.nullValue(request.getParameter("mestab2"));
            List information;
            
            information = this.fosfecPortalService.getFosfecResourcesDetail(ccf,
                year, month);

            modelAndView.addObject("detalleList", information);
            modelAndView.addObject("tab", "tabDetalle");
            modelAndView.addObject("ccfSelected2", ccf);
            modelAndView.addObject("yearSelected2", year);
            modelAndView.addObject("monthSelected2", month);
            modelAndView.addObject("activateBtn", true);
            graphicResourcesDetail(modelAndView, ccf, year, month);

        } catch (Exception uEx) {
            LOGGER.error("Error ", uEx);
            modelAndView.addObject("msg", "ERROR en la consulta");
            modelAndView.addObject("msgType", FAILED_ALERT);
        } finally {
            LOGGER.info("finally save in portal Fosfec");
        }
        init(modelAndView);
        return modelAndView;
    }

    /**
     * Muestra el grafico detalle Fosfec.
     * 
     * @param modelAndView
     * @param ccf
     * @param year
     * @param month 
     */
    private void graphicResourcesDetail(ModelAndView modelAndView,
        String ccf, String year, String month) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            List<FosfecResourcesDto> graphicResult = this.fosfecPortalService.
                graphicFosfecResourcesDetail(ccf, year,
                    month);

            List<String> tableg = new ArrayList();
            tableg.add("CAJA DE COMPENSACI�N");
            tableg.add("TOTAL");

            modelAndView.addObject("graphicData", mapper.writeValueAsString(
                graphicResult));
             modelAndView.addObject("titulo",
                "TOTAL DE EJECUCI�N DE RECURSOS POR CCF");
           LOGGER.info("******"+graphicResult);
        
            modelAndView.addObject("titulografica",
                "RECURSOS EJECUTAD POR CAJA DE COMPENSACI�N");
            modelAndView.addObject("graphicType", GraphicType.B);
            modelAndView.addObject("tab", "tabDetalle");
            modelAndView.addObject("graphicList2", graphicResult);
            modelAndView.addObject("titleList", tableg);

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msgType", FAILED_ALERT);

        } finally {
            LOGGER.info("Finaliza realiacion de  graficos");
        }
    }

    /**
     * Inicia los objetos para el controler.
     *
     * @param modelAndView
     */
    private void init(ModelAndView modelAndView) {
        try {
            List<Integer> years = new ArrayList<>();
            Calendar c1 = Calendar.getInstance();
            for (int i = 2014; i <= c1.get(Calendar.YEAR); i++) {
                years.add(i);
            }
            modelAndView.addObject("ccfs", this.valueListService.
                    getFoundationList());
            modelAndView.addObject("anios", years);
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msgType", FAILED_ALERT);

        } finally {
            LOGGER.info("Finaliza  graficos");
        }
    }
}
