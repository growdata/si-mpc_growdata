/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: TrainingPreparedStatementSetter.java
 * Created on: 2017/01/20, 12:16:58 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.writer;

import com.gml.simpc.api.dto.ProgramDto;
import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import org.springframework.batch.item.database.ItemPreparedStatementSetter;

/**
 * Clase para setear propiedades de dto en consulta de job de escritura a master
 * y stage
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica
 * Jim&eacute;nez Rozo</a>
 */
public class TrainingPreparedStatementSetter
    implements ItemPreparedStatementSetter<ProgramDto> {
    
    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER = 
        Logger.getLogger(TrainingPreparedStatementSetter.class);

    @Override
    public void setValues(ProgramDto programDto, PreparedStatement ps)
        throws SQLException {
        ThreadLocalJob.setProcesed();
        JobResultDto jobResult = ThreadLocalJob.getResult();
        int line = jobResult.getProcesedLines();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        ps.setString(1, programDto.getProgramCode());
        ps.setString(2, programDto.getProgramName());
        ps.setString(3, programDto.getProgramType());
        ps.setString(4, programDto.getCourseType());
        ps.setString(5, programDto.getCcfCode());
        ps.setString(6, programDto.getInstCode());
        ps.setString(7, programDto.getHeadqCode());
        ps.setString(8, programDto.getStateCode());
        ps.setString(9, programDto.getCityCode());
        ps.setString(10, programDto.getCertified());
        ps.setString(11, programDto.getCertName());
        ps.setString(12, programDto.getOtherCertName());
        ps.setString(13, programDto.getIssuedCertName());
        ps.setString(14, programDto.getCine());
        ps.setString(15, programDto.getNecesity());
        ps.setString(16, programDto.getModuleCode());
        ps.setString(17, programDto.getModuleName());
        ps.setString(18, programDto.getTotalHours());
        ps.setString(19, programDto.getSchedule());
        ps.setString(20, programDto.getEnrollmentCostType());
        ps.setString(21, programDto.getEnrollmentCost());
        ps.setString(22, programDto.getOtherCostType());
        ps.setString(23, programDto.getOtherCost());
        try {
            Date startDate = dateFormat.parse(programDto.getStartDate());
            Date endDate = dateFormat.parse(programDto.getEndDate());
            ps.setDate(24, new java.sql.Date(startDate.getTime()));
            ps.setDate(25, new java.sql.Date(endDate.getTime()));
        } catch (ParseException ex) {
            LOGGER.error("Error parsing date in job "+ex);
        }
        ps.setString(26, Integer.toString(line));
    }

}
