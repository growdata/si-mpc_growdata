/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: TrainingPortalController.java
 * Created on: 2017/02/01, 12:54:58 M
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.gml.simpc.api.entity.valuelist.Foundation;
import com.gml.simpc.api.entity.valuelist.InstitutionType;
import com.gml.simpc.api.entity.valuelist.ProgramType;
import com.gml.simpc.api.services.TrainingService;
import com.gml.simpc.web.controller.ControllerDefinition;
import com.gml.simpc.web.utilities.ValueListService;
import com.google.common.collect.Table;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller para portal de capacitacion.
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Controller
public class TrainingPortalController implements ControllerDefinition {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER
            = Logger.getLogger(TrainingPortalController.class);

    /**
     * Servicio para manipular listas de valor.
     */
    @Autowired
    private ValueListService valueListService;

    /**
     * Servicio para consulta de informaci?n de la master data relacionada con
     * capacitaciones.
     */
    @Autowired
    private TrainingService trainingService;

    /**
     * Muestra la informacion incial del portal Capacitacion.
     * 
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping("portal-capacitacion")
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
            HttpServletResponse response) {

        ModelAndView modelAndView = new ModelAndView("portal-capacitacion");

        String activeTab = null;
        String activeTabCcf = request.getParameter("activeTabCcf");
        String activeTabInst = request.getParameter("activeTabInst");
        boolean resultsFinded = false;
        boolean resultOk;

        if (activeTabCcf != null && "true".equals(activeTabCcf)) {
            activeTab = "ccf";
        }
        if (activeTabInst != null && "true".equals(activeTabInst)) {
            activeTab = "instituciones";
        }
        //Setting value lists
        List<InstitutionType> instTypeList = this.valueListService.
                getInstitutionTypeList();
        List<Foundation> ccfList = this.valueListService.getFoundationList();
        List<ProgramType> trainTypeList = this.valueListService.getProgramTypeList();

        //Getting results 
        Table<Long, Long, Integer> resultTable = null;

        if (activeTab != null) {
            String xKey = null;
            List<InstitutionType> resultsInstTypeList = null;
            List<String> yKeys = new ArrayList();
            List<String> labels = new ArrayList();
            ArrayList<JSONObject> jsonResultsList;
            //Receiving filter parameters
            String instTypeSel = request.getParameter("instTypeSel");
            String ccfSel = request.getParameter("ccfSel");
            String anio1Sel = request.getParameter("anio1Sel");
            String mes1Sel = request.getParameter("mes1Sel");
            String anio2Sel = request.getParameter("anio2Sel");
            String mes2Sel = request.getParameter("mes2Sel");

            LOGGER.info("Buscando informacion para portal de capacitacion  "
                    + "con parametros:  ccfSel: " + ccfSel
                    + ", a�o: " + anio1Sel + ", mes: " + mes1Sel);

            if (ccfSel == null) {
                ccfSel = "0";
            }
            if (instTypeSel == null) {
                instTypeSel = "0";
            }
            if (anio1Sel == null) {
                anio1Sel = "0";
            }
            if (mes1Sel == null) {
                mes1Sel = "0";
            }
            if (anio2Sel == null) {
                anio2Sel = "0";
            }
            if (mes2Sel == null) {
                mes2Sel = "0";
            }

            switch (activeTab) {
                case "ccf":
                    LOGGER.info("Buscando por caja de compensaci�n:  ccfSel: "
                            + ccfSel + ", a�o: " + anio1Sel + ", mes: " + mes1Sel);
                    activeTab = "ccf";
                    resultTable = this.trainingService.getModuleCounts(ccfSel,
                            instTypeSel, anio1Sel, mes1Sel);
                    resultsFinded = true;
                    //Preparing graphic results
                    xKey = "instTypes";
                    resultsInstTypeList = instTypeList;
                    for (ProgramType trainType : trainTypeList) {
                        yKeys.add(trainType.getDescription());
                        labels.add(trainType.getDescription());
                    }
                    jsonResultsList = getCcfJsonResult(resultTable, resultsInstTypeList,
                            trainTypeList, xKey);
                    modelAndView.addObject("jsonResultsList", jsonResultsList);
                    modelAndView.addObject("ccfSel", ccfSel);
                    modelAndView.addObject("anio1Sel", anio1Sel);
                    modelAndView.addObject("mes1Sel", mes1Sel);
                    break;
                case "instituciones":
                    LOGGER.info("Buscando por institucion: tipoInstitucionSel: "
                            + instTypeSel + ", a�o: " + anio2Sel + ", mes: " + mes2Sel);
                    activeTab = "instituciones";
                    resultTable = this.trainingService.getModuleCounts(
                            ccfSel, instTypeSel, anio2Sel, mes2Sel);
                    resultsFinded = true;
                    //Preparing graphic results
                    xKey = "capTypes";
                    resultsInstTypeList = new ArrayList<>();
                    for (InstitutionType instType : instTypeList) {
                        if (Objects.
                                equals(Long.parseLong(instTypeSel), instType.getId())) {
                            resultsInstTypeList.add(instType);
                        }
                    }
                    yKeys.add("nro_modulos");
                    for (ProgramType trainType : trainTypeList) {
                        labels.add(trainType.getDescription());
                    }
                    jsonResultsList = getInstJsonResult(resultTable, resultsInstTypeList,
                            trainTypeList, xKey);
                    modelAndView.addObject("jsonResultsList", jsonResultsList);
                    modelAndView.addObject("instTypeSel", instTypeSel);
                    modelAndView.addObject("anio2Sel", anio2Sel);
                    modelAndView.addObject("mes2Sel", mes2Sel);
                    break;
            }
            modelAndView.addObject("resultsInstTypeList", resultsInstTypeList);
            modelAndView.addObject("xKey", xKey);
            modelAndView.addObject("yKeys", yKeys);
            modelAndView.addObject("labels", labels);
        } else {
            activeTab = "ccf";
        }

        List<Integer> years = new ArrayList<>();
        Calendar c1 = Calendar.getInstance();
        for (int i = 2014; i <= c1.get(Calendar.YEAR); i++) {
            years.add(i);
        }
        modelAndView.addObject("resultsFinded", resultsFinded);
        modelAndView.addObject("anios", years);
        modelAndView.addObject("resultsList", resultTable);
        modelAndView.addObject("activeTab", activeTab);
        modelAndView.addObject("instTypeList", instTypeList);
        modelAndView.addObject("ccfList", ccfList);
        modelAndView.addObject("trainTypeList", trainTypeList);
        resultOk = true;
        modelAndView.addObject("resultOk", resultOk);

        return modelAndView;

    }

    /**
     * M&eacute;todo que captura el resultado de una CCF.
     * 
     * @param resultTable
     * @param resultsInstTypeList
     * @param trainTypeList
     * @param xKey
     * @return 
     */
    private ArrayList<JSONObject> getCcfJsonResult(
            Table<Long, Long, Integer> resultTable,
            List<InstitutionType> resultsInstTypeList,
            List<ProgramType> trainTypeList, String xKey) {

        ArrayList<JSONObject> jsonResultsList;
        jsonResultsList = new ArrayList<>();
        try {
            for (InstitutionType instType : resultsInstTypeList) {
                JSONObject item = new JSONObject();
                item.put(xKey, instType.getName());
                for (ProgramType trainType : trainTypeList) {

                    if ((resultTable.get(trainType.getId(), instType.getId()))
                            != null) {
                        item.put(trainType.getDescription(), resultTable.get(
                                trainType.getId(), instType.getId()));
                    } else {
                        item.put(trainType.getDescription(), 0);
                    }
                }
                jsonResultsList.add(item);
            }
        } catch (Exception e) {
            LOGGER.error("ERRROR parsing results to json format" + e);
        }

        return jsonResultsList;
    }

    /**
     * M&eacute;todo que captura el resultado de una instituci&oacute;n.
     * 
     * @param resultTable
     * @param resultsInstTypeList
     * @param trainTypeList
     * @param xKey
     * @return 
     */
    private ArrayList<JSONObject> getInstJsonResult(
            Table<Long, Long, Integer> resultTable,
            List<InstitutionType> resultsInstTypeList,
            List<ProgramType> trainTypeList, String xKey) {

        ArrayList<JSONObject> jsonResultsList;
        jsonResultsList = new ArrayList<>();
        try {
            for (ProgramType trainType : trainTypeList) {
                JSONObject item = new JSONObject();
                item.put(xKey, trainType.getDescription());
                int total = 0;
                for (InstitutionType instType : resultsInstTypeList) {
                    if ((resultTable.get(trainType.getId(), instType.getId()))
                            != null) {
                        total = total + resultTable.get(
                                trainType.getId(), instType.getId());

                    }
                }
                item.put("nro_modulos", total);
                jsonResultsList.add(item);
            }
        } catch (Exception e) {
            LOGGER.error("ERRROR parsing results to json format" + e);
        }

        return jsonResultsList;
    }
}
