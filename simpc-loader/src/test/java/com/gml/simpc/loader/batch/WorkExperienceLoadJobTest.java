package com.gml.simpc.loader.batch;

import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import com.gml.simpc.loader.thread.ThreadLocalValueList;
import com.gml.simpc.loader.utilities.ValueListTestService;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-batch.xml",
    "classpath:batch_jobs/curriculumVitae/workExperience/workExperienceJob.xml"})
public class WorkExperienceLoadJobTest {

     /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER = 
        Logger.getLogger(WorkExperienceLoadJobTest.class);
    
    /**
     * Servicio para ejecutar jobs en modo pruebas
     */
    @Autowired
    JobLauncherTestUtils jobLauncherTestUtils;

    
    /**
     * Test para probar carga de hojas de vida - experiencia laboral
     */
    @Test
    public void WorkExperienceLoadJobTestSucces() throws Exception {
        LOGGER.info("Running WorkExperienceLoadJobTestSucces ");

        //Inicializamos local thread
        ThreadLocalJob.init();
        
         //Inicializamos listas de valores
        ValueListTestService valService = new ValueListTestService();
        valService.updateAll();
        ThreadLocalValueList.set(valService.getValueList());

        // Inicializamos el job.
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath",
            "file:loadTestFiles/curriculum vitae/workExperience/" +
            "workExperienceTestFile.txt");

        // Lanzamos el job.
        JobExecution jobExecution = jobLauncherTestUtils.
            launchJob(jobParametersBuilder.toJobParameters());

        for (Throwable t : jobExecution.getAllFailureExceptions()) {
            t.printStackTrace();
        }

        JobResultDto jobResult = ThreadLocalJob.getResult();
        LOGGER.info(jobResult.toString());

        if (!jobResult.getErrorList().isEmpty()) {
            LOGGER.info(jobResult.getErrorsString());
           
        }

        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().
            getExitCode());
        Assert.assertTrue(jobResult.getErrorList().isEmpty());

        ThreadLocalJob.delete();

    }
    
    /**
     * Test para probar carga de hojas de vida - experiencia laboral error 
     * experienceType
     */
    @Test
    public void WorkExperienceLoadJobTestErrorExperienceType() throws Exception {
        LOGGER.info("Running WorkExperienceLoadJobTestErrorExperienceType ");

        //Inicializamos local thread
        ThreadLocalJob.init();
        
         //Inicializamos listas de valores
        ValueListTestService valService = new ValueListTestService();
        valService.updateAll();
        ThreadLocalValueList.set(valService.getValueList());

        // Inicializamos el job.
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath",
            "file:loadTestFiles/curriculum vitae/workExperience/" +
            "workExperienceTestFile-experienceTypeError.txt");

        // Lanzamos el job.
        JobExecution jobExecution = jobLauncherTestUtils.
            launchJob(jobParametersBuilder.toJobParameters());

        for (Throwable t : jobExecution.getAllFailureExceptions()) {
            t.printStackTrace();
        }

        JobResultDto jobResult = ThreadLocalJob.getResult();
        LOGGER.info(jobResult.toString());
        LOGGER.info(jobResult.toString());

        if (!jobResult.getErrorList().isEmpty()) {
            LOGGER.info(jobResult.getErrorsString());
             System.out.println(jobResult.getErrorsString());
        }

        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().
            getExitCode());
        Assert.assertTrue(jobResult.getErrorList().size() == 1);

        ThreadLocalJob.delete();

    }
    
    /**
     * Test para probar carga de hojas de vida - experiencia laboral error
     * Sector
     */
    @Test
    public void WorkExperienceLoadJobTestErrorSector() throws Exception {
        LOGGER.info("Running WorkExperienceLoadJobTestErrorSector ");

        //Inicializamos local thread
        ThreadLocalJob.init();
        
         //Inicializamos listas de valores
        ValueListTestService valService = new ValueListTestService();
        valService.updateAll();
        ThreadLocalValueList.set(valService.getValueList());

        // Inicializamos el job.
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath",
            "file:loadTestFiles/curriculum vitae/workExperience/" +
            "workExperienceTestFile-sectorError.txt");

        // Lanzamos el job.
        JobExecution jobExecution = jobLauncherTestUtils.
            launchJob(jobParametersBuilder.toJobParameters());

        for (Throwable t : jobExecution.getAllFailureExceptions()) {
            t.printStackTrace();
        }

        JobResultDto jobResult = ThreadLocalJob.getResult();
        LOGGER.info(jobResult.toString());
        LOGGER.info(jobResult.toString());

        if (!jobResult.getErrorList().isEmpty()) {
            LOGGER.info(jobResult.getErrorsString());
            LOGGER.info(jobResult.getErrorsString());
        }

        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().
            getExitCode());
        Assert.assertTrue(jobResult.getErrorList().size() == 1);

        ThreadLocalJob.delete();

    }
    
    /**
     * Test para probar carga de hojas de vida - error cargo equivalente 
     * 
     */
    @Test
    public void WorkExperienceLoadJobTestErrorEquivalentPosition() throws 
        Exception {
        LOGGER.info("Running WorkExperienceLoadJobTestErrorEquivalent" +
            "Position ");

        //Inicializamos local thread
        ThreadLocalJob.init();
        
         //Inicializamos listas de valores
        ValueListTestService valService = new ValueListTestService();
        valService.updateAll();
        ThreadLocalValueList.set(valService.getValueList());

        // Inicializamos el job.
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath",
            "file:loadTestFiles/curriculum vitae/workExperience/" +
            "workExperienceTestFile-equivalentPositionError.txt");

        // Lanzamos el job.
        JobExecution jobExecution = jobLauncherTestUtils.
            launchJob(jobParametersBuilder.toJobParameters());

        for (Throwable t : jobExecution.getAllFailureExceptions()) {
            t.printStackTrace();
        }

        JobResultDto jobResult = ThreadLocalJob.getResult();
        LOGGER.info(jobResult.toString());
        LOGGER.info(jobResult.toString());

        if (!jobResult.getErrorList().isEmpty()) {
            LOGGER.info(jobResult.getErrorsString());
            LOGGER.info(jobResult.getErrorsString());
        }

        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().
            getExitCode());
        Assert.assertTrue(jobResult.getErrorList().size() == 1);

        ThreadLocalJob.delete();

    }
}
