DROP TABLE PARAMETROS;

CREATE TABLE PARAMETROS 
 (	ID NUMBER(19,0), 
      DESCRIPCION VARCHAR2(255 CHAR), 
      NOMBRE VARCHAR2(255 CHAR),
      NOMBRE_ESP VARCHAR2(255 CHAR),
      VALOR VARCHAR2(255 CHAR)
 );

CREATE UNIQUE INDEX UK_80LCPU6RLR3LXBH8TQCS5T281 ON PARAMETROS (NOMBRE) 
; 

ALTER TABLE PARAMETROS MODIFY (ID NOT NULL ENABLE);
ALTER TABLE PARAMETROS MODIFY (NOMBRE NOT NULL ENABLE);
ALTER TABLE PARAMETROS MODIFY (VALOR NOT NULL ENABLE);
ALTER TABLE PARAMETROS ADD PRIMARY KEY (ID);
ALTER TABLE PARAMETROS ADD CONSTRAINT UK_80LCPU6RLR3LXBH8TQCS5T281 UNIQUE (NOMBRE);

INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('1','Tiempo (en minutos) que transcurren para el vencimiento de la sesi�n de un usuario por inactividad en el sistema','VENCIMIENTO_SESION','15','VENCIMIENTO SESI�N');
INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('2','Ruta de almacenamiento interno del Manual de Ayudas en L�nea que muestra el sistema','AYUDA_EN_LINEA_PATH','C:\config\simpc\manuales\Ayudas en linea V3 - 02-11-2016.pdf','RUTA AYUDA EN L�NEA ');
INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('3','Ruta de almacenamiento interno del Manual de Usuario que muestra el sistema','MANUAL_USUARIO_PATH','C:\config\simpc\manuales\Ayudas en linea V3 - 02-11-2016.pdf','RUTA MANUAL DE USUARIO');
INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('4','Dominio de la aplicaci�n','DOMINIO','localhost:7001/masterdata/','DOMINIO');
INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('5','Ruta de almacenamiento interno del Manual de Administrador que muestra el sistema','MANUAL_ADMINISTRADOR_PATH','C:\config\simpc\manuales\Ayudas en linea V3 - 02-11-2016.pdf','RUTA MANUAL DE ADMINISTRADOR');
INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('6','Carpeta donde se almacenan los archivos que son cargados al sistema mediante el m�dulo de cargues','ARCHIVOS_CARGA_PATH','C:\SIMPC\UPLOADS\','RUTA ARCHIVOS DE CARGA');
INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('7','Ubicaci�n donde se almacenan los archivos que se generan de los cruces','ARCHIVOS_CRUCE_PATH','C:\config\simpc\cruces\','RUTA ARCHIVOS DE CRUCES ');
INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('8','Nombre que aparece en la firma de los correos que se env�an desde el sistema','NOMBRE_FIRMA','FULANO','NOMBRE FIRMA DE CORREO');
INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('9','Cargo que se coloca en la firma de los correos que se env�an desde el sistema, teniendo en cuenta el nombre de la persona que se puso en la firma','CARGO_FIRMA','Gerente de Cuenta','CARGO FIRMA DE CORREO');
INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('10','Asunto que se coloca en los correos enviados por el sistema al editar o activar una instituci�n oferente','ACTIVATE_INSTITUTION','Edici�n de instituci�n','ACTIVACI�N DE INSTITUCI�N');
INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('11','Asunto que se coloca en los correos enviados por el sistema al crear una instituci�n oferente','CREATE_INSTITUTION','Creacion de institucion','CREACI�N DE INSTITUCI�N');
INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('12','Asunto que se coloca en los correos que env�a el sistema cuando se realiza la modificaci�n de los datos un usuario','USER_MESSAGE','Modificacion de datos de usuario','MENSAJE DE USUARIO');
INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('13','Asunto que se muestra en el correo que se env�a cuando se hace la aprobaci�n de una instituci�n','APPROVAL_INSTITUTION','Aprobaci�n de Instituci�n','APROBACI�N DE INSTITUCI�N');
INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('14','Asunto que se muestra en el correo que se env�a cuando se rechaza una instituci�n oferente','REJECT_INSTITUTION','Desaprobacion de institucion','RECHAZO DE INSTITUCI�N');
INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('15','Asunto que se coloca en los correos enviados por el sistema al crear una sede','SAVE_HEADQUARTER','Creaci�n de Sede','CREACI�N DE SEDES');
INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('16','Asunto que se env�a en los correos informando que se ha realizado el cargue de un archivo','LOAD_QUEUE','Informaci�n de cargue de archivos','CARGUE DE ARCHIVOS');
INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('18','Asunto que se env�a en los correos para solicitar al usuario que active su contrase�a','USER_PASSWORD','Activaci�n de  contrase�a','ACTIVACI�N CONTRASE�A DE USUARIO');
INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('19','Iindica en d�as cada cu�nto es el vencimiento de la contrase�a de los usuarios externos.  No aplica para los usuarios del Ministerio.','PASSWORD_EXPIRATION','30','EXPIRACI�N DE CONTRASE�A');
INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('20','Ubicaci�n en disco del Jar descargable.','JAR_DESCARGABLE_PATH','C:\Users\Usuario\Desktop\GML\javaee-endorsed-api-7.0.jar','RUTA JAR DESCARGABLE');
INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('21','Asunto en el correo que el sistema env�a al usuario para informar que el cruce de informaci�n masivo se ha realizado','MASS_CONSULT','Consulta Masiva','CONSULTA MASIVA');
INSERT INTO PARAMETROS (ID,DESCRIPCION,NOMBRE,VALOR,NOMBRE_ESP) VALUES ('22','Asunto en el correo que el sistema env�a al Administrador si sucede alg�n error en el proceso autom�tico que se ejecuta a las 12:00 p.m. para la actulizaci�n de la tabla de programas.','ERROR','Error de Actualizaci�n','ERROR DE ACTUALIZACI�N');  

COMMIT;