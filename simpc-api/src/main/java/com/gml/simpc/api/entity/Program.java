/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Program.java
 * Created on: 2016/11/27, 10:10:45 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import com.gml.simpc.api.entity.valuelist.Certifications;
import com.gml.simpc.api.entity.valuelist.Cine;
import com.gml.simpc.api.entity.valuelist.DeliveredCertification;
import com.gml.simpc.api.entity.valuelist.Foundation;
import com.gml.simpc.api.entity.valuelist.ProgramType;
import com.gml.simpc.api.enums.ProgramNecessity;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entidas para manejo de programas.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Table(name = "PROGRAMAS")
@Entity
public class Program implements Serializable {

    /**
     * ID del programa
     */
    @Id
    @Column(name = "ID", length = 10)
    @GeneratedValue(strategy = GenerationType.AUTO, generator =
        "program_seq_gen")
    @SequenceGenerator(name = "program_seq_gen", sequenceName =
        "PROGRAM_SEQ", initialValue = 20001, allocationSize = 1)
    private Long id;

    /**
     * codigo del programa
     */
    @Size(max = 10, message = "C�digo del programa no puede tener m�s de " +
        "10 caracteres.")
    @Column(name = "CODIGO", length = 10, nullable = true)
    private String code;

    /**
     * nombre del municipio.
     */
    @NotNull(message = "El campo Nombre del programa no puede estar vac�o.")
    @Size(max = 150, message = "Nombre del programa no puede tener m�s de " +
        "150 caracteres.")
    @Column(name = "NOMBRE", length = 150, nullable = false)
    private String name;

    /**
     * Tipo de capacitaci&oacute;n
     */
    @ManyToOne
    @NotNull(message = "El programa tiene que tener un tipo de capacitaci�n")
    @JoinColumn(name = "TIPO_CAPACITACION", nullable = false)
    private ProgramType type;

    /**
     * Certificaci&oacute;n opcion si o no
     */
    @Column(name = "CERTIFICACION_CALIDAD")
    private String qualityCertificate;
    /**
     * Area de desempe&ntilde;o- Cine
     */

    @ManyToOne
    @JoinColumn(name = "CINE", nullable = false)
    private Cine cine;
    /**
     * Duracion en horasin
     */
    @NotNull(message = "El campo horas no puede estar vac�o")
    @Column(name = "DURACION", nullable = false)
    private int hours;

    /**
     * certificaci&oacute;n
     */
    @ManyToOne
    @JoinColumn(name = "CERTIFICACION", nullable = true)
    private Certifications certification;
    
    /**
     * certificaci&oacute;n emitida
     */
    @ManyToOne
    @JoinColumn(name = "CERTIFICACION_EMITIDA", nullable = true)
    private DeliveredCertification deliveredCertification;
    
    /**
     * certificaci&oacute;n emitida
     */
    @Size(max = 80, message = "El nombre de la certificaci�n no puede tener "
            + "m�s de 80 caracteres.")
    @Column(name = "CUAL_CERTIFICACION", length = 80, nullable = true)
    private String whichName;

    /**
     * certificaci&oacute;n
     */
    @NotNull(message = "El campo necesidad no puede estar vac�o")
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "NECESIDAD", nullable = false)
    private ProgramNecessity necessity;

    /**
     * CCF del programa
     */
    @ManyToOne
    @JoinColumn(name = "CODIGO_CCF", nullable = false)
    private Foundation ccf;

    /**
     * Id de Usuario de creaci&oacute:n. Campo de Auditor&iacute;a.
     */
    @ManyToOne
    @JoinColumn(name = "ID_USUARIO_CREACION", nullable = false)
    private User creationUserId;
    /**
     * fecha de creaci&oacute:n. Campo de Auditor&iacute;a.
     */
    @Column(name = "FECHA_CREACION", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    /**
     * Id de Usuario de modificaci&oacute;n.Campo de Auditor&iacute;a.
     */
    @ManyToOne
    @JoinColumn(name = "ID_USUARIO_MODIFICACION")
    private User modificationUserId;
    /**
     * fecha de modificaci&oacute:n. Campo de Auditor&iacute;a.
     */
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modificationDate;
    /**
     * Campo para observaciones en los programas
     */

    @Column(name = "OBSERVACIONES", length = 200, nullable = true)
    private String observations;

    /**
     * Constructor por defecto.
     */
    public Program() {
        this.id = null;
    }

    /**
     * Constructor por tipo y sede.
     *
     * @param type
     * @param
     */
    public Program(ProgramType type) {
        this.type = type;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCcf(Foundation ccf) {
        this.ccf = ccf;
    }

    public Foundation getCcf() {
        return ccf;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQualityCertificate() {
        return qualityCertificate;
    }

    public void setQualityCertificate(String qualityCertificate) {
        this.qualityCertificate = qualityCertificate;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public Certifications getCertification() {
        return certification;
    }

    public void setCertification(Certifications certification) {
        this.certification = certification;
    }
    public DeliveredCertification getDeliveredCertification() {
        return deliveredCertification;
    }

    public void setDeliveredCertification(DeliveredCertification deliveredCertification) {
        this.deliveredCertification = deliveredCertification;
    }
    
    public String getWhichName() {
        return whichName;
    }

    public void setWhichName(String whichName) {
        this.whichName = whichName;
    }

    public Cine getCine() {
        return cine;
    }

    public void setCine(Cine cine) {
        this.cine = cine;
    }

    public ProgramNecessity getNecessity() {
        return necessity;
    }

    public void setNecessity(ProgramNecessity necessity) {
        this.necessity = necessity;
    }

    public User getCreationUserId() {
        return creationUserId;
    }

    public void setCreationUserId(User creationUserId) {
        this.creationUserId = creationUserId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public User getModificationUserId() {
        return modificationUserId;
    }

    public void setModificationUserId(User modificationUserId) {
        this.modificationUserId = modificationUserId;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public ProgramType getType() {
        return type;
    }

    public void setType(ProgramType type) {
        this.type = type;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Programa{").append("Id=").append(id).
            append(", C?digo=").append(code).
            append(", Nombre=").append(name).
            append(", Tipo Programa=").append(type).
            append(", C?digo Cine=").append(cine.getValue()).
            append(", Duraci?n=").append(hours).
            append(",Certificaci?n =").append(certification).
            append(",Certificaci?n emitida=").append(deliveredCertification).
            append(", Necesidad=").append(necessity).
            append(", Id del Usuario creador=").append(creationUserId).
            append(", Fecha de Creaci?n=").append(creationDate).
            append(", Id del Usuario Modificador=").append(modificationUserId).
            append(", Fecha de Modificaci?n=").append(modificationDate)
            .append('}');
        return stringBuilder.toString();
    }

}
