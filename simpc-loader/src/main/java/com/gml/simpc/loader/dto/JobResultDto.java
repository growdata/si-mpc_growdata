/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: JobResultDto.java
 * Created on: 2017/01/06, 12:36:46 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica Jim&eacute;nez Rozo</a>
 */
public class JobResultDto {
    
    /**
     * Total de registros procesados. Necesario para poder determinar l�nea
     * en la que se genera cada error.
     */
    private int procesedLines;
    
    /**
     * Estado del job al finalizar.
     */
    private String status;
    
    /**
     * Lista de errores de validacion
     */
    private List <ValidationErrorDto> errorList;
    
    /*-----------------------------------------------------------------------*/

    public JobResultDto() {
        this.procesedLines=0;
        this.errorList =  new ArrayList<>();
    }

    /*-----------------------------------------------------------------------*/
    
    public int getProcesedLines() {
        return procesedLines;
    }

    public void setProcesedLines(int procesedLines) {
        this.procesedLines = procesedLines;
    }

    public List<ValidationErrorDto> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<ValidationErrorDto> errorList) {
        this.errorList = errorList;
    }
    
    public int getSuccessLines() {
        int successLines = procesedLines - errorList.size();
        return successLines;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    
    /*-----------------------------------------------------------------------*/

     /**
     * Usado para debug
     * @return 
     */
    @Override
    public String toString() {
       StringBuilder sb = new StringBuilder();
        sb.append("Ending job in status ");
        sb.append(status);
        sb.append(" with following result -> Totals rows:");
        sb.append(procesedLines);
        sb.append(". Errors: ");
        sb.append(errorList.size());
        sb.append(". Success: ");
        sb.append(getSuccessLines());
       
        return sb.toString();
    }

     /**
     * Usado para paso de lista de errores a string
     */
    public String getErrorsString() {
        
        StringBuilder buffer = new StringBuilder();
        boolean first = true;
        
        for (ValidationErrorDto validationError : errorList) {
            if (!first) {
                buffer.append("; ");
            }
            buffer.append("Error en el Registro ");
            buffer.append(validationError.getLine());
            buffer.append(": ");
            buffer.append(validationError.getMessage());
            first = false;
        }
        return buffer.toString();
    }
}
