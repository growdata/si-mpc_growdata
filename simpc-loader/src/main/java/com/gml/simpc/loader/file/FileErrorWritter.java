/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: FileErrorWritter.java
 * Created on: 2017/01/24, 06:26:43 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.file;

import com.gml.simpc.api.entity.LoadProcess;
import com.gml.simpc.loader.dto.ValidationErrorDto;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Clase creada para escribir archivos con lista de errores de carga
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica
 * Jim&eacute;nez Rozo</a>
 */
public class FileErrorWritter {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.
        getLogger(FileErrorWritter.class);

    /**
     * Metodo que escribe archivo de error de carga
     *
     * @param loadProcess
     * @param errorsList
     * @param type
     */
    public void writeErrorFile(LoadProcess loadProcess,
        List<ValidationErrorDto> errorsList, String type) {
        LOGGER.info("Ejecutando metodo [ writeErrorFile ]");
        
        try {
            String path = loadProcess.getNewFileRoot() + 
                loadProcess.getNewFilePath();
            LOGGER.info(path);
            String fileName = getFileName(type, loadProcess);
            LOGGER.info(fileName);
            File file = new File(path + fileName);
            file.getParentFile().mkdirs();
            boolean fileCreated = file.createNewFile();

            try {
                writeFileText(fileCreated, file, errorsList);
            } catch (IOException ex) {
                LOGGER.error("Error ", ex);
            }
        } catch (IOException ex) {
            LOGGER.error("Error escribiendo archivo de errores", ex);
        }
    }

    /**
     * Escribe texto en el archivo.
     *
     * @param fileCreated
     * @param file
     * @param errorsList
     *
     * @return
     *
     * @throws IOException
     */
    private void writeFileText(boolean fileCreated, File file,
        List<ValidationErrorDto> errorsList) throws IOException {
        LOGGER.info("Ejecutando metodo [ writeFileText ]");
        
        if (fileCreated) {
            LOGGER.info("Archivo creado.");
            try (FileOutputStream fos = new FileOutputStream(file.getPath());
                OutputStreamWriter osw = new OutputStreamWriter(fos,
                    "UTF-8"); Writer writer = new BufferedWriter(osw)) {

                writer.write("Linea,Error\n");

                for (ValidationErrorDto validationErrorDto : errorsList) {
                    writer.write(validationErrorDto.getLine() + "," +
                        validationErrorDto.getMessage() + "\n");
                }
            }
        }
    }

    /**
     * Obtiene el nombre del archivo.
     *
     * @param type
     * @param loadProcess
     *
     * @return
     */
    private String getFileName(String type, LoadProcess loadProcess) {
        String fileName = "";

        if ("structure".equals(type)) {
            fileName = loadProcess.getStructureErrorFileName();
        } else if ("bussiness".equals(type)) {
            fileName = loadProcess.getBussinessErrorFileName();
        }

        return fileName;
    }
}
