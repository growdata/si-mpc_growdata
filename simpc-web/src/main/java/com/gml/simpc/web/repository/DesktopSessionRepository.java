/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: DesktopSessionRepository.java
 * Created on: 2016/10/19, 02:03:51 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository;

import com.gml.simpc.api.entity.DesktopSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repositorio para sesiones de escritorio.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Transactional
public interface DesktopSessionRepository
    extends JpaRepository<DesktopSession, Long> {

    /**
     * Busca si el token coincide con uno existente.
     *
     * @param token Token generado en el momento del registro
     *
     * @return Entidad de activaci&ocute;n
     */
    DesktopSession findByUserToken(String token);

    /**
     * Elimina los tokens de sesi&oacute;n.
     *
     * @return
     */
    @Modifying
    @Query(value = "DELETE FROM SESIONES_ESCITORIO WHERE " +
        "FECHA_SESION + INTERVAL '15' MINUTE < SYSDATE", nativeQuery = true)
    Integer deleteExpiratedTokens();
}
