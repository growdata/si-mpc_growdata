<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CAPACITACI�N PARA LA INSERCI�N Y REINSERCI�N LABORAL</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster.bundle.min.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster-sideTip-shadow.min.css" rel="stylesheet"/>
        <link href="resources/css/morris.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <div class="col-sm-3" style="padding:0px;">
                        <img src="resources/images/logo_mpc.png" width="100%" alt=""/>
                    </div>
                    <div class="col-sm-8">
                        <a class="navbar-brand" href="/masterdata/index.htm">Mecanismo de Protecci�n al Cesante "SI-MPC"</a>
                    </div>
                </div>
                <div class="navbar-nav navbar-form credenciales pull-right">
                    <table>
                        <tr>
                            <td>
                                <strong>Bienvenido</strong> 
                            </td>
                            <td style="width: 120px; text-align: right">
                                <a id="" href="/masterdata/index.htm" class="btn btn-warning btn-xs">Volver al inicio</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </nav>
        <div class="container">
            <h1> Capacitaci�n Para La Inserci�n y Reinserci�n Laboral</h1>
            <!-- Search area -->
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Filtros de B�squeda</h2></div>
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li <c:if test="${activeTab eq 'ccf'}">class="active"</c:if> ><a data-toggle="tab" href="#ccf" id="tabSedes" onclick="borraGrafico()">Caja de compensaci�n</a></li>
                        <li <c:if test="${activeTab eq 'instituciones'}">class="active"</c:if> ><a data-toggle="tab" href="#instituciones" id="tabSedes" onclick="borraGrafico()">Tipo Instituci�n</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="alert-area" ></div>
                            <div id="ccf" class="tab-pane fade <c:if test="${activeTab == 'ccf'}">in active</c:if>" >
                                <div class="panel-body">
                                    <form id="ccfFilterForm" action="/masterdata/portal-capacitacion.htm" method="POST"  >
                                        <input type="hidden" id="resultOk" value="${resultOk}">
                                        <input type="hidden" id="activeTabCcf" name="activeTabCcf" value="${activeTab eq 'ccf'}"/>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="ccfLabel" class="control-label">Caja de compensaci�n</label>     
                                            <select class="form-control" name="ccfSel" id="ccfSel" > 
                                                <option value='0' label="" selected>Todas las CCF</option>
                                                <c:if test="${not empty ccfList}">     
                                                    <c:forEach items="${ccfList}" var="item" begin="0">
                                                        <option <c:if test="${item.code eq ccfSel}">selected="selected"</c:if>  
                                                                                                    value="${item.code}">${item.shortName}
                                                        </option>                                     
                                                    </c:forEach>  
                                                </c:if>
                                            </select>   
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="year" class="control-label">A�o</label>
                                            <select class="form-control"  id="anio1Sel" name="anio1Sel" required="true">
                                                <option label="Todos los a�os"  value="0">Todos los a�os</option>
                                                <c:if test="${not empty anios}">
                                                    <c:forEach var="item" items="${anios}">
                                                        <option <c:if test="${item eq anio1Sel}">selected="selected"</c:if>  
                                                                                                 value="${item}" >${item}</option>
                                                    </c:forEach>
                                                </c:if>
                                            </select>
                                        </div>
                                    </div> 
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="monthtab1" class="control-label">Mes</label>
                                            <select class="form-control" id="mes1Sel" name="mes1Sel"  >
                                                <option label="Todos los Meses" value="0">Todos los Meses</option>
                                                <option  <c:if test="${'01' eq mes1Sel}">selected="selected"</c:if>  value="01">Enero</option>
                                                <option  <c:if test="${'02' eq mes1Sel}">selected="selected"</c:if>  value="02">Febrero</option>
                                                <option  <c:if test="${'03' eq mes1Sel}">selected="selected"</c:if>  value="03">Marzo</option>
                                                <option  <c:if test="${'04' eq mes1Sel}">selected="selected"</c:if>  value="04">Abril</option>
                                                <option  <c:if test="${'05' eq mes1Sel}">selected="selected"</c:if>  value="05">Mayo</option>
                                                <option  <c:if test="${'06' eq mes1Sel}">selected="selected"</c:if>  value="06">Junio</option>
                                                <option  <c:if test="${'07' eq mes1Sel}">selected="selected"</c:if>  value="07">Julio</option>
                                                <option  <c:if test="${'08' eq mes1Sel}">selected="selected"</c:if>  value="08">Agosto</option>
                                                <option  <c:if test="${'09' eq mes1Sel}">selected="selected"</c:if>  value="09">Septiembre</option>
                                                <option  <c:if test="${'10' eq mes1Sel}">selected="selected"</c:if>  value="10">Octubre</option>
                                                <option  <c:if test="${'11' eq mes1Sel}">selected="selected"</c:if>  value="11">Noviembre</option>
                                                <option  <c:if test="${'12' eq mes1Sel}">selected="selected"</c:if>  value="12">Diciembre</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-sm-12" style="text-align:right">
                                            <button type="button"  onClick="validarCcf()" class="btn btn-success" id="rango" name="rango">Buscar</button>    
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="instituciones" class="tab-pane fade <c:if test="${activeTab == 'instituciones'}">in active</c:if>" >
                                <div class="panel-body">
                                    <form id="instFilterForm" action="/masterdata/portal-capacitacion.htm" method="POST"  >
                                        <input type="hidden" id="activeTabInst" name="activeTabInst" value="${activeTab eq 'instituciones'}"/>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="tipoInstitucionLabel" class="control-label">Tipo de Instituci�n</label>     
                                            <select class="form-control" name="instTypeSel" id="instTypeSel" > 
                                                <option value="0" label="Seleccione ..." selected>Seleccione ...</option>
                                                <c:if test="${not empty instTypeList}">     
                                                    <c:forEach items="${instTypeList}" var="item" begin="0">
                                                        <option <c:if test="${item.id eq instTypeSel}">selected="selected"</c:if> 
                                                                                                       value="${item.id}">${item.name}
                                                        </option>                                     
                                                    </c:forEach>  
                                                </c:if>
                                            </select>   
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="year" class="control-label">A�o</label>
                                            <select class="form-control"  id="anio2Sel" name="anio2Sel" required="true">
                                                <option label="Todos los a�os"  value="0">Todos los a�os</option>
                                                <c:if test="${not empty anios}">
                                                    <c:forEach var="item" items="${anios}">
                                                        <option <c:if test="${item eq anio2Sel}">selected="selected"</c:if> 
                                                                                                 value="${item}" >${item}</option>
                                                    </c:forEach>
                                                </c:if>
                                            </select>
                                        </div>
                                    </div> 
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="monthtab2" class="control-label">Mes</label>
                                            <select class="form-control" id="mes2Sel" name="mes2Sel"  >
                                                <option label="Todos los Meses" value="0">Todos los Meses</option>
                                                <option  <c:if test="${'01' eq mes2Sel}">selected="selected"</c:if>  value="01">Enero</option>
                                                <option  <c:if test="${'02' eq mes2Sel}">selected="selected"</c:if>  value="02">Febrero</option>
                                                <option  <c:if test="${'03' eq mes2Sel}">selected="selected"</c:if>  value="03">Marzo</option>
                                                <option  <c:if test="${'04' eq mes2Sel}">selected="selected"</c:if>  value="04">Abril</option>
                                                <option  <c:if test="${'05' eq mes2Sel}">selected="selected"</c:if>  value="05">Mayo</option>
                                                <option  <c:if test="${'06' eq mes2Sel}">selected="selected"</c:if>  value="06">Junio</option>
                                                <option  <c:if test="${'07' eq mes2Sel}">selected="selected"</c:if>  value="07">Julio</option>
                                                <option  <c:if test="${'08' eq mes2Sel}">selected="selected"</c:if>  value="08">Agosto</option>
                                                <option  <c:if test="${'09' eq mes2Sel}">selected="selected"</c:if>  value="09">Septiembre</option>
                                                <option  <c:if test="${'10' eq mes2Sel}">selected="selected"</c:if>  value="10">Octubre</option>
                                                <option  <c:if test="${'11' eq mes2Sel}">selected="selected"</c:if>  value="11">Noviembre</option>
                                                <option  <c:if test="${'12' eq mes2Sel}">selected="selected"</c:if>  value="12">Diciembre</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-sm-12" style="text-align:right">
                                            <button type="button"  onClick="validarInst()" class="btn btn-success" id="rango" name="rango">Buscar</button>    
                                        </div>
                                    </form>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div> 
                <!-- End Search area -->
                <!-- Results area -->
                <div class="panel panel-default">
                    <div class="panel-heading"><h2>Cantidad de m�dulos por tipo de formaci�n y tipo de instituci�n</h2></div>
                    <div class="panel-body">
                        <div class=" table-responsive col-md-12">
                            <center>
                                <button type="button" class="btn btn-default desableBtn"
                                        data-toggle="modal" data-target="#graphicModal">  
                                    <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                                    Ver gr&aacute;fico
                                </button>
                            </center>

                            <table id="tablaResultados" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Tipo de Formaci�n</th>
                                        <c:if test="${resultOk}">
                                                <c:forEach items="${resultsInstTypeList}" var="instType" begin="0">
                                                <th>No. de M�dulos ${instType.name}</th>  
                                                </c:forEach> 
                                        </c:if>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Tipo de Formaci�n</th>
                                        <c:if test="${resultOk}">
                                            <c:forEach items="${resultsInstTypeList}" var="instType" begin="0">
                                            <th>No. de M�dulos ${instType.name}</th>  
                                            </c:forEach>
                                        </c:if>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <c:if test="${resultOk}">
                                        <c:forEach items="${trainTypeList}" var="trainType" begin="0">
                                            <tr> 
                                                <td>${trainType.name}</td>
                                                <c:forEach items="${resultsInstTypeList}" var="instType" begin="0">
                                                    <td align="right">
                                                        <c:if test="${resultsList.get(trainType.id, instType.id)==null}">
                                                            0
                                                        </c:if>
                                                        <c:if test="${resultsList.get(trainType.id, instType.id)!=null}">
                                                            ${resultsList.get(trainType.id, instType.id)}
                                                        </c:if>   
                                                    </td>
                                                </c:forEach> 
                                            </tr>   
                                        </c:forEach>  
                                    </c:if>
                                </tbody> 
                            </table>
                        </div>
                    </div>
                </div>
            <!-- End Results area -->
        </div>
        <!--Ends container -->
        <!--Footer -->
        <div class="footer">
            <img src="resources/images/bg_banderin.jpg" width="100%"/> 
            <div class="clearfix"></div> 
        </div>
        <!--Ends footer -->
        <div class="clearfix"></div>  

        <!--GraphicModal -->
        <div class="modal fade" id="graphicModal" tabindex="-1" role="dialog" aria-labelledby="graphicModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12  " id="graphicContainer2">
                                <h4 class="modal-title" id="graphicModalLabel"><Strong><center>Cantidad de M�dulos</center></Strong></h4>
                                <div id="graphicContainer"></div>
                                <div id="legend" class="graphic-legend inline-block" style="font-size: 12px;"></div>
                                <br>
                                <Strong> ${titulografica}</Strong>
                                <div class=" table-responsive col-md-12">
                                    <table id="tablag" class=" table-striped table-bordered " cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Tipo de Formaci�n</th>
                                                <c:if test="${resultOk}">
                                                    <c:forEach items="${resultsInstTypeList}" var="instType" begin="0">
                                                    <th>No. de M�dulos ${instType.name}</th>  
                                                    </c:forEach> 
                                                </c:if>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:if test="${resultOk}">
                                                <c:forEach items="${trainTypeList}" var="trainType" begin="0">
                                                    <tr> 
                                                        <td>${trainType.name}</td>
                                                        <c:forEach items="${resultsInstTypeList}" var="instType" begin="0">
                                                            <td align="right">
                                                                <c:if test="${resultsList.get(trainType.id, instType.id)==null}">
                                                                    0
                                                                </c:if>
                                                                <c:if test="${resultsList.get(trainType.id, instType.id)!=null}">
                                                                    ${resultsList.get(trainType.id, instType.id)}
                                                                </c:if>   
                                                            </td>
                                                        </c:forEach> 
                                                    </tr>   
                                                </c:forEach>
                                            </c:if>
                                        </tbody> 
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" onclick="printGraphic()">Exportar PDF</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

        <!--End GraphicModal -->

        <!-- jQuery -->
        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.buttons.min.js"></script>
        <script src="resources/js/buttons.flash.min.js"></script>
        <script src="resources/js/jszip.min.js"></script>
        <script src="resources/js/pdfmake.min.js"></script>
        <script src="resources/js/vfs_fonts.js"></script>
        <script src="resources/js/buttons.html5.min.js"></script>
        <script src="resources/js/buttons.print.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/bootstrap-confirmation.js"></script>
        <script src="resources/js/jquery.validate.min.js"></script>
        <script src="resources/js/tooltipster.bundle.js"></script>
        <script src="resources/js/custom-functionalities.js"></script>
        <script src="resources/js/raphael.min.js"></script>
        <script src="resources/js/morris.js"></script>


        <script>
            $(document).ready(function () {

                $('#fechaInicio').datepicker({format: 'yyyy-mm-dd', language: "es", autoclose: true});
                $('#fechaFin').datepicker({format: 'yyyy-mm-dd', language: "es", autoclose: true});

                $('#tablaResultados').DataTable({
                    "language": {
                        "url": "resources/js/Spanish.json"
                    },
                    "bFilter": true,
                    "bInfo": false,
                    dom: 'Blfrtip',
                    buttons: [
                        'csv', 'excel', 'pdf', 'print'
                    ]

                });
                $('#graphicModal').on('shown.bs.modal', function () {
                    $(function () {
                        var json = '<c:out value="${jsonResultsList}" escapeXml="false"/>';
                        var ykeys = [];
                        var labels = [];
<c:forEach items="${yKeys}" var="yKey" begin="0">
                        ykeys.push('<c:out value="${yKey}" escapeXml="false"/>');
</c:forEach>
<c:forEach items="${labels}" var="label" begin="0">
                        labels.push('<c:out value="${label}" escapeXml="false"/>');
</c:forEach>
                        $("#graphicContainer").empty();
                        $('#legend').empty();

                        var browsersChart = Morris.Bar({
                            element: 'graphicContainer',
                            data: jQuery.parseJSON(json),
                            xkey: '<c:out value="${xKey}" escapeXml="false"/>',
                            ykeys: ykeys,
                            labels: labels,
                            stacked: true,
                            hideHover: 'auto',
                            barGap: 2,
                            barSizeRatio: 0.75,
                            xLabelAngle: 60,
                            gridTextSize: 10,
                            xLabelMargin: 10,
                            //padding: 75,
                            resize: true
                        });
                        $('svg').height(375);
                        browsersChart.options.data.forEach(function (labels, i) {
                            var legendItem = $('<span></span>').text(browsersChart.options.labels[i]).prepend('<br><span>&nbsp;</span>');
                            legendItem.find('span')
                                    .css('backgroundColor', browsersChart.options.barColors[i])
                                    .css('width', '18px')
                                    .css('display', 'inline-block')
                                    .css('margin', '5px');
                            $('#legend').append(legendItem);
                        });
                    });
                });
            });

            function validarCcf() {
                if (($("#mes1Sel").val() !== "0") && $("#anio1Sel").val() === "0") {
                    $("#alert-area").append($("<div class='alert alert-message alert-danger" +
                            " fade in' data-alert><p> Debe seleccionar a�o </p></div>"));
                    $(".alert-message").delay(3000).fadeOut("slow", function () {
                        $(this).remove();
                    });
                } else {
                    $("#activeTabCcf").val("true");
                    $("#activeTabInst").val("false");
                    $('#ccfFilterForm').submit();
                }
            }
            function validarInst() {
                if (($("#mes2Sel").val() !== "0") && $("#anio2Sel").val() === "0") {
                    $("#alert-area").append($("<div class='alert alert-message alert-danger" +
                            " fade in' data-alert><p> Debe seleccionar a�o </p></div>"));
                    $(".alert-message").delay(3000).fadeOut("slow", function () {
                        $(this).remove();
                    });
                } else if ($("#instTypeSel").val() === "0") {
                    $("#alert-area").append($("<div class='alert alert-message alert-danger" +
                            " fade in' data-alert><p> Debe seleccionar un tipo de instituci�n </p></div>"));
                    $(".alert-message").delay(3000).fadeOut("slow", function () {
                        $(this).remove();
                    });
                } else {
                    $("#activeTabCcf").val("false");
                    $("#activeTabInst").val("true");
                    $('#instFilterForm').submit();
                }
            }

            /**
             * Imprime el grafico en un pdf.
             */
            function printGraphic() {
                var objeto = document.getElementById('graphicContainer2'); //obtenemos el objeto a imprimir
                var ventana = window.open('', '_blank'); //abrimos una ventana vac�a nueva
                ventana.document.write(objeto.innerHTML); //imprimimos el HTML del objeto en la nueva ventana
                ventana.document.close(); //cerramos el documento
                ventana.print(); //imprimimos la ventana
                ventana.close(); //cerramos la ventana

            }

            function borraGrafico() {

                $("#graphicContainer2").empty();
                $(".desableBtn").attr("disabled", "desabled");
            }
        </script>
        <style>
            .buttons-csv{
                display: inline-block;
                background-image:url(resources/images/csv.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;

            }
            .buttons-csv span{
                opacity: 0;
            }
            .buttons-excel{
                display: inline-block;
                background-image:url(resources/images/excel.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;

            }
            .buttons-excel span{
                opacity: 0;
            }
            .buttons-pdf{
                display: inline-block;
                background-image:url(resources/images/pdf.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;

            }
            .buttons-pdf span{
                opacity: 0;
            }
            .buttons-print{
                display: inline-block;
                background-image:url(resources/images/print.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;


            }
            .buttons-print span{
                opacity: 0;

            }
            div.dt-buttons {
                float: right;
                margin-left:10px;
            }
        </style>
    </body>
</html>
