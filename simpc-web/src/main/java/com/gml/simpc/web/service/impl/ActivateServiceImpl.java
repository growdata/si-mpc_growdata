/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserServiceImpl.java
 * Created on: 2016/10/19, 02:11:43 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.entity.Activate;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.entity.exeption.UserException;
import com.gml.simpc.api.enums.LinkReason;
import com.gml.simpc.api.services.ActivationService;
import com.gml.simpc.api.utilities.Util;

import static com.gml.simpc.api.utilities.Util.PARAMETER_NAME_DOMAIN;

import com.gml.simpc.commons.mail.sender.Notificator;
import com.gml.simpc.web.constants.WebConstants;
import com.gml.simpc.web.repository.ActivationRepository;
import com.gml.simpc.web.repository.ParameterRepository;
import com.gml.simpc.web.repository.UserRepository;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.gml.simpc.api.utilities.Util.PARAMETER_PASS;

/**
 * Servicio para administrar tokens de activaci&oacute;n.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Service(value = "activateService")
public class ActivateServiceImpl implements ActivationService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(ActivateServiceImpl.class);
    /**
     * Mensaje para obtener la contrase&ntilde;a.
     */
    private static final String CRETION_PASSWORD_MESSAGE = "Su usuario ha sido " +
        "creado y se encuentra pendiente de activaci�n.\n Para completar el " +
        "proceso de activaci�n del usuario, presione clic en el siguiente enlace:\n " +
        "<a href=\"%s\">Activar Usuario</a>";
    /**
     * Mensaje para recuperar la contrase&ntilde;a.
     */
    private static final String RECOVER_PASSWORD_MESSAGE = "Para completar el " +
        "proceso de asignaci�n de contrase�a siga el enlace: " +
        "\n<a href=\"%s\">Obtener Contrase�a</a>";
    /**
     * Mensaje para activar usuario.
     */
    private static final String ACTIVATE_PASSWORD_MESSAGE = "Su usuario se " +
        "encuentra actualmente inactivo y debe actualizar la contrase�a. Para " +
        "completar el proceso de asignaci�n de contrase�a siga el enlace: " +
        "\n<a href=\"%s\">Obtener Contrase�a</a>";
    /**
     * Mensaje para activar usuario previa expiraci�n.
     */
    private static final String ACTIVATE_PASSWORD_PREV_MESSAGE = "Su usuario est� " +
        "pr�ximo a inactivarse y debe actualizar la contrase�a. Para " +
        "completar el proceso de reasignaci�n de contrase�a siga el enlace: " +
        "\n<a href=\"%s\">Obtener Contrase�a</a>";
    /**
     * Repositorio para usuarios.
     */
    @Autowired
    private ActivationRepository activationRepository;
    /**
     * Repositorio de usuarios.
     */
    @Autowired
    private UserRepository userRepository;
    /**
     * Repositorio de par&aacute;metros.
     */
    @Autowired
    private ParameterRepository parameterRepository;
    /**
     * Servicio de notificaci&oacute;n de correos.
     */
    @Autowired
    private Notificator notificator;

    /**
     * 
     * @return 
     */
    @Override
    public List<Activate> getAll() {
        LOGGER.info("Ejecutando metodo [ getAll ]");

        return this.activationRepository.findAll();
    }

    /**
     * 
     * @param activate 
     */
    @Override
    public void save(Activate activate) {
        LOGGER.info("Ejecutando metodo [ save ]");

        this.activationRepository.saveAndFlush(activate);
    }

    /**
     * 
     * @param user 
     */
    @Override
    public void update(Activate user) {
        LOGGER.info("Ejecutando metodo [ update ]");
        this.activationRepository.saveAndFlush(user);
    }

    /**
     * 
     * @param user 
     */
    @Override
    public void remove(Activate user) {
        LOGGER.info("Ejecutando metodo [ remove ]");
        this.activationRepository.delete(user);
    }

    /**
     * 
     * @param token
     * @return 
     */
    @Override
    public Activate findByUserToken(String token) {
        LOGGER.info("Ejecutando metodo [ findByUserToken ]");
        return this.activationRepository.findByUserToken(token);
    }

    /**
     * 
     */
    @Override
    public void deleteExpiratedTokens() {
        this.activationRepository.deleteExpiratedTokens();
    }

    /**
     * 
     * @param email
     * @param linkReason
     * @return
     * @throws UserException 
     */
    @Override
    public boolean sendPasswordLink(String email, LinkReason linkReason) 
        throws UserException {
        User loadedUser = this.userRepository.
            findByUsernameIgnoreCaseOrEmailIgnoreCase(email, email);

        if (loadedUser != null) {
            String key = generateToken(loadedUser.getUsername());

            String dominio = this.parameterRepository.
                findByName(PARAMETER_NAME_DOMAIN).getValue();
            StringBuilder url = new StringBuilder(dominio);
            url.append(WebConstants.ACTIVATION_PATH).append("?").
                append("key=").append(key);
            notifyUser(loadedUser, loadedUser.getEmail(), key, url.toString(), 
                linkReason);
            LOGGER.info("sendPasswordLink url : " + url.toString());
        }

        return loadedUser != null;
    }

    /**
     * Genera el token de activaci&ocute;n que sera enviado al usuario que se
     * acaba de registrar
     *
     * @param userName Usuario para el que se generara el token
     *
     * @return Clave &ucute;nica de activaci&ocute;n que sera una
     *         combinaci&ocute;n del <code>TimeStamp</code> y el nombre de 
     *         usuario.
     */
    private String generateToken(String userName) {
        LOGGER.info("Metodo [generateToken]");
        StringBuilder sbuilder = new StringBuilder();
        sbuilder.append(userName).append(new Date().getTime());

        return Util.calculateSha1(String.valueOf(sbuilder));
    }

    /**
     * Env&iacute;a un mensaje al usuario.
     * 
     * @param user
     * @param email
     * @param token
     * @param url
     * @param linkReason
     * @throws UserException 
     */
    private void notifyUser(User user, String email, String token,
        String url, LinkReason linkReason) throws UserException {
        LOGGER.info("Metodo [notifyUser]****" + email);
        
        String finalUrl = "<a href=\"%s\">Obtener Contrase�a</a>";
        
        switch(linkReason) {
            case CREATION:
                finalUrl = String.format(CRETION_PASSWORD_MESSAGE, url);
                break;
            case EXPIRATION:
                finalUrl = String.format(ACTIVATE_PASSWORD_MESSAGE, url);
                break;
            case PREV_EXPIRATION:
                finalUrl = String.format(ACTIVATE_PASSWORD_PREV_MESSAGE, url);
                break;    
            case FORGOT:
                finalUrl = String.format(RECOVER_PASSWORD_MESSAGE, url);
                break;
        }

        this.notificator.sendNotification(email, finalUrl, user.getFirstName().
            concat(" ").concat(user.getFirstSurname()), PARAMETER_PASS);

        Activate activate = new Activate();
        activate.setUser(user);
        activate.setUserToken(token);
        activate.setUserDate(new Date());

        this.activationRepository.save(activate);
    }

    @Override
    public void deleteTokensByUser(Long userId) {
        this.activationRepository.deleteTokensByUser(userId);
    }
}
