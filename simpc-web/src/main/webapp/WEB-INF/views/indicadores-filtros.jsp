<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css/morris.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
    </head>
    <body>
        <h1>Indicadores</h1>
        <div id="buttonsDiv" class="col-sm-offset-4 col-sm-8" style="text-align:right">
            <a href="indicadores.htm" type="submit" class="btn btn-success">Volver a selecci�n de indicador</a>
            <br/>
            <br/>
        </div>  
        <h2>
            Configure filtros para las variables seleccionadas
        </h2><br/>
        <form method="get" action="/masterdata/generarResultados.htm"> 
            <c:if test="${not empty indicatorSourceList}">    
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Seleccione indicador</h4>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-6">
                            <div class="form-group">
                                ${indicator.id} - ${indicator.name}
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
                <div class="panel panel-default" id="configurationDiv">
                    <div class="panel-heading"><h4>Configure indicador</h4></div>
                    <div class="panel-body">
                        <div class="col-sm-6" id ="sourceDiv">
                            <div class="form-group">
                                <label for="indicador" class=" control-label">Fuente de datos: </label>
                                <input type="hidden" id="indicatorSourceSel" name="indicatorSourceSel" value="${indicatorSource.id}"  />
                                ${indicatorSource.name}
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-sm-6" id ="graphicTypeDiv">
                            <div class="form-group">
                                <label for="indicador" class=" control-label">Tipo de gr�fica: </label>
                                <input type="hidden" id="graphicTypeSel" name="graphicTypeSel" value="${graphicType}"  />
                                <c:if test="${graphicType.equals('A')}">Linea</c:if>
                                <c:if test="${graphicType.equals('B')}">Barras</c:if>
                                <c:if test="${graphicType.equals('D')}">C�rculo</c:if>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default" id="varConfigurationDiv">
                    <div class="panel-heading"><h4>Configure variables para el indicador</h4></div>
                    <div class="panel-body">
                        <c:if test="${!graphicType.equals('D')}">
                            <div class="col-sm-6" id="xVariableDiv">
                                <label for="indicador" class=" control-label">Variable horizontal: </label>
                                <input type="hidden"  id="xVariableSel" name="xVariableSel"  value="${xVariable.field}"/>
                                ${xVariable.description}
                                <br/>
                                <c:if test="${xVariable.type.equals('periodo')}">
                                    <div class="col-sm-4">
                                        <label for="fechaInicio" class="control-label ">Fecha Inicio</label>
                                        <input id="xVariableFechaInicio" name="xVariableFechaInicio" class="form-control "
                                               data-date-format="yyyy/mm/dd"   path="fechaInicio" />
                                        <fmt:formatDate pattern="dd-MM-yyyy" />
                                    </div>
                                    <div class="col-sm-4" >
                                        <label for="fechaFin" class="control-label ">Fecha Fin</label>
                                        <input id="xVariableFechaFin" name="xVariableFechaFin"  class="form-control "
                                               data-date-format="yyyy/mm/dd"   path="fechaFin"/>
                                        <fmt:formatDate pattern="dd-MM-yyyy" />
                                    </div>
                                </c:if>
                                <c:if test="${xVariable.type.equals('campo')}">
                                    <input type="text" style="width: 300px;" id="xVariableCampo" name="xVariableCampo" placeholder="Ingrese si desea filtrar resultados por este campo"/>
                                    <br/><br/><br/>
                                </c:if>
                                <c:if test="${xVariable.type.equals('tabla')}">
                                    <select id="xVariableLista" name="xVariableLista">
                                        <option value="0">Seleccione �nicamente si desea filtrar por �ste campo...</option>
                                        <c:forEach var="item" items="${xVariableList}">
                                            <option value="${item.id}">${item.descripcion}</option>
                                        </c:forEach>
                                    </select>   
                                    <br/><br/><br/><br/>
                                </c:if>
                            </div>
                        </c:if>        
                        <div class="col-sm-6" id="seriesDiv">
                            <label for="indicador" class=" control-label">Variable de agrupaci�n:</label>
                            <input type="hidden" id="seriesSel" name="seriesSel" value="${serieVariable.field}"/>
                            ${serieVariable.description}
                            <br/>
                            <c:if test="${serieVariable.type.equals('periodo')}">
                                    <div class="col-sm-4">
                                        <label for="fechaInicio" class="control-label ">Fecha Inicio</label>
                                        <input id="serieVariableFechaInicio" name="serieVariableFechaInicio" class="form-control "
                                               data-date-format="yyyy/mm/dd"   path="fechaInicio" />
                                        <fmt:formatDate pattern="dd-MM-yyyy" />
                                    </div>
                                    <div class="col-sm-4" >
                                        <label for="fechaFin" class="control-label ">Fecha Fin</label>
                                        <input id="serieVariableFechaFin" name="serieVariableFechaFin"  class="form-control "
                                               data-date-format="yyyy/mm/dd"   path="fechaFin"/>
                                        <fmt:formatDate pattern="dd-MM-yyyy" />
                                    </div>
                                </c:if>
                            <c:if test="${serieVariable.type.equals('campo')}">
                                <input type="text" style="width: 300px;" id="serieVariableCampo" name="serieVariableCampo" placeholder="Ingrese si desea filtrar resultados por este campo"/>
                                <br/><br/><br/>
                            </c:if>
                            <c:if test="${serieVariable.type.equals('tabla')}">
                                 <div class="col-sm-12" >
                                    <select id="serieVariableLista" name="serieVariableLista">
                                        <option value="0">Seleccione �nicamente si desea filtrar por �ste campo...</option>
                                        <c:forEach var="item" items="${serieVariableList}">
                                            <option value="${item.id}">${item.descripcion}</option>
                                        </c:forEach>
                                    </select>    
                                    <br/><br/><br/> 
                                 </div>     
                            </c:if>
                        </div>
                        <div class="col-sm-6" id="yVariableDiv" >
                             <br/>
                            <label for="indicador" class=" control-label">Variable a medir: </label>
                            <input type="hidden" id="yVariableSel" name="yVariableSel" value="${yVariable.field}"/>
                            ${yVariable.description}
                            <br/>
                            <c:if test="${yVariable.type.equals('periodo')}">
                                     <div class="col-sm-4 ">
                                        <div class="form-group">
                                            <label for="year" class="control-label">A�o</label>
                                            <select class="form-control"  id="anio" name="anio" required="true">
                                                <option label="A�O"  value=""></option>
                                                <c:if test="${not empty anios}">
                                                    <c:forEach var="item" items="${anios}">
                                                        <option <c:if test="${item eq yearSelected}">selected="selected"</c:if>  value="${item}" >${item}</option>
                                                    </c:forEach>
                                                </c:if>
                                            </select>
                                        </div>
                                    </div> 
                                    <div class="col-sm-4 ">
                                        <div class="form-group">
                                            <label for="monthtab2" class="control-label">Mes</label>
                                            <select class="form-control" id="mes" name="mes"  >
                                                <option label="Todos los Meses" value=""></option>
                                                <option <c:if test="${monthSelected2 == '01'}">selected="selected"</c:if> value="01">Enero</option>
                                                <option <c:if test="${monthSelected2 == '02'}">selected="selected"</c:if> value="02">Febrero</option>
                                                <option <c:if test="${monthSelected2 == '03'}">selected="selected"</c:if> value="03">Marzo</option>
                                                <option <c:if test="${monthSelected2 == '04'}">selected="selected"</c:if> value="04">Abril</option>
                                                <option <c:if test="${monthSelected2 == '05'}">selected="selected"</c:if> value="05">Mayo</option>
                                                <option <c:if test="${monthSelected2 == '06'}">selected="selected"</c:if> value="06">Junio</option>
                                                <option <c:if test="${monthSelected2 == '07'}">selected="selected"</c:if> value="07">Julio</option>
                                                <option <c:if test="${monthSelected2 == '08'}">selected="selected"</c:if> value="08">Agosto</option>
                                                <option <c:if test="${monthSelected2 == '09'}">selected="selected"</c:if> value="09">Septiembre</option>
                                                <option <c:if test="${monthSelected2 == '10'}">selected="selected"</c:if> value="10">Octubre</option>
                                                <option <c:if test="${monthSelected2 == '11'}">selected="selected"</c:if> value="11">Noviembre</option>
                                                <option <c:if test="${monthSelected2 == '12'}">selected="selected"</c:if> value="12">Diciembre</option>
                                            </select>
                                        </div>
                                    </div>
                                </c:if>
                            <c:if test="${yVariable.type.equals('campo')}">
                                <div class="col-sm-12">
                                    <input type="text" style="width: 300px;" id="yVariableCampo" name="yVariableCampo" placeholder="Ingrese si desea filtrar resultados por este campo"/>
                                    <br/><br/><br/>
                                </div>    
                            </c:if>
                            <c:if test="${yVariable.type.equals('tabla')}">
                                <select id="yVariableLista" name="yVariableLista">
                                    <option value="0">Seleccione �nicamente si desea filtrar por �ste campo...</option>
                                    <c:forEach var="item" items="${yVariableList}">
                                        <option value="${item.id}">${item.descripcion}</option>
                                    </c:forEach>
                                    <br/><br/><br/>
                                </select>    
                            </c:if>    
                        </div>
<!--                        <div class="col-sm-6" id ="functionDiv">
                             <br/>
                            <label for="indicador" class=" control-label">Funci�n a utilizar sobre la variable a medir: </label>
                            <input type="hidden" id="functionSel" name="functionSel" value="${functionSel}"/>
                         
                            <div class="clearfix"></div>
                        </div>-->
                        <div id="generarDiv" class="col-sm-offset-4 col-sm-8" style="text-align:right">
                            <button type="submit" class="btn btn-success" >Generar</button>
                        </div>
                    </div>
                </div>
                
                
            </c:if>
        </form>    
        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
	<script src="resources/js/dataTables.buttons.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/buttons.flash.min.js"></script>
        <script src="resources/js/jszip.min.js"></script>
        <script src="resources/js/pdfmake.min.js"></script>
        <script src="resources/js/vfs_fonts.js"></script>
        <script src="resources/js/buttons.html5.min.js"></script>
        <script src="resources/js/buttons.print.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/raphael.min.js"></script>
        <script src="resources/js/morris.js"></script>
        <script src="resources/js/xepOnline.js"></script>
        <script>
            $(document).ready(function () {
                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Mi�rcoles", "Jueves", "Viernes", "S�bado", "Domingo"],
                    daysShort: ["Dom", "Lun", "Mar", "Mi�", "Jue", "Vie", "S�b", "Dom"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Hoy"
                };

                $('#xVariableFechaInicio').datepicker({language: "es", autoclose: true,format: 'dd/mm/yy'});
                $('#xVariableFechaFin').datepicker({language: "es", autoclose: true,format: 'dd/mm/yy'});
                $('#serieVariableFechaInicio').datepicker({language: "es", autoclose: true,format: 'dd/mm/yy'});
                $('#serieVariableFechaFin').datepicker({language: "es", autoclose: true,format: 'dd/mm/yy'});
                $('#yVariableFechaInicio').datepicker({language: "es", autoclose: true,format: 'dd/mm/yy'});
                $('#yVariableFechaFin').datepicker({language: "es", autoclose: true,format: 'dd/mm/yy'});
            });
            
            
            
        </script>
        <style>
            .buttons-csv{
                display: inline-block;
                background-image:url(resources/images/csv.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-csv span{
                opacity: 0;
            }
            .buttons-excel{
                display: inline-block;
                background-image:url(resources/images/excel.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-excel span{
                opacity: 0;
            }
            .buttons-pdf{
                display: inline-block;
                background-image:url(resources/images/pdf.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-pdf span{
                opacity: 0;
            }
            .buttons-print{
                display: inline-block;
                background-image:url(resources/images/print.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-print span{
                opacity: 0;
            }
            
            div.dt-buttons {
                float: right;
                margin-left:10px;
            }
        </style>
    </body>
</html>
