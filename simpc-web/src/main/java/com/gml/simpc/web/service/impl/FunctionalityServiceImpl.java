/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: FunctionalityServiceImpl.java
 * Created on: 2016/10/24, 11:38:11 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.entity.Functionality;
import com.gml.simpc.api.services.FunctionalityService;
import com.gml.simpc.web.repository.FunctionalityRepository;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author <a href="mailto:julianm@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
@Service(value = "functionalityService")
public class FunctionalityServiceImpl implements FunctionalityService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(FunctionalityServiceImpl.class);
    /**
     * Repositorio para usuarios.
     */
    @Autowired
    private FunctionalityRepository repository;

    /**
     * Genera las funcionalidades por nombre de usuario.
     * 
     * @param label
     * @return 
     */
    @Override
    public List<Functionality> getFunctionalitiesByUsername(String label) {
        LOGGER.info("Ejecutando metodo [ getFunctionalitiesByUsername ].");
        return this.repository.findByUsername(label);
    }

    /**
     * Genera las funcionalidades asociadas a un perfil.
     * 
     * @param id
     * @return 
     */
    @Override
    public List<Functionality> getFunctionalitiesByProfile(Long id) {
        LOGGER.info("Ejecutando metodo [ getFunctionalitiesByUsername ].");
        return this.repository.getFunctionalitiesByProfile(id);
    }

    /**
     * Genera las funcionalidades que no se encuentran asociadas a un perfil.
     * 
     * @param id
     * @return 
     */
    @Override
    public List<Functionality> getFunctionalitiesByNotProfile(Long id) {
        LOGGER.info("Ejecutando metodo [ getFunctionalitiesByNotProfile ].");
        return this.repository.getFunctionalitiesByNotProfile(id);
    }

    /**
     * Genera todas las funcionalidades.
     * 
     * @return 
     */
    @Override
    public List<Functionality> getAll() {
        LOGGER.info("Ejecutando metodo [ getAll de instituciones ]");
        return this.repository.findAll();
    }

}
