/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IndicatorVariableServiceImpl.java
 * Created on: 2016/12/16, 10:31:31 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.entity.IndicatorVariable;
import com.gml.simpc.api.services.IndicatorVariableService;
import com.gml.simpc.web.repository.IndicatorVariableRepository;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

/**
 * Servicio para manipular indicadores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Service
public class IndicatorVariableServiceImpl implements IndicatorVariableService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(IndicatorVariableServiceImpl.class);
    
     /**
     * Plantilla Jdbc para operar la base de datos.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    /**
     * Repositorio de indicadores.
     */
    @Autowired
    private IndicatorVariableRepository indicatorVariableRepository;

    /**
     * Genera la lista de variables para los indicadores.
     * 
     * @return 
     */
    @Override
    public List<IndicatorVariable> findAll() {
        LOGGER.info("Ejecutando metodo [ findAll ]");
        List<IndicatorVariable> indicatorVariableList = this.indicatorVariableRepository.findAll();
        return indicatorVariableList;
    }

    /**
     * Genera uan variable espec&icute;fica para los indicadores.
     * 
     * @param id
     * @param field
     * @return 
     */
    @Override
    public IndicatorVariable findByIndicatorIdAndField(Long id, String field) {
        LOGGER.info("Ejecutando metodo [ findByIndicatorIdAndField ] para indicator "+id+" and field "+field);
        IndicatorVariable indicatorVariableList = this.indicatorVariableRepository.findByIndicatorIdAndField(id,field);
        return indicatorVariableList;
    }

    /**
     * Genera una variable segun el id.
     * 
     * @param id
     * @return 
     */
    @Override
    public IndicatorVariable findOne(Long id) {
        IndicatorVariable indicatorVariable = this.indicatorVariableRepository.findOne(id);
        return indicatorVariable;
    }

    /**
     * Genera una lista de valiables segun el tipo.
     * 
     * @param variable
     * @return 
     */
    @Override
    public List<Map<String, Object>> getVariableList(IndicatorVariable variable) {
        StringBuilder query = new StringBuilder("select id,descripcion from ");
        query.append(variable.getDetail());
        List<Map<String, Object>> queryForList = this.jdbcTemplate.queryForList(query.toString());
        return queryForList;
    }
    
}
