/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ValueListValidator.java
 * Created on: 2017/01/13, 06:26:19 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.validator;

import com.gml.simpc.api.utilities.Util;
import com.gml.simpc.api.utilities.ValueList;
import com.gml.simpc.loader.thread.ThreadLocalValueList;
import java.io.Serializable;
import org.apache.commons.validator.Field;
import org.apache.commons.validator.ValidatorAction;
import org.apache.commons.validator.util.ValidatorUtils;
import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springmodules.validation.commons.FieldChecks;

/**
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica
 * Jim&eacute;nez Rozo</a>
 */
public class ValueListValidator implements Serializable {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(ValueListValidator.class);

    /**
     * M�todo para validar tipos de documento desde las listas de valor.
     */
    public boolean validateDocumentType(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();

            if (!valueList.getDocumentTypesFull().isEmpty() &&
                valueList.getDocumentTypesFull().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }

        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar tipos de orientacion desde las listas de valor.
     */
    public boolean validateOrientationType(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();

            if (!valueList.getOrientationType().isEmpty() &&
                valueList.getOrientationType().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar cajas de compensaci�n desde las listas de valor.
     */
    public boolean validateFoundation(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getFoundations().isEmpty() &&
                valueList.getFoundations().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar depatamentos desde las listas de valor.
     */
    public boolean validateState(Object bean, ValidatorAction va, Field field,
        Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getStates().isEmpty() &&
                valueList.getStates().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar ciudades desde las listas de valor.
     */
    public boolean validateCity(Object bean, ValidatorAction va, Field field,
        Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getCities().isEmpty() &&
                valueList.getCities().get(value) != null) {

                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar estado civil desde las listas de valor.
     */
    public boolean validateCivilStatus(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getCivilStatusMap().isEmpty() &&
                valueList.getCivilStatusMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar grupos etnicos desde las listas de valor.
     */
    public boolean validateEthnicGroup(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getEthnicGroupMap().isEmpty() &&
                valueList.getEthnicGroupMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar tipos de discapacidad desde las listas de valor.
     */
    public boolean validateDisabilityType(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getDisabilityTypeMap().isEmpty() &&
                valueList.getDisabilityTypeMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar estado laboral desde las listas de valor.
     */
    public boolean validateEmploymentStatus(Object bean, ValidatorAction va,
        Field field,
        Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getEmploymentStatusMap().isEmpty() &&
                valueList.getEmploymentStatusMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar nivel educativo desde las listas de valor.
     */
    public boolean validateEducationLevel(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getEducationalLevelMap().isEmpty() &&
                valueList.getEducationalLevelMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar Areas de desempe�o desde las listas de valor.
     */
    public boolean validatePerformanceArea(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getPerformanceAreaMap().isEmpty() &&
                valueList.getPerformanceAreaMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar nucleos de conocimiento desde las listas de valor.
     */
    public boolean validateKnowledgeCore(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getKnowledgeCoreMap().isEmpty() &&
                valueList.getKnowledgeCoreMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar titulos desde las listas de valor.
     */
    public boolean validateObtainedTitle(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getObtainedTitleMap().isEmpty() &&
                valueList.getObtainedTitleMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar experiencia laboral desde las listas de valor.
     */
    public boolean validateEmploymentExperience(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getEmploymentExperienceMap().isEmpty() &&
                valueList.getEmploymentExperienceMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar sectores desde las listas de valor.
     */
    public boolean validateSector(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getSectorMap().isEmpty() &&
                valueList.getSectorMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar cargos equivalentes desde las listas de valor.
     */
    public boolean validateEquivalentPosition(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getEquivalentPositionMap().isEmpty() &&
                valueList.getEquivalentPositionMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar idiomas desde las listas de valor.
     */
    public boolean validateLanguage(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getLanguageMap().isEmpty() &&
                valueList.getLanguageMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar idiomas desde las listas de valor.
     */
    public boolean valueListPopulation(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getPopulation().isEmpty() &&
                valueList.getPopulation().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar otros conocimientos desde las listas de valor.
     */
    public boolean validateOtherKnowledge(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getOtherKnowledgeMap().isEmpty() &&
                valueList.getOtherKnowledgeMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar tipos de capcitacion desde las listas de valor.
     */
    public boolean validateTrainingType(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getTrainingTypeMap().isEmpty() &&
                valueList.getTrainingTypeMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }
    
    /**
     * M�todo para validar tipos de capcitacion desde las listas de valor.
     */
    public boolean validateProgramType(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getProgramTypes().isEmpty() &&
                valueList.getProgramTypes().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }
    
    /**
     * M�todo para validar tipos de capcitacion desde las listas de valor.
     */
    public boolean validateSchedule(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getScheduleMap().isEmpty() &&
                valueList.getScheduleMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar categorias de licencia de carro
     * desde las listas de valor.
     */
    public boolean validateCarCategoryLicense(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getCarLicenseCategoryMap().isEmpty() &&
                valueList.getCarLicenseCategoryMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar tipos de capcitacion desde las listas de valor.
     */
    public boolean validateMotorcycleCategoryLicense(Object bean,
        ValidatorAction va, Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getMotorcycleLicenseCategoryMap().isEmpty() &&
                valueList.getMotorcycleLicenseCategoryMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar tipos de orientacion desde las listas de valor.
     */
    public boolean valueListCine(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();

            if (!valueList.getCineMap().isEmpty() &&
                valueList.getCineMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }
    
    /**
     * M�todo para validar los nembres de certificado desde las listas de valor.
     */
    public boolean validateCertification(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();

            if (!valueList.getCertificationsMap().isEmpty() &&
                valueList.getCertificationsMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }
    
    /**
     * M�todo para validar los nembres de certificado desde las listas de valor.
     */
    public boolean validateIssuedCertName(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();

            if (!valueList.getDeliveredCertificationMap().isEmpty() &&
                valueList.getDeliveredCertificationMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para validar las administradoras de pension
     */
    public boolean valueListPensionFunds(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();

            if (!valueList.getPensionFundsMap().isEmpty() &&
                valueList.getPensionFundsMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para listas de valor de las administradoras de salud
     */
    public boolean valueListEps(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();

            if (!valueList.getEpsMap().isEmpty() &&
                valueList.getEpsMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }

    /**
     * M�todo para listas de valor para el fondo de cesantias
     */
    public boolean valueListSeveranceFund(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();

            if (!valueList.getSeveranceFundMap().isEmpty() &&
                valueList.getSeveranceFundMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }
    
    /**
     * M�todo para validar Areas de desempe�o de orientacion desde las listas de valor.
     */
    public boolean validatePerformanceAreaOrientation(Object bean, ValidatorAction va,
        Field field, Errors errors) {

        try {
            String value = ValidatorUtils.getValueAsString(bean, field.
                getProperty());
            if (value.replace(Util.SPACE, Util.EMPTY_TEXT).isEmpty()) {
                return true;
            }
            ValueList valueList = ThreadLocalValueList.get();
            if (!valueList.getPerformanceAreaOrientationMap().isEmpty() &&
                valueList.getPerformanceAreaOrientationMap().get(value) != null) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        FieldChecks.rejectValue(errors, field, va);
        return false;
    }
}
