/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ModuleServiceImpl.java
 * Created on: 2016/12/14, 03:39:19 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.dao.ModuleDao;
import com.gml.simpc.api.dto.ModuleDto;
import com.gml.simpc.api.services.ModuleService;
import com.gml.simpc.commons.annotations.AnnotationInterceptor;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.gml.simpc.api.utilities.Util.selectFilter;

/**
 * Servicio para manipular modulos.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Service
public class ModuleServiceImpl implements ModuleService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(ModuleServiceImpl.class);
    /**
     * Dao para manejar la persistencia de modulos.
     */
    @Autowired
    private ModuleDao moduleDao;

    /**
     * Busca modulos seg&ucute;n el id.
     * 
     * @param id
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar M�dulos por" +
        " C�digo del Programa", entityTableName = "Programas_modulos")
    @Override
    public List<ModuleDto> findModules(String id) {
        return this.moduleDao.findModules(id);
    }
    
    /**
     * Genera lista de modulos seg&ucute;n los filtros ingresados.
     * 
     * @param searchModules
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar M�dulos por" +
        " Filtros", entityTableName = "Programas_modulos")
    @Override
    public List<ModuleDto> findModulesByFilters(ModuleDto searchModules) {
           
        switch (selectFilter(searchModules.getStartDate(), searchModules.getEndDate())) {
            case WITH_START_DATE:
                return this.moduleDao.findModulesByFiltersStartDate(searchModules);

            case WITH_END_DATE:
                return this.moduleDao.findModulesByFiltersEndDate(searchModules);
            case WITH_RANGE:
                return this.moduleDao.findModulesByFiltersRange(searchModules);
            default:
                return this.moduleDao.findModulesByFilters(searchModules);
        }

    }
}
