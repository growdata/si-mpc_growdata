/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Functionality.java
 * Created on: 2016/10/21, 10:55:55 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import com.gml.simpc.api.enums.AdminFunctionality;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad que representa las funcionalidades del sistema.
 *
 * @author <a href="mailto:julianm@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
@Table(name = "FUNCIONALIDADES")
@Entity
public class Functionality implements Serializable {

    /**
     * ID de la entidad.
     */
    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    /**
     * Identificador del padre del item
     */
    @Column(name = "PADRE_ID")
    private Long parentId;

    /**
     * Etiqueta de la funcionalidad.
     */
    @Column(name = "ETIQUETA")
    private String label;
    /**
     * Descripci&oacute;n de la funcionalidad.
     */
    @Column(name = "DESCRIPCION")
    private String description;
    /**
     * Path de la funcionalidad
     */
    @Column(name = "RUTA")
    private String path;
    /**
     * &Iacute;cono de la funcionalidad.
     */
    @Column(name = "ICONO")
    private String icon;
    /**
     * Estado del perfil.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "ADMINISTRATIVA", columnDefinition = "char(1) default 'F'",
        nullable = false)
    private AdminFunctionality adminStatus = AdminFunctionality.F;
    /**
     * Orden en que se muestran las funcionalidades.
     */
    @Column(name = "ORDEN")
    private int order;
    /**
     * popup de las funcionalidades
     * agregado por Andr�s Gonz�lez GrowData
     */
    @Column(name = "POPUP")
    private String popup;
    /**
     * Constructor por id.
     *
     * @param id
     */
    public Functionality(Long id) {
        this.id = id;
    }

    /**
     * Constructor por defecto.
     */
    public Functionality() {
        this.id = null;
    }

    public AdminFunctionality getAdminStatus() {
        return adminStatus;
    }
    
    public void setPopup(String popup){
        this.popup=popup;
    }
    
    public String getPopup(){
        return popup;
    }
    public void setAdminStatus(AdminFunctionality adminStatus) {
        this.adminStatus = adminStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Functionality other = (Functionality) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
}
