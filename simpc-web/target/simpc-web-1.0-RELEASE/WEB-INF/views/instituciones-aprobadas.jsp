<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <style>
            input[type=number]::-webkit-outer-spin-button,
            input[type=number]::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            input[type=number] {
                -moz-appearance:textfield;
            }
        </style>
    </head>

    <body>
        <c:if test="${not empty msg}">
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>


        <h1>Banco de Oferentes</h1>
        <div class="panel panel-default">
            <div class="panel-heading"><h2>B&uacute;squeda</h2></div>
            <div class="panel-body">
                <legend>
                    <form:form id="buscarIns" method="get" action="/masterdata/buscarInstitucionesAprobadas.htm">
                         <div id="alert-area" ></div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="nombreInstitucion" class="control-label">Nombre de la Instituci&oacute;n</label>
                                <input type="text" class="form-control" id="nombreInstitucion" 
                                       placeholder="Nombre de la Instituci�n" maxlength="50"
                                       path="institutionName" name="institutionName"/>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="tipoInstitucionLabel" class="control-label">Tipo de Instituci&oacute;n</label>     
                                <select class="form-control" name="tipoInstitucion" id="tipoInstitucion" > 
                                    <option value='' label="Seleccione ..." selected></option>
                                    <c:if test="${not empty institutionTypes}">     
                                        <c:forEach items="${institutionTypes}" var="item" begin="0">

                                            <option  value="${item.id}">${item.name}</option>                                     
                                        </c:forEach>  
                                    </c:if>
                                </select>   
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="ccfLabel" class="control-label">CCF que Registra</label>     
                                <select class="form-control" name="ccfRegistra" id="ccfRegistra" > Registra
                                    <option value='' label="Seleccione ..." selected></option>
                                    <c:if test="${not empty ccfs}">     
                                        <c:forEach items="${ccfs}" var="item" begin="0">

                                            <option  value="${item.code}">${item.shortName}</option>                                     
                                        </c:forEach>  
                                    </c:if>
                                </select>   
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-sm-6" align="center">
                            <label for="fechaInicioBegin" class="control-label">Fecha de Aprobaci�n</label>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="fechaIniApro" class="control-label">Inicio</label>
                                        <input id="fechaIniApro" class="form-control datepicker" 
                                               data-date-format="dd-mm-yyyy" path="fechaIniApro" name = "fechaIniApro" 
                                               autocomplete="off"/>
                                        <fmt:formatDate value="${fechaIniApro}" pattern="dd-MM-yyyy" />
                                        <div class="clearfix"></div>
                                    </div>
                                </div>        
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="fechaFinApro" class="control-label">Fin</label>
                                        <input id="fechaFinApro" class="form-control datepicker" name="fechaFinApro"
                                               data-date-format="dd-mm-yyyy" path="fechaFinApro"  
                                               autocomplete="off"/>
                                        <fmt:formatDate value="${fechaFinApro}" pattern="dd-MM-yyyy" />
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>  
                        </div> 
                        <div class="col-sm-6" align="center">
                            <label for="theRegistryDate" class="control-label">Fecha de Registro</label>
                            <div class="form-group">
                                <div class="col-sm-6" >
                                    <div class="form-group">
                                        <label for="fechaIniReg" class="control-label">Inicio</label>
                                        <input id="fechaIniReg" class="form-control datepicker" 
                                               data-date-format="dd-mm-yyyy" path="fechaIniReg" name ="fechaIniReg"
                                               autocomplete="off"/>
                                        <fmt:formatDate value="${fechaIniReg}" pattern="dd-MM-yyyy" />
                                        <div class="clearfix"></div>
                                    </div>
                                </div>       
                                <div class="col-sm-6" >
                                    <div class="form-group">
                                        <label for="fechaFinReg" class="control-label">Fin</label>
                                        <input id="fechaFinReg" class="form-control datepicker" 
                                               data-date-format="dd-mm-yyyy" path="fechaFinReg" name ="fechaFinReg"
                                               autocomplete="off"/>
                                        <fmt:formatDate value="${fechaFinReg}" pattern="dd-MM-yyyy" />
                                        <div class="clearfix"></div>
                                    </div>
                                </div>       
                            </div>  
                        </div> 
                        <div class="col-sm-offset-4 col-sm-8" style="text-align:right">
                            <input onclick="verificarForm()" type="button" value="Buscar" class="btn btn-success">
                        </div>
                    </form:form>
                </legend>
                <div class="clearfix"></div>
            </div>
        </div>

        <br/>
        <div class="panel panel-default">
            <div class="panel-heading"><h2>Detalle Consulta Banco de Oferentes</h2></div>
            <div class="panel-body">
                <div class=" table-responsive">
                    <table id="institutions" class="table table-striped table-bordered" 
                           cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nombre de la Instituci&oacute;n</th>
                                <th>Tipo de Instituci&oacute;n</th>
                                <th>Estado</th>
                                <th>Fecha de aprobaci&oacute;n</th>
                                <th style="width: 200px; text-align: center">Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Nombre de la Instituci&oacute;n</th>
                                <th>Tipo de Instituci&oacute;n</th>
                                <th>Estado</th>
                                <th>Fecha de aprobaci&oacute;n</th>
                                <th style="width: 200px; text-align: center">Acciones</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:if test="${not empty institutionList}">
                                <c:forEach var="item" items="${institutionList}">
                                    <tr>
                                        <td>${item.institutionName}</td>
                                        <td>${item.type}</td>
                                        <td>${item.status.label}</td>
                                        <td>${item.approvedDate}</td>
                                        <td>
                                <center>
                                    <div class="btn-group" role="group" >
                                        <table>
                                            <tr>
                                                <td>
                                                    <button type="button" class="btn btn-info btn-xs"
                                                            data-toggle="modal" data-target="#detailModal"
                                                            onclick="loadInstitutionDetail(${item.id})">
                                                        <i class="fa fa-id-card-o" aria-hidden="true"></i> 
                                                        Ver
                                                    </button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </center>
                                </td>
                                </tr>
                            </c:forEach>
                        </c:if>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="modal fade" id="detailModal" role="dialog" 
             aria-labelledby="creationModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="creationModalLabel">Detalle de la Instituci&oacute;n</h4>
                    </div>

                    <div class="modal-body" style="height: 100vh">
                        <iframe id="detailArea" width="100%" height="100%" style="border: 0">
                        </iframe>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/dataTables.buttons.min.js"></script>
        <script src="resources/js/buttons.flash.min.js"></script>
        <script src="resources/js/jszip.min.js"></script>
        <script src="resources/js/pdfmake.min.js"></script>
        <script src="resources/js/vfs_fonts.js"></script>
        <script src="resources/js/buttons.html5.min.js"></script>
        <script src="resources/js/buttons.print.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/jquery.ui.widget.js"></script>
        <script src="resources/js/jquery.iframe-transport.js"></script>
        <script src="resources/js/bootstrap-confirmation.js"></script>
        <script src="resources/js/jquery.fileupload.js"></script>
        <script src="resources/js/jquery.validate.min.js"></script>
        <script src="resources/js/moment.js"></script>
        <script>

            $(document).ready(function () {
                
//                $('.container').hide();
                
                $('#institutions').DataTable({
                    "language": {
                        "url": "resources/js/Spanish.json"
                    }, "initComplete": function (settings, json) {
                        $(".dt-buttons").each(function (index) {
                            console.log(index + ": " + $(this).text());
                            if ($(this).find('.exportar').length === 0) {
                                $(this).prepend('<label class="exportar">Exportar a:&nbsp;&nbsp;</label>');
                            }
                        });
                    },
                    "bFilter": false,
                    "bInfo": false,
                    dom: 'Blfrtip',
                    buttons: [
                        {
                            extend: 'csv',
                            fieldBoundary: '',
                            footer: false,
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'CSV',
                            exportOptions: {
                                columns: [0, 1, 2, 3]
                            }
                        },
                        {
                            extend: 'excel',
                            footer: false,
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'EXCEL',
                            exportOptions: {
                                columns: [0, 1, 2, 3]
                            }
                        },
                        {
                            extend: 'pdf',
                            footer: false,
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'PDF',
                            exportOptions: {
                                columns: [0, 1, 2, 3]
                            }
                        },
                        {
                            extend: 'print',
                            footer: false,
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'COPY',
                            exportOptions: {
                                columns: [0, 1, 2, 3]
                            }
                        }
                    ]
                });
                
                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Mi�rcoles", "Jueves", "Viernes", "S�bado", "Domingo"],
                    daysShort: ["Dom", "Lun", "Mar", "Mi�", "Jue", "Vie", "S�b", "Dom"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Hoy"
                };

                $('#fechaIniApro').datepicker({language: "es", autoclose: true});
                $('#fechaFinApro').datepicker({language: "es", autoclose: true});
                $('#fechaIniReg').datepicker({language: "es", autoclose: true});
                $('#fechaFinReg').datepicker({language: "es", autoclose: true});
            });
            
            function loadInstitutionDetail(id) {
                $('#detailArea').attr("src",
                        'detalle-institucion-oferente.htm?id=' + id + "&notSedes=" + true);
            }
            
            function verificarForm(){
                
                
                
                if($('#nombreInstitucion').val() === "" && $('#tipoInstitucion').val() === "" && $('#ccfRegistra').val() === "" &&
                        $('#fechaIniApro').val() === "" && $('#fechaFinApro').val() === "" && $('#fechaIniReg').val() === "" &&
                        $('#fechaFinReg').val ||
                        $('#fechaIniApro').val() !== "" && $('#fechaFinApro').val() === "" ||
                        $('#fechaIniApro').val() === "" && $('#fechaFinApro').val() !== "" ||
                        $('#fechaIniReg').val() !== "" && $('#fechaFinReg').val() === "" || 
                        $('#fechaIniReg').val() === "" && $('#fechaFinReg').val() !== ""){
                               $("#alert-area").append($("<div class='alert alert-message alert-danger" +
                            " fade in' data-alert><p><font size=3>Aseg�rese de seleccionar un filtro.</font></p></div>"));
                                $(".alert-message").delay(3000).fadeOut("slow", function () {
                        $(this).remove();
                    });
                                
                } else 
                if ((moment($('#fechaIniApro').val(),"DD-MM-YYYY") > moment($('#fechaFinApro').val(),"DD-MM-YYYY")))
                {$("#alert-area").append($("<div class='alert alert-message alert-danger" +
                            " fade in' data-alert><p><font size=3>En la fecha de aprobaci�n, la fecha fin debe ser mayor que la fecha inicio.</font></p></div>"));
                                $(".alert-message").delay(3000).fadeOut("slow", function () {
                        $(this).remove();
                    });}
                
       else if (moment($('#fechaIniReg').val(),"DD-MM-YYYY") > moment($('#fechaFinReg').val(),"DD-MM-YYYY"))
                {
                    
                         $("#alert-area").append($("<div class='alert alert-message alert-danger" +
                            " fade in' data-alert><font size=3>En la fecha de registro, la fecha fin debe ser mayor que la fecha inicio.</font></div>"));
                                $(".alert-message").delay(3000).fadeOut("slow", function () {
                        $(this).remove();
                    });
                } else{
                   
                    $('#buscarIns').submit();
                }
            }

        </script>
        <style>
            .buttons-csv{
                display: inline-block;
                background-image:url(resources/images/csv.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-csv span{
                opacity: 0;
            }
            .buttons-excel{
                display: inline-block;
                background-image:url(resources/images/excel.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-excel span{
                opacity: 0;
            }
            .buttons-pdf{
                display: inline-block;
                background-image:url(resources/images/pdf.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-pdf span{
                opacity: 0;
            }
            .buttons-print{
                display: inline-block;
                background-image:url(resources/images/print.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-print span{
                opacity: 0;
            }

            div.dt-buttons {
                float: right;
                margin-left:10px;
            }
        </style>
    </body>
</html>
