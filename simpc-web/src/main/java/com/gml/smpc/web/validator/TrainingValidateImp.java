/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Light.java
 * Created on: 2017/01/18, 04:30:10 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.smpc.web.validator;

import com.gml.simpc.loader.dto.ValidationErrorDto;
import com.gml.simpc.loader.validator.ValidateCommand;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

/**
 * Clase para implementar validaciones particulares de negocio en carges de
 * información
 * en cuanto a capacitación
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica
 * Jim&eacute;nez Rozo</a>
 */
@Service(value = "trainingValidateService")
public class TrainingValidateImp implements ValidateCommand {

    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(
        TrainingValidateImp.class);

    /**
     * Consulta para validaciones sobre la institucion que llega
     * 1. Que la institucion exista
     * 2. Que la institucion este activa
     */
    private static final String INST_RULE_QUERY =
        " select stage_data.LINEA,   " +
        " 'Institución no existe ('||stage_data.codigo_inst||')' MENSAJE  " +
        " from ST_CAP_PROGRAMA_MODULO stage_data " +
        " left join instituciones inst on stage_data.codigo_inst = inst.id " +
        " where  stage_data.CARGA_ID=?" +
        " and inst.id is null" +
        " UNION " +
        " select stage_data.LINEA, " +
        " 'Institucion inactiva o sin aprobar ('||stage_data.codigo_inst||')' MENSAJE" +
        " from ST_CAP_PROGRAMA_MODULO stage_data,instituciones inst" +
        " where stage_data.CARGA_ID=?" +
        " and stage_data.codigo_inst = inst.id" +
        " and (inst.estado = 'I' or inst.ESTADO_APROBACION != 'A' ) ";
    

    /**
     * Consulta para validaciones sobre la sede que llega
     * 1. Que la sede exista
     */
    private static final String HEADQ_RULE_QUERY =
        " select stage_data.LINEA,   " +
        " 'Sede no existe ('||stage_data.codigo_sede||')' MENSAJE  " +
        " from ST_CAP_PROGRAMA_MODULO stage_data " +
        "  left join sedes s on stage_data.codigo_sede = s.CODIGO " +
        "  where  stage_data.CARGA_ID= ? and s.codigo is null";
    
    /**
     * Consulta para validaciones que la sede se encuentre ligada a la institución
     *
     */
    private static final String HEADQ_INST_RULE_QUERY =
        "SELECT STAGE_DATA.LINEA, 'La sede ('||stage_data.codigo_sede||') " +
        "no se encuentra ligada a la institución " +
        "('||stage_data.codigo_inst||')' MENSAJE " +  
        "FROM ST_CAP_PROGRAMA_MODULO STAGE_DATA " +
        "LEFT JOIN SEDES S ON STAGE_DATA.CODIGO_SEDE = S.CODIGO " +
        "AND STAGE_DATA.CODIGO_INST = S.ID_INSTITUCION " +  
        "WHERE STAGE_DATA.CARGA_ID = ? AND S.CODIGO IS NULL";

    /**
     * Plantilla JDBC para consultar las propiedades.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Atributo para almacenar la lista de errores
     */
    List<ValidationErrorDto> errorsList;

    /**
     * Verifica si es del tipo que le corresponde.
     *
     * @param loadType
     *
     * @return
     */
    @Override
    public boolean verify(String loadType) {
        return "CAP-1".equals(loadType);
    }

    /**
     * Realiza la validación.
     *
     * @param loadId
     *
     * @return
     */
    @Override
    public List<ValidationErrorDto> validate(Long loadId) {

        LOGGER.
            info("Validating training bussiness rules for load ... " + loadId);
        this.errorsList = new ArrayList<>();
        validationQuery("institution", INST_RULE_QUERY, new Long[]{loadId,
            loadId});
        validationQuery("headquarter", HEADQ_RULE_QUERY, new Long[]{loadId});
        validationQuery("headquarter", HEADQ_INST_RULE_QUERY, new Long[]{loadId});
        LOGGER.info("Final errors: " + this.errorsList.size());
        return this.errorsList;
    }

    private void validationQuery(String type, String query, Long[] params) {
        try {
            List<ValidationErrorDto> resultList =
                this.jdbcTemplate.query(query, params, new RowMapper() {
                    @Override
                    public ValidationErrorDto mapRow(ResultSet rs, int rownumber)
                        throws SQLException {
                        ValidationErrorDto e = new ValidationErrorDto(
                            Integer.parseInt(rs.getString("LINEA")),
                            rs.getString("MENSAJE"));

                        return e;
                    }
                });
            LOGGER.info("Validating rules " + type + ". Errors foud: " +
                resultList.size());
            addErrors(resultList);
        } catch (Exception e) {
            LOGGER.error("Error executing bussiness validation type " + type +
                ": " + e);
            LOGGER.error("Validation query  " + query + " params " + params);
        }

    }

    private void addErrors(List<ValidationErrorDto> errorsList) {
        for (ValidationErrorDto validationErrorDto : errorsList) {
            this.errorsList.add(validationErrorDto);
        }
    }

}
