/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserRepository.java
 * Created on: 2016/10/19, 02:03:51 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository;

import com.gml.simpc.api.entity.Headquarter;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Repositorio para las sedes
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public interface HeadquarterRepository
    extends JpaRepository<Headquarter, Long> {

    /**
     * Este query trae los sedes dada una instituci&oacute;n
     *
     * @param institution
     *
     * @return
     */
    List<Headquarter> findByInstitutionId(Long institution);

    /**
     * Este query trae la sede principal de una institci&oacute;n
     *
     * @param institution
     * @param principal
     *
     * @return
     */
    List<Headquarter> findByInstitutionIdAndPrincipal(Long institution,
        boolean principal);

    /**
     * Este query filtra las sedes.
     *
     * @param name
     * @param stateId
     * @param cityId
     * @param insitutionId
     *
     * @return
     */
    @Query(value =
        "SELECT * FROM SEDES WHERE UPPER(NOMBRE) LIKE UPPER(?1) AND ID_DEPARTAMENTO LIKE ?2 " +
        "AND ID_MUNICIPIO LIKE ?3 AND ID_INSTITUCION = ?4",
        nativeQuery = true)
    List<Headquarter> findByFilters(String name, String stateId,
        String cityId, Long insitutionId);

    /**
     * Query que retorna la secuencia actual de la tabla sedes.
     *
     */
    @Query(value = "SELECT INSTITUTION_SEQ.NEXTVAL FROM DUAL",
        nativeQuery = true)
    long headquarterSequence();
    
    /**
     * Encontrar sede por codigo
     */
    Headquarter findByCodigo(String code);

}
