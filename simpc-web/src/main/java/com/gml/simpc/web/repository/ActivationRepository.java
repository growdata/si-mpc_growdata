/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ActivationRepository.java
 * Created on: 2016/10/19, 02:03:51 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository;

import com.gml.simpc.api.entity.Activate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repositorio para tokens de activaci&oacute;n.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Transactional
public interface ActivationRepository extends JpaRepository<Activate, Long> {

    /**
     * Busca si el token ingresado al activar el usuario coincide con el que
     * esta en base tras el registro
     *
     * @param token Token generado en el momento del registro
     *
     * @return Entidad de activaci&ocute;n
     */
    Activate findByUserToken(String token);

    /**
     * Elimina los tokens de creaci&oacute;n de cuentas.
     *
     * @return
     */
    @Modifying
    @Query(value = "DELETE FROM CUENTAS WHERE FECHA_SESION + INTERVAL '1440' " +
        "MINUTE < SYSDATE", nativeQuery = true)
    Integer deleteExpiratedTokens();

    /**
     * Elimina todos los tokens de un usuario.
     *
     * @param userId
     */
    @Modifying
    @Query(nativeQuery = true, value = "DELETE FROM CUENTAS WHERE usuario = ?1")
    void deleteTokensByUser(Long userId);
}
