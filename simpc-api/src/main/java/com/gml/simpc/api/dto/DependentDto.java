/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: DependentDto.java
 * Created on: 2017/01/05, 04:42:32 PM
 * Objetivo: Mapear los campos requeridos para una carga de archivos
 * de dependientes
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

/**
 * Transfiere la informacion de los campos de beneficios economicos
 * en relacion a los dependientes.
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
public class DependentDto {

    private String dependentOrder;
    private String eventType;
    private String idType;
    private String numberId;
    private String personInChargeTypeId;
    private String postulationDateCheck;
    private String personInChargeNumber;
    private String personInCharge;
    private String firstName;
    private String secondName;
    private String firstSurname;
    private String secondSurname;
    private String birthday;
    private String gender;
    private String personInChargeRelationship;
    private String personInChargeStudiesCurrently;
    private Long cargaId;

    public String getDependentOrder() {
        return dependentOrder;
    }

    public String getEventType() {
        return eventType;
    }

    public String getIdType() {
        return idType;
    }

    public String getNumberId() {
        return numberId;
    }

    public String getPersonInChargeTypeId() {
        return personInChargeTypeId;
    }

    public String getPersonInChargeNumber() {
        return personInChargeNumber;
    }

    public void setFirstSurname(String firstSurname) {
        this.firstSurname = firstSurname;
    }

    public String getFirstSurname() {
        return firstSurname;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getSecondSurname() {
        return secondSurname;
    }

    public String getGender() {
        return gender;
    }

    public String getPersonInChargeRelationship() {
        return personInChargeRelationship;
    }

    public String getPersonInChargeStudiesCurrently() {
        return personInChargeStudiesCurrently;
    }

    public void setDependentOrder(String dependentOrder) {
        this.dependentOrder = dependentOrder;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public void setNumberId(String numberId) {
        this.numberId = numberId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setPersonInChargeTypeId(String personInChargeTypeId) {
        this.personInChargeTypeId = personInChargeTypeId;
    }

    public void setPersonInChargeNumber(String personInChargeNumber) {
        this.personInChargeNumber = personInChargeNumber;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public Long getCargaId() {
        return cargaId;
    }

    public void setCargaId(Long cargaId) {
        this.cargaId = cargaId;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setPersonInChargeRelationship(String personInChargeRelationship) {
        this.personInChargeRelationship = personInChargeRelationship;
    }

    public void setPersonInChargeStudiesCurrently(
        String personInChargeStudiesCurrently) {
        this.personInChargeStudiesCurrently = personInChargeStudiesCurrently;
    }

    public String getPersonInCharge() {
        return personInCharge;
    }

    public void setPersonInCharge(String personInCharge) {
        this.personInCharge = personInCharge;
    }

    public String getPostulationDateCheck() {
        return postulationDateCheck;
    }

    public void setPostulationDateCheck(String postulationDateCheck) {
        this.postulationDateCheck = postulationDateCheck;
    }
    

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CargaId=").append(cargaId).
            append("~&~Tipo de Identificación=").append(idType).
            append("~&~Número de Identificación=").append(numberId).
            append("~&~Orden de dependiente=").append(dependentOrder).
            append("~&~Novedad=").append(eventType).
            append("~&~Fecha de postulacion al MPC=").append(postulationDateCheck).
            append("~&~Tipo de Identificación de persona a cargo=").append(
            personInChargeTypeId).
            append("~&~Número de Identificacion de persona a cargo=").append(
            personInChargeNumber).
            append("~&~Personas a cargo=").append(
            personInCharge).
            append("~&~Primer Nombre=").append(firstName).
            append("~&~Segundo Nombre=").append(secondName).
            append("~&~Primer Apellido=").append(firstSurname).
            append("~&~Segundo Apellido=").append(secondSurname).
            append("~&~Fecha de Nacimiento=").append(birthday).
            append("~&~Genero=").append(gender).
            append("~&~Relación con persona a cargo=").
            append(personInChargeRelationship).
            append("~&~Persona a cargo estudia Actualmente =").append(
            personInChargeStudiesCurrently);

        return stringBuilder.toString();
    }

}