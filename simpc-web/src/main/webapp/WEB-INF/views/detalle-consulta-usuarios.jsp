<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
    </head>
    <body>
        <c:if test="${not empty msg}">
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        <div class="col-sm-12">
            <h1>Datos de Usuario</h1>
            <br/>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <input type="hidden" id="id" name="id" path="id"/>
                <label for="nomeroDocumento" class=" control-label">Numero de Identificación</label>
                <p class="form-control-static">${parentUser.identificationNumber}</p>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="primerNombre" class="control-label">Primer Nombre</label>
                <p class="form-control-static">${parentUser.firstName}</p>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="segundoNombre" class="control-label">Segundo Nombre</label>
                <p class="form-control-static">${parentUser.secondName}</p>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="primerApellido" class=" control-label">Primer Apellido</label>
                <p class="form-control-static">${parentUser.firstSurname}</p>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="segundoApellido" class="control-label">Segundo Apellido</label>
                <p class="form-control-static">${parentUser.secondSurname}</p>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="direccionPredio" class="control-label">Dirección</label>
                <p class="form-control-static">${parentUser.address}</p>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="direccionPredio" class="control-label">Municipio</label>
                <p class="form-control-static">${parentUser.state.name}</p>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="direccionPredio" class="control-label">Departamento</label>
                <p class="form-control-static">${parentUser.city.name}</p>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="telefono" class="control-label">Telefono de Contacto</label>
                <p class="form-control-static">${parentUser.phoneNumber}</p>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="correoElectronico" class=" control-label">E-mail de Contacto</label>
                <p class="form-control-static">${parentUser.email}</p>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="nombreUsuario" class=" control-label">Nombre de Usuario</label>
                <p class="form-control-static">${parentUser.username}</p>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="cajaCompensacion" class="control-label">Entidad</label>
                <p class="form-control-static">${parentUser.ccf.name}</p>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="estado" class="control-label">Estado de Usuario</label>
                <p class="form-control-static">${parentUser.status.label}</p>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="perfil" class=" control-label">Perfil</label>
                <p class="form-control-static">${parentUser.profile.name}</p>
            </div>
        </div>
        
        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script>
            
        </script>
    </body>
</html>
