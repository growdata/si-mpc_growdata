<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-toggle.min.css" rel="stylesheet"/>
        <link href="resources/css/print-buttons.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>

    </head>
    <body>
        <c:if test="${not empty msg}">
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        <br>
        <label for="iexpo" class="control-label pull-right">Descargar en PDF:
            <div class="btn-group ">

                <button type="button"  onclick='window.print()' class="btn buttons-pdf"></button>
            </div>
        </label>
        <br>
        <div class="col-sm-12">
            <center>
                <label for="programName" class="control-label"><strong>${programName}</strong></label>
            </center>
        </div>
        <br>
        <br>
        <br>
        <div class="col-sm-4 col-sm-12 row">

        </div>

        <table id="programas" class="table " width="100%" >
            <thead>
                <tr>
                    <th>C�digo del Modulo</th>
                    <th>Nombre del Modulo</th>
                    <th>Fecha de Inicio</th>                               
                    <th>Fecha de Finalizaci�n</th>
                    <th>Horario</th>
                    <th>(%)Asistencia Requerido</th>
                    <th>Costo de la Matricula</th>
                    <th>Otros Costos</th>
                    <th>Sede</th>
                </tr>
            </thead>
            <c:forEach items="${ModulesList}" var="modulos" begin="0">
                <tbody>
                <td>${modulos.moduleCode}</td>
                <td>${modulos.moduleName}</td>
                <td>${modulos.startDate}</td>
                <td>${modulos.endDate}</td>
                <td>${modulos.schedule}</td>
                <td>${modulos.percentageRequire}</td>
                <td>${modulos.enrollmentCost}</td>
                <td>${modulos.otherCost}</td>
                <td>${modulos.intitutionHqCode}</td>

            </tbody> 
        </c:forEach>
    </table>




    <script src="resources/js/jquery-1.12.3.js"></script>
    <script src="resources/js/jquery.dataTables.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/dataTables.bootstrap.min.js"></script>
    <script src="resources/js/bootstrap-datepicker.js"></script>
    <script src="resources/js/bootstrap-confirmation.js"></script>
    <script src="resources/js/jquery.ui.widget.js"></script>
    <script src="resources/js/jquery.iframe-transport.js"></script>
    <script src="resources/js/jquery.fileupload.js"></script>
    <script src="resources/js/bootstrap-toggle.min.js"></script>  
    <script src="resources/js/dataTables.buttons.min.js"></script>
    <script src="resources/js/buttons.flash.min.js"></script>
    <script src="resources/js/jszip.min.js"></script>
    <script src="resources/js/pdfmake.min.js"></script>
    <script src="resources/js/vfs_fonts.js"></script>
    <script src="resources/js/buttons.html5.min.js"></script>
    <script src="resources/js/buttons.print.min.js"></script>
    <style>


        .buttons-pdf{
            display: inline-block;
            background-image:url(resources/images/pdf.png);
            cursor:pointer !important;
            width: 32px !important;
            height: 32px !important;
            border: none !important;

        }
        .buttons-pdf span{
            opacity: 0;
        }
        .buttons-print{
            display: inline-block;
            background-image:url(resources/images/print.png);
            cursor:pointer !important;
            width: 32px !important;
            height: 32px !important;
            border: none !important;


        }
        .buttons-print span{
            opacity: 0;

        }
        div.dt-buttons {
            float: right;
            margin-left:10px;
        }

    </style>
</body>
</html>
