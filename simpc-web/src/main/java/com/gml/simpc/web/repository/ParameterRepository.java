/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IRepository.java
 * Created on: 2016/10/24, 09:59:11 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository;

import com.gml.simpc.api.entity.Parameter;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repositorio de par&aacute;metros.
 *
 * @author <a href="mailto:jonathanp@gmlsoftware.com">Jonathan Ponton</a>
 */
public interface ParameterRepository extends JpaRepository<Parameter, Long> {

    /**
     * M&eacute;todo para consultar par&aacute;metros por nombre.
     *
     * @param name
     *
     * @return
     */
    List<Parameter> findBySpNameLikeIgnoreCase(String name);

    /**
     * M&eacute;todo para consultar par&aacute;metros por nombre.
     *
     * @param name
     *
     * @return
     */
    Parameter findByName(String name);
}
