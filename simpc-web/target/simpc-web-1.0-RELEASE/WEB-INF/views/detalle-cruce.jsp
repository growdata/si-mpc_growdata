<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <style>
            input[type=number]::-webkit-outer-spin-button,
            input[type=number]::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            input[type=number] {
                -moz-appearance:textfield;
            }
        </style>
    </head>
    <body>
        <c:if test="${not empty msg}">
            <br/>
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        <br/>
        <div class="row">
            <div class="col-sm-5">
                <label for="id" class=" control-label">Id de la carga</label>
                <p class="form-control-static" id="fechaCargues">${loadProcess.id}</p>
            </div>
            <div class="col-sm-5">
                <label for="nombreArchivos" class="control-label">Nombre del Archivos</label>
                <p class="form-control-static" id="nombreArchivos">${loadProcess.originalFileName}</p>
            </div>
            <div class="col-sm-5">
                <label for="fechaCargues" class=" control-label">Tipo De Cruce</label>
                <p class="form-control-static" id="fechaCargues">${loadProcess.initDate}</p>
            </div>
            <div class="col-sm-5">
                <label for="fechaFinCargues" class="control-label">Estado</label>
                <p class="form-control-static" id="fechaFinCargues">${loadProcess.endDate}</p>
            </div>
            <div class="col-sm-5">
                <label for="peso" class=" control-label">Total de Registros </label>
                <p class="form-control-static" id="peso"></p>
            </div>
            <label for="descripcion" class=" control-label">${file1}</label><br/>
                
               
                    <form id="downloadStrcForm${loadProcess.id}" name="downloadStrcForm${loadProcess.id}" action="descargar-errores.htm" >
                        <input type="hidden" name="type" id="type" value="estructura">
                        <input type="hidden" name="loadId" id="loadId" value="${loadProcess.id}">
                        <button type="button" class="btn btn-success btnDownloadBuss" id="btnDownloadStrc" name="btnDownloadStrc" itemID="${loadProcess.id}">
                            <i class="fa fa-download" aria-hidden="true"></i>
                            Descargar
                        </button>    
                    </form>   
                
            
        </div>    
       

        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/jquery.ui.widget.js"></script>
        <script src="resources/js/jquery.iframe-transport.js"></script>
        <script src="resources/js/jquery.fileupload.js"></script>
        <script src="resources/js/jquery.validate.min.js"></script>
        <script src="resources/js/tooltipster.bundle.js"></script>
        <script src="resources/js/custom-functionalities.js"></script>
        <script>
            $(document).ready(function () {
                $(".btnDownloadBuss").click(function () {
                    var id = $(this).attr("itemId");
                    $('#downloadBussForm' + id).submit();
                });

                $(".btnDownloadStrc").click(function () {
                    var id = $(this).attr("itemId");
                    $('#downloadStrcForm' + id).submit();
                });
            });

        </script>                                           
    </body>
</html>    
