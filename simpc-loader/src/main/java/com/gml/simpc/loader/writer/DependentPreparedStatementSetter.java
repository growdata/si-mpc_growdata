/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: OrientationPreparedStatementSetter.java
 * Created on: 2017/01/20, 12:16:58 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 * Objetive: se prepara el statement para cada campo para las tablas de stage y master
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.writer;

import com.gml.simpc.api.dto.DependentDto;
import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.springframework.batch.item.database.ItemPreparedStatementSetter;

/**
 * Transfiere la informacion de los Beneficios Economicos (Archivo dependientes)
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
public class DependentPreparedStatementSetter
    implements ItemPreparedStatementSetter<DependentDto> {

    @Override
    public void setValues(DependentDto dependentDto, PreparedStatement ps)
        throws SQLException {
        ThreadLocalJob.setProcesed();
        JobResultDto jobResult = ThreadLocalJob.getResult();
     
        int line = jobResult.getProcesedLines();
        ps.setString(1, dependentDto.getDependentOrder());
        ps.setString(2, dependentDto.getEventType());
        ps.setString(3, dependentDto.getIdType());
        ps.setString(4, dependentDto.getNumberId());
        ps.setString(5, dependentDto.getPostulationDateCheck());
        ps.setString(6, dependentDto.getPersonInChargeTypeId());
        ps.setString(7, dependentDto.getPersonInChargeNumber());
        ps.setString(8, dependentDto.getFirstName());
        ps.setString(9, dependentDto.getSecondName());
        ps.setString(10, dependentDto.getFirstSurname());
        ps.setString(11, dependentDto.getSecondSurname());
        ps.setString(12, dependentDto.getBirthday());
        ps.setString(13, dependentDto.getGender());
        ps.setString(14, dependentDto.getPersonInChargeRelationship());
        ps.setString(15, dependentDto.getPersonInChargeStudiesCurrently());   
        ps.setString(16, Integer.toString(line));
        
    }

}
