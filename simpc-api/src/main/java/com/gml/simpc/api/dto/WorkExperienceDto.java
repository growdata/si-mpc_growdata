/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: basicInformation.java
 * Created on: 2016/12/27, 04:23:26 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Leonardo Rocha</a>
 */
public class WorkExperienceDto {

    private String documentType;
    private String numberIdentification;
    private String workExperienceType;
    private String product;
    private String peopleAmount;
    private String companyName;
    private String sector;
    private String companyPhone;
    private String country;
    private String position;
    private String equivalentPosition;
    private String admisionDate;
    private String currentlyWorking;
    private String retirementDate;
    private Long cargaId;


    /*
     * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public WorkExperienceDto() {

    }

    public WorkExperienceDto(String documentType, String numberIdentification,
        String workExperienceType, String product,
        String peopleAmount, String companyName, String sector,
        String companyPhone, String country, String position,
        String equivalentPosition, String admisionDate, String currentlyWorking,
        String retirementDate, Long cargaId) {
        this.documentType = documentType;
        this.numberIdentification = numberIdentification;
        this.workExperienceType = workExperienceType;
        this.product = product;
        this.peopleAmount = peopleAmount;
        this.companyName = companyName;
        this.sector = sector;
        this.companyPhone = companyPhone;
        this.country = country;
        this.position = position;
        this.equivalentPosition = equivalentPosition;
        this.admisionDate = admisionDate;
        this.currentlyWorking = currentlyWorking;
        this.retirementDate = retirementDate;
        this.cargaId = cargaId;
    }

    public Long getCargaId() {
        return cargaId;
    }

    public void setCargaId(Long cargaId) {
        this.cargaId = cargaId;
    }

    public String getWorkExperienceType() {
        return workExperienceType;
    }

    public void setWorkExperienceType(String workExperienceType) {
        this.workExperienceType = workExperienceType;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getPeopleAmount() {
        return peopleAmount;
    }

    public void setPeopleAmount(String peopleAmount) {
        this.peopleAmount = peopleAmount;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getEquivalentPosition() {
        return equivalentPosition;
    }

    public void setEquivalentPosition(String equivalentPosition) {
        this.equivalentPosition = equivalentPosition;
    }

    public String getAdmisionDate() {
        return admisionDate;
    }

    public void setAdmisionDate(String admisionDate) {
        this.admisionDate = admisionDate;
    }

    public String getCurrentlyWorking() {
        return currentlyWorking;
    }

    public void setCurrentlyWorking(String currentlyWorking) {
        this.currentlyWorking = currentlyWorking;
    }

    public String getRetirementDate() {
        return retirementDate;
    }

    public void setRetirementDate(String retirementDate) {
        this.retirementDate = retirementDate;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getNumberIdentification() {
        return numberIdentification;
    }

    public void setNumberIdentification(String numberIdentification) {
        this.numberIdentification = numberIdentification;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder=new StringBuilder();
        stringBuilder.append("Tipo de experiencia Laboral=" ).append( 
            workExperienceType ).
            append("~&~Producto o Servicio Comercializado=" ).append(product ).
            append("~&~Cantidad de Personas con las que trabaja=" ).append(peopleAmount ).
            append("~&~Nombre de la empresa=" ).append( companyName ).
            append("~&~Sector=" ).append( sector ).
            append("~&~Tel�fono de la compa��a=" ).append( companyPhone ).
            append("~&~Pa�s=" ).append( country ).
            append("~&~Cargo=" ).append( position ).
            append("~&~Cargo Equivalente=" ).append( equivalentPosition ).
            append("~&~Fecha de Admisi�n=").append(admisionDate ).
            append("~&~Actualmente Trabajando=" ).append( currentlyWorking ).
            append("~&~Fecha de retiro=" ).append( retirementDate);
        return stringBuilder.toString();
    }

}
