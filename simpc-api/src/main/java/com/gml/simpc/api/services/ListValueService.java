/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: DocumentTypeService.java
 * Created on: 2017/01/11, 04:06:13 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.dto.ValueListDto;
import java.util.List;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public interface ListValueService {

    /**
     * Obtiene todos los registros de cada tabla de lista de valor
     *
     * @return
     */
    List<ValueListDto> getValueListByTableName(String tableName);
}
