/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: InstitutionCreateTest.java
 * Created on: 2016/11/15, 05:13:28 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.test;

import com.gml.simpc.api.entity.Institution;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.entity.exeption.SystemException;
import com.gml.simpc.api.entity.exeption.code.ErrorCode;
import com.gml.simpc.api.entity.valuelist.InstitutionType;
import com.gml.simpc.api.enums.ApprovalInstitutionStatus;
import com.gml.simpc.api.enums.LegalNature;
import com.gml.simpc.api.enums.Origin;
import com.gml.simpc.api.services.InstitutionService;
import com.gml.simpc.api.utilities.Util;
import com.gml.simpc.commons.interceptor.Interceptor;
import com.gml.simpc.web.repository.InstitutionRepository;
import com.gml.simpc.web.repository.UserRepository;
import com.gml.simpc.web.service.impl.InstitutionServiceImpl;
import java.util.Date;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.fail;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@RunWith(MockitoJUnitRunner.class)
public class InstitutionCreateTest extends Mockito {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(InstitutionCreateTest.class);
    /**
     * correo de prueba para los test
     */
    private static final String CORREO_FOR_TEST = "correoTest@gmail.com";
    /**
     * numero de telefono de prueba
     */
    private static final String PHONE_FOR_TEST = "3057777422";
    /**
     * id de usuario de pruebas
     */
    private static final Long ID_USER_QUEMADO = 1L;
    /**
     * id de usuario de pruebas
     */
    private static int VERIFICATION_DIGIT_TEST = 341;
    /**
     * NIT para la instituci&oacute;n de la prueba uniaria
     */
    private static final Long NIT_INSTITUTION_TEST = 123123L;

    /**
     * Instancia de la clase institutionController para hacerle test al
     * m&eacute;todo que guarda las institutciones para probar que el
     * certificado de calidad si est� inclu�do cuando se trata de
     * una institutci&oacute;n tipo :
     * ?	Instituciones de Educaci�n para el Trabajo y Desarrollo Humano
     * ?	Cajas de Compensaci�n Familiar.
     *
     */
    @Mock
    private Interceptor interceptor;
    /**
     * Repositorio para instituciones.
     */
    @Mock
    private InstitutionRepository institutionRepository;

    /**
     * Repositorio para usuariows.
     */
    @Mock
    private UserRepository userRepository;
    
    @InjectMocks
    private InstitutionService institutionService = new InstitutionServiceImpl();

    @Test
    public void caseInstitutionTypeCCF() {
        try {
            /**
             * La primera prueba la hacemos enviando una instituci&oacute;n
             * tipo cajas de compensaci&oacute;n familiar sin certificado
             * de calidad, el m&eacute;todo debe disparar una userException
             */
            Institution institution = new Institution();
            institution.setContactEmail(CORREO_FOR_TEST);
            institution.setContactPhone(PHONE_FOR_TEST);
            institution.setCreationDate(new Date());
            institution.setCreationUserId(new User(ID_USER_QUEMADO));
            institution.setDv(VERIFICATION_DIGIT_TEST);
            institution.setId(Long.MIN_VALUE);
            institution.setInstitutionName(CORREO_FOR_TEST);
            institution.setLegalNature(LegalNature.N);
            institution.setNit(NIT_INSTITUTION_TEST.toString());
            institution.setOrigin(Origin.R);
            institution.setApprovalStatus(ApprovalInstitutionStatus.N);
            InstitutionType institutionTypeCCF = new InstitutionType();
            institutionTypeCCF.setId(
                Util.INSTITUTION_TYPE_CAJA_COMPENSACION_FAMILIAR);
            institution.setType(institutionTypeCCF);
            institutionService.save(institution);
            /**
             * Debe fallar pues le hemos mandado una instituci&oacute;n tipo
             * CCF sin certificado de calidad ni fecha de vencimiento
             */
            fail("Fall� por que cuando le envie instituci�n tipo CCF sin " +
                "certificado de calidad ni fecha de vencimiento no dispar� " +
                "user Exception");
        } catch (NullPointerException ex) {
            ex.printStackTrace();
            LOGGER.error("El m�todo genera null Pointer Exception ", ex);
            fail("Fall� por que  alguno de los par�metros que se supone deben" +
                " enviarse va nulo");
        } catch (SystemException uEx) {
            if (uEx.getErrorCode().equals(
                ErrorCode.ERR_003_INSTITUTION_VALIDATE_QUALITYCERTIFICATE)) {
                LOGGER.info(
                    "Se ha enviado user Exception cuando lo deber�a hacer, " +
                    "prueba superada.");
            } else {
                fail("Fall� por que env�o userException pero no el ErrorCode " +
                    "correcto");
            }
        }catch(Exception ex){
            fail("se present� excepci�n "+ex);
        }
    }

    @Test
    public void caseInstitutionTypeFormacionParaTrabajo() {
        try {
            /**
             * La segunda prueba la hacemos enviando una instituci&oacute;n
             * tipo cformaci&oacute;n para el trabajo sin certificado
             * de calidad, el m&eacute;todo debe disparar una userException
             */
            Institution institution = new Institution();
            institution.setContactEmail(CORREO_FOR_TEST);
            institution.setContactPhone(PHONE_FOR_TEST);
            institution.setCreationDate(new Date());
            institution.setCreationUserId(new User(ID_USER_QUEMADO));
            institution.setDv(VERIFICATION_DIGIT_TEST);
            institution.setId(Long.MIN_VALUE);
            institution.setInstitutionName(CORREO_FOR_TEST);
            institution.setLegalNature(LegalNature.N);
            institution.setNit(NIT_INSTITUTION_TEST.toString());
            institution.setOrigin(Origin.R);
            institution.setApprovalStatus(ApprovalInstitutionStatus.N);
            InstitutionType institutionTypeFormacionTRabajo =
                new InstitutionType();
            institutionTypeFormacionTRabajo.setId(
                Util.INSTITUTION_TYPE_FORMACION_TRABAJO);
            institution.setType(institutionTypeFormacionTRabajo);
            if (institutionService == null) {
                LOGGER.error(" el institutionService va nulo");
            }
            institutionService.save(institution);
            /**
             * Debe fallar pues le hemos mandado una instituci&oacute;n tipo
             * Formaci�n para el trabajo sin certificado de calidad
             * ni fecha de vencimiento
             */
            fail("Fall� por que cuando le envie instituci�n tipo formaci�n " +
                "para el trabajo sin certificado de calidad ni fecha de " +
                "vencimiento no dispar� user Exception");
        } catch (NullPointerException ex) {
            ex.printStackTrace();
            LOGGER.error("El m�todo genera null Pointer Exception ", ex);
            fail("Fall� por que  alguno de los par�metros que se supone deben" +
                " enviarse va nulo");
        } catch (SystemException uEx) {
            if (uEx.getErrorCode().equals(
                ErrorCode.ERR_003_INSTITUTION_VALIDATE_QUALITYCERTIFICATE)) {
                LOGGER.info(
                    "Se ha enviado user Exception cuando lo deber�a hacer, " +
                    "prueba superada.");
            } else {
                fail("Fall� por que env�o userException pero no el ErrorCode " +
                    "correcto");
            }

        }catch(Exception ex){
            fail("se present� excepci�n "+ex);
        }

    }

    @Test
    public void caseInstitutionTypeFormacionParaTrabajoConFechaCaduca() {
        try {
            /**
             * La segunda prueba la hacemos enviando una instituci&oacute;n
             * tipo cformaci&oacute;n para el trabajo con certificado
             * de calidad pero con la fecha de vencimiento del certificado
             * del d&iacute;a de hoy, es decir, vencida.
             * El m&eacute;todo debe enviar un userException
             */
            Institution institution = new Institution();
            institution.setContactEmail(CORREO_FOR_TEST);
            institution.setContactPhone(PHONE_FOR_TEST);
            institution.setCreationDate(new Date());
            institution.setCreationUserId(new User(ID_USER_QUEMADO));
            institution.setDv(VERIFICATION_DIGIT_TEST);
            institution.setId(Long.MIN_VALUE);
            institution.setInstitutionName(CORREO_FOR_TEST);
            institution.setLegalNature(LegalNature.N);
            institution.setNit(NIT_INSTITUTION_TEST.toString());
            institution.setOrigin(Origin.R);
            institution.setApprovalStatus(ApprovalInstitutionStatus.N);
            InstitutionType institutionTypeFormacionTrabajo =
                new InstitutionType();
            institutionTypeFormacionTrabajo.setId(
                Util.INSTITUTION_TYPE_FORMACION_TRABAJO);
            institution.setType(institutionTypeFormacionTrabajo);
            institution.setQualityCertificate(new byte[5 / 2]);
            institution.setQualityCertificateExpiration(
                new Date());
            institutionService.save(institution);
            /**
             * Debe fallar pues le hemos mandado una instituci&oacute;n tipo
             * Formaci�n para el trabajo sin certificado de calidad
             * ni fecha de vencimiento
             */
            fail("Fall� por que cuando le envie instituci�n tipo formaci�n " +
                "para el trabajo sin certificado de calidad ni fecha de " +
                "vencimiento no dispar� user Exception");
        } catch (SystemException uEx) {
            if (uEx.getErrorCode().equals(
                ErrorCode.ERR_004_INSTITUTION_VALIDATE_CERTIFICATE_EXPIRATION)) {
                LOGGER.info(
                    "Se ha enviado user Exception cuando lo deber�a hacer, " +
                    "prueba superada.");
            } else {
                fail("Fall� por que env�o userException pero no el ErrorCode " +
                    "correcto");
            }

        }catch(Exception ex){
            fail("se present� excepci�n "+ex);
        }

    }

}
