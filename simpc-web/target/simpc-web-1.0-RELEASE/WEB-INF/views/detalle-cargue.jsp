<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <style>
            input[type=number]::-webkit-outer-spin-button,
            input[type=number]::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            input[type=number] {
                -moz-appearance:textfield;
            }
        </style>
    </head>
    <body>
        <c:if test="${not empty msg}">
            <br/>
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        <div id="fileMessage" class="alert alert-danger alert-dismissible" role="alert" hidden>
            <button type="button" class="close" onclick="$('#fileMessage').hide()"
                    aria-label="Close">
                <span aria-hidden="true">�</span>
            </button>
            <strong>Archivo no encontrado.</strong>
        </div>
        <br/>
        <div class="row">
            <div class="col-sm-5">
                <label for="id" class=" control-label">Id de la Carga</label>
                <p class="form-control-static" id="fechaCargues">${loadProcess.id}</p>
            </div>
            <div class="col-sm-5">
                <label for="nombreArchivos" class="control-label">Nombre del Archivo</label>
                <p class="form-control-static" id="nombreArchivos">${loadProcess.originalFileName}</p>
            </div>
            <div class="col-sm-5">
                <label for="fechaCargues" class=" control-label">Fecha Inicio</label>
                <p class="form-control-static" id="fechaCargues">${loadProcess.initDate}</p>
            </div>
            <c:if test="${not empty loadProcess.endDate}">
            <div class="col-sm-5">
                <label for="fechaFinCargues" class="control-label">Fecha Fin</label>
                <p class="form-control-static" id="fechaFinCargues">${loadProcess.endDate}</p>
            </div>
            </c:if>
            <c:if test="${empty loadProcess.endDate}">
            <div class="col-sm-5">
                <label for="fechaCargues" class=" control-label">Fecha Fin</label>
                <p class="form-control-static" id="fechaCargues">${loadProcess.initDate}</p>
            </div>
            </c:if>    
            <div class="col-sm-5">
                <label for="peso" class=" control-label">Peso del Archivo </label>
                <p class="form-control-static" id="peso">${loadProcess.size} kilobytes</p>
            </div>
            <div class="col-sm-5">
                <label for="duracion" class="control-label">Duraci&oacute;n</label>
                <p class="form-control-static" id="duracion">${loadProcess.time} milisegundos</p>
            </div>

            <div class="col-sm-5">
                <label for="cantidadCumplen" class=" control-label"> Total de Registros que Cumplen</label>
                <p class="form-control-static" id="cantidadCumplen">${loadProcess.successRows}</p>
            </div>
            <div class="col-sm-5">
                <label for="cantidadNO" class="control-label">Total de Registros que NO Cumplen</label>
                <p class="form-control-static" id="cantidadNO">${loadProcess.failRows}</p>
            </div>
            <div class="col-sm-5">
                <label for="tipoInsitucion" class=" control-label">Total de Registros Procesados</label>
                <p class="form-control-static">${loadProcess.totalRows}</p>
            </div>

        </div>    
        <div class="row">
            <div class="col-sm-12">
                <label for="descripcion" class=" control-label">Observaci&oacute;n</label><br/>
                <textarea style="border:0px;width: 100%;height:100px;" class="form-control-dinamic" id="descripcion">${loadProcess.detail}</textarea>
            </div>
            <div class="col-sm-12" style="text-align:right">
                <c:if test="${loadProcess.status == 'ERRORES_ESTRUCTURA'}">
                    <form id="downloadStrcForm${loadProcess.id}" name="downloadStrcForm${loadProcess.id}" action="descargar-errores.htm" >
                        <input type="hidden" name="type" id="type" value="estructura">
                        <input type="hidden" name="loadId" id="loadId" value="${loadProcess.id}">
                        <button type="button" class="btn btn-warning btnDownloadStrc" 
                                id="btnDownloadStrc" name="btnDownloadStrc" itemID="${loadProcess.id}"
                                onclick="downloadFile('downloadStrcForm' + ${loadProcess.id})">
                            <i class="fa fa-download" aria-hidden="true"></i>
                            Descargar errores
                        </button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </form>   
                </c:if>    
                <c:if test="${loadProcess.status=='ERRORES_NEGOCIO'}">
                    <form id="downloadBussForm${loadProcess.id}" name="downloadBussForm${loadProcess.id}" action="descargar-errores.htm" >
                        <input type="hidden" name="type" id="type" value="negocio">
                        <input type="hidden" name="loadId" id="loadId" value="${loadProcess.id}">
                        <button type="button" class="btn btn-warning btnDownloadBuss" 
                                id="btnDownloadBuss" name="btnDownloadBuss" itemId="${loadProcess.id}"
                                onclick="downloadFile('downloadBussForm' + ${loadProcess.id})">
                            <i class="fa fa-download" aria-hidden="true"></i>
                            Descargar Errores
                        </button> 
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </form>
                </c:if>
                <c:if test="${loadProcess.status!='ERRORES_NEGOCIO' && loadProcess.status != 'ERRORES_ESTRUCTURA'}">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </c:if>
            </div>
        </div>
                
        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/jquery.ui.widget.js"></script>
        <script src="resources/js/jquery.iframe-transport.js"></script>
        <script src="resources/js/jquery.fileupload.js"></script>
        <script src="resources/js/jquery.validate.min.js"></script>
        <script src="resources/js/tooltipster.bundle.js"></script>
        <script src="resources/js/custom-functionalities.js"></script>
    </body>
</html>    

