/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: VerifyRecaptcha.java
 * Created on: 2016/12/06, 05:40:29 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.commons.captcha;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.net.ssl.HttpsURLConnection;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Utilidad para verificaci&oacute;n de captcha.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Service
public final class VerifyRecaptcha {

    /**
     * Agente para la petic&oacute;n.
     */
    private final static String USER_AGENT = "Mozilla/5.0";
    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(VerifyRecaptcha.class);
    /**
     * Sitio de verificaci&oacute;n del captcha.
     */
    @Value("${captcha.site.verify}")
    private String siteVerify;
    /**
     * Secret para verificaci&oacute;n del captcha.
     */
    @Value("${captcha.site.secret}")
    private String secret;
    /**
     * Key para validaci&oacute;n del captcha.
     */
    @Value("${captcha.site.key}")
    private String key;

    /**
     * Verifica si el captcha fue respondido correctamente.
     *
     * @param gRecaptchaResponse
     *
     * @return
     *
     * @throws IOException
     */
    public boolean verify(String gRecaptchaResponse) throws IOException {
        if (gRecaptchaResponse == null || "".equals(gRecaptchaResponse)) {
            return false;
        }

        try {
            String params = "?secret=" + secret + "&response=" +
                gRecaptchaResponse;
            URL obj = new URL(siteVerify + params);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

            // add reuqest header
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            con.getResponseCode();

            StringBuilder response;
            try (BufferedReader in = new BufferedReader(new InputStreamReader(
                con.getInputStream()))) {
                String inputLine;
                response = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
            }

            JsonObject jsonObject;
            try ( //parse JSON response and return 'success' value
                JsonReader jsonReader = Json.
                    createReader(new StringReader(response.toString()))) {
                jsonObject = jsonReader.readObject();
            }

            LOGGER.info("Response: " + response.toString());

            return jsonObject.getBoolean("success");
        } catch (Exception e) {
            LOGGER.error("Error", e);

            return false;
        }
    }

    public String getKey() {
        return key;
    }
}
