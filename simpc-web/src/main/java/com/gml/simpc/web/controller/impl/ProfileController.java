
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ProfileController.java
 * Created on: 2016/10/19, 02:14:58 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.gml.simpc.api.entity.Profile;
import com.gml.simpc.api.entity.Resource;
import com.gml.simpc.api.entity.exeption.UserException;
import com.gml.simpc.api.enums.ProfileStatus;
import com.gml.simpc.api.services.FunctionalityService;
import com.gml.simpc.api.services.ProfileService;
import com.gml.simpc.api.services.ResourcesService;
import com.gml.simpc.web.controller.ControllerDefinition;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_001_UNKNOW;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_020_PROFILE_USED;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_022_PROFILE_USED;
import static com.gml.simpc.api.utilities.Util.FAILED_ALERT;
import static com.gml.simpc.api.utilities.Util.SUCCESS_ALERT;

/**
 * Controlador para la ventana de manejo de consulta perfiles.
 *
 * @author <a href="mailto:jonathanp@gmlsoftware.com">Jonathan Ponton</a>
 */
@Controller
public class ProfileController implements ControllerDefinition {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(ProfileController.class);
    /**
     * Separador de ids que vienen desde la vista.
     */
    private static final String SEPARATOR = ",";
    /**
     * Lista de instituciones encontradas.
     */
    private List<Profile> profileList;

    /**
     * Servicio para adminisraci&oacute;n de actualizaci&oacute;n de perfiles;
     * Servicio de funcionalidades intermediario para la comunicaci&oacute;n con la
     * persistencia.
     */
    @Autowired
    private FunctionalityService functionalityService;

    /**
     * Servicio para adminisraci&oacute;n de consulta perfiles; Servicio de
     * perfiles intermediario para la comunicaci&oacute;n con la persistencia.
     */
    @Autowired
    private ProfileService profileService;

    /**
     * Servicio para adminisraci&oacute;n de consulta perfiles; Servicio de
     * perfiles intermediario para la comunicaci&oacute;n con la
     * persistencia.
     */
    @Autowired
    private ResourcesService resourceService;

    /**
     * M&eacute;todo para obtener un perfil especifico.
     * 
     * @param request
     * @param response
     * @return
     * @throws UserException 
     */
    @RequestMapping(value = "obtener-perfil",
        method = RequestMethod.GET)
    public @ResponseBody
    String onEditProfile(HttpServletRequest request,
        HttpServletResponse response) throws UserException {
        LOGGER.info("Ejecutando metodo [ onEditProfile ]");

        try {
            Profile profile = this.profileService.
                findById(Long.parseLong(request.getParameter("id")));
            ObjectMapper mapper = new ObjectMapper();

            return mapper.writeValueAsString(profile);
        } catch (Exception ex) {
            LOGGER.error("Error", ex);

            throw new UserException(ERR_001_UNKNOW);
        }
    }

    /**
     * M&eacute;todo para iniciar la consulta de perfiles.
     * 
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = "consulta-perfiles",
        method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response) {
        LOGGER.info("Ejecutando metodo [ onDisplay consulta-perfiles ]");
        ModelAndView modelAndView =
            new ModelAndView("consulta-perfiles");
        boolean sw = false;
        try {

            modelAndView.addObject("profilesList", this.profileService.
                getAll());

            if (request.getParameter("id") != null) {
                Profile profile = profileService.
                    findById(Long.parseLong(request.getParameter("id")));
                modelAndView.addObject("creationProfile", profile);

                sw = true;
            }

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("Finaliza la invocacion de [ onDisplay ].");
        }

        init(modelAndView, sw);

        return modelAndView;
    }

    /**
     * M&eacute;todo que consulta perfiles de acuerdo a lo
     * diligenciado en los filtros de formulario de consulta.
     * 
     * @param request
     * @param response
     * @param searchProfile
     * @return 
     */
    @RequestMapping(value = "buscarPerfiles",
        method = RequestMethod.GET)
    public ModelAndView search(HttpServletRequest request,
        HttpServletResponse response,
        @ModelAttribute("searchProfile") Profile searchProfile) {
        LOGGER.info("entra a consultar en el controller de perfiles");

        ModelAndView modelAndView =
            new ModelAndView("consulta-perfiles");

        String functionalityId = request.getParameter("functionalityId");

        try {

            String code = String.valueOf(
                searchProfile.getStatus() == null ? "" :
                searchProfile.getStatus().name());

            LOGGER.info("NOMBRE: " + searchProfile.getName());
            LOGGER.info("ESTADO: " + code);
            LOGGER.info("FUNCIONALIDAD: " + functionalityId);

            if ("".equals(searchProfile.getName()) && "".equals(code) &&
                "".endsWith(functionalityId)) {
                modelAndView.addObject("profilesList", this.profileService.
                    getAll());
            } else {
                List<Profile> profilesList = this.profileService.
                    findByFilters(searchProfile.getName(),
                        code, functionalityId);
                modelAndView.addObject("profilesList", profilesList);
            }

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msgType", FAILED_ALERT);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("finally de consultar en InstitutionsController");
        }

        init(modelAndView, false);

        return modelAndView;
    }

    /**
     * M&eacute;todo que guarda un nuevo perfil.
     * 
     * @param request
     * @param response
     * @param creationProfile
     * @return
     * @throws IOException 
     */
    @RequestMapping(value = "guardar-perfil",
        method = RequestMethod.POST)
    public ModelAndView save(HttpServletRequest request,
        HttpServletResponse response, @Valid @ModelAttribute(
            "creationProfile"
        ) Profile creationProfile) throws IOException {

        ModelAndView modelAndView =
            new ModelAndView("consulta-perfiles");
        boolean creationError = false;
        
        LOGGER.info("VALOR DE EL NOMBRE DEL PERFIL " + creationProfile.getName());

        try {
            this.profileService.save(creationProfile);
            modelAndView.addObject("creationProfile", creationProfile);
            modelAndView.addObject("msg", "Se ha guardado el perfil " +
                creationProfile.getName() + " exitosamente.");
            modelAndView.addObject("msgType", SUCCESS_ALERT);
            modelAndView.addObject("profilesList", this.profileService.
                getAll());
        } catch (DataIntegrityViolationException ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("internalMsg", ERR_022_PROFILE_USED.
                getMessage());
            ObjectMapper om = new ObjectMapper();
            modelAndView.addObject("jsonProfile",
                om.writeValueAsString(creationProfile));
            modelAndView.addObject("msgType", FAILED_ALERT);
            modelAndView.addObject("profilesList", this.profileService.
                getAll());
        } catch (ConstraintViolationException ex) {
            LOGGER.error("Error ", ex);
            StringBuilder builder = new StringBuilder("Alertas: ");
            for (ConstraintViolation error : ex.getConstraintViolations()) {
                builder.append(error.getMessage());
                builder.append("\n");
            }
            modelAndView.addObject("internalMsg", builder.toString());
            modelAndView.addObject("creationProfile", creationProfile);
            modelAndView.addObject("msgType", FAILED_ALERT);
            creationError = !creationError;
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
            modelAndView.addObject("msgType", FAILED_ALERT);

        } finally {
            LOGGER.info("finally save in ProfileController");
        }

        init(modelAndView, creationError);

        return modelAndView;
    }

    /**
     * M&eacute;todo que editar un perfil.
     * 
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = "edicion-perfil",
        method = RequestMethod.GET)
    public ModelAndView editProfile(HttpServletRequest request,
        HttpServletResponse response) {

        ModelAndView modelAndView =
            new ModelAndView("edicion-perfil");
        boolean creationError = false;

        Long id = Long.parseLong(request.getParameter("id"));
        Profile parent = null;

        try {
            parent = this.profileService.findById(id);
            creationError = true;
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        }

        modelAndView.addObject("creationProfile", parent);
        modelAndView.addObject("editionProfile", parent);

        init(modelAndView, creationError);

        return modelAndView;
    }

    /**
     * M&eacute;todo que se ejecuta al abrir la venttana de index; Trae el
     * detalle necesario para mostrar en la ventana.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping("detalle-consulta-perfiles")
    public ModelAndView detailProfile(HttpServletRequest request,
        HttpServletResponse response) {
        Long id = Long.parseLong(request.getParameter("id"));
        Profile parent;
        ModelAndView modelAndView =
            new ModelAndView("detalle-consulta-perfiles");
        try {
            LOGGER.info("Se busca la instucion con este ID" + id);
            parent = this.profileService.findById(id);

            modelAndView.addObject("parentProfile", parent);
            modelAndView.addObject("profile", new Profile());
            modelAndView.addObject("functionalityProfileList",
                this.functionalityService.
                    getFunctionalitiesByProfile(id));
            modelAndView.addObject("functionalitySystemList",
                this.functionalityService.
                    getFunctionalitiesByNotProfile(id));
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("finally de display Aprobacion");
        }
        init(modelAndView, false);
        return modelAndView;
    }

    /**
     * M&eacute;todo que borra un perfil.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "borrarPerfil",
        method = RequestMethod.POST)
    public ModelAndView delete(HttpServletRequest request,
        HttpServletResponse response) {
        LOGGER.info("entra a borrar en el controller de perfil");
        ModelAndView modelAndView =
            new ModelAndView("consulta-perfiles");
        try {
            Long profileId = Long.parseLong(request.getParameter("id"));
            this.resourceService.deleteByProfileId(profileId);
            this.profileService.
                remove(this.profileService.findById(profileId));
            modelAndView.addObject("profilesList", this.profileService.
                getAll());
            modelAndView.addObject("msg",
                "Se ha eliminado el perfil exitosamente.");
            modelAndView.addObject("msgType", SUCCESS_ALERT);
            modelAndView.addObject("profileList", profileList);
        } catch (DataIntegrityViolationException ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msgType", FAILED_ALERT);
            modelAndView.addObject("msg", ERR_020_PROFILE_USED.getMessage());
            modelAndView.addObject("profilesList", this.profileService.
                getAll());
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msgType", FAILED_ALERT);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("finally de eliminar perfil");
        }
        init(modelAndView, false);
        return modelAndView;
    }

    /**
     * M&eacute;todo que actualiza un perfil.
     * 
     * @param request
     * @param response
     * @param profile
     * @return 
     */
    @RequestMapping(value = "actualizar-recursos",
        method = RequestMethod.POST)
    public ModelAndView updateResource(HttpServletRequest request,
        HttpServletResponse response, @ModelAttribute(
            "profile"
        ) Profile profile) {

        ModelAndView modelAndView =
            new ModelAndView("consulta-perfiles");
        boolean creationError = false;

        try {
            String ids = request.getParameter("idValues");
            LOGGER.info("IDS: " + ids);

            String[] stringsIdList = ids.split(SEPARATOR);
            List<Resource> resources = new LinkedList<>();

            Long parentId = Long.parseLong(request.getParameter("parent"));

            LOGGER.info("Perfil ID: " + parentId);
            LOGGER.info("Recursos: " + stringsIdList.length);

            for (String stringsId : stringsIdList) {
                if (!stringsId.trim().isEmpty()) {
                    Resource resource = new Resource();
                    resource.setIdFunctionality(Long.valueOf(stringsId));
                    resource.setIdProfile(parentId);
                    resources.add(resource);
                }
            }

            LOGGER.info("Recursos: " + resources.size());

            this.profileService.updateProfileFunctionalities(parentId,
                resources);
            modelAndView.addObject("parentProfile", profile);
            modelAndView.addObject("functionalityProfileList",
                this.functionalityService.
                    getFunctionalitiesByProfile(parentId));
            modelAndView.addObject("functionalitySystemList",
                this.functionalityService.
                    getFunctionalitiesByNotProfile(parentId));
            modelAndView.addObject("msg",
                "Se ha actualizado el perfil exitosamente.");
            modelAndView.addObject("msgType", SUCCESS_ALERT);
            modelAndView.addObject("profilesList", this.profileService.
                getAll());
        } catch (ConstraintViolationException ex) {
            LOGGER.error("Error ", ex);

            StringBuilder builder = new StringBuilder("Alertas: ");
            for (ConstraintViolation error : ex.getConstraintViolations()) {
                builder.append(error.getMessage());
                builder.append("\n");
            }
            modelAndView.addObject("internalMsg", builder.toString());
            modelAndView.addObject("msgType", FAILED_ALERT);
            creationError = !creationError;
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
            modelAndView.addObject("msgType", FAILED_ALERT);
        } finally {
            LOGGER.info("finally updateResource in ProfileController");
        }

        init(modelAndView, creationError);

        return modelAndView;
    }

    private void init(ModelAndView modelAndView, boolean creationError) {
        LOGGER.info("Ejecutando metodo [ init ]");

        modelAndView.addObject("profileStatusList", ProfileStatus.values());
        modelAndView.addObject("profileStatusList", ProfileStatus.values());
        modelAndView.addObject("searchProfile", new Profile());

        if (!creationError) {
            modelAndView.addObject("creationProfile", new Profile());
        }
    }
}
