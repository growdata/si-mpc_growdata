/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ErrorCode.java
 * Created on: 2016/05/31, 09:34:34 AM
 * Project: Twitter Master
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity.exeption.code;

/**
 * Enumeraci&oacute;n con los c&oacute;digos de error que utiliza la
 * aplicaci&oacute;n.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public enum ErrorCode {

    ERR_001_UNKNOW("Error 001: Ha ocurrido un error desconocido. Por favor " +
        "comun�quese con el administrador del sistema."),
    ERR_002_USER_EXIST("Error 002: Usuario ya se encuentra registrado."),
    ERR_003_INSTITUTION_VALIDATE_QUALITYCERTIFICATE(
        "Error 003: Debe adjuntar el certificado de calidad de la instituci�n"),
    ERR_004_INSTITUTION_VALIDATE_CERTIFICATE_EXPIRATION("Error 004: La " +
        "instituci�n tiene fecha de certificaci�n vencida"),
    ERR_005_INVALID_REMOVE_PRINCIPAL_SEDE("Error 005: La " +
        "sede principal no se puede eliminar"),
    ERR_006_USER_DONT_EXIST("Error 006: El usuario no existe."),
    ERR_007_NOTIFICATION_FAIL("Error 007: Error al enviar la notificaci�n."),
    ERR_015_TEMPLATE_FAIL("Error 015: Error al " +
        "aplicar la plantilla de correo."),
    ERR_016_PASSWORD_DIFERENT_TO_CONFIRMATION(
        "Error 016: Las contrase�as no coinciden."),
    ERR_017_CREDENTIALS_FAILED("Error 017: Las credenciales no coinciden"),
    ERR_018_PASSWORD_MUST_BE_DIFERENT(
        "Error 018: La nueva contrase�a debe ser distinta a la anterior"),
    ERR_019_NOTIFICATION_FAILED("Error 019: Error enviando e-mail"),
    ERR_020_PROFILE_USED("Error 020: El perfil se encuentra asignado a uno " +
        "o varios usuarios, por esta raz�n no es posible eliminarlo."),
    ERR_021_PROFILE_USED("Error 021: El %s ya se encuentra " +
        "registrado, por favor int�ntelo con uno distinto."),
    ERR_022_PROFILE_USED("Error 021: El nombre del perfil ya se encuentra " +
        "registrado, por favor int�ntelo con uno distinto."),
    ERR_023_PROFILE_USED("Error 023: El usuario con ese correo ya se encuentra registrado."),
    ERR_025_PROFILE_USED("Error 020: El perfil se encuentra asignado a uno " +
        "o varios usuarios, por esta raz�n no es posible eliminarlo."),
    ERR_026_MISSING_DATA("Error 021: Informaci�n insuficiente para consultar " +
        "indicador."),
    ERR_027_DATE_OUT_OF_RANGE("Error 022: Rango de fechas excede el rango " +
        "permitido de %s periodos."),
    ERR_028_CONFIGURATION_ERROR("Error 023: Error de configuraci�n del " +
        "indicador, comuniquese con el administrador del sistema."),
    ERR_030_VALIDATION_ERROR("Error 030: El d�gito de verificaci�n " +
        "es incorrecto, por favor valide que este pertenezca al numero  " +
        "de identificaci�n que ha ingresado."),
    ERR_031_INSTITUTION_FAILED("Error 031: El %s ya se encuentra " +
        "registrado, por favor int�ntelo con uno distinto."),
    ERR_032_ACTIVE_DIRECTORY_USED("Error 032: El nombre de usuario %s ya" +
        " se encuentra registrado  en el directorio activo, por favor " +
        "int�ntelo con uno distinto."),
    ERR_033_LDAP_AUTHENTICATION_FAILED("Error 033: No se  ha logrado " +
        "aut�nticar en el directorio activo, int�ntelo de nuevo."),
    ERR_034_ERROR_PROGRAM_UPDATE_LOAD_PROCESS_NOT_FOUND("Error 034: Se est� " +
        "intentando actualizar un programa cuyo registro de cargue no aparece " +
        "en la tabla WF_CARGA_PROCESO"),
     ERR_035_ERROR_PROGRAM_UPDATE_FUNDACION_NOT_FOUND("Error 035: Se est� " +
        "intentando actualizar un programa cuyo c�digo ccf no apaece en la " +
         "tabla FUNDACIONES"),
     ERR_036_ERROR_PROGRAM_UPDATE_HEADQUARTER_NOT_FOUND("Error 036: Se est� " +
        "intentando actualizar un programa cuyo c�digo de sede no apaece en " +
         "la tabla SEDES"),
     ERR_037_ERROR_PROGRAM_UPDATE_PROGRAM_TYPE_NOT_FOUND("Error 037: Se est� " +
        "intentando actualizar un programa cuyo tipo de programa no apaece " +
         "en la tabla TIPOS_PROGRAMA"),
     ERR_038_ERROR_PROGRAM_UPDATE_CINE_CODE_NOT_FOUND("Error 038: Se est� " +
        "intentando actualizar un programa cuyo c�digo cine no apaece " +
         "en la tabla CINE"),
     ERR_039_ERROR_FINISHDATE_BEFORE_INITDATE("Error 039: La fecha de " +
         "finalizaci�n est� antes o el mismo d�a que la fecha de inicio"),
     ERR_040_ERROR_DATED_DATES("Error 040: Las fechas est�n vencidas, deben " +
         "ser posteriores  al d�a de hoy"),
     ERR_041_ERROR_PROGRAM_DURATION("Error 041: El progrma no tiene el " +
         "m�nimo n�mero de horas para el tipo de programa seleccionado"),
     ERR_042_ERROR_INVALID_PASSWORD("Error 042: El password debe tener m�nimo" +
         " 8 car�cteres y al menos una letra may�scula y un n�mero."),
     ERR_043_ERROR_INVALID_FORMAT_FILE("Error 043: La certificaci�n no tiene" +
         "  formato v�lido. El formato admitido es PDF"),
	 ERR_044_SESSION_ERROR("Error 042: Sesi�n no existe o ha expirado."),
     ERR_044_ERROR_PROGRAM_UPDATE_INSTITUTION_NOT_FOUND("Error 0044: Se est� " +
        "intentando actualizar un programa cuyo c�digo de INSTITUCION no apaece en " +
         "la tabla INSTITUCION"),
     ERR_045_ERROR_PROGRAM_NAME_EXIST("Error 045: " +
        "El Nombre del programa que se esta intenta crear ya existe " ),
     ERR_046_ERROR_PROGRAM_NAME_EXIST("Error 046: " +
        "El nombre de usuario ya se encuentra registrado en el sistema." ),
     ERR_047_HEADER_FOUND("Error 047: Cabecera de archivo ya encontrada." ),
     ERR_048_READ_HEADER("Error 048: Error al leer la cabecera." ),
     ERR_049_HEADER_LINE_MISMATCH("Error 049: N�mero de l�neas en la " +
         "cabecera no coincide con las l�neas en el archivo." ),
     ERR_050_HEADER_FILE_TYPE_EMPTY("Error 050: Tipo de archivo en la " +
         "cabecera no puede estar vac�o." ),
     ERR_051_HEADER_DATE_NOT_TODAY("Error 051: La fecha en la cabecera no " +
         "es de hoy." ),
     ERR_052_HEADER_DATE_FORMAT_INVALID("Error 052: Formato de fecha en " +
         "cabecera inv�lido." ),
     ERR_053_HEADER_USER_INVALID("Error 053: Usuario en cabecera no " +
         "corresponde con usuario en sesi�n." ),
     ERR_054_HEADER_TOKENS("Error 054: N�mero de tokens en la cabecera no " +
         "corresponde a los esperados." ),
     ERR_055_HEADER_EMPTY("Error 055: Cabecera no puede estar vac�a." ),
     ERR_056_ERROR_ACTIVATE_MINTRABUSER("Error 056: El nombre de usuario no " +
        "existe o no se encuentra registrado en el directorio activo, " +
        "int�ntelo de nuevo."),
     ERR_057_FILE_MISSAHAPEN("Error 057: El archivo que intenta cargar " +
         "no tiene una estructura correcta para realizar cruces masivos.");
     

    /**
     * Mensaje de error.
     */
    private final String message;

    private ErrorCode(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
