/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: IRepository.java
 * Created on: 2016/10/24, 09:59:11 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository.valuelist;

import com.gml.simpc.api.entity.valuelist.Foundation;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author <a href="mailto:julianm@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
public interface FoundationRepository extends JpaRepository<Foundation, Long> {

    /**
     * Consulta por c&oacute;digo.
     *
     * @param code
     * @return
     */
    Foundation findByCode(String code);

    /**
     * Consulta por nombre.
     *
     * @param name
     * @return
     */
    Foundation findByName(String name);
    
    /**
     * Consulta por nombre corto.
     *
     * @param name
     * @return
     */
    Foundation findByShortName(String shortName);
}
