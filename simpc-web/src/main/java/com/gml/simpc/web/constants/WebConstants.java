/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: WebConstants.java
 * Created on: 2016/10/26, 10:08:56 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.constants;

/**
 * Constantes para la web.
 *
 * @author <a href="mailto:julianm@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
public class WebConstants {

    /**
     * P&aacute;gina para la activaci&oacute;n de usuarios.
     */
    public static final String ACTIVATION_PATH = "activar-usuario.htm";
    /**
     * Nombre de archivo de cruce de pila
     */
    public static final String INTERSECTION_PILA_FILE_NAME =
        "cruce-pila-consulta";

    /**
     * Nombre de archivo de cruce de pila
     */
    public static final String INTERSECTION_DEPENDENTS_FILE_NAME =
        "cruce-dependientes-consulta";

    /**
     * Nombre de archivo de cruce de pila
     */
    public static final String INTERSECTION_TRAININGS_FILE_NAME =
        "cruce-orientación y capacitación-consulta";

    /**
     * Nombre de archivo de cruce de pila
     */
    public static final String INTERSECTION_FOSFEC_FILE_NAME =
        "cruce-recursos-fosfec-consulta";

    /**
     * Nombre de archivo de cruce de pila
     */
    public static final String INTERSECTION_INTERMEDIATION_FILE_NAME =
        "cruce-intermediacion-consulta";

    /**
     * Nombre de archivo de cruce de hojas de vida
     */
    public static final String INTERSECTION_CURRICULUM_VITAE_FILE_NAME =
        "cruce-hojas-de-vida-consulta";

    /**
     * Nombre de extensi&oacute;n para archivos xls
     */
    public static final String XLS_EXTENSION_FILE = ".xls";

    /**
     * Nombre de extensi&oacute;n para archivos txt
     */
    public static final String TXT_EXTENSION_FILE = ".txt";

    /**
     * Nombre de extensi&oacute;n para archivos txt
     */
    public static final String CSV_EXTENSION_FILE = ".csv";

    /**
     * separador de coma
     */
    public static final String COMA_SEPARATOR = ",";
    
    /**
     * Separador de registros
     */
    public static final String RECORD_SEPARATOR = "~&~"; 
    
    /**
     * Separador de exportación
     */
    public static final String EXPORT_SEPARATOR = "|$|"; 

    /**
     * igual
     */
    public static final String EQUAL_STRING = "=";
    /**
     * Nit del servicio publico de empleo
     */
    public static final String NIT_SERVICIO_PUBLICO_EMPLEO = "900678508-4";

}
