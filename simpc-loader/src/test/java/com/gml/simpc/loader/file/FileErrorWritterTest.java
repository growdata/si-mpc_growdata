/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: FileErrorWritterTest.java
 * Created on: 2017/02/08, 09:58:17 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.file;

import com.gml.simpc.api.entity.LoadProcess;
import com.gml.simpc.loader.dto.ValidationErrorDto;
import java.util.ArrayList;
import java.util.List;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Pruebas para escritura de archivos de errores
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica</a>
 */
@Ignore
public class FileErrorWritterTest {
    
    public FileErrorWritterTest() {
    }
    
    /**
     * Logeer para la clase.
     */
    private static final org.apache.log4j.Logger LOGGER
            = org.apache.log4j.Logger.getLogger(FileErrorWritterTest.class);

    /**
     * Test of writeFile method, of class FileErrorWritter.
     */
    @Test
    public void testWriteFile() {
        LoadProcess loadProcess = new LoadProcess();
        FileErrorWritter fileErrWr = new FileErrorWritter();
        List<ValidationErrorDto> errorsList = new ArrayList<>();
        errorsList.add(new ValidationErrorDto(1,"Institucion invalida"));
        errorsList.add(new ValidationErrorDto(3,"Sede invalida"));
        loadProcess.setNewFilePath("C:\\SIMPC\\UPLOADS\\2017-01-24\\CAP-1\\");
        loadProcess.setNewFileName("175044690.txt");
        loadProcess.setBussinessErrorFileName();
        loadProcess.setStructureErrorFileName();
        fileErrWr.writeErrorFile(loadProcess,errorsList,"structure");
        fileErrWr.writeErrorFile(loadProcess,errorsList,"bussiness");
    }
    
}
