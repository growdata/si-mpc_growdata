/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: ConsolidantPostulantDto.java
 * Created on: 2017/01/24, 08:18:39 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public class ManagementAndPlacementDto {
    
    /*
     * Estado postulante
     */
     String postulationStatus;
     
     /**
      * Cantidad de mujeres
      */
     int women;
     
     /*
      * Cantidad de hombres
      */
     int men;

    public ManagementAndPlacementDto() {
    }

    public String getPostulationStatus() {
        return postulationStatus;
    }

    public void setPostulationStatus(String postulationStatus) {
        this.postulationStatus = postulationStatus;
    }

    public int getWomen() {
        return women;
    }

    public void setWomen(int women) {
        this.women = women;
    }

    public int getMen() {
        return men;
    }

    public void setMen(int men) {
        this.men = men;
    }

    public ManagementAndPlacementDto(String postulationStatus, int women,
        int men) {
        this.postulationStatus = postulationStatus;
        this.women = women;
        this.men = men;
    }

    @Override
    public String toString() {
        return "ManagementAndPlacementDto{" + "postulationStatus=" +
            postulationStatus + ", women=" + women + ", men=" + men + '}';
    }
    
     
     
}
