/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: TypeQualityCertificateRepository.java
 * Created on: 2017/07/07, 01:18:41 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository.valuelist;

import com.gml.simpc.api.entity.valuelist.TypeQualityCertificate;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 *
 * @author <a href="mailto:jonathanP@gmlsoftware.com">Jonathan Pont�n</a>
 */
public interface TypeQualityCertificateRepository extends JpaRepository<TypeQualityCertificate, String> {

}
