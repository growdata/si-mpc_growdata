/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserRepository.java
 * Created on: 2016/10/19, 02:03:51 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository;

import com.gml.simpc.api.entity.Program;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Repositorio para programas.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public interface ProgramRepository extends JpaRepository<Program, Long> {

    

    /**
     * Filtra los programas por nombre, c&oacute;digo e instituci&oacute;n.
     *
     * @param name          Nombre del programa.
     * @param code          C&oacute;digo del programa.
     * @param institutionId ID de la instituci&oacute;n donde se dicta el
     *                      programa.
     *
     * @return
     */
   @Query(value = "SELECT p.* " +
        "FROM programas p where CONVERT(LOWER (p.NOMBRE),'US7ASCII') like CONVERT(LOWER (?1),'US7ASCII')" +
        " AND ( p.CODIGO like ?2) " +
        "AND (0=?3 OR  p.TIPO_CAPACITACION=?3) " ,    
        nativeQuery = true)
    List<Program> findByFilters(String name, String code, String type);
   
    /**
     * M&eacute;todo para filtrar por c&oacute;digo
     *
     * @param code
     *
     * @return
     */
    Program findByCode(String code);
}
