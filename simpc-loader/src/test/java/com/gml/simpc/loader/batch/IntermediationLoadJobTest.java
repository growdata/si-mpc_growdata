package com.gml.simpc.loader.batch;

import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-batch.xml", 
    "classpath:batch_jobs/curriculumVitae/intermediation/intermediationJob.xml" })
public class IntermediationLoadJobTest {
    
    public static final ThreadLocal<String> THREAD  = new ThreadLocal<>();
    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER = 
        Logger.getLogger(IntermediationLoadJobTest.class);
    
    
    /**
     * Servicio para ejecutar jobs en modo pruebas
     */
    @Autowired
    JobLauncherTestUtils jobLauncherTestUtils;

    
    /**
     * Test �nico para probar carga de hojas de vida - intermediacion
     */
    @Test
    public void shouldRunJob() throws Exception {

       
         LOGGER.info("Running WorkExperienceLoadJobTest - onlyCase");

        //Inicializamos local thread
        ThreadLocalJob.init();

        // Inicializamos el job.
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath",
            "file:loadTestFiles/curriculum vitae/intermediation/" +
            "intermediationTestFile.txt");

        // Lanzamos el job.
        JobExecution jobExecution = jobLauncherTestUtils.
            launchJob(jobParametersBuilder.toJobParameters());

        for (Throwable t : jobExecution.getAllFailureExceptions()) {
            t.printStackTrace();
        }

        JobResultDto jobResult = ThreadLocalJob.getResult();
        LOGGER.info(jobResult.toString());

        if (!jobResult.getErrorList().isEmpty()) {
            LOGGER.info(jobResult.getErrorsString());
          
        }

        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().
            getExitCode());
        
        Assert.assertTrue(jobResult.getErrorList().isEmpty());
        ThreadLocalJob.delete();
    }
}
