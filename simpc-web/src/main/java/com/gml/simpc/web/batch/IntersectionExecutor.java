/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IntersectionExecutor.java
 * Created on: 2017/02/08, 09:58:17 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.batch;

import com.gml.simpc.api.dao.MassiveIntersectionDao;
import com.gml.simpc.api.dto.BasicInformationDto;
import com.gml.simpc.api.dto.EducationLevelDto;
import com.gml.simpc.api.dto.InformalEducationDto;
import com.gml.simpc.api.dto.IntermediationDto;
import com.gml.simpc.api.dto.LanguageDto;
import com.gml.simpc.api.dto.OtherKnowledgeDto;
import com.gml.simpc.api.dto.PilaDto;
import com.gml.simpc.api.dto.PilaEmploymentServiceDto;
import com.gml.simpc.api.dto.ProfitDto;
import com.gml.simpc.api.dto.ProgramDto;
import com.gml.simpc.api.dto.TrainingDto;
import com.gml.simpc.api.dto.WorkExperienceDto;
import com.gml.simpc.api.entity.Intersection;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.enums.IntersectionStatus;
import com.gml.simpc.api.services.ParameterService;
import com.gml.simpc.api.services.UserService;
import com.gml.simpc.api.utilities.Util;
import com.gml.simpc.commons.mail.sender.Notificator;
import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import com.gml.simpc.loader.thread.ThreadLocalValueList;
import com.gml.simpc.web.constants.WebConstants;
import com.gml.simpc.web.repository.IntersectionRepository;
import com.gml.simpc.web.utilities.IntersectionFilesCreator;
import com.gml.simpc.web.utilities.ValueListService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import static com.gml.simpc.api.utilities.Util.ID_PARAMETRO_PATH_CRUCES;
import static com.gml.simpc.api.utilities.Util.PARAMETER_MASS_CONSULT;

/**
 * Clase encargada de ejecutar el cruce.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Scope("prototype")
@Service
public class IntersectionExecutor implements Runnable {

    private static final Logger LOGGER
            = Logger.getLogger(IntersectionExecutor.class);
    /**
     * Cruce a ejecutar.
     */
    private Intersection intersection;
    /**
     * Servicio para lanzar batch jobs.
     */
    @Autowired
    private JobLauncher jobLauncher;
    /**
     * Job para cruces.
     */
    @Autowired
    @Qualifier(value = "personJob")
    private Job intersectionJob;
    /**
     * Repositorio de cruces.
     */
    @Autowired
    private IntersectionRepository intersectionRepository;
    /**
     * Servicio para obtener las listas de valores param&eacute;tricos del
     * sistema.
     */
    @Autowired
    private ValueListService valueListService;
    /**
     * Componente de creaci&oacute;n de archivos de cruce.
     */
    @Autowired
    private IntersectionFilesCreator intersectionFilesCreator;
    /**
     * DAO para cruces masivos.
     */
    @Autowired
    private MassiveIntersectionDao massInterDao;

    /**
     * Repositorio de par&aacute;metros.
     */
    @Autowired
    private ParameterService parameterService;
    /**
     * Clase para enviar correos.
     */
    @Autowired
    private Notificator mailSender;
    /**
     * Servicio de usuarios.
     */
    @Autowired
    private UserService userService;
    /**
     * Mensaje de edicion de una institucion.
     */
    private static final String MENSAJE_MASS_CONSULT
            = "Su cruce ha finalizado. Diríjase a la ventana de historial de "
            + "cruces para encontrar su resultado";

    /**
     * M&eacute;todo que inicia la ejecuci&oacute;n.
     */
    @Override
    public void run() {
        LOGGER.info("Inicia la ejecucion del cruce [ "
                + this.intersection.getId() + " ]");

        try {
            JobExecution jobExecution = launchJob();
            afterJob(jobExecution);
            initFilesCreation();
            endProcess();
        } catch (Exception ex) {
            LOGGER.error("Error", ex);

            endProcessWithError(ex.getMessage());
        }
        this.sendMessage(this.intersection.getUser(), MENSAJE_MASS_CONSULT);

    }

    private void sendMessage(User userSession, String message) {

        for (User usuario : this.userService.findByAdmin(true)) {
            mailSender.sendNotification(usuario.getEmail(),
                    message, usuario.getFirstName().
                            concat(" ").concat(usuario.getFirstSurname()),
                    PARAMETER_MASS_CONSULT);

        }
        mailSender.sendNotification(userSession.getEmail(),
                message, userSession.getFirstName().
                        concat(" ").concat(userSession.getFirstSurname()),
                PARAMETER_MASS_CONSULT);

    }

    /**
     * Adiciona a la ejecuci&oacute;n el cruce a procesar.
     *
     * @param intersection
     */
    public void setIntersection(Intersection intersection) {
        this.intersection = intersection;
    }

    /**
     * Lanza el job.
     *
     * @throws JobExecutionException
     */
    private JobExecution launchJob() throws JobExecutionException {
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath", "file:"
                + this.intersection.getLoadFile());
        jobParametersBuilder.addString("loadId",
                String.valueOf(this.intersection.getId()));
        JobParameters parameters = jobParametersBuilder.toJobParameters();

        //Inicializamos local thread
        ThreadLocalJob.init();

        //Inicializamos listas de valores
        ThreadLocalValueList.set(this.valueListService.getValueList());

        return this.jobLauncher.run(this.intersectionJob, parameters);
    }

    /**
     * Proceso que se ejcuta despues del job.
     *
     * @param jobExecution
     */
    public void afterJob(JobExecution jobExecution) {
        JobResultDto jobResult = ThreadLocalJob.getResult();

        this.intersection.setFailedRecords(jobResult.getErrorList().size());
        this.intersection.setTotalOfRecors(jobResult.getProcesedLines());
        this.intersection.setSuccessRecords(jobResult.getSuccessLines());

        if (jobExecution.getStatus() == BatchStatus.COMPLETED
                && jobResult.getErrorList().isEmpty()) {
            this.intersection.setStatus(IntersectionStatus.LOADED);
        } else {
            this.intersection.setStatus(IntersectionStatus.ERROR);
            this.intersection.setDetail(jobResult.getErrorsString());
        }

        this.intersectionRepository.saveAndFlush(this.intersection);

        ThreadLocalJob.delete();
    }

    /**
     * Crea los archivos de cruce.
     */
    private void initFilesCreation() {
        long timestamp = new Date().getTime();

        String path = this.parameterService.
                findById(ID_PARAMETRO_PATH_CRUCES).getValue();

        path = path.concat("" + this.intersection.getUser().getId()).
                concat("/" + timestamp);

        this.intersection.setResultPath(path);

        loadPilaResults(path,
                isUserEmploymentPublicService(this.intersection.getUser()));
        if (!isUserEmploymentPublicService(this.intersection.getUser())) {
            loadFosfecResults(path);
        }
        loadIntermediationResults(path);
        loadTrainingResults(path);
        loadCurriculumVitaeResults(path);

    }

    /**
     * M&eacute;todo para cagar listas pila.
     *
     * @param path
     */
    private void loadPilaResults(String path, boolean isPublicService) {
        if (!isPublicService) {
            LOGGER.info("Entra a la carga de pila (NO servicio publico de empleo)");
            List<PilaDto> resultsPila = this.massInterDao.
                    findPila(this.intersection.getId());

            List<List<?>> dtoList = new ArrayList<>();
            dtoList.add(resultsPila);

            List<Object> emptyDtoList = new ArrayList<>();
            emptyDtoList.add(new PilaDto());

            createFiles(WebConstants.INTERSECTION_PILA_FILE_NAME,
                    path, dtoList, emptyDtoList);
        } else {
            LOGGER.info("Entra a la carga de pila (SI servicio publico de empleo)");
            List<PilaEmploymentServiceDto> resultsPila = this.massInterDao.
                    findPilaES(this.intersection.getId());

            List<List<?>> dtoList = new ArrayList<>();
            dtoList.add(resultsPila);

            List<Object> emptyDtoList = new ArrayList<>();
            emptyDtoList.add(new PilaEmploymentServiceDto());

            createFiles(WebConstants.INTERSECTION_PILA_FILE_NAME,
                    path, dtoList, emptyDtoList);
        }
    }

    /**
     * M&eacute;todo cargar lista de intermediaci&oacute;n.
     *
     * @param path
     */
    private void loadIntermediationResults(String path) {
        List<IntermediationDto> resultsIntermediacion = this.massInterDao.
                findIntermediation(this.intersection.getId());

        List<List<?>> dtoList = new ArrayList<>();
        dtoList.add(resultsIntermediacion);

        List<Object> emptyDtoList = new ArrayList<>();
        emptyDtoList.add(new IntermediationDto());

        createFiles(WebConstants.INTERSECTION_INTERMEDIATION_FILE_NAME,
                path, dtoList, emptyDtoList);
    }

    /**
     * Crea el archivo de recursos FOSFEC.
     *
     * @param path
     */
    private void loadFosfecResults(String path) {
        List<ProfitDto> resultsFosfec = this.massInterDao.
                findFosfec(this.intersection.getId());

        List<List<?>> dtoList = new ArrayList<>();
        dtoList.add(resultsFosfec);

        List<Object> emptyDtoList = new ArrayList<>();
        emptyDtoList.add(new ProfitDto());

        createFiles(WebConstants.INTERSECTION_FOSFEC_FILE_NAME,
                path, dtoList, emptyDtoList);
    }
    
    /**
     * Crea el archivo de orientación y capacitación.
     *
     * @param path
     */
    private void loadTrainingResults(String path) {
        List<ProgramDto> resultsTraining = this.massInterDao.
                findTraining(this.intersection.getId());

        List<List<?>> dtoList = new ArrayList<>();
        dtoList.add(resultsTraining);

        List<Object> emptyDtoList = new ArrayList<>();
        emptyDtoList.add(new ProgramDto());

        createFiles(WebConstants.INTERSECTION_TRAININGS_FILE_NAME,
                path, dtoList, emptyDtoList);
    }

    /**
     * M&eacute;todo para cargar listas de hojas de vida.
     *
     * @param path
     */
    private void loadCurriculumVitaeResults(String path) {
        List<BasicInformationDto> resultsBasicInformation = this.massInterDao.
                findBasicInformation(this.intersection.getId());
        List<EducationLevelDto> resultsEducationlevel = this.massInterDao.
                findEducationLevel(this.intersection.getId());
        List<WorkExperienceDto> resultsWorkExperience = this.massInterDao.
                findWorkExperience(this.intersection.getId());
        List<InformalEducationDto> resultsInformalEducation = this.massInterDao.
                findInformalEducation(this.intersection.getId());
        List<LanguageDto> resultsLanguage = this.massInterDao.
                findLanguage(this.intersection.getId());
        List<OtherKnowledgeDto> resultsOtherKnowledge = this.massInterDao.
                findOtherKnowledge(this.intersection.getId());
        
        List<BasicInformationDto> basicInformationList = new ArrayList<>();
        List<EducationLevelDto> educationlevelList = new ArrayList<>();
        List<WorkExperienceDto> workExperienceList = new ArrayList<>();
        List<InformalEducationDto> informalEducationList = new ArrayList<>();
        List<LanguageDto> languageList = new ArrayList<>();
        List<OtherKnowledgeDto> otherKnowledgeList = new ArrayList<>();
        
        for (BasicInformationDto basicInformation : resultsBasicInformation) {
            int max = 1;
            int count = 0;
            String documentType = basicInformation.getDocumentType();
            String documentNumber = basicInformation.getNumberIdentification();
            
            basicInformationList.add(basicInformation);
            
            for (EducationLevelDto el : resultsEducationlevel) {
                if (el.getDocumentType().equals(documentType) && el.getNumberIdentification().equals(documentNumber)) {
                    educationlevelList.add(el);
                    count++;
                }
            }
            if (count > max) 
                max = count;
            
            count = 0;
            for (WorkExperienceDto we : resultsWorkExperience) {
                if (we.getDocumentType().equals(documentType) && we.getNumberIdentification().equals(documentNumber)) {
                    workExperienceList.add(we);
                    count++;
                }
            }
            if (count > max) 
                max = count;
            
            count = 0;
            for (InformalEducationDto ie : resultsInformalEducation) {
                if (ie.getDocumentType().equals(documentType) && ie.getNumberIdentification().equals(documentNumber)) {
                    informalEducationList.add(ie);
                    count++;
                }
            }
            if (count > max)
                max = count;
            
            count = 0;
            for (LanguageDto la : resultsLanguage) {
                if (la.getDocumentType().equals(documentType) && la.getNumberIdentification().equals(documentNumber)) {
                    languageList.add(la);
                    count++;
                }
            }
            if (count > max)
                max = count;
            
            count = 0;
            for (OtherKnowledgeDto ok : resultsOtherKnowledge) {
                if (ok.getDocumentType().equals(documentType) && ok.getNumberIdentification().equals(documentNumber)) {
                    otherKnowledgeList.add(ok);
                    count++;
                }
            }
            if (count > max)
                max = count;
            
            BasicInformationDto basicInfo = new BasicInformationDto();
            basicInfo.setDocumentType(basicInformation.getDocumentType()); 
            basicInfo.setNumberIdentification(basicInformation.getNumberIdentification()); 
            for (int i = 1; i < max; i++)
                basicInformationList.add(basicInfo);
            
            count = basicInformationList.size() - educationlevelList.size();
            for (int i = 1; i < count; i++)
                educationlevelList.add(new EducationLevelDto());
            
            count = basicInformationList.size() - workExperienceList.size();
            for (int i = 1; i < count; i++)
                workExperienceList.add(new WorkExperienceDto());
            
            count = basicInformationList.size() - informalEducationList.size();
            for (int i = 1; i < count; i++)
                informalEducationList.add(new InformalEducationDto());
            
            count = basicInformationList.size() - languageList.size();
            for (int i = 1; i < count; i++)
                languageList.add(new LanguageDto());
            
            count = basicInformationList.size() - otherKnowledgeList.size();
            for (int i = 1; i < count; i++)
                otherKnowledgeList.add(new OtherKnowledgeDto());
        }
        
        List<List<?>> dtoList = new ArrayList<>();
        dtoList.add(basicInformationList);
        dtoList.add(educationlevelList);
        dtoList.add(workExperienceList);
        dtoList.add(informalEducationList);
        dtoList.add(languageList);
        dtoList.add(otherKnowledgeList);

        List<Object> emptyDtoList = new ArrayList<>();
        emptyDtoList.add(new BasicInformationDto());
        emptyDtoList.add(new EducationLevelDto());
        emptyDtoList.add(new WorkExperienceDto());
        emptyDtoList.add(new InformalEducationDto());
        emptyDtoList.add(new LanguageDto());
        emptyDtoList.add(new OtherKnowledgeDto());

        createFiles(WebConstants.INTERSECTION_CURRICULUM_VITAE_FILE_NAME,
                path, dtoList, emptyDtoList);
    }

    /**
     * Crea los archivos segun el cruce.
     *
     * @param fileName
     * @param path
     * @param dtoList
     * @param emptyDtoList
     */
    private void createFiles(String fileName, String path, List<List<?>> dtoList,
            List<Object> emptyDtoList) {

        this.intersectionFilesCreator.createXlsFile(dtoList, emptyDtoList,
                fileName, path);
        this.intersectionFilesCreator.createTxtFile(dtoList, emptyDtoList,
                fileName, path);
        this.intersectionFilesCreator.createCsvFile(dtoList, emptyDtoList,
                fileName, path);
    }

    /**
     * Finaliza el proceso exitosamente.
     */
    private void endProcess() {
        this.intersection.setStatus(IntersectionStatus.DONE);

        this.intersection.setEndDate(new Date());
        this.intersectionRepository.saveAndFlush(this.intersection);
    }

    /**
     * Finaliza el proceso con errores.
     */
    private void endProcessWithError(String message) {
        try {
            this.intersection.setStatus(IntersectionStatus.ERROR);
            this.intersection.setDetail(message);
            this.intersection.setEndDate(new Date());
            this.intersectionRepository.saveAndFlush(this.intersection);
        } catch (Exception ex) {
            LOGGER.error("Error", ex);
        }
    }

    /**
     * M&eacute;todo que valida si es usuario del servicio publico de empleo
     *
     * @param user
     *
     * @return
     */
    private Boolean isUserEmploymentPublicService(User user) {
        return "UAESPE".equals(user.getProfile().getName());
        
    }
}
