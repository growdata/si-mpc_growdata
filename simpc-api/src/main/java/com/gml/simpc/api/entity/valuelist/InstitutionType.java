/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: InstitutionType.java
 * Created on: 2016/10/1, 01:47:35 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity.valuelist;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad que representa la instituci&oacute;n.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Table(name = "TIPOS_INSTITUCION")
@Entity
public class InstitutionType implements Serializable {

    /**
     * ID del tipo de instituci&oacute;n.
     */
    @Id
    @Column(name = "ID", length = 10)
    private Long id;
    /**
     * nombre del tupo de instituci&oacute;n.
     */
    @Column(name = "NOMBRE", length = 50)
    private String name;

    /**
     * descripci&oacute;n
     */
    @Column(name = "DESCRIPCION", length = 200)
    private String description;

    /**
     * Constructor por defecto.
     */
    public InstitutionType() {
        this.id = null;
    }

    /**
     * Constructor por nombre.
     *
     * @param name
     */
    public InstitutionType(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InstitutionType other = (InstitutionType) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return name;
    }
}
