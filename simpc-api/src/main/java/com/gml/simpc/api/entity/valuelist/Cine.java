/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Cine.java
 * Created on: 2017/02/19, 08:25:45 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity.valuelist;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
@Table(name = "CINE")
@Entity
public class Cine implements Serializable {

    /**
     * Identificador de la entidad.
     */
    @Id
    @Column(name = "ID", length = 20)
    private Long id;
    /**
     * C&oacute;digo.
     */
    @Column(name = "CODIGO", length = 20)
    private String code;

    /**
     * Descripci&oacute;n
     */
    @Column(name = "VALOR", length = 50)
    private String value;

    public Cine(Long id, String code, String value) {
        this.id = id;
        this.code = code;
        this.value = value;
    }

    public Cine() {

    }

    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
