/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: FileUploaderWebTest.java
 * Created on: 2017/02/08, 09:58:17 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.file;

import com.gml.simpc.api.entity.LoadProcess;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import static com.gml.simpc.api.entity.exeption.code.FileErrorCode.FILE_ERR_001_EMPTY_FILE;
import static com.gml.simpc.api.entity.exeption.code.FileErrorCode.FILE_ERR_008_INPUT_OUTPUT_ERROR;
import static org.junit.Assert.assertTrue;

/**
 * Pruebas para carga web de archivos
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica</a>
 */
@Ignore
public class FileUploaderWebTest {

    /**
     * Logeer para la clase.
     */
    private static final org.apache.log4j.Logger LOGGER =
        org.apache.log4j.Logger.getLogger(FileUploaderWebTest.class);

    /**
     * Servicio para ejecutar jobs en modo pruebas
     */
    public FileUploaderWebTest() {
    }

    /**
     * Caso de prueba para archivos correctos
     */
    @Test
    public void testWebUploadSuccessCase() {

        LOGGER.info("Running FileUploaderWebTest - testWebUploadSuccessCase");
        FileUploader uploader = new FileUploader();

        String fileName =
            "loadTestFiles/program/training/program-code-error.txt";
        String loadPath = "C:/SIMPC/UPLOADS/";
        File file = new File(fileName);
        LoadProcess loadProcess = new LoadProcess(loadPath, "web", "CAP-1", "A");
        uploader.sendFile(file, loadProcess, new HashMap<>());
        LOGGER.info(loadProcess.toString());
        assertTrue(loadProcess.getFileSended() == true);
        assertTrue(loadProcess.getErrorCode() == null);
    }

    /**
     * Caso de prueba para archivos que no existen
     */
    @Test
    public void testWebUpLoadNoFoundFileCase() {
        LOGGER.
            info("Running FileUploaderWebTest - testWebUpLoadNoFoundFileCase");
        FileUploader uploader = new FileUploader();
        String fileName = "loadTestFiles/program/training/non-exists.txt";
        String loadPath = "C:/SIMPC/UPLOADS/";
        File file = new File(fileName);
        LoadProcess loadProcess = new LoadProcess(loadPath, "web", "CAP-1", "A");
        uploader.sendFile(file, loadProcess, new HashMap<>());
        LOGGER.info(loadProcess.toString());
        assertTrue(loadProcess.getFileSended() == false);
        assertTrue(loadProcess.getErrorCode() == FILE_ERR_001_EMPTY_FILE);

    }

    /**
     * Caso de prueba para archivos correctos a partir de un MultipartFile
     */
    @Test
    public void testWebUploadSuccessMultCase() {
        LOGGER.
            info("Running FileUploaderWebTest - testWebUploadSuccessMultCase");
        FileUploader uploader = new FileUploader();
        String fileName =
            "loadTestFiles/program/training/program-code-error.txt";
        String loadPath = "C:/SIMPC/UPLOADS/";
        File testFile = new File(fileName);
        MultipartFile multFile = fileToMockMultipartFile(testFile);
        LoadProcess loadProcess = new LoadProcess(loadPath, "web", "CAP-1", "A");
        uploader.sendFile(multFile, loadProcess, new HashMap<>());

        LOGGER.info(loadProcess.toString());
        assertTrue(loadProcess.getFileSended() == true);
        assertTrue(loadProcess.getErrorCode() == null);
    }

    /**
     * Caso de prueba para ruta de carga incorrecta
     */
    //@Test
    public void testWebUploadBadPathCase() {

        LOGGER.info("Running FileUploaderWebTest - testWebUploadBadPathCase");
        FileUploader uploader = new FileUploader();

        String fileName =
            "loadTestFiles/program/training/program-code-error.txt";
        String loadPath = "G:/SIMPC/UPLOADS/";
        File file = new File(fileName);
        LoadProcess loadProcess = new LoadProcess(loadPath, "web", "CAP-1", "A");
        uploader.sendFile(file, loadProcess, new HashMap<>());
        LOGGER.info(loadProcess.toString());
        assertTrue(loadProcess.getFileSended() == false);
        assertTrue(loadProcess.getErrorCode() == FILE_ERR_008_INPUT_OUTPUT_ERROR);
    }

    /**
     * M�todo privado que convierte un File en MultipartFile usando
     * MockMultipartFile
     *
     * @param file
     *
     * @return
     */
    public static MultipartFile fileToMockMultipartFile(File file) {

        Path path = Paths.get(file.getPath());
        String contentType = "text/plain";
        byte[] content = null;
        try {
            content = Files.readAllBytes(path);
        } catch (final IOException e) {
        }
        MultipartFile result = new MockMultipartFile(file.getName(),
            file.getName(), contentType, content);
        return result;
    }
}
