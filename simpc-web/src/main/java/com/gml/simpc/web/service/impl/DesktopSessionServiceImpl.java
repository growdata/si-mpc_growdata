/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: DesktopSessionServiceImpl.java
 * Created on: 2017/03/07, 11:33:29 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.entity.DesktopSession;
import com.gml.simpc.api.services.DesktopSessionService;
import com.gml.simpc.web.repository.DesktopSessionRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Servicio para sesiones de escritorio.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Service
public class DesktopSessionServiceImpl implements DesktopSessionService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = 
        Logger.getLogger(DesktopSessionServiceImpl.class);
    /**
     * Repositorio de sesiones de escritorio.
     */
    @Autowired
    private DesktopSessionRepository desktopSessionRepository;

    /**
     * Busca el token por usuario.
     * 
     * @param token
     * @return 
     */
    @Override
    public DesktopSession findByUserToken(String token) {
        LOGGER.info("Ejecutando metodo [ findByUserToken ]");
        
        return this.desktopSessionRepository.findByUserToken(token);
    }

    /**
     * Elimina token expirados.
     * 
     */
    @Override
    public void deleteExpiratedTokens() {
        LOGGER.info("Ejecutando metodo [ deleteExpiratedTokens ]");
        
        this.desktopSessionRepository.deleteExpiratedTokens();
    }

    /**
     * Guarda token por sesi&ocute;n de usuario.
     * 
     * @param desktopSession 
     */
    @Override
    public void save(DesktopSession desktopSession) {
        LOGGER.info("Ejecutando metodo [ save ]");
        
        this.desktopSessionRepository.saveAndFlush(desktopSession);
    }
}
