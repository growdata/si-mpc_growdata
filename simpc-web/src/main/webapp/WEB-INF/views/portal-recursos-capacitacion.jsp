<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Recursos de Capacitaci�n Para La Inserci�n y Reinserci�n Laboral</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster.bundle.min.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster-sideTip-shadow.min.css" rel="stylesheet"/>
        <link href="resources/css/morris.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <div class="col-sm-3" style="padding:0px;">
                        <img src="resources/images/logo_mpc.png" width="100%" alt=""/>
                    </div>
                    <div class="col-sm-8">
                        <a class="navbar-brand" href="/masterdata/index.htm">Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</a>
                    </div>
                </div>
                <div class="navbar-nav navbar-form credenciales pull-right">
                    <table>
                        <tr>
                            <td>
                                <strong>Bienvenido</strong> 
                            </td>
                            <td style="width: 120px; text-align: right">
                                <a id="" href="/masterdata/index.htm" class="btn btn-warning btn-xs">Volver al inicio</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </nav>
        <div class="container">
            <h1>Recursos de Capacitaci�n Para La Inserci&oacute;n y Reinserci&oacute;n Laboral</h1>
            <!-- main area -->
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Filtros de B&uacute;squeda</h2></div>
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li><a data-toggle="tab" href="#ccf"  id="ccftab" onclick="borraGrafico()">Caja de compensaci&oacute;n</a></li>
                        <li><a data-toggle="tab" href="#costos" id="costostab"  onclick="borraGrafico()" >Costos</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="alert-area" ></div>
                        <div id="ccf" class="tab-pane fade in ">
                            <div class="panel-body">
                                <form:form  action="/masterdata/recursos-capacitacion-ccf.htm" method="POST"  id="rangoForm" name="rangoForm" modelAttribute="searchCostos" >
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="ccfLabel" class="control-label">Caja de Compensaci&oacute;n</label>
                                            <form:select class="form-control" id="ccflist" path="ccfCode" name="ccflist">
                                                <option label="Todas las CCF" value=""></option>
                                                <c:if test="${not empty ccfList}">
                                                    <c:forEach var="item" items="${ccfList}">
                                                        <c:if test="${item.isCCF == 'V'}">
                                                            <option <c:if test="${item.code eq ccfSelected}">selected="selected"</c:if> label="${item.shortName}" value="${item.code}">${item.shortName}</option>
                                                        </c:if>
                                                    </c:forEach>
                                                </c:if>
                                            </form:select>   
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 ">
                                        <div class="form-group">
                                            <label for="year" class="control-label">A�o</label>
                                            <select class="form-control" onchange="anioAutiObl1()" id="anio1" name="anio" >
                                                <option label="Todos los A�os"  value="">Todos los A�os</option>
                                                <c:if test="${not empty anios}">
                                                    <c:forEach var="item" items="${anios}">
                                                        <option <c:if test="${item eq yearSelected}">selected="selected"</c:if>  value="${item}" >${item}</option>
                                                    </c:forEach>
                                                </c:if>
                                            </select>
                                        </div>
                                    </div> 
                                    <div class="col-sm-4 ">
                                        <div class="form-group">
                                            <label for="monthtab2" class="control-label">Mes</label>
                                            <select class="form-control" onchange="anioObligatorio()" id="mes1" name="mes"  >
                                                <option label="Todos los Meses" value=""></option>
                                                <option <c:if test="${monthSelected2 == '01'}">selected="selected"</c:if> value="01">Enero</option>
                                                <option <c:if test="${monthSelected2 == '02'}">selected="selected"</c:if> value="02">Febrero</option>
                                                <option <c:if test="${monthSelected2 == '03'}">selected="selected"</c:if> value="03">Marzo</option>
                                                <option <c:if test="${monthSelected2 == '04'}">selected="selected"</c:if> value="04">Abril</option>
                                                <option <c:if test="${monthSelected2 == '05'}">selected="selected"</c:if> value="05">Mayo</option>
                                                <option <c:if test="${monthSelected2 == '06'}">selected="selected"</c:if> value="06">Junio</option>
                                                <option <c:if test="${monthSelected2 == '07'}">selected="selected"</c:if> value="07">Julio</option>
                                                <option <c:if test="${monthSelected2 == '08'}">selected="selected"</c:if> value="08">Agosto</option>
                                                <option <c:if test="${monthSelected2 == '09'}">selected="selected"</c:if> value="09">Septiembre</option>
                                                <option <c:if test="${monthSelected2 == '10'}">selected="selected"</c:if> value="10">Octubre</option>
                                                <option <c:if test="${monthSelected2 == '11'}">selected="selected"</c:if> value="11">Noviembre</option>
                                                <option <c:if test="${monthSelected2 == '12'}">selected="selected"</c:if> value="12">Diciembre</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-offset-4 col-sm-8" style="text-align:right">
                                        <input  type="submit" value="Buscar" class="btn btn-success">
                                    </div>
                                </form:form>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading"><h2>Resultado de la B&uacute;squeda</h2></div>
                                <div class="panel-body">
                                    <div class=" table-responsive col-md-12">
                                        <center>
                                            <button type="button" class="btn btn-default desableBtn"
                                                    data-toggle="modal" data-target="#graphicModal">  
                                                <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                                                Ver Gr&aacute;fico
                                            </button>
                                        </center>
                                        <table id="tabla1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Tipo de Formaci&oacute;n</th>
                                                    <th>Costo Total Por Matricula</th>
                                                    <th>Costo de subsidio de Transporte</th>
                                                    <th>Otros Costos Asociados</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Tipo de Formaci&oacute;n</th>
                                                    <th>Costo Total Por Matricula</th>
                                                    <th>Costo de subsidio de Transporte</th>
                                                    <th>Otros Costos Asociados</th>
                                                </tr>
                                            </tfoot>
                                            <c:if test="${!resultOk}">
                                                <tbody>
                                                    <c:if test="${not empty resultList}">
                                                        <tr> 
                                                            <td>Competencias claves y transversales</td>
                                                            <td id="val1" align="right">
                                                                <c:forEach items="${resultList}" var="item" begin="0">
                                                                    <c:if test="${item.tipoCapNombre == 'Comp claves y transversales'}">$${item.enrollmentCost}</c:if>
                                                                </c:forEach>
                                                            </td>
                                                            <td id="val2" align="right">
                                                                <c:forEach items="${resultList}" var="item" begin="0">
                                                                    <c:if test="${item.tipoCapNombre == 'Comp claves y transversales'}">$${item.transportCost}</c:if>
                                                                </c:forEach>
                                                            </td>
                                                            <td id="val3" align="right">
                                                                <c:forEach items="${resultList}" var="item" begin="0">
                                                                    <c:if test="${item.tipoCapNombre == 'Comp claves y transversales'}">$${item.otherCost}</c:if>
                                                                </c:forEach>
                                                            </td>
                                                        </tr>   
                                                        <tr> 
                                                            <td>M�dulo TICS</td>
                                                            <td id="val4" align="right">
                                                                <c:forEach items="${resultList}" var="item" begin="0">
                                                                    <c:if test="${item.tipoCapNombre == 'M�dulo TICS'}">$${item.enrollmentCost}</c:if>
                                                                </c:forEach>
                                                            </td>
                                                            <td id="val5" align="right">
                                                                <c:forEach items="${resultList}" var="item" begin="0">
                                                                    <c:if test="${item.tipoCapNombre == 'M�dulo TICS'}">$${item.transportCost}</c:if>
                                                                </c:forEach>
                                                            </td>
                                                            <td id="val6" align="right">
                                                                <c:forEach items="${resultList}" var="item" begin="0">
                                                                    <c:if test="${item.tipoCapNombre == 'M�dulo TICS'}">$${item.otherCost}</c:if>
                                                                </c:forEach>
                                                            </td>
                                                        </tr>   
                                                        <tr> 
                                                            <td>Validaci�n bachillerato</td>
                                                            <td id="val7" align="right">
                                                                <c:forEach items="${resultList}" var="item" begin="0">
                                                                    <c:if test="${item.tipoCapNombre == 'Validaci�n bachillerato'}">$${item.enrollmentCost}</c:if>
                                                                </c:forEach>
                                                            </td>
                                                            <td id="val8" align="right">
                                                                <c:forEach items="${resultList}" var="item" begin="0">
                                                                    <c:if test="${item.tipoCapNombre == 'Validaci�n bachillerato'}">$${item.transportCost}</c:if>
                                                                </c:forEach>
                                                            </td>
                                                            <td id="val9" align="right">
                                                                <c:forEach items="${resultList}" var="item" begin="0">
                                                                    <c:if test="${item.tipoCapNombre == 'Validaci�n bachillerato'}">$${item.otherCost}</c:if>
                                                                </c:forEach>
                                                            </td>
                                                        </tr>   
                                                        <tr> 
                                                            <td>T�cnico laboral</td>
                                                            <td id="val10" align="right">
                                                                <c:forEach items="${resultList}" var="item" begin="0">
                                                                    <c:if test="${item.tipoCapNombre == 'T�cnico laboral'}">$${item.enrollmentCost}</c:if>
                                                                </c:forEach>
                                                            </td>
                                                            <td id="val11" align="right">
                                                                <c:forEach items="${resultList}" var="item" begin="0">
                                                                    <c:if test="${item.tipoCapNombre == 'T�cnico laboral'}">$${item.transportCost}</c:if>
                                                                </c:forEach>
                                                            </td>
                                                            <td id="val12" align="right">
                                                                <c:forEach items="${resultList}" var="item" begin="0">
                                                                    <c:if test="${item.tipoCapNombre == 'T�cnico laboral'}">$${item.otherCost}</c:if>
                                                                </c:forEach>
                                                            </td>
                                                        </tr>   
                                                        <tr> 
                                                            <td>Certificaci�n de competencias</td>
                                                            <td id="val13" align="right">
                                                                <c:forEach items="${resultList}" var="item" begin="0">
                                                                    <c:if test="${item.tipoCapNombre == 'Certificaci�n de competencias'}">$${item.enrollmentCost}</c:if>
                                                                </c:forEach>
                                                            </td>
                                                            <td id="val14" align="right">
                                                                <c:forEach items="${resultList}" var="item" begin="0">
                                                                    <c:if test="${item.tipoCapNombre == 'Certificaci�n de competencias'}">$${item.transportCost}</c:if>
                                                                </c:forEach>
                                                            </td>
                                                            <td id="val15" align="right">
                                                                <c:forEach items="${resultList}" var="item" begin="0">
                                                                    <c:if test="${item.tipoCapNombre == 'Certificaci�n de competencias'}">$${item.otherCost}</c:if>
                                                                </c:forEach>
                                                            </td>
                                                        </tr>   
                                                        <tr> 
                                                            <td>Reentrenamiento t�cnico</td>
                                                            <td id="val16" align="right">
                                                                <c:forEach items="${resultList}" var="item" begin="0">
                                                                    <c:if test="${item.tipoCapNombre == 'Reentrenamiento t�cnico'}">$${item.enrollmentCost}</c:if>
                                                                </c:forEach>
                                                            </td>
                                                            <td id="val17" align="right">
                                                                <c:forEach items="${resultList}" var="item" begin="0">
                                                                    <c:if test="${item.tipoCapNombre == 'Reentrenamiento t�cnico'}">$${item.transportCost}</c:if>
                                                                </c:forEach>
                                                            </td>
                                                            <td id="val18" align="right">
                                                                <c:forEach items="${resultList}" var="item" begin="0">
                                                                    <c:if test="${item.tipoCapNombre == 'Reentrenamiento t�cnico'}">$${item.otherCost}</c:if>
                                                                </c:forEach>
                                                            </td>
                                                        </tr>   
                                                    </c:if>
                                                </tbody>
                                            </c:if>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="costos" class="tab-pane fade">
                            <div class="panel-body">
                                <form:form id="costosForm" name="costosForm" method="GET" action="/masterdata/recursos-capacitacion-costos.htm"  modelAttribute="searchCostos2"  >
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <input type="hidden" id="activateBtn" value="${activateBtn}">
                                            <label for="ccfLabel2" class="control-label">Cajas de Compensaci&oacute;n</label>
                                            <form:select class="form-control" id="ccflist2" path="ccfCode" name="ccflist2">
                                                <option label="Todas las CCF" value="%"></option>
                                                <c:if test="${not empty ccfList}">
                                                    <c:forEach var="item" items="${ccfList}" begin="0">
                                                        <c:if test="${item.isCCF == 'V'}">
                                                            <option <c:if test="${item.code eq ccfSelected2}">selected="selected"</c:if> label="${item.shortName}" value="${item.code}">${item.shortName}</option>
                                                        </c:if>
                                                    </c:forEach>

                                                </c:if>
                                            </form:select>   
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                   <div class="col-sm-4 ">
                                        <div class="form-group">
                                            <label for="year" class="control-label">A�o</label>
                                            <select class="form-control" onchange="anioAutiObl2()" id="anio2" name="anio" >
                                                <option label="Todos los A�os"  value="">Todos los A�os</option>
                                                <c:if test="${not empty anios}">
                                                    <c:forEach var="item" items="${anios}">
                                                        <option <c:if test="${item eq yearSelected}">selected="selected"</c:if>  value="${item}" >${item}</option>
                                                    </c:forEach>
                                                </c:if>
                                            </select>
                                        </div>
                                    </div> 
                                    <div class="col-sm-4 ">
                                        <div class="form-group">
                                            <label for="monthtab2" class="control-label">Mes</label>
                                            <select class="form-control" onchange="anioObligatorio2()" id="mes2" name="mes"  >
                                                <option label="Todos los Meses" value=""></option>
                                                <option <c:if test="${monthSelected2 == '01'}">selected="selected"</c:if> value="01">Enero</option>
                                                <option <c:if test="${monthSelected2 == '02'}">selected="selected"</c:if> value="02">Febrero</option>
                                                <option <c:if test="${monthSelected2 == '03'}">selected="selected"</c:if> value="03">Marzo</option>
                                                <option <c:if test="${monthSelected2 == '04'}">selected="selected"</c:if> value="04">Abril</option>
                                                <option <c:if test="${monthSelected2 == '05'}">selected="selected"</c:if> value="05">Mayo</option>
                                                <option <c:if test="${monthSelected2 == '06'}">selected="selected"</c:if> value="06">Junio</option>
                                                <option <c:if test="${monthSelected2 == '07'}">selected="selected"</c:if> value="07">Julio</option>
                                                <option <c:if test="${monthSelected2 == '08'}">selected="selected"</c:if> value="08">Agosto</option>
                                                <option <c:if test="${monthSelected2 == '09'}">selected="selected"</c:if> value="09">Septiembre</option>
                                                <option <c:if test="${monthSelected2 == '10'}">selected="selected"</c:if> value="10">Octubre</option>
                                                <option <c:if test="${monthSelected2 == '11'}">selected="selected"</c:if> value="11">Noviembre</option>
                                                <option <c:if test="${monthSelected2 == '12'}">selected="selected"</c:if> value="12">Diciembre</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="costTypeLabel2" class="control-label">Tipo de Costo</label>
                                            <select class="form-control" id="costType2" path="" name="costType2" required="required">
                                                <option label="Seleccione..." value="">Seleccione...</option>
                                                <c:if test="${not empty costList}">
                                                    <c:forEach var="item" items="${costList}" begin="0">
                                                        <option <c:if test="${item.key eq costTypeSelected}">selected="selected"</c:if> value="${item.key}">${item.value}</option>
                                                    </c:forEach>        
                                                </c:if>
                                            </select>   
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12" style="text-align:right">
                                        <input  type="submit" value="Buscar" class="btn btn-success">    
                                    </div>
                                </form:form>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading"><h2>Resultado de la B&uacute;squeda</h2></div>
                                <div class="panel-body">
                                    <div class=" table-responsive col-md-12">
                                        <center>
                                            <button type="button" class="btn btn-default desableBtn"
                                                    data-toggle="modal" data-target="#graphicModal">  
                                                <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                                                Ver gr&aacute;fico
                                            </button>
                                        </center>
                                        <table id="tabla2" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Tipo de Formaci&oacute;n</th>
                                                    <th> <c:choose>
                                                            <c:when test="${tipo=='matricula'}"> Costo de Matr�cula</c:when>
                                                            <c:when test="${tipo=='transporte'}"> Costo de Transporte</c:when>
                                                            <c:when test="${tipo=='otros'}"> Otros Costos</c:when>
                                                            <c:otherwise>Costo Total</c:otherwise>
                                                        </c:choose>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Tipo de Formaci&oacute;n</th>
                                                    <th>
                                                        <c:choose>
                                                            <c:when test="${tipo=='matricula'}"> Costo de Matr�cula</c:when>
                                                            <c:when test="${tipo=='transporte'}"> Costo de Transporte</c:when>
                                                            <c:when test="${tipo=='otros'}"> Otros Costos</c:when>
                                                            <c:otherwise>Costo Total</c:otherwise>
                                                        </c:choose>
                                                    </th>
                                                </tr>
                                            </tfoot>
                                            <c:if test="${resultOk}">
                                                <tbody>
                                                    <tr> 
                                                        <td>Competencias claves y transversales</td>
                                                        <td id="val19" align="right">
                                                            <c:forEach items="${resultList}" var="item" begin="0">
                                                                <c:if test="${item.tipoCapNombre == 'Comp claves y transversales'}">
                                                                    <c:choose>
                                                                        <c:when test="${tipo=='matricula'}">$${item.enrollmentCost}</c:when>
                                                                        <c:when test="${tipo=='transporte'}">$${item.transportCost}</c:when>
                                                                        <c:when test="${tipo=='otros'}">$${item.otherCost}</c:when>
                                                                        <c:otherwise>$${item.totalCost}</c:otherwise>
                                                                    </c:choose>															
                                                                </c:if>
                                                            </c:forEach>
                                                        </td>
                                                    </tr>
                                                    <tr> 
                                                        <td>M�dulo TICS</td>
                                                        <td id="val20" align="right">
                                                            <c:forEach items="${resultList}" var="item" begin="0">
                                                                <c:if test="${item.tipoCapNombre == 'M�dulo TICS'}">
                                                                    <c:choose>
                                                                        <c:when test="${tipo=='matricula'}">$${item.enrollmentCost}</c:when>
                                                                        <c:when test="${tipo=='transporte'}">$${item.transportCost}</c:when>
                                                                        <c:when test="${tipo=='otros'}">$${item.otherCost}</c:when>
                                                                        <c:otherwise>$${item.totalCost}</c:otherwise>
                                                                    </c:choose>															
                                                                </c:if>
                                                            </c:forEach>
                                                        </td>
                                                    </tr>
                                                    <tr> 
                                                        <td>Validaci�n bachillerato</td>
                                                        <td id="val21" align="right">
                                                            <c:forEach items="${resultList}" var="item" begin="0">
                                                                <c:if test="${item.tipoCapNombre == 'Validaci�n bachillerato'}">
                                                                    <c:choose>
                                                                        <c:when test="${tipo=='matricula'}">$${item.enrollmentCost}</c:when>
                                                                        <c:when test="${tipo=='transporte'}">$${item.transportCost}</c:when>
                                                                        <c:when test="${tipo=='otros'}">$${item.otherCost}</c:when>
                                                                        <c:otherwise>$${item.totalCost}</c:otherwise>
                                                                    </c:choose>															
                                                                </c:if>
                                                            </c:forEach>
                                                        </td>
                                                    </tr>
                                                    <tr> 
                                                        <td>T�cnico laboral</td>
                                                        <td id="val22" align="right">
                                                            <c:forEach items="${resultList}" var="item" begin="0">
                                                                <c:if test="${item.tipoCapNombre == 'T�cnico laboral'}">
                                                                    <c:choose>
                                                                        <c:when test="${tipo=='matricula'}">$${item.enrollmentCost}</c:when>
                                                                        <c:when test="${tipo=='transporte'}">$${item.transportCost}</c:when>
                                                                        <c:when test="${tipo=='otros'}">$${item.otherCost}</c:when>
                                                                        <c:otherwise>$${item.totalCost}</c:otherwise>
                                                                    </c:choose>		
                                                                </c:if>
                                                            </c:forEach>
                                                        </td>
                                                    </tr>
                                                    <tr> 
                                                        <td>Certificaci�n de competencias</td>
                                                        <td id="val23" align="right">
                                                            <c:forEach items="${resultList}" var="item" begin="0">
                                                                <c:if test="${item.tipoCapNombre == 'Certificaci�n de competencias'}">
                                                                    <c:choose>
                                                                        <c:when test="${tipo=='matricula'}">$${item.enrollmentCost}</c:when>
                                                                        <c:when test="${tipo=='transporte'}">$${item.transportCost}</c:when>
                                                                        <c:when test="${tipo=='otros'}">$${item.otherCost}</c:when>
                                                                        <c:otherwise>$${item.totalCost}</c:otherwise>
                                                                    </c:choose>																	
                                                                </c:if>
                                                            </c:forEach>
                                                        </td>
                                                    </tr>
                                                    <tr> 
                                                        <td>Reentrenamiento t�cnico</td>
                                                        <td id="val24" align="right">
                                                            <c:forEach items="${resultList}" var="item" begin="0">
                                                                <c:if test="${item.tipoCapNombre == 'Reentrenamiento t�cnico'}">
                                                                    <c:choose>
                                                                        <c:when test="${tipo=='matricula'}">$${item.enrollmentCost}</c:when>
                                                                        <c:when test="${tipo=='transporte'}">$${item.transportCost}</c:when>
                                                                        <c:when test="${tipo=='otros'}">$${item.otherCost}</c:when>
                                                                        <c:otherwise>$${item.totalCost}</c:otherwise>
                                                                    </c:choose>																	
                                                                </c:if>
                                                            </c:forEach>
                                                        </td>
                                                    </tr>   
                                                </tbody> 
                                            </c:if>    
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        </div><!-- /.col-xs-12 main -->
    </div><!--/.container-->
    <div class="clearfix"></div> 
    <div class="modal fade" id="graphicModal" tabindex="-1" role="dialog" aria-labelledby="graphicModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12  " id="graphicContainer2">
                            <h4 class="modal-title" id="graphicModalLabel"><Strong><center>${titulo}</center></Strong></h4>
                            <div id="graphicContainer" ></div>
                            <br>
                            <div id="legend" class="graphic-legend inline-block"></div>
                            <br> <br> <br>
                            <Strong> ${titulografica}</Strong>
                            <div class=" table-responsive col-md-12">
                                <table id="tablag" class=" table-striped table-bordered " cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <c:if test="${not empty titleList}">
                                                <c:forEach items="${titleList}" var="info" begin="0">
                                                    <th>${info}</th>
                                                </c:forEach>  
                                            </c:if>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:if test="${not empty graphicList}">
                                            <c:forEach items="${graphicList}" var="item" begin="0">
                                                <tr> 
                                                    <td>${item.tipoCapNombre}</td>
                                                    <td class="val" align="right">${item.enrollmentCost}</td>
                                                    <td class="val" align="right">${item.transportCost}</td>
                                                    <td class="val" align="right">${item.otherCost}</td>
                                                </tr>   
                                            </c:forEach>  
                                        </c:if>
                                        <c:if test="${not empty graphicList2}">
                                            <c:forEach items="${graphicList2}" var="item" begin="0">
                                                <tr>
                                                    <td>${item.tipoCapNombre}</td>
                                                    <td class="val" align="right">
                                                        <c:if test="${tipo=='matricula'}">    
                                                            ${item.enrollmentCost}
                                                        </c:if>
                                                        <c:if test="${tipo=='transporte'}">   
                                                            ${item.transportCost}
                                                        </c:if>
                                                        <c:if test="${tipo=='otros'}">   
                                                            ${item.otherCost}
                                                        </c:if>
                                                    </td>
                                                </tr>   
                                            </c:forEach>  
                                        </c:if>
                                    </tbody> 
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="printGraphic()">Exportar PDF</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <!--Footer -->
    <div class="footer">
        <img src="resources/images/bg_banderin.jpg" width="100%"/> 
        <div class="clearfix"></div> 
    </div>
    <!--Ends footer -->

    <!-- jQuery -->
    <script src="resources/js/jquery-1.12.3.js"></script>
    <script src="resources/js/jquery.dataTables.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/dataTables.buttons.min.js"></script>
    <script src="resources/js/buttons.flash.min.js"></script>
    <script src="resources/js/jszip.min.js"></script>
    <script src="resources/js/pdfmake.min.js"></script>
    <script src="resources/js/vfs_fonts.js"></script>
    <script src="resources/js/buttons.html5.min.js"></script>
    <script src="resources/js/buttons.print.min.js"></script>
    <script src="resources/js/dataTables.bootstrap.min.js"></script>
    <script src="resources/js/bootstrap-datepicker.js"></script>
    <script src="resources/js/bootstrap-confirmation.js"></script>
    <script src="resources/js/jquery.validate.min.js"></script>
    <script src="resources/js/tooltipster.bundle.js"></script>
    <script src="resources/js/custom-functionalities.js"></script>
    <script src="resources/js/raphael.min.js"></script>
    <script src="resources/js/morris.js"></script>
    <script src="resources/js/xepOnline.js"></script>
    <script src="resources/js/moment.js"></script>
    <script>
        
        $(document).ready(function () {
            
            if($("#activateBtn").val() === ""){
                $(".desableBtn").attr("disabled", "desabled");
            } else {
                $(".desableBtn").removeAttr("disabled");
            }

            $('.containerMsn').hide();

            $(${tab}).tab('show');
            $.fn.datepicker.dates['es'] = {
                days: ["Domingo", "Lunes", "Martes", "Mi�rcoles", "Jueves", "Viernes", "S�bado", "Domingo"],
                daysShort: ["Dom", "Lun", "Mar", "Mi�", "Jue", "Vie", "S�b", "Dom"],
                daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                today: "Hoy"
            };
            $('#fechaFin').datepicker({language: "es", autoclose: true});
            $('#fechaIni').datepicker({language: "es", autoclose: true});
            $('#fechaFin2').datepicker({language: "es", autoclose: true});
            $('#fechaIni2').datepicker({language: "es", autoclose: true});
            $('.table').DataTable({
                "language": {
                    "url": "resources/js/Spanish.json"
                },
                "initComplete": function (settings, json) {
                    $(".dt-buttons").each(function (index) {
                        console.log(index + ": " + $(this).text());
                        if ($(this).find('.exportar').length === 0) {
                            $(this).prepend('<label class="exportar">Exportar a:&nbsp;&nbsp;</label>');
                        }
                    });
                },
                "bFilter": true,
                "bInfo": false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'csv',
                        fieldBoundary: '',
                        text: '<i class="fa fa-file-text-o"></i>',
                        titleAttr: 'CSV'

                    },
                    {
                        extend: 'excel',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        titleAttr: 'EXCEL'
                    },
                    {
                        extend: 'pdf',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        titleAttr: 'PDF'

                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-files-o"></i>',
                        titleAttr: 'COPY'
                    }

                ]

            });

            $('#tablag').DataTable({
                "language": {
                    "url": "resources/js/Spanish.json"
                },
                dom: 'lt',
            });
            
            <c:if test="${not empty internalMsg}">
                $('#creationModal').modal('show');
            </c:if>
                
            $('#graphicModal').on('shown.bs.modal', function () {
                $(function () {
                    var json = '<c:out value="${graphicData}" escapeXml="false"/>';
                    var type = '<c:out value="${graphicType}" escapeXml="false"/>';
                    var ykeys = [];
                    var labels = [];
                    var lineColors = [];
                    var barColors = [];
                        
                    <c:forEach items="${yKeys}" var="yKey" begin="0">
                        ykeys.push('<c:out value="${yKey}" escapeXml="false"/>');
                    </c:forEach>
                    <c:forEach items="${labels}" var="labels" begin="0">
                        labels.push('<c:out value="${labels}" escapeXml="false"/>');
                    </c:forEach>
                    <c:forEach items="${barColors}" var="barColors" begin="0">
                        barColors.push('<c:out value="${barColors}" escapeXml="false"/>');
                    </c:forEach>

                    $("#graphicContainer").empty();
                    $('#legend').empty();
                    if (type === 'B') {
                        var browsersChart = Morris.Bar({
                            element: 'graphicContainer',
                            data: jQuery.parseJSON(json),
                            xkey: '<c:out value="${xKey}" escapeXml="false"/>',
                            ykeys: ykeys,
                            labels: labels,
                            stacked: false,
                            hideHover: 'auto',
                            barGap: 4,
                            barColors: barColors,
                            barSizeRatio: 0.55,
                            xLabelAngle: 45,
                            resize: true
                        });
                        browsersChart.options.data.forEach(function (labels, i) {
                            var legendItem = $('<span></span>').text(browsersChart.options.labels[i]).prepend('<br><span>&nbsp;</span>');
                            legendItem.find('span')
                                    .css('backgroundColor', browsersChart.options.barColors[i])
                                    .css('width', '20px')
                                    .css('display', 'inline-block')
                                    .css('margin', '5px');
                            $('#legend').append(legendItem)
                        });
                    }
                });
            });

            if($.trim($("#val1").text()) === ""){$("#val1").text("$0");} else {$("#val1").text("$" + number_format($.trim($("#val1").text())));}
            if($.trim($("#val2").text()) === ""){$("#val2").text("$0");} else {$("#val2").text("$" + number_format($.trim($("#val2").text())));}
            if($.trim($("#val3").text()) === ""){$("#val3").text("$0");} else {$("#val3").text("$" + number_format($.trim($("#val3").text())));}
            if($.trim($("#val4").text()) === ""){$("#val4").text("$0");} else {$("#val4").text("$" + number_format($.trim($("#val4").text())));}
            if($.trim($("#val5").text()) === ""){$("#val5").text("$0");} else {$("#val5").text("$" + number_format($.trim($("#val5").text())));}
            if($.trim($("#val6").text()) === ""){$("#val6").text("$0");} else {$("#val6").text("$" + number_format($.trim($("#val6").text())));}
            if($.trim($("#val7").text()) === ""){$("#val7").text("$0");} else {$("#val7").text("$" + number_format($.trim($("#val7").text())));}
            if($.trim($("#val8").text()) === ""){$("#val8").text("$0");} else {$("#val8").text("$" + number_format($.trim($("#val8").text())));}
            if($.trim($("#val9").text()) === ""){$("#val9").text("$0");} else {$("#val9").text("$" + number_format($.trim($("#val9").text())));}
            if($.trim($("#val10").text()) === ""){$("#val10").text("$0");} else {$("#val10").text("$" + number_format($.trim($("#val10").text())));}
            if($.trim($("#val11").text()) === ""){$("#val11").text("$0");} else {$("#val11").text("$" + number_format($.trim($("#val11").text())));}
            if($.trim($("#val12").text()) === ""){$("#val12").text("$0");} else {$("#val12").text("$" + number_format($.trim($("#val12").text())));}
            if($.trim($("#val13").text()) === ""){$("#val13").text("$0");} else {$("#val13").text("$" + number_format($.trim($("#val13").text())));}
            if($.trim($("#val14").text()) === ""){$("#val14").text("$0");} else {$("#val14").text("$" + number_format($.trim($("#val14").text())));}
            if($.trim($("#val15").text()) === ""){$("#val15").text("$0");} else {$("#val15").text("$" + number_format($.trim($("#val15").text())));}
            if($.trim($("#val16").text()) === ""){$("#val16").text("$0");} else {$("#val16").text("$" + number_format($.trim($("#val16").text())));}
            if($.trim($("#val17").text()) === ""){$("#val17").text("$0");} else {$("#val17").text("$" + number_format($.trim($("#val17").text())));}
            if($.trim($("#val18").text()) === ""){$("#val18").text("$0");} else {$("#val18").text("$" + number_format($.trim($("#val18").text())));}
            if($.trim($("#val19").text()) === ""){$("#val19").text("$0");} else {$("#val19").text("$" + number_format($.trim($("#val19").text())));}
            if($.trim($("#val20").text()) === ""){$("#val20").text("$0");} else {$("#val20").text("$" + number_format($.trim($("#val20").text())));}
            if($.trim($("#val21").text()) === ""){$("#val21").text("$0");} else {$("#val21").text("$" + number_format($.trim($("#val21").text())));}
            if($.trim($("#val22").text()) === ""){$("#val22").text("$0");} else {$("#val22").text("$" + number_format($.trim($("#val22").text())));}
            if($.trim($("#val23").text()) === ""){$("#val23").text("$0");} else {$("#val23").text("$" + number_format($.trim($("#val23").text())));}
            if($.trim($("#val24").text()) === ""){$("#val24").text("$0");} else {$("#val24").text("$" + number_format($.trim($("#val24").text())));}

            $(".val").each(function(){
                $(this).text("$" + number_format($.trim($(this).text())));
            });

        });
                        
        function borraGrafico() {

            $("#graphicContainer2").empty();
            $(".desableBtn").attr("disabled", "desabled");
        }

        function printGraphic() {
            var objeto = document.getElementById('graphicContainer2');
            var ventana = window.open('', '_blank');
            ventana.document.write(objeto.innerHTML); 
            ventana.document.close();
            ventana.print();
            ventana.close();
        }

        function anioObligatorio(){

            if($('#anio1').val() === ""){
                $('#anio1').attr("required", "required");
                $('#anio1').attr("oninvalid", "setCustomValidity('Si selecciona un mes debe seleccionar tambi�n un a�o')");
                $('#anio1').attr("oninput", "setCustomValidity('')");

            }

            if ($('#mes1').val() === ""){
                $('#anio1').removeAttr("required");
                $('#anio1').attr("oninvalid", "setCustomValidity('')");
            }  
        }

        function anioObligatorio2(){

            if($('#anio2').val() === ""){
                $('#anio2').attr("required", "required");
                $('#anio2').attr("oninvalid", "setCustomValidity('Si selecciona un mes debe seleccionar tambi�n un a�o')");
                $('#anio2').attr("oninput", "setCustomValidity('')");
            }

            if ($('#mes2').val() === ""){
                $('#anio2').removeAttr("required");
                $('#anio2').attr("oninvalid", "setCustomValidity('')");
            }  
        }

        function anioAutiObl1(){

            if($('#mes1').val() !== ""){
                $('#anio1').attr("required", "required");
                $('#anio1').attr("oninvalid", "setCustomValidity('Si selecciona un mes debe seleccionar tambi�n un a�o')");
                $('#anio1').attr("oninput", "setCustomValidity('')");
            }else{
                $('#anio1').removeAttr("required");
                $('#anio1').attr("oninvalid", "setCustomValidity('')");
            }  
        }

        function anioAutiObl2(){

            if($('#mes2').val() !== ""){
                $('#anio2').attr("required", "required");
                $('#anio2').attr("oninvalid", "setCustomValidity('Si selecciona un mes debe seleccionar tambi�n un a�o')");
                $('#anio2').attr("oninput", "setCustomValidity('')");
            }else{
                $('#anio2').removeAttr("required");
                $('#anio2').attr("oninvalid", "setCustomValidity('')");
            }  
        }

        function number_format(amount, decimals) {

            amount += '';
            amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));

            decimals = decimals || 0;

            if (isNaN(amount) || amount === 0){ 
                return parseFloat(0).toFixed(decimals);
            }

            amount = '' + amount.toFixed(decimals);

            var amount_parts = amount.split('.'),
                regexp = /(\d+)(\d{3})/;

            while (regexp.test(amount_parts[0])){
                amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
            }
            return amount_parts.join('.');
        }
        
    </script>
    <style>
        .buttons-csv{
            display: inline-block;
            background-image:url(resources/images/csv.png);
            cursor:pointer !important;
            width: 32px !important;
            height: 32px !important;
            border: none !important;

        }
        .buttons-csv span{
            opacity: 0;
        }
        .buttons-excel{
            display: inline-block;
            background-image:url(resources/images/excel.png);
            cursor:pointer !important;
            width: 32px !important;
            height: 32px !important;
            border: none !important;

        }
        .buttons-excel span{
            opacity: 0;
        }
        .buttons-pdf{
            display: inline-block;
            background-image:url(resources/images/pdf.png);
            cursor:pointer !important;
            width: 32px !important;
            height: 32px !important;
            border: none !important;

        }
        .buttons-pdf span{
            opacity: 0;
        }
        .buttons-print{
            display: inline-block;
            background-image:url(resources/images/print.png);
            cursor:pointer !important;
            width: 32px !important;
            height: 32px !important;
            border: none !important;
        }
        .buttons-print span{
            opacity: 0;
        }
        div.dt-buttons {
            float: right;
            margin-left:10px;
        }
        .graphic-legend > span {
            display: inline-block;
            margin-right: 25px;
            margin-bottom: 10px;
            font-size: 13px;
        }
        .graphic-legend > span:last-child {
            margin-right: 0;
        }

        #graphicContainer {
            max-height: 280px;
            margin-top: 20px;
            margin-bottom: 20px;
        }

        .graphic-legend > span > i {
            display: inline-block;
            width: 15px;
            height: 15px;
            margin-right: 7px;
            margin-top: -3px;
            vertical-align: middle;
            border-radius: 1px;
        }

        #browsers_chart {
            max-height: 280px;
            margin-top: 20px;
            margin-bottom: 20px;
        }
    </style>
</body>
</html>
