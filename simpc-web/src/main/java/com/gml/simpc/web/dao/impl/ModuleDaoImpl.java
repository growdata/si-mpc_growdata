/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ModuleDaoImpl.java
 * Created on: 2016/12/14, 03:30:23 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.dao.impl;

import com.gml.simpc.api.dao.ModuleDao;
import com.gml.simpc.api.dto.ModuleDto;
import com.gml.simpc.api.utilities.Util;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * Dao para consultas de modulo.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Repository
public class ModuleDaoImpl implements ModuleDao {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(ModuleDaoImpl.class);

            
    /**
     * Consulta para encontrar los programas/modulos
     */
    private static final String FIND_PROGRAMAS_RANGO =
        "SELECT decode (cap.en_dominio,0,cap.codigo_programa,p.codigo) CODIGO_PROGRAMA," +
        "decode (cap.en_dominio,0,cap.nombre_programa,p.nombre) NOMBRE_PROGRAMA," +
        "i.nombre_institucion,tp.nombre TIPO_FORMACION  " +
        "FROM Md_f_cap_programa_modulo  cap " +
        "LEFT JOIN programas p ON cap.codigo_programa=p.codigo  " +
        "inner join instituciones i on cap.codigo_inst=i.id  " +
        "inner join tipos_programa tp " +
        "on (tp.id=decode(cap.en_dominio,0,cap.tipo_curso,p.tipo_capacitacion))  " +
        "WHERE cap.codigo_programa like  ? and  " +
        "CONVERT(LOWER ( decode(cap.en_dominio,0,cap.nombre_programa,p.nombre)),'US7ASCII') like   CONVERT(LOWER ( ? ) ,'US7ASCII')  and " +
        " CONVERT(LOWER (cap.nombre_modulo),'US7ASCII') like   CONVERT(LOWER ( ?) ,'US7ASCII')  and   " +
        " decode(cap.en_dominio,0,cap.tipo_curso,p.tipo_capacitacion) like ?  and  " +
        "cap.codigo_inst like ?  and  " +
        "cap.codigo_ccf like ?  and  " +
        "(cap.fecha_inicio  between  ? and    ? )  " +
        "    group by decode (cap.en_dominio,0,cap.codigo_programa,p.codigo), " +
        " decode (cap.en_dominio,0,cap.nombre_programa,p.nombre),i.nombre_institucion,tp.nombre";

    /**
     * Consulta para encontrar los programas/modulos con fecha INCIAL
     */
    private static final String FIND_PROGRAMAS_STARTDATE =
         "SELECT decode (cap.en_dominio,0,cap.codigo_programa,p.codigo) CODIGO_PROGRAMA," +
        "decode (cap.en_dominio,0,cap.nombre_programa,p.nombre) NOMBRE_PROGRAMA," +
        "i.nombre_institucion,tp.nombre TIPO_FORMACION  " +
        "FROM Md_f_cap_programa_modulo  cap " +
        "LEFT JOIN programas p ON cap.codigo_programa=p.codigo  " +
        "inner join instituciones i on cap.codigo_inst=i.id  " +
        "inner join tipos_programa tp " +
        "on (tp.id=decode(cap.en_dominio,0,cap.tipo_curso,p.tipo_capacitacion))  " +
        "WHERE cap.codigo_programa like  ? and  " +
        "CONVERT(LOWER ( decode(cap.en_dominio,0,cap.nombre_programa,p.nombre)),'US7ASCII') like   CONVERT(LOWER ( ? ) ,'US7ASCII')  and " +
        " CONVERT(LOWER (cap.nombre_modulo),'US7ASCII') like   CONVERT(LOWER ( ?) ,'US7ASCII')  and   " +
        " decode(cap.en_dominio,0,cap.tipo_curso,p.tipo_capacitacion) like ?  and  " +
        "cap.codigo_inst like ?  and  " +
        "cap.codigo_ccf like ?  and  " +
        "(cap.fecha_inicio  > ? )  " +
         "    group by decode (cap.en_dominio,0,cap.codigo_programa,p.codigo), " +
        " decode (cap.en_dominio,0,cap.nombre_programa,p.nombre),i.nombre_institucion,tp.nombre";

    /**
     * Consulta para encontrar los programas/modulos con fecha INCIAL
     */
    private static final String FIND_PROGRAMAS_ENDDATE =
        "SELECT decode (cap.en_dominio,0,cap.codigo_programa,p.codigo) CODIGO_PROGRAMA," +
        "decode (cap.en_dominio,0,cap.nombre_programa,p.nombre) NOMBRE_PROGRAMA," +
        "i.nombre_institucion,tp.nombre TIPO_FORMACION  " +
        "FROM Md_f_cap_programa_modulo  cap " +
        "LEFT JOIN programas p ON cap.codigo_programa=p.codigo  " +
        "inner join instituciones i on cap.codigo_inst=i.id  " +
        "inner join tipos_programa tp " +
        "on (tp.id=decode(cap.en_dominio,0,cap.tipo_curso,p.tipo_capacitacion))  " +
        "WHERE cap.codigo_programa like  ? and  " +
        "CONVERT(LOWER ( decode(cap.en_dominio,0,cap.nombre_programa,p.nombre)),'US7ASCII') like   CONVERT(LOWER ( ? ) ,'US7ASCII')  and " +
        " CONVERT(LOWER (cap.nombre_modulo),'US7ASCII') like   CONVERT(LOWER ( ?) ,'US7ASCII')  and   " +
        " decode(cap.en_dominio,0,cap.tipo_curso,p.tipo_capacitacion) like ?  and  " +
        "cap.codigo_inst like ?  and  " +
        "cap.codigo_ccf like ?  and  " +
        "(cap.fecha_inicio  < ? )  "+
         "    group by decode (cap.en_dominio,0,cap.codigo_programa,p.codigo), " +
        " decode (cap.en_dominio,0,cap.nombre_programa,p.nombre),i.nombre_institucion,tp.nombre";

    /**
     * Consulta para encontrar los programas/modulos con fecha INCIAL
     */
    private static final String FIND_PROGRAMAS =
        "SELECT decode (cap.en_dominio,0,cap.codigo_programa,p.codigo) CODIGO_PROGRAMA," +
        "decode (cap.en_dominio,0,cap.nombre_programa,p.nombre) NOMBRE_PROGRAMA," +
        "i.nombre_institucion,tp.nombre TIPO_FORMACION  " +
        "FROM Md_f_cap_programa_modulo  cap " +
        "LEFT JOIN programas p ON cap.codigo_programa=p.codigo  " +
        "inner join instituciones i on cap.codigo_inst=i.id  " +
        "inner join tipos_programa tp " +
        "on (tp.id=decode(cap.en_dominio,0,cap.tipo_curso,p.tipo_capacitacion))  " +
        "WHERE cap.codigo_programa like  ? and  " +
        "CONVERT(LOWER ( decode(cap.en_dominio,0,cap.nombre_programa,p.nombre)),'US7ASCII') like   CONVERT(LOWER ( ? ) ,'US7ASCII')  and " +
        " CONVERT(LOWER (cap.nombre_modulo),'US7ASCII') like   CONVERT(LOWER ( ?) ,'US7ASCII')  and   " +
        " decode(cap.en_dominio,0,cap.tipo_curso,p.tipo_capacitacion) like ?  and  " +
        "cap.codigo_inst like ?  and  " +
        "cap.codigo_ccf like ?  "+
         "    group by decode (cap.en_dominio,0,cap.codigo_programa,p.codigo), " +
        " decode (cap.en_dominio,0,cap.nombre_programa,p.nombre),i.nombre_institucion,tp.nombre";
       

   
    /**
     * Consulta parda encontrar los nombres de los mudulos y su detalle
     */
    private static final String FIND_MODULES =
        "SELECT m.codigo_programa,m.nombre_programa,m.codigo_modulo, s.nombre sede," +
        "m.nombre_modulo,m.fecha_inicio,m.fecha_fin,h.nombre,d.porcentaje_asistencia," +
        "m.costo_matricula,m.otros_costos FROM md_f_cap_programa_modulo m " +
        "INNER JOIN horarios h ON m.tipo_horario=h.id " +
        "LEFT JOIN md_f_programa_detalle d ON m.codigo_modulo=d.codigo_modulo  " +
        "LEFT JOIN sedes s on s.codigo=m.codigo_sede " +
        "WHERE m.codigo_programa= ? ";
    

    /**
     * Plantilla JDBC para consultar las propiedades.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    
    
    /**
     * Se realiza la consulta de programas/modulos teniendo en cuenta los filtros y solo fecha incio
     */
    @Override
    public List<ModuleDto> findModulesByFiltersStartDate(ModuleDto searchModules) {
        String programCode=Util.ceroValue(searchModules.getProgramCode());
        String programName=Util.likeValue(searchModules.getProgramName());
        String moduloName=Util.likeValue(searchModules.getModuleName());
        String fomationType=Util.nullValue(searchModules.getTrainingType());
        String institution=Util.nullValue(searchModules.getInstitution());
        String ccfCode=searchModules.getCodigo_ccf();
        Date startDate=searchModules.getStartDate();
       
    
        return this.jdbcTemplate.query(FIND_PROGRAMAS_STARTDATE, new RowMapper() {
            @Override
            public ModuleDto mapRow(ResultSet rs, int rownumber) throws
                SQLException {
                 ModuleDto e = new ModuleDto();
                e.setProgramCode(rs.getInt("CODIGO_PROGRAMA"));
                e.setProgramName(rs.getString("NOMBRE_PROGRAMA"));
                e.setInstitution(rs.getString("NOMBRE_INSTITUCION"));
                e.setTrainingType(rs.getString("TIPO_FORMACION"));

                return e;
            }
       },programCode,programName,moduloName,fomationType,institution,ccfCode,startDate);}
    
    /**
     * Se realiza la consulta de programas/modulos teniendo en cuenta los filtros y solo fecha final
     */
    @Override
    public List<ModuleDto> findModulesByFiltersEndDate(ModuleDto searchModules) {
         String programCode=Util.ceroValue(searchModules.getProgramCode());
        String programName=Util.likeValue(searchModules.getProgramName());
        String moduloName=Util.likeValue(searchModules.getModuleName());
        String fomationType=Util.nullValue(searchModules.getTrainingType());
        String institution=Util.nullValue(searchModules.getInstitution());
        String ccfCode=searchModules.getCodigo_ccf();
        Date endDate=searchModules.getEndDate();
    
        return this.jdbcTemplate.query(FIND_PROGRAMAS_ENDDATE, new RowMapper() {
            @Override
            public ModuleDto mapRow(ResultSet rs, int rownumber) throws
                SQLException {
                 ModuleDto e = new ModuleDto();
                e.setProgramCode(rs.getInt("CODIGO_PROGRAMA"));
                e.setProgramName(rs.getString("NOMBRE_PROGRAMA"));
                e.setInstitution(rs.getString("NOMBRE_INSTITUCION"));
                e.setTrainingType(rs.getString("TIPO_FORMACION"));

                return e;
            }
        },programCode,programName,moduloName,fomationType,institution,ccfCode,endDate);}
    
    /**
     * Se realiza la consulta de programas/modulos teniendo en cuenta los filtros y Rango de fechas
     */
    @Override
    public List<ModuleDto> findModulesByFiltersRange(ModuleDto searchModules) {
        String programCode=Util.ceroValue(searchModules.getProgramCode());
        String programName=Util.likeValue(searchModules.getProgramName());
        String moduloName=Util.likeValue(searchModules.getModuleName());
        String fomationType=Util.nullValue(searchModules.getTrainingType());
        String institution=Util.nullValue(searchModules.getInstitution());
        String ccfCode=searchModules.getCodigo_ccf();
        Date startDate=searchModules.getStartDate();
        Date endDate=searchModules.getEndDate();
       
        
        
    
        return this.jdbcTemplate.query(FIND_PROGRAMAS_RANGO, new RowMapper() {
            @Override
            public ModuleDto mapRow(ResultSet rs, int rownumber) throws
                SQLException {
                 ModuleDto e = new ModuleDto();
                e.setProgramCode(rs.getInt("CODIGO_PROGRAMA"));
                e.setProgramName(rs.getString("NOMBRE_PROGRAMA"));
                e.setInstitution(rs.getString("NOMBRE_INSTITUCION"));
                e.setTrainingType(rs.getString("TIPO_FORMACION"));

                return e;
            }
        },programCode,programName,moduloName,fomationType,institution,ccfCode,startDate,endDate);}
    
    
    
    /**
     * Se realiza la consulta de programas/modulos teniendo en cuenta los filtros y  sin fechas
     */
    @Override
    public List<ModuleDto> findModulesByFilters(ModuleDto searchModules) {
        String programCode=Util.ceroValue(searchModules.getProgramCode());
        String programName=Util.likeValue(searchModules.getProgramName());
        String moduloName=Util.likeValue(searchModules.getModuleName());
        String fomationType=Util.nullValue(searchModules.getTrainingType());
        String institution=Util.nullValue(searchModules.getInstitution());
        String ccfCode=searchModules.getCodigo_ccf();
       

  
        return this.jdbcTemplate.query(FIND_PROGRAMAS, new RowMapper() {
            @Override
            public ModuleDto mapRow(ResultSet rs, int rownumber) throws
                SQLException {
                 ModuleDto e = new ModuleDto();
                e.setProgramCode(rs.getInt("CODIGO_PROGRAMA"));
                e.setProgramName(rs.getString("NOMBRE_PROGRAMA"));
                e.setInstitution(rs.getString("NOMBRE_INSTITUCION"));
                e.setTrainingType(rs.getString("TIPO_FORMACION"));

                return e;
            }
         },programCode,programName,moduloName,fomationType,institution,ccfCode);}

   

    /**
     * Se realiza la consulta el detalle de los modulos de la ccf q
     * que consulta
     */
    @Override
    public List<ModuleDto> findModules(String id) {
       
        return this.jdbcTemplate.query(FIND_MODULES, new RowMapper() {
            @Override
            public ModuleDto mapRow(ResultSet rs, int rownumber) throws
                SQLException {
                ModuleDto e = new ModuleDto();
                e.setProgramCode(rs.getInt("CODIGO_PROGRAMA"));
                e.setProgramName(rs.getString("NOMBRE_PROGRAMA"));
                e.setModuleCode(rs.getString("CODIGO_MODULO"));
                e.setModuleName(rs.getString("NOMBRE_MODULO"));
                e.setStartDate(rs.getDate("FECHA_INICIO"));
                e.setEndDate(rs.getDate("FECHA_FIN"));
                e.setSchedule(rs.getString("NOMBRE"));
                e.setPercentageRequire(rs.getString("PORCENTAJE_ASISTENCIA"));
                e.setEnrollmentCost(rs.getDouble("COSTO_MATRICULA"));
                e.setOtherCost(rs.getDouble("OTROS_COSTOS"));
                e.setIntitutionHqCode(rs.getString("SEDE"));

                return e;
            }
        }, id);

    }
}
