/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: RequestInterceptor.java
 * Created on: 2016/11/28, 01:40:05 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.interceptor;

import com.gml.simpc.api.entity.Functionality;
import com.gml.simpc.api.entity.Session;
import com.gml.simpc.api.services.UserService;
import com.gml.simpc.commons.annotations.FunctionalityInterceptor;
import com.gml.simpc.commons.storage.SessionStorage;
import com.google.common.net.HttpHeaders;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import static com.gml.simpc.api.utilities.Util.SESSION_PERMISSIONS;
import static com.gml.simpc.api.utilities.Util.SESSION_USER;

/**
 * Interceptor de peticiones en los <code>Controller</code> de la
 * aplicaci&ocute;n.
 *
 * @author <a href="mailto:julianm@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
public class RequestInterceptor implements HandlerInterceptor {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(RequestInterceptor.class);
    /**
     * Lista de ventanas no interceptadas.
     */
    private static final List<String> NOT_INTERCEPTED_VIEWS = new LinkedList<>();
    /**
     * Servicio de <code>User</code>
     */
    @Autowired
    private UserService userService;

    static {
        NOT_INTERCEPTED_VIEWS.add("login.htm");
        NOT_INTERCEPTED_VIEWS.add("obtenerClave.htm");
        NOT_INTERCEPTED_VIEWS.add("activar-usuario.htm");
        NOT_INTERCEPTED_VIEWS.add("portal-prestaciones-economicas.htm");
        NOT_INTERCEPTED_VIEWS.add("modificarClave.htm");
        NOT_INTERCEPTED_VIEWS.add("banco-oferentes-portal.htm");
        NOT_INTERCEPTED_VIEWS.add("portal-capacitacion.htm");
        NOT_INTERCEPTED_VIEWS.add("portal-recursos-capacitacion.htm");
        NOT_INTERCEPTED_VIEWS.add("recursos-capacitacion-ccf.htm");
        NOT_INTERCEPTED_VIEWS.add("recursos-capacitacion-costos.htm");
        NOT_INTERCEPTED_VIEWS.add("buscarPrestacionesEconomicas.htm");
        NOT_INTERCEPTED_VIEWS.add("portal-gestion-y-colocacion.htm");
        NOT_INTERCEPTED_VIEWS.add("buscarGestionYColocacion.htm");
        NOT_INTERCEPTED_VIEWS.add("carga-externa.htm");
        NOT_INTERCEPTED_VIEWS.add("bancoInstituciones.htm");
        NOT_INTERCEPTED_VIEWS.add("bancoSedes");
        NOT_INTERCEPTED_VIEWS.add("bancoProgramas");
        NOT_INTERCEPTED_VIEWS.add("banco-fosfec-portal.htm");
        NOT_INTERCEPTED_VIEWS.add("recursosFosfec");
        NOT_INTERCEPTED_VIEWS.add("detalleCcf");
        NOT_INTERCEPTED_VIEWS.add("login-externo");
        NOT_INTERCEPTED_VIEWS.add("carga-externa");
		NOT_INTERCEPTED_VIEWS.add("sessionPing.htm");
        NOT_INTERCEPTED_VIEWS.add("setUserName.htm");
    }

    @Override
    public boolean preHandle(HttpServletRequest request,
        HttpServletResponse response, Object o) {

        try {
            Session session = (Session) request.getSession().
                getAttribute(SESSION_USER);

            if (!isInSession(session, request, response)) {
                return false;
            }

            if (!updateSession(request, response, session)) {
                return true;
            }

            return verifyFunctionalityPermission(request, response, o);
        } catch (Exception e) {
            LOGGER.info("Ocurrio un error al ejecutar [ preHandle ] ", e);
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request,
        HttpServletResponse response, Object o, ModelAndView mav) {
        response.addHeader(HttpHeaders.X_FRAME_OPTIONS, "SAMEORIGIN");
        response.addHeader(HttpHeaders.X_CONTENT_TYPE_OPTIONS, "nosniff");
        response.addHeader(HttpHeaders.X_XSS_PROTECTION, "1");
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
        HttpServletResponse response, Object o, Exception excptn) {

        SessionStorage.addValue((Session) request.getSession().
            getAttribute(SESSION_USER));
    }

    /**
     * Actualiza la sesi&oacute;n y saca al usuario si no se encuentra.
     *
     * @param request
     * @param response
     * @param user
     *
     * @throws IOException
     */
    private boolean updateSession(HttpServletRequest request,
        HttpServletResponse response, Session session) throws IOException {
        String url = request.getRequestURL().toString();

        if (isNotInterceptedUrl(url)) {
            return false;
        }

        Integer resultUpdateSession = userService.
            updateUserSession(session.getUser().getId());

        if (resultUpdateSession == 0) {
            LOGGER.info("La sesion del usuario esta vencida");
            request.getSession().invalidate();

            response.sendRedirect("/masterdata/login.htm");

            return false;
        }

        return true;
    }

    /**
     * Verifica si el usuario esta en sesi&oacute;n.
     *
     * @param session
     * @param request
     * @param response
     *
     * @return
     *
     * @throws IOException
     */
    private boolean isInSession(Session session, HttpServletRequest request,
        HttpServletResponse response) throws IOException {
        String url = request.getRequestURL().toString();

        if (session == null && !isNotInterceptedUrl(url)) {
            request.getSession().invalidate();
            response.sendRedirect("/masterdata/login.htm");

            return false;
        }

        if (session != null && url.contains("login.htm")) {
            response.sendRedirect("/masterdata/index.htm");
        }

        return true;
    }

    /**
     * Verifica si la url es o no interceptada.
     *
     * @param url
     *
     * @return
     */
    private boolean isNotInterceptedUrl(String url) {
        boolean result = false;

        for (String item : NOT_INTERCEPTED_VIEWS) {
            result = result || url.contains(item);
        }

        return result;
    }

    /**
     * Verifica si se tiene permiso de acceso a la funcionalidad.
     *
     * @param request
     * @param response
     * @param o
     *
     * @return
     *
     * @throws IOException
     */
    private boolean verifyFunctionalityPermission(HttpServletRequest request,
        HttpServletResponse response, Object o) throws IOException {
        HandlerMethod handlerMethod = (HandlerMethod) o;
        FunctionalityInterceptor annotation =
            handlerMethod.getMethod().getDeclaringClass().
            getAnnotation(FunctionalityInterceptor.class);

        if (annotation != null) {
            return searchFunctionality(request, annotation, response);
        }

        return true;
    }

    /**
     * Busca la funcionalidad en la lista de funcionalidades.
     *
     * @param request
     * @param annotation
     * @param response
     *
     * @return
     *
     * @throws IOException
     */
    private boolean searchFunctionality(HttpServletRequest request,
        FunctionalityInterceptor annotation, HttpServletResponse response)
        throws IOException {
        List<Functionality> permissions =
            (List<Functionality>) request.getSession().
            getAttribute(SESSION_PERMISSIONS);

        if (permissions != null) {
            for (Functionality functionality : permissions) {
                if (isAnnothedFunctionality(annotation, functionality)) {
                    return true;
                }
            }
        }

        LOGGER.info("El usuario no tiene permiso de " +
            "entrar a esa ventana");
        response.sendRedirect("/masterdata/403.xhtml");

        return false;
    }

    /**
     * Verifica si la funcionalidad es una de las anotadas.
     *
     * @param annotation
     * @param functionality
     *
     * @return
     */
    private boolean isAnnothedFunctionality(FunctionalityInterceptor annotation,
        Functionality functionality) {

        for (String name : annotation.name()) {
            if (name.equals(functionality.getPath())) {
                return true;
            }
        }

        return false;
    }
}
