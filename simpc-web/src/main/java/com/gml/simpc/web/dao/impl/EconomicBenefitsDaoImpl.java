/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: GraphicDaoImpl.java
 * Created on: 2016/12/27, 04:07:32 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.dao.impl;

import com.gml.simpc.api.dao.EconomicBenefitsDao;
import com.gml.simpc.api.dto.ConsolidantPostulantDto;
import com.gml.simpc.api.utilities.Util;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * Dao que define la forma en que se consulta la informaci&oacute;n de los
 * indicadores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Repository
public class EconomicBenefitsDaoImpl implements EconomicBenefitsDao {

    /**
     * Logger de la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(EconomicBenefitsDaoImpl.class);
    /**
     * Plantilla Jdbc para operar la base de datos.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Consulta para encontrar consolidado postulantes caso 1
     */
    private static final String FIND_CONSOLIDATED_POSTULANTS =
       " select t1.mes,aprobados,denegados,postulados  " +
        "from  " +
        "(select TO_CHAR(FECHA_VERIFICACION_POSTU,'yyyy') anio, " +
        "TO_CHAR(FECHA_VERIFICACION_POSTU,'mm')mes ,SUM(DECODE(VERIFICACION_REQUISITOS, 1, 1, 0)) APROBADOS ," +
        "SUM(DECODE(VERIFICACION_REQUISITOS, 2, 1, 0)) DENEGADOS " +
        " from " +
        "(select TIPO_IDENTIFICACION,NUMERO_IDENTIFICACION,VERIFICACION_REQUISITOS, " +
        "FECHA_VERIFICACION_POSTU,CODIGO_CCF,FECHA_POSTULACION_MPC  " +
        "from md_f_fosfec  group by TIPO_IDENTIFICACION,NUMERO_IDENTIFICACION,VERIFICACION_REQUISITOS," +
        "FECHA_VERIFICACION_POSTU,CODIGO_CCF,FECHA_POSTULACION_MPC)  " +
        "where TO_CHAR(FECHA_VERIFICACION_POSTU,'yyyy') like ? and  " +
        " TO_CHAR(FECHA_VERIFICACION_POSTU,'mm') like ? and  codigo_ccf like ?  " +
        "group by TO_CHAR(FECHA_VERIFICACION_POSTU,'yyyy'),TO_CHAR(FECHA_VERIFICACION_POSTU,'mm')  " +
        "order by 1,2) t1 " +
        " FULL OUTER JOIN ( " +
        "select TO_CHAR(FECHA_POSTULACION_MPC,'yyyy') anio,TO_CHAR(FECHA_POSTULACION_MPC,'mm')mes , " +
        "COUNT (  distinct NUMERO_IDENTIFICACION||NUMERO_IDENTIFICACION||VERIFICACION_REQUISITOS) POSTULADOS " +
        "from " +
        "(select TIPO_IDENTIFICACION,NUMERO_IDENTIFICACION,VERIFICACION_REQUISITOS, " +
        "FECHA_VERIFICACION_POSTU,CODIGO_CCF,FECHA_POSTULACION_MPC  " +
        "from md_f_fosfec  group by TIPO_IDENTIFICACION,NUMERO_IDENTIFICACION,  " +
        "VERIFICACION_REQUISITOS,FECHA_VERIFICACION_POSTU,CODIGO_CCF,FECHA_POSTULACION_MPC)t2  " +
        "where TO_CHAR(FECHA_POSTULACION_MPC,'yyyy') like ? and " +
        "  TO_CHAR(FECHA_POSTULACION_MPC,'mm') like ? and    codigo_ccf like ?  " +
        "group by TO_CHAR(FECHA_POSTULACION_MPC,'yyyy'),TO_CHAR(FECHA_POSTULACION_MPC,'mm')  " +
        "order by 1,2)t2 on t1.anio=t2.anio and t1.mes=t2.mes ";

    /**
     * Consulta detallada postulantes
     */
    private static final String FIND_DETAILED_POSTULANTS =
        "select fundaciones.nombre_corto NOMBRECCF," +
        "COUNT (  distinct NUMERO_IDENTIFICACION||NUMERO_IDENTIFICACION||VERIFICACION_REQUISITOS) POSTULADOS  " +
        "from  (select TIPO_IDENTIFICACION,NUMERO_IDENTIFICACION,VERIFICACION_REQUISITOS,FECHA_VERIFICACION_POSTU," +
        " CODIGO_CCF,FECHA_POSTULACION_MPC  from md_f_fosfec  group by TIPO_IDENTIFICACION,NUMERO_IDENTIFICACION,  " +
        "VERIFICACION_REQUISITOS,FECHA_VERIFICACION_POSTU,CODIGO_CCF,FECHA_POSTULACION_MPC)t2  " +
        "inner join fundaciones on fundaciones.codigo=CODIGO_CCF  " +
        "where TO_CHAR(FECHA_POSTULACION_MPC,'yyyy') like ? and   " +
        "TO_CHAR(FECHA_POSTULACION_MPC,'mm') like ? and codigo_ccf like  ? " +
        "group by fundaciones.nombre_corto  order by 2 desc";

    /**
     * Consulta Consolidada beneficiarios
     */
    private static final String FIND_CONSOLIDATED_BENEFITS =
   "select TO_CHAR(FECHA_VERIFICACION_POSTU,'mm')mes ,  " +
        "SUM(DECODE(VERIFICACION_REQUISITOS, 1, 1, 0)) Beneficiarios    " +
        "from   (select TIPO_IDENTIFICACION,NUMERO_IDENTIFICACION,VERIFICACION_REQUISITOS,    " +
        "FECHA_VERIFICACION_POSTU,CODIGO_CCF,FECHA_POSTULACION_MPC  from md_f_fosfec     " +
        "group by TIPO_IDENTIFICACION,NUMERO_IDENTIFICACION,VERIFICACION_REQUISITOS,FECHA_VERIFICACION_POSTU,   " +
        "CODIGO_CCF,FECHA_POSTULACION_MPC)     " +
        "where TO_CHAR(FECHA_VERIFICACION_POSTU,'yyyy') like ? and " +
        " TO_CHAR(FECHA_VERIFICACION_POSTU,'mm') like ?  and    " +
        "codigo_ccf like ?  group by TO_CHAR(FECHA_VERIFICACION_POSTU,'mm') order by 1";

    /**
     * Consulta Consolidada beneficiarios
     */
    private static final String FIND_DETAILED_BENEFITS =
    "select fundaciones.nombre_corto NOMBRECCF, " +
        "SUM(DECODE(VERIFICACION_REQUISITOS, 1, 1, 0)) BENEFICIARIOS from  " +
        "(select TIPO_IDENTIFICACION,NUMERO_IDENTIFICACION,VERIFICACION_REQUISITOS,  " +
        "FECHA_VERIFICACION_POSTU,CODIGO_CCF,FECHA_POSTULACION_MPC   " +
        "from md_f_fosfec  group by TIPO_IDENTIFICACION,NUMERO_IDENTIFICACION," +
        "VERIFICACION_REQUISITOS,FECHA_VERIFICACION_POSTU,CODIGO_CCF,FECHA_POSTULACION_MPC)   " +
        "inner join fundaciones on codigo=CODIGO_CCF  " +
        "where TO_CHAR(FECHA_VERIFICACION_POSTU,'yyyy') like  ? and " +
        "  TO_CHAR(FECHA_VERIFICACION_POSTU,'mm') like ?  and   codigo_ccf like ?  " +
        "group by fundaciones.nombre_corto  order by 2 desc";

    @Override
    public List<ConsolidantPostulantDto> getConsolidatedPostulants(String idCcf,
        String year, String month) {

        String query = FIND_CONSOLIDATED_POSTULANTS;
        return this.jdbcTemplate.query(query, new RowMapper() {
            @Override
            public ConsolidantPostulantDto mapRow(ResultSet rs, int rownumber)
                throws SQLException {
                ConsolidantPostulantDto e = new ConsolidantPostulantDto();
                e.setOrder(rs.getInt("MES") - 1);
                e.setMonth(Util.MONTHS_NAMES.get(rs.getInt("MES") - 1));
                e.setApprovedApplications(rs.getInt("APROBADOS"));
                e.setPostulants(rs.getInt("POSTULADOS"));
                e.setRequestsDenied(rs.getInt("DENEGADOS"));
                return e;
            }
        }, Util.nullValue(year), Util.nullValue(month),Util.nullValue(idCcf),
           Util.nullValue(year), Util.nullValue(month), Util.nullValue(idCcf));
    }

    @Override
    public List<ConsolidantPostulantDto> getDetailedPostulants(String idCcf,
        String year, String month) {
        String query = FIND_DETAILED_POSTULANTS;
        return this.jdbcTemplate.query(query, new RowMapper() {
            @Override
            public ConsolidantPostulantDto mapRow(ResultSet rs, int rownumber)
                throws SQLException {
                ConsolidantPostulantDto e = new ConsolidantPostulantDto();
                e.setMonth(rs.getString("NOMBRECCF"));
                e.setPostulants(rs.getInt("POSTULADOS"));
                return e;
            }
        },  Util.nullValue(year),Util.nullValue(month), Util.nullValue(idCcf));
    }

    @Override
    public List<ConsolidantPostulantDto> getConsolidatedBenefits(String idCcf,
        String year, String month) {

        String query = FIND_CONSOLIDATED_BENEFITS;
        return this.jdbcTemplate.query(query, new RowMapper() {
            @Override
            public ConsolidantPostulantDto mapRow(ResultSet rs, int rownumber)
                throws SQLException {
                ConsolidantPostulantDto e = new ConsolidantPostulantDto();
                e.setOrder(rs.getInt("MES") - 1);
                
                
                e.setMonth(Util.MONTHS_NAMES.get(rs.getInt("MES") - 1));
                e.setPostulants(rs.getInt("BENEFICIARIOS"));
                return e;
            }
        },  Util.nullValue(year),Util.nullValue(month), Util.nullValue(idCcf));
    }

    @Override
    public List<ConsolidantPostulantDto> getDetailedBenefits(String idCcf,
        String year, String month) {
        
        String query = FIND_DETAILED_BENEFITS;
        return this.jdbcTemplate.query(query, new RowMapper() {
            @Override
            public ConsolidantPostulantDto mapRow(ResultSet rs, int rownumber)
                throws SQLException {
                ConsolidantPostulantDto e = new ConsolidantPostulantDto();
                e.setMonth(rs.getString("NOMBRECCF"));
                e.setPostulants(rs.getInt("BENEFICIARIOS"));
                return e;
            }
        }, Util.nullValue(year), Util.nullValue(month), Util.nullValue(idCcf));

    }
}
