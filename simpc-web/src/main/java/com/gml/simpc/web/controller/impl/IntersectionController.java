/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IntersectionController.java
 * Created on: 2017/01/27, 11:24:20 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.gml.simpc.api.dto.BasicInformationDto;
import com.gml.simpc.api.dto.DependentDto;
import com.gml.simpc.api.dto.EducationLevelDto;
import com.gml.simpc.api.dto.InformalEducationDto;
import com.gml.simpc.api.dto.IntermediationDto;
import com.gml.simpc.api.dto.LanguageDto;
import com.gml.simpc.api.dto.OtherKnowledgeDto;
import com.gml.simpc.api.dto.PilaDto;
import com.gml.simpc.api.dto.PilaEmploymentServiceDto;
import com.gml.simpc.api.dto.ProfitDto;
import com.gml.simpc.api.dto.ProgramDto;
import com.gml.simpc.api.dto.WorkExperienceDto;
import com.gml.simpc.api.entity.Session;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.entity.exeption.UserException;
import com.gml.simpc.api.services.IndividualIntersectionService;
import com.gml.simpc.api.services.IntersectionService;
import com.gml.simpc.api.services.ParameterService;
import com.gml.simpc.api.utilities.Util;
import com.gml.simpc.commons.annotations.FunctionalityInterceptor;
import com.gml.simpc.web.controller.ControllerDefinition;
import com.gml.simpc.web.utilities.IntersectionFilesCreator;
import com.gml.simpc.web.utilities.ValueListService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_001_UNKNOW;
import static com.gml.simpc.api.utilities.Util.FAILED_ALERT;
import static com.gml.simpc.api.utilities.Util.ID_PARAMETRO_PATH_CRUCES;
import static com.gml.simpc.api.utilities.Util.SESSION_USER;
import static com.gml.simpc.api.utilities.Util.SUCCESS_ALERT;
import static com.gml.simpc.api.utilities.Util.DOCUMENT_NOT_FOUND;
import static com.gml.simpc.web.constants.WebConstants.INTERSECTION_CURRICULUM_VITAE_FILE_NAME;
import static com.gml.simpc.web.constants.WebConstants.INTERSECTION_DEPENDENTS_FILE_NAME;
import static com.gml.simpc.web.constants.WebConstants.INTERSECTION_FOSFEC_FILE_NAME;
import static com.gml.simpc.web.constants.WebConstants.INTERSECTION_INTERMEDIATION_FILE_NAME;
import static com.gml.simpc.web.constants.WebConstants.INTERSECTION_PILA_FILE_NAME;
import static com.gml.simpc.web.constants.WebConstants.INTERSECTION_TRAININGS_FILE_NAME;

/**
 * Controlador para la ventana de cruces.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@FunctionalityInterceptor(name = "cruces.htm")
@Controller
public class IntersectionController implements ControllerDefinition {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER
            = Logger.getLogger(IntersectionController.class);
    /**
     * Servicio de listas de valores.
     */
    @Autowired
    private ValueListService valueListService;

    @Autowired
    private IndividualIntersectionService individualIntersectionService;
    /**
     * Servicio de cruce.
     */
    @Autowired
    private IntersectionService intersectionService;
    /**
     * Componente de creaci&oacute;n de archivos de cruce.
     */
    @Autowired
    private IntersectionFilesCreator intersectionFilesCreator;
    /**
     * Repositorio de par&aacute;metros.
     */
    @Autowired
    private ParameterService parameterService;

    /**
     * 
     * Genera la pantalla inicial de cruces de informaci&oacute;n.
     * 
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = "cruces", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
            HttpServletResponse response) {
        LOGGER.info("Ejecutando metodo onDisplay en IntersectionController");

        ModelAndView modelAndView = new ModelAndView("cruces");

        Session session = (Session) request.getSession().
                getAttribute(SESSION_USER);
        User userSession = session.getUser();

        modelAndView.addObject("documentTypeList", this.valueListService.
                getDocumentTypeList());
        modelAndView.addObject("profileUserType", userSession.getProfile().
                getDescription());

        init(modelAndView, request);
        return modelAndView;
    }

    /**
     * 
     * M&eacute;todo para generar los cruces masivos.
     * 
     * @param rq
     * @param rs
     * @param file
     * @return 
     */
    @RequestMapping(value = "cruce-masivo", method = RequestMethod.POST)
    public ModelAndView loadFile(HttpServletRequest rq, HttpServletResponse rs,
            @RequestParam(name = "file") MultipartFile file) {
        LOGGER.info("Ejecutando metodo [ loadFile ]");

        ModelAndView modelAndView = new ModelAndView("cruces");

        try {
            String name = ((MultipartFile) file).getOriginalFilename();
            String ext = name.substring(name.lastIndexOf(".") + 1);

            if (!"txt".equals(ext)) {

                modelAndView.addObject("msgType", FAILED_ALERT);
                modelAndView.addObject("msg", "Por favor ingresar un archivo"
                        + " con extensi�n .txt");

            } else {

                Session session = (Session) rq.getSession().
                        getAttribute(SESSION_USER);
                User userSession = session.getUser();
                this.intersectionService.startIntersection(userSession,
                        file.getBytes(), file.getOriginalFilename());

                modelAndView.addObject("msgType", SUCCESS_ALERT);
                modelAndView.addObject("msg", "El archivo ha sido cargado "
                        + "exitosamente, se le notificar� v�a email cuando finalice el "
                        + "proceso.");
            }
        } catch (UserException ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msgType", FAILED_ALERT);
            modelAndView.addObject("msg", ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msgType", FAILED_ALERT);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("finally de loadFile en IntersectionController");
        }
        init(modelAndView, rq);
        return modelAndView;
    }

    /**
     * 
     * M&eacute;todo para generar los cruces individuales.
     * 
     * @param rq
     * @param rs
     * @return 
     */
    @RequestMapping(value = "cruce-individual", method
            = RequestMethod.POST)
    public ModelAndView loadIndividualResults(HttpServletRequest rq,
            HttpServletResponse rs) {
        LOGGER.info("Ejecutando metodo [ loadPilaResults ]");

        ModelAndView modelAndView = new ModelAndView("cruces");

        try {
            Session session = (Session) rq.getSession().
                    getAttribute(SESSION_USER);
            User userSession = session.getUser();

            String identificationNumber = null;

            if (rq.getParameter("numberIdentification") != null
                    && !"".equals(rq.getParameter("numberIdentification"))) {
                identificationNumber = rq.getParameter("numberIdentification");
            }

            String identificationType = null;

            if (rq.getParameter("identificationType") != null
                    && !"".equals(rq.getParameter("identificationType"))) {
                identificationType = rq.getParameter("identificationType");
            }

            Long timestamp = new Date().getTime();

            String path = this.parameterService.
                    findById(ID_PARAMETRO_PATH_CRUCES).getValue();

            path = path.concat(getIdUsuarioFromRequest(rq)).
                    concat("/" + timestamp);

            loadPilaResults(identificationType, identificationNumber, path,
                    modelAndView, isUserEmploymentPublicService(rq));
            loadIntermediationResults(identificationType, identificationNumber,
                    path, modelAndView);
            try {
                loadCurriculumVitaeResults(identificationType, identificationNumber,
                        path, modelAndView);
            } catch (Exception ex) {
                modelAndView.addObject("msg", DOCUMENT_NOT_FOUND + 
                        " " + identificationType + " - " + identificationNumber);
                modelAndView.addObject("msgType", FAILED_ALERT);
            } finally {
                LOGGER.info("Numero identificaci�n no encontrado en IntersectionController");
            }

            if (!"UAESPE".equals(userSession.getProfile().getName())) {
                loadFosfecResults(identificationType, identificationNumber, path,
                        modelAndView);
                loadDependentsResults(identificationType, identificationNumber,
                        path, modelAndView);
            }
            loadTrainingResults(identificationType, identificationNumber,
                    path, modelAndView);

            this.intersectionService.saveIndividualIntersection(userSession,
                    path);
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msgType", FAILED_ALERT);
            modelAndView.addObject("msg", ex);
        } finally {
            LOGGER.info("finally de loadFile en IntersectionController");
        }
        init(modelAndView, rq);
        return modelAndView;
    }

    /**
     * M&eacute;todo para cagar listas de pila.
     *
     * @param identificationType
     * @param identificationNumber
     * @param path
     * @param modelAndView
     * @param isUserOfEmpoymentPublicService
     */
    private void loadPilaResults(String identificationType,
            String identificationNumber, String path, ModelAndView modelAndView,
            Boolean isUserOfEmpoymentPublicService) {

        List<List<?>> dtoList = new ArrayList<>();
        List<Object> emptyDtoList = new ArrayList<>();

        if (isUserOfEmpoymentPublicService) {
            List<PilaEmploymentServiceDto> result
                    = this.individualIntersectionService.
                            getPilaByDocumentAndNumberTypePublicEmploymentService(
                                    identificationType, identificationNumber);
            modelAndView.addObject("resultsPilaEmploymentService", result);
            dtoList.add(result);
            emptyDtoList.add(new PilaEmploymentServiceDto());
        } else {
            List<PilaDto> resultsPila = this.individualIntersectionService.
                    getPilaByDocumentAndNumberType(identificationType,
                            identificationNumber);
            modelAndView.addObject("resultsPila", resultsPila);
            dtoList.add(resultsPila);
            emptyDtoList.add(new PilaDto());
        }

        this.intersectionFilesCreator.createFiles(INTERSECTION_PILA_FILE_NAME,
                path, dtoList, emptyDtoList);
    }

    /**
     * M&eacute;todo para cagar listas de dependientes.
     *
     * @param identificationType
     * @param identificationNumber
     * @param path
     * @param modelAndView
     */
    private void loadDependentsResults(String identificationType,
            String identificationNumber, String path, ModelAndView modelAndView) {

        List<DependentDto> result = this.individualIntersectionService.
                getDependentsByDocumentAndNumberType(identificationType,
                        identificationNumber);
        modelAndView.addObject("resultsDependents", result);

        List<List<?>> dtoList = new ArrayList<>();
        dtoList.add(result);

        List<Object> emptyDtoList = new ArrayList<>();
        emptyDtoList.add(new DependentDto());

        this.intersectionFilesCreator.
                createFiles(INTERSECTION_DEPENDENTS_FILE_NAME, path, dtoList,
                        emptyDtoList);
    }

    /**
     * M&eacute;todo para cagar listas de Orientaci&oacute;n y
     * capacitaci&oacute;n.
     *
     * @param identificationType
     * @param identificationNumber
     * @param path
     * @param modelAndView
     */
    private void loadTrainingResults(String identificationType,
            String identificationNumber, String path, ModelAndView modelAndView) {

        List<ProgramDto> resultsTrainings
                = this.individualIntersectionService.
                        getTrainingsByDocumentAndNumberType(identificationType,
                                identificationNumber);
        modelAndView.addObject("resultsTrainings", resultsTrainings);

        List<List<?>> dtoList = new ArrayList<>();
        dtoList.add(resultsTrainings);

        List<Object> emptyDtoList = new ArrayList<>();
        emptyDtoList.add(new ProgramDto());

        this.intersectionFilesCreator.
                createFiles(INTERSECTION_TRAININGS_FILE_NAME, path, dtoList,
                        emptyDtoList);
    }

    /**
     * M&eacute;todo para cagar listas pila.
     *
     * @param identificationType
     * @param identificationNumber
     * @param path
     * @param modelAndView
     */
    private void loadFosfecResults(String identificationType,
            String identificationNumber, String path, ModelAndView modelAndView) {
        List<ProfitDto> resultsFosfec = this.individualIntersectionService.
                getFosfecByDocumentAndNumberType(identificationType,
                        identificationNumber);

        List<List<?>> dtoList = new ArrayList<>();
        dtoList.add(resultsFosfec);

        List<Object> emptyDtoList = new ArrayList<>();
        emptyDtoList.add(new ProfitDto());

        this.intersectionFilesCreator.createFiles(INTERSECTION_FOSFEC_FILE_NAME,
                path, dtoList, emptyDtoList);
        modelAndView.addObject("resultsFosfec", resultsFosfec);
    }

    /**
     * M&eacute;todo para sacar el id del usuario de un httpRequest.
     * 
     * @param rq
     * @return 
     */
    private String getIdUsuarioFromRequest(HttpServletRequest rq) {
        Session session = (Session) rq.getSession().getAttribute(
                SESSION_USER);
        String idUsuario = session.getUser().getId().toString();

        return idUsuario;
    }

    /**
     * M&eacute;todo cargar lista de intermediaci&oacute;n.
     *
     * @param identificationType
     * @param identificationNumber
     * @param path
     * @param modelAndView
     */
    private void loadIntermediationResults(String identificationType,
            String identificationNumber, String path, ModelAndView modelAndView) {
        List<IntermediationDto> result
                = this.individualIntersectionService.
                        getIntermediationByDocumentAndNumberType(identificationType,
                                identificationNumber);

        List<List<?>> dtoList = new ArrayList<>();
        dtoList.add(result);

        List<Object> emptyDtoList = new ArrayList<>();
        emptyDtoList.add(new IntermediationDto());

        this.intersectionFilesCreator.
                createFiles(INTERSECTION_INTERMEDIATION_FILE_NAME, path, dtoList,
                        emptyDtoList);

        modelAndView.addObject("resultsIntermediation", result);
    }

    /**
     * M&eacute;todo para cargar listas de hojas de vida.
     *
     * @param identificationType
     * @param identificationNumber
     * @param path
     * @param modelAndView
     */
    private void loadCurriculumVitaeResults(String identificationType,
            String identificationNumber, String path, ModelAndView modelAndView) {

        Integer max = 0;
        List<BasicInformationDto> basicInformation
                = this.individualIntersectionService.
                        getBasicInformationByDocumentAndNumberType(identificationType,
                                identificationNumber);
        if (basicInformation.size() > max) {
            max = basicInformation.size();
        }

        List<EducationLevelDto> educationlevel
                = individualIntersectionService.
                        getEducationLevelByDocumentAndNumberType(identificationType,
                                identificationNumber);
        if (educationlevel.size() > max) {
            max = educationlevel.size();
        }

        List<InformalEducationDto> informalEducation
                = individualIntersectionService.
                        getInformalEducationByDocumentAndNumberType(identificationType,
                                identificationNumber);
        if (informalEducation.size() > max) {
            max = informalEducation.size();
        }

        List<LanguageDto> languages = this.individualIntersectionService.
                getLanguageByDocumentAndNumberType(identificationType,
                        identificationNumber);
        if (languages.size() > max) {
            max = languages.size();
        }

        List<OtherKnowledgeDto> otherKnowledges
                = this.individualIntersectionService.
                        getOtherKnowledgeByDocumentAndNumberType(identificationType,
                                identificationNumber);
        if (otherKnowledges.size() > max) {
            max = otherKnowledges.size();
        }

        List<WorkExperienceDto> workExperience
                = individualIntersectionService.
                        getWorkExperienceByDocumentAndNumberType(identificationType,
                                identificationNumber);
        if (workExperience.size() > max) {
            max = workExperience.size();
        }

        modelAndView.addObject("resultsBasicInformation", basicInformation);
        modelAndView.addObject("resultsEducationlevel", educationlevel);
        modelAndView.addObject("resultsInformalEducation", informalEducation);
        modelAndView.addObject("resultsLanguage", languages);
        modelAndView.addObject("resultsOtherKnowledge", otherKnowledges);
        modelAndView.addObject("resultsWorkExperience", workExperience);

        BasicInformationDto basicInfo = new BasicInformationDto();
        basicInfo.setDocumentType(basicInformation.get(0).getDocumentType());
        basicInfo.setNumberIdentification(basicInformation.get(0).getNumberIdentification());

        for (int i = basicInformation.size(); i < max; i++) {
            basicInformation.add(basicInfo);
        }
        for (int i = educationlevel.size(); i < max; i++) {
            educationlevel.add(new EducationLevelDto());
        }
        for (int i = informalEducation.size(); i < max; i++) {
            informalEducation.add(new InformalEducationDto());
        }
        for (int i = languages.size(); i < max; i++) {
            languages.add(new LanguageDto());
        }
        for (int i = otherKnowledges.size(); i < max; i++) {
            otherKnowledges.add(new OtherKnowledgeDto());
        }
        for (int i = workExperience.size(); i < max; i++) {
            workExperience.add(new WorkExperienceDto());
        }

        List<List<?>> dtoList = new ArrayList<>();
        dtoList.add(basicInformation);
        dtoList.add(educationlevel);
        dtoList.add(informalEducation);
        dtoList.add(languages);
        dtoList.add(otherKnowledges);
        dtoList.add(workExperience);

        List<Object> emptyDtoList = new ArrayList<>();
        emptyDtoList.add(new BasicInformationDto());
        emptyDtoList.add(new EducationLevelDto());
        emptyDtoList.add(new InformalEducationDto());
        emptyDtoList.add(new LanguageDto());
        emptyDtoList.add(new OtherKnowledgeDto());
        emptyDtoList.add(new WorkExperienceDto());

        this.intersectionFilesCreator.
                createFiles(INTERSECTION_CURRICULUM_VITAE_FILE_NAME, path, dtoList,
                        emptyDtoList);
    }

    /**
     * M&eacute;todo que valida si es usuario del servicio publico de empleo.
     *
     * @param rq
     *
     * @return
     */
    private Boolean isUserEmploymentPublicService(HttpServletRequest rq) {
        Session session = (Session) rq.getSession().
                getAttribute(SESSION_USER);
        User user = session.getUser();

        return "UAESPE".equals(user.getProfile().getName());
    }

    /**
     *
     * Inicia los objetos para el controler.
     *
     * @param modelAndView
     */
    private void init(ModelAndView modelAndView, HttpServletRequest request) {
        try {

            Session session = (Session) request.getSession().
                    getAttribute(SESSION_USER);
            User userSession = session.getUser();

            modelAndView.addObject("documentTypeList", this.valueListService.
                    getDocumentTypeList());
            modelAndView.addObject("profileUserType", userSession.getProfile().
                    getDescription());

            if (isUserEmploymentPublicService(request)) {
                modelAndView.addObject("usuarioServicioEmpleo", 1);
            }

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

        } finally {
            LOGGER.info("finally de init");
        }

    }
}
