/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: MailSender.java
 * Created on: 2016/10/28, 05:34:28 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.commons.mail.sender.impl;

import com.gml.simpc.api.entity.MailHistory;
import com.gml.simpc.api.entity.Parameter;
import com.gml.simpc.api.services.MailHistoryService;
import com.gml.simpc.api.services.ParameterService;
import com.gml.simpc.commons.mail.sender.Notificator;
import com.gml.simpc.commons.storage.velocity.VelocityExecutor;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import static com.gml.simpc.api.utilities.Util.PARAMETER_NAME_DOMAIN;
import static com.gml.simpc.api.utilities.Util.PARAMETER_SIGNATURE;
import static com.gml.simpc.api.utilities.Util.PARAMETER_SIGNATURE_POSITION;

/**
 * Funcionalidad para el env&icute;o de correos para la notificaci&ocute;n a los
 * usuarios
 * de plataforma
 *
 * @author <a href="mailto:julianm@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
@Service(value = "mailService")
public class MailSender implements Notificator {

    /**
     * Logger de la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(MailSender.class);
    /**
     * Encoding para el correo.
     */
    private static final String UTF8 = "UTF-8";
    /**
     * Propiedades para env&iacute;o de email.
     */
    private static final Properties PROPERTIES = new Properties();
    /**
     * Email desde el que se env&iacute;a el mensaje.
     */
    @Value("${mail.user}")
    private String from;
    /**
     * Password del email.
     */
    @Value("${mail.pass}")
    private String pass;

    /**
     * Host desde el cual se env&iacute;a el mensaje.
     */
    @Value("${mail.host}")
    private String host;

    /**
     * Variable que indica si se maneja o no SSL para la conexi&oacute;n.
     */
    @Value("${mail.has.ssl}")
    private boolean hasSSL;
    /**
     * Puerto SMTP.
     */
    @Value("${mail.port}")
    private int port;
    /**
     * Path de configuraci&oacute;n.
     */
    @Value("${APP_CONFIG}")
    private String configPath;
    /**
     * Servicio para guardar mails.
     */
    @Autowired
    private MailHistoryService mailHistoryService;
    /**
     * Servicio de par&aacute;metros.
     */
    @Autowired
    private ParameterService parameterService;

    /**
     * M&eacute;todo por el cual inicia el bean.
     */
    @PostConstruct
    public void init() {
        PROPERTIES.put("mail.smtp.host", this.host);

        if (this.hasSSL) {
            PROPERTIES.put("mail.smtp.socketFactory.port", this.port);
            PROPERTIES.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        }

        PROPERTIES.put("mail.smtp.auth", "true");
        PROPERTIES.put("mail.smtp.starttls.enable", "true");
        PROPERTIES.put("mail.smtp.port", this.port);
    }

    @Override
    public void sendNotification(String to, String htmlMessage, 
        String addressee, String subject) {
        LOGGER.info("Ejecutando metodo [ sendNotification ]");
        LOGGER.info("To: " + to);
        LOGGER.info("Message: " + htmlMessage);
        LOGGER.info("ton name " + addressee);
        LOGGER.info("subject: " + subject);

        Session session = Session.getInstance(PROPERTIES, new Authenticator() {

            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, pass);
            }
        });

        try {
            String mailTemplate = getMailTemplate();

            Map<String, Object> params = new HashMap<>();
            params.put("message", htmlMessage);
            params.put("host", getHost());
            params.put("todayDate", getTodayDate());
            params.put("fullName", addressee);
            params.put("signatureName", getSignatureName());
            params.put("signaturePosition", getSignaturePosition());

            mailTemplate = VelocityExecutor.applyTemplate(mailTemplate, params);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(this.from, "Master Data"));
            message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(to));
            message.setSubject(getSubject(subject));
            message.setText(mailTemplate, UTF8, "html");

            Transport.send(message);

            MailHistory mailHistory = new MailHistory(this.from, to,
                mailTemplate);

            this.mailHistoryService.save(mailHistory);
            LOGGER.info("Enviado y guardado.");
        } catch (Exception ex) {
            LOGGER.error("Error al enviar mensaje", ex);
        }
    }

    /**
     * Obtiene la plantilla de correo.
     *
     * @return
     *
     * @throws FileNotFoundException
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    private String getMailTemplate() throws FileNotFoundException,
        UnsupportedEncodingException, IOException {
        String path = this.configPath + "/simpc/correo.html";
        StringBuilder builder = new StringBuilder();

        try ( // Localizamos el archivo.
            FileInputStream file = new FileInputStream(path)) {
            InputStreamReader reader = new InputStreamReader(file, UTF8);
            
            // Cargamos el archivo.
            BufferedReader br = new BufferedReader(reader);
            
            // Leemos e imprimimos cada linea.
            String line;
            
            while ((line = br.readLine()) != null) {
                builder.append(line);
            }
        }

        return builder.toString();
    }

    /**
     * Obtiene el host configurado para el sistema.
     *
     * @return
     */
    private String getHost() {
        Parameter pam=this.parameterService.findByName(PARAMETER_NAME_DOMAIN);
       
        return pam.getValue();
    }

    /**
     * Obtiene la fecha del dia de hoy
     *
     * @return
     */
    private String getTodayDate() {
        SimpleDateFormat format = new SimpleDateFormat("EEEEEE, " +
            "dd 'de ' MMMMM 'de ' yyyy", new Locale("es", "ES"));    
        format.setTimeZone(TimeZone.getTimeZone("America/Lima"));

        return format.format(new Date()).toUpperCase();

    }
     /**
     * Obtiene los datos de la firma
     *
     * @return
     */
    private String getSignatureName() {
        return this.parameterService.
            findByName(PARAMETER_SIGNATURE).getValue();
    }

    /**
     * Obtiene los datos de la posicion de la firma
     *
     * @return
     */
    private String getSignaturePosition() {
        return this.parameterService.
            findByName(PARAMETER_SIGNATURE_POSITION).getValue();
    }

    /**
     * Obtiene los parametros de subjet
     *
     * @return
     */
    private String getSubject(String subject) {
        LOGGER.info("Subject**********: " + subject );
        return this.parameterService.
            findByName(subject).getValue();
    }

   

}
