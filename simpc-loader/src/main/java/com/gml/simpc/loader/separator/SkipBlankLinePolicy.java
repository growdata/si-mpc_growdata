/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CommonsValidator.java
 * Created on: 2016/11/11, 10:33:45 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 * Objetive: Define end of line.
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.separator;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.separator.SimpleRecordSeparatorPolicy;

public class SkipBlankLinePolicy extends SimpleRecordSeparatorPolicy {
    
     /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(SkipBlankLinePolicy.class);

    @Override
    public boolean isEndOfRecord(String line) {
        if (line.trim().isEmpty()) {
            return false;
        }
        return super.isEndOfRecord(line);
    }
    
    @Override
    public String postProcess(String record) {
        if (record == null || record.trim().isEmpty()) {
            return null;
        }
        return super.postProcess(record);
    }
}

