/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserController.java
 * Created on: 2016/10/19, 02:14:58 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gml.simpc.api.entity.Program;
import com.gml.simpc.api.entity.Session;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.entity.exeption.UserException;
import com.gml.simpc.api.entity.exeption.code.ErrorCode;
import com.gml.simpc.api.entity.valuelist.Certifications;
import com.gml.simpc.api.entity.valuelist.ProgramType;
import com.gml.simpc.api.enums.ProgramNecessity;
import com.gml.simpc.api.services.ProgramService;
import com.gml.simpc.web.controller.ControllerDefinition;
import com.gml.simpc.web.utilities.ProgramUpdateService;
import com.gml.simpc.web.utilities.ValueListService;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_001_UNKNOW;
import static com.gml.simpc.api.utilities.Util.FAILED_ALERT;
import static com.gml.simpc.api.utilities.Util.SESSION_USER;
import static com.gml.simpc.api.utilities.Util.SUCCESS_ALERT;

/**
 * Controlador para la ventana de manejo de programas.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Controller
public class ProgramController implements ControllerDefinition {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER
            = Logger.getLogger(ProgramController.class);

    /**
     * Servicio para adminisraci&oacute;n de programas intermediario para la
     * comunicaci&oacute;n con la persistencia.
     */
    @Autowired
    private ProgramService programService;

    /**
     * Servicio para la actualizacion de programas.
     */
    @Autowired
    private ProgramUpdateService programUpdateService;

    /**
     * Servicio para adminisraci&oacute;n de listas de valores.
     */
    @Autowired
    private ValueListService valueList;

    /**
     * Lista de programas.
     */
    private List<Program> programList;

    /**
     * Lista de programas.
     */
    private List<Certifications> certificationsList;

    /**
     * Inicializa el binder para formatear fechas.
     *
     * @param binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class,
                new CustomDateEditor(sdf, true));
    }
    
    /**
     * Servicio para adminisraci&oacute;n de consulta programas; Servicio de
     * programas intermediario para la comunicaci&oacute;n con la
     * persistencia.
     */

    /**
     * M&eacute;todo para obtener un programa.
     * 
     * @param request
     * @param response
     * @return
     * @throws UserException 
     */
    @RequestMapping(value = "obtener-programa",
        method = RequestMethod.GET)
    public @ResponseBody
    String onEditProfile(HttpServletRequest request,
        HttpServletResponse response) throws UserException {
        LOGGER.info("Ejecutando metodo [ onEditProfile ]");

        try {
            Program program = this.programService.
                findById(Long.parseLong(request.getParameter("id")));
            ObjectMapper mapper = new ObjectMapper();

            return mapper.writeValueAsString(program);
        } catch (Exception ex) {
            LOGGER.error("Error", ex);

            throw new UserException(ERR_001_UNKNOW);
        }
    }

    /**
     * M&eacute;todo que se ejecuta al abrir la ventana de index; Trae listas
     * necesarias para el funcionamiento de la ventana.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "programas",
            method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
            HttpServletResponse response) {
        LOGGER.info("Ejecutando metodo [ onDisplay ProgramController ]");
        ModelAndView modelAndView
                = new ModelAndView("programas");

        try {

            if (request.getParameter("id") != null) {
                Program program = programService.
                        findById(Long.parseLong(request.getParameter("id")));
                modelAndView.addObject("creationProgram", program);
                modelAndView.addObject("disableCodeEdit", true);

                init(modelAndView, true);
            } else {

                //modelAndView.addObject("certifiedRequired", true);
                init(modelAndView, false);
            }

        } catch (Exception ex) {
            LOGGER.error("Error on Display Programs", ex);
        }

        return modelAndView;

    }

    /**
     * M&eacute;todo que borra un programa
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "borrarPrograma", method = RequestMethod.POST)
    public ModelAndView delete(HttpServletRequest request,
            HttpServletResponse response) {
        LOGGER.info("entra a borrar en el controller de programas");
        ModelAndView modelAndView
                = new ModelAndView("programas");
        try {
            if (request.getParameter("id") != null) {
                Long programId = Long.parseLong(request.getParameter("id"));
                this.programService.
                        remove(this.programService.findById(programId));
            }
            modelAndView.addObject("msg", "Se ha eliminado el programa "
                    + "exitosamente.");
            modelAndView.addObject("msgType", SUCCESS_ALERT);

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msgType", FAILED_ALERT);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("finally de eliminar programas");
        }

        init(modelAndView, false);

        return modelAndView;
    }

    /**
     * M&eacute;todo que consulta los programas de acuerdo a lo diligenciado en
     * los filtros de formulario de consulta.
     *
     * @param request
     * @param response
     * @param searchProgram
     *
     * @return
     */
    @RequestMapping(value = "buscarProgramas",
            method = RequestMethod.POST)
    public ModelAndView search(HttpServletRequest request,
            HttpServletResponse response,
            @ModelAttribute("searchProgram") Program searchProgram) {

        ModelAndView modelAndView
                = new ModelAndView("programas");

        try {
            programList = this.programService.findByFilters(searchProgram);
            modelAndView.addObject("programList", programList);
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("finally de consultar en programController");
        }

        init(modelAndView, false);

        return modelAndView;
    }

    /**
     * M&eacute;etodo para actualizar un programa.
     * 
     * @param program
     * @param programId
     * @throws IOException 
     */
    private void updateProgram(Program program, Long programId)
            throws IOException {

        Program programEdition = programService.findById(programId);

        /*
         * Guardamos el programa
         */
        programEdition.setModificationDate(new Date());
        programEdition.setModificationUserId(program.getModificationUserId());
        programEdition.setQualityCertificate(program.getQualityCertificate());
//        programEdition.setCode(program.getCode());
        programEdition.setHours(program.getHours());
        programEdition.setNecessity(program.getNecessity());
        programEdition.setQualityCertificate(program.getQualityCertificate());
        programEdition.setCertification(program.
                getCertification());
        programEdition.setObservations(program.getObservations());
        programEdition.setType(program.getType());
        programEdition.setName(program.getName());
        programEdition.setWhichName(program.getWhichName());
        programEdition.setDeliveredCertification(program.
                getDeliveredCertification());
        programEdition.setCine(program.getCine());

        if ("NO".equals(programEdition.getQualityCertificate())) {
            programEdition.setCertification(null);
            programEdition.setWhichName(null);
        } else if (programEdition.getCertification().getId() != 7) {
            programEdition.setWhichName(null);
        }

        programService.update(programEdition);

    }

    /**
     * M&eacute;todo para crear un programa.
     * 
     * @param program
     * @throws IOException
     * @throws UserException 
     */
    private void createProgram(Program program)
            throws IOException, UserException {
        /*
         * Guardamos el programa
         */

        program.setCreationDate(new Date());
        programService.save(program);

    }

    /**
     * M&eacute;todo que guarda un programa.
     *
     * @param request
     * @param response
     * @param creationProgram
     * @return
     */
    @RequestMapping(value = "guardarPrograma",
            method = RequestMethod.POST)
    public ModelAndView save(HttpServletRequest request,
            HttpServletResponse response, @Valid @ModelAttribute(
                    "creationProgram"
            ) Program creationProgram) {
        Session session = (Session) request.getSession().getAttribute(
                SESSION_USER);
        User userSession = session.getUser();

        ModelAndView modelAndView = new ModelAndView("programas");
        boolean creationError = false;

        try {
            
            String idProgram = request.getParameter("captureID");
            
            if ("".equals(idProgram)) {
                creationProgram.setModificationUserId(userSession);
                creationProgram.setCreationUserId(userSession);
                creationProgram.setCcf(userSession.getCcf());

                if (creationProgram.getType().getId() == 4) {
                    creationProgram.setName("T�cnico Laboral en "
                            + creationProgram.getName());
                }
                createProgram(creationProgram);
            } else {
                Long programId = Long.parseLong(request.getParameter("captureID"));
                creationProgram.setModificationUserId(userSession);
                creationProgram.setCode(request.getParameter("captureID"));
                updateProgram(creationProgram, programId);
            }
            modelAndView.addObject("msg",
                    "Se ha guardado el programa exitosamente.");
            modelAndView.addObject("msgType", SUCCESS_ALERT);
        } catch (UserException uEx) {
            LOGGER.error("Error [ save ] 1", uEx);
            modelAndView.addObject("internalMsg", uEx.getMessage());
            modelAndView.addObject("creationProgram", creationProgram);
            modelAndView.addObject("certifiedRequired", true);
            modelAndView.addObject("msgType", FAILED_ALERT);
            creationProgram.setId(creationProgram.getId());

        } catch (DataIntegrityViolationException r) {
            LOGGER.error("Error [ save ] 2", r);
            if (r.toString().contains("NOMBRE_UNIQUE")) {
                creationProgram.setId(creationProgram.getId());
                modelAndView.addObject("msg",
                        ErrorCode.ERR_045_ERROR_PROGRAM_NAME_EXIST.getMessage());
                modelAndView.addObject("creationProgram", creationProgram);
                modelAndView.addObject("certifiedRequired", true);
                modelAndView.addObject("msgType", FAILED_ALERT);
            }
        } catch (ConstraintViolationException ex) {
            LOGGER.error("Error [ save ] 3", ex);

            StringBuilder builder = new StringBuilder("Alertas: ");
            for (ConstraintViolation error : ex.getConstraintViolations()) {
                builder.append(error.getMessage());
                builder.append("\n");
            }
            creationProgram.setId(null);
            modelAndView.addObject("internalMsg", builder.toString());

            modelAndView.addObject("creationProgram", creationProgram);
            modelAndView.addObject("certifiedRequired", true);
            modelAndView.addObject("msgType", FAILED_ALERT);

            creationError = !creationError;
        } catch (Exception ex) {
            LOGGER.error("Error [ save ] 4", ex);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
            modelAndView.addObject("msgType", FAILED_ALERT);
        } finally {
            LOGGER.info("finally save in ProgramController");
        }

        init(modelAndView, creationError);

        return modelAndView;
    }

    /**
     * M&eacute;todo para ingresar al detalle de un programa.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "detalle-programa",
            method = RequestMethod.GET)
    public ModelAndView detallePrograma(HttpServletRequest request,
            HttpServletResponse response) {
        LOGGER.info("entra a detalle programa controller");

        Long id = Long.parseLong(request.getParameter("id"));

        ModelAndView modelAndView
                = new ModelAndView("detalle-programa");

        try {
            Program parent = this.programService.findById(id);
            modelAndView.addObject("programDetailed", parent);
        } catch (Exception ex) {
            LOGGER.error("Error [ detallePrograma ]", ex);

            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
            modelAndView.addObject("msgType", FAILED_ALERT);
        } finally {
            LOGGER.info("finally de display sedes");
        }

        return modelAndView;
    }

    /**
     * Inicia los objetos para el controler.
     * 
     * @param modelAndView
     * @param creationError 
     */
    private void init(ModelAndView modelAndView, boolean creationError) {
        try {
            modelAndView.addObject("certificationsList",
                    this.valueList.getCertifications());
            modelAndView.addObject("deliveredCertificationList",
                    this.valueList.getDeliveredCertificationList());
            modelAndView.addObject("searchProgram", new Program());
            modelAndView.addObject("tiposPrograma",
                    this.valueList.getProgramTypeList());

            if (!creationError) {
                modelAndView.addObject("creationProgram", new Program(
                        new ProgramType()));
            }

            modelAndView.addObject("cines", this.valueList.getCineList());
            List<ProgramNecessity> necesidades = new ArrayList<>();
            necesidades.add(ProgramNecessity.NECESIDAD_DE_LA_EMPRESA);
            necesidades.add(ProgramNecessity.NECESIDAD_DEL_BUSCADOR);
            necesidades.add(ProgramNecessity.NECESIDAD_DEL_SECTOR_PRODUCTIVO);
            necesidades.add(ProgramNecessity.NINGUNA_DE_LAS_ANTERIORES);
            modelAndView.addObject("necesidades", necesidades);

        } catch (Exception ex) {
            LOGGER.error("Error [ init ]", ex);
        }
    }
}
