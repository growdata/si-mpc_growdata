/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ControllerDefinition.java
 * Created on: 2016/11/14, 01:32:24 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller;

import java.io.Serializable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

/**
 * Define el comportamiento de los controladores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;z</a>
 */
public interface ControllerDefinition extends Serializable {

    /**
     * M&eacute;todo que se inicia al invocar la vista.
     *
     * @param request
     * @param response
     *
     * @return
     */
    ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response);
}
