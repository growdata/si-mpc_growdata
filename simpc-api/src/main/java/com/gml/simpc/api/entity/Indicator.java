/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Indicator.java
 * Created on: 2016/12/12, 03:28:43 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import com.gml.simpc.api.enums.GraphicType;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Entidad que define la estructura de los indicadores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Table(name = "INDICADORES")
@Entity
public class Indicator implements Serializable {

    /**
     * ID del indicador.
     */
    @Id
    @Column(name = "ID", length = 10)
    private Long id;
    /**
     * Nombre del tipo de indicador.
     */
    @Column(name = "NOMBRE", length = 50)
    private String name;
    /**
     * Variable del indicador.
     */
    @Column(name = "VARIABLE", length = 50)
    private String var;
    /**
     * Descripci&oacute;n.
     */
    @Column(name = "DESCRIPCION", length = 200)
    private String description;
    /**
     * Lista de gr&aacute;ficos.
     */
    @OneToMany(mappedBy = "indicator", fetch = FetchType.EAGER)
    private List<IndicatorGraphic> graphics;
    /**
     * Tipos de gr&aacute;fico del indicador.
     */
    @Transient
    private Set<GraphicType> types;
    /**
     * Columnas para visualizar en la tabla de indicadores.
     */
    @Column(name = "COLUMNAS")
    private String columns;

    /**
     * Constructor por defecto.
     */
    public Indicator() {
        this.id = null;
    }

    /**
     * Constructor por nombre.
     *
     * @param name
     */
    public Indicator(String name) {
        this.name = name;
    }

    /**
     * Constructor por lista de gr&aacute;ficos.
     *
     * @param graphics
     */
    public Indicator(List<IndicatorGraphic> graphics) {
        this.graphics = graphics;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVar() {
        return var;
    }

    public void setVar(String var) {
        this.var = var;
    }

    public List<IndicatorGraphic> getGraphics() {
        return graphics;
    }

    public void setGraphics(List<IndicatorGraphic> graphics) {
        this.graphics = graphics;
    }

    public Set<GraphicType> getTypes() {
        return types;
    }

    public void setTypes(Set<GraphicType> types) {
        this.types = types;
    }

    public String getColumns() {
        return columns;
    }

    public void setColumns(String columns) {
        this.columns = columns;
    }
}
