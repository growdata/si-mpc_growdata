/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IndicatorRepository.java
 * Created on: 2016/12/16, 10:26:35 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository;

import com.gml.simpc.api.entity.IndicatorConfiguration;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Repositorio de indicadores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface IndicatorConfigurationRepository extends JpaRepository<IndicatorConfiguration, Long> {
    
    
    /**
     * Este query me trae los indicadores en orden
     *
     * @return
     */
    @Query(value = "select *  from ind_indicadores order by id",
            nativeQuery = true)
    
    List<IndicatorConfiguration> findIndicator();
    

}
