/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: FileException.java
 * Created on: 2017/03/09, 11:02:48 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity.exeption;

import com.gml.simpc.api.entity.exeption.code.FileErrorCode;

/**
 * Excepci&oacute;n dise�ada para saltar entre capas y disparar rollback de
 * transacciones.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class FileException extends RuntimeException {

    /**
     * C&oacute;digo de error.
     */
    private final FileErrorCode errorCode;

    /**
     * @param errorCode
     */
    public FileException(FileErrorCode errorCode) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
    }

    /**
     * @param errorCode
     * @param cause
     */
    public FileException(FileErrorCode errorCode, Throwable cause) {
        super(errorCode.getMessage(), cause);
        this.errorCode = errorCode;
    }
    
    /**
     * @param errorCode
     * @param params 
     */
    public FileException(FileErrorCode errorCode, Object ... params) {
        super(String.format(errorCode.getMessage(), params));
        this.errorCode = errorCode;
    }

    public FileErrorCode getFileErrorCode() {
        return errorCode;
    }
}