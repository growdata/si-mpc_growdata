/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: SupplierBankService.java
 * Created on: 2017/01/12, 03:17:00 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.dto.GraphicDataDto;
import com.gml.simpc.api.dto.TableDataDto;
import com.gml.simpc.api.dto.ValueListDto;
import com.gml.simpc.api.entity.Institution;
import com.gml.simpc.api.entity.valuelist.City;
import com.gml.simpc.api.entity.valuelist.InstitutionType;
import com.gml.simpc.api.entity.valuelist.OrientationType;
import com.gml.simpc.api.entity.valuelist.ProgramType;
import com.gml.simpc.api.entity.valuelist.State;
import java.util.List;

/**
 * Interface que define el servicio para manipular las consulatas del portal
 * banco de oferentes
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface SupplierBankService {

    /**
     * Obtiene los tipo de instituciones
     *
     *
     * @return
     */
    List<InstitutionType> getInstitutionTypes();

    /**
     * Obtiene las ciudades
     *
     *
     * @return
     */
    List<City> getCities();

    /**
     * Obtiene los estados
     *
     *
     * @return
     */
    List<State> getStates();

    /**
     * Obtiene los tipos de formacion para un programa-capacitacion
     *
     * @return
     */
    List<ProgramType> getFormationType();

    /**
     * Obtiene los tipos de formacion para un programa-orientacion
     *
     * @return
     */
    List<OrientationType> getOrientationType();

    /**
     * Obtiene lista de duraciones de programas
     *
     *
     * @return
     */
    List<ValueListDto> getDuration();

    /**
     * Obtiene las instituciones segun el filtro
     *
     * @return
     */
    List<Institution> getInstitutionsByTypeAndCityAndState(String type,
        String city, String state);

    /**
     * Obtiene los datos para el grafico segun el filtro
     *
     * @return
     */
    List<GraphicDataDto> graphicInstitutionsByTypeAndCityAndState(String type,
        String city, String state);

    /**
     * Obtiene los datos para mostrar con el filtro de sedes
     *
     * @return
     */
    List<TableDataDto> getHeadquarterByTypeAndCityAndState(String type,
        String city, String state);

    /**
     * Obtiene los datos para el grafico segun el filtro en sedes
     *
     * @return
     */
    List<GraphicDataDto> graphicHeadQuartersByTypeAndCityAndState(String type,
        String city, String state);

    /**
     * Obtiene los datos para mostrar con el filtro de sedes
     *
     * @return
     */
    List<TableDataDto> getProgramByTypeAndDurationAndModule(String formationType,
        String duration, String module);

    
}
