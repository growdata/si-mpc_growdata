<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css//bootstrap-toggle.min.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
    </head>
    <body>
        <c:if test="${not empty msg}">
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        <div class="col-sm-12">
            <h1>Aprobaci�n de Instituciones</h1>

            <div class="panel panel-default">
                <div class="panel-heading"><h2>Instituciones Pendientes de Aprobaci&oacute;n</h2></div>
                <div class="panel-body">
                    <div class=" table-responsive">
                        <table id="institutions" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>C�digo</th>
                                    <th>Nombre</th>
                                    <th>CCF de �ltima Actualizaci&oacute;n</th>
                                    <th>Fecha y Hora de �ltima Actualizaci&oacute;n</th>
                                    <th>Acciones</th>
                                </tr>

                            </thead>
                            <tfoot>
                                <tr>
                                    <th>C�digo</th>
                                    <th>Nombre</th>
                                    <th>CCF de �ltima Actualizaci&oacute;n</th>
                                    <th>Fecha y Hora de �ltima Actualizaci&oacute;n</th>
                                    <th>Acciones</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <c:if test="${not empty IntitutionList}">
                                    <c:forEach items="${IntitutionList}" var="institucion" begin="0">
                                        <c:if test="${institucion.status == 'A'}">
                                            <tr> 
                                                <td>${institucion.getId()}</td>
                                                <td>${institucion.getInstitutionName()}</td>
                                                <td>${institucion.getModificationUserId().getCcf().getName()}</td>
                                                <td>${institucion.getModificationDate()}</td>
                                                <td>
                                                <center>
                                                    <div class="btn-group" role="group" >
                                                        <button type="button" class="btn btn-info btn-xs"
                                                                data-toggle="modal" data-target="#ventanaDetalle"
                                                                onclick="loadInstitutionDetail(${institucion.getId()})">
                                                                <i class="fa fa-id-card-o" aria-hidden="true"></i> Ver
                                                        </button>
                                                    </div>
                                                </center>
                                                </td>
                                            </tr>
                                        </c:if>
                                    </c:forEach>  
                                </c:if>
                            </tbody> 

                        </table>
                    </div>
                    <div class="modal fade" id="ventanaDetalle" role="dialog" aria-labelledby="creationModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title " id="creationModalLabel">Detalle de la Instituci&oacute;n</h4>
                                </div>

                                <div class="modal-body" style="height: 100vh">
                                    <iframe id="detailArea" width="100%" height="100%" style="border: 0">                               
                                    </iframe>
                                </div>
                            </div>                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/bootstrap-confirmation.js"></script>
        <script src="resources/js/jquery.ui.widget.js"></script>
        <script src="resources/js/jquery.iframe-transport.js"></script>
        <script src="resources/js/jquery.fileupload.js"></script>
        <script src="resources/js/bootstrap-toggle.min.js"></script>
        <script>
            var variable = 'valor0';
            $(document).ready(function () {
                $('#institutions').DataTable({
                    "language": {
                        "url": "resources/js/Spanish.json"
                    },
                    "bFilter": false,
                    "bInfo": false
                });
                $('#fechaVencimiento').datepicker();
                loadStates();

            <c:if test="${not empty internalMsg}">
                $('#creationModal').modal('show');
            </c:if>
                
            });

            function isNumberKey(event) {
                var keycode = event.which;
                if (!(event.shiftKey === false && (keycode === 8 ||
                        keycode === 37 || keycode === 39 ||
                        (keycode >= 48 && keycode <= 57)))) {
                    event.preventDefault();
                }
            }

            function loadStates() {
                
                <c:if test="${not empty stateList}">
                    <c:forEach var="item" items="${stateList}" varStatus="iter">
                        <c:if test="${iter.index == 0}">
                            jQuery("<option>").attr("value", '${item.id}').
                                    text('${item.name}').attr("selected", "true").
                                    appendTo("#departamento");
                            loadCities("${item.id}");
                        </c:if>
                        <c:if test="${iter.index > 0}">
                            jQuery("<option>").attr("value", '${item.id}').
                                    text('${item.name}').appendTo("#departamento");
                        </c:if>
                    </c:forEach>
                </c:if>
            }

            function loadCities() {
                jQuery("#departamento").onchange(function () {
                    jQuery("#municipio").html('');
                    var stateCode = jQuery("#departamento").val();
                    loadCities(stateCode);
                });
            }

            function loadCities(stateCode) {
                jQuery("#municipio").find('option').remove();
                <c:if test="${not empty citiesList}">
                    <c:forEach items="${citiesList}" var="item" >
                        var code = "<c:out value="${item.state.id}"/>";
                        if (code === stateCode) {
                            jQuery("<option>").attr("value", '${item.id}').
                                    text('${item.name}').appendTo("#municipio");
                        }
                    </c:forEach>
                </c:if>
            }

            function loadInstitutionDetail(id) {
                $('#detailArea').attr("src",
                        'detalle-institucion-aprobar.htm?id=' + id);

            }
        </script>
    </body>
</html>
