/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CommonsValidator.java
 * Created on: 2016/12/11, 12:33:45 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 * Objetive: Test program massive upload: training records
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.stage;

/**
 * Unit Tests for load to stage from training load.
 * Requires set correct value in property name="password", 
 * resources/default package/database.xml.
 * resources/default package/database.xml.
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-batch.xml", 
    "classpath:batch_jobs/curriculumVitae/informalEducation/informalEducationStageJob.xml",
    "classpath:database.xml"})
public class InformalEducationStageJobTest {
    
    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER = 
        Logger.getLogger(InformalEducationStageJobTest.class);
    
    /**
     * Servicio para ejecutar jobs en modo pruebas
    */
    @Autowired
    JobLauncherTestUtils jobLauncherTestUtils;
    
    /**
     * Case for success writting
     * Requires set correct value in property name="password", 
     * resources/default package/database.xml.
     * Please uncomment @Test anotation only for local tests. Comment again after pull request.
     * @author <a href="mailto:javierr@gmlsoftware.com">Javier Leonardo Rocha</a>
    */
    
    @Test
    public void successCaseJob() throws Exception {
        
        LOGGER.info("Running informalEducationStageJobTest - successCaseJob");

       //Inicializamos local thread
        ThreadLocalJob.init();
 
        // Inicializamos el job.
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath", 
            "file:loadTestFiles/curriculum vitae/informalEducation/" +
            "informalEducationTestFile.txt"); 
        jobParametersBuilder.addString("loadId", "42");
        
        JobExecution jobExecution = jobLauncherTestUtils.
            launchJob(jobParametersBuilder.toJobParameters());
        
        for (Throwable t : jobExecution.getAllFailureExceptions()) {
            t.printStackTrace();
        }
        
        JobResultDto jobResult = ThreadLocalJob.getResult();
        LOGGER.info(jobResult.toString());
        System.out.println(jobResult.toString());
        
        if (!jobResult.getErrorList().isEmpty()){
            LOGGER.info(jobResult.getErrorsString());
            System.out.println(jobResult.getErrorsString());
        }
        
        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().getExitCode());
        Assert.assertTrue(jobResult.getErrorList().isEmpty());
        
        ThreadLocalJob.delete();
    }

 }
