/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: PilaDtoRowMapper.java
 * Created on: 2017/02/13, 11:30:02 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.dao.mapper;

import com.gml.simpc.api.dto.PilaDto;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 * Row Mapper para PILA.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class PilaDtoRowMapper implements RowMapper<PilaDto> {
    
    @Override
    public PilaDto mapRow(ResultSet rs, int i) throws SQLException {
        PilaDto e = new PilaDto();
        e.setId_Carga(rs.getLong("ID_CARGA"));
        e.setTipoIdAportanteU(rs.getString("TIPOIDAPORTANTEU"));
        e.setMaxDateU(rs.getString("MAXFECHAU"));
        e.setContributingNameU(rs.getString("NOMBREAPORTANTEU"));
        e.setNumberIdentification(rs.getString("NUMERODOCUMENTO"));
        e.setNumberIdentificationContributingU(rs.getString("NUMIDAPORTANTEU"));
        e.setDocumentType(rs.getString("TIPODOCUMENTO"));
        e.setPrimerPeriodoU(rs.getString("PRIMERPERIODOU"));
        e.setAfpuCode(rs.getString("CODIGOAFPU"));
        e.setEpsuCode(rs.getString("CODIGOEPSU"));
        e.setQuotes(rs.getString("COTIZACIONES"));
        e.setHealthQuotesDays(rs.getString("DIAS_COT_SALUD"));
        e.setDaysInd(rs.getString("DIASIND"));
        e.setDateCutPila(rs.getString("FECHACORTEPILA"));
        e.setPaidDateU(rs.getString("FECHAPAGOU"));
        e.setMaxDate(rs.getString("MAXFECHA"));
        e.setMonthsDep(rs.getString("MESESDEP"));
        e.setMonthsInd(rs.getString("MESESIND"));
        e.setContributingSubTypeU(rs.getString("SUBTIPOCOTIZANTEU"));
        e.setTypePayrollU(rs.getString("TIPOPLANILLAU"));
        e.setIbcMayorU(rs.getString("IBCMAYORU"));
        e.setSalarioBasicoU(rs.getString("SALARIOBASICOU"));
        e.setTipoIdAportanteU(rs.getString("TIPOIDAPORTANTEU"));
        e.setDiasDep(rs.getString("DIASDEP"));
        e.setPeriodosComo52(rs.getString("PERIODOSCOMO52"));
        e.setPrimerPeriodoComo52(rs.getString("PRIMERPERIODOCOMO52"));
        e.setUltimoPeriodoComo52(rs.getString("ULTIMOPERIODOCOMO52"));
        return e;
    }

}
