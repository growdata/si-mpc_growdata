/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: basicInformation.java
 * Created on: 2016/12/27, 04:23:26 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Leonardo Rocha</a>
 */
public class BasicInformationDto {

    private String ccfCode;
    private String agencyCode;
    private String registerDate;
    private String lastModificationDate;
    private String documentType;
    private String numberIdentification;
    private String email;
    private String firstName;
    private String secondName;
    private String firstSurName;
    private String secondSurName;
    private String birthDate;
    private String gender;
    private String militaryCard;
    private String militaryCardType;
    private String militaryCardNumber;
    private String phoneContact;
    private String civilStatus;
    private String country;
    private String nationality;
    private String stateDivipola;
    private String cityDivipola;
    private String houseBoss;
    private String targetPopulation;
    private String ethnicGroup;
    private String populationType;
    private String disability;
    private String disabilityType;
    private String residenceCountry;
    private String residenceState;
    private String residenceCity;
    private String addres;
    private String neighborhood;
    private String otherPhone;
    private String observations;
    private String wageAspiration;
    private String travelPossibility;
    private String movePossibility;
    private String distanceJobOffersInterest;
    private String employmentStatus;
    private String vehicleOwner;
    private String drivingLicenseCar;
    private String categoryLicenseCar;
    private String drivingLicenseMotorcycle;
    private String categoryLicenseMotorcycle;
    private Long cargaId;

    /*
     * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public BasicInformationDto() {

    }

    public BasicInformationDto(String ccfCode, String agencyCode,
        String registerDate, String lastModificationDate, String documentType,
        String numberIdentification, String email, String firstName,
        String secondName, String firstSurName, String secondSurName,
        String birthDate, String gender, String militaryCard,
        String militaryCardType, String militaryCardNumer, String phoneContact,
        String civilStatus, String country, String nationality,
        String stateDivipola, String cityDivipola, String houseBoss,
        String targetPopulation, String ethnicGroup, String populationType,
        String disability, String disabilityType, String residenceCountry,
        String residenceState,
        String residenceCity, String addres, String neighborhood,
        String otherPhone, String observations, String wageAspiration,
        String travelPossibility, String movePossibility,
        String distanceJobOffersInterest, String employmentStatus,
        String vehicleOwner, String drivingLicenseCar, String categoryLicenseCar,
        String drivingLicenseMotorcycle, String categoryLicenseMotorcycle,
        Long cargaId) {
        this.ccfCode = ccfCode;
        this.agencyCode = agencyCode;
        this.registerDate = registerDate;
        this.lastModificationDate = lastModificationDate;
        this.documentType = documentType;
        this.numberIdentification = numberIdentification;
        this.email = email;
        this.firstName = firstName;
        this.secondName = secondName;
        this.firstSurName = firstSurName;
        this.secondSurName = secondSurName;
        this.birthDate = birthDate;
        this.gender = gender;
        this.militaryCard = militaryCard;
        this.militaryCardType = militaryCardType;
        this.militaryCardNumber = militaryCardNumer;
        this.phoneContact = phoneContact;
        this.civilStatus = civilStatus;
        this.country = country;
        this.nationality = nationality;
        this.stateDivipola = stateDivipola;
        this.cityDivipola = cityDivipola;
        this.houseBoss = houseBoss;
        this.targetPopulation = targetPopulation;
        this.ethnicGroup = ethnicGroup;
        this.populationType = populationType;
        this.disability = disability;
        this.disabilityType = disabilityType;
        this.residenceCountry = residenceCountry;
        this.residenceState = residenceState;
        this.residenceCity = residenceCity;
        this.addres = addres;
        this.neighborhood = neighborhood;
        this.otherPhone = otherPhone;
        this.observations = observations;
        this.wageAspiration = wageAspiration;
        this.travelPossibility = travelPossibility;
        this.movePossibility = movePossibility;
        this.distanceJobOffersInterest = distanceJobOffersInterest;
        this.employmentStatus = employmentStatus;
        this.vehicleOwner = vehicleOwner;
        this.drivingLicenseCar = drivingLicenseCar;
        this.categoryLicenseCar = categoryLicenseCar;
        this.drivingLicenseMotorcycle = drivingLicenseMotorcycle;
        this.categoryLicenseMotorcycle = categoryLicenseMotorcycle;
        this.cargaId = cargaId;
    }

    public String getCcfCode() {
        return ccfCode;
    }

    public void setCcfCode(String ccfCode) {
        this.ccfCode = ccfCode;
    }

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    public String getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(String lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getNumberIdentification() {
        return numberIdentification;
    }

    public void setNumberIdentification(String numberIdentification) {
        this.numberIdentification = numberIdentification;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getFirstSurName() {
        return firstSurName;
    }

    public void setFirstSurName(String firstSurName) {
        this.firstSurName = firstSurName;
    }

    public String getSecondSurName() {
        return secondSurName;
    }

    public void setSecondSurName(String secondSurName) {
        this.secondSurName = secondSurName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMilitaryCard() {
        return militaryCard;
    }

    public void setMilitaryCard(String militaryCard) {
        this.militaryCard = militaryCard;
    }

    public String getMilitaryCardType() {
        return militaryCardType;
    }

    public void setMilitaryCardType(String militaryCardType) {
        this.militaryCardType = militaryCardType;
    }

    public String getMilitaryCardNumber() {
        return militaryCardNumber;
    }

    public void setMilitaryCardNumber(String militaryCardNumber) {
        this.militaryCardNumber = militaryCardNumber;
    }

    public String getPhoneContact() {
        return phoneContact;
    }

    public void setPhoneContact(String phoneContact) {
        this.phoneContact = phoneContact;
    }

    public String getCivilStatus() {
        return civilStatus;
    }

    public void setCivilStatus(String civilStatus) {
        this.civilStatus = civilStatus;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getStateDivipola() {
        return stateDivipola;
    }

    public void setStateDivipola(String stateDivipola) {
        this.stateDivipola = stateDivipola;
    }

    public String getCityDivipola() {
        return cityDivipola;
    }

    public void setCityDivipola(String cityDivipola) {
        this.cityDivipola = cityDivipola;
    }

    public String getHouseBoss() {
        return houseBoss;
    }

    public void setHouseBoss(String houseBoss) {
        this.houseBoss = houseBoss;
    }

    public String getTargetPopulation() {
        return targetPopulation;
    }

    public void setTargetPopulation(String targetPopulation) {
        this.targetPopulation = targetPopulation;
    }

    public String getEthnicGroup() {
        return ethnicGroup;
    }

    public void setEthnicGroup(String ethnicGroup) {
        this.ethnicGroup = ethnicGroup;
    }

    public String getPopulationType() {
        return populationType;
    }

    public void setPopulationType(String populationType) {
        this.populationType = populationType;
    }

    public String getDisability() {
        return disability;
    }

    public void setDisability(String disability) {
        this.disability = disability;
    }

    public String getResidenceCountry() {
        return residenceCountry;
    }

    public void setResidenceCountry(String residenceCountry) {
        this.residenceCountry = residenceCountry;
    }

    public String getResidenceState() {
        return residenceState;
    }

    public void setResidenceState(String residenceState) {
        this.residenceState = residenceState;
    }

    public String getResidenceCity() {
        return residenceCity;
    }

    public void setResidenceCity(String residenceCity) {
        this.residenceCity = residenceCity;
    }

    public String getAddres() {
        return addres;
    }

    public void setAddres(String addres) {
        this.addres = addres;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getOtherPhone() {
        return otherPhone;
    }

    public void setOtherPhone(String otherPhone) {
        this.otherPhone = otherPhone;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public String getWageAspiration() {
        return wageAspiration;
    }

    public void setWageAspiration(String wageAspiration) {
        this.wageAspiration = wageAspiration;
    }

    public String getTravelPossibility() {
        return travelPossibility;
    }

    public void setTravelPossibility(String travelPossibility) {
        this.travelPossibility = travelPossibility;
    }

    public String getMovePossibility() {
        return movePossibility;
    }

    public void setMovePossibility(String movePossibility) {
        this.movePossibility = movePossibility;
    }

    public String getDistanceJobOffersInterest() {
        return distanceJobOffersInterest;
    }

    public void setDistanceJobOffersInterest(String distanceJobOffersInterest) {
        this.distanceJobOffersInterest = distanceJobOffersInterest;
    }

    public String getEmploymentStatus() {
        return employmentStatus;
    }

    public void setEmploymentStatus(String employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

    public String getVehicleOwner() {
        return vehicleOwner;
    }

    public void setVehicleOwner(String vehicleOwner) {
        this.vehicleOwner = vehicleOwner;
    }

    public String getDrivingLicenseCar() {
        return drivingLicenseCar;
    }

    public void setDrivingLicenseCar(String drivingLicenseCar) {
        this.drivingLicenseCar = drivingLicenseCar;
    }

    public String getDrivingLicenseMotorcycle() {
        return drivingLicenseMotorcycle;
    }

    public void setDrivingLicenseMotorcycle(String drivingLicenseMotorcycle) {
        this.drivingLicenseMotorcycle = drivingLicenseMotorcycle;
    }

    public String getCategoryLicenseMotorcycle() {
        return categoryLicenseMotorcycle;
    }

    public String getDisabilityType() {
        return disabilityType;
    }

    public void setDisabilityType(String disabilityType) {
        this.disabilityType = disabilityType;
    }

    public String getCategoryLicenseCar() {
        return categoryLicenseCar;
    }

    public void setCategoryLicenseCar(String categoryLicenseCar) {
        this.categoryLicenseCar = categoryLicenseCar;
    }

    public Long getCargaId() {
        return cargaId;
    }

    public void setCargaId(Long cargaId) {
        this.cargaId = cargaId;
    }

    /*
     * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public void setCategoryLicenseMotorcycle(String categoryLicenseMotorcycle) {
        this.categoryLicenseMotorcycle = categoryLicenseMotorcycle;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CargaId=").append(cargaId).
            append("~&~Tipo de Documento=").append(documentType).
            append("~&~N�mero de Documento=").append(numberIdentification).
            append("~&~CodigoCCF=").append(ccfCode).
            append("~&~C�digo de la Agencia=").append(agencyCode).
            append("~&~Fecha de Registro=").append(registerDate).
            append("~&~Fecha �ltima Actualizaci�n=").append(lastModificationDate).
            append("~&~Correo Electr�nico=").append(email).
            append("~&~Primer Nombre=").append(firstName).
            append("~&~Segundo Nombre=").append(secondName).
            append("~&~Primer Apellido=").append(firstSurName).
            append("~&~Segundo Apellido=").append(secondSurName).
            append("~&~Fecha de Cumplea�os=").append(birthDate).
            append("~&~Genero=").append(gender).
            append("~&~Libreta Militar=").append(militaryCard).
            append("~&~Tipo de Libreta Militar=").append(militaryCardType).
            append("~&~N�mero de Libreta Militar=").append(militaryCardNumber).
            append("~&~Tel�fono de Contacto=").append(phoneContact).
            append("~&~Estado Civil=").append(civilStatus).
            append("~&~Pa�s=").append(country).
            append("~&~Nacionalidad=").append(nationality).
            append("~&~Departamento=").append(stateDivipola).
            append("~&~Municipio=").append(cityDivipola).
            append("~&~Cabeza de Hogar=").append(houseBoss).
            append("~&~Poblaci�n Folicalizada=").append(targetPopulation).
            append("~&~Grupo �tnico=").append(ethnicGroup).
            append("~&~Tipo de Poblaci�n=").append(populationType).
            append("~&~Discapacidad=").append(disability).
            append("~&~Tipo De Discapacidad=").append(disabilityType).
            append("~&~Pa�s de Residencia=").append(residenceCountry).
            append("~&~Departamento de Residencia=").append(residenceState).
            append("~&~Municipio de Residencia=").append(residenceCity).
            append("~&~Direcci�n=").append(addres).
            append("~&~Barrio=").append(neighborhood).
            append("~&~Otro Tel�fono=").append(otherPhone).
            append("~&~Observaciones=").append(observations).
            append("~&~Aspiraci�n Salarial=").append(wageAspiration).
            append("~&~Posibilidad De Viajar=").append(travelPossibility).
            append("~&~Posibilidad De Mudarse=").append(movePossibility).
            append("~&~Inter�s en TeleTrabajo=").append(distanceJobOffersInterest).
            append("~&~Situaci�n Laboral=").append(employmentStatus).
            append("~&~Due�o de Veh�culo=").append(vehicleOwner).
            append("~&~Licencia de Carro=").append(drivingLicenseCar).
            append("~&~Categor�a de Licencia de Carro=").append(categoryLicenseCar).
            append("~&~Licencia de Moto=").append(drivingLicenseMotorcycle).
            append("~&~Categor�a de Licencia de Moto=").append(categoryLicenseMotorcycle);
        return stringBuilder.toString();
    }    
}
