/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ModuleService.java
 * Created on: 2016/12/14, 03:38:44 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.dto.ModuleDto;
import java.util.List;

/**
 * Interface que define el servicio para manipular modulos.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface ModuleService {

    /**
     * Consulta Modulos por CCF.
     *
     * @param startDate
     * @param endDate
     * @param ccfCode
     * @param programName
     * @param programCode
     * @param formationType
     * @param moduloName
     * @param institution
     *
     * @return
     */
    List<ModuleDto> findModulesByFilters( ModuleDto searchModules);
    
   /**
     * Consulta Modulos en Detalle
     *
     * @param id
     *
     * @return
     */
    List<ModuleDto> findModules(String id);

   
}
