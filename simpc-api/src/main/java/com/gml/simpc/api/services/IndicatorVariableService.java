/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IndicatorService.java
 * Created on: 2016/12/16, 10:28:47 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.entity.IndicatorVariable;
import java.util.List;
import java.util.Map;

/**
 * Interface que define el servicio de indicadores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface IndicatorVariableService {

    /**
     * Obtiene todos los indicadores del sistema.
     *
     * @return
     */
    List<IndicatorVariable> findAll();

    IndicatorVariable findOne(Long id);

    IndicatorVariable findByIndicatorIdAndField(Long id, String field);

    List<Map<String, Object>> getVariableList(IndicatorVariable xVariable);


}
