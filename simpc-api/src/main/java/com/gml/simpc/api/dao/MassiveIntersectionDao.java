/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: MassiveIntersectionDao.java
 * Created on: 2017/02/13, 03:02:02 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dao;

import com.gml.simpc.api.dto.BasicInformationDto;
import com.gml.simpc.api.dto.EducationLevelDto;
import com.gml.simpc.api.dto.InformalEducationDto;
import com.gml.simpc.api.dto.IntermediationDto;
import com.gml.simpc.api.dto.LanguageDto;
import com.gml.simpc.api.dto.OtherKnowledgeDto;
import com.gml.simpc.api.dto.PilaDto;
import com.gml.simpc.api.dto.PilaEmploymentServiceDto;
import com.gml.simpc.api.dto.ProfitDto;
import com.gml.simpc.api.dto.ProgramDto;
import com.gml.simpc.api.dto.WorkExperienceDto;
import java.util.List;

/**
 * Dao para cruces masivos.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface MassiveIntersectionDao {

    /**
     * Obtiene los registros de pila por id de cruce.
     *
     * @param intersectionId
     *
     * @return
     */
    List<PilaDto> findPila(long intersectionId);
    
    /**
     * Obtiene los registros de pila por id de cruce.
     *
     * @param intersectionId
     *
     * @return
     */
    List<PilaEmploymentServiceDto> findPilaES(long intersectionId);

    /**
     * Obtiene registros de intermediaci&oacute;n por id de cruce.
     *
     * @param intersectionId
     *
     * @return
     */
    List<IntermediationDto> findIntermediation(long intersectionId);

    /**
     * Obtiene registros de informaci&oacute;n b&aacute;sica por id de carga.
     *
     * @param intersectionId
     *
     * @return
     */
    List<BasicInformationDto> findBasicInformation(long intersectionId);

    /**
     * Obtiene los registros de nivel educativo por id de carga.
     *
     * @param intersectionId
     *
     * @return
     */
    List<EducationLevelDto> findEducationLevel(long intersectionId);

    /**
     * Obtiene los registros de experiencia laboral por id de carga.
     *
     * @param intersectionId
     *
     * @return
     */
    List<WorkExperienceDto> findWorkExperience(long intersectionId);

    /**
     * Obtiene los registros de educaci&oacute;n informal por id de carga.
     *
     * @param intersectionId
     *
     * @return
     */
    List<InformalEducationDto> findInformalEducation(long intersectionId);

    /**
     * Obtiene los registros de lenguajes por id de carga.
     *
     * @param intersectionId
     *
     * @return
     */
    List<LanguageDto> findLanguage(long intersectionId);

    /**
     * Obtiene los registros de otros conocimientos por id de carga.
     *
     * @param intersectionId
     *
     * @return
     */
    List<OtherKnowledgeDto> findOtherKnowledge(long intersectionId);
    
    /**
     * Obtiene los registros de fosfec por id de carga.
     *
     * @param intersectionId
     *
     * @return
     */
    List<ProfitDto> findFosfec(long intersectionId);
    
    /**
     * Obtiene los registros de orientacion y capacitación.
     *
     * @param intersectionId
     *
     * @return
     */
    List<ProgramDto> findTraining(long intersectionId);
}
