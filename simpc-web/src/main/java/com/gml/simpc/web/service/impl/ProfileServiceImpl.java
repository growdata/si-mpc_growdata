/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: FunctionalityServiceImpl.java
 * Created on: 2016/10/24, 11:38:11 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.entity.Functionality;
import com.gml.simpc.api.entity.Profile;
import com.gml.simpc.api.entity.Resource;
import com.gml.simpc.api.services.ProfileService;
import com.gml.simpc.api.utilities.Util;
import com.gml.simpc.commons.annotations.AnnotationInterceptor;
import com.gml.simpc.web.repository.FunctionalityRepository;
import com.gml.simpc.web.repository.ProfileRepository;
import com.gml.simpc.web.repository.ResourceRepository;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.gml.simpc.api.utilities.Util.likeValue;

/**
 *
 * @author <a href="mailto:jonathanp@gmlsoftware.com">Jonathan Pont�n</a>
 */
@Service(value = "profileService")
public class ProfileServiceImpl implements ProfileService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(ProfileServiceImpl.class);
    /**
     * Constante que representa un objeto no encontrado.
     */
    private static final int NOT_FOUND = -1;
    /**
     * Repositorio para tipos de perfiles.
     */
    @Autowired
    private ProfileRepository profileRepository;
    /**
     * Repositorio de recursos.
     */
    @Autowired
    private ResourceRepository resourceRepository;
    /**
     * Repositorio de funcionalidades.
     */
    @Autowired
    private FunctionalityRepository functionalityRepository;

    /** 
     * Genera lista de todos los perfiles.
     * 
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar Todos los" +
        " Perfiles", entityTableName = "PERFILES")
    @Override
    public List<Profile> getAll() {
        LOGGER.info("Ejecutando metodo [ getAll ]");
        return profileRepository.findAll();
    }

    /**
     * Guarda perfies.
     * 
     * @param profile 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Guardar",
        entityTableName = "PERFILES")
    @Override
    public void save(Profile profile) {
        this.profileRepository.saveAndFlush(profile);
    }

    /**
     * Actualiza perfiles.
     * 
     * @param profile 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Actualizar",
        entityTableName = "PERFILES")
    @Override
    public void update(Profile profile) {
        LOGGER.info("Ejecutando metodo [ update ]");
        this.profileRepository.saveAndFlush(profile);
    }

    /**
     * Elimina perfiles.
     * 
     * @param profile 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Borrar",
        entityTableName = "PERFILES")
    @Override
    public void remove(Profile profile) {
        this.profileRepository.delete(profile);
    }

    /**
     * Genera una lista de perfiles seg&ucute;n los filtros ingresados.
     * 
     * @param name
     * @param status
     * @param functionalityId
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar por Filtros",
        entityTableName = "PERFILES")
    @Override
    public List<Profile> findByFilters(String name, String status,
        String functionalityId) {
        LOGGER.info("Ejecutando metodo [ findByNameLikeIgnoreCaseAndStatus ]");

        return this.profileRepository.
            findByFilters(likeValue(name),
                likeValue(status), Util.nullValue(functionalityId));
    }

    /**
     * Busca un perfil seg&ucute;n su id.
     * 
     * @param id
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar por Id",
        entityTableName = "PERFILES")
    @Override
    public Profile findById(Long id) {
        LOGGER.info("Ejecutando metodo [ findById ]");
        return this.profileRepository.
            findById(id);
    }

    /**
     * Guarda las funcionalidades de un perfil espec&icute;fico.
     * 
     * @param profileId
     * @param resources 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Actualizar Funciona" +
        "lidades de los perfiles", entityTableName = "RECURSOS")
    @Transactional
    @Override
    public void updateProfileFunctionalities(Long profileId,
        List<Resource> resources) {
        LOGGER.info("Ejecutando metodo [ updateProfileFunctionalities ]");

        List<Functionality> functionalities = this.functionalityRepository.
            getChildrenFunctionalities();
        Set<Long> parents = new HashSet<>();

        for (Resource resource : resources) {
            int index = functionalities.indexOf(new Functionality(resource.
                getIdFunctionality()));

            if (index != NOT_FOUND) {
                Functionality functionality = functionalities.get(index);

                parents.add(functionality.getParentId());
            }
        }

        for (Long parentId : parents) {
            resources.add(new Resource(profileId, parentId));
        }

        int result = this.resourceRepository.deleteByProfile(profileId);

        LOGGER.info("Se eliminaron [ " + result + " ] recursos");
        LOGGER.info("Hay [ " + resources.size() + " ] recursos");

        this.resourceRepository.save(resources);
    }
}
