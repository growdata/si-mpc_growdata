
  CREATE TABLE AREAS_DESEMPENO_ORIENTACION 
   (	"ID" NUMBER(19,0), 
	"CODIGO" VARCHAR2(20 CHAR), 
	"VALOR" VARCHAR2(250 CHAR)
   );
commit; 

Insert into AREAS_DESEMPENO_ORIENTACION  (ID,CODIGO,VALOR) values ('1','1','Orientación Laboral');
Insert into AREAS_DESEMPENO_ORIENTACION  (ID,CODIGO,VALOR) values ('2','2','Intermediación Laboral');
Insert into AREAS_DESEMPENO_ORIENTACION  (ID,CODIGO,VALOR) values ('3','3','Emprendimiento');
Insert into AREAS_DESEMPENO_ORIENTACION  (ID,CODIGO,VALOR) values ('4','4','Certificación de competencias laborales');


Commit;
--------------------------------------------------------