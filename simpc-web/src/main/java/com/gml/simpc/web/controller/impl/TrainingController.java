/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: TrainingController.java
 * Created on: 2016/12/13, 10:34:57 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.gml.simpc.api.dto.ModuleDto;
import com.gml.simpc.api.entity.Session;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.services.ModuleService;
import com.gml.simpc.api.utilities.Util;
import com.gml.simpc.web.controller.ControllerDefinition;
import com.gml.simpc.web.repository.InstitutionRepository;
import com.gml.simpc.web.utilities.ValueListService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import static com.gml.simpc.api.utilities.Util.FAILED_ALERT;
import static com.gml.simpc.api.utilities.Util.SESSION_USER;

/**
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
@Controller
public class TrainingController implements ControllerDefinition {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(TrainingController.class);
    /**
     * Servicio para manipular modulos.
     */
    @Autowired
    private ModuleService moduleService;
    /**
     * Servicio para manipular listas de valor.
     */
    @Autowired
    private ValueListService valueListService;
    
    /**
     * Reporitorio para obtener lista de instituciones.
     */
    @Autowired
    InstitutionRepository institutionRepository;
    
    /**
     * Inicializa el binder para formatear fechas.
     *
     * @param binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class,
            new CustomDateEditor(sdf, true));
    }

    /**
     * Despliega la informacion de los programas-m&oacute;dulos.
     * 
     * @param request
     * @param response
     * 
     * @return 
     */
    @RequestMapping(value = "programas-modulos",
        method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView("programas-modulos");

        try {
            init(modelAndView);
            return modelAndView;

        } catch (Exception o) {
            LOGGER.error("Error ", o);
            return modelAndView;

        }
    }

    /**
     * Despliega la informacion de los programas-m&oacute;dulos.
     * 
     * @param request
     * @param response
     * @param searchModules
     * 
     * @return 
     */
    @RequestMapping(value = "rango-modulos",
        method = RequestMethod.POST)
    public ModelAndView rangeModule(HttpServletRequest request,
        HttpServletResponse response,
        @ModelAttribute("searchModules") ModuleDto searchModules) {
        ModelAndView modelAndView = new ModelAndView("programas-modulos");
        
        try {
            //Receiving parameters
            
            Session session = (Session) request.getSession().getAttribute(
                SESSION_USER);
            User userSession = session.getUser();
            searchModules.setCodigo_ccf(Util.nullValue(userSession.getCcf().getCode()));
            if (userSession.isAdmin()) {
                searchModules.setCodigo_ccf("%");
     
            }
            
            List<ModuleDto> result = this.moduleService.
                findModulesByFilters(searchModules);
            
            init(modelAndView);
            
            modelAndView.addObject("programList", result);
             modelAndView.addObject("searchModules", searchModules);
             modelAndView.addObject("formationSelected", searchModules.getTrainingType());
             modelAndView.addObject("instiSelected", searchModules.getInstitution());
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msgType", FAILED_ALERT);
            modelAndView.addObject("msg", "Por favor contacte al administrador");
        } finally {
            LOGGER.info("finally programas/modulos");
        }
        
        return modelAndView;
    }

    /**
     * M&eacute;todo que se ejecuta al abrir el detalle de un m&oacute;dulo.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping("detalle-modulos")
    public ModelAndView detail(HttpServletRequest request,
        HttpServletResponse response) {

        String id = request.getParameter("id");

        ModelAndView modelAndView =
            new ModelAndView("detalle-modulos");

        try {
            List ModuleList = this.moduleService.findModules(id);
            String name = ((ModuleDto) ModuleList.get(0)).getProgramName();
            modelAndView.addObject("programName", name.toUpperCase());
            modelAndView.addObject("ModulesList", ModuleList);

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg", "Por Favor Contacte al administrador");
        } finally {
            LOGGER.info("finally de display Aprobacion");
        }

        return modelAndView;

    }

    /**
     * Inicia los objetos para el controler.
     *
     * @param modelAndView
     */
    private void init(ModelAndView modelAndView) {

        try {
            modelAndView.addObject("searchModules", new ModuleDto());
            modelAndView.addObject("formationType", this.valueListService.
                getProgramTypeList());
            modelAndView.addObject("institutionList", this.institutionRepository.findAll());
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg",
                "Se presento un error al cargar los filtros," +
                " contacte al administrador");
        } finally {
            LOGGER.info("finally de display Aprobacion");
        }

    }
}
