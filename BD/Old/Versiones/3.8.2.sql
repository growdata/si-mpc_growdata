--Creaci�n de vista para cajas de compensacion
create or replace view vw_cajas_compensacion as
    select codigo id,nombre_corto descripcion
    from fundaciones 
    where es_ccf = 'V'
;   
commit;
--Creaci�n de vista para tipos de institucion
create or replace view vw_tipos_institucion as
    select 
    t.id,t.DESCRIPCION
    from MD_F_CAP_PROGRAMA_MODULO md, INSTITUCIONES i,TIPOS_INSTITUCION t
    where md.codigo_inst = i.id and t.id = i.TIPO_INSTITUCION_ID
    group by t.id,t.DESCRIPCION
;

commit;

-------------------------------------
create or replace view PIE_MONTOS as
with data  as (

SELECT TIPO_IDENTIFICACION,NUMERO_IDENTIFICACION, verificacion_requisitos,CODIGO_CCF ,  fecha_liquidacion_beneficio,
SUM (MONTO_LIQUIDADO_SALUD) salud ,
SUM (MONTO_LIQUIDADO_PENSION) PENSION,
SUM (MONTO_LIQUIDADO_CUOTA) CUOTA
,
SUM (MONTO_LIQUIDADO_CESANTIAS) CESANTIAS,
SUM (MONTO_LIQUIDADO_ALIMENTACION) ALIMENTACION FROM MD_F_FOSFEC , vw_cajas_compensacion
WHERE CODIGO_CCF = vw_cajas_compensacion.id  
GROUP BY CODIGO_CCF,fecha_liquidacion_beneficio,verificacion_requisitos,TIPO_IDENTIFICACION,NUMERO_IDENTIFICACION )

SELECT VERIFICACION_REQUISITOS,fecha_liquidacion_beneficio,CODIGO_CCF , "DESCRIPCION_MONTO","VALUE" FROM data
unpivot
(
  value
    for DESCRIPCION_MONTO in ("SALUD","PENSION","CUOTA","CESANTIAS","ALIMENTACION"));
-------------------------------------
create or replace view ASISTENTES_CAPACITACIONES as
select  
capacitaciones."CARGA_ID",capacitaciones."LINEA",capacitaciones."CODIGO_PROGRAMA",capacitaciones."NOMBRE_PROGRAMA",capacitaciones."TIPO_PROGRAMA",capacitaciones."TIPO_CURSO",capacitaciones."CODIGO_CCF",capacitaciones."CODIGO_INST",capacitaciones."CODIGO_SEDE",capacitaciones."CODIGO_DEPARTAMENTO",capacitaciones."CODIGO_MUNICIPIO",capacitaciones."CERTIFICADA",capacitaciones."NOMBRE_CERTIFICACION",capacitaciones."CINE",capacitaciones."NECESIDAD",capacitaciones."CODIGO_MODULO",capacitaciones."NOMBRE_MODULO",capacitaciones."DURACION",capacitaciones."TIPO_HORARIO",capacitaciones."TIPO_COSTO_MATRICULA",capacitaciones."COSTO_MATRICULA",capacitaciones."TIPO_OTROS_COSTOS",capacitaciones."OTROS_COSTOS",capacitaciones."FECHA_INICIO",capacitaciones."FECHA_FIN",capacitaciones."EN_DOMINIO"
,detalle.TIPO_DOCUMENTO
,NVL2(detalle.DOCUMENTO,1,0) asistente_fosfec 
        ,detalle.DOCUMENTO
        ,detalle.ESTADO
        ,detalle.PORCENTAJE_ASISTENCIA
        ,detalle.COSTO_TRANSPORTE
        ,fosfec.VERIFICACION_REQUISITOS verificacion
        ,fosfec.FECHA_LIQUIDACION_BENEFICIO fecha_liquidacion
        ,fosfec.NUMERO_IDENTIFICACION  cedula_beneficiario
        ,fosfec.TIPO_IDENTIFICACION tipo_id_beneficiario
        ,fosfec.CODIGO_CCF  CCF_beneficiario
  from 
    md_f_fosfec fosfec left join MD_F_PROGRAMA_DETALLE detalle  
    on (fosfec.NUMERO_IDENTIFICACION=detalle.DOCUMENTO and fosfec.TIPO_IDENTIFICACION=detalle.TIPO_DOCUMENTO)
                        left join   MD_F_CAP_PROGRAMA_MODULO  capacitaciones  on capacitaciones.CODIGO_MODULO = detalle.CODIGO_MODULO;


commit;
create or replace view VW_AHORRO_CESANTIAS_PERCAPITA as
select FECHA_LIQUIDACION_BENEFICIO periodo,CODIGO_CCF
,((sum( MONTO_LIQUIDADO_CESANTIAS)) / (count(distinct(numero_identificacion||TIPO_IDENTIFICACION)))) valor_percapita_cesantias

from MD_F_FOSFEC

where  MONTO_LIQUIDADO_CESANTIAS <> 0
group by FECHA_LIQUIDACION_BENEFICIO ,CODIGO_CCF;

commit;



truncate table IND_INDICADORES;
truncate table IND_VARIABLE;
truncate table IND_FUENTES;
COMMIT;


Insert into SYSTEM.IND_FUENTES (ID,NOMBRE,TABLA) values ('1','Prestaciones Economicas','MD_F_FOSFEC');
Insert into SYSTEM.IND_FUENTES (ID,NOMBRE,TABLA) values ('2','Capacitaci�n','MD_F_CAP_PROGRAMA_MODULO');
Insert into SYSTEM.IND_FUENTES (ID,NOMBRE,TABLA) values ('3','Detalle de capacitaci�nN','ASISTENTES_CAPACITACIONES');
Insert into SYSTEM.IND_FUENTES (ID,NOMBRE,TABLA) values ('4','Valores percapita en salud y pension','VW_SALUD_PENSION_PERCAPITA');
Insert into SYSTEM.IND_FUENTES (ID,NOMBRE,TABLA) values ('5','Valores percapita en alimentacion','VW_ALIMENTACION_PERCAPITA');
Insert into SYSTEM.IND_FUENTES (ID,NOMBRE,TABLA) values ('6','Valores percapita ahorro de cesantias','VW_AHORRO_CESANTIAS_PERCAPITA');
Insert into SYSTEM.IND_FUENTES (ID,NOMBRE,TABLA) values ('7','Valores percapita cuota monetaria','VW_CUOTA_MON_PERCAPITA');
Insert into SYSTEM.IND_FUENTES (ID,NOMBRE,TABLA) values ('8','Valores percapita prestaciones economicas','VW_PRESTACIONES_PERCAPITA');
Insert into SYSTEM.IND_FUENTES (ID,NOMBRE,TABLA) values ('9','Valores percapita por cuota monetaria por dependientes ','VW_CUOTA_DEPENDIETES_PERCAPITA');
Insert into SYSTEM.IND_FUENTES (ID,NOMBRE,TABLA) values ('10','Valores  liquidados ','PIE_MONTOS');
--------------------------------------------------------
COMMIT;


Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','21','campo',null,'TIPO_IDENTIFICACION','Tipo de identificacion');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','22','campo',null,'numero_identificacion','N�mero de identificaci�n');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('2','221','periodo','yyyy-MM','fecha_inicio','Fecha de inicio del  modulo');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('2','222','periodo','yyyy-MM','fecha_fin','Fecha de fin del modulo');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('2','223','tabla','tipos_programa','tipo_curso','Tipo de formaci�n');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('2','224','tabla','vw_cajas_compensacion','codigo_ccf','Caja de compensaci�n atravez del cual se dicta la capacitacion');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values (null,'225','campo',null,'    distinct codigo_programa','C�digo de programa');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('2','226','tabla','vw_tipos_institucion','codigo_inst','Tipo de instituci�n');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('4','403','campo',null,'VALOR_PERCAPITA_SALUD','Percapita de salud');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('4','402','tabla','vw_cajas_compensacion','CODIGO_CCF','Codigo de la ccf');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('4','401','periodo','yyyy','PERIODO','Valor   de donde se sustrae el a�o');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('2','227','tabla','vw_cine','cine','C�digo cine');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('2','228','tabla','vw_necesidad','necesidad','Necesidad');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','302','campo',null,'NOMBRE_PROGRAMA','Nombre del programa');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','303','tabla','tipos_programa','TIPO_CURSO','Tipo de Formacion');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','304','tabla','vw_cajas_compensacion','CCF_beneficiario','CCF que pertenece el postulante');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','305','campo',null,'CODIGO_INST','Codigo de la institucion que Dicta el curso');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','306','campo',null,'CODIGO_SEDE','Codigo de la sede donde se  dicta el modulo de este programa');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','307','campo',null,'CODIGO_MUNICIPIO','Codigo del Municipio donde se dicta este modulo');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','308','campo',null,'CERTIFICADA','Esta cerrtificada');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','309','campo',null,'CINE','Codigo Cine');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','310','campo',null,'CODIGO_MODULO','Codigo del modulo');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','311','campo',null,'NOMBRE_MODULO','Nombre del Modulo');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','312','campo',null,'COSTO_MATRICULA','Costo de la matricula');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','313','campo',null,'OTROS_COSTOS','Otros Costos');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','314','periodo','yyyy-MM','FECHA_INICIO','Fecha inicio del curso');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','1','periodo','yyyy-MM','fecha_postulacion_mpc','Fecha que se radica la solucitud');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','315','campo',null,'TIPO_DOCUMENTO','Tipo de documento del asistente');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','4','campo',null,'verificacion_requisitos','Estado de la solicitud  ');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','5','tabla','vw_estados_beneficio','terminacion_beneficio','Terminaci�n del beneficio');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','6','campo',null,'periodo_beneficio_liquidado','Periodo de liquidaci�n');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','7','tabla','vw_cajas_compensacion','codigo_ccf','Caja de compensacion a la  que pertenece  el postulante');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','8','periodo','yyyy-MM','fecha_verificacion_postu','Fecha de verificaci�n de la  solicitud');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','9','periodo','yyyy-MM','fecha_terminacion_beneficio','Corresponde a la fecha en la cual se aplico la ultima o sexta liquidacion al beneficio');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','10','campo',null,'monto_liquidado_cuota','Corresponde al monto liquidado al cesante por cuota monetaria para el periodo  de beneficio reportado. ');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','11','campo',null,'Monto_Liquidado_Cesantias','Corresponde al monto liquidado al cesante por ahorro de cesantias por periodo liquidado. ');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','12','campo',null,'Monto_Liquidado_Alimentacion','Corresponde al monto liquidado al cesante por cuota monetaria para el periodo  de beneficio reportado. ');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','13','campo',null,'monto_liquidado_salud','Monto liquidado en salud');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','14','campo',null,'monto_liquidado_pension','Monto liquidado en pensi�n');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','15','campo',null,'valor_total_prestaciones','Valor total de  prestaciones');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','16','campo',null,'beneficiario_del_mecanismo','Beneficiario del mecanismo');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','23','campo',null,'inscripcion_servicio_empleo','inscripcion al servicio de empleo');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','24','campo',null,'tipo_vinculacion_ccf','tipo de  vinculacion ccf');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','25','periodo','yyyy-MM','fecha_liquidacion_beneficio','fecha liquidacion beneficio');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('1','26','campo',null,'TERMINACION_BENEFICIO','terminacion de beneficio');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('10','27','campo',null,'value','Valor total de prestaciiones');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('2','344','campo',null,'codigo_programa','C�digo de programa');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('2','345','tabla','vw_tipos_certificacion','nombre_certificacion','Nombre de la certificacion');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','323','campo',null,'tipo_id_beneficiario','Tipo id    FOSFEC');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('10','347','tabla','vw_cajas_compensacion','codigo_ccf','Caja de compesacion');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('10','348','periodo','yyyy-MM','fecha_liquidacion_beneficio','fecha liquidacion beneficio');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('10','349','campo',null,'DESCRIPCION_MONTO','Tipo de monto liquidado');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','301','campo',null,'CODIGO_PROGRAMA','Codigo del programa');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','316','campo',null,'DOCUMENTO','Numero de documento del asistente');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','317','campo',null,'ESTADO','Estado del asistente

1- Finalizado
2 -En curso
3- Desertado
4- Inscrito');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','318','campo',null,'VERIFICACION','Estado de la solicitud');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','319','periodo','yyyy-MM','FECHA_LIQUIDACION','Ultima fecha de liquidacion del postulante');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','320','campo',null,'PORCENTAJE_ASISTENCIA','Porcentaje de asistensia');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values (null,'321','campo',null,'  distinct TIPO_DOCUMENTO || DOCUMENTO','Clave de documento unico asistente de capacitacion');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values (null,'3','campo',null,'    distinct TIPO_IDENTIFICACION || numero_identificacion ','Clave de indentificacion unica para  beneficiarios fosfec');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','322','campo',null,'CEDULA_BENEFICIARIO','Cedula  FOSFEC');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values (null,'324','campo',null,'   distinct CEDULA_BENEFICIARIO || tipo_id_beneficiario','Clave de cedulas y tipo fosfec en  capacitaciones asistentes');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('2','229','tabla','vw_tipos_certificacion','nombre_certificacion','Certificacion emitida');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','325','campo',null,'COSTO_TRANSPORTE','subsidio de transporte');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('4','404','campo',null,'VALOR_PERCAPITA_PENSION','Percapita de pension');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('5','501','periodo','yyyy','PERIODO','A�o');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('5','502','tabla','vw_cajas_compensacion','CODIGO_CCF','Codigo CCF');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('5','503','campo',null,'VALOR_PERCAPITA_ALIMENTACION','Percapita del bono de alimentacion');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('6','601','periodo','yyyy','PERIODO','Periodo');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('6','602','tabla','vw_cajas_compensacion','CODIGO_CCF','Codigo CCF');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('6','603','campo',null,'VALOR_PERCAPITA_CESANTIAS','valor percapita Cesantias');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('7','701','periodo','yyyy','PERIODO','Periodo');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('7','702','tabla','vw_cajas_compensacion','CODIGO_CCF','Codigo CCf');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('7','703','campo',null,'VALOR_PERCAPITA_CUOTA_MON','Valor percapita Cuota Monetaria');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('8','801','periodo','yyyy','PERIODO','Periodo');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('8','802','tabla','vw_cajas_compensacion','CODIGO_CCF','Codigo CCF');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('8','803','campo',null,'VALOR_PERCAPITA_PRESTACIONES','Valor percapita del total de  cesantias');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('9','901','periodo','yyyy','PERIODO','Perido');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('9','902','tabla','vw_cajas_compensacion','CODIGO_CCF','Codigo CCF');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('9','903','campo',null,'VALOR_PERCAPITA_DEPENDIENTES','Valor percapita  dependientes');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO,DESCRIPCION) values ('3','326','campo',null,'asistente_fosfec','Beneficiario  0= No toma capacitacion 1=Toma capacitacion');
--------------------------------------------------------

COMMIT;

Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('261','Valor Per-capita de Salud  (Barras x CCF)','4','B','402','401','403','AVG','Total Liquidado en Salud Sobre el total de beneficiarios
',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('72','Beneficiarios de Liquidados (salud y pension)(Lineas x Periodo)','1','L','25','7','13','COUNT','N�mero de Beneficiarios liquidadados  por periodo','MONTO_LIQUIDADO_SALUD <> 0 and MONTO_LIQUIDADO_PENSION <> 0','3');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('82','Beneficiarios cuota monetaria  (Lineas Periodo)','1','L','25','7','10','COUNT','N�mero de Beneficiarios liquidados en Cuota Monetaria por periodo','MONTO_LIQUIDADO_CUOTA <> 0','3');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('271','Valor Per-capita de  Pensi�n  (Barras x CCF)','4','B','402','401','404','AVG','Total Liquidado en Pensi�n Sobre el total de beneficiarios
',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('331','Capacitaciones realizadas en la agce  (Barras x CCF)','2','B','224','221','225','COUNT','Numero de capacitaciones realizadas por la ccf.',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('341','Capacitaciones realizadas por tipo de Instituci�n  (Barras x Tipo de Institucion)','2','B','226','221','225','COUNT','Numero de capacitaciones realizas por tipo de instituci�n',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('351','Capacitaciones realizadas por tipo de formaci�n  (Barras x Tipos de Formacion)','2','B','223','221','225','COUNT','Numero de capacitaciones realizadas por tipo de formaci�n',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('361','Capacitaciones realizadas por  cine a.c.  (Barras x Cine)','2','B','227','221','225','COUNT','Numero de Capacitaciones realizadas por  cine a.c.',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('381','Por tipo de certificaci�n de programa  (Barras x  certificacion)','2','B','229','221','225','COUNT','Numero de capacitaciones realizadas por tipo de certificaci�n de programa',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('401','Personas ocupadas remitidas a este curso (Barras x Tipos de Formacion)','3','B','303','319','321','COUNT','Personas ocupadas remitidas a este curso','TIPO_CURSO is not null',null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('411','Personas que se remitieron al curso (Barras x Tipos de Formacion)','3','B','303','319','321','COUNT','N�mero total de personas que se remitieron al curso','TIPO_CURSO is not null',null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('421','Personas que desertaron (Barras x Tipos de Formacion)','3','B','303','319','321','COUNT','n�mero total de personas que desertaron','TIPO_CURSO is not null and estado=3',null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('431','Asistencia a la capacitaci�n  (Circulo)','3','D','319','303','317','COUNT','% de asistencia de las personas ','TIPO_CURSO is not null','321');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('451','Costo total del subsidio de transporte por tipo de programa  (Barras x Tipos de Formacion)','3','B','303','314','325','SUM','Costo total del subsidio de transporte por tipo de programa ','TIPO_CURSO is not null',null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('461','Otros costos asociados al curso por tipo de programa (Barras x Tipos de Formacion)','3','B','303','314','313','SUM','Otros costos asociados al curso por tipo de programa','TIPO_CURSO is not null',null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('21','Postulados No Aceptados
  (Barra x CCF)
','1','B','7','8','3','COUNT','Corresponde a las solicitudes Denegadas por CCF','verificacion_requisitos=2',null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('31','Beneficiarios (Postulados aceptados al Mecanismo)
  (Barras x CCF)
','1','B','7','8','3','COUNT','Se obtiene de las solicitudes Aprobadas por CCF','verificacion_requisitos=1',null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('41','Proporci�n de beneficiarios sobre el total de postulados (Circulo)','1','D','1','7','4','COUNT','Porcentaje de solicitudes aprobadas sobre radicadas
. 
1. Aprobado
2. Denegado
3. Desistido
4. Cancelado
5. En Proceso de Verificaci�n
',null,'3');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('51','Proporci�n de beneficiarios salientes del total de beneficiarios (Circulo)','1','D','25','7','26','COUNT','Porcentaje de beneficios finalizados por periodo de liquidaci�n de beneficios. 1. Termino con el beneficio 2. No ha terminado con el Beneficio','verificacion_requisitos=1','3');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('71','Beneficiarios de Liquidados (salud y pension)( Barras x CCF)','1','B','7','25','13','COUNT','N�mero de Beneficiarios liquidadados  por periodo','MONTO_LIQUIDADO_SALUD <> 0 and MONTO_LIQUIDADO_PENSION <> 0','3');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('91','Beneficiarios de incentivo de ahorro (Barras x CCF)','1','B','7','25','11','COUNT','N�mero de Beneficiarios liquidados en cesant�as por periodo','MONTO_LIQUIDADO_CESANTIAS <> 0','3');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('101','N�mero de Beneficiarios de Bono de Alimentaci�n (Barras x CCF)','1','B','7','25','12','COUNT','N�mero de Beneficiarios liquidados en bono de alimentaci�n por periodo','MONTO_LIQUIDADO_ALIMENTACION <> 0','3');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('111','Beneficiarios del incentivo de ahorro del total de beneficiarios del Mecanismo de Protecci�n al Cesante.  (Circulo)','10','D','348','347','349','COUNT','Porcentaje de beneficios liquidados por ahorro de cesant�as sobre el total de beneficios liquidados por CCF','verificacion_requisitos=1 and value <> 0',null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('201','Total liquidado en Salud (Barras x CCF)','1','B','7','25','13','SUM','Valor total liquidado en Salud, para cada CCF o consolidado por rango de periodos.',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('211','Total Liquidado en Pensi�n  (Barras x CCF)','1','B','7','25','14','SUM','Valor total liquidado en Pensi�n, para cada CCF o consolidado por rango de periodos.',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('221','Total Liquidado en Cuota Monetaria  (Barras x CCF)','1','B','7','25','10','SUM','Valor total liquidado en Cuota Monetaria, para cada CCF o consolidado por rango de periodos',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('231','Total Liquidado en Bono de Alimentaci�n  (Barras x CCF)','1','B','7','25','12','SUM','Valor total liquidado en Bono de Alimentaci�n, para cada CCF o consolidado por rango de periodos',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('241','Total Liquidado Por Ahorro de Cesant�as  (Barras x CCF)','1','B','7','25','11','SUM','Valor total liquidado por Ahorro de Cesant�as, para cada CCF o consolidado por rango de periodos',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('251','Total Prestaciones Econ�micas  (Barras x CCF)','1','B','7','25','27','SUM','Valor total de liquidaci�n  prestaciones econ�micas',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('281','Valor Per-capita de  Cuota    (Barras x CCF)','7','B','702','701','703','AVG','Total Liquidado en Cuota Monetaria Sobre el total de beneficiarios de cuota monetaria
',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('291','Valor Per-capita de  Bono de Alimentaci�n  (Barras x CCF)','5','B','501','502','503','AVG','Total Liquidado en Ahorro de Cesant�as Sobre el total de beneficiarios de ahorro de cesant�as
',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('301','Valor Per-capita Liquidaci�n  Ahorro de Cesant�as  (Barras x CCF)','6','B','601','602','603','AVG','Total Liquidado en Ahorro de Cesant�as Sobre el total de beneficiarios de ahorro de cesant�as
',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('321','Valor Per-capita de Cuota Monetaria por dependientes econ�micos   (Barras x CCF)','9','B','901','902','903','AVG','Valor per-capita de cuota monetaria Sobre el total de dependientes econ�micos del beneficiario del subsidio
',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('11','Postulados al Mecanismo (Barra x CCF)','1','B','7','1','3','COUNT','Corresponde a las solicitudes radicadas en el sistema por CCF',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('22','Postulados No Aceptados
  (Lineas x Periodo)

','1','L','8','7','3','COUNT','Corresponde a las solicitudes Denegadas por periodo','verificacion_requisitos=2',null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('32','Beneficiarios (Postulados aceptados al Mecanismo)  (Linea x Periodo)','1','L','8','7','3','COUNT','Se obtiene de las solicitudes Aprobadas por periodo','verificacion_requisitos=1',null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('102','N�mero de Beneficiarios de Bono de Alimentaci�n  (Lineas x Periodo)','1','L','25','7','12','COUNT','N�mero de Beneficiarios liquidados en bono de alimentaci�n por periodo','MONTO_LIQUIDADO_ALIMENTACION <> 0','3');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('131','Porcentaje de ahorro para cesant�as (Circulo)','10','D','348','347','349','COUNT','Porcentaje de ahorro para cesant�as (Circulo)','verificacion_requisitos=1 and value <> 0',null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('141','Beneficiarios de las Agencias de Gesti�n y Colocaci�n (todos los inscritos a agencia)','1','B','7','25','3','COUNT','N�mero de beneficiarios que se inscribieron  a la Agencia de Gesti�n y Colocaci�n.
','verificacion_requisitos=1',null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('161','Proporci�n de beneficiarios de incentivo sobre el total de beneficiarios','3','D','319','304','326','COUNT','Proporci�n de los beneficiarios que se inscribieron a la Agencia de Gesti�n y Colocaci�n (B2). La variable B corresponde a los beneficios liquidados en Zenith por periodo) 
1. inscritos en una capacitacion
0. No inscritos
',null,'324');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('181','Beneficiarios por tipo de curso de capacitaci�n','3','D','319','303','303','COUNT','Proporci�n de beneficiarios por tipo de curso del total de beneficiarios que asisten a un curso, donde i=[t�cnico laboral, validaci�n, educaci�n continua, alfabetizaci�n
','verificacion=1 and TIPO_CURSO is not null','321');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('191','Beneficiarios de capacitaci�n que terminaron','3','D','319','303','317','COUNT','Proporci�n de beneficiarios que finalizaron el curso de capacitaci�n

1- Finalizado
2 -En curso
3- Desertado
4- Inscrito','TIPO_CURSO is not null','321');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('202','Total liquidado en Salud (Lineas x Periodo)','1','L','25','7','13','SUM','Valor total liquidado en Salud, para cada CCF o consolidado por rango de periodos.',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('212','Total Liquidado en Pensi�n (Lineas x Periodo)','1','L','25','7','14','SUM','Valor total liquidado en Pensi�n, para cada CCF o consolidado por rango de periodos.',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('222','Total Liquidado en Cuota Monetaria (Lineas x Periodo)','1','L','25','7','10','SUM','Valor total liquidado en Cuota Monetaria, para cada CCF o consolidado por rango de periodos',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('232','Total Liquidado en Bono de Alimentaci�n (Lineas x Periodo)','1','L','25','7','12','SUM','Valor total liquidado en Bono de Alimentaci�n, para cada CCF o consolidado por rango de periodos',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('242','Total Liquidado Por Ahorro de Cesant�as  (Lineas x Periodo)','1','L','25','7','11','SUM','Valor total liquidado por Ahorro de Cesant�as, para cada CCF o consolidado por rango de periodos',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('252','Total Prestaciones Econ�micas (Lineas x Periodo)','1','L','25','7','27','SUM','Valor total de liquidaci�n  prestaciones econ�micas',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('371','Por necesidad de formaci�n la capacitaci�n  (Barras x necesidad)','2','B','228','221','225','COUNT','Numero de capacitaciones realizadas por necesidad de formaci�n ',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('441','Costo total de la matricula por tipo de programa (Barras x Tipos de Formacion)','3','B','303','314','312','SUM','Costo total de la matricula por tipo de programa','TIPO_CURSO is not null',null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('12','Postulados al Mecanismo (Lineas x Periodo)','1','L','1','7','3','COUNT','Corresponde a las solicitudes radicadas en el sistema por periodo',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('81','Beneficiarios cuota monetaria ( Barras x CCF)','1','B','7','25','10','COUNT','N�mero de Beneficiarios liquidados en Cuota Monetaria por periodo','MONTO_LIQUIDADO_CUOTA <> 0','3');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('121','Proporci�n Beneficiarios de cuota monetaria ( Circulo)','10','D','348','347','349','COUNT','Porcentaje de Beneficiarios que se les liquidada Cuota Monetaria sobre le total de beneficios liquidados por CCF por periodo y CCF
','verificacion_requisitos=1 and value <> 0',null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('311','Valor Per-capita Prestaciones Econ�micas  (Barras x CCF)','8','B','801','802','803','AVG','Total Liquidado por prestaciones econ�micas Sobre el total de beneficiarios
',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('92','Beneficiarios de incentivo de ahorro (Lineas x Periodo)','1','L','25','7','11','COUNT','N�mero de Beneficiarios liquidados en cesant�as por periodo','MONTO_LIQUIDADO_CESANTIAS <> 0','3');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('151','Beneficiarios de capacitaci�n (si una persona esta en 3 se reporta una sola vez por persona) ( Barras x tipo de formacion)','3','B','303','319','321','COUNT','N�mero de beneficiarios que reciben los servicios de capacitaci�n.
',null,null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION,DESCRIPCION,CONDICION,VAR_FUNCION) values ('171','Beneficiarios de Capacitaci�n del total de beneficiarios','3','D','319','304','326','COUNT','Proporci�n de beneficiarios que reciben los servicios de capacitaci�n del total de beneficiarios. (La variable B corresponde a los beneficios liquidados en Zenith por periodo)
1. inscritos en una capacitacion
0. No inscritos','verificacion=1','324');
--------------------------------------------------------

COMMIT;








