/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ApprovalService.java
 * Created on: 2016/11/30, 01:08:41 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.entity.Approval;
import com.gml.simpc.api.entity.Institution;
import com.gml.simpc.api.entity.OptionsTemplate;
import java.util.List;

/**
 * Interface que define las operaciones del servicio de aprobaciones.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface ApprovalService {

    /**
     * Encontrar lista Causales de desaprobacion de una institucion
     * @param institution
     * @return 
     */
    List<Approval> findByInstitution(Institution institution);

    /**
     * Servicio para guardar una aprovaci&oacute;n.
     *
     * @param approval
     */
    void save(Approval approval);

    /**
     * Servicio para guardar una plantilla de aprobacion
     *
     * @param template
     */
    void saveTemplate(OptionsTemplate template);

    /**
     * Servicio para recuperar la plantilla por tipo
     * @return 
     */
    public OptionsTemplate findTemplate();

}
