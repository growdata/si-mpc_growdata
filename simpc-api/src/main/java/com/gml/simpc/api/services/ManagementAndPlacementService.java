/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ActionLogService.java
 * Created on: 2016/11/15, 10:46:55 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.dto.ManagementAndPlacementDto;
import java.util.Date;
import java.util.List;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public interface ManagementAndPlacementService {

    /**
     * Consulta Gestion y colocacion
     */
    List<ManagementAndPlacementDto> findManagementAndPlacementResults(Date init,
        Date finish,String idStatusPostulation);

    

}
