/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Indicator.java
 * Created on: 2016/12/12, 03:28:43 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Entidad que define la estructura de los indicadores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Table(name = "IND_FUENTES")
@Entity
public class IndicatorSource implements Serializable {

    /**
     * ID.
     */
    @Id
    @Column(name = "ID", length = 10)
    private Long id;
    
    /**
     * Nombre.
     */
    @Column(name = "NOMBRE", length = 50)
    private String name;
    
    /**
     * Tabla de donde salen los datos.
     */
    @Column(name = "TABLA", length = 50)
    private String table;
    
    /**
     * Lista de variables.
     */
    @OneToMany(mappedBy = "indicatorSource", fetch = FetchType.EAGER)
    private List<IndicatorVariable> variables;

    /**
     * Constructor por defecto.
     */
    public IndicatorSource() {
        this.id = null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public List<IndicatorVariable> getVariables() {
        return variables;
    }

    public void setVariables(List<IndicatorVariable> variables) {
        this.variables = variables;
    }

    @Override
    public String toString() {
        return "IndicatorSource{" + "id=" + id + ", name=" + name + ", table=" + 
                table + ", variables=" + variables.size() + '}';
    }

    
    
}
