/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CommonsValidator.java
 * Created on: 2016/11/11, 10:33:45 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 * Objetive: Write success program detail recors to database.
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.writer;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemWriter;

/**
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class DefaultWriter implements ItemWriter<Object> {
    
    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(DefaultWriter.class);

    @Override
    public void write(List<? extends Object> items) throws Exception {
        if (LOGGER.isDebugEnabled()) { 
            for (Object detail : items) { 
                LOGGER.info("Writting item ... " + detail.toString()); 
            } 
        }
    }

}
