/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: ProgramDto.java
 * Created on: 2016/12/19, 04:23:26 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * Objective: map the required fields into a load of training or orientation
 * programs.
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

/**
 * Class to map training and orientation load data. This includes program and
 * module fields.
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica Jim&eacute;nez Rozo</a>
 */
public class ProgramDto {
    
    private String programCode;
    private String programName;
    private String programType;
    private String courseType;
    private String ccfCode;
    private String instCode;
    private String headqCode;
    private String stateCode;
    private String cityCode;
    private String certified;
    private String certName;
    private String otherCertName;
    private String issuedCertName;
    private String cine;
    private String necesity;
    private String performance;
    private String moduleCode;
    private String moduleName;
    private String totalHours;
    private String schedule;
    private String enrollmentCostType;
    private String enrollmentCost;
    private String otherCostType;
    private String otherCost;
    private String startDate;
    private String endDate;
    
    private Long cargaId;
    
    /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

    public ProgramDto() {
    }

    /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    
    public String getProgramCode() {
        return programCode;
    }

    public void setProgramCode(String programCode) {
        this.programCode = programCode;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getProgramType() {
        return programType;
    }

    public void setProgramType(String programType) {
        this.programType = programType;
    }

    public String getCourseType() {
        return courseType;
    }

    public void setCourseType(String courseType) {
        this.courseType = courseType;
    }

    public String getPerformance() {
        return performance;
    }

    public void setPerformance(String performance) {
        this.performance = performance;
    }

    public String getCcfCode() {
        return ccfCode;
    }

    public void setCcfCode(String ccfCode) {
        this.ccfCode = ccfCode;
    }

    public String getInstCode() {
        return instCode;
    }

    public void setInstCode(String instCode) {
        this.instCode = instCode;
    }

    public String getHeadqCode() {
        return headqCode;
    }

    public void setHeadqCode(String headqCode) {
        this.headqCode = headqCode;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCertified() {
        return certified;
    }

    public void setCertified(String certified) {
        this.certified = certified;
    }

    public String getCertName() {
        return certName;
    }

    public void setCertName(String certName) {
        this.certName = certName;
    }
    
    public String getOtherCertName() {
        return otherCertName;
    }

    public void setOtherCertName(String otherCertName) {
        this.otherCertName = otherCertName;
    }
    
    public String getIssuedCertName() {
        return issuedCertName;
    }

    public void setIssuedCertName(String issuedCertName) {
        this.issuedCertName = issuedCertName;
    }

    public String getCine() {
        return cine;
    }

    public void setCine(String cine) {
        this.cine = cine;
    }

    public String getNecesity() {
        return necesity;
    }

    public void setNecesity(String necesity) {
        this.necesity = necesity;
    }

    public String getModuleCode() {
        return moduleCode;
    }

    public void setModuleCode(String moduleCode) {
        this.moduleCode = moduleCode;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(String totalHours) {
        this.totalHours = totalHours;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getEnrollmentCostType() {
        return enrollmentCostType;
    }

    public void setEnrollmentCostType(String enrollmentCostType) {
        this.enrollmentCostType = enrollmentCostType;
    }

    public String getEnrollmentCost() {
        return enrollmentCost;
    }

    public void setEnrollmentCost(String enrollmentCost) {
        this.enrollmentCost = enrollmentCost;
    }

    public String getOtherCostType() {
        return otherCostType;
    }

    public void setOtherCostType(String otherCostType) {
        this.otherCostType = otherCostType;
    }

    public String getOtherCost() {
        return otherCost;
    }

    public void setOtherCost(String otherCost) {
        this.otherCost = otherCost;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Long getCargaId() {
        return cargaId;
    }

    public void setCargaId(Long cargaId) {
        this.cargaId = cargaId;
    }
    
    
    
    

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

    @Override
    public String toString() {
            StringBuilder stringBuilder= new StringBuilder();
            stringBuilder.append("CargaId=").append(cargaId).
                append("~&~C�digo de Programa=").append( programCode ).
                append("~&~Nombre de Programa=").append(programName ).
                append("~&~Tipo de Capacitaci�n=").append( programType ).
                append("~&~Tipo de Curso=").append(courseType ).
                append("~&~C�digo CCF=").append( ccfCode ).
                append("~&~C�digo Inst=").append( instCode ).
                append("~&~C�digo Sede=").append( headqCode ).
                append("~&~C�digo Departamento=").append( stateCode ).
                append("~&~C�digo Municipio=").append( cityCode ).
                append("~&~Certificado=").append( certified ).
                append("~&~Nombre Certificaci�n=").append( certName ).
                append("~&~Nombre de Otra Certificaci�n=").append( otherCertName ).
                append("~&~Certificaci�n Emitida=").append( issuedCertName ).
                append("~&~Cine=").append( cine ).
                append("~&~Necesidad=").append( necesity ).
                append("~&~Desempe�o=").append( performance ).
                append("~&~C�digo M�dulo=").append( moduleCode ).
                append("~&~Nomobre M�dulo=").append( moduleName ).
                append("~&~Total de Horas=").append( totalHours ).
                append("~&~Horario=").append( schedule ).
                append("~&~Tipo Costo=").append(enrollmentCostType ).
                append("~&~Costo Matricula=").append( enrollmentCost ).
                append("~&~Otro Costo Tipo=").append( otherCostType ).
                append("~&~Otro Costo=").append( otherCost ).
                append("~&~Fecha de inicio=").append( startDate ).
                append("~&~Fecha Fin=").append( endDate );
            return stringBuilder.toString();
    }

    
}
