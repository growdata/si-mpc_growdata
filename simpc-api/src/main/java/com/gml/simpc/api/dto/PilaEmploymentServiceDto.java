/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: PilaDto.java
 * Created on: 2016/12/27, 04:23:26 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Leonardo Rocha</a>
 */
public class PilaEmploymentServiceDto {

    String documentType;
    String numberIdentification;
    String tipoIdAportanteU;
    String numberIdentificationContributingU;
    String contributingNameU;
    String maxDateU;
    String primerPeriodoU;
    Long id_Carga;

    public Long getId_Carga() {
        return id_Carga;
    }

    public void setId_Carga(Long id_Carga) {
        this.id_Carga = id_Carga;
    }

    public String getMaxDateU() {
        return maxDateU;
    }

    public void setMaxDateU(String maxDateU) {
        this.maxDateU = maxDateU;
    }

    public String getContributingNameU() {
        return contributingNameU;
    }

    public void setContributingNameU(String contributingNameU) {
        this.contributingNameU = contributingNameU;
    }

    public String getNumberIdentification() {
        return numberIdentification;
    }

    public void setNumberIdentification(String numberIdentification) {
        this.numberIdentification = numberIdentification;
    }

    public String getNumberIdentificationContributingU() {
        return numberIdentificationContributingU;
    }

    public void setNumberIdentificationContributingU(
        String numberIdentificationContributingU) {
        this.numberIdentificationContributingU =
            numberIdentificationContributingU;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getTipoIdAportanteU() {
        return tipoIdAportanteU;
    }

    public void setTipoIdAportanteU(String tipoIdAportanteU) {
        this.tipoIdAportanteU = tipoIdAportanteU;
    }

    public String getPrimerPeriodoU() {
        return primerPeriodoU;
    }

    public void setPrimerPeriodoU(String primerPeriodoU) {
        this.primerPeriodoU = primerPeriodoU;
    }

    public PilaEmploymentServiceDto(String documentType,
        String numberIdentification, String tipoIdAportanteU,
        String numberIdentificationContributingU, String contributingNameU,
        String maxDateU, String primerPeriodoU, Long id_Carga) {
        this.documentType = documentType;
        this.numberIdentification = numberIdentification;
        this.tipoIdAportanteU = tipoIdAportanteU;
        this.numberIdentificationContributingU =
            numberIdentificationContributingU;
        this.contributingNameU = contributingNameU;
        this.maxDateU = maxDateU;
        this.primerPeriodoU = primerPeriodoU;
        this.id_Carga = id_Carga;
    }

    public PilaEmploymentServiceDto() {
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Id_Carga=").append(id_Carga).
            append("~&~tipoDocumento=").append(documentType).
            append("~&~numeroDocumento=").append(numberIdentification).
            append("~&~Tipo Identificación Aportante Último Periodo =").append(tipoIdAportanteU).
            append("~&~numeroDocumentoAportanteU=").append(numberIdentificationContributingU).
            append("~&~nombreAportanteU=").append(contributingNameU).
            append("~&~maxFechaU=").append(maxDateU).
            append("~&~Primer Periodo cotizado con último aportante = ").append(primerPeriodoU);
        return stringBuilder.toString();
    }

}
