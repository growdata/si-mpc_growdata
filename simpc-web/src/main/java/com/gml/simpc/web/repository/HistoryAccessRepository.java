/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ActionLogRepository.java
 * Created on: 2016/11/15, 10:52:58 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository;

import com.gml.simpc.api.entity.HistoryAccess;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public interface HistoryAccessRepository extends
    JpaRepository<HistoryAccess, String> {

    /**
     * Consulta de historial de accesos
     * @param initDate
     * @param finishDate
     * @param documento
     * @param userName
     * @param idCCf
     * @return 
     */
    @Query(value =
        "Select ha.id, ha.fecha_acceso, ha.id_usuario  ,ha.origen from Historico_accesos " +
        "ha inner join usuarios us on (us.ID=ha.ID_USUARIO) where " +
        "us.NOMBRE_USUARIO like ?4 and us.NUMERO_IDENTIFICACION like ?3 " +
        "and us.CAJA_COMPENSACION like ?5 and ha.FECHA_ACCESO >= ?1 and " +
        "ha.FECHA_ACCESO <= ?2 ",
        nativeQuery = true)
    List<HistoryAccess> getHistoricAccessUser(Date initDate,
        Date finishDate, String documento, String userName, String idCCf);

}
