/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Institution.java
 * Created on: 2016/11/19, 11:47:45 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gml.simpc.api.entity.valuelist.DocumentType;
import com.gml.simpc.api.entity.valuelist.InstitutionType;
import com.gml.simpc.api.entity.valuelist.TypeQualityCertificate;
import com.gml.simpc.api.enums.ApprovalInstitutionStatus;
import com.gml.simpc.api.enums.InstitutionStatus;
import com.gml.simpc.api.enums.LegalNature;
import com.gml.simpc.api.enums.Origin;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;

/**
 * Entidad que representa la instituci&oacute;n.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Table(name = "INSTITUCIONES", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"NOMBRE_INSTITUCION"},
        name = Institution.UQ_NOMBRE_INSTITUCION),
    @UniqueConstraint(columnNames = {"TIPO_DOCUMENTO", "NIT"},
        name = Institution.UQ_NUMERO_DOCUMENTO),})
@Entity
public class Institution implements Serializable {

    public static final String UQ_NOMBRE_INSTITUCION =
        "UQ_NOMBRE_INSTITUCION";
    public static final String UQ_NUMERO_DOCUMENTO = "UQ_NUMERO_DOCUMENTO";

    /**
     * ID de la instituci&oacute;n.
     */
    @Id
    @Column(name = "ID", length = 10)
    @GeneratedValue(strategy = GenerationType.AUTO, generator =
        "institution_seq_gen")
    @SequenceGenerator(name = "institution_seq_gen", sequenceName =
        "INSTITUTION_SEQ", initialValue = 10001, allocationSize = 1)
    private Long id;
    /**
     * tipo de instituci&oacute;n.
     */
    @ManyToOne
    @JoinColumn(name = "TIPO_INSTITUCION_ID", nullable = false)
    private InstitutionType type;
    /**
     * tipo de instituci&oacute;n.
     */
    @ManyToOne
    @JoinColumn(name = "TIPO_DOCUMENTO", nullable = false)
    private DocumentType documentType;
    /**
     * nit o c&eacute;dula de la instituci&oacute;n.
     */
    @Column(name = "NIT", length = 10, nullable = true)
    private String nit;
    /**
     * digito de verificaci&oacute;n
     */
    @Column(name = "DIGITO_VERIFICACION", length = 1)
    private Integer dv;
    /**
     * nombre de la instituci&oacute;n.
     */
    @NotNull(message = "El campo Nombre de Instituci�n no puede estar vac�o.")
    @Size(max = 50, message = "Nombre de Instituci�n no puede tener m�s de " +
        "50 caracteres.")
    @Column(name = "NOMBRE_INSTITUCION", length = 50, nullable = false)
    private String institutionName;
    /**
     * digito de verificaci&oacute;n
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "ORIGEN", columnDefinition = "char(1) default 'R'",
        nullable = false)
    private Origin origin = Origin.R;
    /**
     * estado de aprobacion de la instituci&oacute;n (En tr&aacute;mite -
     * Aprobada - No
     * aprobada).
     */
    @Enumerated(EnumType.STRING)
    
    @Column(name = "ESTADO_APROBACION", columnDefinition = "char(1) default 'N'",
        nullable = false)
    private ApprovalInstitutionStatus approvalStatus =
        ApprovalInstitutionStatus.N;
    /**
     * estado de la instituci&oacute;n (En tr&aacute;mite - Aprobada - No
     * aprobada).
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "ESTADO", columnDefinition = "char(1) default 'A'",
        nullable = false)
    private InstitutionStatus status = InstitutionStatus.A;
    /**
     * naturaleza jur&iacute;dica.
     */
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "NATURALEZA_JURIDICA", columnDefinition =
        "number(1) default 1",
        nullable = false)
    private LegalNature legalNature = LegalNature.N;
    /**
     * Certificaci&oacute;n de calidad en formato pdf de la instituci&oacute;n.
     */
    @Basic(fetch = FetchType.LAZY)
    @Lob
    @Column(name = "CERTIFICACION_CALIDAD")
    private byte[] qualityCertificate;
    /**
     * Fecha de vencimiento de la certificaci&oacute;n
     */
    @Column(name = "VENCIMIENTO_CERTIFICACION")
    @Temporal(TemporalType.DATE)
    private Date qualityCertificateExpiration;
    /**
     * Tel&eacute;fono de contacto de la instituci&oacute;n.
     */
    @Column(name = "NUMERO_TELEFONO", length = 15)
    private String contactPhone;
    /**
     * Correo electr&oacute;nico de la instituci&oacute;n.
     */
    @Email(message = "No es una direcci�n de correo v�lida.")
    @NotNull(message = "El campo Email no puede estar vac�o.")
    @Size(max = 50, message = "El Email no puede tener m�s de 50 caracteres.")
    @Column(name = "CORREO_ELECTRONICO", length = 50)
    private String contactEmail;
    /**
     * Id de Usuario de creaci&oacute:n. Campo de Auditor&iacute;a.
     */
    @ManyToOne
    @JoinColumn(name = "ID_USUARIO_CREACION", nullable = false)
    private User creationUserId;
    
    /**
     * Id de Usuario de creaci&oacute:n. Campo de tipo de certificacion.
     */
    @ManyToOne
    @JoinColumn(name = "TIPO_CERTIFICACION", nullable = true)
    private TypeQualityCertificate typeQualityCertificate;
    /**
     * fecha de creaci&oacute:n. Campo de Auditor&iacute;a.
     */
    @Column(name = "FECHA_CREACION", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    /**
     * Id de Usuario de modificaci&oacute;n.Campo de Auditor&iacute;a.
     */
    @ManyToOne
    @JoinColumn(name = "ID_USUARIO_MODIFICACION")
    private User modificationUserId;
    /**
     * fecha de modificaci&oacute:n. Campo de Auditor&iacute;a.
     */
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modificationDate;
    /**
     * fecha de aprobaci&oacute;n
     */
    @Column(name = "FECHA_APROBACION", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date approvedDate;

    /**
     * Sede para registro inicial.
     */
    @Transient
    private Headquarter headquarter;

    /**
     * Archivo cargado para el certificado de calidad.
     */
    @JsonIgnore
    @Transient
    private Object file;

    /**
     * Fecha de expiraci&oacute;n del certificado en formato de texto.
     */
    @Transient
    private String dateToShow;

    /**
     * Constructor por defecto.
     */
    public Institution() {
        this.id = null;
    }

    /**
     * Constructor por id y nombre.
     *
     * @param id
     * @param institutionName
     */
    public Institution(Long id, String institutionName) {
        this.id = id;
        this.institutionName = institutionName;
    }

    public Institution(Long id) {
        this.id = id;
    }

    /**
     * Constructor por sede.
     *
     * @param headquarter
     */
    public Institution(Headquarter headquarter) {
        this.headquarter = headquarter;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public InstitutionStatus getStatus() {
        return status;
    }

    public void setStatus(InstitutionStatus status) {
        this.status = status;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public InstitutionType getType() {
        return type;
    }

    public void setType(InstitutionType type) {
        this.type = type;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public Integer getDv() {
        return dv;
    }

    public void setDv(Integer dv) {
        this.dv = dv;
    }

    public Origin getOrigin() {
        return origin;
    }

    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    public LegalNature getLegalNature() {
        return legalNature;
    }

    public void setLegalNature(LegalNature legalNature) {
        this.legalNature = legalNature;
    }

    public byte[] getQualityCertificate() {
        return qualityCertificate;
    }

    public void setQualityCertificate(byte[] qualityCertificate) {
        this.qualityCertificate = qualityCertificate;
    }

    public Date getQualityCertificateExpiration() {
        return qualityCertificateExpiration;
    }

    public void setQualityCertificateExpiration(
        Date qualityCertificateExpiration) {
        this.qualityCertificateExpiration = qualityCertificateExpiration;
    }

    public User getCreationUserId() {
        return creationUserId;
    }

    public void setCreationUserId(User creationUserId) {
        this.creationUserId = creationUserId;
    }
    
    public TypeQualityCertificate getTypeQualityCertificate() {
        return typeQualityCertificate;
    }

    public void setTypeQualityCertificate(TypeQualityCertificate typeQualityCertificate) {
        this.typeQualityCertificate = typeQualityCertificate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public User getModificationUserId() {
        return modificationUserId;
    }

    public void setModificationUserId(User modificationUserId) {
        this.modificationUserId = modificationUserId;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Headquarter getHeadquarter() {
        return headquarter;
    }

    public void setHeadquarter(Headquarter headquarter) {
        this.headquarter = headquarter;
    }

    public Object getFile() {
        return file;
    }

    public void setFile(Object file) {
        this.file = file;
    }

    public String getDateToShow() {
        return dateToShow;
    }

    public void setDateToShow(String dateToShow) {
        this.dateToShow = dateToShow;
    }

    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setAprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    public ApprovalInstitutionStatus getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(ApprovalInstitutionStatus approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Instituci�n {").append("id=").append(id).
            append(", Tipo=").append(type.getName()).
            append(", Nit=").append(nit).
            append(", Digito de verificaci�n=").append(dv).
            append(", Nombre=").append(institutionName).
            append(", Origen=").append(origin).
            append(", Estado=").append(status.getLabel()).
            append(", Naturaleza Legal=").append(legalNature.getLabel()).
            append(", Tel�fono de Contacto=").append(contactPhone).
            append(", Correo Electr�nico=").append(contactEmail).
            append(", Creado por Usuario=").append(creationUserId.getUsername()).
            append(", Fecha de creaci�n=").append(creationDate).
            append(", Modificado por Usuario=").append(modificationUserId.
            getUsername()).
            append(", Fecha de modificaci�n=").append(modificationDate).
            append('}');
            
        return stringBuilder.toString();
    }
}
