/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: TrainingResourcesDaoImpl.java
 * Created on: 2017/04/11, 02:56:42 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.dao.impl;

import com.gml.simpc.api.dao.TrainingResourcesDao;
import com.gml.simpc.api.dto.TrainingDto;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
@Repository
public class TrainingResourcesDaoImpl implements TrainingResourcesDao {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(TrainingResourcesDaoImpl.class);

    /**
     * Plantilla JDBC para consultar las propiedades.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    
    /**
     * Consulta para contadores de modulos para un tipo de institucion
     * especifico
     */
    private static final String FIND_COSTS_BY_CCF_PERIOD ="SELECT CAP.TIPOFORMACION TIPOFORMACION , " +
        "NVL(SUM (CAP.MATRICULA),0) MATRICULA, " +
        "NVL(SUM (CAP.TRANSPORTE),0) TRANSPORTE, " +
        "NVL(SUM (CAP.OTROS),0) OTROS " +
        "FROM VW_COSTOS_RECURSOS CAP " +
        "WHERE (CODIGO_CCF LIKE ?) AND TO_CHAR(FECHA_INICIO,'MM' )LIKE ? AND TO_CHAR(FECHA_INICIO,'yyyy' )LIKE ? " +
        "GROUP BY CAP.TIPOFORMACION ";
    
    /**
     * Consulta para contadores de modulos para un tipo de institucion
     * especifico
     */
    private static final String FIND_COSTS_BY_CCF = "SELECT CAP.TIPOFORMACION TIPOFORMACION , " +
        "NVL(SUM (CAP.MATRICULA),0) MATRICULA, " +
        "NVL(SUM (CAP.TRANSPORTE),0) TRANSPORTE, " +
        "NVL(SUM (CAP.OTROS),0) OTROS " +
        "FROM VW_COSTOS_RECURSOS CAP " +
        "WHERE (CODIGO_CCF LIKE ?) AND TO_CHAR(FECHA_INICIO,'MM' )LIKE ? AND TO_CHAR(FECHA_INICIO,'yyyy' )LIKE ? " +
        "GROUP BY CAP.TIPOFORMACION ";
    
    /**
     * Metodo para obtenet los costos segun el filtro de recursos de
     * capacitacion
     */
    @Override
    public List<TrainingDto> getCostsByPeriod(String ccf, String year,
        String month){
         LOGGER.info("VALOR DEL CCF ****" + ccf+"anio"+year+"mes"+month);
        
    return this.jdbcTemplate.query(FIND_COSTS_BY_CCF_PERIOD, new RowMapper() {
            @Override
            public TrainingDto mapRow(ResultSet rs, int rownumber) throws
                SQLException {
                
                TrainingDto dto = new TrainingDto();
                dto.setTipoCapNombre(rs.getString("TIPOFORMACION"));
                dto.setEnrollmentCost(rs.getBigDecimal("MATRICULA"));
                dto.setTransportCost(rs.getBigDecimal("TRANSPORTE"));
                dto.setOtherCost(rs.getBigDecimal("OTROS"));
                LOGGER.info(dto.getEnrollmentCost()+"matricula****"+dto.getTransportCost()+"transporte"+dto.getOtherCost()+"otros****");
                dto.setTotalCost();
                 LOGGER.info(dto.getTotalCost());

                return dto;
            }
        }, ccf,month,year);
    }
    
    
    /**
     * Metodo para obtenet los costos segun el filtro de recursos de
     * capacitacion
     */
    @Override
    public List<TrainingDto> getCostsByCcf(String ccf,String year,String month) {
        return this.jdbcTemplate.query(FIND_COSTS_BY_CCF, new RowMapper() {
            @Override
            public TrainingDto mapRow(ResultSet rs, int rownumber) throws
                SQLException {
                TrainingDto dto = new TrainingDto();
                dto.setTipoCapNombre(rs.getString("TIPOFORMACION"));
                dto.setEnrollmentCost(rs.getBigDecimal("MATRICULA"));
                dto.setTransportCost(rs.getBigDecimal("TRANSPORTE"));
                dto.setOtherCost(rs.getBigDecimal("OTROS"));
                LOGGER.info(dto.getEnrollmentCost()+"*********"+dto.getTransportCost()+"transporte****"+dto.getOtherCost()+"tros");
                dto.setTotalCost();
                 LOGGER.info(dto.getTotalCost());

                return dto;
            }
        }, ccf,month,year);
    }

   

}
