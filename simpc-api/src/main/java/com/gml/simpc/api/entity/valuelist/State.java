/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: State.java
 * Created on: 2016/10/19, 01:48:45 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity.valuelist;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad que representa el departamento.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Table(name = "DEPARTAMENTOS")
@Entity
public class State implements Serializable {

    /**
     * ID del departamento
     */
    @Id
    @Column(name = "ID")
    private Long id;

    /**
     * nombre del departamento.
     */
    @Column(name = "NOMBRE", nullable = false)
    private String name;
    /**
     * codigo divipola del &aacute;rea administrativa.
     */
    @Column(name = "DIVIPOLA")
    private String divipola;

    /**
     * Constructor por defecto.
     */
    public State() {
        this.id = null;
    }

    /**
     * Constructor para crear un departamento con nombre.
     * @param name 
     */
    public State(String name) {
        this.name = name;
    }

    public State(String divipola, String name) {
        this.divipola=divipola;
        this.name=name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDivipola() {
        return divipola;
    }

    public void setDivipola(String divipola) {
        this.divipola = divipola;
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final State other = (State) obj;
        return Objects.equals(this.name, other.name);
    }
}
