/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: PilaDtoRowMapper.java
 * Created on: 2017/02/13, 11:30:02 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.dao.mapper;

import com.gml.simpc.api.dto.PilaEmploymentServiceDto;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 * Row Mapper para PILA.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class PilaEmploymentServiceDtoRowMapper implements
    RowMapper<PilaEmploymentServiceDto> {

    @Override
    public PilaEmploymentServiceDto mapRow(ResultSet rs, int i) throws
        SQLException {
        PilaEmploymentServiceDto e = new PilaEmploymentServiceDto();
        e.setId_Carga(rs.getLong("ID_CARGA"));
        e.setTipoIdAportanteU(rs.getString("TIPOIDAPORTANTEU"));
        e.setMaxDateU(rs.getString("MAXFECHAU"));
        e.setContributingNameU(rs.getString("NOMBREAPORTANTEU"));
        e.setNumberIdentification(rs.getString("NUMERODOCUMENTO"));
        e.setNumberIdentificationContributingU(rs.getString("NUMIDAPORTANTEU"));
        e.setDocumentType(rs.getString("TIPODOCUMENTO"));
        e.setPrimerPeriodoU(rs.getString("PRIMERPERIODOU"));
        return e;
    }
}
