/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IndicatorConfigurationServiceImpl.java
 * Created on: 2016/12/16, 10:31:31 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.dao.IndicatorDao;
import com.gml.simpc.api.dto.Page;
import com.gml.simpc.api.dto.TableDataDto;
import com.gml.simpc.api.entity.IndicatorConfiguration;
import com.gml.simpc.api.entity.IndicatorSource;
import com.gml.simpc.api.entity.IndicatorVariable;
import com.gml.simpc.api.services.IndicatorConfigurationService;
import com.gml.simpc.web.repository.IndicatorConfigurationRepository;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Servicio para manipular indicadores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Service
public class IndicatorConfigurationServiceImpl implements
        IndicatorConfigurationService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER
            = Logger.getLogger(IndicatorConfigurationServiceImpl.class);
    /**
     * Constante para la palabra tabla.
     */
    private static final String TABLE = "tabla";
    /**
     * Constante para la palabra campo.
     */
    private static final String FIELD = "campo";
    /**
     * Constante para la palabra periodo.
     */
    private static final String PERIODO = "periodo";
    /**
     * Tipo de grafico lineas.
     */
    private static final String LINES = "L";
    /**
     * Tipo de grafico barras.
     */
    private static final String BARS = "B";
    /**
     * Repositorio de indicadores.
     */
    @Autowired
    private IndicatorConfigurationRepository indicatorConfigurationRepository;
    /**
     * Dao de indicadores.
     */
    @Autowired
    private IndicatorDao indicatorDao;

    /**
     * Devuelve una lista de todos los indicadores configurados.
     *
     * @return
     */
    @Override
    public List<IndicatorConfiguration> findAll() {
        LOGGER.info("Ejecutando metodo [ findAll ]");
        List<IndicatorConfiguration> indicatorList
                = this.indicatorConfigurationRepository.findIndicator();
        return indicatorList;
    }

    /**
     * Devuelve el indicador seleccionado.
     *
     * @param id
     *
     * @return
     */
    @Override
    public IndicatorConfiguration findOne(Long id) {
        IndicatorConfiguration indicatorConfig
                = this.indicatorConfigurationRepository.findOne(id);
        return indicatorConfig;
    }

    /**
     * Se realiza la consulta para la realizaci&ocute;n del grafico.
     *
     * @param indicator
     *
     * @return
     */
    @Override
    public Object getDataGraphic(IndicatorConfiguration indicator) {

        String graphicTypeSel = indicator.getGraphicType();
        String query;

        switch (indicator.getId().intValue()) {

            case 51:
                query = getQueryFor51(indicator);
                break;
            case 161:
                query = getQueryFor161(indicator);
                break;
            case 171:
                query = getQueryFor171(indicator);
                break;
            case 471:
                query = getQueryFor471(indicator);
                break;
            case 481:
                query = getQueryFor481(indicator);
                break;
            default:
                query = generateDataGraphicQuery(indicator);
        }

        if (graphicTypeSel.equals(BARS) || graphicTypeSel.equals(LINES)) {
            return indicatorDao.getDataXYGraphic(query);
        } else {
            return indicatorDao.getDataPieGraphic(query);
        }

    }

    /**
     * Se realiza la consulta para obtener la tabla.
     *
     * @param indicatorConfiguration
     *
     * @return
     */
    @Override
    public List<TableDataDto> getData(
            IndicatorConfiguration indicatorConfiguration) {

        return indicatorDao.getData(generateDataTableQuery(
                indicatorConfiguration));
    }

    /**
     * Se realiza la consulta para obtener la tabla.
     *
     * @param indicatorConfiguration
     *
     * @return
     */
    @Override
    public List<TableDataDto> getColumnName(
            IndicatorConfiguration indicatorConfiguration) {

        return indicatorDao.getColumnName(generateDataTableQuery(
                indicatorConfiguration).concat("  and ROWNUM = 1"));
    }

    /**
     * Genera la consulta espec&icute;fica para el indicador 51;
     * 
     * @param indicator
     * @return 
     */
    private String getQueryFor51(IndicatorConfiguration indicator) {
        String query = String.format(
                "SELECT COUNT(1) value, TERMINACION_BENEFICIO xKey FROM ( \n"
                + "SELECT TERMINACION_BENEFICIO, \n"
                + "  ROW_NUMBER() OVER (PARTITION BY CODIGO_CCF, TIPO_IDENTIFICACION, NUMERO_IDENTIFICACION ORDER BY FECHA_LIQUIDACION_BENEFICIO DESC) ROW_NUMBER \n"
                + "FROM MD_F_FOSFEC \n"
                + "WHERE %1$s AND %2$s) \n"
                + "WHERE ROW_NUMBER = 1 \n"
                + "GROUP BY TERMINACION_BENEFICIO", indicator.getxVar().getFilter(), indicator.getSerieVar().getFilter());

        return query;
    }

    /**
     * Genera la consulta espec&icute;fica para el indicador 161.
     * 
     * @param indicator
     * @return 
     */
    private String getQueryFor161(IndicatorConfiguration indicator) {
        String query = String.format(
                "SELECT COUNT(1) VALUE, LIQUIDACION_BENEFICIO XKEY FROM ( "
                + "SELECT LIQUIDACION_BENEFICIO, "
                + "ROW_NUMBER() OVER (PARTITION BY CODIGO_CCF, TIPO_IDENTIFICACION, NUMERO_IDENTIFICACION ORDER BY FECHA_LIQUIDACION_BENEFICIO DESC) ROW_NUMBER "
                + "FROM "
                + "(SELECT DISTINCT CODIGO_CCF, TIPO_IDENTIFICACION, NUMERO_IDENTIFICACION, "
                + "CASE WHEN MONTO_LIQUIDADO_CESANTIAS = 0 THEN 'No Beneficiario' "
                + "WHEN MONTO_LIQUIDADO_CESANTIAS > 0 THEN 'Beneficiario' "
                + "END LIQUIDACION_BENEFICIO, "
                + "FECHA_LIQUIDACION_BENEFICIO "
                + "FROM MD_F_FOSFEC "
                + "WHERE %1$s AND %2$s)) "
                + "WHERE ROW_NUMBER = 1 "
                + "GROUP BY LIQUIDACION_BENEFICIO", indicator.getxVar().getFilter(), indicator.getSerieVar().getFilter());

        return query;
    }

    /**
     * Genera la consulta espec&icute;fica para el indicador 171.
     * 
     * @param indicator
     * @return 
     */
    private String getQueryFor171(IndicatorConfiguration indicator) {
        String query1 = String.format(
                "SELECT 'BENEFICIARIOS' XKEY,  COUNT(1) VALUE "
                + "FROM "
                + "(SELECT D.TIPO_DOCUMENTO TIPO_IDENTIFICACION, D.DOCUMENTO NUMERO_IDENTIFICACION, M.FECHA_INICIO "
                + "FROM MD_F_CAP_PROGRAMA_MODULO M "
                + "INNER JOIN MD_F_PROGRAMA_DETALLE D ON M.CODIGO_MODULO = D.CODIGO_MODULO "
                + "WHERE %1$s AND M.%2$s) S "
                + "INNER JOIN "
                + "(SELECT DISTINCT F.TIPO_IDENTIFICACION, F.NUMERO_IDENTIFICACION, F.FECHA_LIQUIDACION_BENEFICIO "
                + "FROM MD_F_FOSFEC F "
                + "WHERE %1$s) F "
                + "ON S.TIPO_IDENTIFICACION = F.TIPO_IDENTIFICACION AND S.NUMERO_IDENTIFICACION = F.NUMERO_IDENTIFICACION AND TO_CHAR(S.FECHA_INICIO, 'YYYY-MM') = TO_CHAR(F.FECHA_LIQUIDACION_BENEFICIO, 'YYYY-MM')"
                + "UNION ", indicator.getxVar().getFilter(), indicator.getSerieVar().getFilter());

        String replaceFirst = query1.replaceFirst("fecha_liquidacion_beneficio", "FECHA_INICIO");
        String replaceFirstFinal = replaceFirst.replaceFirst("fecha_liquidacion_beneficio", "FECHA_INICIO");

        String query2 = String.format(
                "SELECT 'NO BENEFICIARIOS' XKEY,  COUNT(1) VALUE "
                + "FROM "
                + "(SELECT D.TIPO_DOCUMENTO TIPO_IDENTIFICACION, D.DOCUMENTO NUMERO_IDENTIFICACION, M.FECHA_INICIO "
                + "FROM MD_F_CAP_PROGRAMA_MODULO M "
                + "INNER JOIN MD_F_PROGRAMA_DETALLE D ON M.CODIGO_MODULO = D.CODIGO_MODULO "
                + "WHERE %1$s AND M.%2$s) S "
                + "LEFT JOIN "
                + "(SELECT F.TIPO_IDENTIFICACION, F.NUMERO_IDENTIFICACION, F.FECHA_LIQUIDACION_BENEFICIO "
                + "FROM MD_F_FOSFEC F "
                + "WHERE %1$s) F "
                + "ON S.TIPO_IDENTIFICACION = F.TIPO_IDENTIFICACION AND S.NUMERO_IDENTIFICACION = F.NUMERO_IDENTIFICACION AND TO_CHAR(S.FECHA_INICIO, 'YYYY-MM') = TO_CHAR(F.FECHA_LIQUIDACION_BENEFICIO, 'YYYY-MM')"
                + "WHERE F.NUMERO_IDENTIFICACION IS NULL", indicator.getxVar().getFilter(), indicator.getSerieVar().getFilter());

        String replaceFirst1 = query2.replaceFirst("fecha_liquidacion_beneficio", "FECHA_INICIO");
        String replaceFirst1Final = replaceFirst1.replaceFirst("fecha_liquidacion_beneficio", "FECHA_INICIO");

        String query = replaceFirstFinal + replaceFirst1Final;

        return query;
    }

    /**
     * Genera la consulta espec&icute;fica para el indicador 471.
     * 
     * @param indicator
     * @return 
     */
    private String getQueryFor471(IndicatorConfiguration indicator) {
        String query = String.format(
                "SELECT B.DESCRIPCION XKEY, COUNT(1) VALUE "
                + "FROM "
                + "(SELECT DISTINCT P.DESCRIPCION, D.TIPO_DOCUMENTO TIPO_IDENTIFICACION, D.DOCUMENTO NUMERO_IDENTIFICACION, M.FECHA_FIN "
                + "FROM MD_F_CAP_PROGRAMA_MODULO M "
                + "INNER JOIN MD_F_PROGRAMA_DETALLE D ON M.CODIGO_MODULO = D.CODIGO_MODULO "
                + "INNER JOIN TIPOS_PROGRAMA P ON M.TIPO_CURSO = P.ID "
                + "INNER JOIN INSTITUCIONES I ON M.CODIGO_INST = I.ID "
                + "INNER JOIN TIPOS_INSTITUCION T ON I.TIPO_INSTITUCION_ID = T.ID "
                + "WHERE %2$s AND %1$s "
                + "AND D.ESTADO = 1) B "
                + "INNER JOIN "
                + "(SELECT DISTINCT F.TIPO_IDENTIFICACION, F.NUMERO_IDENTIFICACION, F.FECHA_LIQUIDACION_BENEFICIO "
                + "FROM MD_F_FOSFEC F "
                + "WHERE %2$s) F "
                + "ON B.TIPO_IDENTIFICACION = F.TIPO_IDENTIFICACION AND B.NUMERO_IDENTIFICACION = F.NUMERO_IDENTIFICACION "
                + "AND TO_CHAR(B.FECHA_FIN, 'YYYY-MM') = TO_CHAR(F.FECHA_LIQUIDACION_BENEFICIO, 'YYYY-MM') "
                + "GROUP BY  B.DESCRIPCION ORDER BY 2 DESC", indicator.getxVar().getFilter(), indicator.getSerieVar().getFilter());

        String replaceFirst = query.replaceFirst("fecha_liquidacion_beneficio", "FECHA_FIN");
        String replaceFirstFinal = replaceFirst.replaceFirst("fecha_liquidacion_beneficio", "FECHA_FIN");

        return replaceFirstFinal;
    }

    /**
     * Genera una consulta espec&icute;fica para el indicqdor 481.
     * 
     * @param indicator
     * @return 
     */
    private String getQueryFor481(IndicatorConfiguration indicator) {
        String query = String.format(
                "SELECT B.DESCRIPCION XKEY, COUNT(1) VALUE "
                + "FROM "
                + "(SELECT DISTINCT P.DESCRIPCION, D.TIPO_DOCUMENTO TIPO_IDENTIFICACION, D.DOCUMENTO NUMERO_IDENTIFICACION, M.FECHA_INICIO "
                + "FROM MD_F_CAP_PROGRAMA_MODULO M "
                + "INNER JOIN MD_F_PROGRAMA_DETALLE D ON M.CODIGO_MODULO = D.CODIGO_MODULO "
                + "INNER JOIN TIPOS_PROGRAMA P ON M.TIPO_CURSO = P.ID "
                + "INNER JOIN INSTITUCIONES I ON M.CODIGO_INST = I.ID "
                + "INNER JOIN TIPOS_INSTITUCION T ON I.TIPO_INSTITUCION_ID = T.ID "
                + "WHERE %2$s AND %1$s) B "
                + "INNER JOIN "
                + "(SELECT DISTINCT F.TIPO_IDENTIFICACION, F.NUMERO_IDENTIFICACION, F.FECHA_LIQUIDACION_BENEFICIO "
                + "FROM MD_F_FOSFEC F "
                + "WHERE %2$s) F "
                + "ON B.TIPO_IDENTIFICACION = F.TIPO_IDENTIFICACION AND B.NUMERO_IDENTIFICACION = F.NUMERO_IDENTIFICACION "
                + "AND TO_CHAR(B.FECHA_INICIO, 'YYYY-MM') = TO_CHAR(F.FECHA_LIQUIDACION_BENEFICIO, 'YYYY-MM') "
                + "GROUP BY  B.DESCRIPCION ORDER BY 2 DESC", indicator.getxVar().getFilter(), indicator.getSerieVar().getFilter());

        String replaceFirst = query.replaceFirst("fecha_liquidacion_beneficio", "FECHA_INICIO");
        String replaceFirstFinal = replaceFirst.replaceFirst("fecha_liquidacion_beneficio", "FECHA_INICIO");

        return replaceFirstFinal;
    }

    /**
     * Genera la consulta que devuelve los datos para las graficas.
     *
     * @param indicatorConfiguration
     *
     * @return
     */
    private String generateDataGraphicQuery(
            IndicatorConfiguration indicatorConfiguration) {
        IndicatorVariable xVariable = indicatorConfiguration.getxVar();
        IndicatorVariable yVariable = indicatorConfiguration.getyVar();
        IndicatorVariable varfuntion = indicatorConfiguration.getVarfuntion();
        String functionSel = indicatorConfiguration.getFunction();

        StringBuilder query = new StringBuilder("SELECT ");

        if ("CINE A.C.".equals(xVariable.getDescription())) {

            switch (xVariable.getFilter()) {

                case "cine like '1'":
                    xVariable.setFilter("cine like '01'");
                    break;
                case "cine like '2'":
                    xVariable.setFilter("cine like '02'");
                    break;
                case "cine like '3'":
                    xVariable.setFilter("cine like '03'");
                    break;
                case "cine like '4'":
                    xVariable.setFilter("cine like '04'");
                    break;
                case "cine like '5'":
                    xVariable.setFilter("cine like '05'");
                    break;
                case "cine like '6'":
                    xVariable.setFilter("cine like '06'");
                    break;
                case "cine like '7'":
                    xVariable.setFilter("cine like '07'");
                    break;
                case "cine like '8'":
                    xVariable.setFilter("cine like '08'");
                    break;
                case "cine like '9'":
                    xVariable.setFilter("cine like '09'");
                    break;

            }

        }

        //Grafico de DONA
        if ("D".equals(indicatorConfiguration.getGraphicType())) {
            buildDonut(functionSel, yVariable, query, varfuntion);
        } else if (xVariable != null) {
            buildLinesAndBars(xVariable, query, functionSel, varfuntion,
                    yVariable);
        }

        return generateDataContitionsQuery(indicatorConfiguration, query, "G");
    }

    /**
     * Genera la consulta para los graficos de lineas y barras.
     *
     * @param xVariable
     * @param query
     * @param functionSel
     * @param varfuntion
     * @param yVariable
     */
    private void buildLinesAndBars(IndicatorVariable xVariable,
            StringBuilder query, String functionSel, IndicatorVariable varfuntion,
            IndicatorVariable yVariable) {
        //XValue for select
        if (xVariable.getType().equals(PERIODO)) {
            query.append("TO_CHAR(");
            query.append(xVariable.getField());
            query.append(" , '");
            query.append(xVariable.getDetail());
            query.append("') ");
        }
        if (xVariable.getType().equals(FIELD)) {
            query.append(xVariable.getField());
        }
        if (xVariable.getType().equals(TABLE)) {
            query.append(xVariable.getDetail());
            query.append(".descripcion  ");
        }
        query.append(" as xKey,");

        if (functionSel != null) {
            query.append(functionSel);
            query.append("(");
            if (varfuntion != null) {
                if (varfuntion.getType().equals(PERIODO)) {
                    query.append("TO_CHAR(");
                    query.append(varfuntion.getField());
                    query.append(" , '");
                    query.append(varfuntion.getDetail());
                    query.append("') ");
                }
                if (varfuntion.getType().equals(FIELD)) {
                    query.append(varfuntion.getField());
                }
                if (varfuntion.getType().equals(TABLE)) {
                    query.append(varfuntion.getDetail());
                    query.append(".descripcion  ");
                }
            } else {
                query.append(yVariable.getField());
            }

            query.append(")value ");
        } else {
            query.append(yVariable.getField());
            query.append("  value ");
        }
    }

    /**
     * Genera la consulta para graficos de donas.
     *
     * @param functionSel
     * @param yVariable
     * @param query
     * @param varfuntion
     */
    private void buildDonut(String functionSel, IndicatorVariable yVariable,
            StringBuilder query, IndicatorVariable varfuntion) {
        if (functionSel != null) {
            //XValue for select

            if (PERIODO.equals(yVariable.getType())) {
                query.append("TO_CHAR(");
                query.append(yVariable.getField());
                query.append(" , '");
                query.append(yVariable.getDetail());
                query.append("') ");
            }

            if (FIELD.equals(yVariable.getType())) {
                query.append(yVariable.getField());
            }
            if (TABLE.equals(yVariable.getType())) {
                query.append(yVariable.getDetail());
                query.append(".descripcion  ");
            }
            query.append(" as xKey,");

            query.append(functionSel);
            query.append("(");

            if (varfuntion != null) {
                if (varfuntion.getType().equals(PERIODO)) {
                    query.append("TO_CHAR(");
                    query.append(varfuntion.getField());
                    query.append(" , '");
                    query.append(varfuntion.getDetail());
                    query.append("') ");
                }
                if (varfuntion.getType().equals(FIELD)) {
                    query.append(varfuntion.getField());
                }
                if (varfuntion.getType().equals(TABLE)) {
                    query.append(varfuntion.getDetail());
                    query.append(".descripcion  ");
                }
            } else {
                query.append(yVariable.getField());
            }

            query.append(")value ");
        } else {
            query.append(yVariable.getField());

            query.append("  value ");
        }
    }

    /**
     * Genera la consulta que devuelve los datos para la tabla de resultados.
     * 
     * @param indicatorConfiguration
     * @return 
     */
    private String generateDataTableQuery(
            IndicatorConfiguration indicatorConfiguration) {

        StringBuilder query = new StringBuilder("SELECT  ");
        List<IndicatorVariable> columnNames = indicatorConfiguration.
                getIndicatorSource().getVariables();

        for (int i = columnNames.size() - 1; i >= 0; --i) {

            IndicatorVariable name = columnNames.get(i);
            query.append(name.getField());
            query.append(",");
        }

        return generateDataContitionsQuery(indicatorConfiguration,
                new StringBuilder(query.substring(0, query.length() - 1)), "T");

    }

    /**
     * Genera la parte de las consulta despues del 'from' que va igual para la
     * graficas como para la tabla.
     *
     * @param indicatorConfiguration
     * @param query
     * @param type
     *
     * @return
     */
    private String generateDataContitionsQuery(
            IndicatorConfiguration indicatorConfiguration, StringBuilder query,
            String type) {
        boolean haveTableData = false;
        IndicatorSource indicatorSource = indicatorConfiguration.
                getIndicatorSource();
        IndicatorVariable xVariable = indicatorConfiguration.getxVar();
        IndicatorVariable seriesVariable = indicatorConfiguration.getSerieVar();
        IndicatorVariable yVariable = indicatorConfiguration.getyVar();
        String conditions = indicatorConfiguration.getConditions();
        String functionSel = indicatorConfiguration.getFunction();

        query.append(" FROM ");
        query.append(indicatorSource.getTable());

        if (xVariable != null) {
            //XValue for from
            if (xVariable.getType().equals(TABLE)) {
                query.append(" , ");
                query.append(xVariable.getDetail());
            }
        }
        //Serie for from
        if (seriesVariable.getType().equals(TABLE)) {
            query.append(" , ");
            query.append(seriesVariable.getDetail());
        }

        //XValue for where
        if (xVariable != null) {
            if (xVariable.getType().equals(TABLE)) {
                haveTableData = true;
                query.append(" WHERE ");
                query.append(xVariable.getField());
                query.append(" = ");
                query.append(xVariable.getDetail());
                query.append(".id ");
            }

            if (xVariable.getFilter() != null) {
                if (!haveTableData) {
                    query.append(" WHERE ");
                    haveTableData = true;
                } else {
                    query.append(" AND ");
                }
                query.append(xVariable.getFilter());
            }
        }
        //Serie for where
        if (seriesVariable.getType().equals(TABLE)) {
            if (!haveTableData) {
                query.append(" WHERE ");
                haveTableData = true;
            } else {
                query.append(" and ");
            }
            query.append(seriesVariable.getField());
            query.append(" = ");
            query.append(seriesVariable.getDetail());
            query.append(".id ");
        }
        if (seriesVariable.getFilter() != null) {
            if (!haveTableData) {
                query.append(" WHERE ");
            } else {
                query.append(" AND ");
            }
            query.append(seriesVariable.getFilter());
        }
        //Condicion for where
        if (conditions != null) {

            query.append(" AND  (  ");
            query.append(conditions);
            query.append(" ) ");
        }

        if ("G".equals(type) && functionSel != null) {
            query.append(" GROUP BY  ");

            if ("D".equals(indicatorConfiguration.getGraphicType())) {
                if (yVariable != null) {
                    if (yVariable.getType().equals(PERIODO)) {
                        query.append("TO_CHAR(");
                        query.append(yVariable.getField());
                        query.append(" , '");
                        query.append(yVariable.getDetail());
                        query.append("') ");

                    }

                    if (yVariable.getType().equals(FIELD)) {
                        query.append(yVariable.getField());
                    }
                    if (yVariable.getType().equals(TABLE)) {
                        query.append(yVariable.getDetail());
                        query.append(".descripcion  ");
                    }

                }

            } else //XValue for group by
            {
                if (xVariable != null) {
                    if (xVariable.getType().equals(PERIODO)) {
                        query.append("TO_CHAR(");
                        query.append(xVariable.getField());
                        query.append(" , '");
                        query.append(xVariable.getDetail());
                        query.append("') ");

                    }

                    if (xVariable.getType().equals(FIELD)) {
                        query.append(xVariable.getField());
                    }
                    if (xVariable.getType().equals(TABLE)) {
                        query.append(xVariable.getDetail());
                        query.append(".descripcion  ");
                    }

                }

                switch (indicatorConfiguration.getGraphicType()) {
                    case BARS:
                        query.append(" ORDER BY  2  desc");
                        break;
                    case LINES:
                        query.append(" ORDER BY  1");
                        break;
                }

            }
        }

        LOGGER.info("Consulta  generada : " + type + "  " + query.toString());
        return query.toString();
    }

    /**
     * Genera la consulta para obtener una pagina de la tabla.
     * 
     * @param indicatorConfiguration
     * @param pageNo
     * @param pageSize
     * @return 
     */
    @Override
    public Page<TableDataDto> getDataTablePage(
            IndicatorConfiguration indicatorConfiguration, int pageNo, int pageSize) {
        Page<TableDataDto> page = indicatorDao.
                getDataTablePage(pageNo, pageSize,
                        generateDataTableQuery(indicatorConfiguration));

        return page;
    }

    /**
     * Genera la consulta para obtener los datos en un archivo.
     *
     * @param indicatorConfiguration
     *
     * @return
     */
    @Override
    public List<TableDataDto> getDataForFiles(
            IndicatorConfiguration indicatorConfiguration) {
        List<TableDataDto> fullList = indicatorDao.
                getDataForFiles(generateDataTableQuery(indicatorConfiguration));

        return fullList;
    }
}
