/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: EducationLevelDtoRowMapper.java
 * Created on: 2017/02/13, 11:46:39 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.dao.mapper;

import com.gml.simpc.api.dto.EducationLevelDto;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 * Row Mapper para nivel educativo.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class EducationLevelDtoRowMapper implements RowMapper<EducationLevelDto> {

    @Override
    public EducationLevelDto mapRow(ResultSet rs, int i) throws SQLException {
        EducationLevelDto dto = new EducationLevelDto();
        dto.setCargaId(rs.getLong("CARGA_ID"));
        dto.setDocumentType(rs.getString("TIPO_DOCUMENTO"));
        dto.setNumberIdentification(rs.getString("NUMERO_DOCUMENTO"));
        dto.setEducationLevel(rs.getString("NIVEL_EDUCATIVO"));
        dto.setPerformanceArea(rs.getString("AREA_DESEMPENO"));
        dto.setNucleusKnowledge(rs.getString("NUCLEO_CONOCIMIENTO"));
        dto.setDegreeObtained(rs.getString("TITULO_OBTENIDO"));
        dto.setDegreeHomologated(rs.getString("TITULO_HOMOLOGADO"));
        dto.setDegreeCountry(rs.getString("PAIS_TITULO"));
        dto.setUniversity(rs.getString("INSTITUCION"));
        dto.setStatus(rs.getString("ESTADO"));
        dto.setFinishDate(rs.getString("FECHA_FINALIZACION"));
        dto.setProfessionalCard(rs.getString("TIENE_TARJETA_PROFESIONAL"));
        dto.setProfessionalCard(rs.getString("TIENE_TARJETA_PROFESIONAL"));
        dto.setProfessionalCardNumber(rs.
            getString("NUMERO_TARJETA_PROFESIONAL"));
        dto.setProfessionalCardExpeditionDate(rs.
            getString("FECHA_EXP_TARJ_PROFESIONAL"));
        dto.setObservations(rs.getString("OBSERVACIONES"));
        dto.setPractice(rs.getString("ESTA_INTERESADO_PRACTICA_EMPR"));

        return dto;
    }
}
