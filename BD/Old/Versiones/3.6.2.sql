--Creaci�n de vista para estado de asistencia a capacitacion
create or replace view PIE_MONTOS as
with data  as (



SELECT verificacion_requisitos,codigo_ccf CCF, TO_CHAR(fecha_liquidacion_beneficio,'YYYY-MM') fecha_liquidacion,
SUM (MONTO_LIQUIDADO_SALUD) salud ,
SUM (MONTO_LIQUIDADO_PENSION) PENSION,
SUM (MONTO_LIQUIDADO_CUOTA) CUOTA,
SUM (MONTO_LIQUIDADO_CESANTIAS) CESANTIAS,
SUM (MONTO_LIQUIDADO_ALIMENTACION) ALIMENTACION FROM MD_F_FOSFEC , vw_cajas_compensacion
WHERE codigo_ccf = vw_cajas_compensacion.id  
GROUP BY codigo_ccf,TO_CHAR(fecha_liquidacion_beneficio,'YYYY-MM'),verificacion_requisitos )






SELECT VERIFICACION_REQUISITOS,fecha_liquidacion,CCF, "MONTO_LABEL","VALUE" FROM data
unpivot
(
  value
    for MONTO_LABEL in ("SALUD","PENSION","CUOTA","CESANTIAS","ALIMENTACION"))


COMMIT;


create or replace view  vw_tipos_certificacion as
 select 
NOMBRE_CERTIFICACION id,
case when NOMBRE_CERTIFICACION is not null then 'NTC555'
ELSE 'No tiene'
 
end descripcion
from MD_F_CAP_PROGRAMA_MODULO
group by NOMBRE_CERTIFICACION;

COMMIT;
