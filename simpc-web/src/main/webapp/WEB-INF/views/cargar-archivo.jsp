<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <link href="resources/css/load.css" rel="stylesheet"/>
        <style>
            input[type=number]::-webkit-outer-spin-button,
            input[type=number]::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            input[type=number] {
                -moz-appearance:textfield;
            }
        </style>
    </head>
    <body>
        <br/>
        <div class="alert alert-danger" id="msg20MB" role="alert">
            <button type="button" class="close" data-dismiss="alert"
                    aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>Error!</strong> El archivo cargado excede los 20MB.
        </div>
        <c:if test="${not empty msg}">
            <br/>
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        <c:if test="${empty loadTypeList}">
            <br/>
            <div class="alert alert-danger">
                <strong>Error!</strong> No hay tipos de carga disponibles. Consulte al administrador.
            </div>
        </c:if>
        <c:if test="${not empty loadTypeList}">
            <div class="container">
                <h1>Portal de Cargas - Lista De Cargas</h1>
                <br/>
                <div class="row">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h2>Cargar archivos</h2></div>
                        <div class="panel-body">
                            <p>Seleccione el tipo de carga a realizar.</p>
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Formación para la inserción y reinserción laboral <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li onclick="selLoadType('ORI-1');"><a href="#">Orientación - Encabezado</a></li>
                                    <li onclick="selLoadType('ORI-2');"><a href="#">Orientación - Detalle</a></li>
                                    <li onclick="selLoadType('CAP-1');"><a href="#">Capacitación - Encabezado</a></li>
                                    <li onclick="selLoadType('CAP-2');"><a href="#">Capacitación - Detalle</a></li>
                                </ul>
                            </div>
                            
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Hojas de vida <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li onclick="selLoadType('HDV-1');"><a href="#">Datos Básicos</a></li>
                                    <li onclick="selLoadType('HDV-2');"><a href="#">Nivel Educativo</a></li>
                                    <li onclick="selLoadType('HDV-3');"><a href="#">Educación Informal</a></li>
                                    <li onclick="selLoadType('HDV-4');"><a href="#">Experiencia Laboral</a></li>
                                    <li onclick="selLoadType('HDV-5');"><a href="#">Idiomas</a></li>
                                    <li onclick="selLoadType('HDV-6');"><a href="#">Otros Conocimientos</a></li>
                                    <li onclick="selLoadType('HDV-7');"><a href="#">Intermediación</a></li>
                                </ul>
                            </div>
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Prestaciones Económicas <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li onclick="selLoadType('PES-1');"><a href="#">Maestro FOSFEC</a></li>
                                    <li onclick="selLoadType('PES-2');"><a href="#">Dependientes Económicos</a></li>
                                </ul>
                            </div>
                            
                        </div> 
                    </div>   
                </div>   

                <div id="divSelFile" class="row" style="display:none;">    
                    <div class="panel panel-default" >
                        <div class="panel-heading"><h2><p id="tipoCargaDesc"></p></h2></div>
                        <div class="panel-body">
                            <form:form id="formLoadProcess" modelAttribute="loadProcess" enctype="multipart/form-data" method="post" action="/masterdata/generar-carga.htm">    
                                <form:input type="hidden" id="tipoCarga" path="loadType" />
                                <p>A continuación seleccione el archivo a cargar desde su ordenador.</p>
                                <br/>
                                <form:input type="file" class="form-control load-file" id="archivoCarga" name="file" path="file"/>
                                <div id="radioWriteType">
                                    <br/>
                                    <p>El archivo seleccionado es nuevo o es de actualizacion?.</p>
                                    <br/>
                                    <form:radiobutton checked="checked" id="writeTypeN" path="writeType" value="N" />Archivo nuevo
                                    <form:radiobutton id="writeTypeA" path="writeType" value="A"/>Actualizacion
                                </div>    
                                <br/>
                                <a onclick="validateLoad();" id="saveButton" class="btn btn-success">Iniciar Cargue</a>
                            </form:form>
                        </div> 
                    </div>      
                </div>
            </div>
        </c:if>
        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/jquery.ui.widget.js"></script>
        <script src="resources/js/jquery.iframe-transport.js"></script>
        <script src="resources/js/jquery.fileupload.js"></script>
        <script>
            $(document).ready(function () {
                $('#msg20MB').hide();
                $('#saveButton').hide();
                $('#archivoCarga').bind('change', function() {                    
                    if (this.files[0].size > 20971520) {
                        $('#msg20MB').show();
                        $('#saveButton').hide();
                    } else {
                        $('#msg20MB').hide();
                        $('#saveButton').show();
                    }
                });
            });
            
            function selLoadType(selectedType){
                if (selectedType.indexOf("HDV") !== -1){
                    $("#writeTypeN").prop( "checked", false );        
                    $("#writeTypeA").prop( "checked", true );
                    $("#radioWriteType").hide();
                }
                if (selectedType.indexOf("HDV") === -1){
                    $("#writeTypeN").prop( "checked", false );        
                    $("#writeTypeA").prop( "checked", false );
                    $("#radioWriteType").show();
                }
                <c:forEach var="item" items="${loadTypeList}">
                    if(selectedType === '${item.key}'){
                        $("#tipoCarga").val('${item.value.name}');
                        $("#tipoCargaDesc").text("Carga de  "+'${item.value.description}');
                        $("#divSelFile").show();
                    }
                </c:forEach>
            }
            
            function validateLoad(){
                
                if (!$("#writeTypeN").is(':checked') && !$("#writeTypeA").is(':checked')) {
                    alert("Debe seleccionar si es nuevo o actualizacion");
                }else{
                    $("#saveButton").attr("disabled", true);
                    $("#saveButton").text("Realizando cargue...");
                    $("#saveButton").attr("class", "btn btn-warning");
                    $("#formLoadProcess").submit();
                }
            }
        </script>                                           
    </body>
</html>    
