/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserServiceImpl.java
 * Created on: 2016/10/19, 02:11:43 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.entity.LoadProcess;
import com.gml.simpc.api.entity.exeption.UserException;
import com.gml.simpc.api.services.LoadProcessService;
import com.gml.simpc.commons.annotations.AnnotationInterceptor;
import com.gml.simpc.web.repository.LoadProcessRepository;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Servicio para administrar procesos de carga.
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "loadProcessService")
public class LoadProcessServiceImpl implements LoadProcessService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(
        LoadProcessServiceImpl.class);

    /**
     * Servicio de repositorio para los procesos de carga.
     */
    @Autowired
    private LoadProcessRepository loadProcessRepository;

    /**
     * Guardada nuevo proceso de carga en la base de datos.
     * 
     * @param loadProcess
     * @throws UserException 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Iniciar Cargue",
        entityTableName = "WF_CARGA_PROCESO")
    @Override
    public void save(LoadProcess loadProcess) throws UserException {
        LOGGER.info("Ejecutando metodo [ save de procesos de carga ]");
        this.loadProcessRepository.save(loadProcess);
    }

    /**
     * Actualiza un proceso de carga
     * 
     * @param loadProcess 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Actualizar Estado de " +
        "Cargue", entityTableName = "WF_CARGA_PROCESO")
    @Override
    public void update(LoadProcess loadProcess) {
        LOGGER.info("Ejecutando metodo [ update de procesos de carga ]");
        this.loadProcessRepository.saveAndFlush(loadProcess);
    }

    /**
     * Elimina un proceso de carga.
     * 
     * @param loadProcess 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Borrar Cargue",
        entityTableName = "WF_CARGA_PROCESO")
    @Override
    public void remove(LoadProcess loadProcess) {
        LOGGER.info("Ejecutando metodo [ remove de procesos de carga ]");
        this.loadProcessRepository.delete(loadProcess);
    }

    /**
     * Devuelve todos los procesos de carga.
     * 
     * @return 
     */
    @Override
    public List<LoadProcess> getAll() {
        LOGGER.info("Ejecutando metodo [ getAll de procesos de carga ]");
        return this.loadProcessRepository.findAll();
    }

    /**
     * Metodo que encuentra un proceso de carga por id.
     * 
     * @param id
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar Por id",
        entityTableName = "WF_CARGA_PROCESO")
    @Override
    public LoadProcess findById(Long id) {
        LOGGER.info("Ejecutando metodo [ findById ]");
        return this.loadProcessRepository.findOne(id);
    }

    /**
     * Metodo que busca por rango de fechas y tipo de archivo.
     * 
     * @param start
     * @param end
     * @param fileId
     * @param ccf
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar por Fecha y" +
        " Archivo", entityTableName = "WF_CARGA_PROCESO")
    @Override
    public List<LoadProcess> findByDateAndFile(Date start, Date end,
        String fileId, String ccf) {
        return this.loadProcessRepository.findByDateAndFile(start, end, fileId,
            ccf);
    }

    /**
     * Metodo que busca por rango de fechas y tipo de archivo.
     * 
     * @param year
     * @param month
     * @param file
     * @param consultant
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "findByperiodAndFile",
        entityTableName = "WF_CARGA_PROCESO")
    @Override
    public List<LoadProcess> findByperiodAndFile(int year, String month,
        String file, String consultant) {
        return this.loadProcessRepository.findByperiodAndFile(year, month, file,
            consultant);
    }

    /**
     * Metodo que encuentra un proceso de carga por id.
     * 
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar primera" +
        " carga con estructura v�lida", entityTableName = "WF_CARGA_PROCESO")
    @Override
    public LoadProcess findFirstValidated() {
        LOGGER.info("Ejecutando metodo [ findFirstValidated ]");
        return this.loadProcessRepository.findFirstValidated();
    }

    /**
     * Metodo que encuentra un proceso de carga por cabecera.
     * 
     * @param header
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar por cabecera", 
        entityTableName = "WF_CARGA_PROCESO")
    @Override
    public LoadProcess getLoadProcessByHeader(String header) {
        LOGGER.info("Ejecutando metodo [ findFirstValidated ]");
        
        return this.loadProcessRepository.findByHeader(header);
    }
}
