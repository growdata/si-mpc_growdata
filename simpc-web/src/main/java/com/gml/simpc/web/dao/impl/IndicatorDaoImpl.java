/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IndicatorDaoImpl.java
 * Created on: 2017/04/30, 01:31:58 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.dao.impl;

import com.gml.simpc.api.dao.IndicatorDao;
import com.gml.simpc.api.dto.GraphicDataDto;
import com.gml.simpc.api.dto.Page;
import com.gml.simpc.api.dto.PieGraphicDto;
import com.gml.simpc.api.dto.TableDataDto;
import com.gml.simpc.web.paging.PaginationHelper;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
/**
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
@Repository
public class IndicatorDaoImpl implements IndicatorDao {

    /**
     * Logger de la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(IndicatorDaoImpl.class);
    /**
     * Plantilla Jdbc para operar la base de datos.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<GraphicDataDto> getDataXYGraphic(String query) {

        return this.jdbcTemplate.query(query, new RowMapper() {

            public GraphicDataDto mapRow(ResultSet rs, int rownumber) throws
                SQLException {
                GraphicDataDto e = new GraphicDataDto();
                e.setAxisX(rs.getString("xKey"));
                e.setAxisY(rs.getInt("value"));

                return e;
            }
        });
    }

    @Override
    public List<PieGraphicDto> getDataPieGraphic(String query) {
        return this.jdbcTemplate.query(query, new RowMapper() {

            public PieGraphicDto mapRow(ResultSet rs, int rownumber) throws
                SQLException {
                PieGraphicDto e = new PieGraphicDto();
                e.setLabel(rs.getString("xKey"));
                e.setValue(rs.getBigDecimal("value"));

                return e;
            }
        });

    }

    @Override
    public List<TableDataDto> getData(String query) {

        return this.jdbcTemplate.query(query, new RowMapper() {
            @Override
            public TableDataDto mapRow(ResultSet rs, int rownumber) throws
                SQLException {
                TableDataDto e = new TableDataDto();

                ResultSetMetaData rsmd = rs.getMetaData();
                int columns = rsmd.getColumnCount();

                while (columns != 0) {
                    e.addColumn(rs.getString(columns--));
                }

                return e;
            }
        });

    }

    @Override
    public List<TableDataDto> getColumnName(String query) {

        return this.jdbcTemplate.query(query, new RowMapper() {
            @Override
            public TableDataDto mapRow(ResultSet rs, int rownumber) throws
                SQLException {
                TableDataDto e = new TableDataDto();

                ResultSetMetaData rsmd = rs.getMetaData();
                int columns = rsmd.getColumnCount();

                for (int i = 1; i <= columns; i++) {
                    e.addColumn(rsmd.getColumnName(i));
                    LOGGER.info("cantidad" + i + " valor" + rsmd.getColumnName(i
                    ));
                }

                return e;
            }
        });

    }

    @Override
    public Page<TableDataDto> getDataTablePage(int pageNo, int pageSize,String query)
         {
        PaginationHelper ph = new PaginationHelper();
        return ph.fetchPage(
            jdbcTemplate,
            "SELECT count(*) FROM  ( ".concat(query).concat(" )"),
            query,
            pageNo,
            pageSize,
            new RowMapper<TableDataDto>() {
            @Override
            public TableDataDto mapRow(ResultSet rs, int i) throws SQLException {
                TableDataDto e = new TableDataDto();

                ResultSetMetaData rsmd = rs.getMetaData();
                int columns = rsmd.getColumnCount();

                while (columns != 0) {
                    e.addColumn(rs.getString(columns--));
                }

                return e;
            }
        }
        );

    }

    @Override
    public List <TableDataDto> getDataForFiles(String query){
        return this.jdbcTemplate.query(query, new RowMapper() {
            @Override
            public TableDataDto mapRow(ResultSet rs, int rownumber) throws
                SQLException {
                TableDataDto e = new TableDataDto();

                ResultSetMetaData rsmd = rs.getMetaData();
                int columns = rsmd.getColumnCount();

                while (columns != 0) {
                    e.addColumn(rs.getString(columns--));
                }

                return e;
            }
        });
}
    
}
