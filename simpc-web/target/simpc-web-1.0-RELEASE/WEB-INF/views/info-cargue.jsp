<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <style>
            input[type=number]::-webkit-outer-spin-button,
            input[type=number]::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            input[type=number] {
                -moz-appearance:textfield;
            }
        </style>
    </head>
    <body>
        <c:if test="${not empty msg}">
            <br/>
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        <h1>Historial de Cargues</h1>
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading"><h2>Filtros de la B&uacute;squeda</h2></div>
            <div class="panel-body">
                     <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active" ><a data-toggle="tab" rol="tab" href="#menu1" id="tabRango">Rango de Fechas</a></li>
                    <li role="presentation" ><a data-toggle="tab" href="#menu2" id="tabPeriodo">Per&iacute;odo</a></li>
                </ul>

                <div class="tab-content" >
                    <div id="alert-area" ></div>
                    <div id="menu1" class="tab-pane fade in active">

                        <legend>           

                            <form id="rangoForm" name="rangoForm" action="detalle-rango.htm" >
                                <div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="fechaInicio" class="control-label ">Fecha Inicio</label>
                                            <input id="fechaInicio" name="fechaInicio" class="form-control "
                                                   data-date-format="yyyy/mm/dd"   path="fechaInicio" required="required"  />
                                            <fmt:formatDate value="${fechaInicio}" pattern="dd-MM-yyyy" />
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>  
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="fechaFin" class="control-label ">Fecha Fin</label>
                                            <input id="fechaFin" name="fechaFin"  class="form-control "
                                                   data-date-format="yyyy/mm/dd"   path="fechaFin" required="required" />
                                            <fmt:formatDate value="${fechaFin}" pattern="dd-MM-yyyy" />
                                            <div class="clearfix"></div>
                                        </div>
                                    </div> 

                                    <div class="col-sm-4">

                                        <label for="filetype" class="control-label">Tipo De Archivo</label>
                                        <select class="form-control " id="file" name="file" >
                                            <option value='' label="Seleccione ..." selected>Seleccione...</option>
                                            <c:if test="${not empty fileType}">
                                                <c:forEach var="item" items="${fileType}">
                                                    <option value="${item.name}">${item.description}</option>
                                                </c:forEach>
                                            </c:if>
                                        </select>                                      
                                        </center>
                                    </div>
                                    <div class="col-sm-12" style="text-align:right">
                                        <button type="button" class="btn btn-success" id="rango" name="rango">Buscar</button>    
                                    </div>
                                </div>
                            </form>
                        </legend>
                    </div>
                    <div id="menu2" class="tab-pane fade">
                        <form id="peridoForm" name="periodoForm" action="detalle-periodo.htm" >
                            <div class="col-sm-4 ">
                                <div class="form-group">
                                    <label for="year" class="control-label">A�o</label>
                                    <select class="form-control"  id="anio" name="anio" >
                                        <option  value="0" selected>Seleccione...</option>
                                        <c:if test="${not empty anios}">
                                            <c:forEach var="item" items="${anios}">
                                                <option value="${item}" >${item}</option>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="month" class="control-label">Mes</label>
                                    <select class="form-control" id="mes" name="mes"  >
                                        <option  value="" selected>Seleccione...</option>
                                        <option value="1">Enero</option>
                                        <option value="2">Febrero</option>
                                        <option value="3">Marzo</option>
                                        <option value="4">Abril</option>
                                        <option value="5">Mayo</option>
                                        <option value="6">Junio</option>
                                        <option value="7">Julio</option>
                                        <option value="8">Agosto</option>
                                        <option value="9">Septiembre</option>
                                        <option value="10">Octubre</option>
                                        <option value="11">Noviembre</option>
                                        <option value="12">Diciembre</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="filetype" class="control-label">Tipo De Archivo</label>
                                    <select class="form-control" id="file2" name="file2" >
                                       <option value='' label="Seleccione ..." selected>Seleccione...</option>
                                        <c:if test="${not empty fileType}">
                                            <c:forEach var="item" items="${fileType}">
                                                 <option value="${item.name}">${item.description}</option>
                                            </c:forEach>

                                        </c:if>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12" style="text-align:right">
                                <button type="button" class="btn btn-success" id="periodo" name="periodo">Buscar</button>
                            </div> 

                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="panel panel-default">
            <div class="panel-heading"><h2>Resultado de la B&uacute;squeda</h2></div>
            <div class="panel-body">

                <div class=" table-responsive">
                    <table id="loads" class="table table-striped table-bordered" 
                           cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Fecha Cargue</th>
                                <th>Tipo de archivo</th>
                                <th>Nombre del Archivo</th>
                                <th>Usuario</th> 
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tfoot> 
                            <tr> 
                                <th>Id</th>
                                <th>Fecha Cargue</th>
                                <th>Tipo de archivo</th>
                                <th>Nombre del Archivo</th>
                                <th>Usuario</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:if test="${not empty loadProcessList}">
                                <c:forEach var="item" items="${loadProcessList}">
                                    <tr>
                                        <td>
                                            <c:if test="${item.status=='ARCHIVO_CARGADO'}">
                                                <c:set var="altype" value="info" />
                                            </c:if>
                                            <c:if test="${item.status=='ESTRUCTURA_VALIDADA'}">
                                                <c:set var="altype" value="info" />
                                            </c:if>
                                            <c:if test="${item.status=='FALLA_VALIDACION'}">
                                                <c:set var="altype" value="danger" />
                                            </c:if>   
                                            <c:if test="${item.status=='CARGADO_STAGE'}">
                                                <c:set var="altype" value="danger" />
                                            </c:if>      
                                            <c:if test="${item.status=='ERRORES_ESTRUCTURA'}">
                                                <c:set var="altype" value="warning" />
                                            </c:if>    
                                            <c:if test="${item.status=='ERRORES_NEGOCIO'}">
                                                <c:set var="altype" value="warning" />
                                            </c:if>
                                            <c:if test="${item.status=='FALLA_PROCESAMIENTO'}">
                                                <c:set var="altype" value="danger" />
                                            </c:if>  
                                            <c:if test="${item.status=='CARGADO_MASTER'}">
                                                <c:set var="altype" value="success" />
                                            </c:if>
                                            <label class="alert alert-${altype}">${item.id}</label>
                                        </td>
                                        <td>${item.initDate}</td>
                                        <td>
                                            <c:forEach var="typeList" items="${fileType}">
                                                <c:if test="${typeList.name.equals(item.loadType)}">
                                                    ${typeList.description}
                                                </c:if>        
                                            </c:forEach>

                                        </td>
                                        <td>${item.originalFileName}</td>

                                        <td>${item.consultant.username}-${item.consultant.ccf.name}</td>


                                        <td>
                                            <c:if test="${item.status=='ARCHIVO_CARGADO'}">
                                                Archivo cargado. En espera de validaci�n de estructura
                                            </c:if>
                                            <c:if test="${item.status=='ESTRUCTURA_VALIDADA'}">
                                                Archivo con estructura correcta. En espera de procesamiento
                                            </c:if>
                                            <c:if test="${item.status=='FALLA_VALIDACION'}">
                                                Archivo mal formado
                                            </c:if>   
                                            <c:if test="${item.status=='CARGADO_STAGE'}">
                                                Error ejecutando reglas de negocio
                                            </c:if>      
                                            <c:if test="${item.status=='ERRORES_ESTRUCTURA'}">
                                                Archivo con errores de estructura 
                                            </c:if>    
                                            <c:if test="${item.status=='ERRORES_NEGOCIO'}">
                                                Archivo con errores de negocio. 
                                            </c:if>
                                            <c:if test="${item.status=='FALLA_PROCESAMIENTO'}">
                                                La carga present� un error inesperado
                                            </c:if>  
                                            <c:if test="${item.status=='CARGADO_MASTER'}">
                                                Archivo correctamente cargado a Master data
                                            </c:if>

                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-info btn-xs"
                                                    data-toggle="modal" data-target="#ventanaDetalle"
                                                    onclick="loadDetail(${item.id})">
                                                <i class="fa fa-id-card-o" aria-hidden="true"></i> Ver
                                            </button>
                                        </td>

                                    </tr>

                                </c:forEach>
                            </c:if>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="modal fade" id="detailModal" role="dialog" 
             aria-labelledby="creationModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="creationModalLabel"><h1>Detalle de Carga</h1></h4>
                    </div>

                    <div class="modal-body">
                        <div id="detailArea"> </div>
                    </div>
                </div>
            </div>
        </div>  

        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/jquery.ui.widget.js"></script>
        <script src="resources/js/jquery.iframe-transport.js"></script>
        <script src="resources/js/jquery.fileupload.js"></script>
        <script src="resources/js/jquery.validate.min.js"></script>
        <script src="resources/js/tooltipster.bundle.js"></script>
        <script src="resources/js/custom-functionalities.js"></script>
        <script>
        $(document).ready(function () {
              $(${tab}).tab('show');
            $('#loads').DataTable({
                "language": {
                    "url": "resources/js/Spanish.json"
                }, "initComplete": function (settings, json) {
                    $(".dt-buttons").each(function (index) {
                        console.log(index + ": " + $(this).text());
                        if ($(this).find('.exportar').length === 0) {
                            $(this).prepend('<label class="exportar">Exportar a:&nbsp;&nbsp;</label>');
                        }
                    });
                },
                "bFilter": false,
                "bInfo": false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'print',
                        text: '<i class="fa fa-files-o"></i>',
                        titleAttr: 'COPY'
                    },
                    {
                        extend: 'excel',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        titleAttr: 'EXCEL'
                    },
                    {
                        extend: 'csv',
                        fieldBoundary: '',
                        text: '<i class="fa fa-file-text-o"></i>',
                        titleAttr: 'CSV'
                    },
                    {
                        extend: 'pdf',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        titleAttr: 'PDF'
                    }
                ]
            });
            $.fn.datepicker.dates['es'] = {
                days: ["Domingo", "Lunes", "Martes", "Mi�rcoles", "Jueves", "Viernes", "S�bado", "Domingo"],
                daysShort: ["Dom", "Lun", "Mar", "Mi�", "Jue", "Vie", "S�b", "Dom"],
                daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                today: "Hoy"
            };

            $('#fechaFin').datepicker({language: "es", autoclose: true});
            $('#fechaInicio').datepicker({language: "es", autoclose: true});

            $("#rango").click(function () {
                if ($("#fechaInicio").val() <= $("#fechaFin").val()) {
                    if (validateForm("rangoForm")) {
                        $('#rangoForm').submit();
                    }
                } else {
                    newAlert('alert-danger', '<strong>ATENCION!</strong> El a�o final debe ser mayor al incial');
                }
            });
            $("#periodo").click(function () {

                if ($("#anio").val() > 0) {

                    document.forms["periodoForm"].submit();
                } else {

                    newAlert('alert-danger', '<strong>ATENCION!</strong> Debe de Ingesar un a�o');
                }


            });
            function newAlert(type, message) {
                $("#alert-area").append($("<div class='alert alert-message " +
                        type + " fade in' data-alert><p> " + message + " </p></div>"));
                $(".alert-message").delay(3000).fadeOut("slow", function () {
                    $(this).remove();
                });
            }
        });

        function loadDetail(idCarga) {
            $('#detailModal').modal('show');
            $.ajax({
                type: "GET",
                url: 'detalle-cargue.htm?',
                data: "id=" + idCarga,
                success: function (data) {
                    $('#detailArea').html(data);
                }
            });
        }

        function downloadFile(form) {
            $.ajax({
                type: "GET",
                url: 'descargar-errores.htm',
                data: $("#" + form).serialize(),
                success: function(data) {
                    var blob = new Blob([data], {type: 'text/csv;charset=utf-8;'});
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.setAttribute('download', 'Errores.csv');
                    link.click();
                },
                error: function() {
                    $("#fileMessage").show();
                }
            });
        }

        </script> 
        <style>
            .buttons-csv{
                display: inline-block;
                background-image:url(resources/images/csv.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-csv span{
                opacity: 0;
            }
            .buttons-excel{
                display: inline-block;
                background-image:url(resources/images/excel.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-excel span{
                opacity: 0;
            }
            .buttons-pdf{
                display: inline-block;
                background-image:url(resources/images/pdf.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-pdf span{
                opacity: 0;
            }
            .buttons-print{
                display: inline-block;
                background-image:url(resources/images/print.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-print span{
                opacity: 0;
            }

            div.dt-buttons {
                float: right;
                margin-left:10px;
            }
        </style>
    </body>
</html>    

