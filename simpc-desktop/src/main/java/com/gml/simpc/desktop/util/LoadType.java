/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: LoadType.java
 * Created on: 2017/04/27, 04:54:08 PM
 * Project: SIMPC - Sistema de informaci&ocute,n para el mecanismo de
 *                  protecci&oacute,n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.desktop.util;

/**
 * Enumeraci&oacute;n para tipos de carga.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute,nez</a>
 */
public enum LoadType {

    CAP_1("1", "CAP-1", "Capacitación - Encabezado"),
    CAP_2("2", "CAP-2", "Capacitación - Detalle"),
    ORI_1("3", "ORI-1", "Orientación - Encabezado"),
    ORI_2("4", "ORI-2", "Orientación - Detalle"),
    HDV_1("5", "HDV-1", "Datos Básicos"),
    HDV_2("6", "HDV-2", "Nivel Educativo"),
    HDV_3("7", "HDV-3", "Educación Informal"),
    HDV_4("8", "HDV-4", "Experiencia Laboral"),
    HDV_5("9", "HDV-5", "Idiomas"),
    HDV_6("10", "HDV-6", "Otros Conocimientos"),
    HDV_7("11", "HDV-7", "Intermediación"),
    PES_1("12", "PES-1", "Maestro FOSFEC"),
    PES_2("13", "PES-2", "Dependientes Económicos");

    private String id;
    private String code;
    private String name;

    private LoadType(String id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
