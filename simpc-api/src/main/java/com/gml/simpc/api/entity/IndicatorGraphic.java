/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IndicatorGraphic.java
 * Created on: 2016/12/12, 03:31:25 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import com.gml.simpc.api.enums.GraphicType;
import com.gml.simpc.api.enums.IndicatorGroupFunctiontType;
import com.gml.simpc.api.enums.Periodicity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entidad que define las gr&aacute;ficas de indicador.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Table(name = "GRAFICOS_INDICADOR")
@Entity
public class IndicatorGraphic implements Serializable {

    /**
     * ID del gr&aacute;fico.
     */
    @Id
    @Column(name = "ID", length = 10)
    private Long id;
    
    /**
     * Indicador.
     */
    @ManyToOne
    @JoinColumn(name = "INDICADOR", nullable = false)
    private Indicator indicator;
    
    /**
     * Nombre de la gr&aacute;fica.
     */
    @Column(name = "NOMBRE")
    private String name;
    
    /**
     * Periodicidad del gr&aacute;fico.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "PERIODICIDAD")
    private Periodicity periodicity;
    
    /**
     * N&uacute;mero de periodos para graficar el indicador.
     */
    @Column(name = "NUMERO_PERIODOS")
    private int numberOfPeriods;
    
    /**
     * Tipo de gr&aacute;fico.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "TIPO_GRAFICO")
    private GraphicType type;
    
    /**
     * Variable de agrupaci&oacute;n.
     */
    @Column(name = "VARIABLE_AGRUPACION")
    private String aggrVar;
    
    /**
     * Variable de fecha.
     */
    @Column(name = "VARIABLE_FECHA", length = 50)
    private String dateVar;
    
    /**
     * Etiqueta de la variable de agrupaci&oacute;n.
     */
    @Column(name = "ETIQUETA_VARIABLE_AGRUPACION")
    private String aggrVarLabel;
    
    /**
     * Etiquetas para las gr&aacute;ficas.
     */
    @Column(name = "ETIQUETAS")
    private String labels;

    /**
     *Campo de agrupacion.
     */
    @Column(name = "CAMPO_AGRUPACION" , nullable = true)
    private String groupField;
    
    /**
     * Función de agregación.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "FUNCION_AGREGACION" , nullable = true)
    private IndicatorGroupFunctiontType agregateFunction;
    
    /*-------------------------------------------------------------*/
    
    public Indicator getIndicator() {
        return indicator;
    }

    public void setIndicator(Indicator indicator) {
        this.indicator = indicator;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Periodicity getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(Periodicity periodicity) {
        this.periodicity = periodicity;
    }

    public int getNumberOfPeriods() {
        return numberOfPeriods;
    }

    public void setNumberOfPeriods(int numberOfPeriods) {
        this.numberOfPeriods = numberOfPeriods;
    }

    public GraphicType getType() {
        return type;
    }

    public void setType(GraphicType type) {
        this.type = type;
    }

    public String getAggrVar() {
        return aggrVar;
    }

    public void setAggrVar(String aggrVar) {
        this.aggrVar = aggrVar;
    }

    public String getAggrVarLabel() {
        return aggrVarLabel;
    }

    public void setAggrVarLabel(String aggrVarLabel) {
        this.aggrVarLabel = aggrVarLabel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDateVar() {
        return dateVar;
    }

    public void setDateVar(String dateVar) {
        this.dateVar = dateVar;
    }

    public String getLabels() {
        return labels;
    }

    public void setLabels(String labels) {
        this.labels = labels;
    }

    public String getGroupField() {
        return groupField;
    }

    public void setGroupField(String groupField) {
        this.groupField = groupField;
    }

    public void setAgregateFunction(IndicatorGroupFunctiontType agregateFunction) {
        this.agregateFunction = agregateFunction;
    }

    public IndicatorGroupFunctiontType getAgregateFunction() {
        return agregateFunction;
    }

   
    
}
