/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserController.java
 * Created on: 2016/10/19, 02:14:58 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.gml.simpc.api.entity.Parameter;
import com.gml.simpc.api.services.ParameterService;
import com.gml.simpc.commons.annotations.FunctionalityInterceptor;
import com.gml.simpc.web.controller.ControllerDefinition;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_001_UNKNOW;
import static com.gml.simpc.api.utilities.Util.FAILED_ALERT;
import static com.gml.simpc.api.utilities.Util.SUCCESS_ALERT;
import static com.gml.simpc.api.utilities.Util.likeValue;

/**
 * Controlador para la ventana de manejo de consulta parametros.
 *
 * @author <a href="mailto:jonathanp@gmlsoftware.com">Jonathan Ponton</a>
 */
@FunctionalityInterceptor(name = "consulta-parametros.htm")
@Controller
public class ParameterController implements ControllerDefinition {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = 
        Logger.getLogger(ParameterController.class);

    /**
     * Servicio para adminisraci&oacute;n de consulta parametros; Servicio
     * de parametros intermediario para la comunicaci&oacute;n con la
     * persistencia.
     */
    @Autowired
    private ParameterService parameterService;

    /**
     * M&eacute;todo que inicia la consulta de parametros.
     * 
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = "consulta-parametros",method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,HttpServletResponse response) {
        LOGGER.info("Ejecutando metodo [ onDisplay consulta-parametros ]");
        ModelAndView modelAndView = new ModelAndView("consulta-parametros");
        try {
            modelAndView.addObject("parameterList",this.parameterService.getAll());
            modelAndView.addObject("selectedParameter",new Parameter());
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("Finaliza la invocacion de [ onDisplay ].");
        }

        init(modelAndView, false);

        return modelAndView;
    }

    /**
     * M&eacute;todo para buscar parametros segun los filtros ingresados.
     * 
     * @param request
     * @param response
     * @param searchParameter
     * @return 
     */
    @RequestMapping(value = "buscarParametros",method = RequestMethod.POST)
    public ModelAndView search(HttpServletRequest request,HttpServletResponse response,
            @ModelAttribute("searchParameter") Parameter searchParameter) {
        
        LOGGER.info("entra a consultar en el controller de parametros");
        ModelAndView modelAndView = new ModelAndView("consulta-parametros");

        try {
            modelAndView.addObject("parameterList",this.parameterService.
                findBySpNameLikeIgnoreCase(likeValue(searchParameter.getName())));
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("Finaliza la invocacion de [ buscarParametros ].");
        }

        init(modelAndView, false);

        return modelAndView;
    }
    
    /**
     * M&eacute;todo para abrir el detalle de un parametro.
     * 
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = "detalle-parametro", method = RequestMethod.GET)
    public ModelAndView detail(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.info("Ejecutando metodo [ detail ]");
        ModelAndView modelAndView = new ModelAndView("detalle-parametro");
        if (request.getParameter("id") != null) {
            try {
                modelAndView.addObject("parameter",
                    this.parameterService.findById(Long.parseLong(request.getParameter("id"))));
            } catch (Exception ex) {
                LOGGER.error("Error ", ex);
                modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
            } finally {
                LOGGER.info("Finaliza la invocacion de [ detail ].");
            }
            init(modelAndView, true);
        } else {
            init(modelAndView, false);
        }

        return modelAndView;
    }
    
    /**
     * M&oacute;todo para abrir la edici&oacute;n de un parametro.
     * 
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = "edicion-parametro", method = RequestMethod.GET)
    public ModelAndView edit(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.info("Ejecutando metodo [ edit ]");
        ModelAndView modelAndView = new ModelAndView("edicion-parametro");
        if (request.getParameter("id") != null) {
            try {
                modelAndView.addObject("parameter",
                    this.parameterService.findById(Long.parseLong(request.getParameter("id"))));
            } catch (Exception ex) {
                LOGGER.error("Error ", ex);
                modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
            } finally {
                LOGGER.info("Finaliza la invocacion de [ edit ].");
            }
            
            init(modelAndView, true);
        } else {
            init(modelAndView, false);
        }

        return modelAndView;
    }
    
    /**
     * M&eacute;todo que guarda un parametro.
     * 
     * @param request
     * @param response
     * @param parameter
     * @return 
     */
    @RequestMapping(value = "guardarParametro",method = RequestMethod.POST)
    public ModelAndView save(HttpServletRequest request, HttpServletResponse response, 
        @ModelAttribute("selectedParameter") Parameter parameter) {

        ModelAndView modelAndView = null;
        boolean creationError = false;

        try {
            this.parameterService.update(parameter);
            
            modelAndView = onDisplay(request, response);
            
            modelAndView.addObject("msg", "Se ha actualizado el parametro " + parameter.getSpName() + " exitosamente.");
            modelAndView.addObject("msgType", SUCCESS_ALERT);
            
        } catch (ConstraintViolationException ex) {
            LOGGER.error("Error", ex);
            
            StringBuilder builder = new StringBuilder("Alertas: ");
            for (ConstraintViolation error : ex.getConstraintViolations()) {
                builder.append(error.getMessage());
                builder.append("\n");
            }
            modelAndView = onDisplay(request, response);
            modelAndView.addObject("internalMsg", builder.toString());
            modelAndView.addObject("creationInstitution", parameter);
            modelAndView.addObject("msgType", FAILED_ALERT);
            creationError = !creationError;
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView = onDisplay(request, response);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
            modelAndView.addObject("msgType", FAILED_ALERT);
        } finally {
            LOGGER.info("finally save in parameterController");
        }

        init(modelAndView, creationError);

        return modelAndView;
    }

    /**
     * M&eacute;todo que inicializa el model and view.
     * 
     * @param modelAndView
     * @param creationError 
     */
    private void init(ModelAndView modelAndView, boolean creationError) {
        LOGGER.info("Ejecutando metodo [ init ]");
        if (!creationError) {
            modelAndView.addObject("searchParameter", new Parameter());
        }
    }
}
