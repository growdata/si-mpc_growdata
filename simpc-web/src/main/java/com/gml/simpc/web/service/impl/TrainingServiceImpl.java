/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: SupplierBankServiceImpl.java
 * Created on: 2017/01/12, 03:14:16 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.dto.TrainingDto;
import com.gml.simpc.api.services.TrainingService;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

/**
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service
public class TrainingServiceImpl implements TrainingService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(TrainingServiceImpl.class);

    /**
     * Plantilla JDBC para consultar las propiedades.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Consulta base para contadores de modulos especifico; Incluye select, from
     * y where para caja de compesaci�n y tipo de institucion.
     */
    private static final String FIND_MODULES_COUNTS_BASE =
        " select " +
        " tipos_cap.tipo_cap_id ,tipos_cap.tipo_cap_nombre ,data.tipo_inst_id,data.tipo_inst_nombre,data.cant" +
        " from (" +
        "    select tcap.id as tipo_cap_id ,tcap.nombre as tipo_cap_nombre from  TIPOS_PROGRAMA tcap" +
        " )tipos_cap " +
        " left join" +
        " (" +
        "  select  info.tipo_cap_id  ,info.tipo_cap_nombre  ,info.tipo_inst_id ,info.tipo_inst_nombre  ,count(distinct(info.codigo_modulo)) cant" +
        "  from " +
        "        MD_VIEW_portal_cap info" +
        " where " +
        "        info.CCF_CODE like ?     " +
        "        and info.TIPO_INST_ID like ?     ";

    private static final String FIND_MODULES_COUNTS_ANNIO =
        "and EXTRACT(year FROM  info.FECHA_FIN) = ";

    private static final String FIND_MODULES_COUNTS_MES =
        "and EXTRACT(month FROM  info.FECHA_FIN) = ";

    private static final String FIND_MODULES_COUNTS_END =
        "  group by " +
        "       info.tipo_cap_id ,info.tipo_cap_nombre  ,info.tipo_inst_id ,info.tipo_inst_nombre" +
        "  order by " +
        "        info.tipo_inst_nombre" +
        " )data" +
        " on tipos_cap.tipo_cap_id = data.tipo_cap_id" +
        " order by tipos_cap.tipo_cap_nombre";

    /**
     * M&ecute;todo que genera los resultados del portal de capacitaciones.
     * 
     * @param ccf
     * @param instType
     * @param anio
     * @param mes
     * @return 
     */
    @Override
    public Table getModuleCounts(String ccf, String instType, String anio,
        String mes) {
        LOGGER.info("Consulta para portal de capacitacion: " +
            "ccfSel: " + ccf + "tipoInstitucionSel: " + instType +
            ", a�o: " + anio + ", mes: " + mes);
        Table<Long, Long, Integer> resultTable = HashBasedTable.create();
        List<TrainingDto> prevList;

        String ccfSel = ccf;
        String instTypeSel = instType;

        if ("0".equals(ccf)) {
            ccfSel = "%";
        }
        if ("0".equals(instTypeSel)) {
            instTypeSel = "%";
        }

        String query = FIND_MODULES_COUNTS_BASE;

        if (!"0".equals(anio)) {
            query = query + FIND_MODULES_COUNTS_ANNIO + anio;
        }
        if (!"0".equals(mes)) {
            query = query + FIND_MODULES_COUNTS_MES + mes;
        }
        query = query + FIND_MODULES_COUNTS_END;
        try {
            prevList = this.jdbcTemplate.
                query(query, new RowMapper() {
                    @Override
                    public TrainingDto mapRow(ResultSet rs, int rownumber)
                    throws
                    SQLException {
                        TrainingDto trainingDao = new TrainingDto();
                        trainingDao.setTipoCapId(rs.getLong("TIPO_CAP_ID"));
                        trainingDao.setTipoCapNombre(rs.getString(
                                "TIPO_CAP_NOMBRE"));
                        trainingDao.setTipoInstId(rs.getLong("TIPO_INST_ID"));
                        trainingDao.setTipoInstNombre(rs.getString(
                                "TIPO_INST_NOMBRE"));
                        trainingDao.setContModules(rs.getInt("CANT"));
                        return trainingDao;
                    }
                }, ccfSel, instTypeSel);
            for (TrainingDto trainingDao : prevList) {
                resultTable.put(trainingDao.getTipoCapId(),
                    trainingDao.getTipoInstId(), trainingDao.getContModules());
            }
        } catch (DataAccessException e) {
            LOGGER.error("Error in query " + query);
            LOGGER.error(e);
        }

        return resultTable;
    }

}
