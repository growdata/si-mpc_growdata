/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gml.simpc.loader.listener;

import org.springframework.batch.core.step.skip.SkipLimitExceededException;
import org.springframework.batch.core.step.skip.SkipPolicy;
import org.springframework.dao.EmptyResultDataAccessException;

/**
 *
 * @author Usuario
 */
public class CustomSkipPolicy implements SkipPolicy {

    @Override
    public boolean shouldSkip(Throwable thrwbl, int i) 
            throws SkipLimitExceededException {
        if (thrwbl instanceof EmptyResultDataAccessException) {
            return true;
        }
        
        return false;
    }
}
