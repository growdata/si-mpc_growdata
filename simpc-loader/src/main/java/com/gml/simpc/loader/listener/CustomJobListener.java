/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: LoadResultDto.java
 * Created on: 2017/12/23, 04:21:39 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * Objective: to provide actions before and after the execution of batch jobs.
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.listener;
/**
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica Jim&eacute;nez Rozo</a>
 */

import com.gml.simpc.loader.thread.ThreadLocalJob;
import org.apache.log4j.Logger;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

public class CustomJobListener implements JobExecutionListener{
    
    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(CustomJobListener.class);

    @Override
    public void beforeJob(JobExecution jobExecution) {
        LOGGER.info("JOBLISTENER: Executing Job with ID: ".concat(jobExecution.getJobId().toString()));
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        LOGGER.info("JOBLISTENER: Job finished with ID: ".
            concat(jobExecution.getJobId().toString()).
            concat(" in status ".concat(jobExecution.getStatus().toString()).
            concat(" steps "+jobExecution.getStepExecutions())));
        ThreadLocalJob.setStatus(jobExecution.getStatus().toString());
    }

}
