/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IService.java
 * Created on: 2016/10/24, 11:31:09 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.entity.Functionality;
import java.util.List;

/**
 *
 * @author <a href="mailto:julianm@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
public interface FunctionalityService {

    /**
     * Encuentra las funcionalidades para un usuario.
     *
     * @param username
     *
     * @return
     */
    List<Functionality> getFunctionalitiesByUsername(String username);

    /**
     * Encuentra las funcionalidades para un usuario.
     *
     * @param id
     *
     * @return
     */
    List<Functionality> getFunctionalitiesByNotProfile(Long id);

    /**
     * Encuentra las funcionalidades para un usuario.
     *
     * @param id
     *
     * @return
     */
    List<Functionality> getFunctionalitiesByProfile(Long id);

    /**
     * Encuentra las funcionalidades para un usuario.
     *
     * @param id
     *
     * @return
     */
    List<Functionality> getAll();
}
