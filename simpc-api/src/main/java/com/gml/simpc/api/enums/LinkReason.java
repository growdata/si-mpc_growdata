/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: LinkReason.java
 * Created on: 2017/02/21, 10:17:08 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.enums;

/**
 * Raz&oacute;n por la cual se env&iacute;a el enlace de activaci&oacute;n de
 * password.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public enum LinkReason {

    CREATION, FORGOT, EXPIRATION,PREV_EXPIRATION;
}
