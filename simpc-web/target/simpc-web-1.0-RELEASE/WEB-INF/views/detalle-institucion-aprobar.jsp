<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>MECANISMO DE PROTECCI�N AL CESANTE "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css//bootstrap-toggle.min.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
    </head>
    <body>
        <c:if test="${not empty msg}">
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

        <div class="col-sm-12">
            <h1>Datos de la Instituci&oacute;n</h1>
            <br/>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="tipoInsitucion" class=" control-label">Tipo de Instituci&oacute;n</label>
                    <p class="form-control-static">${parentInstitution.type.name}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="nombreInstitucion" class="control-label">Nombre de la Instituci&oacute;n</label>
                    <p class="form-control-static">${parentInstitution.institutionName}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="nombreInstitucion" class="control-label">Tipo de Documento</label>
                    <p class="form-control-static">${parentInstitution.documentType.name}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="nit" class="control-label">N�mero de Documento</label>
                    <p class="form-control-static">${parentInstitution.nit}</p>
                </div>
            </div>
            <c:if test="${parentInstitution.dv != null}">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="dv" class="control-label">D&iacute;gito de Verificaci&oacute;n</label>
                        <p class="form-control-static">${parentInstitution.dv}</p>
                    </div>
                </div>
            </c:if>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="origen" class=" control-label">Origen</label>
                    <p class="form-control-static">${parentInstitution.origin.label}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="naturalezaLegal" class=" control-label">Naturaleza Jur&iacute;dica</label>
                    <p class="form-control-static">${parentInstitution.legalNature.label}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="telefonoContacto" class="control-label">Tel&eacute;fono de Contacto</label>
                    <p class="form-control-static">${parentInstitution.contactPhone}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="emailContacto" class="control-label">Email de Contacto</label>
                    <p class="form-control-static">${parentInstitution.contactEmail}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="emailContacto" class="control-label">Estado de Activaci�n</label>
                    <p class="form-control-static">${parentInstitution.status.label}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="emailContacto" class="control-label">Estado de Aprobaci�n</label>
                    <p class="form-control-static">${parentInstitution.approvalStatus.label}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="emailContacto" class="control-label">Tipo de Certificaci�n Emitida</label>
                    <p class="form-control-static">${parentInstitution.typeQualityCertificate.code}</p>
                </div>
            </div>
            <c:if test="${parentInstitution.qualityCertificate != null}">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="emailContacto" class="control-label">Fecha de Vencimiento de la Certificaci&oacute;n</label>
                    <p class="form-control-static">${parentInstitution.dateToShow}</p>
                </div>
            </div>
            </c:if>
            <div class="clearfix"></div>
            <div class="col-sm-4" style="vertical-align: bottom">
                <div class="form-group">
                    <form method="post" action="/masterdata/descargarCertificado.htm" target="_blank">
                        <input type="hidden" name="id" value="${parentInstitution.id}"/>
                        <button type="submit" class="btn btn-success" 
                                <c:if test="${parentInstitution.dateToShow == null}">
                                    disabled="true"
                                </c:if>
                                >Descargar Certificaci&oacute;n de Calidad </button>
                    </form>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <br/>                
        <div class="col-sm-12">
            <h1>Datos de las Sedes Asociadas</h1>
            <br/>
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Detalles Sedes Asociadas</h2></div>
                <div class="panel-body">
                    <div class=" table-responsive">
                        <table id="headqList" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Nombre de la Sede</th>
                                    <th>Direcci&oacute;n</th>
                                    <th>Municipio</th>
                                    <th>Departamento</th>
                                    <th>Principal</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Nombre de la Sede</th>
                                    <th>Direcci&oacute;n</th>
                                    <th>Municipio</th>
                                    <th>Departamento</th>
                                    <th>Principal</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <c:if test="${not empty Headquarter}">
                                    <c:forEach var="item" items="${Headquarter}">
                                        <tr>
                                            <td>${item.name}</td>
                                            <td>${item.address}</td>
                                            <td>${item.state.name}</td>
                                            <td>${item.city.name}</td>
                                            <td>
                                                <c:if test="${item.principal==true}">
                                                    <input type="checkbox" name="dance"
                                                           checked disabled readonly/>
                                                </c:if>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </c:if>    
                            </tbody>

                        </table>
                    </div>
                </div>
            </div> 
        </div>
        <form:form id="aprobacionForm"  action="aprobacion.htm" target="_parent">
            <center>
                <input type="hidden" name="email" id="email"  value='${parentInstitution.contactEmail}'> 
                <input type="hidden" name="name" id="name"  value='${parentInstitution.institutionName}'> 
                <input type="hidden" name="nameConsultor" id="nameConsultor"  value='${parentInstitution.getCreationUserId().getEmail()}'> 
                <input type="hidden" name="valorId" id="valorId"  value='${parentInstitution.id}'>     
                <label class="btn-xs" >
                    <h4>
                        <input type="checkbox" name="selected" id="selected" class="btn btn-xs" 
                        checked data-toggle="toggle" data-on="SI" data-off="NO" 
                        data-onstyle="success" data-offstyle="danger"/>
                    </h4>
                    Aprobar Instituci&oacute;n</label>
            </center>
            <div class="modal-footer">        
                <div class="col-sm-12" style="text-align:right">
                    <button class="btn btn-success btn-primary" data-btn-ok-label="Confirmar" 
                            data-btn-cancel-label="Cancelar" data-title="Est� seguro?" 
                            data-toggle="confirmation" data-placement="top" 
                            confirmation-callback>Guardar
                    </button>
                    <button type="button" class="btn btn-danger btn-primary" 
                            onclick="cerrarse()">Cancelar
                    </button> 
                </div>
            </div>
        </form:form>  
    
        <div class="modal fade " id="exampleModal"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="dialog">
                <div class="modal-content "   >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="exampleModalLabel"><strong>Causa de rechazo de la Intituci&oacute;n</strong> </h4>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="alert alert-warning alert-dismissible" id="myAlert">
                                <font SIZE=3> Por favor ingrese la causa del rechazo. </font>
                            </div>
                        </div>
                        <form:form id="rechazaForm"  action='rechazar.htm' target="_parent">                       
                            <div class="form-group">
                                <input type="hidden" name="email" id="email"  value='${parentInstitution.contactEmail}'> 
                                <input type="hidden" name="nameConsultor" id="nameConsultor"  value='${parentInstitution.getCreationUserId().getEmail()}'> 
                                <input type="hidden" name="name" id="name"  value='${parentInstitution.institutionName}'> 
                                <input type="hidden" name="valorId" id="valorId"  value='${parentInstitution.id}'> 
                                <label for="message-text" class="form-control-label">Causa:</label>
                                <textarea class="form-control" id="message-text" name="message-text"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" 
                                    onclick="closeWindow()">Enviar mensaje
                            </button>
                        </div>
                    </form:form >             
                </div>
            </div>
        </div>

        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/bootstrap-confirmation.js"></script>
        <script src="resources/js/jquery.ui.widget.js"></script>
        <script src="resources/js/jquery.iframe-transport.js"></script>
        <script src="resources/js/jquery.fileupload.js"></script>
        <script src="resources/js/bootstrap-toggle.min.js"></script>
        <script>
            
            $(document).ready(function () {
                
                $('.container').hide();
                
                $('#headqList').DataTable({
                    "language": {
                        "url": "resources/js/Spanish.json"
                    },
                    "bFilter": false,
                    "bInfo": false
                });

                loadStates("departamento", "municipio");
                loadStates("departamentoModal", "municipioModal");
            });

            function loadStates(deptoId, mnpoId) {
                <c:if test="${not empty stateList}">
                    <c:forEach var="item" items="${stateList}" varStatus="iter">
                        <c:if test="${iter.index == 0}">
                                            jQuery("<option>").attr("value", '${item.id}').
                                                    text('${item.name}').attr("selected", "true").
                                                    appendTo("#" + deptoId);

                                            loadCities("${item.id}", deptoId, mnpoId);
                        </c:if>
                        <c:if test="${iter.index > 0}">
                                            jQuery("<option>").attr("value", '${item.id}').
                                                    text('${item.name}').appendTo("#" + deptoId);
                        </c:if>
                    </c:forEach>
                </c:if>
            }

            function loadCitiesByState(deptoId, mnpoId) {
                jQuery("#" + deptoId).change(function () {
                    jQuery("#" + mnpoId).html('');
                    var stateCode = jQuery("#" + deptoId).val();

                    loadCities(stateCode, deptoId, mnpoId);
                });
            }

            function loadCities(stateCode, deptoId, mnpoId) {
                jQuery("#" + mnpoId).find('option').remove();

                <c:if test="${not empty citiesList}">
                    <c:forEach items="${citiesList}" var="item" >
                        var code = "<c:out value="${item.state.id}"/>";

                        if (code === stateCode) {
                            jQuery("<option>").attr("value", '${item.id}').
                                    text('${item.name}').appendTo("#" + mnpoId);
                        }
                    </c:forEach>
                </c:if>
            }

            $('body').confirmation({
                selector: '[data-toggle="confirmation"]', onConfirm: function (event, element) {
                    if ($('#selected').is(':checked')) {
                        document.forms["aprobacionForm"].submit();
                    } else {
                        $('#exampleModal').modal('show');
                    }
                }
            });

            function closeWindow() {

                if($.trim($("#message-text").val()) === ""){
                    $('.container').hide("fast");
                    $('.container').show("fast");
                } else {
                    document.forms["rechazaForm"].action = 'rechazar.htm';
                    document.forms["rechazaForm"].submit();
                }

            }
            function cerrarse() {
                parent.location.reload();
            }
        </script>
    </body>
</html>
