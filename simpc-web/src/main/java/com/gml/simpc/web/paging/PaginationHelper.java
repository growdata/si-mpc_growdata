package com.gml.simpc.web.paging;

import com.gml.simpc.api.dto.Page;
import com.gml.simpc.api.dto.TableDataDto;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

// TODO: complete this object/class
public class PaginationHelper {

    public Page<TableDataDto> fetchPage(final JdbcTemplate jt, 
        final String sqlCountRows, final String sqlFetchRows,
        final int pageNo, final int pageSize, 
        final RowMapper<TableDataDto> rowMapper) {

        // determine how many rows are available
        final int rowCount = jt.queryForObject(sqlCountRows, Integer.class);

        // calculate the number of pages
        int pageCount = rowCount / pageSize;
        
        if (rowCount > pageSize * pageCount) {
            pageCount++;
        }

        // create the page object
        final Page<TableDataDto> page = new Page<>();
        page.setPageNumber(pageNo);
        page.setPagesAvailable(pageCount);
        page.setRecordsTotal(rowCount);
        page.setRecordsFiltered(rowCount);

        // fetch a single page of results
        final int startRow = (pageNo - 1) * pageSize;
        jt.query(sqlFetchRows, new ResultSetExtractor() {
            
            @Override
            public Object extractData(ResultSet rs) throws SQLException,
                DataAccessException {
                final List pageItems = page.getData();
                int currentRow = 0;
                
                while (rs.next() && currentRow < startRow + pageSize) {
                    
                    if (currentRow >= startRow) {
                        pageItems.add(rowMapper.mapRow(rs, currentRow));
                    }
                    
                    currentRow++;
                }
                
                return page;
            }
        });
        
        return page;
    }
}
