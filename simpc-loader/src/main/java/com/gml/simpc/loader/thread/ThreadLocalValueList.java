/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: LocalThreadMessage.java
 * Created on: 2017/01/03, 11:56:31 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.thread;

import com.gml.simpc.api.utilities.ValueList;

/**
 * Clase para almacenar en thread local las listas de valores paramétricas
 * del sistema
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica Jim&eacute;nez Rozo</a>
 */
public final class ThreadLocalValueList {
    
    /**
     * Objeto thread local.
     */
    public static final ThreadLocal<ValueList> THREAD  = new ThreadLocal<>();
 
    /**
     * Constructor privado para impedir instacias de la clase.
     */
    private ThreadLocalValueList() {}
    
    /**
     * Obtiene todas las listas de valores.
     *
     */
    public static ValueList get() {
        return THREAD.get();
    }

    /**
     * Actualiza en el thread todas las listas de valor.
     * @param valor
     *
     */
    public static void set(ValueList valueListService) {
        THREAD.set(valueListService);
    }
    
     /**
     * Elimina el valor del thread.
     */
    public static void delete() {
        THREAD.remove();
    }
}
