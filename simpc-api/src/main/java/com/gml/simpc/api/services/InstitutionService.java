/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: InstitutionService.java
 * Created on: 2016/10/19, 02:10:35 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.entity.Institution;
import com.gml.simpc.api.entity.exeption.UserException;
import com.gml.simpc.api.entity.valuelist.InstitutionType;
import com.gml.simpc.api.enums.ApprovalInstitutionStatus;
import java.util.Date;
import java.util.List;

/**
 * Interface que define el servicio de instituciones oferentes.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public interface InstitutionService {

    /**
     * Obtiene todas las instituciones oferentes creadas.
     *
     * @return
     */
    List<Institution> getAll();

    /**
     * Servicio para guardar una instituci&oacute;n oferente.
     *
     * @param institution
     */
    void save(Institution institution);

    /**
     * Servicio para actualizar instituciones oferentes.
     *
     * @param institution
     */
    void update(Institution institution) throws UserException;

    /**
     * Elimina una instituci&oacute;n oferente.
     *
     * @param institution
     */
    void remove(Institution institution);

    /**
     * Metodo que consulta por filtros
     *
     * @param institution
     *
     * @return
     */
    List<Institution> findByFilters(Institution institution);

    /**
     * Metodo que consulta por filtros las instituciones aprobadas
     *
     * @param institutionName
     * @param institutionType
     * @param regisCcf
     * @param regisDateFin
     * @param approDateIni
     * @param approDateFin
     * @param regisDateIni
     * @return
     */
    List<Institution> findApprovedInstitutions(String institutionName,
        String institutionType, String regisCcf, Date approDateIni,
        Date approDateFin,Date regisDateIni,Date regisDateFin);

    /**
     * Metodo que consulta los tipos de institucion.
     *
     * @return
     */
    List<InstitutionType> findAllInstitutionTypes();

    /**
     * Consulta una instituci&oacute;n por ID.
     *
     * @param id
     *
     * @return
     */
    Institution findById(Long id);

    /**
     * Consulta instituciones por estado.
     *
     * @param status
     *
     * @return
     */
    List<Institution> findByApprovalStatus(ApprovalInstitutionStatus status);

    /**
     * Metodo que actualiza el campo de estado de aprobación
     *
     * @param status
     * @param updateUserId
     * @param today
     * @param idInstitution
     */
    void updateApprovalState(char status, Long updateUserId, Date today,
        Long idInstitution);

    /**
     * Metodo que actualiza el campo de estado
     *
     * @param status
     * @param updateUserId
     * @param today
     * @param idInstitution
     */
    void updateState(char status, Long updateUserId, Date today,
        int idInstitution);

    /**
     * Metodo que activa por primera ves una institución
     * 
     * @param status
     * @param approvalStatus
     * @param updateUserId
     * @param today
     * @param idInstitution
     */
    Institution activateInstitution(String status, String approvalStatus, 
        Long updateUserId, Date today, Long idInstitution);
    
}
