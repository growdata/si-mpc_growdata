<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <style>
            input[type=number]::-webkit-outer-spin-button,
            input[type=number]::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            input[type=number] {
                -moz-appearance:textfield;
            }
        </style>
    </head>
    <body>
        <c:if test="${not empty msg}">
            <br/>
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        <c:if test="${loadProcess.status=='ESTRUCTURA_VALIDADA'}">
            <c:set var="altype" value="success" />
        </c:if>
        <c:if test="${loadProcess.status=='ERRORES_ESTRUCTURA'}">
            <c:set var="altype" value="warning" />
        </c:if>
        <c:if test="${loadProcess.status=='FALLA_VALIDACION'}">
            <c:set var="altype" value="danger" />
        </c:if>            
        <h1>Portal de cargas</h1>
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading"><h2>Resultado de la carga</h2></div>
            <div class="panel-body">
                <div class="alert alert-${altype} alert-dismissible" role="alert">
                    <ul>
                        <c:set var="errorsList" value="${loadProcess.detail.split(';')}"/>
                        <c:forEach var="error" items="${errorsList}">
                            <li>
                                ${error}
                            </li>
                        </c:forEach>
                    </ul>        
                </div>    
                <div class="col-sm-10">
                    <div class="form-group">
                        <label for="name" class=" control-label">Id de carga:</label>
                        <p class="form-control-static">${loadProcess.id}</p>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="form-group">
                        <label for="name" class=" control-label">Nombre del archivo cargado:</label>
                        <p class="form-control-static">${loadProcess.originalFileName}</p>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="form-group">
                        <label for="description" class=" control-label">Ruta de destino:</label>
                        <p class="form-control-static">${loadProcess.newFilePath}${loadProcess.newFileName}</p>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="form-group">
                        <label for="description" class=" control-label">Tama�o:</label>
                        <p class="form-control-static">${loadProcess.size}</p>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="form-group">
                        <label for="totalRows" class=" control-label">Registros totales recibidos:</label>
                        <p class="form-control-static">${loadProcess.totalRows}</p>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="form-group">
                        <label for="successRows" class=" control-label">Registros con estructura v�lida</label>
                        <p class="form-control-static">${loadProcess.successRows}</p>
                    </div>
                </div>

                <div class="col-sm-10">
                    <div class="form-group">
                        <label for="failRows" class=" control-label">Registros con errores</label>
                        <p class="form-control-static">${loadProcess.failRows}</p>
                    </div>
                </div>  
                <div class="col-sm-10">   
                    <a id="" href="cargar-archivo.htm" class="btn btn-info">Iniciar Nuevo Cargue</a>
                    <a id="" href="consulta-cargas.htm" class="btn btn-info">Ir a lista de Cargues</a>

                </div>             

                <div class="clearfix"></div>
            </div>
        </div>

        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/jquery.ui.widget.js"></script>
        <script src="resources/js/jquery.iframe-transport.js"></script>
        <script src="resources/js/jquery.fileupload.js"></script>
        <script>

        </script>                                           
    </body>
</html>    

