/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: TableDataDto.java
 * Created on: 2017/01/04, 09:44:30 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class TableDataDto implements Serializable {

    /**
     * Columnas con informaci&oacute;n.
     */
    private final List<String> columns;
    
    public TableDataDto() {
        this.columns = new LinkedList<>();
    }
    
    /**
     * Agrega la data de una columna al DTO.
     * @param data 
     */
    public final void addColumn(String data) {
        this.columns.add(data);
    }
    
    /**
     * Retorna la data de una columna del DTO.
     * @param pos
     * @return 
     */
    public final String getColumn(int pos) {
        return this.columns.get(pos);
    }

    public List<String> getColumns() {
        return columns;
    }
}
