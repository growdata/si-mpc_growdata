/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Annotation.java
 * Created on: 2016/11/04, 03:26:00 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.commons.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
/**
 * Anotaci&oacute;n para el ejemplo.
 *
 * @author Javier Rocha
 */
@Target({TYPE, METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AnnotationInterceptor {

    /**
     * Valor para imprimir antes de la ejecuci&oacute;n del m&eacute;todo.
     *
     * @return
     */
    String beforeExecutionValue();

    /**
     * Valor para imprimir despues de la ejecuci&oacute;n del m&eacute;todo.
     *
     * @return
     */
    String afterExecutionValue();

    /**
     * Nombre del m&eacute;todo que se ejecuta
     */
    String methodName();

    /**
     * Nombre de la entidad que ser&aacute; modificado o consultado o creado
     */
    String entityTableName();
}
