/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ApplicationContextAwareImp.java
 * Created on: 2017/02/08, 09:58:17 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.batch;

import org.springframework.batch.core.Job;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.hibernate.dialect.Oracle12cDialect;

/**
 *
 * @author angie
 */
@Service("ApplicationContextAwareImp")
public class ApplicationContextAwareImp implements ApplicationContextAware {

    private ApplicationContext context;

    public ApplicationContext getContext() {
        return context;
    }

    @Override
    public void setApplicationContext(ApplicationContext context)
        throws BeansException {
        this.context = context;
    }

    /**
     * Obtiene un Job por nombre.
     *
     * @param jobName
     *
     * @return
     */
    public Job getJob(String jobName) {
        return this.context.getBean(jobName, Job.class);
    }

    /**
     * Obtiene un bean por tipo de clase.
     *
     * @param classType
     *
     * @return
     */
    public <T> T getBean(Class<T> classType) {
        return this.context.getBean(classType);
    }
}
