/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserRepository.java
 * Created on: 2016/10/19, 02:03:51 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository.valuelist;

import com.gml.simpc.api.entity.valuelist.LoadType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Repositorio para tipos de carga
 *
 *  <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
public interface LoadTypeRepository extends JpaRepository<LoadType, String> {

    /**
     * Consulta por nombre.
     * @param name
     * @return 
     */
    @Query(value =
        "SELECT * FROM WF_TIPOS_CARGA WHERE NOMBRE = ?1 AND ROWNUM = 1",
        nativeQuery = true)
    LoadType findByName(String name);

    /**
     * Consulta por nombre id.
     * @param id
     * @return 
     */
    public LoadType findById(Long id);
}
