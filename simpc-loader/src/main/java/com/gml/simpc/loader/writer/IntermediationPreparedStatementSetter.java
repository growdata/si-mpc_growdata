/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: TrainingPreparedStatementSetter.java
 * Created on: 2017/01/20, 12:16:58 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.writer;

import com.gml.simpc.api.dto.IntermediationDto;
import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.springframework.batch.item.database.ItemPreparedStatementSetter;

/**
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica Jim&eacute;nez Rozo</a>
 */
public class IntermediationPreparedStatementSetter 
        implements ItemPreparedStatementSetter<IntermediationDto> {
 
    @Override
    public void setValues(IntermediationDto intermediationDto, 
        PreparedStatement ps) 
            throws SQLException {
        ThreadLocalJob.setProcesed();
        JobResultDto jobResult = ThreadLocalJob.getResult();
        int line = jobResult.getProcesedLines();
         ps.setString(1, intermediationDto.getDocumentType());
         ps.setString(2, intermediationDto.getDocumentNumber());
         ps.setString(3, intermediationDto.getCompany());
        ps.setString(4, intermediationDto.getVacancyCode());
        ps.setString(5, intermediationDto.getApplicationDate());
        ps.setString(6, intermediationDto.getApplicationStatus());
        ps.setString(7, intermediationDto.getDeclineReason());
        ps.setString(8, intermediationDto.getDeclineDate());
        ps.setString(9, intermediationDto.getProcessStatus());
        ps.setString(10, intermediationDto.getReferralDate());
        ps.setString(11, intermediationDto.getCompanyRejectionReason());
        ps.setDouble(12, Double.parseDouble(intermediationDto.getSalaryProposal()));
        ps.setString(13, intermediationDto.getCompanyResponseDate());
        ps.setString(14, intermediationDto.getJustification());
        ps.setString(15, Integer.toString(line));
    }

}
