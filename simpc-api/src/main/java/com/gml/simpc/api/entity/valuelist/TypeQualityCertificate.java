/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: TypeQualityCertificate.java
 * Created on: 2017/07/07, 10:08:57 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity.valuelist;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author <a href="mailto:jonathanP@gmlsoftware.com">Jonathan Pont�n</a>
 */
@Table(name = "TIPO_CERTIFICACIONES")
@Entity
public class TypeQualityCertificate implements Serializable {

    /**
     * Identificador del tipo de certifiacion.
     */
    @Id
    @Column(name = "ID", length = 20)
    private Long id;
    /**
     * C&oacute;digo.
     */
    @Column(name = "CODIGO", length = 50)
        private String code;


    public TypeQualityCertificate(long id, String code) {
        this.id = id;
        this.code = code;
    }

    public TypeQualityCertificate() {
    }

    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
