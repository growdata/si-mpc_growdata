<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css/morris.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>

    </head>
    <body>
        <h1>Indicadores</h1>
        <h2>
            Seleccione uno de los indicadores predefinidos. 
        </h2><br/>
        <form method="get" action="/masterdata/generarResultados.htm" id="creationIndicator" > 
            <c:if test="${not empty indicatorSourceList}">    
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Indicadores</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nombreindicador" class=" control-label">Nombre Indicador</label>
                                    <select class="form-control" id="indicatorSel" name="indicatorSel" onchange="onSelectIndicator(), listar();"  >
                                        <option value="0">Seleccione...</option>
                                        <c:forEach var="item" items="${indicatorList}">
                                            <option <c:if test="${item.id eq indicatorSelected}">selected="selected"</c:if> value="${item.id}">${item.id} - ${item.name}</option>
                                        </c:forEach>
                                    </select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        <div id="configuracion">
                            <div class="row">
                                <div class="col-sm-6" >
                                    <div class="form-group">
                                        <label for="indicador" class=" control-label">Fuente de datos</label>
                                        <select class="form-control disabled" id="indicatorSourceSel" name="indicatorSourceSel"  >
                                            <option value="0">Seleccione...</option>
                                            <c:forEach var="item" items="${indicatorSourceList}">
                                                <option value="${item.id}" >${item.name}</option>
                                            </c:forEach>
                                        </select>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6" >
                                    <div class="form-group">
                                        <label for="indicador" class=" control-label">Tipo de gr�fica</label>
                                        <select class="form-control disabled" id="graphicTypeSel" name="graphicTypeSel"  >
                                            <option value="0">Seleccione...</option>
                                            <option value="L">Linea</option>
                                            <option value="B">Barras</option>
                                            <option value="D">C�rculo</option>
                                        </select>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                            </div>

                            <div class="panel panel-default" >
                                <div class="panel-heading"><h4>Seleccione Filtros</h4></div>
                                <div id="alert-area"></div>
                                <div style="margin-left: 231px; margin-top: 25px;">
                                    <label for="indicador" class=" control-label" id="serieVariable" name="serieVariable" ></label>
                                </div>
                                <div id="alert-area" ></div>
                                <div class="panel-body">
                                    <div class="col-sm-8">
                                        <div class="col-sm-6" align="center">
                                        <label class="control-label">Periodo inicial</label>
                                        <div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="year" class="control-label">A�o</label>
                                                    <select class="form-control"  id="anio" name="anio" required="true">
                                                        <option label="A�O"  value=""></option>
                                                        <c:if test="${not empty anios}">
                                                            <c:forEach var="item" items="${anios}">
                                                                <option <c:if test="${item eq yearSelected}">selected="selected"</c:if>  value="${item}" >${item}</option>
                                                            </c:forEach>
                                                        </c:if>
                                                    </select>
                                                </div>
                                            </div> 
                                            <div class="col-sm-6 ">
                                                <div class="form-group">
                                                    <label for="monthtab2" class="control-label">Mes</label>
                                                    <select class="form-control" id="mes" name="mes"  required="true" >
                                                        <option label="MES" value=""></option>
                                                        <option <c:if test="${monthSelected == '01'}">selected="selected"</c:if> value="01">Enero</option>
                                                        <option <c:if test="${monthSelected == '02'}">selected="selected"</c:if> value="02">Febrero</option>
                                                        <option <c:if test="${monthSelected == '03'}">selected="selected"</c:if> value="03">Marzo</option>
                                                        <option <c:if test="${monthSelected == '04'}">selected="selected"</c:if> value="04">Abril</option>
                                                        <option <c:if test="${monthSelected == '05'}">selected="selected"</c:if> value="05">Mayo</option>
                                                        <option <c:if test="${monthSelected == '06'}">selected="selected"</c:if> value="06">Junio</option>
                                                        <option <c:if test="${monthSelected == '07'}">selected="selected"</c:if> value="07">Julio</option>
                                                        <option <c:if test="${monthSelected == '08'}">selected="selected"</c:if> value="08">Agosto</option>
                                                        <option <c:if test="${monthSelected == '09'}">selected="selected"</c:if> value="09">Septiembre</option>
                                                        <option <c:if test="${monthSelected == '10'}">selected="selected"</c:if> value="10">Octubre</option>
                                                        <option <c:if test="${monthSelected == '11'}">selected="selected"</c:if> value="11">Noviembre</option>
                                                        <option <c:if test="${monthSelected == '12'}">selected="selected"</c:if> value="12">Diciembre</option>
                                                    </select>
                                                </div>
                                            </div>
                                            </div>
                                            </div>
                                        <div class="col-sm-6" align="center">
                                        <label class="control-label">Periodo final</label>        
                                            <div>
                                                <div class="col-sm-6 ">
                                                    <div class="form-group">
                                                        <label for="year" class="control-label">A�o</label>
                                                        <select class="form-control"  id="anio2" name="anio2" required="true">
                                                            <option label="A�O"  value=""></option>
                                                        <c:if test="${not empty anios}">
                                                            <c:forEach var="item" items="${anios}">
                                                                <option <c:if test="${item eq yearSelected2}">selected="selected"</c:if>  value="${item}" >${item}</option>
                                                            </c:forEach>
                                                        </c:if>
                                                    </select>
                                                </div>
                                            </div> 
                                            <div class="col-sm-6 ">
                                                <div class="form-group">
                                                    <label for="monthtab2" class="control-label">Mes</label>
                                                    <select class="form-control" id="mes2" name="mes2"  required="true" >
                                                        <option label="MES" value=""></option>
                                                        <option <c:if test="${monthSelected2 == '01'}">selected="selected"</c:if> value="01">Enero</option>
                                                        <option <c:if test="${monthSelected2 == '02'}">selected="selected"</c:if> value="02">Febrero</option>
                                                        <option <c:if test="${monthSelected2 == '03'}">selected="selected"</c:if> value="03">Marzo</option>
                                                        <option <c:if test="${monthSelected2 == '04'}">selected="selected"</c:if> value="04">Abril</option>
                                                        <option <c:if test="${monthSelected2 == '05'}">selected="selected"</c:if> value="05">Mayo</option>
                                                        <option <c:if test="${monthSelected2 == '06'}">selected="selected"</c:if> value="06">Junio</option>
                                                        <option <c:if test="${monthSelected2 == '07'}">selected="selected"</c:if> value="07">Julio</option>
                                                        <option <c:if test="${monthSelected2 == '08'}">selected="selected"</c:if> value="08">Agosto</option>
                                                        <option <c:if test="${monthSelected2 == '09'}">selected="selected"</c:if> value="09">Septiembre</option>
                                                        <option <c:if test="${monthSelected2 == '10'}">selected="selected"</c:if> value="10">Octubre</option>
                                                        <option <c:if test="${monthSelected2 == '11'}">selected="selected"</c:if> value="11">Noviembre</option>
                                                        <option <c:if test="${monthSelected2 == '12'}">selected="selected"</c:if> value="12">Diciembre</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                                        </div>
                                        </div>

                                        <div class="col-sm-3" >
                                            <label for="indicador" class=" control-label" id="xVariableSel" name="xVariableSel" ></label>

                                            <select id="serieVariableLista" name="serieVariableLista" class="form-control" >
                                                <option value="">Todas</option>
                                            <c:forEach var="item" items="${variableList}">
                                                <%--<c:if test="${item.isCCF == 'V'}">--%>
                                                    <!--<option value="${item.code}">${item.shortName}</option>-->
                                                <%--</c:if>--%>
                                                <option value="${item.id}">${item.descripcion}</option>
                                            </c:forEach>
                                        </select>    
                                        <br/><br/><br/> 
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row"> 
                                <input type="hidden" id="tiposerie" name="tiposerie"  />
                                <input type="hidden" id="tipox" name="tipox"  />
                                <div class="col-sm-offset-4 col-sm-8" style="text-align:right">
                                    <input onclick="verificarForm()" type="button" value="Generar" class="btn btn-success">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </c:if>
        </form>    
        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/dataTables.buttons.min.js"></script>
        <script src="resources/js/buttons.flash.min.js"></script>
        <script src="resources/js/jszip.min.js"></script>
        <script src="resources/js/pdfmake.min.js"></script>
        <script src="resources/js/vfs_fonts.js"></script>
        <script src="resources/js/buttons.html5.min.js"></script>
        <script src="resources/js/buttons.print.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/raphael.min.js"></script>
        <script src="resources/js/morris.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/xepOnline.js"></script>
        <script>
                                        $(document).ready(function () {
                                            $(".disabled").attr("disabled", "true");
                                            $("#configuracion").hide();
                                            onSelectIndicator();


//                                            $.fn.datepicker.dates['es'] = {
//                                                days: ["Domingo", "Lunes", "Martes", "Mi�rcoles", "Jueves", "Viernes", "S�bado", "Domingo"],
//                                                daysShort: ["Dom", "Lun", "Mar", "Mi�", "Jue", "Vie", "S�b", "Dom"],
//                                                daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
//                                                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
//                                                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
//                                                today: "Hoy"
//                                            };
//
//                                            $('#xVariableFechaInicio').datepicker({language: "es", autoclose: true, format: 'dd/mm/yy'});
//                                            $('#xVariableFechaFin').datepicker({language: "es", autoclose: true, format: 'dd/mm/yy'});



                                        });

                                        function deleteOptions(selectId) {
                                            $(selectId)
                                                    .find('option')
                                                    .remove()
                                                    .end()
                                                    .append('<option value="0">Seleccione... </option>')
                                                    .val('0')
                                                    ;
                                        }

                                       

                                        function listar() {
                                            document.forms["creationIndicator"].action = 'listas.htm';
                                            document.forms["creationIndicator"].submit();

                                            onSelectIndicator();
                                        }


                                        function onSelectIndicator() {

                                            var indicatorSel = $("#indicatorSel").val();


                                            if ($("#indicatorSel").val() !== "0") {


                                                $("#configuracion").show();

            <c:forEach var="indicator" items="${indicatorList}">
                                                if (indicatorSel === "<c:out value="${indicator.id}"/>") {
                                                    $("#indicatorSourceSel").val("<c:out value="${indicator.indicatorSource.id}"/>");
                                                    $("#graphicTypeSel").val("<c:out value="${indicator.graphicType}"/>");
                                                    $("#xVariableSel").text("<c:out value="${indicator.serieVar.description}"/>");
                                                    $("#serieVariable").text("<c:out value="${indicator.xVar.description}"/>");
                                                    $("#tiposerie").val("<c:out value="${indicator.xVar.type}"/>");
                                                    $("#tipox").val("<c:out value="${indicator.serieVar.type}"/>");


                <c:if test="${indicator.graphicType eq 'L'}">

                                                    $("#xVariableSel").text("<c:out value="${indicator.serieVar.description}"/>");
                                                    $("#serieVariable").text("<c:out value="${indicator.xVar.description}"/>");

                </c:if>
                <c:if test="${indicator.graphicType eq 'B'}">

                                                    $("#xVariableSel").text("<c:out value="${indicator.xVar.description}"/>");
                                                    $("#serieVariable").text("<c:out value="${indicator.serieVar.description}"/>");

                </c:if>

                                                }
            </c:forEach>
                                            }
                                        }

            function verificarForm(){
                
                if($('#anio').val() > $('#anio2').val()){
                        $("#alert-area").append($("<div class='alert alert-message alert-danger" +
                            " fade in' data-alert><font size=3>El periodo inicial no puede ser mayor que el periodo final</font></div>"));
                        $(".alert-message").delay(3000).fadeOut("slow", function () {
                            $(this).remove();
                        });
                } else if ($('#anio').val() === "" || $('#anio2').val() ==="") {
                        $("#alert-area").append($("<div class='alert alert-message alert-danger" +
                            " fade in' data-alert><font size=3>Ingrese alg�n periodo</font></div>"));
                        $(".alert-message").delay(3000).fadeOut("slow", function () {
                            $(this).remove();
                        });
                } else if ($('#anio').val() === $('#anio2').val() && $('#mes').val() > $('#mes2').val()) {
                        $("#alert-area").append($("<div class='alert alert-message alert-danger" +
                            " fade in' data-alert><font size=3>Si el periodo se encuentra en el mismo a�o, el mes del periodo inicial debe ser menor al mes del periodo final</font></div>"));
                        $(".alert-message").delay(3000).fadeOut("slow", function () {
                            $(this).remove();
                        });
                } else if (($('#anio').val() !== "" && $('#anio2').val() !== "") && $('#mes').val() === "" || ($('#anio').val() !== "" && $('#anio2').val() !== "") && $('#mes2').val() === "") {
                        $("#alert-area").append($("<div class='alert alert-message alert-danger" +
                            " fade in' data-alert><font size=3>Ingrese ambos meses de manera correcta</font></div>"));
                        $(".alert-message").delay(3000).fadeOut("slow", function () {
                            $(this).remove();
                        });
                }
                else{
                    $('#creationIndicator').submit();
                }

            }
        </script>
        <style>
            .buttons-csv{
                display: inline-block;
                background-image:url(resources/images/csv.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-csv span{
                opacity: 0;
            }
            .buttons-excel{
                display: inline-block;
                background-image:url(resources/images/excel.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-excel span{
                opacity: 0;
            }
            .buttons-pdf{
                display: inline-block;
                background-image:url(resources/images/pdf.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-pdf span{
                opacity: 0;
            }
            .buttons-print{
                display: inline-block;
                background-image:url(resources/images/print.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-print span{
                opacity: 0;
            }

            div.dt-buttons {
                float: right;
                margin-left:10px;
            }
        </style>
    </body>
</html>
