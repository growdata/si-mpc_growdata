
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserController.java
 * Created on: 2016/10/19, 02:14:58 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gml.simpc.api.dto.ConsolidantPostulantDto;
import com.gml.simpc.api.entity.IndicatorGraphic;
import com.gml.simpc.api.entity.valuelist.Foundation;
import com.gml.simpc.api.enums.GraphicType;
import com.gml.simpc.api.services.EconomicBenefitsService;
import com.gml.simpc.api.utilities.Util;
import com.gml.simpc.web.controller.ControllerDefinition;
import com.gml.simpc.web.utilities.ValueListService;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_001_UNKNOW;
import static com.gml.simpc.api.utilities.Util.FAILED_ALERT;

/**
 * Controlador para la ventana de manejo de portal de beneficios economicos
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Controller
public class EconomicBenefitsPortalController implements ControllerDefinition {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER
            = Logger.getLogger(EconomicBenefitsPortalController.class);

    /**
     * Servicio de listas de valor.
     */
    @Autowired
    ValueListService valueListService;

    /**
     * Anio 2014, se usa para los datos mostrados en los filtros de la pagina.
     */
    public static final int YEAR2014 = 2014;

    /**
     * Servicio de Beneficios economicos.
     */
    @Autowired
    private EconomicBenefitsService economicBenefitsService;

    /**
     * M&eacute;todo para pintar portal de beneficios economicos.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "portal-prestaciones-economicas",
            method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
            HttpServletResponse response) {
        LOGGER.info(
                "Ejecutando metodo [ onDisplay portal-prestaciones-economicas ]");
        ModelAndView modelAndView
                = new ModelAndView("portal-prestaciones-economicas");
        try {
            modelAndView.addObject("foundations", valueListService.
                    getFoundationList());
            modelAndView.addObject("years", listaAnos());
            modelAndView.addObject("months", Util.MONTHS_NAMES);
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("Finaliza la invocacion de [ onDisplay ].");
        }

        return modelAndView;
    }

    /**
     * M&eacute;todo para generar las prestaciones econ&oacute;micas.
     * 
     * @param request
     * @param response
     * @param idCCf
     * @param month
     * @param year
     * @return 
     */
    @RequestMapping(value = "buscarPrestacionesEconomicas",
            method = RequestMethod.GET)
    public ModelAndView search(HttpServletRequest request,
            HttpServletResponse response, @RequestParam(name = "ccf") String idCCf,
            @RequestParam(name = "mes") Integer month,
            @RequestParam(name = "ano") Integer year) {
        LOGGER.info(
                "entra a search en el controller de EconomicBenefitsPortalCroller");

        ModelAndView modelAndView
                = new ModelAndView("portal-prestaciones-economicas");

        try {
            int tipoConsulta
                    = Integer.parseInt(request.getParameter("tipoConsulta"));
            List<ConsolidantPostulantDto> listaConsolidados = null;

            String sMonth = month == null ? null : Integer.toString(month);
            String sYear = year == null ? null : Integer.toString(year);

            switch (tipoConsulta) {
                case 1:
                    listaConsolidados = consolidatedPostulants(idCCf, sYear,
                            sMonth, modelAndView);
                    break;
                case 2:
                    listaConsolidados = detailedPostulants(idCCf, sYear, sMonth,
                            modelAndView);
                    break;
                case 3:
                    listaConsolidados = consolidatedBenefits(idCCf, sYear,
                            sMonth, modelAndView);
                    break;
                case 4:
                    listaConsolidados = detailedBenefits(idCCf, sYear, sMonth,
                            modelAndView);
                    break;
                default:
                    break;
            }

            modelAndView.addObject("resultList" + tipoConsulta,
                    listaConsolidados);

            modelAndView.addObject("tipoConsulta", tipoConsulta);
            IndicatorGraphic graphic = new IndicatorGraphic();
            if (tipoConsulta == 1 || tipoConsulta == 3) {
                graphic.setType(GraphicType.L);
            } else if (tipoConsulta == 2 || tipoConsulta == 4) {
                graphic.setType(GraphicType.B);
            }

            modelAndView.addObject("years", listaAnos());
            modelAndView.addObject("months", Util.MONTHS_NAMES);
            modelAndView.addObject("ccfSelected", idCCf);
            modelAndView.addObject("yearSelected", year);
            modelAndView.addObject("monthsSelected", month);
            modelAndView.addObject("activateBtn", true);
            modelAndView.addObject("tipoConsulta", tipoConsulta);

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("Finaliza la invocacion de [ buscarPerfiles ].");
        }

        init(modelAndView);

        return modelAndView;
    }

    /**
     * Muestra el gr&aacute;fico segun el filtro de la tabla # 1.
     * 
     * @param modelAndView
     * @param listaConsolidados
     * @param idCcf
     * @param year 
     */
    private void graphicConsolidatedpostulant(ModelAndView modelAndView,
            List<ConsolidantPostulantDto> listaConsolidados, String idCcf,
            String year) {
        IndicatorGraphic graphic = new IndicatorGraphic();
        graphic.setType(GraphicType.L);
        modelAndView.addObject("graphic", graphic);
        ObjectMapper mapper = new ObjectMapper();
        try {
            String xKey = "month";
            List<String> yKeys = new ArrayList();
            List<String> labels = new ArrayList();
            List<String> tableg = new ArrayList();
            List<String> lineColors = new ArrayList();
            yKeys.add("postulants");
            yKeys.add("approvedApplications");
            yKeys.add("requestsDenied");
            labels.add("Postulantes al subsidio");
            labels.add("Solicitudes aprobadas");
            labels.add("Solicitudes Denegadas");

            tableg.add("MES");
            tableg.add("POSTULANTE AL SUBSIDIO");
            tableg.add("SOLICITUDES APROBADAS");
            tableg.add("SOLICITUDES DENEGADAS");
            lineColors.add("blue");
            lineColors.add("red");
            lineColors.add("green");

            modelAndView.addObject("graphicData",
                    mapper.writeValueAsString(listaConsolidados));
            modelAndView.addObject("graphicList", listaConsolidados);
            modelAndView.addObject("xKey", xKey);
            modelAndView.addObject("yKeys", yKeys);
            modelAndView.addObject("labels", labels);
            modelAndView.addObject("titleList", tableg);
            modelAndView.addObject("lineColors", lineColors);
            modelAndView.addObject("titulografica", "CANTIDAD DE SOLICITUDES");

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msgType", FAILED_ALERT);

        } finally {
            LOGGER.info("Finaliza realiacion de  graficos");
        }
        if (idCcf != null) {
            if ("".equals(idCcf)) {
                modelAndView.addObject("titulo",
                        "POSTULANTES AL SUBSIDIO FOSFEC " + " " + year);
            } else {
                Map ccfs = valueListService.getFoundations();
                Foundation ccf = (Foundation) ccfs.get(idCcf);
                modelAndView.addObject("titulo",
                        "POSTULANTES AL SUBSIDIO FOSFEC " + ccf.getShortName() + " "
                        + year);
            }
        } else {
            modelAndView.addObject("titulo",
                    "POSTULANTES AL SUBSIDIO FOSFEC " + " " + year);
        }

    }

    /**
     * Muestra el gr&aacute;fico segun el filtro de la tabla # 2.
     * 
     * @param modelAndView
     * @param listaConsolidados
     * @param year
     * @param month 
     */
    private void graphicDetailpostulant(ModelAndView modelAndView,
            List<ConsolidantPostulantDto> listaConsolidados, String year,
            String month) {
        IndicatorGraphic graphic = new IndicatorGraphic();
        graphic.setType(GraphicType.B);
        modelAndView.addObject("graphic", graphic);
        ObjectMapper mapper = new ObjectMapper();

        try {
            String xKey = "month";
            List<String> yKeys = new ArrayList();
            List<String> labels = new ArrayList();
            List<String> tableg = new ArrayList();
            List<String> barColors = new ArrayList();

            yKeys.add("postulants");
            labels.add("Postulantes al subsidio");
            barColors.add("blue");

            tableg.add("CAJAS DE COMPENSACION");
            tableg.add("N�MERO DE SOLICITUDES");

            modelAndView.addObject("graphicData",
                    mapper.writeValueAsString(listaConsolidados));
            modelAndView.addObject("graphicList2", listaConsolidados);
            modelAndView.addObject("xKey", xKey);
            modelAndView.addObject("yKeys", yKeys);
            modelAndView.addObject("labels", labels);
            modelAndView.addObject("titleList", tableg);
            modelAndView.addObject("barColors", barColors);
            modelAndView.addObject("titulografica", "CANTIDAD DE SOLICITUDES");

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msgType", FAILED_ALERT);

        } finally {
            LOGGER.info("Finaliza realiacion de  graficos");
        }

        modelAndView.addObject("titulo",
                "POSTULANTES AL SUBSIDIO FOSFEC " + year + " - " +  month);
    }

    /**
     * Muestra el gr&aacute;fico segun el filtro de la tabla # 3.
     * 
     * @param modelAndView
     * @param listaConsolidados
     * @param idCcf
     * @param year 
     */
    private void graphicConsolidatedBeneficiaries(ModelAndView modelAndView,
            List<ConsolidantPostulantDto> listaConsolidados, String idCcf,
            String year) {
        IndicatorGraphic graphic = new IndicatorGraphic();
        graphic.setType(GraphicType.L);
        modelAndView.addObject("graphic", graphic);
        ObjectMapper mapper = new ObjectMapper();

        try {
            String xKey = "month";
            List<String> yKeys = new ArrayList();
            List<String> labels = new ArrayList();
            List<String> tableg = new ArrayList();
            List<String> lineColors = new ArrayList();

            yKeys.add("postulants");
            labels.add("Beneficiarios al subsidio");
            lineColors.add("blue");

            tableg.add("MES");
            tableg.add("N�MERO DE SOLICITUDES");

            modelAndView.addObject("graphicData",
                    mapper.writeValueAsString(listaConsolidados));
            modelAndView.addObject("graphicList3", listaConsolidados);
            modelAndView.addObject("xKey", xKey);
            modelAndView.addObject("yKeys", yKeys);
            modelAndView.addObject("labels", labels);
            modelAndView.addObject("titleList", tableg);
            modelAndView.addObject("lineColors", lineColors);
            modelAndView.addObject("titulografica", "CANTIDAD DE SOLICITUDES");

            if (idCcf != null) {
                Map ccfs = valueListService.getFoundations();
                Foundation ccf = (Foundation) ccfs.get(idCcf);
                modelAndView.addObject("titulo",
                        "BENEFICIARIOS SUBSIDIO FOSFEC " + ccf.getShortName() + " "
                        + year);
            } else {
                modelAndView.addObject("titulo",
                        "BENEFICIARIOS SUBSIDIO FOSFEC " + " " + year);
            }
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msgType", FAILED_ALERT);

        } finally {
            LOGGER.info("Finaliza realiacion de  graficos");
        }
    }

    /**
     * Muestra el gr&aacute;fico segun el filtro de la tabla # 4.
     * 
     * @param modelAndView
     * @param listaConsolidados
     * @param year
     * @param month 
     */
    private void graphicDetailBeneficiaries(ModelAndView modelAndView,
            List<ConsolidantPostulantDto> listaConsolidados, String year,
            String month) {
        IndicatorGraphic graphic = new IndicatorGraphic();
        graphic.setType(GraphicType.B);
        modelAndView.addObject("graphic", graphic);
        ObjectMapper mapper = new ObjectMapper();

        try {
            String xKey = "month";
            List<String> yKeys = new ArrayList();
            List<String> labels = new ArrayList();
            List<String> tableg = new ArrayList();
            List<String> barColors = new ArrayList();

            yKeys.add("postulants");
            labels.add("Beneficiarios al subsidio");
            barColors.add("blue");

            tableg.add("CAJAS DE COMPENSACION");
            tableg.add("N�MERO DE SOLICITUDES");

            modelAndView.addObject("graphicData",
                    mapper.writeValueAsString(listaConsolidados));
            modelAndView.addObject("graphicList4", listaConsolidados);
            modelAndView.addObject("xKey", xKey);
            modelAndView.addObject("yKeys", yKeys);
            modelAndView.addObject("labels", labels);
            modelAndView.addObject("titleList", tableg);
            modelAndView.addObject("barColors", barColors);
            modelAndView.addObject("titulografica", "CANTIDAD DE SOLICITUDES");

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msgType", FAILED_ALERT);

        } finally {
            LOGGER.info("Finaliza realiacion de  graficos");
        }

        modelAndView.addObject("titulo",
                "BENEFICIARIOS AL SUBSIDIO FOSFEC " + year + month);
    }

    /**
     * Inicia los objetos para el controler.
     *
     * @param modelAndView
     */
    private void init(ModelAndView modelAndView) {
        modelAndView.addObject("years", listaAnos());
        modelAndView.addObject("months", Util.MONTHS_NAMES);
        modelAndView.addObject("foundations", valueListService.
                getFoundationList());

    }

    /**
     * Genera la lista de anios.
     * 
     * @return 
     */
    private List<String> listaAnos() {
        List<String> years = new ArrayList<>();
        int yearActual = Calendar.getInstance().get(Calendar.YEAR);
        for (int a = YEAR2014; a <= yearActual; a++) {
            years.add(Integer.toString(a));
        }
        return years;
    }

    /**
     * Genera la consulta detallada de beneficiarios.
     * 
     * @param sIdCcf
     * @param sYear
     * @param sMonth
     * @param modelAndView
     * @return 
     */
    private List<ConsolidantPostulantDto> detailedBenefits(String sIdCcf,
            String sYear, String sMonth, ModelAndView modelAndView) {
        List<ConsolidantPostulantDto> listaConsolidados
                = this.economicBenefitsService.getDetailedBenefits(sIdCcf, sYear,
                        sMonth);

        graphicDetailBeneficiaries(modelAndView, listaConsolidados,
                sYear == null ? "" : sYear, sMonth == null ? "" : sMonth);

        return listaConsolidados;
    }

    /**
     * Genera los consulta consolidada de beneficiarios.
     * 
     * @param sIdCcf
     * @param sYear
     * @param sMonth
     * @param modelAndView
     * @return 
     */
    private List<ConsolidantPostulantDto> consolidatedBenefits(String sIdCcf,
            String sYear, String sMonth, ModelAndView modelAndView) {
        List<ConsolidantPostulantDto> listaConsolidados
                = this.economicBenefitsService.getConsolidatedBenefits(sIdCcf, sYear,
                        sMonth);

        graphicConsolidatedBeneficiaries(modelAndView,
                listaConsolidados, sIdCcf, sYear == null ? "" : sYear);

        return listaConsolidados;
    }

    /**
     * Genera la consulta detallada de postulantes.
     * 
     * @param sIdCcf
     * @param sYear
     * @param sMonth
     * @param modelAndView
     * @return 
     */
    private List<ConsolidantPostulantDto> detailedPostulants(String sIdCcf,
            String sYear, String sMonth, ModelAndView modelAndView) {
        List<ConsolidantPostulantDto> listaConsolidados
                = this.economicBenefitsService.getDetailedPostulants(sIdCcf, sYear,
                        sMonth);

        graphicDetailpostulant(modelAndView, listaConsolidados,
                sYear == null ? "" : sYear, sMonth == null ? "" : sMonth);

        return listaConsolidados;
    }

    /**
     * Genera los consulta consolidada de postulantes.
     * 
     * @param sIdCcf
     * @param sYear
     * @param sMonth
     * @param modelAndView
     * @return 
     */
    private List<ConsolidantPostulantDto> consolidatedPostulants(String sIdCcf,
            String sYear, String sMonth, ModelAndView modelAndView) {
        List<ConsolidantPostulantDto> listaConsolidados
                = this.economicBenefitsService.getConsolidatedPostulants(sIdCcf,
                        sYear, sMonth);

        graphicConsolidatedpostulant(modelAndView, listaConsolidados,
                sIdCcf, sYear == null ? "" : sYear);

        return listaConsolidados;
    }
}
