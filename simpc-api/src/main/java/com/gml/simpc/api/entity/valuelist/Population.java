/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: Population.java
 * Created on: 2017/02/22, 11:53:59 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity.valuelist;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
@Table(name = "POBLACION")
@Entity
public class Population implements Serializable {
 /**
     * ID del tipo de instituci&oacute;n.
     */
    @Id
    @Column(name = "ID", length = 10)
    private Long id;
    /**
     * nombre del tupo de instituci&oacute;n.
     */
    @Column(name = "NOMBRE", length = 50)
    private String name;

    /**
     * descripci&oacute;n
     */
    @Column(name = "CODIGO", length = 200)
    private String code;
    

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }
}
