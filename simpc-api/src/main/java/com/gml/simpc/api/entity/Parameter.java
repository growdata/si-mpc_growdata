/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Parameter.java
 * Created on: 2016/10/30, 11:50:06 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad para el manejo de par&aacute;metros.
 *
 * @author <a href="mailto:jonathanp@gmlsoftware.com">Jonathan Ponton</a>
 */
@Table(name = "PARAMETROS")
@Entity
public class Parameter implements Serializable {

    /**
     * ID del parametro.
     */
    @Id
    @Column(name = "ID")
    private Long id;
    /**
     * Nombre del parametro.
     */
    @Column(name = "NOMBRE", nullable = false, unique = true)
    private String name;
    /**
     * Nombre del parametro en espa�ol.
     */
    @Column(name = "NOMBRE_ESP", nullable = false, unique = true)
    private String spName;
    /**
     * Descripci&oacute;n del parametro.
     */
    @Column(name = "DESCRIPCION")
    private String description;
    /**
     * Valor del parametro.
     */
    @Column(name = "VALOR", nullable = false)
    private String value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getSpName() {
        return spName;
    }

    public void setSpName(String spName) {
        this.spName = spName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Parametro{" + "id=" + id + ", Nombre=" + name + 
            ", Descripci�n=" +description + ", Valor=" + value + '}';
    }
    
}
