/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: OtherKnowledgeDtoRowMapper.java
 * Created on: 2017/02/13, 11:56:54 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.dao.mapper;

import com.gml.simpc.api.dto.OtherKnowledgeDto;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 * Row Mapper para otros conocimientos.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class OtherKnowledgeDtoRowMapper implements RowMapper<OtherKnowledgeDto> {

    @Override
    public OtherKnowledgeDto mapRow(ResultSet rs, int i) throws SQLException {
        OtherKnowledgeDto dto = new OtherKnowledgeDto();
        dto.setDocumentType(rs.getString("TIPO_DOCUMENTO"));
        dto.setNumberIdentification(rs.getString("NUMERO_DOCUMENTO"));
        dto.setType(rs.getString("TIPO"));
        dto.setTool(rs.getString("HERRAMIENTA"));
        dto.setLevel(rs.getString("NIVEL"));
        dto.setCargaId(rs.getLong("CARGA_ID"));
        
        return dto;
    }
}
