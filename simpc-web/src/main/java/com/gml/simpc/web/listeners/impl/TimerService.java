/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: TimerService.java
 * Created on: 2016/11/21, 04:12:49 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.listeners.impl;

import com.gml.simpc.api.dao.IndividualIntersectionDao;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.enums.LinkReason;
import com.gml.simpc.api.enums.UserStatus;
import com.gml.simpc.api.services.ActivationService;
import com.gml.simpc.api.services.DesktopSessionService;
import com.gml.simpc.api.services.SessionService;
import com.gml.simpc.api.services.UserService;
import com.gml.simpc.commons.mail.sender.Notificator;
import com.gml.simpc.web.utilities.ProgramUpdateService;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Componente con timers que ejecutan procesos.
 *
 * @author <a href="mailto:julianm@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
@Service
public class TimerService {

    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(TimerService.class);
    /**
     * Mensaje de error si el proceso falla.
     */
    public static final String ERROR_MESSAGE = "Se presento un error al " +
        "confirmar los programas a el sistema, Por favor revise en la tabla " +
        "MD_F_CAP_PROGRAMA_MODULO los posibles errores en los registros con " +
        "el valor 2 en la columna EN_DOMINIO. Al finalizar la corrección " +
        "cambie la columna EN_DOMINIO a 0 para que los registros sean " +
        "tomados por el sistema.";
    /**
     * Servicio para adminisraci&oacute;n de activaciones.
     */
    @Autowired
    private ActivationService activationService;
    /**
     * Servicio de sesiones.
     */
    @Autowired
    private SessionService sessionService;
    /**
     * Servicio de usuarios.
     */
    @Autowired
    private UserService userService;
    /**
     * Servicio de usuarios.
     */
    @Autowired
    private DesktopSessionService desktopSessionService;
    /**
     * Servicio para confirmaci&oacute;n de programas.
     */
    @Autowired
    private ProgramUpdateService programUpdateService;
    /**
     * para el archivo de correo.
     */
    @Autowired
    private Notificator notificator;
    /**
     * Dao para acceso de informaci&oacute;n en la Master Data.
     */
    @Autowired
    private IndividualIntersectionDao individualIntersectionDao;

    /**
     * M&ecute;todo asincrono para eliminar token de activaci&ocute;n de usuario
     * y notificar al administrador en caso de ser necesario.
     */
    @Scheduled(fixedRate = 180000)
    public void expirateToken() {
        LOGGER.info("Ejecutando metodo [ expirateToken ]");

        try {
            this.activationService.deleteExpiratedTokens();
        } catch (Exception e) {
            LOGGER.error("Se produjo un error:", e);
        } finally {
            LOGGER.info("Finaliza la expiracion de tokens.");
        }
    }

    /**
     * M&ecute;todo asincrono para eliminar token de activaci&ocute;n de usuario
     * y notificar al administrador en caso de ser necesario.
     * Se ejecuta cada tres minutos
     */
    @Scheduled(fixedRate = 180000)
    public void expirateSession() {
        LOGGER.info("Ejecutando metodo [ expirateSession ]");

        try {
            this.sessionService.expireSessions();
        } catch (Exception e) {
            LOGGER.error("Se produjo un error ", e);
        } finally {
            LOGGER.info("Finaliza la expiracion de sesiones.");
        }
    }

    /**
     * M&ecute;todo asincrono para inactivar usuarios con password vencido.
     * Se ejecuta todos los d&iacute;as a las 23:00
     */
    @Scheduled(cron = "*/10 * * * * ?")
    @Transactional
    public void expiratePassword() {
        LOGGER.info("Ejecutando metodo [ expiratePassword ]");

        try {
            List<User> usersToExpire = this.userService.findUsersToExpire();
            LOGGER.info("Inactivando usuarios: " + usersToExpire.size());
            for (User user : usersToExpire) {
                LOGGER.info("Inactivando usuario: " + user.getUsername());
                user.setStatus(UserStatus.I);

                this.userService.save(user);

                this.activationService.sendPasswordLink(user.getEmail(),
                    LinkReason.EXPIRATION);
            }
        } catch (Exception e) {
            LOGGER.error("Se produjo un error ", e);
        } finally {
            LOGGER.info("Finaliza la expiracion de passwords.");
        }
    }

    /**
     * M&ecute;todo asincrono para inactivar usuarios con password vencido.
     * Se ejecuta todos los d&iacute;as a las 23:05
     */
    @Scheduled(cron = "0 15 23 * * ?")
    public void expiratePasswordPrev() {
        LOGGER.info("Ejecutando metodo [ expiratePasswordPrev ]");

        try {
            List<User> usersToExpire = this.userService.findUsersToExpirePrev();
            LOGGER.info("Avisando usuarios: " + usersToExpire.size());
            for (User user : usersToExpire) {
                LOGGER.info("Avisando usuario: " + user.getUsername());
                this.activationService.sendPasswordLink(user.getEmail(),
                    LinkReason.PREV_EXPIRATION);
            }
        } catch (Exception e) {
            LOGGER.error("Se produjo un error ", e);
        } finally {
            LOGGER.info("Finaliza la expiracion de passwords previa.");
        }
    }

    /**
     * M&ecute;todo asincrono para eliminar token de sesi&oacute;n de
     * escritorio.
     */
    @Scheduled(fixedRate = 180000)
    public void expirateDesktopSessions() {
        LOGGER.info("Ejecutando metodo [ expirateDesktopSessions ]");

        try {
            this.desktopSessionService.deleteExpiratedTokens();
        } catch (Exception e) {
            LOGGER.error("Se produjo un error:", e);
        } finally {
            LOGGER.info("Finaliza la expiracion de tokens.");
        }
    }

    /**
     * Proceso para la confirmaci&oacute;n de programas.
     */
    @Scheduled(cron = "0 0 23 * * ?")
    public void initProgramCommitment() {
        try {
            this.programUpdateService.lockObjetiveResultSet();
            this.programUpdateService.moveProgramsFromMasterDataToDomain();
            LOGGER.info("PROGRAMAS CARGADOS EXITOSAMENTE");
        } catch (Exception ex) {
            LOGGER.error("Error", ex);

            notifyError();
        }
    }

    /**
     * Notifica si hubo error en el proceso.
     */
    private void notifyError() {
        LOGGER.info("Ejecutando metodo [ notifyError ]");

        try {
            this.programUpdateService.discardObjetiveResultSet();

            for (User usuario : this.userService.findByAdmin(true)) {
                this.notificator.sendNotification(usuario.getEmail(),
                    ERROR_MESSAGE, usuario.getFirstName().concat(" ").
                    concat(usuario.getFirstSurname()), "COMMIT_PROGRAMS");
            }
        } catch (Exception ex) {
            LOGGER.error("Error", ex);
        }
    }
}
