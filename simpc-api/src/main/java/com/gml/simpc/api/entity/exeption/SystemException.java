/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: SystemException.java
 * Created on: 2016/10/21, 02:41:40 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity.exeption;

import com.gml.simpc.api.entity.exeption.code.ErrorCode;

import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_001_UNKNOW;

/**
 * Excepci&oacute;n dise�ada para saltar entre capas y disparar rollback de
 * transacciones.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class SystemException extends RuntimeException {

    /**
     * C&oacute;digo de error.
     */
    private final ErrorCode errorCode;

    /**
     * Constructor por defecto.
     */
    public SystemException() {
        this.errorCode = ERR_001_UNKNOW;
    }

    /**
     * @param errorCode
     */
    public SystemException(ErrorCode errorCode) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
    }

    /**
     * @param errorCode
     * @param cause
     */
    public SystemException(ErrorCode errorCode, Throwable cause) {
        super(errorCode.getMessage(), cause);
        this.errorCode = errorCode;
    }

    /**
     * @param cause
     */
    public SystemException(Throwable cause) {
        super(cause);

        this.errorCode = ERR_001_UNKNOW;
    }

    /**
     * @param message
     */
    public SystemException(String message) {
        super(message);

        this.errorCode = ERR_001_UNKNOW;
    }
    
    /**
     * @param errorCode
     * @param params 
     */
    public SystemException(ErrorCode errorCode, Object ... params) {
        super(String.format(errorCode.getMessage(), params));
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
