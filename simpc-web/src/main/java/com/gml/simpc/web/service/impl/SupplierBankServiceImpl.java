/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: SupplierBankServiceImpl.java
 * Created on: 2017/01/12, 03:14:16 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.dao.SupplierDao;
import com.gml.simpc.api.dto.GraphicDataDto;
import com.gml.simpc.api.dto.TableDataDto;
import com.gml.simpc.api.dto.ValueListDto;
import com.gml.simpc.api.entity.Institution;
import com.gml.simpc.api.entity.valuelist.City;
import com.gml.simpc.api.entity.valuelist.InstitutionType;
import com.gml.simpc.api.entity.valuelist.OrientationType;
import com.gml.simpc.api.entity.valuelist.ProgramType;
import com.gml.simpc.api.entity.valuelist.State;
import com.gml.simpc.api.services.SupplierBankService;
import com.gml.simpc.web.utilities.ValueListService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
@Service
public class SupplierBankServiceImpl implements SupplierBankService {

    
    /**
     * Servicio para manipular listas de valor
     */
    @Autowired
    private ValueListService valueListService ;

    /**
     * Dao para manejar la persistencia .
     */
    @Autowired
    private SupplierDao supplierDao;

    
    /**
     * Obtiene lista de duraciones de programas
     *
     * @return
     */
    @Override
    public List<ValueListDto> getDuration() {
        return supplierDao.getDuration();
    }

    /**
     * Genera lista de instituciones segun el su tipo, ciudad y municipio.
     * 
     * @param type
     * @param city
     * @param state
     * @return 
     */
    @Override
    public List<Institution> getInstitutionsByTypeAndCityAndState(String type,
        String city, String state) {
        return supplierDao.getInstitutionsByTypeAndCityAndState(type, city,
            state);
    }
    
    /**
     * Genera los graficos de las instituciones segun el su tipo, ciudad y 
     * municipio.
     * 
     * @param type
     * @param city
     * @param state
     * @return 
     */
    @Override
    public List<GraphicDataDto> graphicInstitutionsByTypeAndCityAndState(String type,
        String city, String state) {
        return supplierDao.graphicInstitutionsByTypeAndCityAndState(type, city,
            state);
    }
    
    /**
     * Genera lista de sedes segun el su tipo, ciudad y municipio.
     * 
     * @param type
     * @param city
     * @param state
     * @return 
     */
    @Override
    public List<TableDataDto> getHeadquarterByTypeAndCityAndState(String type,
        String city, String state) {
        return supplierDao.getHeadquarterByTypeAndCityAndState(type, city, state);
    }
    
    /**
     * Genera los graficos de las sedes segun el su tipo, ciudad y 
     * municipio.
     * 
     * @param type
     * @param city
     * @param state
     * @return 
     */
     @Override
    public List<GraphicDataDto> graphicHeadQuartersByTypeAndCityAndState(
        String type, String city, String state){
    return supplierDao.graphicHeadquarterByTypeAndCityAndState(type, city, state);
                       
    }
    
    /**
     * Genera lista de programas m&ocute;dulos segun los filtros ingresados.
     * 
     * @param formationType
     * @param duration
     * @param module
     * @return 
     */
    @Override
    public List<TableDataDto> getProgramByTypeAndDurationAndModule(
        String formationType,String duration,String module) {
        return supplierDao.getProgramByTypeAndDurationAndModule(formationType, 
            duration, module);
    }
    
    /**
     * Devuelve la lista de tipos de institucion.
     * 
     * @return 
     */
    @Override
    public List<InstitutionType> getInstitutionTypes() {
        return this.valueListService.getInstitutionTypeList();
       
    }

    /**
     * Devuelve la lista de departamentos.
     * 
     * @return 
     */
    @Override
    public List<City> getCities() {
       return this.valueListService.getCityList();
    }

    /**
     * Devuelve la lista de municipios.
     * 
     * @return 
     */
    @Override
    public List<State> getStates() {
        return this.valueListService.getStateList();
    }

    /**
     * Devuelve la lista de tipos de formaci&ocute;n.
     * 
     * @return 
     */
    @Override
    public List<ProgramType> getFormationType() {
       return this.valueListService.getProgramTypeList();
    }

    /**
     * Devuelve la lista de tipos de orientaci&ocute;n.
     * 
     * @return 
     */
    @Override
    public List<OrientationType> getOrientationType() {
        return this.valueListService.getOrientationTypeList();
    }

}
