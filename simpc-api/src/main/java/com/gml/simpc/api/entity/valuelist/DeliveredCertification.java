/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: DeliveredCertification.java
 * Created on: 2017/28/06, 04:58:07 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity.valuelist;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author <a href="mailto:jonathanP@gmlsoftware.com">Jonathan Pont�n</a>
 */
@Table(name = "CERTIFICACIONES_EMITIDAS")
@Entity
public class DeliveredCertification implements Serializable {

    /**
     * Identificador de la certifiacion.
     */
    @Id
    @Column(name = "ID", length = 20)
    private Long id;
    /**
     * C&oacute;digo.
     */
    @Column(name = "CODIGO", length = 255)
        private String code;
    /**
     * Nombre corto.
     */
    @Column(name = "NOMBRE_CORTO", length = 255)
        private String shortName;


    public DeliveredCertification(long id, String code) {
        this.id = id;
        this.code = code;
    }

    public DeliveredCertification() {
    }

    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

}