--Creaci�n de vista para cajas de compensacion
create or replace view vw_cajas_compensacion as
    select codigo id,nombre_corto descripcion
    from fundaciones 
    where es_ccf = 'V'
;   

--Creaci�n de vista para tipos de institucion
create or replace view vw_tipos_institucion as
    select 
    t.id,t.DESCRIPCION
    from MD_F_CAP_PROGRAMA_MODULO md, INSTITUCIONES i,TIPOS_INSTITUCION t
    where md.codigo_inst = i.id and t.id = i.TIPO_INSTITUCION_ID
    group by t.id,t.DESCRIPCION
;






--Creaci�n de vista para codigos cine
create or replace view vw_cine as
    select id,valor descripcion
    from cine
;  

--Creacio de vista para necesidad
create or replace view vw_necesidad as
select 
necesidad id,
case when necesidad = '1' then 'Necesidad de la empresa.'
 when necesidad = '2' then 'Necesidad del buscador.'
 when necesidad = '3' then 'Necesidad del sector productivo.'
 when necesidad = '4' then 'Ninguna de las anteriores'
end descripcion
from MD_F_CAP_PROGRAMA_MODULO
group by necesidad;

--Creaci�n de vista para estado de aprobacion en fosfec
create or replace view vw_estados_aprobacion as
    select 
    verificacion_requisitos id,
    case when verificacion_requisitos =1 then  'APROBADO'
    when  verificacion_requisitos =2 then 'DENEGADO'
    when verificacion_requisitos=3 then 'DESISTIDO'
    when  verificacion_requisitos =4 then 'CANCELADO'
    when  verificacion_requisitos =5 then 'EN PROCESO DE VERIFICACI�N'
    end descripcion
    from MD_F_FOSFEC group by verificacion_requisitos;
    
--Creaci�n de vista para estado de beneficio en fosfec
create or replace view vw_estados_beneficio as
    select 
    terminacion_beneficio id,
    case when terminacion_beneficio =1 then  'FINALIZADO'
    when  terminacion_beneficio =2 then 'NO FINALIZADO'
    end descripcion
    from MD_F_FOSFEC group by terminacion_beneficio;    

--Creaci�n de vista para estado de asistencia a capacitacion
create or replace view vw_estados_asistencia_cap as
select estado id,
case when estado = '1' then 'Finalizado'
 when estado = '2' then 'En curso'
 when estado = '3' then 'Desertado'
 when estado = '4' then 'Inscrito'
 end descripcion
 from MD_F_PROGRAMA_DETALLE
 group by estado;


--Borrado y creacion de tablas
DROP TABLE IND_VARIABLES;
DROP TABLE IND_FUENTES;
DROP TABLE IND_INDICADORES;

CREATE TABLE IND_FUENTES 
   (	
    ID NUMBER(19,0), 
    NOMBRE VARCHAR2(255), 
    TABLA VARCHAR2(255)
   );
ALTER TABLE IND_FUENTES ADD PRIMARY KEY (ID);

CREATE TABLE IND_VARIABLES 
   (	
    IND_FUENTE_ID NUMBER(19,0),
    ID NUMBER(19,0), 
    CAMPO VARCHAR2(255),
    DESCRIPCION VARCHAR2(255), 
    TIPO VARCHAR2(255),
    DETALLE VARCHAR2(255)
   );
ALTER TABLE IND_VARIABLES ADD PRIMARY KEY (ID);
ALTER TABLE IND_VARIABLES ADD CONSTRAINT FK_IND_VARIABLES 
    FOREIGN KEY (IND_FUENTE_ID) REFERENCES IND_FUENTES (ID); 

CREATE TABLE IND_INDICADORES 
   (	
    ID NUMBER(19,0), 
    NOMBRE VARCHAR2(255),
    DESCRIPCION VARCHAR2(255),
    IND_FUENTE_ID NUMBER(19,0),
    TIPO_GRAFICO VARCHAR2(50), 
    IND_VARIABLE_X_ID NUMBER(19,0),
    IND_VARIABLE_SERIE_ID NUMBER(19,0),
    IND_VARIABLE_Y_ID NUMBER(19,0),
    FUNCION VARCHAR2(50) 
   );
ALTER TABLE IND_INDICADORES ADD PRIMARY KEY (ID);

--Inserciones de fuentes y variables

insert into IND_FUENTES (Id,Nombre,Tabla) values (1,'Prestaciones Economicas','MD_F_FOSFEC');
insert into IND_FUENTES (Id,Nombre,Tabla) values (2,'Capacitaci�n','MD_F_CAP_PROGRAMA_MODULO');
insert into IND_FUENTES (Id,Nombre,Tabla) values (3,'Detalle de capacitaci�n','MD_VIEW_PORTAL_CAP');

COMMIT;