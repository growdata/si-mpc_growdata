/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: LoginController.java
 * Created on: 2016/10/21, 10:45:40 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gml.simpc.api.entity.HistoryAccess;
import com.gml.simpc.api.entity.Session;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.entity.exeption.code.ErrorCode;
import com.gml.simpc.api.enums.LinkReason;
import com.gml.simpc.api.enums.ProfileStatus;
import com.gml.simpc.api.services.ActivationService;
import com.gml.simpc.api.services.HistoryAccessService;
import com.gml.simpc.api.services.SessionService;
import com.gml.simpc.api.services.UserService;
import com.gml.simpc.commons.captcha.VerifyRecaptcha;
import com.gml.simpc.web.controller.ControllerDefinition;
import com.gml.simpc.web.utilities.LDAPOperations;
import java.io.IOException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import static com.gml.simpc.api.utilities.Util.FAILED_ALERT;
import static com.gml.simpc.api.utilities.Util.PASSWORD_RECOVERY_SUCCESFUL;
import static com.gml.simpc.api.utilities.Util.SESSION_USER;
import static com.gml.simpc.api.utilities.Util.SUCCESS_ALERT;

/**
 * Controlador para el acceso a la plataforma
 *
 * @author <a href="mailto:julianm@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
@Controller
public class LoginController implements ControllerDefinition {

    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(LoginController.class);
    /**
     * Nit del ministerio de trabajo.
     */
    private static final String NIT_MINTRABAJO = "830115226";
    /**
     * Nit del ministerio de trabajo con d&iacute;gito de verificaci&oacute;n.
     */
    private static final String NIT_MINTRABAJO_AND_DV = NIT_MINTRABAJO + "-3";
    /**
     * Servicio para adminisraci&oacute;n de usuarios.
     */
    @Autowired
    private UserService userService;
    /**
     * Servicio para adminisraci&oacute;n de sesiones.
     */
    @Autowired
    private SessionService sessionService;
    /**
     * Servicio de activaci&oacute;n de cuentas.
     */
    @Autowired
    private ActivationService activateService;
    /**
     * Servicio de log de accesos.
     */
    @Autowired
    private HistoryAccessService historyAccessService;
    /**
     * Servicio para verificaci&oacute;n de Captcha.
     */
    @Autowired
    private VerifyRecaptcha verifyRecaptcha;
    /**
     * Plantilla LDAP.
     */
    @Autowired
    private LDAPOperations ldapOperations;

    /**
     * M&eacute;todo que se ejecuta al abrir la p&aacute;gina.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "/login.htm", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
            HttpServletResponse response) {
        LOGGER.info("Ejecutando metodo [ onDisplay LoginController ]");

        return init(null, null);
    }

    /**
     * M&eacute;todo que se ejecuta cuando el usuario va a iniciar
     * sesi&oacute;n.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "obtenerClave", method
            = RequestMethod.GET)
    public ModelAndView recoverPassword(HttpServletRequest request,
            HttpServletResponse response) {
        LOGGER.info("Ejecutando metodo [ recoverPassword ]");

        try {
            String username = request.getParameter("username");

            boolean userExist = this.activateService.sendPasswordLink(username,
                    LinkReason.FORGOT);

            if (userExist) {
                return init(SUCCESS_ALERT, PASSWORD_RECOVERY_SUCCESFUL);
            }

            return init(FAILED_ALERT, "El usuario no se encuentra registrado "
                    + "en el sistema.");
        } catch (Exception e) {
            LOGGER.info("Error [ recoverPassword ]", e);

            return init(FAILED_ALERT, ErrorCode.ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("Finaliza el metodo recoverPassword");
        }
    }

    /**
     * M&eacute;todo para guardar el log del acceso
     * 
     * @param user 
     */
    private void saveAccessLog(User user) {
        HistoryAccess historyAccess = new HistoryAccess();
        historyAccess.setUser(user);
        historyAccess.setAccessDate(new Date());

        this.historyAccessService.save(historyAccess);
    }

    /**
     * M&eacute;todo que se ejecuta cuando el usuario va a iniciar
     * sesi&oacute;n.
     *
     * @param request
     * @param response
     * @param user
     * @param responseField
     *
     * @return
     */
    @RequestMapping(value = "/login.htm", method = RequestMethod.POST)
    public ModelAndView executeLogin(HttpServletRequest request,
            HttpServletResponse response, @ModelAttribute("user") User user,
         /** @RequestParam("g-recaptcha-response")**/ String responseField) {
        LOGGER.info("Ejecutando metodo [ executeLogin ]");
        try {
           /** String gRecaptchaResponse = request
                    .getParameter("g-recaptcha-response");
            boolean verify = this.verifyRecaptcha.verify(gRecaptchaResponse);

            if (!verify) {
                return init(FAILED_ALERT, "Captcha inv�lido.");
            }**/
            LOGGER.info("consltando email de usuario");
            LOGGER.info("usuario: "+user.getEmail());
            User userdb = this.userService.
                    findByUsernameOrEmail(user.getEmail(),
                            user.getEmail());

            LOGGER.info("id: "+userdb.getCcf().getNit());
            LOGGER.info("usuario: "+userdb.getUsername());
            LOGGER.info("pass: "+userdb.getPassword());
            
            boolean userExist;

            if (userdb != null
                    && (NIT_MINTRABAJO.equals(userdb.getCcf().getNit())
                    || NIT_MINTRABAJO_AND_DV.equals(userdb.getCcf().getNit()))) {
                LOGGER.info("verificando usuario en ldap");
                userExist = this.ldapOperations.authenticate(user.getEmail(),
                        user.getPassword());
                LOGGER.info("verific� usuario en ldap");
                
            } else {
                LOGGER.info("validando usuario en bd");
                userdb = this.userService.searchUserLogin(user.getEmail(),
                        user.getEmail(), user.getPassword(),
                        ProfileStatus.A.name());

                userExist = (userdb != null);
                LOGGER.info("DATOS DEL USUARIO: "+user.getUsername());
            }

            if (userExist && "Activo".equals(userdb.getStatus().getLabel())) {
                Session session = this.sessionService.findByUser(userdb);

                if (session != null) {
                    return init(FAILED_ALERT, "Inicio de sesi�n fallido. Ya "
                            + "existe un usuario logeado con esta cuenta.");
                }

                Session newSession = new Session();
                newSession.setUser(userdb);
                newSession.setLastOperation(new Date());
                newSession.setLastPing(new Date());

                saveAccessLog(userdb);
                this.sessionService.save(newSession);

                request.getSession().setAttribute(SESSION_USER, newSession);

                return new ModelAndView("redirect:/index.htm");

            } else if (userExist && "Inactivo".equals(userdb.getStatus().getLabel())) {
                LOGGER.info("Usuario Inactivo");

                return init(FAILED_ALERT, "Inicio de sesi�n fallido. El usuario "
                        + "con el que intenta ingresar se encuentra inactivo.");
            } else {
                LOGGER.info("Usuario no existe");

                return init(FAILED_ALERT, "Inicio de sesi�n fallido. Las "
                        + "credenciales no coinciden.");
            }
        } catch (Exception e) {
            LOGGER.info("Error [ executeLogin ]", e);

            return init(FAILED_ALERT, ErrorCode.ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("Finaliza el metodo executeLogin");
        }
    }

    /**
     * M&eacute;todo que ejecuta la salida de la sesi&oacute;n del usuario en
     * curso.
     *
     * @param request
     * @param response
     *
     * @return Modelo que expone el resultado del m&eacute;todo.
     *
     * @throws IOException Sucede cuando no encuentra a la p&aacute;gina a la
     * que se redirige
     */
    @RequestMapping(value = "/logout.htm", method = RequestMethod.POST)
    public ModelAndView logout(HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        LOGGER.info("Ejecutando metodo [ logout LoginController ]");

        Session session = (Session) request.getSession().
                getAttribute(SESSION_USER);

        sessionService.remove(session);

        request.getSession().invalidate();

        return new ModelAndView("redirect:/login.htm");
    }

    /**
     * M&eacute;todo que inicializa el model and view.
     *
     * @param messageType
     * @param message
     *
     * @return
     */
    private ModelAndView init(String messageType, String message) {
        ModelAndView modelAndView = new ModelAndView("login", "user",
                new User());
        modelAndView.addObject("captchaKey", this.verifyRecaptcha.getKey());

        if (message != null) {
            modelAndView.addObject("msg", message);
            modelAndView.addObject("msgType", messageType);
        }

        return modelAndView;
    }

    
    @RequestMapping(value = "/sessionPing.htm", method = RequestMethod.POST)
    public String sessionPing(HttpServletRequest request,
            HttpServletResponse response) {
        LOGGER.info("Ejecutando metodo [ sessionPing LoginController ]");

        Session session = (Session) request.getSession().
                getAttribute(SESSION_USER);

        if (session != null) {
            sessionService.sessionPing(session);
            
            return "Ping hecho a sesi�n.";
        }

        return "Sin sesi�n.";
    }

    /**
     * M&eacute;todo que obtiene el nombre de usuario.
     *
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = "/setUserName.htm", method = RequestMethod.GET,
            headers = "Accept=*/*", produces = "application/json")
    public @ResponseBody
    String setUserName(HttpServletRequest request,
            HttpServletResponse response) {
        LOGGER.info("Ejecutando metodo [ setUserName LoginController ]");

        Session session = (Session) request.getSession().
                getAttribute(SESSION_USER);

        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(session.getUser());
        } catch (IOException ex) {
            LOGGER.error("Error obteniendo nombre de usuario " + ex);
            return "";
        }
    }
}
