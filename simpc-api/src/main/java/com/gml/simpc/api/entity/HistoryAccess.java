/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: HistoryAccess.java
 * Created on: 2016/12/26, 11:17:00 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import com.gml.simpc.api.enums.RequestOrigin;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entidad que representa el hist&oacute;rico de accesos del usuario.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Table(name = "HISTORICO_ACCESOS")
@Entity
public class HistoryAccess implements Serializable {

    /**
     * ID del registro.
     */
    @Id
    @Column(name = "ID", length = 10)
    @GeneratedValue(strategy = GenerationType.AUTO, generator =
        "historicAccess_seq_gen")
    @SequenceGenerator(name = "historicAccess_seq_gen", sequenceName =
        "HISTORIC_ACCESS_SEQ", allocationSize = 1, initialValue= 1)
    private Long id;
    /**
     * Id de Usuario que accede al sistema.
     */
    @ManyToOne
    @JoinColumn(name = "ID_USUARIO", nullable = false)
    private User user;
    /**
     * fecha de acceso.
     */
    @Column(name = "FECHA_ACCESO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date accessDate;
    /**
     * Origen de la petici&oacute;n.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "ORIGEN", columnDefinition = "VARCHAR2(7) DEFAULT 'WEB'", 
        nullable = false)
    private RequestOrigin origin = RequestOrigin.WEB;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getAccessDate() {
        return accessDate;
    }

    public void setAccessDate(Date accessDate) {
        this.accessDate = accessDate;
    }

    @Override
    public String toString() {
        return "Histórico Acceso{" + "id=" + id + ", user=" + user + ", " +
            "Fecha de acceso=" +accessDate + '}';
    }
    
    

    public RequestOrigin getOrigin() {
        return origin;
    }

    public void setOrigin(RequestOrigin origin) {
        this.origin = origin;
    }
}
