/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CommonsValidator.java
 * Created on: 2016/01/03, 09:33:45 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 * Objetive: To provide service for launch batch jobs from web for training detail
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.batch;

import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import com.gml.simpc.loader.thread.ThreadLocalValueList;
import com.gml.simpc.web.utilities.ValueListService;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.batch.core.JobExecution;

/**
 * Service for launch spring batch jobs.
 * It uses ApplicationContextAware for access to jobs xml configuration.
 * Requires load type to select suitable job.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Service(value = "jobService")
public class JobService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(JobService.class);

    /**
     * Servicio para lanzar batch jobs
     */
    @Autowired
    private JobLauncher jobLauncher;

    /**
     * Servicio para obtener definicion de los jobs desde el contexto de spring
     */
    @Autowired
    private ApplicationContextAwareImp appConAwareImp;

    /**
     * Servicio para obtener las listas de valores paramétricos del sistema
     */
    @Autowired
    private ValueListService valueListService;

    /**
     * Metodo que lanza un batch job
     *
     * @param readerPath, ruta del archivo a cargar
     * @param jobName,    nombre del job
     * @param loadId
     * @param writeType
     * @return 
     */
    public JobResultDto launchJob(String readerPath, String jobName, Long loadId,
        String writeType) {

        JobResultDto jobResult;
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath", "file:" + readerPath);
        jobParametersBuilder.addString("writeType", writeType);
        if (loadId != null) {
            jobParametersBuilder.addString("loadId", loadId.toString());
        }
        JobParameters parameters = jobParametersBuilder.toJobParameters();
        LOGGER.info("Executing " + jobName + " for load " + loadId +
            " with file " +
            parameters.getString("filePath"));

        //Inicializamos local thread
        ThreadLocalJob.init();

        //Inicializamos listas de valores
        ThreadLocalValueList.set(this.valueListService.getValueList());

        Job loadJob;
        try {
            loadJob = appConAwareImp.getJob(jobName);
            JobExecution jobExecution = this.jobLauncher.run(loadJob, 
                parameters);
            List<Throwable> exceptions = jobExecution.getAllFailureExceptions();
            
            if (exceptions != null) {
                for (Throwable t : exceptions) {
                    LOGGER.error("Error", t);
                }
            }
        } catch (Exception e) {
            ThreadLocalJob.setStatus("EXECUTION_ERROR");
            LOGGER.error("Error launching " + jobName, e);
        }
        jobResult = ThreadLocalJob.getResult();
        LOGGER.info(jobResult.toString());

        if (!jobResult.getErrorList().isEmpty()) {
            LOGGER.info("Errors details for " + jobName + ": " +
                jobResult.getErrorList());
        }

        ThreadLocalJob.delete();

        return jobResult;
    }
}
