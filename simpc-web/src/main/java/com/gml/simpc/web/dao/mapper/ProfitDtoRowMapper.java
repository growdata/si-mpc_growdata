/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: PilaDtoRowMapper.java
 * Created on: 2017/02/13, 11:30:02 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.dao.mapper;

import com.gml.simpc.api.dto.ProfitDto;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 * Row Mapper para PILA.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class ProfitDtoRowMapper implements RowMapper<ProfitDto> {

    @Override
    public ProfitDto mapRow(ResultSet rs, int i) throws SQLException {
        ProfitDto e = new ProfitDto();
        e.setCargaId(rs.getLong("CARGA_ID"));
        e.setUnemployedIDType(rs.getString("TIPO_IDENTIFICACION"));
        e.setUnemployedID(rs.getString("NUMERO_IDENTIFICACION"));
        e.setEventType(rs.getString("TIPO_NOVEDAD"));
        e.setCcfCode(rs.getString("CODIGO_CCF"));
        e.setFirstName(rs.getString("PRIMER_NOMBRE"));
        e.setSecodName(rs.getString("SEGUNDO_NOMBRE"));
        e.setFirstSurname(rs.getString("PRIMER_APELLIDO"));
        e.setSecondSurname(rs.getString("SEGUNDO_APELLIDO"));
        e.setBirthday(rs.getString("FECHA_NACIMIENTO"));
        e.setGender(rs.getString("GENERO"));
        e.setResidentState(rs.getString("DEPARTAMENTO_RESIDENCIA"));
        e.setResidentCity(rs.getString("CIUDAD_RESIDENCIA"));
        e.setSalaryrange(rs.getString("RANGO_SALARIAL"));
        e.setPublicServiceRegistration(rs.getString(
            "INSCRIPCION_SERVICIO_EMPLEO"));
        e.setVinculationType(rs.getString("TIPO_VINCULACION_CCF"));
        e.setLastCff(rs.getString("ULTIMA_CCF"));
        e.setPostulationDateMpc(rs.getString("FECHA_POSTULACION_MPC"));
        e.setHealthManager(rs.getString("ADMINISTRADORA_SALUD"));
        e.setRetirementPostulation(rs.getString("POSTULA_PENSION"));
        e.setRetirementManager(rs.getString("ADMINISTRADORA_PENSION"));
        e.setEcononomicSubsidyFee(rs.getString("SUBSIDIO_CUOTA_MONETARIA"));
        e.setMpcSavings(rs.getString("AHORRO_CESANTIAS_MPC"));
        e.setSeveranceFundAdministrator(rs.getString(
            "ADMINISTRADORA_FONDO_CESANTIAS"));
        e.setPercentageSavings(rs.getString("PORCENTAJE_AHORRO_CESANTIAS"));
        e.setFeedBonusPostulation(rs.getString("POSTULA_BONO_ALIMENTACION"));
        e.setRequirements(rs.getString("VERIFICACION_REQUISITOS"));
        e.setPostulationDateCheck(rs.getString("FECHA_VERIFICACION_POSTU"));
        e.setBeneficiary(rs.getString("BENEFICIARIO_DEL_MECANISMO"));
        e.setApplyWithRoute(rs.getString("CUMPLE_RUTA_DE_EMPLEABILDAD"));
        e.setSettlementDate(rs.getString("FECHA_LIQUIDACION_BENEFICIO"));
        e.setBenefitClearedPeriod(rs.getString("PERIODO_BENEFICIO_LIQUIDADO"));
        e.setNumberLiquidBenefi(rs.getString("NUMERO_BENEFICIOS_LIQUIDADOS"));
        e.setNumberMoneyChargesLiquided(rs.getString("NUMERO_CUOTAS_LIQUIDADAS"));
        e.setNumberDependents(rs.getString("NUMERO_PERSONAS_CARGO"));
        e.setEconomicIncentiveSavings(rs.getString("NUMERO_CUOTAS_INCENTIVO"));
        e.setNumbrLiquidBonds(rs.getString("NUMERO_BONOS_LIQUIDADOS"));
        e.setHelthLiquidedAmount(rs.getString("MONTO_LIQUIDADO_SALUD"));
        e.setRetirementliquidedAmount(rs.getString("MONTO_LIQUIDADO_PENSION"));
        e.setMonetaryLiquidedAmount(rs.getString("MONTO_LIQUIDADO_CUOTA"));
        e.setLiquidedAssetAmount(rs.getString("MONTO_LIQUIDADO_CESANTIAS"));
        e.setFeedBonusLiquidedAmount(rs.getString("MONTO_LIQUIDADO_ALIMENTACION"));
        e.setTotalValueProfits(rs.getString("VALOR_TOTAL_PRESTACIONES"));
        e.setVoluntaryWaiver(rs.getString("RENUNCIA_VOLUNTARIA"));
        e.setWaiverDate(rs.getString("FECHA_RENUNCIA"));
        e.setLostBenefit(rs.getString("PERDIDA_BENEFICIO"));
        e.setLostBenefitDate(rs.getString("FECHA_PERDIDA_BENEFICIO"));
        e.setBenefitEnd(rs.getString("TERMINACION_BENEFICIO"));
        e.setBenefitEndDate(rs.getString("FECHA_TERMINACION_BENEFICIO"));
        return e;
    }
}
