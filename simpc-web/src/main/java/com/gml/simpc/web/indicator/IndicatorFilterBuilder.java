/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IndicatorFilterBuilder.java
 * Created on: 2016/12/27, 10:25:57 AM
 * Project: MPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.indicator;

import com.gml.simpc.api.dto.IndicatorFilterDto;
import com.gml.simpc.api.dto.PeriodDto;
import com.gml.simpc.api.entity.IndicatorGraphic;
import com.gml.simpc.api.entity.exeption.UserException;
import com.gml.simpc.api.enums.Periodicity;
import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;

import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_026_MISSING_DATA;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_027_DATE_OUT_OF_RANGE;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_028_CONFIGURATION_ERROR;

/**
 * Utileria para construir el filtro de indicadores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public final class IndicatorFilterBuilder {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(IndicatorFilterBuilder.class);
    /**
     * Formato para evaluar las variables.
     */
    private static final String VAR_FORMAT = "\\w+\\.\\w+(=\\w+)?";
    /**
     * Formato para evaluar las columnas.
     * Parejas de la forma nombre_campo:descripcion separadas por coma
     * Almenos una obligatoria
     */
    private static final String COLUMNS_FORMAT =
        "\\w+:[0-9a-zA-Z������������\\s]+(,\\w+:[0-9a-zA-Z������������\\s]+)*";
    /**
     * Formato del nombre del campo fecha.
     */
    private static final String DATE_FIELD_FORMAT = "\\w+";
    /**
     * Separador entre el resultado y la variable
     */
    private static final String RESULT_SEPARATOR = "=";
    /**
     * Separador de columnas.
     */
    private static final String COLUMNS_SEPARATOR = ",";
    /**
     * Separador entre nombre de tabla y campo.
     */
    private static final String TABLE_AND_FIELD_SEPARATOR = "\\.";
    /**
     * Separador entre columnas y t&iacute;tulos.
     */
    private static final String COLUMN_AND_TITLE_SEPARATOR = ":";
    /**
     * Primera posici&oacute;n de un array.
     */
    private static final int FIRST_POSITION = 0;
    /**
     * Segunda posici&oacute;n de un array.
     */
    private static final int SECOND_POSITION = 1;
    /**
     * Constante para validar un solo per&iacute;odo.
     */
    private static final int ONE_PERIOD = 1;
    /**
     * Constante que representa la equivalencia de un an&tilde;o en
     * per&iacute;odos.
     */
    private static final int ONE_YEAR = 13;
    /**
     * Primer d&iacute;a del mes.
     */
    private static final int FIRST_DAY_OF_MONTH = 1;
    /**
     * Constante para comparaci&oacute;n de fecha exitosa.
     */
    private static final int EXPECTED_DATE = 0;

    /**
     * Constructor privado para impedir instancias de las clases.
     */
    private IndicatorFilterBuilder() {
    }

    /**
     * M&eacute;todo que construye el filtro para consultar indicadores.
     *
     * @param dto
     * @param graphic
     *
     * @return
     *
     * @throws UserException
     */
    public static IndicatorFilterDto buildIndicatorFilter(PeriodDto dto,
        IndicatorGraphic graphic) throws UserException {
        LOGGER.info("Ejecutando metodo [ buildIndicatorInfo ]");

        if (dto == null || graphic == null || graphic.getIndicator() == null) {
            throw new UserException(ERR_026_MISSING_DATA);
        }

        String var = graphic.getIndicator().getVar();
        String dateField = graphic.getDateVar();
        String columns = graphic.getIndicator().getColumns();

        evaluateVarFormat(var);
        evaluateDateFieldFormat(dateField);
        evaluateColumnsFormat(columns);

        IndicatorFilterDto filter = initIndicatorFilter(var);
        filter.setDateField(dateField);
        validatePeriods(filter, graphic);
        validateDates(dto, filter, graphic);
        buildGraphicLabels(graphic, filter);
        
        filter.setType(graphic.getType());

        addFilterColumns(filter, columns);
        filter.setGroupField(graphic.getGroupField());
        filter.setAgregateFunction(graphic.getAgregateFunction());

        return filter;
    }
    /**
     * Valida los per&iacute;odos de filtro.
     *
     * @param filter
     * @param graphic
     *
     * @throws UserException
     */
    private static void validatePeriods(IndicatorFilterDto filter,
        IndicatorGraphic graphic) throws UserException {
        LOGGER.info("Ejecutando metodo [ validatePeriods ]");

        filter.setNumberOfPeriods(graphic.getNumberOfPeriods());

        if (graphic.getPeriodicity() == Periodicity.A) {
            filter.multiplyNumberOfPeriods(ONE_YEAR);
        }

        filter.setHasManyPeriods(filter.getNumberOfPeriods() > ONE_PERIOD);
    }

    /**
     * Valida las fechas y si cumplen con el rango configurado.
     *
     * @param dto
     * @param filter
     * @param graphic
     *
     * @throws UserException
     */
    private static void validateDates(PeriodDto dto, IndicatorFilterDto filter,
        IndicatorGraphic graphic) throws UserException {
        LOGGER.info("Ejecutando metodo [ validateDates ]");

        Calendar cal = Calendar.getInstance();
        cal.set(dto.getStartYear(), dto.getStartMonth() - 1,
            FIRST_DAY_OF_MONTH, 0, 0, 0);
        cal.set(Calendar.MILLISECOND, 0);

        Date startDate = cal.getTime();

        filter.setStartDate(startDate);

        cal.add(Calendar.MONTH, filter.getNumberOfPeriods());
        Date endDate = cal.getTime();

        if (filter.hasManyPeriods()) {
            if (graphic.getPeriodicity() != Periodicity.A) {
                cal.set(dto.getEndYear(), dto.getEndMonth(), FIRST_DAY_OF_MONTH,
                    0, 0, 0);
                cal.set(Calendar.MILLISECOND, 0);

                Date expectedDate = cal.getTime();

                if (expectedDate.compareTo(startDate) < EXPECTED_DATE ||
                    endDate.compareTo(expectedDate) < EXPECTED_DATE) {
                    throw new UserException(ERR_027_DATE_OUT_OF_RANGE,
                        filter.getNumberOfPeriods());
                }
            }
        }

        filter.setEndDate(endDate);
    }

    /**
     * Verifica si la variable cumple con el formato esperado.
     *
     * @param var
     *
     * @throws UserException
     */
    private static void evaluateVarFormat(String var)
        throws UserException {
        LOGGER.info("Ejecutando metodo [ evaluateVarFormat ]");
        if (!var.matches(VAR_FORMAT)) {
            throw new UserException(ERR_028_CONFIGURATION_ERROR);
        }
    }

    /**
     * Verifica si las columnas cumplen con el formato esperado.
     *
     * @param columns
     *
     * @throws UserException
     */
    private static void evaluateColumnsFormat(String columns)
        throws UserException {
        LOGGER.info("Ejecutando metodo [ evaluateColumnsFormat ]");

        if (!columns.matches(COLUMNS_FORMAT)) {
            LOGGER.error("Error validando "+columns+" con formato "+COLUMNS_FORMAT);
            throw new UserException(ERR_028_CONFIGURATION_ERROR);
        }
    }

    /**
     * Verifica si la columna cumple con el formato esperado.
     *
     * @param field
     *
     * @throws UserException
     */
    private static void evaluateDateFieldFormat(String field)
        throws UserException {
        LOGGER.info("Ejecutando metodo [ evaluateDateFieldFormat ]");

        if (!field.matches(DATE_FIELD_FORMAT)) {
            throw new UserException(ERR_028_CONFIGURATION_ERROR);
        }
    }

    /**
     * Inica la construcci&oacute;n del filtro para indicadores a partir del
     * nombre de la variable.
     *
     * @param var
     *
     * @return
     */
    private static IndicatorFilterDto initIndicatorFilter(String var) {
        LOGGER.info("Ejecutando metodo [ buildIndicatorFilter ]");

        IndicatorFilterDto dto = new IndicatorFilterDto();

        String tokens[] = null;

        if (var.contains(RESULT_SEPARATOR)) {
            tokens = var.split(RESULT_SEPARATOR);

            dto.setValue(tokens[SECOND_POSITION]);
        }

        if (tokens != null) {
            tokens = tokens[FIRST_POSITION].split(TABLE_AND_FIELD_SEPARATOR);
        } else {
            tokens = var.split(TABLE_AND_FIELD_SEPARATOR);
        }

        dto.setTableName(tokens[FIRST_POSITION]);
        dto.setFieldName(tokens[SECOND_POSITION]);

        return dto;
    }

    /**
     * Obtiene las columnas a consultar para graficar indicadores.
     *
     * @param dto
     * @param columns
     */
    private static void addFilterColumns(IndicatorFilterDto dto, String columns) {
        LOGGER.info("Ejecutando metodo [ addFilterColumns ]");

        String columnList[] = columns.split(COLUMNS_SEPARATOR);

        for (String column : columnList) {
            String columnTokens[] = column.split(COLUMN_AND_TITLE_SEPARATOR);

            dto.getColumns().add(columnTokens[FIRST_POSITION]);
            dto.getTitles().add(columnTokens[SECOND_POSITION]);
        }
    }

    /**
     * Obtiene los labels para graficar Pies.
     *
     * @param graphic
     * @param dto
     */
    private static void buildGraphicLabels(IndicatorGraphic graphic,
        IndicatorFilterDto dto) {
        LOGGER.info("Ejecutando metodo [ buildGraphicLabels ]");
                
        if (graphic.getLabels() == null) {
            dto.setLabel1("A");
            dto.setLabel2("B");

            return;
        }

        String labels[] = graphic.getLabels().split(COLUMNS_SEPARATOR);

        if (labels.length < 2) {
            dto.setLabel1("A");
            dto.setLabel2("B");

            return;
        }

        dto.setLabel1(labels[FIRST_POSITION]);
        dto.setLabel2(labels[SECOND_POSITION]);
    }
    
    
}
