<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster.bundle.min.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster-sideTip-shadow.min.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
    </head>
    <body>
        <c:if test="${not empty internalMsg}">
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${internalMsg}</strong>
            </div>
        </c:if>
        <form:form id="creationSedeForm" modelAttribute="creationHeadquarter" method="post" 
                   action="/masterdata/guardarSede.htm" target="_parent">
            <c:if test="${creationHeadquarter.principal}">
                <input type="hidden" name="principal" path="principal" value="${creationHeadquarter.principal}"/>
            </c:if>
            <input class="principalStatus" type="hidden" name="principalStatus" value=""/>    
            <form:input type="hidden" path="id" value="${creationHeadquarter.id}"/>
            <form:input type="hidden" path="codigo" value="${creationHeadquarter.codigo}"/>
            <form:input type="hidden" path="institutionId" value="${parentInstitution.id}"/>
            <form:input type="hidden" path="institution.id" value="${parentInstitution.id}"/>
            <form:input type="hidden" path="institution.type.id" value="${parentInstitution.type.id}"/>
            <form:input type="hidden" path="institution.type.name" value="${parentInstitution.type.name}"/>
            <form:input type="hidden" path="institution.nit" value="${parentInstitution.nit}"/>
            <form:input type="hidden" path="institution.dv" value="${parentInstitution.dv}"/>
            <form:input type="hidden" path="institution.institutionName" value="${parentInstitution.institutionName}"/>
            <form:input type="hidden" path="institution.origin.code" value="${parentInstitution.origin.code}"/>
            <form:input type="hidden" path="institution.status.code" value="${parentInstitution.status.code}"/>
            <form:input type="hidden" path="institution.legalNature.code" value="${parentInstitution.legalNature.code}"/>
            <form:input type="hidden" path="institution.contactPhone" value="${parentInstitution.contactPhone}"/>
            <form:input type="hidden" path="institution.contactEmail" value="${parentInstitution.contactEmail}"/>
            <form:input type="hidden" path="institution.dateToShow" value="${parentInstitution.dateToShow}"/>
            <c:if test="${creationHeadquarter.status.code == 'I' && creationHeadquarter.id != null}">
               <form:input type="hidden" path="name" value="${creationHeadquarter.name}"/> 
               <form:input type="hidden" path="address" value="${creationHeadquarter.address}"/> 
               <form:input type="hidden" path="state.id" value="${creationHeadquarter.state.id}"/> 
               <form:input type="hidden" path="city.id" value="${creationHeadquarter.city.id}"/> 
            </c:if>
            <div class="col-sm-6">
                <h1>Datos de la sede</h1>
                <br/>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="name" class="control-label">Nombre de la Sede(*)</label>
                    <form:input type="text" class="form-control disable" id="name" name="name" 
                                placeholder="Nombre de la Sede" required="required"
                                path="name" maxlength="50"/>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="address" class="control-label">Direcci&oacute;n de la Sede(*)</label>
                    <form:input type="text" class="form-control disable" id="address" name="address" 
                                placeholder="Direcci�n de la sede" required="required"
                                path="address" maxlength="200"/>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="departamento" class="control-label">Departamento(*)</label>
                    <form:select onchange="mostrarMunicipio()" class="form-control disable" id="departamentoModal" 
                                 path="state.id" required="required">
                        <option id="selectedDep" value="">Seleccione...</option>
                    </form:select>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-sm-3 listaMunicipio">
                <div class="form-group">
                    <label for="municipio" class="control-label">Municipio(*)</label>
                    <form:select class="form-control disable" id="municipioModal" 
                                 path="city.id" required="required">
                    </form:select>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-sm-3 estadoSede">
                <div class="form-group">
                    <label for="naturalezaLegal" class=" control-label">Estado(*)</label>
                    <form:select onchange="msnDanger()" class="form-control" id="status" path="status" required="required">
                        <c:if test="${not empty headquarterStatusList}">
                            <c:forEach var="item" items="${headquarterStatusList}">
                                <c:if test="${item == creationHeadquarter.status}">
                                    <option label="${item.label}" value="${item}" selected="false"/>
                                </c:if>
                                <c:if test="${item != creationHeadquarter.status}">
                                    <option label="${item.label}" value="${item}"/>
                                </c:if>
                            </c:forEach>
                        </c:if>
                    </form:select>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <form:checkbox onchange="mensajeSede()" id="esPrincipal" class="control-label disable"
                                   path="principal" name=""/>
                    <label for="nombreSede" class="control-label">Es principal</label>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="container">
                <div class="alert alert-warning alert-dismissible" id="myAlert">
                    <strong>�Atenci�n!</strong> Al activar esta sede como principal, la sede <c:out value="${headquarterName}"/> dejar� de ser la sede principal.
                </div>
            </div>
            <div class="col-sm-6 msnModal">
                <div class="form-group">
                    <label for="message-text" class="form-control-label">Causa de la modificaci�n de la Sede:</label>
                    <textarea class="form-control" id="message-text" name="message-text" required="required"></textarea>
                </div>
            </div>
            <div class="container2">
                <div class="alert alert-danger alert-dismissible" id="myAlert">
                    <strong>�Atenci�n!</strong> No es posible inactivar esta sede ya que la instituci�n <c:out value="${institutionParentName}"/> no posee m�s sedes activas.
                </div>
            </div>
            <div class="col-sm-6" style="text-align:right">
                <br/>
                <button id="createSedeButton" class="btn btn-success"
                        data-btn-ok-label="Confirmar" data-btn-cancel-label="Cancelar"
                        data-title="Est� seguro?" data-toggle="confirmation" 
                        data-placement="top" confirmation-callback>Guardar</button>
                <button type="button" class="btn btn-danger" onclick="window.parent.closeModal()">Cancelar</button>
            </div>
    </form:form>

        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
		<script src="resources/js/dataTables.buttons.min.js"></script>
        <script src="resources/js/buttons.flash.min.js"></script>
        <script src="resources/js/jszip.min.js"></script>
        <script src="resources/js/pdfmake.min.js"></script>
        <script src="resources/js/vfs_fonts.js"></script>
        <script src="resources/js/buttons.html5.min.js"></script>
        <script src="resources/js/buttons.print.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/jquery.ui.widget.js"></script>
        <script src="resources/js/jquery.iframe-transport.js"></script>
		<script src="resources/js/bootstrap-confirmation.js"></script>
        <script src="resources/js/jquery.fileupload.js"></script>
		<script src="resources/js/jquery.validate.min.js"></script>
		<script src="resources/js/tooltipster.bundle.js"></script>
		<script src="resources/js/custom-functionalities.js"></script>
        <script>
            
            var estadoInicial = ($('#status').val());
            var muestra = "";
            var ignore = "";
            var ultimateHQ = "";
            
            $(document).ready(function () {
                
                var institutionParentName = "<c:out value="${institutionParentName}"/>";
                var headquarterId = "<c:out value="${creationHeadquarter.id}"/>";
                
                if($('#status').val() === 'A' && institutionParentName !== "" && headquarterId !== ""){
                    ultimateHQ = "true";
                }
                
                var headquarterName = "<c:out value="${headquarterName}"/>";
                $('.container2').hide("fast");
                $('#institutions').DataTable({
                    "language": {
                        "url": "resources/js/Spanish.json"
                    },
                    "bFilter": false,
                    "bInfo": false
                });
                loadStates("departamentoModal", "municipioModal");
                loadCitiesByState("departamentoModal", "municipioModal");
                
                if(headquarterName === ""){
                    $('.container').hide("fast");
                    $('.principalStatus').val("false");
                    ignore = "null";
                }else{    
                    <c:if test="${creationHeadquarter.principal}">
                        $('.container').hide("fast");
                        $('.principalStatus').val("true");
                        ignore = "true";
                    </c:if>
                    <c:if test="${!creationHeadquarter.principal}">
                        $('.container').hide("fast");
                        $('.principalStatus').val("false");
                        muestra = "true";
                        ignore = "false";
                    </c:if>

                }
                
                <c:if test="${not empty creationHeadquarter.id}">
                    $('#departamentoModal').val(${creationHeadquarter.state.id});
                    loadCities(jQuery("#departamentoModal").val(), "departamentoModal", "municipioModal");
                    $('#municipioModal').val(${creationHeadquarter.city.id});
                </c:if>
                
                <c:choose>
                    <c:when test="${creationHeadquarter.id != null}">
                        $(".estadoSede").show();
                        if($('#status').val() === 'I'){
                           $(".disable").attr("disabled","true");
                        }else{
                           $(".disable").removeAttr("disabled");
                        }
                        $(".msnModal").show();
                        $("#selectedDep").removeAttr("selected");
                    </c:when>
                    <c:otherwise>
                        $(".estadoSede").hide();
                        $(".msnModal").hide();
                        $('.listaMunicipio').hide();
                        $("#selectedDep").attr("selected", "true");
                    </c:otherwise>
                </c:choose>
                
            });
                    
            function loadStates(deptoId, mnpoId) {
                <c:if test="${not empty stateList}">
                    <c:forEach var="item" items="${stateList}" varStatus="iter">
                        <c:if test="${iter.index == 0}">
                            jQuery("<option>").attr("value", '${item.id}').
                                    text('${item.name}').attr("selected", "false").
                                    appendTo("#" + deptoId);
                            loadCities("${item.id}", deptoId, mnpoId);
                        </c:if>
                        <c:if test="${iter.index > 0}">
                            jQuery("<option>").attr("value", '${item.id}').
                                    text('${item.name}').appendTo("#" + deptoId);
                        </c:if>
                    </c:forEach>
                </c:if>
            }

            function loadCitiesByState(deptoId, mnpoId) {
                jQuery("#" + deptoId).change(function () {
                    jQuery("#" + mnpoId).html('');
                    var stateCode = jQuery("#" + deptoId).val();
                    loadCities(stateCode, deptoId, mnpoId);
                });
            }

            function loadCities(stateCode, deptoId, mnpoId) {
                jQuery("#" + mnpoId).find('option').remove();
                jQuery("<option>").attr("value", '').
                        text('Seleccione...').appendTo("#" + mnpoId);
                <c:if test="${not empty citiesList}">
                    <c:forEach items="${citiesList}" var="item" >
                        var code = "<c:out value="${item.state.id}"/>";
                        if (code === stateCode) {
                            jQuery("<option>").attr("value", '${item.id}').
                                    text('${item.name}').appendTo("#" + mnpoId);
                        }
                    </c:forEach>
                </c:if>
            }
            
            $('#createSedeButton').confirmation({
                onConfirm: function (event, element) {
                    if (validateForm("creationSedeForm")) {
                        $('#creationSedeForm').submit();
                    }
                }
            });
            
            function mostrarMunicipio(){
                $('.listaMunicipio').show();
            }
            
            function mensajeSede(){
            
                if (muestra === 'true' && ignore === "false"){
                    $('.container').show("fast");
                    $('.principalStatus').val("true");
                    muestra = "false";
                }else if((muestra === 'false' && ignore === "false")){
                    $('.container').hide("fast");
                    $('.principalStatus').val("false");
                    muestra = "true";
                }
                
                if ((ignore === "true" || ignore === "null") && $('.principalStatus').val() === "true"){
                    $('.principalStatus').val("false");
                }else if((ignore === "true" || ignore === "null") && $('.principalStatus').val() === "false"){
                    $('.principalStatus').val("true");
                }
            }
            
            function msnDanger(){
                
                if(ultimateHQ === "true" && $('#status').val() === 'A'){
                    $('.container2').hide("fast");
                    $('#createSedeButton').removeAttr("disabled");
                }else if(ultimateHQ === "true" && $('#status').val() === 'I'){
                    $('.container2').show("fast");
                    $('#createSedeButton').attr("disabled","true");
                }
            }
            
        </script>
    </body>
</html>
