/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Headquarter.java
 * Created on: 2016/10/19, 01:37:45 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gml.simpc.api.entity.valuelist.City;
import com.gml.simpc.api.entity.valuelist.State;
import com.gml.simpc.api.enums.HeadquarterStatus;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entidad que representa la sede
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Table(name = "SEDES")
@Entity
public class Headquarter implements Serializable {

    /**
     * ID de la sede.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator =
        "headquarter_seq_gen")
    @SequenceGenerator(name = "headquarter_seq_gen", sequenceName =
        "HEADQUARTER_SEQ", initialValue = 101, allocationSize = 1)
    private Long id;

    /**
     * nombre de la sede.
     */
    @Column(name = "CODIGO", nullable = false, length = 200)
    private String codigo;

    /**
     * nombre de la sede.
     */
    @NotNull(message = "El campo Nombre Sede no puede estar vac�o.")
    @Size(max = 50, message = "El Nombre Sede no puede tener m�s de 50 " +
        "caracteres.")
    @Column(name = "NOMBRE", nullable = false, length = 50)
    private String name;
    /**
     * nombre de la sede.
     */
    @Size(max = 50, message = "El Direcci�n no puede tener m�s de 200 " +
        "caracteres.")
    @Column(name = "DIRECCION", nullable = false, length = 200)
    private String address;
    /**
     * departamento al cual pertenece el municipio.
     */
    @ManyToOne
    @JoinColumn(name = "ID_MUNICIPIO", nullable = false)
    private City city;
    /**
     * departamento al cual pertenece el municipio.
     */
    @ManyToOne
    @JoinColumn(name = "ID_DEPARTAMENTO", nullable = false)
    private State state;
    /**
     * departamento al cual pertenece el municipio.
     */
    @Column(name = "ID_INSTITUCION", nullable = false)
    private Long institutionId;
    /**
     * Booleano que determina si esta sede es principal o no.
     */
    @Column(name = "PRINCIPAL", nullable = false,
        columnDefinition = "NUMBER(1) DEFAULT 0")
    boolean principal;
    /**
     *
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "ESTADO", columnDefinition = "char(1) default 'A'",
        nullable = false)
    private HeadquarterStatus status = HeadquarterStatus.A;
    /**
     * Id de Usuario de creaci&oacute:n. Campo de Auditor&iacute;a.
     */
    @Column(name = "ID_USUARIO_CREACION", length = 10, nullable = false)
    private Long creationUserId;
    /**
     * fecha de creaci&oacute:n. Campo de Auditor&iacute;a.
     */
    @Column(name = "FECHA_CREACION", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    /**
     * Id de Usuario de modificaci&oacute;n.Campo de Auditor&iacute;a.
     */
    @Column(name = "ID_USUARIO_MODIFICACION")
    private Long modificationUserId;
    /**
     * fecha de modificaci&oacute:n. Campo de Auditor&iacute;a.
     */
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modificationDate;
    @JsonIgnore
    @Transient
    private Institution institution;

    /**
     * Constructor por defecto.
     */
    public Headquarter() {
        this.id = null;
    }

    /**
     * Constructor por departamento y municipio.
     *
     * @param city
     * @param state
     */
    public Headquarter(City city, State state) {
        this.city = city;
        this.state = state;
    }

    /**
     * Constructor por departamento, municipio e instituci&oacute;n.
     *
     * @param city
     * @param state
     * @param institution
     */
    public Headquarter(City city, State state, Institution institution) {
        this.city = city;
        this.state = state;
        this.institution = institution;
        this.institutionId = institution.getId();
    }

    /**
     * Constructor por id y nombre.
     *
     * @param id
     * @param name
     */
    public Headquarter(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public boolean isPrincipal() {
        return principal;
    }

    public void setPrincipal(boolean principal) {
        this.principal = principal;
    }

    public Long getCreationUserId() {
        return creationUserId;
    }

    public void setCreationUserId(Long creationUserId) {
        this.creationUserId = creationUserId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Long getModificationUserId() {
        return modificationUserId;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setModificationUserId(Long modificationUserId) {
        this.modificationUserId = modificationUserId;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Long getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(Long institution) {
        this.institutionId = institution;
    }

    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
        this.institutionId = institution.getId();
    }

    public HeadquarterStatus getStatus() {
        return status;
    }

    public void setStatus(HeadquarterStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Id De la Sede en Tabla ").append(id).
            append(", C�digo=").append(codigo).
            append(", Nombre=").append(name).
            append(", Direcci�n=").append(address).
            append(", Municipio=").append(city).
            append(", Departamento=").append(state).
            append(", Id Instituci�n=").append(institutionId).
            append(", Es Principal =").append(principal).
            append(", Estado=").append(status).
            append(", Id de Usuario creador=").append(creationUserId).
            append(", Fecha de creaci�n=").append(creationDate).
            append(",Id de Usuario Modificador=").append(modificationUserId).
            append(", Fecha de modificaci�n=").append(modificationDate).
            append(", Instituci�n=").append(institution);
        return stringBuilder.toString();
    }

}
