/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: IndicatorGroupFunction.java
 * Created on: 2017/04/11, 10:19:18 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.enums;

/**
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica Jim&eacute;nez Rozo</a>
 */
public enum IndicatorGroupFunctiontType {

    C('C', "Cantidad de repeticiones"),
    S('S', "Suma de valores"),
    A('A', "Promedio de valores");
    
     /**
     * C&oacute;digo
     */
    private final char code;
    /**
     * Valor
     */
    private final String label;

    private IndicatorGroupFunctiontType(char code, String label) {
        this.code = code;
        this.label = label;
    }

    public char getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }
    
    
}
