/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: SupplierBankService.java
 * Created on: 2017/01/12, 03:17:00 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.google.common.collect.Table;

/**
 * Interface que define el servicio para manipular las consultas del portal
 * de capacitaciones
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
public interface TrainingService {

    /**
     * Obtiene los contadores de modulo por:
     * @param instTypeSel
     * @param ccfSel
     * @param anio
     * @param mes
     * @return
     */
    public Table getModuleCounts(String ccfSel,String instTypeSel,String anio,
            String mes);
    
    /**
     * Obtiene los contadores de modulo por tipo de capacitacion 
     * para un tipo de institucion espcifico
     * @param instTypeSel
     * @param anio
     * @param mes
     * @return
     */
        
}
