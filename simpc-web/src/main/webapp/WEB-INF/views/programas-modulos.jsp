<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster.bundle.min.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster-sideTip-shadow.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-toggle.min.css" rel="stylesheet"/>
        <link href="resources/css/print-buttons.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>

    </head>
    <body>
        <c:if test="${not empty msg}">
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        <br/>
        <h1>Programas/M&oacute;dulos Registrados</h1>
        <div class="panel panel-default">
            <div class="panel-heading"><h2>Programas Registrados</h2></div>
            <div class="panel-body">
                <div id="alert-area" ></div>
                <form:form id="rangoForm" name="rangoForm" action="/masterdata/rango-modulos.htm" modelAttribute="searchModules" 
                           method="POST">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="fechaInicio" class="control-label ">Fecha Inicio</label> 
                            <form:input id="fechaInicio" name="fechaInicio" class="form-control "
                                        data-date-format="dd-mm-yyyy"  path="startDate" autocomplete="off"  value="${initDateSelected}" />
                            <fmt:formatDate value="${fechaInicio}" pattern="dd-MM-yyyy" />
                            <div class="clearfix"></div>
                        </div>
                    </div>  
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="fechaFin" class="control-label ">Fecha Fin</label>
                            <form:input id="fechaFin" name="fechaFin"  class="form-control "
                                        data-date-format="dd-mm-yyyy"    autocomplete="off" path="endDate"  value="${endDateSelected}"/>
                            <fmt:formatDate value="${fechaFin}" pattern="dd-MM-yyyy" />
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="nombreModuloLabel" class="control-label">Nombre del Programa</label>
                                <form:input type="text" id="nombrePrograma" name="nombrePrograma" class="form-control" path="programName" placeholder="Escriba Nombre del Programa" value="${programNameSelected}"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="codigoModuloLabel" class="control-label" >C&oacute;digo del Programa
                            </label>
                            <form:input type="text"  id="codigoPrograma" name="codigoPrograma" 
                                        maxlength="10" class="form-control" path="programCode"  placeholder="Escriba C�digo del Programa" value="${programCodeSelected}"/>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="nombreModuloLabel" class="control-label">Nombre del M&oacute;dulo </label>
                        <form:input type="text" id="nombreModulo" name="nombreModulo" class="form-control"  path="moduleName" placeholder="Escriba aqu� nombre del M�dulo" value="${moduleNameSelected}"/>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="formationType" class="control-label">Tipo de Formaci&oacute;n</label>
                            <form:select class="form-control" name="tipoFormacion" id="tipoFormacion" path="trainingType"> 
                                <form:option value='' label="Seleccione ..." ></form:option>
                                <c:if test="${not empty formationType}">     
                                    <c:forEach items="${formationType}" var="item" begin="0">
                                        <option   <c:if test="${item.id eq formationSelected}">selected="selected"</c:if> value="${item.id}">${item.name}</option>                                     
                                    </c:forEach>  
                                </c:if>
                            </form:select>   
                        </div>
                    </div> 
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="formationType" class="control-label">Nombre de la instituci�n</label>
                            <form:select class="form-control" name="institucion" id="institucion" path="institution"> 
                                <form:option value='' label="Seleccione ..." ></form:option>
                                <c:if test="${not empty institutionList}">     
                                    <c:forEach items="${institutionList}" var="item" begin="0">
                                        <option   <c:if test="${item.id eq instiSelected}">selected="selected"</c:if>  value="${item.id}">${item.institutionName}</option>                                     
                                    </c:forEach>  
                                </c:if>
                            </form:select>   
                        </div>
                    </div> 
                    <div class="col-sm-12" style="text-align:right">
                        <button type="button"  onClick="validarCampos()" class="btn btn-success" id="rango" name="rango">Buscar</button>    
                    </div>
                </form:form>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading"><h2>Resultado de la B&uacute;squeda</h2></div>
            <div class="panel-body">

                <div class=" table-responsive">
                    <table id="institutions" class="table table-striped table-bordered" cellspacing="0" width="100%" >
                        <thead>
                            <tr>
                               <th>Nombre del programa</th>
                                <th>C�digo del programa</th>
                                <th>Nombre de la instituci�n</th>                               
                                <th>Tipo de Formaci�n</th>
                                <th>Detalle</th>
                            </tr>

                        </thead>
                        <tfoot>
                            <tr>
                                <th>Nombre del programa</th>
                                <th>C�digo del programa</th>
                                <th>Nombre de la instituci�n</th>                               
                                <th>Tipo de Formaci�n</th>
                                <th>Detalle</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:if test="${not empty programList}">
                                <c:forEach items="${programList}" var="modulos" begin="0">

                                    <tr> 
                                        <td id="name">${modulos.programName}</td> 
                                        <td>${modulos.programCode}</td>
                                         <td>${modulos.institution}</td>  
                                        <td>${modulos.trainingType}</td>  
                                       

                                        <td>
                                            <button type="button" class="btn btn-info btn-xs"
                                                    
                                                    onclick="loadModuleDetail(${modulos.programCode})">
                                                <i class="fa fa-id-card-o" aria-hidden="true"></i> 
                                                Ver
                                            </button>
                                        </td>    

                                    </tr>   
                                </c:forEach>  
                            </c:if>
                        </tbody> 

                    </table>
                </div>
            </div>
        </div>

        <div class="modal fade" id="detailModal" tabindex="-1"  
             aria-labelledby="creationModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                        <h2>Detalle del Programa</h2>
                    </div>

                    <div class="modal-body" style="height: 100vh">
                        <iframe id="detailArea" width="100%" height="100%" style="border: 0">
                        </iframe>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div> 
        </div>    

        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/bootstrap-confirmation.js"></script>
        <script src="resources/js/jquery.ui.widget.js"></script>
        <script src="resources/js/jquery.iframe-transport.js"></script>
        <script src="resources/js/jquery.fileupload.js"></script>
        <script src="resources/js/bootstrap-toggle.min.js"></script>  
        <script src="resources/js/dataTables.buttons.min.js"></script>
        <script src="resources/js/buttons.flash.min.js"></script>
        <script src="resources/js/jszip.min.js"></script>
        <script src="resources/js/pdfmake.min.js"></script>
        <script src="resources/js/vfs_fonts.js"></script>
        <script src="resources/js/buttons.html5.min.js"></script>
        <script src="resources/js/buttons.print.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/jquery.validate.min.js"></script>
        <script src="resources/js/tooltipster.bundle.js"></script>
        <script src="resources/js/moment.js"></script>
        <script src="resources/js/tooltipster.bundle.js"></script>
        <script src="resources/js/custom-functionalities.js"></script>
        <script>

            $(document).ready(function () {
                
                $('#codigoPrograma').val("")
                
                $("#codigoPrograma").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });

               

                $('#institutions').DataTable({
                    "language": {
                        "url": "resources/js/Spanish.json"
                    }, "initComplete": function (settings, json) {
                        $(".dt-buttons").each(function (index) {
                            console.log(index + ": " + $(this).text());
                            if ($(this).find('.exportar').length === 0) {
                                $(this).prepend('<label class="exportar">Exportar a:&nbsp;&nbsp;</label>');
                            }
                        });
                    },
                    "bFilter": false,
                    "bInfo": false,
                    dom: 'Blfrtip',
                    buttons: [
                        {
                            extend: 'print',
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'COPY',
                            exportOptions: {
                                columns: [0, 1, 2, 3]
                            }
                        },
                        {
                            extend: 'excel',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            titleAttr: 'EXCEL',
                            exportOptions: {
                                columns: [0, 1, 2, 3]
                            }
                        },
                        {
                            extend: 'csv',
                            fieldBoundary: '',
                            text: '<i class="fa fa-file-text-o"></i>',
                            titleAttr: 'CSV',
                            exportOptions: {
                                columns: [0, 1, 2, 3]
                            }
                        },
                        {
                            extend: 'pdf',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            titleAttr: 'PDF',
                            exportOptions: {
                                columns: [0, 1, 2, 3]
                            }
                        }

                    ]

                });
                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Mi�rcoles", "Jueves", "Viernes", "S�bado", "Domingo"],
                    daysShort: ["Dom", "Lun", "Mar", "Mi�", "Jue", "Vie", "S�b", "Dom"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Hoy"
                };
                $('#fechaInicio').datepicker({language: "es", autoclose: true});
                $('#fechaFin').datepicker({language: "es", autoclose: true});
                
             
            });
            
             
                
                
                
                
                
            function loadModuleDetail(id) {


                $('#detailArea').attr("src",
                        'detalle-modulos.htm?id=' + id);
                        
                $('#detailModal').modal('show');
            }

            function validarCampos() {

                var fechaInicio = document.getElementById("fechaInicio").value;
                var fechaFin = document.getElementById("fechaFin").value;
                var nombrePrograma = document.getElementById("nombrePrograma").value;
                var codigoPrograma = document.getElementById("codigoPrograma").value;
                var nombreModulo = document.getElementById("nombreModulo").value;
                var tipoFormacion = document.getElementById("tipoFormacion").value;
                var institucion = document.getElementById("institucion").value;
              
                
                if (
                        (fechaFin.length === 0  || fechaFin === null || /^\s+$/.test(fechaFin)) &&
                        (fechaInicio.length === 0 || fechaInicio === null || /^\s+$/.test(fechaInicio))&&
                        (nombrePrograma.length === 0) &&
                        (codigoPrograma.length === 0 || codigoPrograma === '0') &&
                        (nombreModulo.length === 0) &&
                        (tipoFormacion === '')&&
                        (institucion === '')
                    )    
                   {

                    $("#alert-area").append($("<div class='alert alert-message alert-danger" +
                            " fade in' data-alert><p> Debe seleccionar al menos un filtro </p></div>"));
                    $(".alert-message").delay(3000).fadeOut("slow", function () {
                        $(this).remove();
                    });
                } else  if(moment(fechaInicio,"DD-MM-YYYY").isAfter(moment(fechaFin,"DD-MM-YYYY"))){
                   
                   
                    $("#alert-area").append($("<div class='alert alert-message alert-danger" +
                            " fade in' data-alert><p> Fecha de fin debe ser maryor que fecha de inicio</p></div>"));
                    $(".alert-message").delay(3000).fadeOut("slow", function () {
                        $(this).remove();
                    });
                    
                } 
                
                else {
                    if($('#codigoPrograma').val() === ""){
                        $('#codigoPrograma').val("0")
                    }
                    $('#rangoForm').submit();
                }
            }
        </script>
        <style>
            .buttons-csv{
                display: inline-block;
                background-image:url(resources/images/csv.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;

            }
            .buttons-csv span{
                opacity: 0;
            }
            .buttons-excel{
                display: inline-block;
                background-image:url(resources/images/excel.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;

            }
            .buttons-excel span{
                opacity: 0;
            }
            .buttons-pdf{
                display: inline-block;
                background-image:url(resources/images/pdf.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;

            }
            .buttons-pdf span{
                opacity: 0;
            }
            .buttons-print{
                display: inline-block;
                background-image:url(resources/images/print.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;


            }
            .buttons-print span{
                opacity: 0;

            }
            div.dt-buttons {
                float: right;
                margin-left:10px;
            }

        </style>

    </body>
</html>
