/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ProfitsDto.java
 * Created on: 2017/01/02, 03:07:47 PM
 * Objetivo: Mapear los campos requeridos para una carga de archivos de fosfec
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

/**
 * Transfiere la informacion de los Beneficios Economicos (Archivo Fosfec)
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
public class ProfitDto {

    private String eventType;
    private String ccfCode;
    private String unemployedIDType;
    private String unemployedID;
    private String firstName;
    private String secodName;
    private String firstSurname;
    private String secondSurname;
    private String birthday;
    private String gender;
    private String residentState;
    private String residentCity;
    private String salaryrange;
    private String publicServiceRegistration;
    private String vinculationType;
    private String lastCff;
    private String postulationDateMpc;
    private String healthManager;
    private String retirementPostulation;
    private String retirementManager;
    private String econonomicSubsidyFee;
    private String mpcSavings;
    private String severanceFundAdministrator;
    private String percentageSavings;
    private String feedBonusPostulation;
    private String requirements;
    private String postulationDateCheck;
    private String beneficiary;
    private String applyWithRoute;
    private String settlementDate;
    private String benefitClearedPeriod;
    private String numberLiquidBenefi;
    private String numberMoneyChargesLiquided;
    private String numberDependents;
    private String economicIncentiveSavings;
    private String numbrLiquidBonds;
    private String helthLiquidedAmount;
    private String retirementliquidedAmount;
    private String monetaryLiquidedAmount;
    private String liquidedAssetAmount;
    private String feedBonusLiquidedAmount;
    private String totalValueProfits;
    private String voluntaryWaiver;
    private String waiverDate;
    private String lostBenefit;
    private String lostBenefitDate;
    private String benefitEnd;
    private String benefitEndDate;
    private String writeType;
    private Long cargaId;

    public String getWriteType() {
        return writeType;
    }

    public String getEventType() {
        return eventType;
    }

    public String getCcfCode() {
        return ccfCode;
    }

    public String getUnemployedIDType() {
        return unemployedIDType;
    }

    public String getUnemployedID() {
        return unemployedID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecodName() {
        return secodName;
    }

    public String getFirstSurname() {
        return firstSurname;
    }

    public String getSecondSurname() {
        return secondSurname;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getGender() {
        return gender;
    }

    public String getResidentState() {
        return residentState;
    }

    public String getResidentCity() {
        return residentCity;
    }

    public String getSalaryrange() {
        return salaryrange;
    }

    public String getPublicServiceRegistration() {
        return publicServiceRegistration;
    }

    public String getVinculationType() {
        return vinculationType;
    }

    public String getLastCff() {
        return lastCff;
    }

    public String getPostulationDateMpc() {
        return postulationDateMpc;
    }

    public String getHealthManager() {
        return healthManager;
    }

    public String getRetirementPostulation() {
        return retirementPostulation;
    }

    public String getRetirementManager() {
        return retirementManager;
    }

    public String getEcononomicSubsidyFee() {
        return econonomicSubsidyFee;
    }

    public String getMpcSavings() {
        return mpcSavings;
    }

    public String getSeveranceFundAdministrator() {
        return severanceFundAdministrator;
    }

    public String getPercentageSavings() {
        return percentageSavings;
    }

    public String getFeedBonusPostulation() {
        return feedBonusPostulation;
    }

    public String getRequirements() {
        return requirements;
    }

    public String getPostulationDateCheck() {
        return postulationDateCheck;
    }

    public String getBeneficiary() {
        return beneficiary;
    }

    public String getApplyWithRoute() {
        return applyWithRoute;
    }

    public String getSettlementDate() {
        return settlementDate;
    }

    public String getBenefitClearedPeriod() {
        return benefitClearedPeriod;
    }

    public String getNumberLiquidBenefi() {
        return numberLiquidBenefi;
    }

    public String getNumberMoneyChargesLiquided() {
        return numberMoneyChargesLiquided;
    }

    public String getNumberDependents() {
        return numberDependents;
    }

    public String getEconomicIncentiveSavings() {
        return economicIncentiveSavings;
    }

    public String getNumbrLiquidBonds() {
        return numbrLiquidBonds;
    }

    public String getHelthLiquidedAmount() {
        return helthLiquidedAmount;
    }

    public String getRetirementliquidedAmount() {
        return retirementliquidedAmount;
    }

    public String getMonetaryLiquidedAmount() {
        return monetaryLiquidedAmount;
    }

    public String getLiquidedAssetAmount() {
        return liquidedAssetAmount;
    }

    public String getFeedBonusLiquidedAmount() {
        return feedBonusLiquidedAmount;
    }

    public String getTotalValueProfits() {
        return totalValueProfits;
    }

    public String getVoluntaryWaiver() {
        return voluntaryWaiver;
    }

    public String getWaiverDate() {
        return waiverDate;
    }

    public String getLostBenefit() {
        return lostBenefit;
    }

    public String getLostBenefitDate() {
        return lostBenefitDate;
    }

    public String getBenefitEnd() {
        return benefitEnd;
    }

    public String getBenefitEndDate() {
        return benefitEndDate;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public void setCcfCode(String CcfCode) {
        this.ccfCode = CcfCode;
    }

    public void setUnemployedIDType(String UnemployedIDType) {
        this.unemployedIDType = UnemployedIDType;
    }

    public void setUnemployedID(String UnemployedID) {
        this.unemployedID = UnemployedID;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSecodName(String secodName) {
        this.secodName = secodName;
    }

    public void setFirtSurname(String firstSurname) {
        this.firstSurname = firstSurname;
    }

    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setResidentState(String residentState) {
        this.residentState = residentState;
    }

    public void setResidentCity(String residentCity) {
        this.residentCity = residentCity;
    }

    public void setSalaryrange(String salaryrange) {
        this.salaryrange = salaryrange;
    }

    public void setPublicServiceRegistration(String publicServiceRegistration) {
        this.publicServiceRegistration = publicServiceRegistration;
    }

    public void setVinculationType(String vinculationType) {
        this.vinculationType = vinculationType;
    }

    public void setLastCff(String lastCff) {
        this.lastCff = lastCff;
    }

    public void setPostulationDateMpc(String postulationDateMpc) {
        this.postulationDateMpc = postulationDateMpc;
    }

    public void setHealthManager(String healthManager) {
        this.healthManager = healthManager;
    }

    public void setRetirementPostulation(String retirementPostulation) {
        this.retirementPostulation = retirementPostulation;
    }

    public void setRetirementManager(String retirementManager) {
        this.retirementManager = retirementManager;
    }

    public void setEcononomicSubsidyFee(String econonomicSubsidyFee) {
        this.econonomicSubsidyFee = econonomicSubsidyFee;
    }

    public void setMpcSavings(String MpcSavings) {
        this.mpcSavings = MpcSavings;
    }

    public void setSeveranceFundAdministrator(String severanceFundAdministrator) {
        this.severanceFundAdministrator = severanceFundAdministrator;
    }

    public void setPercentageSavings(String percentageSavings) {
        this.percentageSavings = percentageSavings;
    }

    public void setFeedBonusPostulation(String feedBonusPostulation) {
        this.feedBonusPostulation = feedBonusPostulation;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    public void setPostulationDateCheck(String postulationDateCheck) {
        this.postulationDateCheck = postulationDateCheck;
    }

    public void setBeneficiary(String beneficiary) {
        this.beneficiary = beneficiary;
    }

    public void setApplyWithRoute(String applyWithRoute) {
        this.applyWithRoute = applyWithRoute;
    }

    public void setSettlementDate(String settlementDate) {
        this.settlementDate = settlementDate;
    }

    public void setBenefitClearedPeriod(String benefitClearedPeriod) {
        this.benefitClearedPeriod = benefitClearedPeriod;
    }

    public void setNumberLiquidBenefi(String numberLiquidBenefi) {
        this.numberLiquidBenefi = numberLiquidBenefi;
    }

    public void setNumberMoneyChargesLiquided(String numberMoneyChargesLiquided) {
        this.numberMoneyChargesLiquided = numberMoneyChargesLiquided;
    }

    public void setNumberDependents(String numberDependents) {
        this.numberDependents = numberDependents;
    }

    public void setEconomicIncentiveSavings(String economicIncentiveSavings) {
        this.economicIncentiveSavings = economicIncentiveSavings;
    }

    public void setNumbrLiquidBonds(String numbrLiquidBonds) {
        this.numbrLiquidBonds = numbrLiquidBonds;
    }

    public void setHelthLiquidedAmount(String helthLiquidedAmount) {
        this.helthLiquidedAmount = helthLiquidedAmount;
    }

    public void setRetirementliquidedAmount(String retirementliquidedAmount) {
        this.retirementliquidedAmount = retirementliquidedAmount;
    }

    public void setFirstSurname(String firstSurname) {
        this.firstSurname = firstSurname;
    }

    public void setWriteType(String writeType) {
        this.writeType = writeType;
    }

    public void setMonetaryLiquidedAmount(String monetaryLiquidedAmount) {
        this.monetaryLiquidedAmount = monetaryLiquidedAmount;
    }

    public void setLiquidedAssetAmount(String liquidedAssetAmount) {
        this.liquidedAssetAmount = liquidedAssetAmount;
    }

    public void setFeedBonusLiquidedAmount(String feedBonusLiquidedAmount) {
        this.feedBonusLiquidedAmount = feedBonusLiquidedAmount;
    }

    public void setTotalValueProfits(String totalValueProfits) {
        this.totalValueProfits = totalValueProfits;
    }

    public void setVoluntaryWaiver(String voluntaryWaiver) {
        this.voluntaryWaiver = voluntaryWaiver;
    }

    public void setWaiverDate(String waiverDate) {
        this.waiverDate = waiverDate;
    }

    public void setLostBenefit(String lostBenefit) {
        this.lostBenefit = lostBenefit;
    }

    public void setLostBenefitDate(String lostBenefitDate) {
        this.lostBenefitDate = lostBenefitDate;
    }

    public void setBenefitEnd(String benefitEnd) {
        this.benefitEnd = benefitEnd;
    }

    public void setBenefitEndDate(String benefitEndDate) {
        this.benefitEndDate = benefitEndDate;
    }

    public Long getCargaId() {
        return cargaId;
    }

    public void setCargaId(Long cargaId) {
        this.cargaId = cargaId;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CargaId=").append(cargaId).
            append("~&~Tipo de Documento=").append(unemployedIDType).
            append("~&~N�mero de Documento=").append(unemployedID).
            append("~&~Tipo de Novedad=").append(eventType).
            append("~&~C�digo CCF=").append(ccfCode).
            append("~&~Primer Nombre=").append(firstName).
            append("~&~Segundo Nombre=").append(secodName).
            append("~&~Primer Apellido=").append(firstSurname).
            append("~&~Segundo Apellido=").append(secondSurname).
            append("~&~Fecha de Nacimiento=").append(birthday).
            append("~&~G�nero=").append(gender).
            append("~&~Departamento =").append(residentState).
            append("~&~Municipio=").append(residentCity).
            append("~&~Rango Salarial=").append(salaryrange).
            append("~&~Inscripci�n Servicio de Empleo=").append(publicServiceRegistration).
            append("~&~Tipo Vinculaci�n CCF=").append(vinculationType).
            append("~&~�ltima CCF=").append(lastCff).
            append("~&~Fecha de Postulaci�n MPC=").append(postulationDateMpc).
            append("~&~Administradora Salud=").append(healthManager).
            append("~&~Postula Pensi�n=").append(retirementPostulation).
            append("~&~Administradora Pensi�n=").append(retirementManager).
            append("~&~Subsidio Cuota Monetaria=").append(econonomicSubsidyFee).
            append("~&~Ahorro Cesant�as MPC=").append(mpcSavings).
            append("~&~Administradora Fondos Cesant�as=").append(severanceFundAdministrator).
            append("~&~Porcentaje Ahorrado Cesant�as=").append(percentageSavings).
            append("~&~Postula Bono Alimentaci�n=").append(feedBonusPostulation).
            append("~&~Verificaci�n Requisitos=").append(requirements).
            append("~&~Fecha Verificaci�n Postulaci�n=").append(postulationDateCheck).
            append("~&~Beneficiario del Mecanismo=").append(beneficiary).
            append("~&~Cumple Ruta de Empleabilidad=").append(applyWithRoute).
            append("~&~Fecha Liquidaci�n Beneficio=").append(settlementDate).
            append("~&~Periodo Beneficio Liquidado=").append(benefitClearedPeriod).
            append("~&~N�mero Beneficios Liquidados=").append(numberLiquidBenefi).
            append("~&~N�mero Cuotas Liquidadas=").append(numberMoneyChargesLiquided).
            append("~&~N�mero Personas A Cargo=").append(numberDependents).
            append("~&~N�mero Cuotas Incentivo=").append(economicIncentiveSavings).
            append("~&~N�mero Bonos Liquidados=").append(numbrLiquidBonds).
            append("~&~Monto Liquidado Salud=").append(helthLiquidedAmount).
            append("~&~Monto Liquidado Pensi�n=").append(retirementliquidedAmount).
            append("~&~Monto Liquidado Cuota=").append(monetaryLiquidedAmount).
            append("~&~Monto Liquidado Cesant�as=").append(liquidedAssetAmount).
            append("~&~Monto Liquidado Alimentaci�n=").append(feedBonusLiquidedAmount).
            append("~&~Valor Total Prestaciones=").append(totalValueProfits).
            append("~&~Renuncia Voluntaria=").append(voluntaryWaiver).
            append("~&~Fecha de Renuncia=").append(waiverDate).
            append("~&~Perdida de Beneficio=").append(lostBenefit).
            append("~&~Fecha Perdida de Beneficio=").append(lostBenefitDate).
            append("~&~Terminaci�n Beneficio=").append(benefitEnd).
            append("~&~Fecha de Terminaci�n Beneficio=").append(benefitEndDate);
        return stringBuilder.toString();
    }

}
