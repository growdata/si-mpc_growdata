package com.gml.simpc.loader.batch;

import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import com.gml.simpc.loader.thread.ThreadLocalValueList;
import com.gml.simpc.loader.utilities.ValueListTestService;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-batch.xml",
    "classpath:batch_jobs/curriculumVitae/basicInformation/basicInformationJob.xml"})
public class BasicInformationLoadJobTest {

    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER = 
        Logger.getLogger(BasicInformationLoadJobTest.class);
    public static final ThreadLocal<String> THREAD = new ThreadLocal<>();

    /**
     * Servicio para ejecutar jobs en modo pruebas
     */
    @Autowired
    JobLauncherTestUtils jobLauncherTestUtils;

    
    /**
     * Test �nico para probar carga de hojas de vida - datos b�sicos
     */
    @Test
    public void succesBasicInformacion() throws Exception {
        System.out.println("Running BasicInformationLoadJobTest - succesBasicInformacion");

        //Inicializamos local thread
        ThreadLocalJob.init();
        
         //Inicializamos listas de valores
        ValueListTestService valService = new ValueListTestService();
        valService.updateAll();
        ThreadLocalValueList.set(valService.getValueList());

        // Inicializamos el job.
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath",
            "file:loadTestFiles/curriculum vitae/basicInformation/" +
            "basicInformationTestFile.txt");

        // Lanzamos el job.
        JobExecution jobExecution = jobLauncherTestUtils.
            launchJob(jobParametersBuilder.toJobParameters());

        for (Throwable t : jobExecution.getAllFailureExceptions()) {
            t.printStackTrace();
        }

        JobResultDto jobResult = ThreadLocalJob.getResult();
        System.out.println(jobResult.toString());

        if (!jobResult.getErrorList().isEmpty()) {
            System.out.println(jobResult.getErrorsString());
        }

        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().
            getExitCode());
         Assert.assertTrue(jobResult.getErrorList().isEmpty());

        ThreadLocalJob.delete();
    }
    
    
    /**
     * Test �nico para probar carga de hojas de vida - datos b�sicos
     */
    @Test
    public void civilStatusError() throws Exception {

        System.out.println("Running BasicInformationLoadJobTest - civilStatusError");

        //Inicializamos local thread
        ThreadLocalJob.init();
        
         //Inicializamos listas de valores
        ValueListTestService valService = new ValueListTestService();
        valService.updateAll();
        ThreadLocalValueList.set(valService.getValueList());

        // Inicializamos el job.
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath",
            "file:loadTestFiles/curriculum vitae/basicInformation/" +
            "basicInformationTestFile-civilStatusError.txt");

        // Lanzamos el job.
        JobExecution jobExecution = jobLauncherTestUtils.
            launchJob(jobParametersBuilder.toJobParameters());

        for (Throwable t : jobExecution.getAllFailureExceptions()) {
            t.printStackTrace();
        }

        JobResultDto jobResult = ThreadLocalJob.getResult();
        System.out.println(jobResult.toString());

        if (!jobResult.getErrorList().isEmpty()) {
            System.out.println(jobResult.getErrorsString());
        }

        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().
            getExitCode());
        Assert.assertTrue(jobResult.getErrorList().size() != 2);

        ThreadLocalJob.delete();
    }
    
    /**
     * Test �nico para probar carga de hojas de vida - datos b�sicos
     */
    @Test
    public void ccfError() throws Exception {

        System.out.println("Running BasicInformationLoadJobTest - ccfError");

        //Inicializamos local thread
        ThreadLocalJob.init();
        
         //Inicializamos listas de valores
        ValueListTestService valService = new ValueListTestService();
        valService.updateAll();
        ThreadLocalValueList.set(valService.getValueList());

        // Inicializamos el job.
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath",
            "file:loadTestFiles/curriculum vitae/basicInformation/" +
            "basicInformationTestFile-ccfError.txt");

        // Lanzamos el job.
        JobExecution jobExecution = jobLauncherTestUtils.
            launchJob(jobParametersBuilder.toJobParameters());

        for (Throwable t : jobExecution.getAllFailureExceptions()) {
            t.printStackTrace();
        }

        JobResultDto jobResult = ThreadLocalJob.getResult();
        System.out.println(jobResult.toString());

        if (!jobResult.getErrorList().isEmpty()) {
            System.out.println(jobResult.getErrorsString());
        }

        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().
            getExitCode());
        Assert.assertTrue(jobResult.getErrorList().size() != 2);

        ThreadLocalJob.delete();
    }
    
    
    
}
