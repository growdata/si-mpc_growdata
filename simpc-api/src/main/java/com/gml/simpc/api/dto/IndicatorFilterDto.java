/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IndicatorFilterDto.java
 * Created on: 2016/12/27, 11:32:00 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

import com.gml.simpc.api.enums.GraphicType;
import com.gml.simpc.api.enums.IndicatorGroupFunctiontType;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * DTO que contiene los filtros para indicadores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class IndicatorFilterDto {

    /**
     * Nombre de la tabla donde consultar el indicador.
     */
    private String tableName;
    /**
     * Nombre del campo que representa el indicador.
     */
    private String fieldName;
    /**
     * Valor con el que se evalua el indicador.
     */
    private String value;
    /**
     * Campo de fecha para filtrar en tiempo.
     */
    private String dateField;
    /**
     * Indica si tiene m&acute;s de un per&iacute;odo.
     */
    private boolean hasManyPeriods;
    /**
     * N&uacute;mero de per&iacute;odos.
     */
    private int numberOfPeriods;
    /**
     * Nombres de las columnas para consulta.
     */
    private final List<String> columns;
    /**
     * T&iacute;tulos de las columnas para presentaci&oacute;n.
     */
    private final List<String> titles;
    /**
     * Fecha inicial para filtro.
     */
    private Date startDate;
    /**
     * Fecha final para filtro.
     */
    private Date endDate;
    /**
     * Tipo de gr&aacute;fico.
     */
    private GraphicType type;
    /**
     * Label 1 para gr&aacute;ficas de pie.
     */
    private String label1;
    /**
     * Label 2 para gr&aacute;ficas de pie.
     */
    private String label2;
    
    /**
     * Campo de agrupación
     */
    private String groupField;
    
     /**
     * Funcion de agregación
     */
    private IndicatorGroupFunctiontType agregateFunction;

    /**
     * Constructor por defecto.
     */
    public IndicatorFilterDto() {
        this.columns = new LinkedList<>();
        this.titles = new LinkedList<>();
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDateField() {
        return dateField;
    }

    public void setDateField(String dateField) {
        this.dateField = dateField;
    }

    public boolean hasManyPeriods() {
        return hasManyPeriods;
    }

    public void setHasManyPeriods(boolean hasManyPeriods) {
        this.hasManyPeriods = hasManyPeriods;
    }

    public int getNumberOfPeriods() {
        return numberOfPeriods;
    }

    public void setNumberOfPeriods(int numberOfPeriods) {
        this.numberOfPeriods = numberOfPeriods;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<String> getColumns() {
        return columns;
    }

    public List<String> getTitles() {
        return titles;
    }

    public GraphicType getType() {
        return type;
    }

    public void setType(GraphicType type) {
        this.type = type;
    }

    public String getLabel1() {
        return label1;
    }

    public void setLabel1(String label1) {
        this.label1 = label1;
    }

    public String getLabel2() {
        return label2;
    }

    public void setLabel2(String label2) {
        this.label2 = label2;
    }

    public String getGroupField() {
        return groupField;
    }

    public void setGroupField(String groupField) {
        this.groupField = groupField;
    }

    public IndicatorGroupFunctiontType getAgregateFunction() {
        return agregateFunction;
    }

    public void setAgregateFunction(IndicatorGroupFunctiontType agregateFunction) {
        this.agregateFunction = agregateFunction;
    }
    
    
    /**
     * Aumenta el n&uacute;mero de per&iacute;odos aplicandole un multiplicador.
     *
     * @param multiplier
     */
    public void multiplyNumberOfPeriods(int multiplier) {
        this.numberOfPeriods = this.numberOfPeriods * multiplier;
    }

    @Override
    public String toString() {
        return "IndicatorFilterDto{" + "tableName=" + tableName + 
                ", fieldName=" + fieldName + ", value=" + value + 
                ", dateField=" + dateField + ", hasManyPeriods=" + 
                hasManyPeriods + ", numberOfPeriods=" + numberOfPeriods + 
                ", columns=" + columns + ", titles=" + titles + ", startDate=" 
                + startDate + ", endDate=" + endDate + ", type=" + type + ", label1=" 
                + label1 + ", label2=" + label2 + ", groupField=" + groupField 
                + ", agregateFunction=" + agregateFunction + '}';
    }
    
    
}
