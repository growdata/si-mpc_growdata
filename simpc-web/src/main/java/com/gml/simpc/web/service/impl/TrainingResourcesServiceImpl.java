/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: SupplierBankServiceImpl.java
 * Created on: 2017/01/12, 03:14:16 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.dao.TrainingResourcesDao;
import com.gml.simpc.api.dto.TrainingDto;
import com.gml.simpc.api.services.TrainingResourcesService;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service
public class TrainingResourcesServiceImpl implements TrainingResourcesService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER
            = Logger.getLogger(TrainingResourcesServiceImpl.class);

    /**
     * Dao para manejar la persistencia de capacitaciones.
     */
    @Autowired
    private TrainingResourcesDao trainingResourcesDao;

    /**
     * Genera lista de costos por ccf.
     *
     * @param ccf
     * @param year
     * @param month
     * @param costType
     * @return
     */
    @Override
    public List<TrainingDto> getCostsByCcf(String ccf, String year, String month,
            String costType) {

        LOGGER.info("VALOR DEL CCF " + ccf + "anio" + year + "mes" + month
                + "tipo de costo" + costType);
        return this.trainingResourcesDao.getCostsByCcf(ccf, year, month);

    }

    /**
     * Genera lista de costos por periodo.
     * 
     * @param ccf
     * @param year
     * @param month
     * @return 
     */
    @Override
    public List<TrainingDto> getCostsByPeriod(String ccf, String year,
            String month) {

        LOGGER.info("VALOR DEL CCF " + ccf + "anio" + year + "mes" + month);
        return trainingResourcesDao.getCostsByPeriod(ccf, year, month);
    }

}
