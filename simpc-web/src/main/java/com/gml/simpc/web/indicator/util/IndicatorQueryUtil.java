/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IndicatorQueryUtil.java
 * Created on: 2017/01/03, 11:36:41 AM
 * Project: MPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.indicator.util;

import com.gml.simpc.api.dto.IndicatorFilterDto;
import com.gml.simpc.api.enums.IndicatorGroupFunctiontType;

/**
 * Utileria para crear los querys de indicadores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public final class IndicatorQueryUtil {

    /**
     * Constructor privado para impedir instancias de la clase.
     */
    private IndicatorQueryUtil() {
    }

    /**
     * Constuye el query que obtiene la data para pintar el gr&aacute;fico.
     *
     * @param filter
     *
     * @return
     */
    public static String buildGraphicDataQuery(IndicatorFilterDto filter) {
        StringBuilder query = new StringBuilder("SELECT ");

        //Se adiciona a la consulta agrupaci�n por periodo
        if (filter.hasManyPeriods()) {
            query.append("TO_CHAR(");
            query.append(filter.getDateField());
            query.append(", 'yyyyMM') X ");
        }
        
        //Si as� esta configurado, se adiciona a la consulta campo particular de agrupaci�n
        if (filter.getGroupField()!= null){
            query.append(" , ");
            query.append(filter.getGroupField());
            query.append(" group_field ");
        }
        
        //Se adiciona funci�n de agregaci�n
        
        if (filter.getAgregateFunction().equals(IndicatorGroupFunctiontType.S)){
            query.append(", SUM(CASE WHEN ");
            query.append(filter.getFieldName());
            query.append(" = ? THEN 1 ELSE 0 END) Y, SUM(CASE WHEN ");
            query.append(filter.getFieldName());
            query.append(" <> ? THEN 1 ELSE 0 END) Z  ");
        }
        if (filter.getAgregateFunction().equals(IndicatorGroupFunctiontType.C)){
            query.append(" , count(*) Y");
            query.append(" , 0 Z");
        }
        
        query.append(" FROM ");
        query.append(filter.getTableName());
        query.append(" WHERE ");
        
        addDefaultQuerySection(query, filter);

        if (filter.hasManyPeriods()) {
            query.append("GROUP BY TO_CHAR(");
            query.append(filter.getDateField());
            query.append(", 'yyyyMM') ");
        }
        if (filter.getGroupField()!= null){
            query.append(" , ");
            query.append(filter.getGroupField());
        }

        return query.toString();
    }

    /**
     * Construye el query que obtiene la informaci&oacute;n que acompan&ntilde;a
     * las gr&aacute;ficas.
     *
     * @param filter
     *
     * @return
     */
    public static String buildTableDataQuery(IndicatorFilterDto filter) {
        StringBuilder query = new StringBuilder("SELECT ");

        for (int i = 0; i < filter.getColumns().size(); i++) {
            if (i > 0) {
                query.append(", ");
            }

            query.append(filter.getColumns().get(i));
        }

        query.append(" FROM ");
        query.append(filter.getTableName());
        query.append(" WHERE ");
        addDefaultQuerySection(query, filter);

        if (filter.getValue()!=null){
            query.append(" AND  ");
            query.append(filter.getFieldName());
            query.append(" = ?  ");
        }
        return query.toString();
    }

    /**
     * Agrega la secci&oacute;n por defecto a los querys.
     *
     * @param query
     * @param filter
     */
    private static void addDefaultQuerySection(StringBuilder query,
        IndicatorFilterDto filter) {
        
        query.append(filter.getDateField());
        query.append(" > ? ");

        if (filter.getEndDate() != null) {
            query.append("AND ");
            query.append(filter.getDateField());
            query.append(" < ? ");
        }
    }
}
