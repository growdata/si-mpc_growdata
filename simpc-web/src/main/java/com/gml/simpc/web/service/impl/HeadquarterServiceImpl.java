/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserServiceImpl.java
 * Created on: 2016/10/19, 02:11:43 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.entity.Headquarter;
import com.gml.simpc.api.entity.Institution;
import com.gml.simpc.api.services.HeadquarterService;
import com.gml.simpc.commons.annotations.AnnotationInterceptor;
import com.gml.simpc.web.repository.HeadquarterRepository;
import com.gml.simpc.web.utilities.ValueListService;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.gml.simpc.api.utilities.Util.likeValue;
import static com.gml.simpc.api.utilities.Util.nullValue;
import static com.gml.simpc.api.utilities.Util.nullValue;
import static com.gml.simpc.api.utilities.Util.nullValue;
import static com.gml.simpc.api.utilities.Util.nullValue;

/**
 * Servicio para administrar sedes.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Service(value = "headquarterService")
public class HeadquarterServiceImpl implements HeadquarterService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(HeadquarterServiceImpl.class);

    /**
     * Servicio para listas de valor.
     */
    @Autowired
    private ValueListService valueListService;

    /**
     * Repositorio para las sedes.
     */
    @Autowired
    private HeadquarterRepository headquarterRepository;

    /**
     * Guarda sedes.
     * 
     * @param headquarter 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Guardar",
        entityTableName = "SEDES")
    @Override
    @Transactional
    public void save(Headquarter headquarter) {
        LOGGER.info("Ejecutando metodo [ save de sedes]");

        if (headquarter.getId() != null) {
            headquarter.setId(headquarter.getId());
            headquarter.setCodigo(headquarter.getCodigo());
        } else {
            long idSede = headquarterRepository.headquarterSequence();
            String Divipola = valueListService.getCitiesById().
                get(headquarter.getCity().getId()).getDivipola();
            headquarter.setId(idSede);
            headquarter.setCodigo(Divipola + idSede);
        }

        if (headquarter.isPrincipal()) {
            List<Headquarter> sedes = this.headquarterRepository.
                findByInstitutionId(headquarter.getInstitutionId());
            for (Headquarter sede : sedes) {
                if (!sede.getId().equals(headquarter.getId()) &&
                    sede.isPrincipal()) {
                    sede.setPrincipal(false);
                    this.headquarterRepository.saveAndFlush(sede);
                }
            }
        }else{
            headquarter.setPrincipal(false);
        }

        this.headquarterRepository.saveAndFlush(headquarter);
    }

    /**
     * Elimina sedes.
     * 
     * @param headquarter 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Borrar",
        entityTableName = "SEDES")
    @Override
    public void remove(Headquarter headquarter) {
        LOGGER.info("Ejecutando metodo [ remove de sedes ]");
        this.headquarterRepository.delete(headquarter);
    }

    /**
     * Elimina sedes.
     * 
     * @param headquarters 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Borrar Lista de Sedes",
        entityTableName = "SEDES")
    @Override
    public void remove(List<Headquarter> headquarters) {
        LOGGER.info("Ejecutando metodo [ remove de sedes ]");
        this.headquarterRepository.delete(headquarters);
    }

    /**
     * Generar sedes.
     * 
     * @return 
     */
    @Override
    public List<Headquarter> getAll() {
        LOGGER.info("Ejecutando metodo [ getAll de sedes ]");
        return this.headquarterRepository.findAll();
    }

    /**
     * Encuentra lista de sedes por instituci&oacute;n.
     * 
     * @param institution
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName =
        "Consultar Por Institución",
        entityTableName = "SEDES")
    @Override
    public List<Headquarter> findByInstitution(Institution institution) {
        LOGGER.info("Ejecutando metodo [ findByInstitution de sedes ]");
        return this.headquarterRepository.
            findByInstitutionId(institution.getId());
    }

    /**
     * Genera listado de sedes por filtros.
     * 
     * @param headquarter
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", entityTableName = "SEDES",
        methodName = "Consultar por filtros de Sede")
    @Override
    public List<Headquarter> findByFilters(Headquarter headquarter) {
        LOGGER.info("Ejecutando metodo [ findByFilters de sedes ]");
        return this.headquarterRepository.
            findByFilters(likeValue(headquarter.getName()),
                nullValue(headquarter.getState().getId()),
                nullValue(headquarter.getCity().getId()),
                headquarter.getInstitutionId());
    }

    /**
     * Genera listado de sedes por id.
     * 
     * @param id
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", entityTableName = "SEDES",
        methodName = "Consultar por Id")
    @Override
    public Headquarter findById(Long id) {
        return this.headquarterRepository.findOne(id);
    }

    /**
     * Genera sede principar por instituci&ocute;n.
     * 
     * @param institution
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", entityTableName = "SEDES",
        methodName = "Consultar las sedes Principales por institución")
    @Override
    public Headquarter findPrincipalByInstitution(Institution institution) {
        return this.headquarterRepository.
            findByInstitutionIdAndPrincipal(institution.getId(), true).get(0);
    }
}
