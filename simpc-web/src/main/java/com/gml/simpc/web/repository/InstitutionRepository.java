
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserRepository.java
 * Created on: 2016/10/19, 02:03:51 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository;

import com.gml.simpc.api.entity.Institution;
import com.gml.simpc.api.enums.ApprovalInstitutionStatus;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repositorio para instituciones.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Transactional
public interface InstitutionRepository
        extends JpaRepository<Institution, Long> {

    /**
     * Este query filtra las instituciones.
     *
     * @param institutionType
     * @param institutionName
     * @param approvalStatus
     *
     * @return
     */
    @Query(value = "SELECT * FROM instituciones WHERE tipo_institucion_id "
            + "LIKE ?1 AND estado_aprobacion LIKE ?2 AND UPPER(nombre_institucion) LIKE UPPER(?3)",
            nativeQuery = true)
    List<Institution> findByFilters(String institutionType, String approvalStatus,
            String institutionName);

    /**
     * Este query filtra las instituciones aprobadas con fechas de registro y
     * fechas de aprobación.
     *
     * @param institutionName
     * @param institutionType
     * @param regisCcf
     * @param approDateIni
     * @param approDateFin
     * @param regisDateIni
     * @param regisDateFin
     * @return
     */
    @Query(value = "SELECT * FROM INSTITUCIONES I "
            + "INNER JOIN USUARIOS U ON U.ID=I.ID_USUARIO_MODIFICACION "
            + "WHERE I.ESTADO = 'A' AND ESTADO_APROBACION = 'A' "
            + "AND UPPER(I.NOMBRE_INSTITUCION) LIKE "
            + "UPPER(?1) AND I.TIPO_INSTITUCION_ID LIKE ?2 AND "
            + "U.CAJA_COMPENSACION LIKE ?3 AND I.FECHA_APROBACION BETWEEN "
            + "?4 AND ?5 AND I.FECHA_CREACION BETWEEN ?6 "
            + "AND ?7",
            nativeQuery = true)
    List<Institution> findApprovedInstitutionsAllDates(String institutionName,
            String institutionType, String regisCcf, Date approDateIni,
            Date approDateFin, Date regisDateIni, Date regisDateFin);

    /**
     * Este query filtra las instituciones aprobadas con fechas de aprobación.
     *
     * @param institutionName
     * @param institutionType
     * @param regisCcf
     * @param approDateIni
     * @param approDateFin
     * @return
     */
    @Query(value = "SELECT * FROM INSTITUCIONES I "
            + "INNER JOIN USUARIOS U ON U.ID=I.ID_USUARIO_MODIFICACION "
            + "WHERE I.ESTADO = 'A' AND ESTADO_APROBACION = 'A' "
            + "AND UPPER(I.NOMBRE_INSTITUCION) LIKE "
            + "UPPER(?1) AND I.TIPO_INSTITUCION_ID LIKE ?2 AND "
            + "U.CAJA_COMPENSACION LIKE ?3 AND I.FECHA_APROBACION BETWEEN ?4 "
            + "AND ?5",
            nativeQuery = true)
    List<Institution> findApprovedInstitutionsapproDates(String institutionName,
            String institutionType, String regisCcf, Date approDateIni,
            Date approDateFin);

    /**
     * Este query filtra las instituciones aprobadas con fechas de registro.
     *
     * @param institutionName
     * @param institutionType
     * @param regisCcf
     * @param regisDateIni
     * @param regisDateFin
     * @return
     */
    @Query(value = "SELECT * FROM INSTITUCIONES I "
            + "INNER JOIN USUARIOS U ON U.ID=I.ID_USUARIO_MODIFICACION "
            + "WHERE I.ESTADO = 'A' AND ESTADO_APROBACION = 'A' "
            + "AND UPPER(I.NOMBRE_INSTITUCION) LIKE "
            + "UPPER(?1) AND I.TIPO_INSTITUCION_ID LIKE ?2 AND "
            + "U.CAJA_COMPENSACION LIKE ?3 AND I.FECHA_CREACION BETWEEN ?4 "
            + "AND ?5",
            nativeQuery = true)
    List<Institution> findApprovedInstitutionsRegisDates(String institutionName,
            String institutionType, String regisCcf, Date regisDateIni,
            Date regisDateFin);

    /**
     * Este query filtra las instituciones aprobadas sin fechas.
     *
     * @param institutionName
     * @param institutionType
     * @param regisCcf
     * @return
     */
    @Query(value = "SELECT * FROM INSTITUCIONES I "
            + "INNER JOIN USUARIOS U ON U.ID=I.ID_USUARIO_MODIFICACION "
            + "WHERE I.ESTADO = 'A' AND ESTADO_APROBACION = 'A' "
            + "AND UPPER(I.NOMBRE_INSTITUCION) LIKE "
            + "UPPER(?1) AND I.TIPO_INSTITUCION_ID LIKE ?2 AND "
            + "U.CAJA_COMPENSACION LIKE ?3 ",
            nativeQuery = true)
    List<Institution> findApprovedInstitutionsNotDates(String institutionName,
            String institutionType, String regisCcf);

    /**
     * Este query actualiza el campo estado de aprobación
     *
     * @param estado
     * @param updateUserId
     * @param today
     * @param idInstitution
     */
    @Modifying
    @Query(value = "UPDATE instituciones SET estado_aprobacion = ?1, "
            + "id_usuario_modificacion = ?2, fecha_modificacion = ?3, "
            + "fecha_aprobacion = ?3 WHERE id = ?4", nativeQuery = true)
    void updateApprovalState(char estado, Long updateUserId, Date today,
            Long idInstitution);

    /**
     * Este query actualiza el campo de estado
     *
     * @param estado
     * @param updateUserId
     * @param today
     * @param idInstitution
     */
    @Modifying
    @Query(value = "UPDATE instituciones SET estado = ?1, "
            + "id_usuario_modificacion = ?2, fecha_modificacion = ?3, "
            + "fecha_aprobacion = ?3 WHERE id = ?4", nativeQuery = true)
    void updateState(char estado, Long updateUserId, Date today,
            int idInstitution);

    /**
     * Este query activa por primera ves una institución
     *
     * @param status
     * @param approvalStatus
     * @param updateUserId
     * @param today
     * @param idInstitution
     */
    @Modifying
    @Query(value = "UPDATE instituciones SET estado = ?1, "
            + "estado_aprobacion = ?2, id_usuario_modificacion = ?3, "
            + "fecha_aprobacion = ?4 WHERE id = ?5", nativeQuery = true)
    void activateInstitution(String status, String approvalStatus,
            Long updateUserId, Date today, Long idInstitution);

    /**
     * Consulta instituciones por estado.
     *
     * @param status
     *
     * @return
     */
    List<Institution> findByApprovalStatus(ApprovalInstitutionStatus status);

    /**
     * M&eacute;todo que filtra por estado.
     *
     * @param status
     *
     * @return
     */
    @Query(value = "SELECT ins.id, ins.correo_electronico, "
            + "ins.nombre_institucion, ins.tipo_institucion_id, "
            + "ins.naturaleza_juridica, ins.nit, ins.origen, ins.numero_telefono, "
            + "ins.estado, ins.digito_verificacion, ins.vencimiento_certificacion, "
            + "ins.fecha_creacion, ins.fecha_modificacion, ins.id_usuario_creacion, "
            + "ins.id_usuario_modificacion, NULL AS CERTIFICACION_CALIDAD "
            + "FROM instituciones ins WHERE estado = ?1",
            nativeQuery = true)
    List<Institution> searchInstitutionsByStatus(String status);

    /**
     * Actualiza la instituci&oacute;n con certificado de calidad.
     *
     * @param institutionType
     * @param institutionName
     * @param docType
     * @param docNumber
     * @param dv
     * @param origin
     * @param legalNature
     * @param contactPhone
     * @param contactEmail
     * @param status
     * @param approvalStatus
     * @param qualityCertificateExpiration
     * @param qualityCertificate
     * @param modificationUser
     * @param certificationType
     * @param id
     */
    @Modifying
    @Query(nativeQuery = true, value = "UPDATE instituciones SET "
            + "tipo_institucion_id = ?1, nombre_institucion = ?2, "
            + "tipo_documento = ?3, nit = ?4, "
            + "digito_verificacion = DECODE(?5, -1, NULL, ?5), "
            + "origen = ?6, naturaleza_juridica = ?7, numero_telefono = ?8, "
            + "correo_electronico = ?9, estado = ?10, estado_aprobacion = ?11, "
            + "vencimiento_certificacion = ?12, certificacion_calidad = ?13,  "
            + "fecha_modificacion = CURRENT_TIMESTAMP, id_usuario_modificacion = ?14, "
            + "tipo_certificacion = ?15 WHERE id = ?16")
    void updateInstitution(Long institutionType, String institutionName,
            Long docType, String docNumber, Integer dv, String origin,
            Integer legalNature, String contactPhone, String contactEmail,
            String status, String approvalStatus, Date qualityCertificateExpiration,
            byte[] qualityCertificate, Long modificationUser, Long certificationType,
            Long id);

    /**
     * Actualiza la instituci&oacute;n sin certificado de calidad.
     *
     * @param institutionType
     * @param institutionName
     * @param docType
     * @param docNumber
     * @param dv
     * @param origin
     * @param legalNature
     * @param contactPhone
     * @param contactEmail
     * @param status
     * @param approvalStatus
     * @param modificationUser
     * @param id
     * @param certificationType
     */
    @Modifying
    @Query(nativeQuery = true, value = "UPDATE instituciones SET "
            + "tipo_institucion_id = ?1, nombre_institucion = ?2, "
            + "tipo_documento = ?3, nit = ?4, "
            + "digito_verificacion = DECODE(?5, -1, NULL, ?5), "
            + "origen = ?6, naturaleza_juridica = ?7, numero_telefono = ?8, "
            + "correo_electronico = ?9, estado = ?10, estado_aprobacion = ?11, "
            + "fecha_modificacion = CURRENT_TIMESTAMP, id_usuario_modificacion = ?12, "
            + "tipo_certificacion = ?14 WHERE id = ?13")
    void updateInstitution(Long institutionType, String institutionName,
            Long docType, String docNumber, Integer dv, String origin,
            Integer legalNature, String contactPhone, String contactEmail,
            String status, String approvalStatus, Long modificationUser, Long id,
            Long certificationType);

    /**
     * Actualiza la instituci&oacute;n con certificado de calidad.
     *
     * @param institutionType
     * @param institutionName
     * @param docType
     * @param docNumber
     * @param dv
     * @param origin
     * @param legalNature
     * @param contactPhone
     * @param contactEmail
     * @param status
     * @param approvalStatus
     * @param qualityCertificateExpiration
     * @param modificationUser
     * @param id
     * @param certificationType
     */
    @Modifying
    @Query(nativeQuery = true, value = "UPDATE instituciones SET "
            + "tipo_institucion_id = ?1, nombre_institucion = ?2, "
            + "tipo_documento = ?3, nit = ?4, "
            + "digito_verificacion = DECODE(?5, -1, NULL, ?5), "
            + "origen = ?6, naturaleza_juridica = ?7, numero_telefono = ?8, "
            + "correo_electronico = ?9, estado = ?10, estado_aprobacion = ?11, "
            + "vencimiento_certificacion = ?12, "
            + "fecha_modificacion = CURRENT_TIMESTAMP, id_usuario_modificacion = ?13, "
            + "tipo_certificacion = ?15 WHERE id = ?14")
    void updateInstitution(Long institutionType, String institutionName,
            Long docType, String docNumber, Integer dv, String origin,
            Integer legalNature, String contactPhone, String contactEmail,
            String status, String approvalStatus, Date qualityCertificateExpiration,
            Long modificationUser, Long id, Long certificationType);

}
