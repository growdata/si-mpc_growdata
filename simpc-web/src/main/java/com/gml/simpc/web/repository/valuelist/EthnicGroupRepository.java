/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: EthnicGroupRepository.java
 * Created on: 2017/01/23, 02:17:05 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository.valuelist;

import com.gml.simpc.api.entity.valuelist.EthnicGroup;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface EthnicGroupRepository 
    extends JpaRepository<EthnicGroup, Long> {

}