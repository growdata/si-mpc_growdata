/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IndicatorFilterBuilderTest.java
 * Created on: 2017/02/08, 09:58:17 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.indicator;

import com.gml.simpc.api.dto.IndicatorFilterDto;
import com.gml.simpc.api.dto.PeriodDto;
import com.gml.simpc.api.entity.Indicator;
import com.gml.simpc.api.entity.IndicatorGraphic;
import com.gml.simpc.api.entity.exeption.UserException;
import com.gml.simpc.api.enums.Periodicity;
import java.util.Calendar;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static com.gml.simpc.web.indicator.IndicatorFilterBuilder.buildIndicatorFilter;
import static org.junit.Assert.fail;

/**
 * Test para probar <b>IndicatorFilterBuilder</b>.
 *
 * @author Manuel Mart�nez
 */
@RunWith(JUnit4.class)
public class IndicatorFilterBuilderTest extends Assert {

    /**
     * Se espera un UserException cuando no se env&iacute;a per&iacute;odo para
     * construcci&oacute;n del filtro.
     *
     * @throws UserException
     */
    @Test(expected = UserException.class)
    public void case1_nullPeriod() throws UserException {
        buildIndicatorFilter(null, null);
    }

    /**
     * Se espera un UserException cuando no se env&iacute;a gr&aacute;fica para
     * construcci&oacute;n del filtro.
     *
     * @throws UserException
     */
    @Test(expected = UserException.class)
    public void case2_nullGraphic() throws UserException {
        buildIndicatorFilter(new PeriodDto(), null);
    }

    /**
     * Se espera un UserException cuando no se env&iacute;a indicador para
     * construcci&oacute;n del filtro.
     *
     * @throws UserException
     */
    @Test(expected = UserException.class)
    public void case3_nullIndicator() throws UserException {
        buildIndicatorFilter(new PeriodDto(), new IndicatorGraphic());
    }

    /**
     * Verifica que se validen las variables mal configuradas.
     *
     * @throws UserException
     */
    @Test(expected = UserException.class)
    public void case4_malformedVar() throws UserException {
        IndicatorGraphic graphic = new IndicatorGraphic();
        Indicator indicator = new Indicator();
        // Variable mal formada.
        indicator.setVar("x+++*");
        graphic.setIndicator(indicator);

        buildIndicatorFilter(new PeriodDto(), graphic);
    }

    /**
     * Verifica que se valide la variable de fecha mal configurada.
     *
     * @throws UserException
     */
    @Test(expected = UserException.class)
    public void case5_malformedDateVar() throws UserException {
        IndicatorGraphic graphic = new IndicatorGraphic();
        Indicator indicator = new Indicator();
        indicator.setVar("table1.field1=A");
        graphic.setIndicator(indicator);
        // Variable mal formada.
        graphic.setDateVar("x+++*");

        buildIndicatorFilter(new PeriodDto(), graphic);
    }

    /**
     * Verifica que se valide las columnas para presentaci&oacute;n esten bien
     * formadas.
     *
     * @throws UserException
     */
    @Test(expected = UserException.class)
    public void case6_malformedColumns() throws UserException {
        IndicatorGraphic graphic = new IndicatorGraphic();
        Indicator indicator = new Indicator();
        indicator.setVar("table1.field1=A");
        // Variable mal formada.
        indicator.setColumns("x+++*");
        graphic.setIndicator(indicator);
        graphic.setDateVar("date_field");

        buildIndicatorFilter(new PeriodDto(), graphic);
    }

    /**
     * Verifica que al iniciar la construcci�n del filtro se obtenga:
     * <ul>
     * <li>Nombre de la tabla</li>
     * <li>Nombre del campo</li>
     * <li>Valor a verificar</li>
     * </ul>
     */
    @Test
    public void case7_initFilterBuilding() {

        try {
            IndicatorGraphic graphic = new IndicatorGraphic();
            Indicator indicator = new Indicator();
            indicator.setVar("table1.field1=A");
            indicator.setColumns("field1:Campo 1,field2:Campo 2");
            graphic.setIndicator(indicator);
            graphic.setDateVar("date_field");

            IndicatorFilterDto fil = buildIndicatorFilter(new PeriodDto(),
                graphic);

            assertEquals("table1", fil.getTableName());
            assertEquals("field1", fil.getFieldName());
            assertEquals("A", fil.getValue());
        } catch (Exception ex) {
            fail("No debio presentarse ninguna excepci�n.");
        }
    }

    /**
     * Verifica que la periodicidad se agregue bien al filtro cuando es mensual.
     */
    @Test
    public void case8_validateMonthlyPeriodicity() {

        try {
            IndicatorGraphic graphic = new IndicatorGraphic();
            Indicator indicator = new Indicator();
            indicator.setVar("table1.field1=A");
            indicator.setColumns("field1:Campo 1,field2:Campo 2");
            graphic.setIndicator(indicator);
            graphic.setDateVar("date_field");
            graphic.setPeriodicity(Periodicity.M);
            graphic.setNumberOfPeriods(1);

            IndicatorFilterDto fil = buildIndicatorFilter(new PeriodDto(),
                graphic);

            assertEquals(1, fil.getNumberOfPeriods());
            assertFalse(fil.hasManyPeriods());
        } catch (Exception ex) {
            fail("No debio presentarse ninguna excepci�n.");
        }
    }

    /**
     * Verifica que la periodicidad se agregue bien al filtro cuando es anual.
     */
    @Test
    public void case9_validateYearlyPeriodicity() {

        try {
            IndicatorGraphic graphic = new IndicatorGraphic();
            Indicator indicator = new Indicator();
            indicator.setVar("table1.field1=A");
            indicator.setColumns("field1:Campo 1,field2:Campo 2");
            graphic.setIndicator(indicator);
            graphic.setDateVar("date_field");
            graphic.setPeriodicity(Periodicity.A);
            graphic.setNumberOfPeriods(1);

            IndicatorFilterDto fil = buildIndicatorFilter(new PeriodDto(),
                graphic);

            assertEquals(13, fil.getNumberOfPeriods());
            assertTrue(fil.hasManyPeriods());
        } catch (Exception ex) {
            fail("No debio presentarse ninguna excepci�n.");
        }
    }

    /**
     * Valida la fecha de fin para cuando se configura un solo per&iacute;odo.
     */
    @Test
    public void case10_validateDatesOnePeriod() {

        try {
            IndicatorGraphic graphic = new IndicatorGraphic();
            Indicator indicator = new Indicator();
            indicator.setVar("table1.field1=A");
            indicator.setColumns("field1:Campo 1,field2:Campo 2");
            graphic.setIndicator(indicator);
            graphic.setDateVar("date_field");
            graphic.setPeriodicity(Periodicity.M);
            graphic.setNumberOfPeriods(1);

            PeriodDto period = new PeriodDto();
            period.setStartYear(2014);
            period.setStartMonth(1);

            IndicatorFilterDto fil = buildIndicatorFilter(period, graphic);

            Calendar cal = Calendar.getInstance();
            cal.set(2014, 1, 1, 0, 0, 0);
            cal.set(Calendar.MILLISECOND, 0);

            if (cal.getTime().compareTo(fil.getEndDate()) != 0) {
                fail("La fecha de fin no coincide.");
            }
        } catch (Exception ex) {
            fail("No debio presentarse ninguna excepci�n.");
        }
    }

    /**
     * Valida la fecha de fin para cuando se configura un solo an&tilde;o.
     */
    @Test
    public void case11_validateDatesOneYear() {

        try {
            IndicatorGraphic graphic = new IndicatorGraphic();
            Indicator indicator = new Indicator();
            indicator.setVar("table1.field1=A");
            indicator.setColumns("field1:Campo 1,field2:Campo 2");
            graphic.setIndicator(indicator);
            graphic.setDateVar("date_field");
            graphic.setPeriodicity(Periodicity.A);
            graphic.setNumberOfPeriods(1);

            PeriodDto period = new PeriodDto();
            period.setStartYear(2014);
            period.setStartMonth(1);

            IndicatorFilterDto fil = buildIndicatorFilter(period, graphic);

            Calendar cal = Calendar.getInstance();
            cal.set(2015, 1, 1, 0, 0, 0);
            cal.set(Calendar.MILLISECOND, 0);

            if (cal.getTime().compareTo(fil.getEndDate()) != 0) {
                fail("La fecha de fin no coincide.");
            }
        } catch (Exception ex) {
            fail("No debio presentarse ninguna excepci�n.");
        }
    }

    /**
     * Valida la fecha de fin para cuando se configura varios per&iacute;odos.
     */
    @Test
    public void case12_validateDatesManyPeriod() {

        try {
            IndicatorGraphic graphic = new IndicatorGraphic();
            Indicator indicator = new Indicator();
            indicator.setVar("table1.field1=A");
            indicator.setColumns("field1:Campo 1,field2:Campo 2");
            graphic.setIndicator(indicator);
            graphic.setDateVar("date_field");
            graphic.setPeriodicity(Periodicity.M);
            graphic.setNumberOfPeriods(6);

            PeriodDto period = new PeriodDto();
            period.setStartYear(2014);
            period.setStartMonth(1);
            period.setEndYear(2014);
            period.setEndMonth(6);

            IndicatorFilterDto fil = buildIndicatorFilter(period, graphic);

            Calendar cal = Calendar.getInstance();
            cal.set(2014, 6, 1, 0, 0, 0);
            cal.set(Calendar.MILLISECOND, 0);

            if (!cal.getTime().equals(fil.getEndDate())) {
                fail("La fecha de fin no coincide.");
            }
        } catch (Exception ex) {
            fail("No debio presentarse ninguna excepci�n.");
        }
    }

    /**
     * Se espera una excepci&oacute;n para las fechas fuera de rango.
     *
     * @throws UserException
     */
    @Test(expected = UserException.class)
    public void case13_validateDatesOutOfRange() throws UserException {

        IndicatorGraphic graphic = new IndicatorGraphic();
        Indicator indicator = new Indicator();
        indicator.setVar("table1.field1=A");
        indicator.setColumns("field1:Campo 1,field2:Campo 2");
        graphic.setIndicator(indicator);
        graphic.setDateVar("date_field");
        graphic.setPeriodicity(Periodicity.M);
        graphic.setNumberOfPeriods(6);

        PeriodDto period = new PeriodDto();
        period.setStartYear(2014);
        period.setStartMonth(1);
        period.setEndYear(2014);
        period.setEndMonth(7);

        buildIndicatorFilter(period, graphic);
    }

    /**
     * Valida que se asignen las columnas bien.
     */
    @Test
    public void case12_validateColumns() {

        try {
            IndicatorGraphic graphic = new IndicatorGraphic();
            Indicator indicator = new Indicator();
            indicator.setVar("table1.field1=A");
            indicator.setColumns("field1:Campo 1,field2:Campo 2");
            graphic.setIndicator(indicator);
            graphic.setDateVar("date_field");
            graphic.setPeriodicity(Periodicity.M);
            graphic.setNumberOfPeriods(6);

            PeriodDto period = new PeriodDto();
            period.setStartYear(2014);
            period.setStartMonth(1);
            period.setEndYear(2014);
            period.setEndMonth(6);

            IndicatorFilterDto fil = buildIndicatorFilter(period, graphic);

            assertEquals(2, fil.getColumns().size());
            assertEquals(2, fil.getTitles().size());
        } catch (Exception ex) {
            fail("No debio presentarse ninguna excepci�n.");
        }
    }

    /**
     * Valida que la fecha de finalizaci&oacute;n no sea menor a la de inicio.
     *
     * @throws UserException
     */
    @Test(expected = UserException.class)
    public void case14_validateEndDateOutOfRange() throws UserException {

        IndicatorGraphic graphic = new IndicatorGraphic();
        Indicator indicator = new Indicator();
        indicator.setVar("table1.field1=A");
        indicator.setColumns("field1:Campo 1,field2:Campo 2");
        graphic.setIndicator(indicator);
        graphic.setDateVar("date_field");
        graphic.setPeriodicity(Periodicity.M);
        graphic.setNumberOfPeriods(6);

        PeriodDto period = new PeriodDto();
        period.setStartYear(2014);
        period.setStartMonth(1);
        period.setEndYear(2013);
        period.setEndMonth(7);

        buildIndicatorFilter(period, graphic);
    }
}
