$(document).ready(function () {
   $('form input').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'right',
        theme: 'tooltipster-shadow',
        animation: 'grow',
        timer: 3000
    });
    
    $.extend($.validator.messages, {
        required: "Este campo es obligatorio.",
        remote: "Por favor, rellena este campo.",
        email: "Por favor, escribe una direcci�n de correo v�lida.",
        url: "Por favor, escribe una URL v�lida.",
        date: "Por favor, escribe una fecha v�lida.",
        dateISO: "Por favor, escribe una fecha (ISO) v�lida.",
        number: "Por favor, escribe un n�mero v�lido.",
        digits: "Por favor, escribe s�lo d�gitos.",
        creditcard: "Por favor, escribe un n�mero de tarjeta v�lido.",
        equalTo: "Por favor, escribe el mismo valor de nuevo.",
        extension: "Por favor, escribe un valor con una extensi�n aceptada.",
        maxlength: $.validator.format( "Por favor, no escribas m�s de {0} caracteres." ),
        minlength: $.validator.format( "Por favor, no escribas menos de {0} caracteres." ),
        rangelength: $.validator.format( "Por favor, escribe un valor entre {0} y {1} caracteres." ),
        range: $.validator.format( "Por favor, escribe un valor entre {0} y {1}." ),
        max: $.validator.format( "Por favor, escribe un valor menor o igual a {0}." ),
        min: $.validator.format( "Por favor, escribe un valor mayor o igual a {0}." ),
        nifES: "Por favor, escribe un NIF v�lido.",
        nieES: "Por favor, escribe un NIE v�lido.",
        cifES: "Por favor, escribe un CIF v�lido."
    } );
});

/**
 * Valida un formulario de acuerdo a lo configurado en los campos.
 * @param {type} name
 * @returns {unresolved}
 */
function validateForm(name) {
    var flag = $('#' + name).validate({
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'li',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            $(element).tooltipster('content', $(error).text());
            $(element).tooltipster('show');
        },
        success: function (label, element) {
            $(element).tooltipster('hide');
        }
    });
    
    return flag;
}

/**
 * Verifica si se escriben solo numeros.
 * @param {type} event
 * @returns {undefined}
 */
function isNumberKey(event) {
    var keycode = event.which;

    if (!(event.shiftKey === false && (keycode === 8 || 
            keycode === 37 || keycode === 39 ||
            (keycode >= 48 && keycode <= 57)))) {
        event.preventDefault();
    }
}

/**
 * Funci&oacute;n para hacer un llamado Ajax en la operaci&oacute;n de editar.
 * @param {type} url
 * @param {type} modal
 * @returns {undefined}
 */
function onEdit(url, modal) {
    $.ajax({
        url: url,
        success: function(result){
            var json = jQuery.parseJSON(result);

            foundComponents(json, '');

            $('.hiddendiv').hide();
            $('#' + modal).modal('show');
        }
    });
}

/**
 * Funci&oacute;n para hacer un llamado Ajax en la operaci&oacute;n de editar.
 * @param {type} url
 * @param {type} modal
 * @param {type} func
 * @returns {undefined}
 */
function onEdit(url, modal, func) {
    $.ajax({
        url: url,
        success: function(result){
            var json = jQuery.parseJSON(result);

            foundComponents(json, '');

            $('.hiddendiv').hide();
            $('#' + modal).modal('show');
            
            if (func !== undefined) {
                func();
            }
        }
    });
}

/**
 * Encuentra los componentes en el formulario y setea los valores.
 * @param {type} json
 * @param {type} parent
 * @returns {undefined}
 */
function foundComponents(json, parent) {
    for (var x in json) {
        if (typeof json[x] === 'object') {
            foundComponents(json[x], parent + x);
        } else {
            if ($("#" + parent + x) === undefined) {
                continue;
            }

            $("#" + parent + x).val(json[x]);
        }
    }
}

/**
 * Localiza un componente por nombre y le modifica el valor.
 * @param {type} json
 * @param {type} parent
 * @param {type} name
 * @returns {undefined}
 */
function foundComponent(json, parent, name) {
    for (var x in json) {
        if (typeof json[x] === 'object') {
            foundComponents(json[x], parent + x);
        } else {
            if (parent + x === name) {
                $("#" + parent + x).val(json[x]);
            }            
        }
    }
}
