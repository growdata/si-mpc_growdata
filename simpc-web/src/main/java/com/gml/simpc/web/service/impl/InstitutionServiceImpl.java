
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserServiceImpl.java
 * Created on: 2016/10/19, 02:11:43 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.entity.Headquarter;
import com.gml.simpc.api.entity.Institution;
import com.gml.simpc.api.entity.exeption.SystemException;
import com.gml.simpc.api.entity.exeption.UserException;
import com.gml.simpc.api.entity.exeption.code.ErrorCode;
import com.gml.simpc.api.entity.valuelist.InstitutionType;
import com.gml.simpc.api.enums.ApprovalInstitutionStatus;
import com.gml.simpc.api.services.HeadquarterService;
import com.gml.simpc.api.services.InstitutionService;
import com.gml.simpc.commons.annotations.AnnotationInterceptor;
import com.gml.simpc.web.repository.InstitutionRepository;
import com.gml.simpc.web.repository.valuelist.InstitutionTypeRepository;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_001_UNKNOW;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_003_INSTITUTION_VALIDATE_QUALITYCERTIFICATE;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_004_INSTITUTION_VALIDATE_CERTIFICATE_EXPIRATION;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_031_INSTITUTION_FAILED;
import static com.gml.simpc.api.utilities.Util.INSTITUTION_TYPE_CAJA_COMPENSACION_FAMILIAR;
import static com.gml.simpc.api.utilities.Util.INSTITUTION_TYPE_FORMACION_TRABAJO;
import static com.gml.simpc.api.utilities.Util.likeValue;
import static com.gml.simpc.api.utilities.Util.nullValue;
import static com.gml.simpc.api.utilities.Util.selectFilter;
import static com.gml.simpc.api.utilities.Util.nullValue;
import static com.gml.simpc.api.utilities.Util.nullValue;
import static com.gml.simpc.api.utilities.Util.nullValue;
import static com.gml.simpc.api.utilities.Util.nullValue;
import static com.gml.simpc.api.utilities.Util.nullValue;
import static com.gml.simpc.api.utilities.Util.nullValue;
import static com.gml.simpc.api.utilities.Util.nullValue;

/**
 * Servicio para administrar instituciones.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Service(value = "institutionService")
public class InstitutionServiceImpl implements InstitutionService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER
            = Logger.getLogger(InstitutionServiceImpl.class);
    /**
     * Repositorio para instituciones.
     */
    @Autowired
    private InstitutionRepository institutionRepository;

    /**
     * Repositorio para tipos de instituciones.
     */
    @Autowired
    private InstitutionTypeRepository institutionTypeRepository;

    /**
     * Servicio para adminisraci&oacute;n de sedes; Servicio de sedes
     * intermediario para la comunicaci&oacute;n con la persistencia.
     */
    @Autowired
    private HeadquarterService headquarterService;

    /**
     * Genera la lista de todas las instituciones.
     * 
     * @return 
     */
    @Override
    public List<Institution> getAll() {
        LOGGER.info("Ejecutando metodo [ getAll de instituciones ]");
        return this.institutionRepository.findAll();
    }

    /**
     * Guarda instituciones.
     * 
     * @param institution 
     */
    @Transactional
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
            afterExecutionValue = "DESPUES", methodName = "Guardar",
            entityTableName = "INSTITUCIONES")
    @Override
    public void save(Institution institution) {
        LOGGER.info("Ejecutando metodo [ save de instituciones]");

        try {

            if (institution.getType().getId().
                    equals(INSTITUTION_TYPE_CAJA_COMPENSACION_FAMILIAR)
                    || institution.
                            getType().getId().equals(INSTITUTION_TYPE_FORMACION_TRABAJO)) {
                verifyQualityCertificate(institution);

                verifyQualityCertificateExpiration(institution);
            }
            this.institutionRepository.saveAndFlush(institution);

            LOGGER.info("Se creo la institucion: "
                    + institution.getId());

            Headquarter headquarter = institution.getHeadquarter();
            headquarter.setInstitution(institution);
            headquarter.setCreationDate(new Date());
//            headquarter.setPrincipal(true);

            //TODO: Reemplazar id de usuario con el  de sesi&oacute:n cuando
            //se pueda
            headquarter.setCreationUserId(1L);
            /*
             * Guardamos la sede principal
             */
            this.headquarterService.save(institution.getHeadquarter());

        } catch (DataIntegrityViolationException ex) {
            LOGGER.error("Error", ex);
            String constraint = ex.getLocalizedMessage();

            throw validateConstraint(constraint);
        }
    }

    /**
     * Actualiza instituciones.
     * 
     * @param inst
     * @throws UserException 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
            afterExecutionValue = "DESPUES", methodName = "Actualizar",
            entityTableName = "INSTITUCIONES")
    @Override
    public void update(Institution inst) throws UserException {
        LOGGER.info("Ejecutando metodo [ update de instituciones]");

        try {
            if (inst.getQualityCertificate() == null
                    || inst.getQualityCertificate().length == 0) {
                if (inst.getQualityCertificateExpiration() == null) {
                    this.institutionRepository.updateInstitution(
                            inst.getType().getId(), inst.getInstitutionName(),
                            inst.getDocumentType().getId(), inst.getNit(),
                            (inst.getDv() == null ? -1 : inst.getDv()),
                            inst.getOrigin().name(), inst.getLegalNature().ordinal(),
                            inst.getContactPhone(), inst.getContactEmail(),
                            inst.getStatus().name(), inst.getApprovalStatus().name(),
                            inst.getModificationUserId().getId(), inst.getId(),
                            inst.getTypeQualityCertificate().getId());
                } else {
                    this.institutionRepository.updateInstitution(
                            inst.getType().getId(), inst.getInstitutionName(),
                            inst.getDocumentType().getId(), inst.getNit(),
                            (inst.getDv() == null ? -1 : inst.getDv()),
                            inst.getOrigin().name(), inst.getLegalNature().ordinal(),
                            inst.getContactPhone(), inst.getContactEmail(),
                            inst.getStatus().name(), inst.getApprovalStatus().name(),
                            inst.getQualityCertificateExpiration(),
                            inst.getModificationUserId().getId(), inst.getId(), 
                            inst.getTypeQualityCertificate().getId());
                }
            } else {
                this.institutionRepository.updateInstitution(
                        inst.getType().getId(), inst.getInstitutionName(),
                        inst.getDocumentType().getId(), inst.getNit(),
                        (inst.getDv() == null ? -1 : inst.getDv()),
                        inst.getOrigin().name(), inst.getLegalNature().ordinal(),
                        inst.getContactPhone(), inst.getContactEmail(),
                        inst.getStatus().name(), inst.getApprovalStatus().name(),
                        inst.getQualityCertificateExpiration(),
                        inst.getQualityCertificate(),
                        inst.getModificationUserId().getId(),
                        inst.getTypeQualityCertificate().getId(), inst.getId());
            }
        } catch (DataIntegrityViolationException ex) {
            LOGGER.error("Error", ex);
            String constraint = ex.getLocalizedMessage();

            throw validateConstraint(constraint);
        }
    }

    /**
     * Elimina instituciones.
     * 
     * @param institution 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
            afterExecutionValue = "DESPUES", methodName = "Borrar",
            entityTableName = "INSTITUCIONES")
    @Override
    @Transactional
    public void remove(Institution institution) {
        LOGGER.info("Ejecutando metodo [ remove de instituciones ]");
        this.headquarterService.remove(headquarterService.
                findByInstitution(institution));
        this.institutionRepository.delete(institution);

    }

    /**
     * Genera lista de instituciones seg&ucute;n los filtros ingresados.
     *
     * @param institution
     *
     * @return
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
            afterExecutionValue = "DESPUES", methodName = "Consultar por Filtros",
            entityTableName = "INSTITUCIONES")
    @Override
    public List<Institution> findByFilters(Institution institution) {
        LOGGER.info("Ejecutando metodo [ findByFilters de instituciones ]");

        String code = String.valueOf(institution.getApprovalStatus() == null ? ""
                : institution.getApprovalStatus().name());

        return this.institutionRepository.
                findByFilters(nullValue(institution.getType().getId()),
                        likeValue(code),
                        likeValue(institution.getInstitutionName()));
    }

    /**
     * Genera lista de instituciones seg&ucute;n los filtros y fechas
     * ingresados.
     * 
     * @param institutionName
     * @param institutionType
     * @param regisCcf
     * @param approDateIni
     * @param approDateFin
     * @param regisDateIni
     * @param regisDateFin
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
            afterExecutionValue = "DESPUES", methodName = "Consultar "
            + "Instituciones Aprobadas", entityTableName = "INSTITUCIONES")
    @Override
    public List<Institution> findApprovedInstitutions(String institutionName,
            String institutionType, String regisCcf, Date approDateIni,
            Date approDateFin, Date regisDateIni, Date regisDateFin) {
        LOGGER.info("entra  a  findAprovedInstitutions");

        if (regisDateIni == null && regisDateFin == null
                && approDateIni != null && approDateFin != null) {
            return this.institutionRepository.
                    findApprovedInstitutionsapproDates(
                            likeValue(institutionName),
                            likeValue(institutionType),
                            likeValue(regisCcf), approDateIni, approDateFin);
        } else if (approDateIni == null && approDateFin == null
                && regisDateIni != null && regisDateFin != null) {
            return this.institutionRepository.
                    findApprovedInstitutionsRegisDates(
                            likeValue(institutionName),
                            likeValue(institutionType),
                            likeValue(regisCcf), regisDateIni, regisDateFin);
        } else if (approDateIni == null && approDateFin == null
                && regisDateIni == null && regisDateFin == null) {
            return this.institutionRepository.
                    findApprovedInstitutionsNotDates(
                            likeValue(institutionName),
                            likeValue(institutionType),
                            likeValue(regisCcf));
        } else {
            return this.institutionRepository.findApprovedInstitutionsAllDates(
                    likeValue(institutionName), likeValue(institutionType),
                    likeValue(regisCcf), approDateIni, approDateFin,
                    regisDateIni, regisDateFin);
        }
    }

    /**
     * Metodo que encuentra todos los tipos de instituciones
     * 
     * @return 
     */
    @Override
    public List<InstitutionType> findAllInstitutionTypes() {
        LOGGER.info("Ejecutando metodo [ findAllInstitutionTypes de "
                + "instituciones ]");
        return this.institutionTypeRepository.findAll();
    }

    /**
     * Metodo que encuentra una institucion por id.
     * 
     * @param id
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
            afterExecutionValue = "DESPUES", methodName = "Consultar por Id",
            entityTableName = "INSTITUCIONES")
    @Override
    public Institution findById(Long id) {
        LOGGER.info("Ejecutando metodo [ findById ]");

        return this.institutionRepository.findOne(id);
    }

    /**
     * Metodo que encuentra una institucion por estado
     * 
     * @param status
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
            afterExecutionValue = "DESPUES", methodName = "Encontrar por Estado",
            entityTableName = "INSTITUCIONES")
    @Override
    public List<Institution> findByApprovalStatus(
            ApprovalInstitutionStatus status) {
        return this.institutionRepository.findByApprovalStatus(status);
    }

    /**
     * Metodo que realiza un update sobre instituciones referente al estado de
     * aprobación.
     * 
     * @param status
     * @param updateUserId
     * @param today
     * @param idInstitution 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
            afterExecutionValue = "DESPUES", methodName = "Actualizar Estado de "
            + "Aprobación de la Institución", entityTableName = "INSTITUCIONES")
    @Override
    public void updateApprovalState(char status, Long updateUserId, Date today,
            Long idInstitution) {
        this.institutionRepository.updateApprovalState(status, updateUserId,
                today, idInstitution);
    }

    /**
     * Metodo que realiza un update sobre instituciones referente al estado.
     * 
     * @param status
     * @param updateUserId
     * @param today
     * @param idInstitution 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
            afterExecutionValue = "DESPUES", methodName = "Actualizar Estado de "
            + "la Institución", entityTableName = "INSTITUCIONES")
    @Override
    public void updateState(char status, Long updateUserId, Date today,
            int idInstitution) {
        this.institutionRepository.updateState(status, updateUserId, today,
                idInstitution);
    }

    /**
     * Metodo que activa por primera ves una institución.
     * 
     * @param status
     * @param approvalStatus
     * @param updateUserId
     * @param today
     * @param idInstitution
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
            afterExecutionValue = "DESPUES", methodName = "Activar Institución",
            entityTableName = "INSTITUCIONES")
    @Override
    public Institution activateInstitution(String status, String approvalStatus,
            Long updateUserId, Date today, Long idInstitution) {
        this.institutionRepository.activateInstitution(status, approvalStatus,
                updateUserId, today, idInstitution);
        return this.institutionRepository.findOne(idInstitution);
    }

    /**
     * Verifica si la instituci&oacute;n tiene certificado de calidad.
     *
     * @param institution
     *
     * @throws UserException
     */
    private void verifyQualityCertificate(Institution institution)
            throws SystemException {
        if (institution.getQualityCertificate() == null
                || institution.getQualityCertificate().length < 1) {
            LOGGER.error(" El certificado es nulo");
            throw new SystemException(
                    ERR_003_INSTITUTION_VALIDATE_QUALITYCERTIFICATE);
        }
    }

    /**
     * Verifica la fecha de vencimiento del certificado de calidad.
     *
     * @param institution
     *
     * @throws UserException
     */
    private void verifyQualityCertificateExpiration(Institution institution)
            throws SystemException {
        if (institution.getQualityCertificateExpiration() != null) {
            if (institution.getQualityCertificateExpiration().
                    compareTo(new Date()) <= 0) {
                throw new SystemException(
                        ERR_004_INSTITUTION_VALIDATE_CERTIFICATE_EXPIRATION);
            }
        } else {
            throw new SystemException(
                    ErrorCode.ERR_004_INSTITUTION_VALIDATE_CERTIFICATE_EXPIRATION);
        }
    }

    /**
     * M&eacute;todo para evaluar el constraint que dispara la base de datos.
     *
     * @param constraint
     *
     * @return
     */
    private SystemException validateConstraint(String constraint) {
        if (constraint.contains(Institution.UQ_NOMBRE_INSTITUCION)) {
            return new SystemException(ERR_031_INSTITUTION_FAILED,
                    "Nombre de la Institución");
        } else if (constraint.contains(Institution.UQ_NUMERO_DOCUMENTO)) {
            return new SystemException(ERR_031_INSTITUTION_FAILED,
                    "Número de Documento");
        }
        return new SystemException(ERR_001_UNKNOW);
    }
}
