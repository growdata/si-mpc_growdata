/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: MailHistoryRepository.java
 * Created on: 2017/01/13, 02:47:25 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository;

import com.gml.simpc.api.entity.MailHistory;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repositorio para el historico de email.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface MailHistoryRepository extends JpaRepository<MailHistory, Long> {

}
