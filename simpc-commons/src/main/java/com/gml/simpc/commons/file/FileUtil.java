/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: FileUtil.java
 * Created on: 2017/01/31, 10:37:18 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.commons.file;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

/**
 * Utileria para el manejo de archivos.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public final class FileUtil {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(FileUtil.class);
    
    /**
     * Constructor privado para impedir instancias de la clase.
     */
    private FileUtil() {

    }

    /**
     * Escribe un archivo en disco.
     *
     * @param path
     * @param file
     *
     * @throws IOException
     */
    public static void writeFile(String path, byte[] file) throws IOException {
        LOGGER.info("Escribiendo el archivo [ " + path + " ]");
        
        FileUtils.writeByteArrayToFile(new File(path), file);
    }
}
