/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: basicInformation.java
 * Created on: 2016/12/27, 04:23:26 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Leonardo Rocha</a>
 */
public class OtherKnowledgeDto {

    private String documentType;
    private String numberIdentification;
    private String type;
    private String tool;
    private String level;
    private Long cargaId;


    /*
     * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    public OtherKnowledgeDto() {

    }

    public OtherKnowledgeDto(String documentType, String numberIdentification,
        String type, String tool, String level, Long cargaId) {

        this.documentType = documentType;
        this.numberIdentification = numberIdentification;
        this.type = type;
        this.tool = tool;
        this.level = level;
        this.cargaId = cargaId;
    }

    public Long getCargaId() {
        return cargaId;
    }

    public void setCargaId(Long cargaId) {
        this.cargaId = cargaId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTool() {
        return tool;
    }

    public void setTool(String tool) {
        this.tool = tool;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getNumberIdentification() {
        return numberIdentification;
    }

    public void setNumberIdentification(String numberIdentification) {
        this.numberIdentification = numberIdentification;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Tipo=").append(type).
            append("~&~Herramienta=").append(tool).
            append("~&~Nivel=").append(level);
        return stringBuilder.toString();
    }

}
