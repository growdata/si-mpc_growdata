/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: LoadService.java
 * Created on: 2017/01/24, 04:51:25 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.batch;

import com.gml.simpc.api.entity.LoadProcess;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.entity.exeption.SystemException;
import com.gml.simpc.api.entity.exeption.UserException;
import com.gml.simpc.api.entity.exeption.code.ErrorCode;
import com.gml.simpc.api.enums.ProcessStatus;
import com.gml.simpc.api.services.LoadProcessService;
import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.file.FileErrorWritter;
import com.gml.simpc.loader.utilities.BatchJobNames;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_047_HEADER_FOUND;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_049_HEADER_LINE_MISMATCH;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_050_HEADER_FILE_TYPE_EMPTY;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_051_HEADER_DATE_NOT_TODAY;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_052_HEADER_DATE_FORMAT_INVALID;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_053_HEADER_USER_INVALID;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_054_HEADER_TOKENS;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_055_HEADER_EMPTY;
import static com.gml.simpc.api.entity.exeption.code.FileErrorCode.FILE_ERR_010_PROCESS_CREATION_ERROR;
import static com.gml.simpc.api.entity.exeption.code.FileErrorCode.FILE_ERR_011_VAL_JOB_CREATION_ERROR;

/**
 * Servicio para tareas relacionadas con carga de archivos
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica
 * Jim&eacute;nez Rozo</a>
 */
@Service(value = "loadService")
public class LoadService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(LoadService.class);
    /**
     * Token divisor del header.
     */
    private static final String HEADER_SPLIT_TOKEN = "\\|\\$\\|";
    /**
     * Formato de fecha en el header.
     */
    private static final String HEADER_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    /**
     * Caracter para salto de l&iacute;nea.
     */
    private static final char SKIP_LINE_CHAR = '\n';
    /**
     * Formateador de fecha.
     */
    private final SimpleDateFormat formatter =
        new SimpleDateFormat(HEADER_DATE_FORMAT);
    /**
     * Servicio para manejo de jobs
     */
    @Autowired
    private JobService jobService;

    /**
     * Servicio para servicios jpa de procesos de carga
     */
    @Autowired
    private LoadProcessService loadProcessService;

    /**
     * Metodo privado para crear registro de carga
     *
     * @param loadProcess
     *
     * @return
     */
    public LoadProcess createProcess(LoadProcess loadProcess) {

        Date date = new Date();
        loadProcess.setInitDate(date);
        loadProcess.setInitDateStrVal(date);
        try {
            String filePath = loadProcess.getNewFileRoot() +
                loadProcess.getNewFilePath() + loadProcess.getNewFileName();

            LOGGER.info("FILE PATH: " + filePath);

            String header = getHeader(filePath, loadProcess.getConsultant());

            LoadProcess foundProcess = this.loadProcessService.
                getLoadProcessByHeader(header);

            if (foundProcess != null) {
                throw new SystemException(ERR_047_HEADER_FOUND);
            }

            loadProcess.setHeader(header);
            this.loadProcessService.save(loadProcess);
            JobResultDto launchJobResult = launchValidationJob(loadProcess);

            loadProcess.setFailRows(launchJobResult.getErrorList().size());
            loadProcess.setTotalRows(launchJobResult.getProcesedLines());
            loadProcess.setSuccessRows(launchJobResult.getSuccessLines());

            if ("COMPLETED".equals(launchJobResult.getStatus())) {
                if (!launchJobResult.getErrorList().isEmpty()) {
                    loadProcess.setDetail(launchJobResult.getErrorsString());
                    loadProcess.setStatus(ProcessStatus.ERRORES_ESTRUCTURA);
                } else {
                    loadProcess.setStatus(ProcessStatus.ESTRUCTURA_VALIDADA);
                }
            } else {
                LOGGER.error("Error lauching job");
                loadProcess.
                    setDetail(FILE_ERR_011_VAL_JOB_CREATION_ERROR.getMessage());
                loadProcess.setStatus(ProcessStatus.FALLA_VALIDACION);
            }

            this.loadProcessService.save(loadProcess);

            FileErrorWritter fileErrWr = new FileErrorWritter();
            fileErrWr.writeErrorFile(loadProcess,
                launchJobResult.getErrorList(), "structure");
        } catch (UserException ex) {
            loadProcess.
                setDetail(FILE_ERR_010_PROCESS_CREATION_ERROR.getMessage());

            LOGGER.error("Error creating process:", ex);
        } catch (SystemException ex) {
            loadProcess.setDetail(ex.getMessage());
            loadProcess.setStatus(ProcessStatus.ERRORES_ESTRUCTURA);
        }

        return loadProcess;
    }

    private JobResultDto launchValidationJob(LoadProcess loadProcess) {
        BatchJobNames batchJobNames = new BatchJobNames();
        String jobName = batchJobNames.getLoadJobNames()
            .get(loadProcess.getLoadType());

        String filePath = loadProcess.getNewFileRoot() +
            loadProcess.getNewFilePath() + loadProcess.getNewFileName();

        JobResultDto launchJobResult = this.jobService.
            launchJob(filePath, jobName, loadProcess.getId(),
                loadProcess.getWriteType());

        return launchJobResult;
    }

    /**
     * Obtiene la cabecera del archivo.
     *
     * @param pathFile
     * @param user
     *
     * @return
     */
    private String getHeader(String pathFile, User user) {        
        return verifyHeader(user, pathFile);
    }

    /**
     * Verifica la cabecera.
     *
     * @param user
     */
    private String verifyHeader(User user, String pathFile) {
        try {
            List<String> lines = Files.readAllLines(Paths.get(pathFile), 
                Charset.forName("utf-8"));
            String header = lines.get(0);
            
            int numberOfLines = lines.size() - 1;
            
            LOGGER.info("LINEAS: " + numberOfLines);
            verifyNullability(header);
            
            String[] headerParts = header.split(HEADER_SPLIT_TOKEN);
            
            verifyNumberOfTokens(headerParts);
            verifyUsername(headerParts, user);
            verifyDate(headerParts);
            verifyFileType(headerParts);
            verifyNumberOfLines(headerParts, numberOfLines);
            
            return header;
        } catch (IOException ex) {
            throw new SystemException(ErrorCode.ERR_001_UNKNOW);
        }
    }

    /**
     * Verifica que las l&iacute;neas correspondan.
     *
     * @param headerParts
     * @param linesNumber
     *
     * @throws NumberFormatException
     * @throws SystemException
     */
    private void verifyNumberOfLines(String[] headerParts, int linesNumber)
        throws SystemException {

        try {
            if (Long.valueOf(headerParts[3]) != linesNumber) {
                throw new SystemException(ERR_049_HEADER_LINE_MISMATCH);
            }
        } catch (NumberFormatException ex) {
            throw new SystemException(ERR_049_HEADER_LINE_MISMATCH);
        }
    }

    /**
     * Verifica el tipo de archivo no este vac&iacute;o.
     *
     * @param headerParts
     *
     * @throws SystemException
     */
    private void verifyFileType(String[] headerParts) throws SystemException {
        if (headerParts[2].trim().isEmpty()) {
            throw new SystemException(ERR_050_HEADER_FILE_TYPE_EMPTY);
        }
    }

    /**
     * Verifica que la fecha no sea diferente al d&iacute;a.
     *
     * @param headerParts
     *
     * @throws SystemException
     */
    private void verifyDate(String[] headerParts) throws SystemException {
        try {
            Date headerDate = this.formatter.parse(headerParts[1]);
            Date today = new Date();

            Calendar cal = Calendar.getInstance();
            cal.setTime(today);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            today = cal.getTime();

            cal.add(Calendar.DATE, 1);

            if (headerDate.compareTo(today) == -1 ||
                headerDate.compareTo(new Date()) == 1) {
                throw new SystemException(ERR_051_HEADER_DATE_NOT_TODAY+" "+headerDate+" "+today);
            }
        } catch (ParseException ex) {
            throw new SystemException(ERR_052_HEADER_DATE_FORMAT_INVALID);
        }
    }

    /**
     * Verifica el usuario en el archivo.
     *
     * @param headerParts
     * @param user
     *
     * @throws SystemException
     */
    private void verifyUsername(String[] headerParts, User user) throws
        SystemException {
        if (!headerParts[0].equalsIgnoreCase(user.getUsername())) {
            
            
            throw new SystemException(ERR_053_HEADER_USER_INVALID);
        }
    }

    /**
     * Verifica el n&uacute;mero de tokens en el archivo.
     *
     * @param headerParts
     *
     * @throws SystemException
     */
    private void verifyNumberOfTokens(String[] headerParts) throws
        SystemException {
        if (headerParts.length != 4) {
            throw new SystemException(ERR_054_HEADER_TOKENS);
        }
    }

    /**
     * Verifica que la cabecera no este vac&iacute;a.
     *
     * @param header
     *
     * @throws SystemException
     */
    private void verifyNullability(String header) throws SystemException {
        if (header == null || header.trim().isEmpty()) {
            throw new SystemException(ERR_055_HEADER_EMPTY);
        }
    }
}
