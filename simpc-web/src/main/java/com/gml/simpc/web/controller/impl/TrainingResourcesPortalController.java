/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: TrainingPortalController.java
 * Created on: 2017/02/01, 12:54:58 M
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gml.simpc.api.dto.TrainingDto;
import com.gml.simpc.api.entity.valuelist.Foundation;
import com.gml.simpc.api.entity.valuelist.ProgramType;
import com.gml.simpc.api.enums.GraphicType;
import com.gml.simpc.api.services.TrainingResourcesService;
import com.gml.simpc.api.utilities.Util;
import com.gml.simpc.web.controller.ControllerDefinition;
import com.gml.simpc.web.utilities.ValueListService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import static com.gml.simpc.api.utilities.Util.FAILED_ALERT;

/**
 * Controller para portal de capacitacion
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Controller
public class TrainingResourcesPortalController implements ControllerDefinition {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(TrainingResourcesPortalController.class);

    /**
     * Servicio para manipular listas de .
     */
    @Autowired
    private ValueListService valueListService;

    /**
     * Servicio para consulta de información de la master data relacionada con
     * capacitaciones.
     */
    @Autowired
    private TrainingResourcesService trainingResourcesService;

    /**
     * Inicializa el binder para formatear fechas.
     *
     * @param binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class,
            new CustomDateEditor(sdf, true));
    }

    /**
     * Muestra la informacion incial del portal
     * Banco de oferentes.
     * 
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping("portal-recursos-capacitacion")
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response) {

        LOGGER.info("entra a ondisplay");
        ModelAndView modelAndView =
            new ModelAndView("portal-recursos-capacitacion");
        String activeTab = "ccftab";
        modelAndView.addObject("tab", activeTab);
        init(modelAndView);
        LOGGER.info("sale a ondisplay");
        return modelAndView;

    }

    /**
     * Muestra el resultado de costos capacitacion-ccf.
     * 
     * @param request
     * @param response
     * @param searchCostos
     * 
     * @return 
     */
    @RequestMapping(name = "recursos-capacitacion-ccf",
        method = RequestMethod.POST)
    public ModelAndView costByCcf(HttpServletRequest request,
        HttpServletResponse response,
        @ModelAttribute("searchCostos") TrainingDto searchCostos) {
        ModelAndView modelAndView = new ModelAndView(
            "portal-recursos-capacitacion");
        String activeTab = "ccftab";
        modelAndView.addObject("tab", activeTab);

        try {

            String ccf = Util.nullValue(searchCostos.getCcfCode());
            String year = Util.nullValue(request.getParameter("anio"));
            String month = Util.nullValue(request.getParameter("mes"));

            LOGGER.info("VALOR DEL CCF " + ccf + "anio" + year + "mes" + month);

            List<TrainingDto> costs = this.trainingResourcesService.
                getCostsByPeriod(ccf, year, month);
            
            modelAndView.addObject("resultList", costs);

            graphicCcfCost(modelAndView, costs, ccf);
            modelAndView.addObject("ccfSelected", ccf);
            modelAndView.addObject("yearSelected", year);
            modelAndView.addObject("monthSelected2", month);
            modelAndView.addObject("activateBtn", true);
            init(modelAndView);

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msgType", FAILED_ALERT);
            modelAndView.addObject("msg", "Por favor contacte al administrador");
        } finally {
            LOGGER.info("finally portal de  recursos de capacitacion");
        }
        return modelAndView;

    }

    /**
     * Muestra el resultado de costos capacitacion-costo.
     * 
     * @param request
     * @param searchCostos2
     * @param response
     * 
     * @return 
     */
    @RequestMapping(name = "recursos-capacitacion-costos",
        method = RequestMethod.GET)
    public ModelAndView costByType(HttpServletRequest request,
        HttpServletResponse response,
        @ModelAttribute("searchCostos2") TrainingDto searchCostos2) {
        ModelAndView modelAndView = new ModelAndView(
            "portal-recursos-capacitacion");
        String activeTab = "costostab";
        modelAndView.addObject("tab", activeTab);
        
        boolean resultOk;

        try {
            String type = request.getParameter("costType2");
            LOGGER.info("***" + type);
            
            switch (type) {
                case "1":
                    modelAndView.addObject("tipo", "matricula");
                    break;
                case "2":
                    modelAndView.addObject("tipo", "transporte");
                    break;
                case "3":
                default:
                    modelAndView.addObject("tipo", "otros");
                    break;
            }

            String ccf = Util.nullValue(searchCostos2.getCcfCode());
            String year = Util.nullValue(request.getParameter("anio"));
            String month = Util.nullValue(request.getParameter("mes"));
            String costType = request.getParameter("costType2");

            List<TrainingDto> costs = this.trainingResourcesService.
                getCostsByCcf(ccf,year,month,costType);
            modelAndView.addObject("resultList", costs);
            graphicTotalCost(modelAndView, costs, type, searchCostos2.
                getCcfCode());

            modelAndView.addObject("ccfSelected2", ccf);
            modelAndView.addObject("yearSelected", year);
            modelAndView.addObject("monthSelected2", month);
            modelAndView.addObject("costTypeSelected", costType);
            resultOk = true;
            modelAndView.addObject("resultOk", resultOk);
            modelAndView.addObject("activateBtn", true);

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msgType", FAILED_ALERT);
            modelAndView.addObject("msg", "Por favor contacte al administrador");
        } finally {
            LOGGER.info("finally portal de  recursos de capacitacion");
        }
        
        init(modelAndView);
        
        return modelAndView;
    }

    /**
     * Muestra el grafico de costos.
     * 
     * @param modelAndView
     * @param costs
     * @param costType
     * @param ccfName 
     */
    private void graphicTotalCost(ModelAndView modelAndView,
        List<TrainingDto> costs, String costType, String ccfName) {

        modelAndView.addObject("graphicType", GraphicType.B);
        try {
            LOGGER.info("entra a graficar" + costs);
            ObjectMapper mapper = new ObjectMapper();
            modelAndView.addObject("graphicData", mapper.writeValueAsString(
                costs));

            String xKey = "tipoCapNombre";
            List<String> yKeys = new ArrayList();
            List<String> labels = new ArrayList();
            List<String> tableg = new ArrayList();
            List<String> barColors = new ArrayList();

            String costName;
            
            switch (costType) {
                case "1":
                    costName = "MATRICULA";
                    barColors.add("blue");
                    yKeys.add("enrollmentCost");
                    labels.add("Costos de matricula");
                    break;
                case "2":
                    costName = "TRANSPORTE";
                    barColors.add("red");
                    yKeys.add("transportCost");
                    labels.add("Costos de transporte");
                    break;
                default:
                    costName = "OTROS";
                    barColors.add("grey");
                    yKeys.add("otherCost");
                    labels.add("Otros Costos");
                    break;
            }

            tableg.add("TIPO DE  FORMACION");
            tableg.add("COSTO TOTAL POR " + costName);

            modelAndView.addObject("graphicList2", costs);
            modelAndView.addObject("xKey", xKey);
            modelAndView.addObject("yKeys", yKeys);
            modelAndView.addObject("labels", labels);
            modelAndView.addObject("titleList", tableg);
            modelAndView.addObject("barColors", barColors);
            modelAndView.addObject("titulografica",
                "COSTOS TOTALES POR TIPO DE FORMACION");

            if (!"%".equals(ccfName)) {
                Map ccfs = valueListService.getFoundations();
                Foundation ccf = (Foundation) ccfs.get(ccfName);

                modelAndView.addObject("titulo", "COSTOS  " + costName + " " +
                    ccf.getName());
            } else {
                modelAndView.addObject("titulo", "COSTOS  " + costName);
            }

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msgType", FAILED_ALERT);

        } finally {
            LOGGER.info("Finaliza realiacion de  graficos");
        }
    }

    /**
     * Muestra el grafico de costos.
     * 
     * @param modelAndView
     * @param costs
     * @param ccfName 
     */
    private void graphicCcfCost(ModelAndView modelAndView,
        List<TrainingDto> costs, String ccfName) {

        modelAndView.addObject("graphicType", GraphicType.B);
        try {
            LOGGER.info("entra a graficar" + costs);
            ObjectMapper mapper = new ObjectMapper();
            modelAndView.addObject("graphicData", mapper.writeValueAsString(
                costs));

            String xKey = "tipoCapNombre";
            List<String> yKeys = new ArrayList();
            List<String> labels = new ArrayList();
            List<String> tableg = new ArrayList();
            List<String> barColors = new ArrayList();

            labels.add("Costo Matricula");
            labels.add("Costo de Transporte");
            labels.add("Otros Costos");

            barColors.add("blue");
            yKeys.add("enrollmentCost");

            barColors.add("red");
            yKeys.add("transportCost");

            barColors.add("grey");
            yKeys.add("otherCost");

            tableg.add("TIPO DE  FORMACION");
            tableg.add("MATRICULA");
            tableg.add("TRANSPORTE");
            tableg.add("OTROS");

            modelAndView.addObject("graphicList", costs);
            modelAndView.addObject("xKey", xKey);
            modelAndView.addObject("yKeys", yKeys);
            modelAndView.addObject("labels", labels);
            modelAndView.addObject("titleList", tableg);
            modelAndView.addObject("barColors", barColors);
            modelAndView.addObject("titulografica",
                "COSTOS TOTALES POR TIPO DE FORMACION");

            if (!"%".equals(ccfName)) {
                Map ccfs = valueListService.getFoundations();
                Foundation ccf = (Foundation) ccfs.get(ccfName);

                modelAndView.addObject("titulo", "COSTOS TOTALES " + ccf.
                    getName());
            } else {
                modelAndView.addObject("titulo", "COSTOS TOTALES ");
            }

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msgType", FAILED_ALERT);

        } finally {
            LOGGER.info("Finaliza realiacion de  graficos");
        }
    }

    /**
     * Inicia los objetos para el controler.
     *
     * @param modelAndView
     */
    private void init(ModelAndView modelAndView) {
        try {
            //Setting value lists
            List<Integer> years = new ArrayList<>();
            Calendar c1 = Calendar.getInstance();
            for (int i = 2014; i <= c1.get(Calendar.YEAR); i++) {
                years.add(i);
            }

            modelAndView.addObject("anios", years);
            List<Foundation> ccfList = this.valueListService.getFoundationList();
            List<ProgramType> trainTypeList = this.valueListService.
                getProgramTypeList();
            Map<String, String> costtype = new HashMap<>();
            costtype.put("1", "Costo Total por Matricula");
            costtype.put("2", "Costos de Subsidio de Transporte");
            costtype.put("3", "Otros Costos Asociados");
            modelAndView.addObject("searchCostos", new TrainingDto());
            modelAndView.addObject("searchCostos2", new TrainingDto());

            modelAndView.addObject("ccfList", ccfList);
            modelAndView.addObject("costList", costtype);
            modelAndView.addObject("trainTypeList", trainTypeList);
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msgType", FAILED_ALERT);

        } finally {
            LOGGER.info("Finaliza init de portal de recursos de capacitacion");
        }
    }
}
