/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: OptionsTemplate.java
 * Created on: 2016/12/09, 09:04:54 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entidad para el manejo de plantillas.
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
@Table(name = "PLANTILLAS")
@Entity
public class OptionsTemplate implements Serializable {

    /**
     * ID de la aprobacion
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator =
        "template_seq_gen")
    @SequenceGenerator(name = "template_seq_gen", sequenceName =
        "TEMPLATE_SEQ", allocationSize = 1, initialValue= 1)
    private Long id;
    /**
     * Mensaje guardado de aprobacion
     */
    @Column(name = "Aprobado", columnDefinition = "CLOB")
    private String approvalTemplate;

    /**
     * Mensaje guadado  de  rechazo
     */
    @Column(name = "Rechazado", columnDefinition = "CLOB")
    private String refuseTemplate;

    public String getApprovalTemplate() {
        return approvalTemplate;
    }

    public String getRefuseTemplate() {
        return refuseTemplate;
    }

    public void setApprovalTemplate(String approvalTemplate) {
        this.approvalTemplate = approvalTemplate;
    }

    public void setRefuseTemplate(String refuseTemplate) {
        this.refuseTemplate = refuseTemplate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Plantilla{").append("id=").append(id).append('}');
        return stringBuilder.toString();
    }

}
