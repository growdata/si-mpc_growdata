<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <style>
            input[type=number]::-webkit-outer-spin-button,
            input[type=number]::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            input[type=number] {
                -moz-appearance:textfield;
            }
        </style>
    </head>

    <body>
        <c:if test="${not empty msg}">
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        <h1>Historial de Acciones Realizadas</h1>
        <div class="panel panel-default">
            <div class="panel-heading"><h2>B&uacute;squeda</h2></div>
            <div class="panel-body">
                <legend>
                    <form:form id="buscarAcc" method="get" action="/masterdata/buscarHistorialAcciones.htm"> 
                        <div class="container">
                            <div class="alert alert-warning alert-dismissible" id="myAlert">
                                <font SIZE=3> Si va a ingresar una fecha de acci�n aseg�rese de ingresar los campos 'inicio' y 'fin' de manera correcta. </font>
                            </div>
                        </div>
                        <div class="container2">
                            <div class="alert alert-warning alert-dismissible" id="myAlert">
                                <font SIZE=3> Ingrese por lo menos un filtro. </font>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="fechaInicioBegin" class="control-label">Fecha de acci�n - Inicio</label>
                                <input id="fechaInicioBegin" class="form-control datepicker" required="required"
                                       data-date-format="dd-mm-yyyy" path="initialDate" name ="initialDate" 
                                       autocomplete="off"/>
                                <fmt:formatDate value="${startDate}" pattern="dd-MM-yyyy" />
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="fechaInicioEnd" class="control-label">Fecha de acci�n - Fin</label>
                                <input id="fechaInicioEnd" class="form-control datepicker" name="finishialDate"
                                       data-date-format="dd-mm-yyyy" path="finishialDate" required="required"
                                       autocomplete="off"/>
                                <fmt:formatDate value="${endDate}" pattern="dd-MM-yyyy" />
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="documento" class="control-label">N�mero de Documento</label>
                                <input type="text" class="form-control" id="documento" 
                                       placeholder="N�mero de documento" maxlength="50"
                                       path="documento" name="documento" id="documento"/>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-offset-4 col-sm-8" style="text-align:right">
                            <input onclick="verificarForm()" type="button" value="Buscar" class="btn btn-success">
                        </div>
                    </form:form>
                </legend>
                <div class="clearfix"></div>
            </div>
        </div>

        <br/>
        <div class="panel panel-default">
            <div class="panel-heading"><h2>Acciones</h2></div>
            <div class="panel-body">
                <div class=" table-responsive">
                    <table id="historyActions" class="table table-striped table-bordered" 
                           cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Cedula Usuario</th>
                                <th>Nombre Completo de Usuario</th>
                                <th>Fecha y hora de acci�n</th>
                                <th>Acci�n</th>
                                <th>Tabla asociada</th>
                                <th>Detalle de la Acci�n</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Cedula Usuario</th>
                                <th>Nombre Completo de Usuario</th>
                                <th>Fecha y hora de acci�n</th>
                                <th>Acci�n</th>
                                <th>Tabla asociada</th>
                                <th>Detalle de la Acci�n</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:if test="${not empty historicActionsList}">
                                <c:forEach var="item" items="${historicActionsList}">
                                    <tr>
                                        <td>${item.user.identificationNumber} </td>
                                        <td>${item.user.firstName} ${item.user.secondName} ${item.user.firstSurname} ${item.user.secondSurname}</td>                                        
                                        <td>${item.fecha}</td>
                                        <td>${item.action}</td>
                                        <td>${item.entity}</td>
                                        <td>${item.parameters}</td>
                                    </tr>
                                </c:forEach>
                            </c:if>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/dataTables.buttons.min.js"></script>
        <script src="resources/js/buttons.flash.min.js"></script>
        <script src="resources/js/jszip.min.js"></script>
        <script src="resources/js/pdfmake.min.js"></script>
        <script src="resources/js/vfs_fonts.js"></script>
        <script src="resources/js/buttons.html5.min.js"></script>
        <script src="resources/js/buttons.print.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/jquery.ui.widget.js"></script>
        <script src="resources/js/jquery.iframe-transport.js"></script>
        <script src="resources/js/bootstrap-confirmation.js"></script>
        <script src="resources/js/jquery.fileupload.js"></script>
        <script src="resources/js/jquery.validate.min.js"></script>
        <script src="resources/js/moment.js"></script>
        <script>
            
            $(document).ready(function () {
                
                $('.container').hide();
                $('.container2').hide();
                
                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Mi�rcoles", "Jueves", "Viernes", "S�bado", "Domingo"],
                    daysShort: ["Dom", "Lun", "Mar", "Mi�", "Jue", "Vie", "S�b", "Dom"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Hoy"
                };

                $('#historyActions').DataTable({
                    "language": {
                        "url": "resources/js/Spanish.json"
                    },
                    "initComplete": function (settings, json) {
                        $(".dt-buttons").each(function (index) {
                            console.log(index + ": " + $(this).text());
                            if ($(this).find('.exportar').length === 0) {
                                $(this).prepend('<label class="exportar">Exportar a:&nbsp;&nbsp;</label>');
                            }
                        });
                    },
                    "bFilter": false,
                    "bInfo": false,
                    dom: 'Blfrtip',
                    buttons: [
                        {
                            extend: 'csv',
                            fieldBoundary: '',
                            footer: false,
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'CSV',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            }
                        },
                        {
                            extend: 'excel',
                            footer: false,
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'EXCEL',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            }
                        },
                        {
                            extend: 'pdf',
                            footer: false,
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'PDF',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            }
                        },
                        {
                            extend: 'print',
                            footer: false,
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'COPY',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            }
                        }
                    ]
                });
                
                $('#fechaInicioBegin').datepicker({
                    language: "es",
                    autoclose: true
                });
                
                $('#fechaInicioEnd').datepicker({
                    language: "es",
                    autoclose: true
                });
            });
            
            function verificarForm(){
                if($('#fechaInicioEnd').val() !== "" || $('#fechaInicioBegin').val() !== ""){
                        $('.container2').hide("fast");
                    if( $('#fechaInicioBegin').val() !== "" && $('#fechaInicioEnd').val() === "" ||
                        $('#fechaInicioBegin').val() === "" && $('#fechaInicioEnd').val() !== "" ||
                        $('#fechaInicioBegin').val() === "" && $('#fechaInicioEnd').val() === "" ) {
                            $('.container').hide("fast");
                            $('.container').show("fast");
                    } else if (moment($("#fechaInicioBegin").val(),"DD-MM-YYYY") > moment($("#fechaInicioEnd").val(),"DD-MM-YYYY")){
                            $('.container2').hide("fast");
                            $('.container').hide("fast");
                            $('.container').show("fast");
                    } else{
                        $('#buscarAcc').submit();
                    }
                } else if ($('#fechaInicioEnd').val() === "" && $('#fechaInicioBegin').val() === "" && $('#documento').val() === "") {
                    $('.container').hide("fast");
                    $('.container2').hide("fast");
                    $('.container2').show("fast");
                } else if ($('#fechaInicioEnd').val() === "" && $('#fechaInicioBegin').val() === "" && $('#documento').val() !== "") {
                    $('#buscarAcc').submit();
                }
            }
                
        </script>
        <style>
            .buttons-csv{
                display: inline-block;
                background-image:url(resources/images/csv.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-csv span{
                opacity: 0;
            }
            .buttons-excel{
                display: inline-block;
                background-image:url(resources/images/excel.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-excel span{
                opacity: 0;
            }
            .buttons-pdf{
                display: inline-block;
                background-image:url(resources/images/pdf.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-pdf span{
                opacity: 0;
            }
            .buttons-print{
                display: inline-block;
                background-image:url(resources/images/print.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-print span{
                opacity: 0;
            }
            div.dt-buttons {
                float: right;
                margin-left:10px;
            }
        </style>
    </body>
</html>
