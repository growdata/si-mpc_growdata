/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: PilaDto.java
 * Created on: 2016/12/27, 04:23:26 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Leonardo Rocha</a>
 */
public class PilaDto {
    
    String documentType;
    String numberIdentification;
    String maxDateU;
    String typePayrollU;
    String typeU;
    String contributingSubTypeU;
    String epsuCode;
    String afpuCode;
    String healthQuotesDays;
    String ibcMayorU;
    String salarioBasicoU;
    String tipoIdAportanteU;
    String numberIdentificationContributingU;
    String contributingNameU;
    String quotes;
    String paidDateU;
    String maxDate;
    String monthsDep;
    String monthsInd;
    String diasDep;
    String daysInd;
    String periodosComo52;
    String primerPeriodoComo52;
    String ultimoPeriodoComo52;
    String dateCutPila;
    String primerPeriodoU;
    Long id_Carga;
 
    public String getAfpuCode() {
        return afpuCode;
    }

    public void setAfpuCode(String afpuCode) {
        this.afpuCode = afpuCode;
    }

    public String getEpsuCode() {
        return epsuCode;
    }

    public void setEpsuCode(String epsuCode) {
        this.epsuCode = epsuCode;
    }

    public String getQuotes() {
        return quotes;
    }

    public void setQuotes(String quotes) {
        this.quotes = quotes;
    }

    public String getHealthQuotesDays() {
        return healthQuotesDays;
    }

    public void setHealthQuotesDays(String healthQuotesDays) {
        this.healthQuotesDays = healthQuotesDays;
    }

    public String getDaysInd() {
        return daysInd;
    }

    public void setDaysInd(String daysInd) {
        this.daysInd = daysInd;
    }

    public String getDateCutPila() {
        return dateCutPila;
    }

    public void setDateCutPila(String dateCutPila) {
        this.dateCutPila = dateCutPila;
    }

    public String getPaidDateU() {
        return paidDateU;
    }

    public void setPaidDateU(String paidDateU) {
        this.paidDateU = paidDateU;
    }

    public Long getId_Carga() {
        return id_Carga;
    }

    public void setId_Carga(Long id_Carga) {
        this.id_Carga = id_Carga;
    }

    public String getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(String maxDate) {
        this.maxDate = maxDate;
    }

    public String getMaxDateU() {
        return maxDateU;
    }

    public void setMaxDateU(String maxDateU) {
        this.maxDateU = maxDateU;
    }

    public String getMonthsDep() {
        return monthsDep;
    }

    public void setMonthsDep(String monthsDep) {
        this.monthsDep = monthsDep;
    }

    public String getMonthsInd() {
        return monthsInd;
    }

    public void setMonthsInd(String monthsInd) {
        this.monthsInd = monthsInd;
    }
    
    public String getContributingNameU() {
        return contributingNameU;
    }

    public void setContributingNameU(String contributingNameU) {
        this.contributingNameU = contributingNameU;
    }

    public String getNumberIdentification() {
        return numberIdentification;
    }

    public void setNumberIdentification(String numberIdentification) {
        this.numberIdentification = numberIdentification;
    }

    public String getNumberIdentificationContributingU() {
        return numberIdentificationContributingU;
    }

    public void setNumberIdentificationContributingU(
        String numberIdentificationContributingU) {
        this.numberIdentificationContributingU =
            numberIdentificationContributingU;
    }

    public String getContributingSubTypeU() {
        return contributingSubTypeU;
    }

    public void setContributingSubTypeU(String contributingSubTypeU) {
        this.contributingSubTypeU = contributingSubTypeU;
    }

    public String getTypeU() {
        return typeU;
    }

    public void setTypeU(String typeU) {
        this.typeU = typeU;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getTypePayrollU() {
        return typePayrollU;
    }

    public void setTypePayrollU(String typePayrollU) {
        this.typePayrollU = typePayrollU;
    }

    public String getIbcMayorU() {
        return ibcMayorU;
    }

    public void setIbcMayorU(String ibcMayorU) {
        this.ibcMayorU = ibcMayorU;
    }

    public String getSalarioBasicoU() {
        return salarioBasicoU;
    }

    public void setSalarioBasicoU(String salarioBasicoU) {
        this.salarioBasicoU = salarioBasicoU;
    }

    public String getTipoIdAportanteU() {
        return tipoIdAportanteU;
    }

    public void setTipoIdAportanteU(String tipoIdAportanteU) {
        this.tipoIdAportanteU = tipoIdAportanteU;
    }

    public String getDiasDep() {
        return diasDep;
    }

    public void setDiasDep(String diasDep) {
        this.diasDep = diasDep;
    }

    public String getPeriodosComo52() {
        return periodosComo52;
    }

    public void setPeriodosComo52(String periodosComo52) {
        this.periodosComo52 = periodosComo52;
    }

    public String getPrimerPeriodoComo52() {
        return primerPeriodoComo52;
    }

    public void setPrimerPeriodoComo52(String primerPeriodoComo52) {
        this.primerPeriodoComo52 = primerPeriodoComo52;
    }

    public String getUltimoPeriodoComo52() {
        return ultimoPeriodoComo52;
    }

    public void setUltimoPeriodoComo52(String ultimoPeriodoComo52) {
        this.ultimoPeriodoComo52 = ultimoPeriodoComo52;
    }

    public String getPrimerPeriodoU() {
        return primerPeriodoU;
    }

    public void setPrimerPeriodoU(String primerPeriodoU) {
        this.primerPeriodoU = primerPeriodoU;
    }
    
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Id_Carga=").append(id_Carga).
            append("~&~tipoDocumento=").append(documentType).
            append("~&~numeroDocumento=").append(numberIdentification).
            append("~&~Tipo Identificaci�n Aportante �ltimo Periodo=").append(tipoIdAportanteU).
            append("~&~numeroDocumentoAportanteU=").append(numberIdentificationContributingU).
            append("~&~nombreAportanteU=").append(contributingNameU).
            append("~&~maxFechaU=").append(maxDateU).
            append("~&~Primer Periodo cotizado con �ltimo aportante=").append(primerPeriodoU).
            append("~&~codigoAfpu=").append(afpuCode).
            append("~&~codigoEpsu=").append(epsuCode).
            append("~&~cotizaciones=").append(quotes).
            append("~&~diasCotizadosSalud=").append(healthQuotesDays).
            append("~&~diasInd=").append(daysInd).
            append("~&~fechaCortePila=").append(dateCutPila).
            append("~&~fechaPagoU=").append(paidDateU).
            append("~&~maxFecha=").append(maxDate).
            append("~&~mesesDep=").append(monthsDep).
            append("~&~mesesInd=").append(monthsInd).
            append("~&~subTipoCotizanteU=").append(contributingSubTypeU).
            append("~&~tipoPlanillaU=").append(typePayrollU).
            append("~&~IBC Salud o Pensi�n del �ltimo Periodo=").append(ibcMayorU).
            append("~&~Salario B�sico=").append(salarioBasicoU).
            append("~&~Tipo Identificaci�n Aportante �ltimo Periodo=").append(tipoIdAportanteU).
            append("~&~D�as cotizados como dependiente=").append(diasDep).
            append("~&~Suma Periodos como cotizante 52 desde junio 2013=").append(periodosComo52).
            append("~&~Primer periodo de cotizaci�n como cotizante 52=").append(primerPeriodoComo52).
            append("~&~�ltimo periodo de coizaci�n como cotizante 52=").append(ultimoPeriodoComo52);
        return stringBuilder.toString();
    }   

}
