/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IMailSender.java
 * Created on: 2016/10/31, 03:25:02 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.commons.mail.sender;

/**
 * Interfaz general para el env&icute;o de notificaciones
 *
 * @author <a href="mailto:julianm@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
public interface Notificator {

    /**
     * Env&iacute;a un email.
     *
     * @param to        Destinatario.
     * @param message   Mensaje.
     * @param addressee nombre del destinatario
     */
    public void sendNotification(String to, String message, String addressee,
        String subjet);
}
