/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserController.java
 * Created on: 2016/10/19, 02:14:58 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.gml.simpc.api.entity.Parameter;
import com.gml.simpc.api.services.ParameterService;
import static com.gml.simpc.api.utilities.Util.ADMIN_PATH;
import static com.gml.simpc.api.utilities.Util.HELP_PATH;
import static com.gml.simpc.api.utilities.Util.JAR_PATH;
import static com.gml.simpc.api.utilities.Util.MANUAL_PATH;
import com.gml.simpc.commons.annotations.FunctionalityInterceptor;
import com.gml.simpc.web.controller.ControllerDefinition;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controlador para la ventana de manejo de manuales usuarios.
 *
 * @author <a href="mailto:jonathanp@gmlsoftware.com">Jonathan Ponton</a>
 */
@FunctionalityInterceptor(name = {
    "manuales-usuarios.htm", "descargaManual.pdf",
    "descargaAyuda.pdf", "descargaManualAdmin.pdf"
})
@Controller
public class HelpController implements ControllerDefinition {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(HelpController.class);
    /**
     * Servicio de par&aacute;metros.
     */
    @Autowired
    private ParameterService parameterService;

    @RequestMapping(value = "manuales-usuarios", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response) {
        LOGGER.info("Ejecutando metodo [ onDisplay HelpController ]");

        ModelAndView modelAndView =
            new ModelAndView("manuales-usuarios");

        return modelAndView;
    }

    /**
     * M&eacute;todo para descargar el manual de usuarios.
     * 
     * @param request
     * @param response 
     */
    @RequestMapping(value = "descargaManual.pdf", method = RequestMethod.GET)
    public void downloadManual(HttpServletRequest request,
        HttpServletResponse response) {

        try {
            Parameter parameter = this.parameterService.
                findByName(MANUAL_PATH);

            File manual = new File(parameter.getValue());
            try (InputStream is = new FileInputStream(manual)) {
                IOUtils.copy(is, response.getOutputStream());
            }
            response.setContentType("application/pdf");
            response.flushBuffer();
        } catch (Exception ex) {
            LOGGER.info("Error al descargar archivo.", ex);
        } finally {
            LOGGER.info("Finaliza la invocacion de [ downloadManual ].");
        }
    }

    /**
     * M&eacute;etodo para descargar el manual de ayuda.
     * 
     * @param request
     * @param response 
     */
    @RequestMapping(value = "descargaAyuda.pdf", method = RequestMethod.GET)
    public void downloadOnlineHelp(HttpServletRequest request,
        HttpServletResponse response) {

        try {
            Parameter parameter = this.parameterService.
                findByName(HELP_PATH);

            File manual = new File(parameter.getValue());
            try (InputStream is = new FileInputStream(manual)) {
                IOUtils.copy(is, response.getOutputStream());
            }
            response.setContentType("application/pdf");
            response.flushBuffer();
        } catch (Exception ex) {
            LOGGER.info("Error al descargar archivo.", ex);

        } finally {
            LOGGER.info("Finaliza la invocacion de [ downloadOnlineHelp ].");
        }
    }
    
    /**
     * M&eacute;todo para descargar el archivo .jar (MD Loader).
     * 
     * @param request
     * @param response 
     */
    @RequestMapping(value = "descargaJar.jar", method = RequestMethod.GET)
    public void downloadJar(HttpServletRequest request,
        HttpServletResponse response) {

        try {
            Parameter parameter = this.parameterService.
                findByName(JAR_PATH);

            File manual = new File(parameter.getValue());
            try (InputStream is = new FileInputStream(manual)) {
                IOUtils.copy(is, response.getOutputStream());
            }
            response.setContentType("application/java-archive");
            response.flushBuffer();
        } catch (Exception ex) {
            LOGGER.info("Error al descargar archivo.", ex);

        } finally {
            LOGGER.info("Finaliza la invocacion de [ downloadJar ].");
        }
    }

    /**
     * M&eacute;etodo para descargar el manual de administrador.
     * 
     * @param request
     * @param response 
     */
    @RequestMapping(value = "descargaManualAdmin.pdf", method =
        RequestMethod.GET)
    public void downloadAdminManual(HttpServletRequest request,
        HttpServletResponse response) {

        try {
            Parameter parameter = this.parameterService.
                findByName(ADMIN_PATH);

            File manual = new File(parameter.getValue());
            try (InputStream is = new FileInputStream(manual)) {
                IOUtils.copy(is, response.getOutputStream());
            }
            response.setContentType("application/pdf");
            response.flushBuffer();
        } catch (Exception ex) {
            LOGGER.info("Error al descargar archivo.", ex);
        } finally {
            LOGGER.info("Finaliza la invocacion de [ descargaManualAdmin ].");
        }
    }
}
