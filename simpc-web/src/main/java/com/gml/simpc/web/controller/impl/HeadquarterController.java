/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 **
 * Document: HeadquarterController.java
 * Created on: 2016/11/17, 07:34:03 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.gml.simpc.api.entity.Approval;
import com.gml.simpc.api.entity.Headquarter;
import com.gml.simpc.api.entity.Institution;
import com.gml.simpc.api.entity.Session;
import com.gml.simpc.api.entity.User;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_001_UNKNOW;
import com.gml.simpc.api.entity.valuelist.City;
import com.gml.simpc.api.entity.valuelist.State;
import com.gml.simpc.api.enums.HeadquarterStatus;
import com.gml.simpc.api.services.ApprovalService;
import com.gml.simpc.api.services.HeadquarterService;
import com.gml.simpc.api.services.InstitutionService;
import com.gml.simpc.api.services.UserService;
import static com.gml.simpc.api.utilities.Util.FAILED_ALERT;
import static com.gml.simpc.api.utilities.Util.PARAMETER_SAVE_HEADQUARTER;
import static com.gml.simpc.api.utilities.Util.SESSION_USER;
import static com.gml.simpc.api.utilities.Util.SUCCESS_ALERT;
import com.gml.simpc.commons.annotations.FunctionalityInterceptor;
import com.gml.simpc.commons.mail.sender.Notificator;
import com.gml.simpc.web.controller.ControllerDefinition;
import com.gml.simpc.web.repository.HeadquarterRepository;
import com.gml.simpc.web.utilities.ValueListService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controlador para el manejo de Sedes.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@FunctionalityInterceptor(name = "instituciones-oferentes.htm")
@Controller
public class HeadquarterController implements ControllerDefinition {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER
            = Logger.getLogger(HeadquarterController.class);
    /**
     * Formateador de fechas.
     */
    private final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    /**
     * Servicio para adminisraci&oacute;n de instituciones oferentes; Servicio
     * de instutuciones intermediario para la comunicaci&oacute;n con la 
     * persistencia.
     */
    @Autowired
    private InstitutionService institutionService;
    /**
     * Servicio para adminisraci&oacute;n de instituciones oferentes; servicio
     * de instutuciones intermediario para la comunicaci&oacute;n con la
     * persistencia.
     */
    @Autowired
    private ApprovalService approvalService;
    /**
     * Servicio para adminisraci&oacute;n de sedes; Servicio de sedes
     * intermediario para la comunicaci&oacute;n con la persistencia.
     */
    @Autowired
    private HeadquarterService headquarterService;
    /**
     * Servicio para listas de valores.
     */
    @Autowired
    private ValueListService valueListService;

    @Autowired
    private Notificator notificator;
    /**
     * Servicio de usuarios.
     */
    @Autowired
    private UserService userService;
    /**
     * Repositorio para las sedes.
     */
    @Autowired
    private HeadquarterRepository headquarterRepository;

    /**
     * M&eacute;todo que se ejecuta al abrir la ventana de index; Trae listas
     * necesarias para el funcionamiento de la ventana.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "detalle-institucion-oferente",
            method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
            HttpServletResponse response) {
        LOGGER.info("entra a consultar display sede");

        Long id = Long.parseLong(request.getParameter("id"));
        Boolean notSedes = null;
        if (request.getParameter("notSedes") != null) {
            notSedes = Boolean.parseBoolean(request.getParameter("notSedes"));
        }
        Institution parent = null;

        ModelAndView modelAndView
                = new ModelAndView("detalle-institucion-oferente");

        if (notSedes != null) {
            modelAndView.addObject("notSedes", notSedes);
        }

        try {
            parent = this.institutionService.findById(id);
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("finally de display sedes");
        }

        init(modelAndView, parent, null);
        return modelAndView;
    }

    /**
     * m&eacute;todo que consulta las instituciones oferentes de acuerdo a lo
     * diligenciado en los filtros de formulario de consulta.
     *
     * @param request
     * @param response
     * @param searchHeadquarter
     *
     * @return
     */
    @RequestMapping(value = "buscarSedes",
            method = RequestMethod.GET)
    public ModelAndView search(HttpServletRequest request,
            HttpServletResponse response,
            @ModelAttribute("searchHeadquarter") Headquarter searchHeadquarter) {
        LOGGER.info("entra a consultar en el controller de sedes");

        Institution parentInstitution = institutionService.findById(
                searchHeadquarter.getInstitutionId());

        ModelAndView modelAndView
                = new ModelAndView("detalle-institucion-oferente");

        try {
            List<Headquarter> headquarterList = this.headquarterService.
                    findByFilters(searchHeadquarter);
            modelAndView.addObject("headquarterList", headquarterList);
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("finally de consultar en HeadquarterController");
        }

        init(modelAndView, parentInstitution, null);

        return modelAndView;
    }

    /**
     * M&eacute;todo que guarda una sede.
     * 
     * @param request
     * @param response
     * @param creationHeadquarter
     * @return 
     */
    @RequestMapping(value = "guardarSede",
            method = RequestMethod.POST)
    public ModelAndView save(HttpServletRequest request,
            HttpServletResponse response, @Valid @ModelAttribute(
                    "creationHeadquarter"
            ) Headquarter creationHeadquarter) {

        ModelAndView modelAndView
                = new ModelAndView("detalle-institucion-oferente");
        Institution parent = institutionService.findById(
                creationHeadquarter.getInstitutionId());

        Session session = (Session) request.getSession().getAttribute(
                SESSION_USER);

        boolean msnNuevaSede;

        msnNuevaSede = creationHeadquarter.getId() == null;

        try {
            /*
             * Guardamos la instituci&oacute;n oferente
             */
            creationHeadquarter.setCreationDate(new Date());
            User userSession = session.getUser();

            if ("true".equals(request.getParameter("principalStatus"))) {
                creationHeadquarter.setPrincipal(true);
            } else {
                creationHeadquarter.setPrincipal(false);
            }
            
            creationHeadquarter.setCreationUserId(userSession.getId());
            
            this.headquarterService.save(creationHeadquarter);

            this.institutionService.updateApprovalState('N', userSession.
                    getId(), new Date(), creationHeadquarter.getInstitutionId());
            String rejection;
            if (msnNuevaSede) {
                rejection = "Se ha creado la sede " + creationHeadquarter.getName() + " exitosamente.\n\n" +
                        "Institución a la que pertenece: " + creationHeadquarter.getInstitution().getInstitutionName();
            } else {
                rejection = "Se ha modificado la sede " + creationHeadquarter.getName() + " exitosamente.\n\n" +
                        "Institución a la que pertenece: " + creationHeadquarter.getInstitution().getInstitutionName() + "\n\n" + 
                        "Causa de la modificación: " + request.getParameter("message-text");
            }

            Approval approval = new Approval(creationHeadquarter.
                    getInstitutionId(), userSession,
                    new Date(), rejection);

            this.approvalService.save(approval);

            for (User usuario : this.userService.findByAdmin(true)) {
                notificator.sendNotification(usuario.getEmail(),
                        String.format(rejection, request.
                                getParameter("name")), usuario.getFirstName().
                        concat(" ").
                        concat(usuario.getFirstSurname()),
                        PARAMETER_SAVE_HEADQUARTER);
            }
            if (msnNuevaSede) {
                modelAndView.addObject("msg", "Se ha creado la sede "
                        + "exitosamente.");
                modelAndView.addObject("msgType", SUCCESS_ALERT);
            } else {
                modelAndView.addObject("msg", "Se ha modificado la sede "
                        + "exitosamente.");
                modelAndView.addObject("msgType", SUCCESS_ALERT);
            }
        } catch (ConstraintViolationException ex) {
            LOGGER.error("Error", ex);

            StringBuilder builder = new StringBuilder("Alertas: ");

            for (ConstraintViolation error : ex.getConstraintViolations()) {
                builder.append(error.getMessage());
                builder.append("\n");
            }
            modelAndView.addObject("internalMsg", builder.toString());
            modelAndView.addObject("creationHeadquarter",
                    creationHeadquarter);
            modelAndView.addObject("msgType", FAILED_ALERT);
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
            modelAndView.addObject("msgType", FAILED_ALERT);
        } finally {
            LOGGER.info("finally save in HeadquarterController");
        }

        init(modelAndView, parent, null);

        return modelAndView;
    }

    /**
     * M&eacute;todo que se ejecuta al abrir la ventana de creaci&oacute;n y
     * actualizaci&oacute;n de sedes.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "crear-sede", method = RequestMethod.GET)
    public ModelAndView create(HttpServletRequest request,
            HttpServletResponse response
    ) {
        LOGGER.info("entra a crear sede");

        ModelAndView modelAndView = new ModelAndView("crear-sede");
        Headquarter creationHeadquarter = null;
        Long parentId = Long.parseLong(request.getParameter("parentId"));
        Institution parent = null;
        int cont = 0;
        boolean ultimateHQ = true;

        if (request.getParameter("id") != null) {

            List<Headquarter> sedes = this.headquarterRepository.
                    findByInstitutionId(parentId);
            for (Headquarter sede : sedes) {
                if (sede.getStatus() == HeadquarterStatus.A) {
                    cont++;
                    if (cont == 2) {
                        ultimateHQ = false;
                        break;
                    }
                }
            }
        }
        try {
            if (request.getParameter("id") != null) {
                Long id = Long.parseLong(request.getParameter("id"));
                creationHeadquarter = this.headquarterService.findById(id);
            }
            try {
                parent = this.institutionService.findById(parentId);

                if (request.getParameter("id") != null && ultimateHQ == true) {
                    modelAndView.addObject("institutionParentName", parent.
                            getInstitutionName());
                }

                Headquarter principalHeadquarter = this.headquarterService.
                        findPrincipalByInstitution(parent);

                String headquarterName = principalHeadquarter.
                        getName();

                modelAndView.addObject("headquarterName", headquarterName);

                LOGGER.info("Nombre de sede principal: " + principalHeadquarter.
                        getName());
            } catch (Exception ex) {
                LOGGER.error("La instituion no tiene sedes principales", ex);
            }
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("finally de crear sedes");
        }

        init(modelAndView, parent, creationHeadquarter);

        return modelAndView;
    }

    /**
     * M&eacute;todo que se ejecuta al abrir la ventana de creaci&oacute;n y
     * actualizaci&oacute;n de sedes.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "borrarSede", method = RequestMethod.POST)
    public ModelAndView delete(HttpServletRequest request,
            HttpServletResponse response
    ) {
        LOGGER.info("entra a eliminar sede");

        ModelAndView modelAndView
                = new ModelAndView("detalle-institucion-oferente");
        Long parentId = Long.parseLong(request.getParameter("parentId"));
        Institution parent = null;

        try {
            if (request.getParameter("id") != null) {
                parent = this.institutionService.findById(parentId);
                Long id = Long.parseLong(request.getParameter("id"));

                this.headquarterService.
                        remove(this.headquarterService.findById(id));
            }
            modelAndView.addObject("msg", "Se ha eliminado la sede "
                    + "exitosamente.");
            modelAndView.addObject("msgType", SUCCESS_ALERT);
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg", ex.getMessage());
            modelAndView.addObject("msgType", FAILED_ALERT);
        } finally {
            LOGGER.info("finally de eliminar sedes");
        }

        init(modelAndView, parent, null);

        return modelAndView;
    }

    /**
     * Inicia los objetos para el controler.
     *
     * @param modelAndView
     * @param parent
     * @param headquarter
     */
    private void init(ModelAndView modelAndView, Institution parent,
            Headquarter headquarter) {
        LOGGER.info("Ejecutando metodo [ init ]");

        modelAndView.addObject("headquarterStatusList", HeadquarterStatus.
                values());

        if (parent != null) {
            if (parent.getQualityCertificateExpiration() != null) {
                parent.setDateToShow(format.format(parent.
                        getQualityCertificateExpiration()));
            }
        }

        modelAndView.addObject("parentInstitution", parent);
        modelAndView.addObject("searchHeadquarter",
                new Headquarter(new City(), new State(), parent));

        if (headquarter != null) {
            modelAndView.addObject("creationHeadquarter", headquarter);
        } else {
            modelAndView.addObject("creationHeadquarter",
                    new Headquarter(new City(), new State(), parent));
        }

        modelAndView.addObject("stateList",
                this.valueListService.getStateList());
        modelAndView.addObject("citiesList",
                this.valueListService.getCityList());
    }
}
