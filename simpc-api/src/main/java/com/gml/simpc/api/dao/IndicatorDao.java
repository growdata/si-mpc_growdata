/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IndicatorDao.java
 * Created on: 2017/04/30, 01:13:59 PM
 * Project: MPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dao;

import com.gml.simpc.api.dto.GraphicDataDto;
import com.gml.simpc.api.dto.Page;
import com.gml.simpc.api.dto.PieGraphicDto;
import com.gml.simpc.api.dto.TableDataDto;
import java.util.List;

/**
 * Interfaz que define el DAO para consultar la informacion relacionada a los
 * indicadores
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
public interface IndicatorDao {

    /**
     * Obtiene la informacion para graficar en un plano cartesiano XY
     *
     * @return
     */
    List<GraphicDataDto> getDataXYGraphic(String query);

    /**
     * Obtiene la informacion para graficar un Pie
     *
     * @return
     */
    List<PieGraphicDto> getDataPieGraphic(String query);

    /**
     * Obtiene la tabla plana a mostrar en el indicador
     *
     * @return
     */
    List<TableDataDto> getData(String query);
    /**
     * Obtiene los nombre de las columnas de la tabla plana a mostrar en el indicador
     *
     * @return
     */
    List<TableDataDto> getColumnName(String query);
    
    Page<TableDataDto> getDataTablePage(int pageNo, int pageSize,
        String query);
    
    List <TableDataDto> getDataForFiles(String query);

}
