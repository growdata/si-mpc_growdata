/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IService.java
 * Created on: 2016/10/24, 11:31:09 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.entity.Resource;
import java.util.List;

/**
 *
 * @author <a href="mailto:julianm@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
public interface ResourcesService {

    /**
     * Obtiene todos los usuarios creados.
     *
     * @return
     */
    List<Resource> getAll();

    /**
     * Consulta por id de perfil.
     *
     * @param idProfile
     *
     * @return
     */
    List<Resource> findByIdProfile(Long idProfile);

    /**
     * Servicio para guardar recursos.
     *
     * @param resource
     */
    void save(Resource resource);

    /**
     * Servicio para actualizar recursos.
     *
     * @param resource
     */
    void update(Resource resource);

    /**
     * Elimina un recurso.
     *
     * @param resources
     */
    void remove(Resource resources);
    
    /**
     * Elimina un perfil en recursos.
     *
     * @param resources
     */
    void deleteByProfileId(Long profileID);
}
