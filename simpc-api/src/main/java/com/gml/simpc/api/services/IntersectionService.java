/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IntersectionService.java
 * Created on: 2017/02/07, 04:09:28 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.entity.Intersection;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.entity.exeption.UserException;
import java.util.Date;
import java.util.List;

/**
 * Servicio para los cruces.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface IntersectionService {

    /**
     * M&eacute;todo por el cual inicia el cruce.
     *
     * @param user
     * @param file
     */
    void startIntersection(User user, byte[] file,String fileName) throws UserException;
    
    /**
     * M&eacute;todo gurdar Cruce individual
     *
     * @param user
     */
    void saveIndividualIntersection(User user,String path );
    
    /**
     * Consulta los archivos generados en la fecha
     *
     * @param endDate
     *
     * @return
     */
    public List<Intersection> findByEndDateRange(Date startDate,Date endDate,
        String searchType, String ccf );
    
    /**
     * Consulta un cruce por id;
     * @param id
     * @return 
     */
    Intersection findById(Long id);
}
