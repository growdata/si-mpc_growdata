/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ActionLog.java
 * Created on: 2016/11/11, 05:40:42 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entidad que maneja el log de acciones.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Table(name = "ACCIONES_LOG")
@Entity
public class ActionLog implements Serializable {

    /**
     * ID de la instituci&oacute;n.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator =
        "log_actions_seq")
    @SequenceGenerator(name = "log_actions_seq", sequenceName =
        "LOG_ACTIONS_SEQ", allocationSize = 1, initialValue= 1)
    private Long id;
    /**
     * Usuario que ejecuta la acci&oacute;n.
     */
    @ManyToOne
    @JoinColumn(name = "USUARIO", nullable = false)
    private User user;
    /**
     * Fecha en que se ejecuta la acci&oacute;n.
     */
    @Column(name = "FECHA_MODIFICACION")
    private Date fecha;
    /**
     * Acci&oacute;n que se ejecuta.
     */
    @Column(name = "ACCION")
    private String action;
    /**
     * Par&aacute;metros de la ejecuci&oacute;n.
     */
    @Column(name = "PARAMETROS", columnDefinition = "CLOB")
    private String parameters;
    /**
     * Tabla asociada a la ejecuci&oacute;n.
     */
    @Column(name = "TABLA_ASOCIADA")
    private String entity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }
}
