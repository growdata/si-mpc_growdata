<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
    </head>
    <body>
        <c:if test="${not empty msg}">
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

        <h1>Cruces de Informaci&oacute;n</h1>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#individual" aria-controls="home" role="tab" data-toggle="tab">Consultas individuales</a></li>
            <li role="presentation"><a href="#masiva" aria-controls="profile" role="tab" data-toggle="tab">Consultas Masivas</a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content" style="padding:10px;margin-bottom:30px; border:solid thin #DEDEDE;">
            <div role="tabpanel" class="tab-pane active" id="individual">
                <div class="panel panel-default">
                    <div class="panel-heading"><h2>B&uacute;squeda</h2></div>
                    <div class="panel-body">
                        <legend>
                            <form:form method="post" action="/masterdata/cruce-individual.htm"> 
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="exampleInputName2" class="control-label">Tipo de Documento</label>     
                                        <select class="form-control" name="identificationType" required="true">  
                                            <option label="Seleccione ..." selected></option>
                                            <c:if test="${not empty documentTypeList}">

                                                <c:forEach var="item" items="${documentTypeList}">
                                                    <c:if test="${item.name!='RC' && item.name!='NIUP' && item.name!='NIT' && item.name!='RUT'}">
                                                        <option label="${item.name}" value="${item.name}">${item.name}</option>
                                                    </c:if>
                                                </c:forEach>
                                            </c:if>
                                        </select>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail2" class="control-label">N&uacute;mero de Identificaci&oacute;n</label>

                                        <input type="text" class="form-control" id="exampleInputEmail2" placeholder="N�mero" required="required" name="numberIdentification">
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success pull-right">Buscar</button>
                            </form:form>

                        </legend>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <hr>
                <h2>Resultados</h2>
                <hr>	
                <!-- Nav tabs -->
                <ul class="nav nav-pills nav-stacked col-sm-3" role="tablist">
                    <li role="presentation" class="active"><a href="#pila" aria-controls="home" role="tab" data-toggle="tab">PILA</a></li>
                    <li role="presentation"><a href="#prestaciones" aria-controls="prestaciones" role="tab" data-toggle="tab">Recursos FOSFEC</a></li>
                    <li role="presentation"><a href="#dependencias" aria-controls="dependencias" role="tab" data-toggle="tab">Dependientes Econ�micos</a></li>
                    <li role="presentation"><a href="#hojadevida" aria-controls="hojadevida" role="tab" data-toggle="tab">Hoja de Vida</a></li>
                    <li role="presentation"><a href="#intermediacion" aria-controls="intermediacion" role="tab" data-toggle="tab">Intermediaci�n</a></li>
                    <li role="presentation"><a href="#capacitacion" aria-controls="capacitacion" role="tab" data-toggle="tab">Orientaci�n y Capacitaci�n</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content col-sm-9">
                    <div role="tabpanel" class="tab-pane active" id="pila">
                        <div class="table-responsive col-md-12" >
                            <table class="table table-striped table-bordered" cellspacing="0" > 
                                <thead> 
                                    <tr> 
                                        <c:choose>
                                            <c:when  test="${profileUserType == 'UAESPE'}">
                                                <th>Id De Carga</Th>
                                                <th>Tipo De Identificaci&oacute;N Del Cotizante</Th>
                                                <Th>Numero De Identificaci&oacute;N Del Cotizante</Th>
                                                <Th>Tipo De Identificaci&oacute;N Del Aportante &uacute;Ltimo Periodo </Th>
                                                <Th>Numero De Identificaci&oacute;N Del Aportante &uacute;Ltimo Periodo Cotizado</Th>
                                                <Th>Nombre Del Aportante En El &uacute;Ltimo Periodo De Cotizaci&oacute;N</Th>
                                                <Th>&uacute;Ltimo Periodo Cotizado</Th>
                                                <Th>Primer Periodo De Cotizaci&oacute;N Con El Aportante Del &uacute;Ltimo Periodo Cotizado</Th>
                                            </c:when >
                                            <c:otherwise>
                                                <th>Id De Carga
                                                <th>Tipo De Identificaci�n Del Cotizante</th>
                                                <th>Numero De Identificaci�n Del Cotizante</th>
                                                <th>Tipo De Identificaci�n Del Aportante �ltimo Periodo </th>
                                                <th>Numero De Identificaci�n Del Aportante �ltimo Periodo Cotizado</th>
                                                <th>Nombre Del Aportante En El �ltimo Periodo De Cotizaci�n</th>
                                                <th>�ltimo Periodo Cotizado</th>
                                                <th>Primer Periodo De Cotizaci�n Con El Aportante Del �ltimo Periodo Cotizado</th>
                                                <th>C�digo Afp �ltimo Periodo Cotizado</th>
                                                <th>C�digo Eps �ltimo Periodo Cotizado</th>
                                                <th>N�mero De Cotizaciones �ltimo Periodo</th>
                                                <th>D�as Cotizados  �ltimo Periodo</th>
                                                <th>D�as Cotizados A Cajas De Compensaci�n Familiar En Calidad De Independiente</th> 
                                                <th>Ultima Fecha De Corte</th>
                                                <th>Fecha Pago �ltimo Periodo Cotizado</th>
                                                <th>�ltimo Periodo  Cotizado  A Ccf</th>
                                                <th>Meses De Aportes Como Dependiente</th>
                                                <th>Meses De Aportes Como Independiente</th>
                                                <th>Subtipo De Cotizante �ltimo Periodo Cotizado</th>
                                                <th>Tipo De Cotizante �ltimo Periodo Cotizado</th>
                                                <th>Tipo De Planilla �ltimo Periodo Cotizado</th>
                                                <th>Ibc Salud O Pensi�n Del �ltimo Periodo Cotizado</th>
                                                <th>Salario B�sico</th>
                                                <th>D�as Cotizados A Cajas De Compensaci�n Familiar En Calidad De Dependiente</th>
                                                <th>Suma De Periodos Como Cotizante 52 Desde Junio De 2013</th>
                                                <th>Primer Periodo De Cotizaci�n Con Tipo De Cotizante </th>
                                                <th>�ltimo Periodo De Cotizaci�n Con Tipo De Cotizante 52</th>
                                            </c:otherwise>
                                        </c:choose>
                                    </tr> 
                                </thead> 
                                <tbody> 
                                    <c:if test="${profileUserType == 'UAESPE'}">
                                        <c:forEach var="item" items="${resultsPilaEmploymentService}">
                                            <tr > 
                                                <td>${item.id_Carga}</td>
                                                <td>${item.documentType}</td>
                                                <td>${item.numberIdentification}</td>
                                                <td>${item.tipoIdAportanteU}</td>
                                                <td>${item.numberIdentificationContributingU}</td>
                                                <td>${item.contributingNameU}</td>
                                                <td>${item.maxDateU}</td>
                                                <td>${item.primerPeriodoU}</td>
                                            </tr> 
                                        </c:forEach>
                                    </c:if>
                                    <c:if test="${not empty resultsPila and profileUserType != 'UAESPE'}">
                                        <c:forEach var="item" items="${resultsPila}">
                                            <tr>         
                                                <td>${item.id_Carga}</td>
                                                <td>${item.documentType}</td>
                                                <td>${item.numberIdentification}</td>
                                                <td>${item.tipoIdAportanteU}</td>
                                                <td>${item.numberIdentificationContributingU}</td>
                                                <td>${item.contributingNameU}</td>
                                                <td>${item.maxDateU}</td>
                                                <td>${item.primerPeriodoU}</td>
                                                <td>${item.afpuCode}</td>
                                                <td>${item.epsuCode}</td>
                                                <td>${item.quotes}</td>
                                                <td>${item.healthQuotesDays}</td>
                                                <td>${item.daysInd}</td>
                                                <td>${item.dateCutPila}</td>
                                                <td>${item.paidDateU}</td>
                                                <td>${item.maxDate}</td>
                                                <td>${item.monthsDep}</td>
                                                <td>${item.monthsInd}</td>
                                                <td>${item.contributingSubTypeU}</td>
                                                <td>${item.typeU}</td>
                                                <td>${item.typePayrollU}</td>
                                                <td>${item.ibcMayorU}</td>
                                                <td>${item.salarioBasicoU}</td>
                                                <td>${item.diasDep}</td>
                                                <td>${item.periodosComo52}</td>
                                                <td>${item.primerPeriodoComo52}</td>
                                                <td>${item.ultimoPeriodoComo52}</td>
                                            </tr> 
                                        </c:forEach>
                                    </c:if>
                                </tbody> 
                            </table>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="prestaciones">
                        <c:choose>
                            <c:when test="${profileUserType == 'UAESPE'}">
                                <div class="container">
                                    <div class="alert alert-warning alert-dismissible" id="myAlert">
                                        <font SIZE=3>Este contenido no est� disponible para el perfil asociado a este usuario.</font>
                                    </div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="table-responsive col-md-12">
                                    <table class="table table-striped table-bordered" cellspacing="0" width="100%" >
                                        <thead> 
                                            <tr class="page-break"> 
                                                <th>Id de Carga</th> 
                                                <th>Tipo Documento</th> 
                                                <th>N�mero Documento</th> 
                                                <th>Tipo Novedad</th> 
                                                <th>Codigo Ccf</th> 
                                                <th>Primer Nombre</th> 
                                                <th>Segundo Nombre</th> 
                                                <th>Primer Apellido</th> 
                                                <th>Segundo Apellido</th> 
                                                <th>Fecha Nacimiento</th> 
                                                <th>Genero</th> 
                                                <th>Departamento Residencia</th> 
                                                <th>Ciudad Residencia</th> 
                                                <th>Rango Salarial</th> 
                                                <th>Inscripcion Servicio Empleo</th> 
                                                <th>Tipo Vinculacion Ccf</th> 
                                                <th>Ultima Ccf</th> 
                                                <th>Fecha Postulacion Mpc</th> 
                                                <th>Administradora Salud</th> 
                                                <th>Postula Pension</th> 
                                                <th>Administradora Pension</th> 
                                                <th>Subsidio Cuota Monetaria</th> 
                                                <th>Ahorro Cesantias Mpc</th> 
                                                <th>Administradora Fondo Cesantias</th> 
                                                <th>Porcentaje Ahorro Cesantias</th> 
                                                <th>Postula Bono Alimentacion</th> 
                                                <th>Verificacion Requisitos</th> 
                                                <th>Fecha Verificacion Postulaci�n</th> 
                                                <th>Beneficiario Del Mecanismo</th> 
                                                <th>Cumple Ruta De Empleabildad</th> 
                                                <th>Fecha Liquidacion Beneficios</th> 
                                                <th>Periodo Beneficio Liquidado</th> 
                                                <th>Numero Beneficios Liquidados</th> 
                                                <th>Numero Cuotas Liquidadas</th> 
                                                <th>Numero Personas Cargo</th> 
                                                <th>Numero Cuotas Incentivo</th> 
                                                <th>Numero Bonos Liquidados</th> 
                                                <th>Monto Liquidado Salud</th> 
                                                <th>Monto Liquidado Pension</th> 
                                                <th>Monto Liquidado Cuota</th> 
                                                <th>Monto Liquidado Cesantias</th> 
                                                <th>Monto Liquidado Alimentacion</th> 
                                                <th>Valor Total Prestaciones</th> 
                                                <th>Renuncia Voluntaria</th> 
                                                <th>Fecha Renuncia</th> 
                                                <th>Perdida Beneficio</th> 
                                                <th>Fecha Perdida Beneficio</th> 
                                                <th>Terminacion Beneficio</th> 
                                                <th>Fecha Terminacion Beneficio</th> 
                                            </tr> 
                                        </thead> 
                                        <tbody> 
                                            <c:if test="${not empty resultsFosfec}">
                                                <c:forEach var="item" items="${resultsFosfec}">
                                                    <tr class="page-break"> 
                                                        <td>${item.cargaId}</td> 
                                                        <td>${item.unemployedIDType}</td> 
                                                        <td>${item.unemployedID}</td>
                                                        <td>${item.eventType}</td>
                                                        <td>${item.ccfCode}</td>
                                                        <td>${item.firstName}</td>
                                                        <td>${item.secodName}</td>
                                                        <td>${item.firstSurname}</td>
                                                        <td>${item.secondSurname}</td>
                                                        <td>${item.birthday}</td>
                                                        <td>${item.gender}</td>
                                                        <td>${item.residentState}</td>
                                                        <td>${item.residentCity}</td>
                                                        <td>${item.salaryrange}</td>
                                                        <td>${item.publicServiceRegistration}</td>
                                                        <td>${item.vinculationType}</td>
                                                        <td>${item.lastCff}</td>
                                                        <td>${item.postulationDateMpc}</td>
                                                        <td>${item.healthManager}</td>
                                                        <td>${item.retirementPostulation}</td>
                                                        <td>${item.retirementManager}</td>
                                                        <td>${item.econonomicSubsidyFee}</td>
                                                        <td>${item.mpcSavings}</td>
                                                        <td>${item.severanceFundAdministrator}</td>
                                                        <td>${item.percentageSavings}</td>
                                                        <td>${item.feedBonusPostulation}</td>
                                                        <td>${item.requirements}</td>
                                                        <td>${item.postulationDateCheck}</td>
                                                        <td>${item.beneficiary}</td>
                                                        <td>${item.applyWithRoute}</td>
                                                        <td>${item.settlementDate}</td>
                                                        <td>${item.benefitClearedPeriod}</td>
                                                        <td>${item.numberLiquidBenefi}</td>
                                                        <td>${item.numberMoneyChargesLiquided}</td>
                                                        <td>${item.numberDependents}</td>
                                                        <td>${item.economicIncentiveSavings}</td>
                                                        <td>${item.numbrLiquidBonds}</td>
                                                        <td>${item.helthLiquidedAmount}</td>
                                                        <td>${item.retirementliquidedAmount}</td>
                                                        <td>${item.monetaryLiquidedAmount}</td>
                                                        <td>${item.liquidedAssetAmount}</td>
                                                        <td>${item.feedBonusLiquidedAmount}</td>
                                                        <td>${item.totalValueProfits}</td>
                                                        <td>${item.voluntaryWaiver}</td>
                                                        <td>${item.waiverDate}</td>
                                                        <td>${item.lostBenefit}</td>
                                                        <td>${item.lostBenefitDate}</td>
                                                        <td>${item.benefitEnd}</td>
                                                        <td>${item.benefitEndDate}</td>
                                                    </tr> 
                                                </c:forEach>
                                            </c:if>
                                        </tbody> 
                                    </table>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="dependencias">
                        <c:choose>
                            <c:when test="${profileUserType == 'UAESPE'}">
                                <div class="container">
                                    <div class="alert alert-warning alert-dismissible" id="myAlert">
                                        <font SIZE=3>Este contenido no est� disponible para el perfil asociado a este usuario.</font>
                                    </div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="table-responsive col-md-12">
                                    <table class="table table-striped table-bordered" cellspacing="0" width="100%"  > 
                                        <thead> 
                                            <tr> 
                                                <th>Id de Carga</th> 
                                                <th>Tipo Identificacion</th>
                                                <th>Numero Identificacion</th>
                                                <th>Orden Dependiente</th> 
                                                <th>Tipo Novedad</th>  
                                                <th>Tipo Id Persona Cargo</th> 
                                                <th>Numero Identificacion Cargo</th> 
                                                <th>Persona Cargo</th> 
                                                <th>Primer Nombre</th> 
                                                <th>Segundo Nombre</th> 
                                                <th>Primer Apellido</th> 
                                                <th>Segundo Apellido</th> 
                                                <th>Fecha Nacimiento</th> 
                                                <th>Genero</th> 
                                                <th>Parentesco Personas Cargo</th> 
                                                <th>Persona Cargo Estudia</th>
                                                <th>Fecha de postulaci�n</th> 
                                            </tr> 
                                        </thead> 
                                        <tbody> 
                                            <c:if test="${not empty resultsDependents}">
                                                <c:forEach var="item" items="${resultsDependents}">
                                                    <tr> 
                                                        <td>${item.cargaId}</td>
                                                        <td>${item.idType}</td>
                                                        <td>${item.numberId}</td>
                                                        <td>${item.dependentOrder}</td>
                                                        <td>${item.eventType}</td>
                                                        <td>${item.personInChargeTypeId}</td>
                                                        <td>${item.personInChargeNumber}</td>
                                                        <td>${item.personInCharge}</td>
                                                        <td>${item.firstName}</td>
                                                        <td>${item.secondName}</td>
                                                        <td>${item.firstSurname}</td>
                                                        <td>${item.secondSurname}</td>
                                                        <td>${item.birthday}</td>
                                                        <td>${item.gender}</td>
                                                        <td>${item.personInChargeRelationship}</td>
                                                        <td>${item.personInChargeStudiesCurrently}</td>
                                                        <td>${item.postulationDateCheck}</td>
                                                    </tr> 
                                                </c:forEach>
                                            </c:if>
                                        </tbody> 
                                    </table>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="hojadevida">
                        <div class="table-responsive col-md-12">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%" > 
                                <caption><h2>Datos B�sicos</h2><hr></caption>
                                <thead> 
                                    <tr> 
                                        <th>Id de Carga</th> 
                                        <th>Tipo Documento</th> 
                                        <th>N�mero Documento</th> 
                                        <th>CCF</th>
                                        <th>Agencia Gesti�n Colocaci�n</th>
                                        <th>Fecha Registro</th>
                                        <th>Fecha Ultima Actualizaci�n</th>
                                        <th>Correo Electr�nico</th>
                                        <th>Primer Nombre</th> 
                                        <th>Segundo Nombre</th>
                                        <th>Primer Apellido</th>
                                        <th>Segundo Apellido</th>
                                        <th>Fecha Nacimiento</th>
                                        <th>Sexo</th>
                                        <th>Libreta Militar</th>
                                        <th>Tipo Libreta</th>
                                        <th>No.libreta</th>
                                        <th>Tel�fono Contacto</th>
                                        <th>Estado Civil</th>
                                        <th>Pa�s Nacimiento</th>
                                        <th>Nacionalidad</th>
                                        <th>Departamento Nacimiento</th>
                                        <th>Municipio Nacimiento</th>
                                        <th>Jefe Hogar</th>
                                        <th>Poblaci�n Focalizada</th>
                                        <th>Grupo �tnico</th>
                                        <th>Tipo Poblaci�n</th>
                                        <th>Condiciones discapacidad</th>
                                        <th>Tipo discapacidad</th>
                                        <th>Pais Residencia</th>
                                        <th>Departamento residencia</th>
                                        <th>Municipio Residencia</th>
                                        <th>Direcci�n Residencia</th>
                                        <th>Barrio residencia</th>
                                        <th>Otro tel�fono</th>
                                        <th>Observaciones</th>
                                        <th>Aspiraci�n salarial</th>
                                        <th>Posibilidad Viajar</th>
                                        <th>Posibilidad Trasladarse</th>
                                        <th>Inter�s Ofertas Teletrabajo</th>
                                        <th>Situaci�n Laboral Actual</th>
                                        <th>Propietario Medio de Transporte</th>
                                        <th>Licencia Conducci�n Carro</th>
                                        <th>Categor�a Licencia Carro</th>
                                        <th>Licencia Conducci�n Moto</th>
                                        <th>Categor�a Licencia Moto</th>
                                    </tr> 
                                </thead> 
                                <tbody> 
                                    <c:if test="${not empty resultsBasicInformation}">
                                        <c:forEach var="item" items="${resultsBasicInformation}">
                                            <tr> 
                                                <td>${item.cargaId}</td>
                                                <td>${item.documentType}</td>
                                                <td>${item.numberIdentification}</td>
                                                <td>${item.ccfCode}</td>
                                                <td>${item.agencyCode}</td>
                                                <td>${item.registerDate}</td>
                                                <td>${item.lastModificationDate}</td>
                                                <td>${item.email}</td>
                                                <td>${item.firstName}</td>
                                                <td>${item.secondName}</td>
                                                <td>${item.firstSurName}</td>
                                                <td>${item.secondSurName}</td>
                                                <td>${item.birthDate}</td>
                                                <td>${item.gender}</td>
                                                <td>${item.militaryCard}</td>
                                                <td>${item.militaryCardType}</td>
                                                <td>${item.militaryCardNumber}</td>
                                                <td>${item.phoneContact}</td>
                                                <td>${item.civilStatus}</td>
                                                <td>${item.country}</td>
                                                <td>${item.nationality}</td>
                                                <td>${item.stateDivipola}</td>
                                                <td>${item.cityDivipola}</td>
                                                <td>${item.houseBoss}</td>
                                                <td>${item.targetPopulation}</td>
                                                <td>${item.ethnicGroup}</td>
                                                <td>${item.populationType}</td>
                                                <td>${item.disability}</td>
                                                <td>${item.disabilityType}</td>
                                                <td>${item.residenceCountry}</td>
                                                <td>${item.residenceState}</td>
                                                <td>${item.residenceCity}</td>
                                                <td>${item.addres}</td>
                                                <td>${item.neighborhood}</td>
                                                <td>${item.otherPhone}</td>
                                                <td>${item.observations}</td>
                                                <td>${item.wageAspiration}</td>
                                                <td>${item.travelPossibility}</td>
                                                <td>${item.movePossibility}</td>
                                                <td>${item.distanceJobOffersInterest}</td>
                                                <td>${item.employmentStatus}</td>
                                                <td>${item.vehicleOwner}</td>
                                                <td>${item.drivingLicenseCar}</td>
                                                <td>${item.categoryLicenseCar}</td>
                                                <td>${item.drivingLicenseMotorcycle}</td>
                                                <td>${item.categoryLicenseMotorcycle}</td>
                                            </tr> 
                                        </c:forEach>
                                    </c:if>
                                </tbody> 
                            </table>
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%" > 
                                <caption><h2>Nivel Educativo</h2><hr></caption>
                                <thead> 
                                    <tr> 
                                        <th>Id de Carga</th> 
                                        <th>Tipo Documento</th> 
                                        <th>N�mero Documento</th> 
                                        <th>Nivel educativo</th>
                                        <th>�rea desempe�o</th>
                                        <th>N�cleo del conocimiento</th>
                                        <th>T�tulo obtenido</th>
                                        <th>T�tulo Homologado</th>
                                        <th>Pa�s del t�tulo</th> 
                                        <th>Instituci�n</th>
                                        <th>Estado</th>
                                        <th>Fecha finalizaci�n</th>
                                        <th>Tiene Tarjeta Profesional</th>
                                        <th>N�mero Tarjeta Profesional</th>
                                        <th>Fecha Expedici�n Tarjeta Profesional</th>
                                        <th>Observaciones</th>
                                        <th>Interes Pr�ctica Empresarial</th>
                                    </tr> 
                                </thead> 
                                <tbody> 
                                    <c:if test="${not empty resultsEducationlevel}">
                                        <c:forEach var="item" items="${resultsEducationlevel}">
                                            <tr> 
                                                <td>${item.cargaId}</td>
                                                <td>${item.documentType}</td>
                                                <td>${item.numberIdentification}</td>
                                                <td>${item.educationLevel}</td>
                                                <td>${item.performanceArea}</td>
                                                <td>${item.nucleusKnowledge}</td>
                                                <td>${item.degreeObtained}</td>
                                                <td>${item.degreeHomologated}</td>
                                                <td>${item.degreeCountry}</td>
                                                <td>${item.university}</td>
                                                <td>${item.status}</td>
                                                <td>${item.finishDate}</td>
                                                <td>${item.professionalCard}</td>
                                                <td>${item.professionalCardNumber}</td>
                                                <td>${item.professionalCardExpeditionDate}</td>
                                                <td>${item.observations}</td>
                                                <td>${item.practice}</td>
                                            </tr> 
                                        </c:forEach>
                                    </c:if>
                                </tbody> 
                            </table>
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%" > 
                                <caption><h2>Experiencia Laboral</h2><hr></caption>
                                <thead> 
                                    <tr> 
                                        <th>Id de Carga</th>  
                                        <th>Tipo Documento</th> 
                                        <th>N�mero Documento</th> 
                                        <th>Tipo Experiencia Laboral</th>
                                        <th>Producto Servicio</th>
                                        <th>Cuantas Personas Trabajan Con Usted</th>
                                        <th>Nombre Empresa</th>
                                        <th>Sector</th>
                                        <th>Tel�fono Empresa</th>
                                        <th>Pa�s</th>
                                        <th>Cargo</th>
                                        <th>Cargo Equivalente</th>
                                        <th>Fecha Ingreso</th>
                                        <th>Trabaja Actualmente</th>
                                        <th>Fecha Retiro</th>
                                    </tr> 
                                </thead> 
                                <tbody> 
                                    <c:if test="${not empty resultsWorkExperience}">
                                        <c:forEach var="item" items="${resultsWorkExperience}">
                                            <tr> 
                                                <td>${item.cargaId}</td>
                                                <td>${item.documentType}</td>
                                                <td>${item.numberIdentification}</td>
                                                <td>${item.workExperienceType}</td>
                                                <td>${item.product}</td>
                                                <td>${item.peopleAmount}</td>
                                                <td>${item.companyName}</td>
                                                <td>${item.sector}</td>
                                                <td>${item.companyPhone}</td>
                                                <td>${item.country}</td>
                                                <td>${item.position}</td>
                                                <td>${item.equivalentPosition}</td>
                                                <td>${item.admisionDate}</td>
                                                <td>${item.currentlyWorking}</td>
                                                <td>${item.retirementDate}</td>
                                            </tr> 
                                        </c:forEach>
                                    </c:if>
                                </tbody> 
                            </table>
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%"  > 
                                <caption><h2>Educaci�n Informal</h2><hr></caption>
                                <thead> 
                                    <tr> 
                                        <th>Id de Carga</th>  
                                        <th>Tipo Documento</th> 
                                        <th>N�mero Documento</th> 
                                        <th>Tipo capacitaci�n</th>
                                        <th>Instituci�n</th>
                                        <th>Estado</th>
                                        <th>Fecha Certificaci�n</th>
                                        <th>Nombre del Programa</th>
                                        <th>Pa�s</th> 
                                        <th>Duraci�n(horas)</th>
                                    </tr> 
                                </thead> 
                                <tbody> 
                                    <c:if test="${not empty resultsInformalEducation}">
                                        <c:forEach var="item" items="${resultsInformalEducation}">
                                            <tr> 
                                                <td>${item.cargaId}</td>
                                                <td>${item.documentType}</td>
                                                <td>${item.numberIdentification}</td>
                                                <td>${item.trainingType}</td>
                                                <td>${item.institution}</td>
                                                <td>${item.status}</td>
                                                <td>${item.certificationDate}</td>
                                                <td>${item.programName}</td>
                                                <td>${item.country}</td>
                                                <td>${item.hours}</td>
                                            </tr> 
                                        </c:forEach>
                                    </c:if>
                                </tbody> 
                            </table>
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%" > 
                                <caption><h2>Idiomas</h2><hr></caption>
                                <thead> 
                                    <tr> 
                                        <th>Id de Carga</th>  
                                        <th>Tipo Documento</th> 
                                        <th>N�mero Documento</th> 
                                        <th>Idiomas y Dialectos</th>
                                        <th>Nivel</th>
                                    </tr> 
                                </thead> 
                                <tbody> 
                                    <c:if test="${not empty resultsLanguage}">
                                        <c:forEach var="item" items="${resultsLanguage}">
                                            <tr> 
                                                <td>${item.cargaId}</td>
                                                <td>${item.documentType}</td>
                                                <td>${item.numberIdentification}</td>
                                                <td>${item.languages}</td>
                                                <td>${item.level}</td>
                                            </tr> 
                                        </c:forEach>
                                    </c:if>
                                </tbody> 
                            </table>
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%" > 
                                <caption><h2>Otros Conocimientos</h2><hr></caption>
                                <thead> 
                                    <tr> 
                                        <th>Id de Carga</th>  
                                        <th>Tipo Documento</th> 
                                        <th>N�mero Documento</th> 
                                        <th>Tipo</th>
                                        <th>Herramienta</th>
                                        <th>Nivel</th>
                                    </tr> 
                                </thead> 
                                <tbody> 
                                    <c:if test="${not empty resultsOtherKnowledge}">
                                        <c:forEach var="item" items="${resultsOtherKnowledge}">
                                            <tr> 
                                                <td>${item.cargaId}</td>
                                                <td>${item.documentType}</td>
                                                <td>${item.numberIdentification}</td>
                                                <td>${item.type}</td>
                                                <td>${item.tool}</td>
                                                <td>${item.level}</td>
                                            </tr> 
                                        </c:forEach>
                                    </c:if>
                                </tbody> 
                            </table>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="intermediacion">
                        <div class="table-responsive col-md-12">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%" > 
                                <thead> 
                                    <tr> 
                                        <th>Id de Carga</th>  
                                        <th>Tipo Documento</th> 
                                        <th>N�mero Documento</th> 
                                        <th>C�digo Vacante</th>
                                        <th>Empresa Sede</th>
                                        <th>Fecha Postulaci�n</th>
                                        <th>Estado Postulaci�n</th>
                                        <th>Motivo Declinaci�n</th>
                                        <th>Fecha Declinaci�n</th> 
                                        <th>Estado Proceso</th>
                                        <th>Fecha Remisi�n</th>
                                        <th>Motivo Rechazo Empresa</th>
                                        <th>Propuesta Salarial</th>
                                        <th>Fecha Respuesta Empresa</th>
                                        <th>Justificaci�n</th>
                                    </tr> 

                                </thead> 
                                <tbody> 
                                    <c:if test="${not empty resultsIntermediation}">
                                        <c:forEach var="item" items="${resultsIntermediation}">
                                            <tr> 
                                                <td>${item.cargaId}</td>
                                                <td>${item.documentType}</td>
                                                <td>${item.documentNumber}</td>
                                                <td>${item.vacancyCode}</td>
                                                <td>${item.company}</td>
                                                <td>${item.applicationDate}</td>
                                                <td>${item.applicationStatus}</td>
                                                <td>${item.declineReason}</td>
                                                <td>${item.declineDate}</td>
                                                <td>${item.processStatus}</td>
                                                <td>${item.referralDate}</td>
                                                <td>${item.companyRejectionReason}</td>
                                                <td>${item.salaryProposal}</td>
                                                <td>${item.companyResponseDate}</td>
                                                <td>${item.justification}</td>
                                            </tr> 
                                        </c:forEach>
                                    </c:if>
                                </tbody> 
                            </table>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="capacitacion">
                        <div class="table-responsive col-md-12">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%"  > 
                                <thead> 
                                    <tr> 
                                        <th>Id de Carga</th>  
                                        <th>C�digo Programa</th> 
                                        <th>Nombre Programa</th> 
                                        <th>Tipo Programa</th> 
                                        <th>Tipo Curso</th> 
                                        <th>C�digo Ccf</th> 
                                        <th>C�digo Inst</th> 
                                        <th>C�digo Sede</th> 
                                        <th>C�digo Departamento</th> 
                                        <th>C�digo Municipio</th> 
                                        <th>Certificada</th> 
                                        <th>Nombre Certificaci�n</th> 
                                        <th>Otro</th> 
                                        <th>Certificaci�n Emitida</th> 
                                        <th>Cine</th> 
                                        <th>Necesidad</th> 
                                        <th>C�digo M�dulo</th> 
                                        <th>Nombre M�dulo</th> 
                                        <th>Duraci�n</th> 
                                        <th>Tipo Horario</th> 
                                        <th>Tipo Costo_Matr�cula</th> 
                                        <th>Costo Matr�cula</th> 
                                        <th>Tipo Otros Costos</th> 
                                        <th>Otros Costos</th> 
                                        <th>Fecha Inicio</th> 
                                        <th>Fecha Fin</th> 
                                        <th>Area Desempe�o</th> 
                                    </tr> 
                                </thead> 
                                <tbody> 
                                    <c:if test="${not empty resultsTrainings}">
                                        <c:forEach var="item" items="${resultsTrainings}">
                                            <tr> 
                                                <td>${item.cargaId}</td>
                                                <td>${item.programCode}</td>
                                                <td>${item.programName}</td>
                                                <td>${item.programType}</td>
                                                <td>${item.courseType}</td>
                                                <td>${item.ccfCode}</td>
                                                <td>${item.instCode}</td>
                                                <td>${item.headqCode}</td>
                                                <td>${item.stateCode}</td>
                                                <td>${item.cityCode}</td>
                                                <td>${item.certified}</td>
                                                <td>${item.certName}</td>
                                                <td>${item.otherCertName}</td>
                                                <td>${item.issuedCertName}</td>
                                                <td>${item.cine}</td>
                                                <td>${item.necesity}</td>
                                                <td>${item.moduleCode}</td>
                                                <td>${item.moduleName}</td>
                                                <td>${item.totalHours}</td>
                                                <td>${item.schedule}</td>
                                                <td>${item.enrollmentCostType}</td>
                                                <td>${item.enrollmentCost}</td>
                                                <td>${item.otherCostType}</td>
                                                <td>${item.otherCost}</td>
                                                <td>${item.startDate}</td>
                                                <td>${item.endDate}</td>
                                                <td>${item.performance}</td>
                                            </tr> 
                                        </c:forEach>
                                    </c:if>
                                </tbody> 
                            </table>
                        </div>
                    </div>
                </div>    
                <div class="clearfix"></div>
            </div>

            <div role="tabpanel" class="tab-pane" id="masiva">
                <div class="panel panel-default">
                    <div class="panel-heading"><h2>B&uacute;squeda</h2></div>
                    <div class="panel-body">
                        <legend>
                            <form enctype="multipart/form-data" method="post" action="cruce-masivo.htm" id="masivoform" >    
                                <p>A continuaci�n seleccione el archivo a cargar desde su ordenador.</p>
                                <br/>
                                <input type="file" class="form-control load-file" id="archivoCarga" name="file" 
                                       path="file" accept="text/plain" />
                                <br/>
                               
                              <button id="saveButton" class="btn btn-success "
                                        data-btn-ok-label="Confirmar" data-btn-cancel-label="Cancelar"
                                        data-title="Est� seguro que desea hacer el cruce?" 
                                        data-toggle="confirmation" data-placement="top" 
                                        confirmation-callback>Guardar</button>
                                <button type="button" onclick="resetForm()" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                            </form>
                        </legend>
                        <div class="clearfix"></div>
                    </div>
                </div> 
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="clearfix"></div>

        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/dataTables.buttons.min.js"></script>
        <script src="resources/js/buttons.flash.min.js"></script>
        <script src="resources/js/jszip.min.js"></script>
        <script src="resources/js/pdfmake.min.js"></script>
        <script src="resources/js/vfs_fonts.js"></script>
        <script src="resources/js/buttons.html5.min.js"></script>
        <script src="resources/js/buttons.print.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-confirmation.js"></script>
        <script src="resources/js/jquery.ui.widget.js"></script>
        <script src="resources/js/jquery.iframe-transport.js"></script>
        <script src="resources/js/jquery.fileupload.js"></script>
        <script src="resources/js/bootstrap-toggle.min.js"></script>

    </body>
    <script>

        $(document).ready(function () {
            
            $('.table').DataTable({
                "language": {
                    "url": "resources/js/Spanish.json"
                },
                "initComplete": function (settings, json) {
                    $(".dt-buttons").each(function (index) {
                        console.log(index + ": " + $(this).text());
                        if ($(this).find('.exportar').length === 0) {
                            $(this).prepend('<label class="exportar">Exportar a:&nbsp;&nbsp;</label>');
                        }
                    });
                },
                "bFilter": false,
                "bInfo": false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'csv',
                        fieldBoundary: '',
                        text: '<i class="fa fa-file-text-o"></i>',
                        titleAttr: 'CSV'

                    },
                    {
                        extend: 'excel',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        titleAttr: 'EXCEL'
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-files-o"></i>',
                        titleAttr: 'COPY'
                    }

                ]

            });

        });
        
          $('#saveButton').confirmation({
                onConfirm: function (event, element) {
                    
                        $('#masivoform').submit();
                    
                }
            });
    </script>
    <style>
        .buttons-csv{
            display: inline-block;
            background-image:url(resources/images/csv.png);
            cursor:pointer !important;
            width: 32px !important;
            height: 32px !important;
            border: none !important;

        }
        .buttons-csv span{
            opacity: 0;
        }
        .buttons-excel{
            display: inline-block;
            background-image:url(resources/images/excel.png);
            cursor:pointer !important;
            width: 32px !important;
            height: 32px !important;
            border: none !important;

        }
        .buttons-excel span{
            opacity: 0;
        }
        .buttons-pdf span{
            opacity: 0;

        }
        .buttons-print{
            display: inline-block;
            background-image:url(resources/images/print.png);
            cursor:pointer !important;
            width: 32px !important;
            height: 32px !important;
            border: none !important;


        }
        .buttons-print span{
            opacity: 0;

        }
        div.dt-buttons {
            float: right;
            margin-left:10px;



        }
        table 
        { 
            table-layout:auto !important; 
            width: auto !important;
        }

    </style>
</html>
