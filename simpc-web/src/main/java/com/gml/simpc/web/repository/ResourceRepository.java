/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IRepository.java
 * Created on: 2016/10/24, 09:59:11 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository;

import com.gml.simpc.api.entity.Resource;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author <a href="mailto:julianm@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
public interface ResourceRepository extends JpaRepository<Resource, Long> {

    /**
     * Consulta por id de perfil..
     *
     * @param idProfile
     *
     * @return
     */
    List<Resource> findByIdProfile(Long idProfile);

    /**
     * Elimina todos los recursos por perfil.
     *
     * @param profileId
     *
     * @return
     */
    @Modifying
    @Query(nativeQuery = true, value = "DELETE FROM RECURSOS " +
        "WHERE PERFIL_ID = ?1")
    int deleteByProfile(Long profileId);
    
    /**
     * Elimina todos los recursos por perfil.
     *
     * @param profileId
     *
     * @return
     */
    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "DELETE FROM RECURSOS " +
        "WHERE PERFIL_ID = ?1")
    void deleteByProfileId(Long profileId);
}
