<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
    </head>
    <body>
        <c:if test="${not empty msg}">
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        <div class="col-sm-12">
            <h1>Datos de Perfil</h1>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label for="nombrePerfil" class=" control-label">Nombre de Perfil</label>
                <p class="form-control-static">${parentProfile.name}</p>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label for="estadoPerfil" class="control-label">Estado de Perfil</label>
                <p class="form-control-static">${parentProfile.status.label}</p>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label for="detallePerfil" class="control-label">Descripcion de Perfil</label>
                <p class="form-control-static">${parentProfile.description}</p>
            </div>
        </div>


        <div class="clearfix"></div>
        <div class="panel panel-default">
            <div class="panel-heading"><h2>Funcionalidades para Asignar</h2></div>
            <div class="panel-body">
                <table class=" table-condensed" width="100%">
                    <form:form method="post" action="/masterdata/actualizar-recursos.htm" modelAttribute="profile" id="formIds" target="_parent"> 
                        <tr>
                            <td class="col-md-5" >
                                <input type="hidden" value="${parentProfile.id}" name="parent">
                                <label for="solicitud1">Funcionalidades del Sistema</label>
                                <select id="origen" multiple="multiple" size="5" style="width:100%">
                                    <c:if test="${not empty functionalitySystemList}">
                                        <c:forEach var="item" items="${functionalitySystemList}">
                                            <option value="${item.id}">${item.label}</option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                            <td class="col-md-2" >
                                <div class="text-center">
                                    <input type="button" id="add" value="Agregar >>" />
                                    <br/>
                                    <br/>
                                    <input type="button" id="remove" value="<< Quitar  " />
                                </div>
                            </td>
                            <td class="col-md-5" >
                                <label for="solicitud1">Funcionalidades Asignadas al Perfil</label>
                                <select name="destino" id="destino" size="5" style="width:100%" >
                                    <c:if test="${not empty functionalityProfileList}">
                                        <c:forEach var="item" items="${functionalityProfileList}">
                                            <option value="${item.id}">${item.label}</option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <input type="hidden" name="idValues" id="idValues"/>
                    <form:input type="hidden" path="id" value="${parent}"/>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8 ">
                            <button id="saveButton" class="btn btn-success pull-right"
                                    data-btn-ok-label="Confirmar" data-btn-cancel-label="Cancelar"
                                    data-title="Est� seguro?" 
                                    data-toggle="confirmation" data-placement="left" 
                                    confirmation-callback>Guardar
                            </button>
                        </div>
                    </div>
                </form:form>
                <div class="clearfix"></div>
            </div>
        </div>

        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-confirmation.js"></script>
        <script>

            $(document).ready(function () {

                $('#saveButton').confirmation({
                    onConfirm: function (event, element) {
                        obtenerLista();
                        $('#formIds').submit();
                    }
                });


            });

            $(function ()
            {
                $("#add").click(function ()
                {
                    mover("origen", "destino");
                });

                $("#remove").click(function ()
                {
                    mover("destino", "origen");
                });
            });

            function mover(origen, destino)
            {
                $("#" + origen + " option:selected").remove().appendTo("#" + destino);
            }

            function obtenerLista()
            {
                var ids = '';

                $("#destino option").each(function ()
                {


                    if (ids === '') {
                        ids = ids + $(this).val();
                    } else {
                        ids = ids + ',' + $(this).val();
                    }
                });

                $('#idValues').val(ids);

//                    $('#formIds').submit();
            }



        </script>
    </body>
</html>
