/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ActionLogServiceImpl.java
 * Created on: 2016/11/15, 10:50:35 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.dao.ManagementAndPlacementDao;
import com.gml.simpc.api.dto.ManagementAndPlacementDto;
import com.gml.simpc.api.services.ManagementAndPlacementService;
import com.gml.simpc.commons.annotations.AnnotationInterceptor;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Service(value = "ManagementAndPlacementService")
public class ManagementAndPlacementServiceImpl implements
    ManagementAndPlacementService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(
        ManagementAndPlacementServiceImpl.class);
    /**
     * Repositorio para usuarios.
     */
    @Autowired
    private ManagementAndPlacementDao managementAndPlacementDao;

    /**
     * Genera lista de resultados para gesti&ocute;n intermediacion
     * seg&ucute;n los filtros ingresados.
     * 
     * @param init
     * @param finish
     * @param idStatusPostulation
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consulta Gestión y " +
        "Colocación Por Fechas y Estado Postulación", entityTableName =
        "MD_F_GESTION_INTERMEDIACION")
    @Override
    public List<ManagementAndPlacementDto> findManagementAndPlacementResults(
        Date init, Date finish, String idStatusPostulation) {
        List<ManagementAndPlacementDto> resultado = managementAndPlacementDao.
            findManagementAndPlacementResults(init,
                finish, idStatusPostulation);
        for (ManagementAndPlacementDto dto : resultado) {
            LOGGER.info(dto.toString());
        }
        return resultado;
    }

}
