/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: OrientationPreparedStatementSetter.java
 * Created on: 2017/01/20, 12:16:58 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
  * Objetive: se prepara el statement para cada campo para las tablas de stage y master
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.writer;

import com.gml.simpc.api.dto.ProfitDto;
import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.springframework.batch.item.database.ItemPreparedStatementSetter;

/**
 * Transfiere la informacion de los Beneficios Economicos (Archivo Fosfec)
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
public class FosfecPreparedStatementSetter
    implements ItemPreparedStatementSetter<ProfitDto> {

    @Override
    public void setValues(ProfitDto profitDto, PreparedStatement ps)
        throws SQLException {
        ThreadLocalJob.setProcesed();
        JobResultDto jobResult = ThreadLocalJob.getResult();

        int line = jobResult.getProcesedLines();
        ps.setString(1, profitDto.getEventType());
        ps.setString(2, profitDto.getCcfCode());
        ps.setString(3, profitDto.getUnemployedIDType());
        ps.setString(4, profitDto.getUnemployedID());
        ps.setString(5, profitDto.getFirstName());
        ps.setString(6, profitDto.getSecodName());
        ps.setString(7, profitDto.getFirstSurname());
        ps.setString(8, profitDto.getSecondSurname());
        ps.setString(9, profitDto.getBirthday());
        ps.setString(10, profitDto.getGender());
        ps.setString(11, profitDto.getResidentState());
        ps.setString(12, profitDto.getResidentCity());
        ps.setString(13, profitDto.getSalaryrange());
        ps.setString(14, profitDto.getPublicServiceRegistration());
        ps.setString(15, profitDto.getVinculationType());
        ps.setString(16, profitDto.getLastCff());
        ps.setString(17, profitDto.getPostulationDateMpc());
        ps.setString(18, profitDto.getHealthManager());
        ps.setString(19, profitDto.getRetirementPostulation());
        ps.setString(20, profitDto.getRetirementManager());
        ps.setString(21, profitDto.getEcononomicSubsidyFee());
        ps.setString(22, profitDto.getMpcSavings());
        ps.setString(23, profitDto.getSeveranceFundAdministrator());
        ps.setString(24, profitDto.getPercentageSavings());
        ps.setString(25, profitDto.getFeedBonusPostulation());
        ps.setString(26, profitDto.getRequirements());
        ps.setString(27, profitDto.getPostulationDateCheck());
        ps.setString(28, profitDto.getBeneficiary());
        ps.setString(29, profitDto.getApplyWithRoute());
        ps.setString(30, profitDto.getSettlementDate());
        ps.setString(31, profitDto.getBenefitClearedPeriod());
        ps.setString(32, profitDto.getNumberLiquidBenefi());
        ps.setString(33, profitDto.getNumberMoneyChargesLiquided());
        ps.setString(34, profitDto.getNumberDependents());
        ps.setString(35, profitDto.getEconomicIncentiveSavings());
        ps.setString(36, profitDto.getNumbrLiquidBonds());
        ps.setString(37, profitDto.getHelthLiquidedAmount());
        ps.setString(38, profitDto.getRetirementliquidedAmount());
        ps.setString(39, profitDto.getMonetaryLiquidedAmount());
        ps.setString(40, profitDto.getLiquidedAssetAmount());
        ps.setString(41, profitDto.getFeedBonusLiquidedAmount());
        ps.setString(42, profitDto.getTotalValueProfits());
        ps.setString(43, profitDto.getVoluntaryWaiver());
        ps.setString(44, profitDto.getWaiverDate());
        ps.setString(45, profitDto.getLostBenefit());
        ps.setString(46, profitDto.getLostBenefitDate());
        ps.setString(47, profitDto.getBenefitEnd());
        ps.setString(48, profitDto.getBenefitEndDate());
        ps.setString(49, Integer.toString(line));

    }

}
