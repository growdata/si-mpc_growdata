/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: UserFields.java
 * Created on: 2017/02/15, 01:45:54 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.enums;

/**
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
public enum UserFields {
 
    ENTIDAD,NOMBRE,SEGUNDO_NOMBRE,APELLIDO,DIRECCION,TELEFONO,EMAIL,NOMBREUSUARIO,PERFIL,ESTADO;

}
