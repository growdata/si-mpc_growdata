/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IntermediationDtoRowMapper.java
 * Created on: 2017/02/13, 11:38:23 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.dao.mapper;

import com.gml.simpc.api.dto.IntermediationDto;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 * Row Mapper para intermediaci&oacute;n.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class IntermediationDtoRowMapper implements RowMapper<IntermediationDto> {

    @Override
    public IntermediationDto mapRow(ResultSet rs, int i) throws SQLException {
        IntermediationDto dto = new IntermediationDto();
        dto.setVacancyCode(rs.getString("CODIGO_VACANTE"));
        dto.setCompany(rs.getString("EMPRESA_SEDE"));
        dto.setDocumentType(rs.getString("TIPO_DOCUMENTO"));
        dto.setDocumentNumber(rs.getString("NUMERO_DOCUMENTO"));
        dto.setApplicationDate(rs.getString("FECHA_POSTULACION"));
        dto.setApplicationStatus(rs.getString("ESTADO_POSTULACION"));
        dto.setDeclineReason(rs.getString("MOTIVO_DECLINACION"));
        dto.setDeclineDate(rs.getString("FECHA_DECLINACION"));
        dto.setProcessStatus(rs.getString("ESTADO_PROCESO"));
        dto.setReferralDate(rs.getString("FECHA_REMISION"));
        dto.setCompanyRejectionReason(rs.getString("MOTIVO_RECHAZO_EMPRESA"));
        dto.setSalaryProposal(rs.getString("PROPUESTA_SALARIAL"));
        dto.setCompanyResponseDate(rs.getString("FECHA_RESPUESTA_EMPRESA"));
        dto.setJustification(rs.getString("JUSTIFICACION"));
        dto.setCargaId(rs.getLong("CARGA_ID"));
        
        return dto;
    }
}
