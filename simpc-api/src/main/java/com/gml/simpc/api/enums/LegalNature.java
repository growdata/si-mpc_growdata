/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserStatus.java
 * Created on: 2016/10/21, 02:56:30 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.enums;

/**
 * Enum que maneja los estados de los usuarios
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public enum LegalNature {

    N(1, "Persona Natural"),
    J(2, "Persona Jur�dica");

    /**
     * C&oacute;digo del estado.
     */
    private final Integer code;
    /**
     * Texto del estado.
     */
    private final String label;

    private LegalNature(int code, String label) {
        this.code = code;
        this.label = label;
    }

    public int getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }

    /**
     * Devuelve el status por c&oacute;digo.
     *
     * @param code
     *
     * @return
     */
    public static LegalNature getByCode(char code) {
        LegalNature[] allValues = values();

        for (LegalNature status : allValues) {
            if (status.getCode()==(code)) {
                return status;
            }
        }

        throw new IllegalArgumentException("C&oacute;digo [ " + code +
            " ] no se encuentra registrado.");
    }
    @Override
    public String toString(){
        return label;
    }
}
