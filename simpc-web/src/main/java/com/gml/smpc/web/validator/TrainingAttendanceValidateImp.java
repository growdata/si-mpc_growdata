/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Light.java
 * Created on: 2017/01/18, 04:30:10 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.smpc.web.validator;

import com.gml.simpc.loader.dto.ValidationErrorDto;
import com.gml.simpc.loader.validator.ValidateCommand;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

/**
 * Clase para implementar validaciones particulares de negocio en carges de
 * información
 * en cuanto a capacitación
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica
 * Jim&eacute;nez Rozo</a>
 */
@Service(value = "attendanceValidateService")
public class TrainingAttendanceValidateImp implements ValidateCommand {

    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(
        TrainingAttendanceValidateImp.class);

    /**
     * Consulta para encontrar codigos no existentes de modulo
     */
    private static final String NON_MODULE_RECORD_RULE =
        "select stage_data.LINEA," +
        " 'Modulo no existe  " +
        " ('||stage_data.CODIGO_MODULO|| ')' MENSAJE" +
        " from ST_PROGRAMA_DETALLE stage_data  " +
        " left join MD_F_CAP_PROGRAMA_MODULO master_data  " +
        " on stage_data.CODIGO_MODULO = master_data.CODIGO_MODULO" +
        " where STAGE_DATA.CARGA_ID=? " +
        " AND master_data.CODIGO_MODULO is null";

    /**
     * Consulta para encontrar asistentes sin registro de hojas de vida
     */
    private static final String NON_HDV_RECORD_RULE =
        "select stage_data.LINEA,     " +
        " 'Tipo y numero de documento no se encuentra en hojas de vida " +
        " ('||stage_data.TIPO_DOCUMENTO||' - '||stage_data.DOCUMENTO || ')' MENSAJE    " +
        " from ST_PROGRAMA_DETALLE stage_data " +
        " left join MD_F_DATOS_BASICOS master_data " +
        " on stage_data.TIPO_DOCUMENTO = master_data.TIPO_DOCUMENTO " +
        " and stage_data.DOCUMENTO = master_data.NUMERO_DOCUMENTO " +
        " where STAGE_DATA.CARGA_ID=? " +
        " AND master_data.NUMERO_DOCUMENTO is null";

    /**
     * Plantilla JDBC para consultar las propiedades.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Atributo para almacenar la lista de errores
     */
    List<ValidationErrorDto> errorsList;

    /**
     * Verifica si es del tipo que le corresponde.
     *
     * @param loadType
     *
     * @return
     */
    @Override
    public boolean verify(String loadType) {
        return "CAP-2".equals(loadType);
    }

    /**
     * Realiza la validación.
     *
     * @param loadId
     *
     * @return
     */
    @Override
    public List<ValidationErrorDto> validate(Long loadId) {

        LOGGER.info("Validating training-att bussiness rules for load ... " +
            loadId);
        this.errorsList = new ArrayList<>();
        validationQuery("module_exists", NON_MODULE_RECORD_RULE, new Long[]{
            loadId});
        validationQuery("non_hdv", NON_HDV_RECORD_RULE, new Long[]{loadId});
        LOGGER.info("Final errors: " + this.errorsList.size());
        return this.errorsList;
    }

    private void validationQuery(String type, String query, Long[] params) {
        try {
            List<ValidationErrorDto> resultList =
                this.jdbcTemplate.query(query, params, new RowMapper() {
                    @Override
                    public ValidationErrorDto mapRow(ResultSet rs, int rownumber)
                        throws SQLException {
                        ValidationErrorDto e = new ValidationErrorDto(
                            Integer.parseInt(rs.getString("LINEA")),
                            rs.getString("MENSAJE"));

                        return e;
                    }
                });
            LOGGER.info("Validating rules " + type + ". Errors foud: " +
                resultList.size());
            addErrors(resultList);
        } catch (Exception e) {
            LOGGER.error("Error executing bussiness validation type " + type +
                ": " + e);
            LOGGER.error("Validation query  " + query + " params " + params);
        }

    }

    private void addErrors(List<ValidationErrorDto> errorsList) {
        for (ValidationErrorDto validationErrorDto : errorsList) {
            this.errorsList.add(validationErrorDto);
        }
    }

}
