<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
    </head>
    <body>
        <h1>Ayuda para el Usuario</h1>
        <div class="panel panel-default">
            <div class="panel-heading"><h2>Manuales Descargables </h2></div>
            <div class="panel-body">
                <div align='center' class="col-sm-20">
                    <h1>Haga clic en el siguiente bot�n si desea<br><b>ver el manual de administrador</b></h1>
                    <form method="post" action="/masterdata/descargaManualAdmin.htm">
                        <br>
                        <button align type="submit" class="btn btn-success" 
                            >Manual de Administrador 
                        </button>
                    </form>
                </div>
                <div align='center' class="col-sm-20">
                    <h1>Haga clic en el siguiente bot�n si desea<br><b>ver el manual de usuario</b></h1>
                    <form method="post" action="/masterdata/descargaManual.htm">
                        <br>
                        <button align type="submit" class="btn btn-success" 
                            >Manual de Usuario 
                        </button>
                    </form>
                </div>
                <br>
                <div align='center' class="col-sm-30">
                    <h1>Haga clic en el siguiente bot�n si desea<br><b>ver las ayudas en l�nea</b></h1>
  
                    <form method="post" action="/masterdata/descargaAyuda.htm">
                        <br>
                        <button type="submit" class="btn btn-success" 
                            >Ayudas en L�nea  
                        </button>
                    </form>    
                </div>
            </div>
        </div>
    </body>
</html>
