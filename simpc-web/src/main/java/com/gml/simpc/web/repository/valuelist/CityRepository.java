/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserRepository.java
 * Created on: 2016/10/19, 02:03:51 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository.valuelist;

import com.gml.simpc.api.entity.valuelist.City;
import com.gml.simpc.api.entity.valuelist.State;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repositorio para municipios
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public interface CityRepository extends JpaRepository<City, String> {

    /**
     * Consulta los departamentos por estado.
     *
     * @param state
     *
     * @return
     */
    List<City> findByState(State state);
}
