/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: GraphicDaoImpl.java
 * Created on: 2016/12/27, 04:07:32 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.dao.impl;

import com.gml.simpc.api.dao.GraphicDao;
import com.gml.simpc.api.dto.GraphicDataDto;
import com.gml.simpc.api.dto.IndicatorFilterDto;
import com.gml.simpc.api.dto.TableDataDto;
import com.gml.simpc.web.indicator.util.IndicatorQueryUtil;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * Dao que define la forma en que se consulta la informaci&oacute;n de los
 * indicadores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Repository
public class GraphicDaoImpl implements GraphicDao {

    /**
     * Logger de la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(GraphicDaoImpl.class);
    /**
     * Plantilla Jdbc para operar la base de datos.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<GraphicDataDto> getGraphicData(final IndicatorFilterDto filter) {
        
        LOGGER.info("Ejecutando metodo [ getGraphicData ]");

        String query = IndicatorQueryUtil.buildGraphicDataQuery(filter);
        List<GraphicDataDto> graphicData;
        
        LOGGER.info("Filter : " + filter.toString());

        if (filter.getValue()!=null){
            LOGGER.info("Query with value: " + query);
        
            graphicData = this.jdbcTemplate.query(query,
                new RowMapper<GraphicDataDto>() {

                @Override
                public GraphicDataDto mapRow(ResultSet rs, int i)
                    throws SQLException {
                    GraphicDataDto dto = new GraphicDataDto();
                    dto.setAxisY(rs.getInt("Y"));
                    dto.setAxisZ(rs.getInt("Z"));
                    dto.setGroupField(rs.getString("group_field"));
                    if (filter.hasManyPeriods()) {
                        dto.setAxisX(rs.getString("X"));
                    }

                    return dto;
                }
            }, filter.getValue(), filter.getValue(), filter.getStartDate(),
                filter.getEndDate());
        } else{
            LOGGER.info("Query without value: " + query);
        
            graphicData = this.jdbcTemplate.query(query,
                new RowMapper<GraphicDataDto>() {

                @Override
                public GraphicDataDto mapRow(ResultSet rs, int i)
                    throws SQLException {
                    GraphicDataDto dto = new GraphicDataDto();
                    dto.setAxisY(rs.getInt("Y"));
                    dto.setAxisZ(rs.getInt("Z"));
                    dto.setGroupField(rs.getString("group_field"));

                    if (filter.hasManyPeriods()) {
                        dto.setAxisX(rs.getString("X"));
                    }

                    return dto;
                }
            }, filter.getStartDate(),filter.getEndDate());
        }     

        return graphicData;
    }

    @Override
    public List<TableDataDto> getTableData(final IndicatorFilterDto filter) {
        
        LOGGER.info("Ejecutando metodo [ getTableData ]");
        
        String query = IndicatorQueryUtil.buildTableDataQuery(filter);
        
        LOGGER.info("Filter : " + filter.toString());

        List<TableDataDto> tableData;
        
        if (filter.getValue()!=null){
            LOGGER.info("Query with value: " + query);
            tableData = this.jdbcTemplate.query(query,
                new RowMapper<TableDataDto>() {

                @Override
                public TableDataDto mapRow(ResultSet rs, int i)
                    throws SQLException {
                    TableDataDto dto = new TableDataDto();

                    int numOfColumns = filter.getColumns().size();

                    for (int j = 1; j <= numOfColumns; j++) {
                        dto.addColumn(rs.getString(j));
                    }

                    return dto;
                }
            }, filter.getStartDate(), filter.getEndDate() , filter.getValue());
        } else {
            LOGGER.info("Query without value: " + query);
            tableData = this.jdbcTemplate.query(query,
                new RowMapper<TableDataDto>() {

                @Override
                public TableDataDto mapRow(ResultSet rs, int i)
                    throws SQLException {
                    TableDataDto dto = new TableDataDto();

                    int numOfColumns = filter.getColumns().size();

                    for (int j = 1; j <= numOfColumns; j++) {
                        dto.addColumn(rs.getString(j));
                    }

                    return dto;
                }
            }, filter.getStartDate(), filter.getEndDate());
        }   

        return tableData;
    }

    /**
     * Obtiene los par&aacute;metros necesarios para ejecutar la consulta.
     *
     * @param filter
     *
     * @return
     */
    private Object[] getParameters(IndicatorFilterDto filter) {
        LOGGER.info("Ejecutando metodo [ getParameters ]");

        Object params[];

        if (filter.hasManyPeriods()) {
            params = new Object[4];
            params[0] = filter.getValue();
            params[1] = filter.getValue();
            LOGGER.info("Param1: " + filter.getValue());
            params[2] = filter.getStartDate();
            LOGGER.info("Param2: " + filter.getStartDate());
            params[3] = filter.getEndDate();
            LOGGER.info("Param3: " + filter.getEndDate());
        } else {
            params = new Object[3];
            params[0] = filter.getValue();
            LOGGER.info("Param1: " + filter.getValue());
            params[1] = filter.getStartDate();
            LOGGER.info("Param2: " + filter.getStartDate());
            params[2] = filter.getEndDate();
            LOGGER.info("Param3: " + filter.getEndDate());
        }

        return params;
    }
}
