/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: TrainingPreparedStatementSetter.java
 * Created on: 2017/01/20, 12:16:58 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.writer;

import com.gml.simpc.api.dto.AttendanceDto;
import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.springframework.batch.item.database.ItemPreparedStatementSetter;

/**
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica Jim&eacute;nez Rozo</a>
 */
public class OrientationAttendancePreparedStatementSetter 
        implements ItemPreparedStatementSetter<AttendanceDto> {
 
    @Override
    public void setValues(AttendanceDto attendanceDto, PreparedStatement ps) 
            throws SQLException {
        ThreadLocalJob.setProcesed();
        JobResultDto jobResult = ThreadLocalJob.getResult();
        int line = jobResult.getProcesedLines();
        ps.setString(1, attendanceDto.getModuleCode());
        ps.setString(2, attendanceDto.getDocumentType());
        ps.setString(3, attendanceDto.getDocument());
        ps.setString(4, attendanceDto.getStatus());
        ps.setString(5, attendanceDto.getAttendancePercent());
        ps.setString(6, attendanceDto.getJustification());
        ps.setString(7, Integer.toString(line));
    }
 
}
