<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>MECANISMO DE PROTECCI�N AL CESANTE "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster.bundle.min.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster-sideTip-shadow.min.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <style>
            input[type=number]::-webkit-outer-spin-button,
            input[type=number]::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            input[type=number] {
                -moz-appearance:textfield;
            }
        </style>
    </head>
    <body>
        <c:if test="${not empty msg}">
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        <c:if test="${empty passMode}">
            <h1>Editar Informaci&oacute;n B&aacute;sica</h1>
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Informaci�n B�sica</h2></div>
                <div class="panel-body">
                    <table id="userTable" class="table table-striped table-bordered" 
                           cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Entidad</th>
                                <th>NIT de Entidad</th>
                                <th>Tipo de Documento</th>
                                <th>N�mero de Documento</th>
                                <th>Primer Nombre</th>
                                <th>Segundo Nombre</th>
                                <th>Primer Apellido</th>
                                <th>Segundo Apellido</th>
                                <th>Direcci�n</th>
                                <th>Departamento</th>
                                <th>Municipio</th>
                                <th>Tel�fono de Contacto</th>
                                <th>E-mail de Contacto</th>
                                <th>Nombre de Usuario</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>${userInfo.ccf.shortName}</td>
                                <td>${userInfo.ccf.nit}</td>
                                <td>${userInfo.documentType.name}</td>
                                <td>${userInfo.identificationNumber}</td>
                                <td>${userInfo.firstName}</td>
                                <td>${userInfo.secondName}</td>
                                <td>${userInfo.firstSurname}</td>
                                <td>${userInfo.secondSurname}</td>
                                <td>${userInfo.address}</td>
                                <td>${userInfo.state}</td>
                                <td>${userInfo.city}</td>
                                <td>${userInfo.phoneNumber}</td>
                                <td>${userInfo.email}</td>
                                <td>${userInfo.username}</td>
                            </tr>
                        </tbody>
                    </table>
                    <legend>
                        <c:if test="${not empty internalMsg}">
                            <div id="internal" class="alert alert-${msgType} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-label="Close">
                                    <span aria-hidden="true">�</span>
                                </button>
                                <strong>${internalMsg}</strong>
                            </div>
                        </c:if>
                        <form:form id="creationUser" method="post" action="/masterdata/guardar-usuario-base.htm" 
                                   modelAttribute="creationUser">  
                            <form:input type="hidden" id="tipoUsuario" path="admin"/>
                            <div class="col-sm-6 listAdmin">
                                <div class="form-group">
                                    <input type="hidden" id="id" name="id" path="id"/>
                                    <input type="hidden" id="tipoFormulario" value="${tipoformulario}"/>
                                    <input type="hidden" id="mintrabUser" name="mintrabUser" value="${mintrabUser}"/>
                                    <label for="institucion-busqueda" class=" control-label">Entidad(*)</label>
                                    <form:select class="form-control" id="ccfcode" path="ccf.code" required="required">
                                        <c:if test="${not empty foundationsList}">
                                            <option value="">Seleccione...</option>
                                            <c:forEach var="item" items="${foundationsList}">
                                                <c:if test="${item.code == creationUser.ccf.code}">
                                                    <option label="${item.shortName}" value="${item.code}" selected="true">
                                                        ${item.shortName}
                                                    </option>
                                                </c:if>
                                                <c:if test="${item.code != creationUser.ccf.code}">
                                                    <option label="${item.shortName}" value="${item.code}">
                                                        ${item.shortName}
                                                    </option>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 listUser">
                                <div class="form-group">
                                    <label for="entidad" class="control-label">Entidad(*)</label>
                                    <form:input type="text" class="form-control disable" id="foundation" placeholder="Entidad" 
                                                required="required" path="ccf.name" autocomplete="off"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div> 
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="idusuario" class="control-label">NIT de Entidad</label>
                                    <form:input type="text" class="form-control disable" id="foundationNit" placeholder="NIT de Entidad" 
                                                required="required" path="ccf.nit" autocomplete="off" readonly="true"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 listAdmin">
                                <div class="form-group">
                                    <label for="tipoDeDocumento" class=" control-label">Tipo de Documento(*)</label>
                                    <form:select onchange="activacionDigito()" class="form-control" 
                                                 id="documentTypeid" path="documentType.id" required="required">
                                        <option value="">Seleccione...</option>
                                        <c:if test="${not empty documentTypes}">
                                            <c:forEach var="item" items="${documentTypes}">
                                                <c:if test="${item.id == '2' || item.id == '3' || item.id == '5'}">
                                                    <c:if test="${creationUser.documentType.id == item.id}">
                                                        <option label="${item.name}" value="${item.id}" selected="true">${item.name}</option>
                                                    </c:if>
                                                    <c:if test="${creationUser.documentType.id != item.id}">
                                                        <option label="${item.name}" value="${item.id}">${item.name}</option>
                                                    </c:if>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 listUser">
                                <div class="form-group">
                                    <label for="tipoDeDocumento" class="control-label">Tipo de Documento(*)</label>
                                    <form:input type="text" class="form-control disable" id="documentTypename" placeholder="Entidad" 
                                                required="required" path="documentType.name" autocomplete="off"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div> 
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="numeroDeDocumento" class="control-label">N�mero de Documento(*)</label>
                                    <form:input data-rule-intrule="true" type="text" class="form-control disable" id="identificationNumber" 
                                                placeholder="N�mero de Documento" required="required" 
                                                onkeypress="return isNumberKey(event)"
                                                path="identificationNumber" minlength="4" maxlength="10"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="pnombreusuario" class="control-label">Primer Nombre(*)</label>
                                    <form:input type="text" class="form-control" id="firstName" 
                                                placeholder="Primer Nombre" required="required"
                                                path="firstName"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="snombreusuario" class="control-label">Segundo Nombre</label>
                                    <form:input type="text" class="form-control" id="secondName" 
                                                placeholder="Segundo Nombre"
                                                path="secondName"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="papellidousuario" class="control-label">Primer Apellido(*)</label>
                                    <form:input type="text" class="form-control" id="firstSurname" 
                                                placeholder="Primer Apellido" required="required"
                                                path="firstSurname"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="sapellidousuario" class="control-label">Segundo Apellido</label>
                                    <form:input type="text" class="form-control" id="secondSurname" 
                                                placeholder="Segundo Apellido" path="secondSurname"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="direccionPredio" class="control-label">Direcci�n(*)</label>
                                    <form:input type="text" class="form-control" id="address" 
                                                placeholder="Direcci�n Predio" required="required"
                                                path="address"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="departamento" class="control-label">Departamento(*)</label>
                                    <form:select class="form-control" id="stateid" 
                                                 path="state.id" required="required">
                                        <option value="">Seleccione...</option>
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="municipio" class="control-label">Municipio(*)</label>
                                    <form:select class="form-control" id="cityid" 
                                                 path="city.id" required="required">
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="telefonousuario" class="control-label">Tel�fono de Contacto(*)</label>
                                    <form:input data-rule-celrule="true" type="text" class="form-control" id="phoneNumber"
                                                placeholder="Tel�fono Contacto" required="required"
                                                path="phoneNumber" minlength="7" maxlength="10" onkeypress="return isNumberKey(event)"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="emailusuario" class="control-label">E-mail de Contacto(*)</label>
                                    <form:input data-rule-emailtld="true" class="form-control disable" id="email" 
                                                placeholder="E-Mail Contacto" required="required"
                                                path="email" autocomplete="off" maxlength="50"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="user-name-usuario" class="control-label">Nombre de Usuario(*)</label>
                                    <form:input type="text" class="form-control disable" id="username" 
                                                placeholder="Nombre de Usuario" required="required"
                                                path="username" autocomplete="off"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-12" style="text-align:right">
                                <br/>
                                <button id="saveButton1" class="btn btn-success" type="submit"
                                        data-btn-ok-label="Confirmar" data-btn-cancel-label="Cancelar"
                                        data-title="Est� seguro?" 
                                        data-toggle="confirmation" data-placement="left" 
                                        confirmation-callback>Guardar   
                                </button>
                            </div> 
                        </form:form>
                    </legend>
                    <div class="clearfix"></div>
                </div>
            </div>
        </c:if>
        <c:if test="${!mintrabUser}">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Cambio de Contrase�a</h2></div>
                <div class="panel-body">
                    <legend>
                        <div class="container">
                            <div class="alert alert-warning alert-dismissible" id="myAlert">
                                <font size=3><strong>�Atenci�n!</strong> La contrase�a debe tener m�nimo 8 caracteres, al menos una letra may�scula, una min�scula, un d�gito y un car�cter especial. (Ej: ?, #, /, & )</font>
                            </div>
                        </div>
                        <form:form id="editionPassword" method="post" action="/masterdata/cambiarContrasenia.htm" modelAttribute="editionPassword">   
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="user-pass-usuario" class="control-label">Contrase�a Anterior</label>
                                    <form:input type="password" class="form-control" id="user-pass-usuario" 
                                                placeholder="Contrase�a de Usuario" required="required"
                                                path="lastPassword" autocomplete="off"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="user-pass-usuario" class="control-label">Contrase�a Nueva</label>
                                    <form:input type="password" class="form-control" id="user-pass-usuario" 
                                                placeholder="Contrase�a de Usuario" required="required"
                                                data-rule-passrule="true" path="newPassword" autocomplete="off"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="user-pass-usuario" class="control-label">Confirmaci�n Contrase�a Nueva</label>
                                    <form:input type="password" class="form-control" id="user-pass-usuario" 
                                                placeholder="Contrase�a de Usuario" required="required"
                                                data-rule-passrule="true" path="confirmationPassword" autocomplete="off"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="modal-footer">                        
                                <button id="saveButton2" class="btn btn-success" type="submit"
                                        data-btn-ok-label="Confirmar" data-btn-cancel-label="Cancelar"
                                        data-title="Est� seguro?" 
                                        data-toggle="confirmation" data-placement="left" 
                                        confirmation-callback>Guardar   
                                </button>
                            </div>        
                        </form:form>
                    </legend>
                    <div class="clearfix"></div>
                </div>
            </div>
        </c:if>
        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.buttons.min.js"></script>
        <script src="resources/js/buttons.flash.min.js"></script>
        <script src="resources/js/jszip.min.js"></script>
        <script src="resources/js/pdfmake.min.js"></script>
        <script src="resources/js/vfs_fonts.js"></script>
        <script src="resources/js/buttons.html5.min.js"></script>
        <script src="resources/js/buttons.print.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/bootstrap-confirmation.js"></script>
        <script src="resources/js/jquery.validate.min.js"></script>
        <script src="resources/js/tooltipster.bundle.js"></script>
        <script src="resources/js/custom-functionalities.js"></script>
        <script>

            $(document).ready(function () {

                $("#userTable").hide();

                $('#userTable').DataTable({
                    "language": {
                        "url": "resources/js/Spanish.json"
                    }, "initComplete": function (settings, json) {
                        $(".dt-buttons").each(function (index) {
                            console.log(index + ": " + $(this).text());
                            if ($(this).find('.exportar').length === 0) {
                                $(this).prepend('<label class="exportar">Exportar a:&nbsp;&nbsp;</label>');
                            }
                        });
                    },
                    "bFilter": false,
                    "bInfo": false,
                    dom: 'B',
                    buttons: [
                        {
                            extend: 'csv',
                            fieldBoundary: '',
                            footer: false,
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'CSV',
                            exportOptions: {
                            }
                        },
                        {
                            extend: 'excel',
                            footer: false,
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'EXCEL',
                            exportOptions: {
                            }
                        },
                        {
                            extend: 'pdf',
                            footer: false,
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'PDF',
                            orientation: 'landscape',
                            pageSize:'CUSTOM',
                            customize : function(doc){
            var colCount = new Array();
            $('#userTable').find('tbody tr:first-child td').each(function(){
                if($(this).attr('colspan')){
                    for(var i=1;i<=$(this).attr('colspan');$i++){
                        colCount.push('*');
                    }
                }else{ colCount.push('*'); }
            });
            doc.content[1].table.widths = colCount;
        }


                        },
                        {
                            extend: 'print',
                            footer: false,
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'COPY',
                            exportOptions: {
                            }
                        }
                    ]
                });

                loadStates("stateid", "cityid");
                loadCitiesByState("stateid", "cityid");

                var admin = $("#tipoUsuario").val();

                $('#saveButton1').confirmation({
                    onConfirm: function (event, element) {
                        if (validateForm("creationUser")) {
                            $('#creationUser').submit();
                        }
                    }
                });

                $('#saveButton2').confirmation({
                    onConfirm: function (event, element) {
                        if (validateForm("editionPassword")) {
                            $('#editionPassword').submit();
                        }
                    }
                });

                if ($("#documentTypeid").val() === '2' || $("#documentTypeid").val() === '6') {
                    $("#identificationNumber").removeAttr("data-rule-alpharule");
                    $("#identificationNumber").attr("data-rule-intrule", "true");
                    $("#identificationNumber").attr("onkeypress", "return isNumberKey(event)");
                } else if ($("#documentTypeid").val() === '5' || $("#documentTypeid").val() === '3') {
                    $("#identificationNumber").removeAttr("data-rule-intrule");
                    $("#identificationNumber").attr("data-rule-alpharule", "true");
                    $("#identificationNumber").removeAttr("onkeypress");
                } else {
                    $("#identificationNumber").attr("data-rule-intrule", "true");
                    $("#identificationNumber").attr("onkeypress", "return isNumberKey(event)");
                }
                
                if(${mintrabUser}){
                    $('#username').attr("readonly", "readonly");
                }

                if (admin === "false") {
                    $('.listAdmin').hide();
                    $('.listUser').show();
                    $('.disable').attr("readonly", "readonly");
                } else {
                    $('.listUser').hide();
                    $('.listAdmin').show();
                }

            <c:if test="${not empty internalMsg}">
                
                if(${mintrabUser}){
                    $('#username').attr("readonly", "readonly");
                }
                
                if (admin === "false") {
                    $('.listAdmin').hide();
                    $('.listUser').show();
                    $('.disable').attr("readonly", "readonly");
                    $('.contraModal').hide();
                } else {
                    $('.listUser').hide();
                    $('.listAdmin').show();
                    $('.contraModal').show();
                }
            </c:if>

                $('#stateid').val(${creationUser.state.id});
                loadCities(jQuery("#stateid").val(), "stateid", "cityid");
                $('#cityid').val(${creationUser.city.id});

            });
            
            $.validator.addMethod('alpharule', function (val, elem) {
                var filter = /^([0-9a-zA-Z]{4,10})*$/;

                if (!filter.test(val)) {
                    return false;
                } else {
                    return true;
                }
            }, 'Ingrese un n�mero de identificaci�n v�lido seg�n el tipo de documento seleccionado.');

            $.validator.addMethod('emailtld', function (val, elem) {
                var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

                if (!filter.test(val)) {
                    return false;
                } else {
                    return true;
                }
            }, 'Por favor, escriba una direcci�n de correo v�lida.');

            $.validator.addMethod('intrule', function (val, elem) {
                var filter = /^([0-9]{4,10})*$/;

                if (!filter.test(val)) {
                    return false;
                } else {
                    return true;
                }
            }, 'Por favor, escriba un numero de identificaci�n v�lido.');
            
            $.validator.addMethod('passrule', function (val, elem) {
                var filter = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^a-zA-Z0-9])[A-Za-z\d^a-zA-Z0-9]{8,15}/;

                if (!filter.test(val)) {
                    return false;
                } else {
                    return true;
                }
            }, 'Por favor, ingrese la contrase�a seg�n los par�metros indicados.');

            function activacionDigito() {
                if ($("#documentTypeid").val() === '2' || $("#documentTypeid").val() === '6') {
                    $("#dv").val("");
                    $("#identificationNumber").attr("data-rule-intrule", "true");
                    $("#identificationNumber").attr("onkeypress", "return isNumberKey(event)");
                    $("#identificationNumber").val("");
                } else if ($("#documentTypeid").val() === '5' || $("#documentTypeid").val() === '3') {
                    $("#dv").val("");
                    $("#identificationNumber").removeAttr("data-rule-intrule");
                    $("#identificationNumber").removeAttr("onkeypress");
                    $("#identificationNumber").val("");
                } else {
                    $("#identificationNumber").attr("data-rule-intrule", "true");
                    $("#identificationNumber").attr("onkeypress", "return isNumberKey(event)");
                    $("#identificationNumber").val("");
                }
            }

            function loadStates(deptoId, mnpoId) {
            <c:if test="${not empty stateList}">
                <c:forEach var="item" items="${stateList}" varStatus="iter">
                    <c:if test="${iter.index == 0}">
                jQuery("<option>").attr("value", '${item.id}').
                        text('${item.name}').attr("selected", "false").
                        appendTo("#" + deptoId);
                loadCities("${item.id}", deptoId, mnpoId);
                    </c:if>
                    <c:if test="${iter.index > 0}">
                jQuery("<option>").attr("value", '${item.id}').
                        text('${item.name}').appendTo("#" + deptoId);
                    </c:if>
                </c:forEach>
            </c:if>
            }

            function loadCitiesByState(deptoId, mnpoId) {
                jQuery("#" + deptoId).change(function () {
                    jQuery("#" + mnpoId).html('');
                    var stateCode = jQuery("#" + deptoId).val();
                    loadCities(stateCode, deptoId, mnpoId);
                });
            }

            function loadCities(stateCode, deptoId, mnpoId) {
                jQuery("#" + mnpoId).find('option').remove();
                jQuery("<option>").attr("value", '').
                        text('Seleccione...').appendTo("#" + mnpoId);
            <c:if test="${not empty citiesList}">
                <c:forEach items="${citiesList}" var="item" >
                var code = "<c:out value="${item.state.id}"/>";
                if (code === stateCode) {
                    jQuery("<option>").attr("value", '${item.id}').
                            text('${item.name}').appendTo("#" + mnpoId);
                }
                </c:forEach>
            </c:if>
            }
            
            $.validator.addMethod('celrule', function (val, elem) {
                var filter = /^([0-9]{7,10})*$/;

                if (!filter.test(val)) {
                    return false;
                } else {
                    return true;
                }
            }, 'Por favor, escriba un tel�fono de contacto v�lido.');

        </script>
        <style>
            .buttons-csv{
                display: inline-block;
                background-image:url(resources/images/csv.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-csv span{
                opacity: 0;
            }
            .buttons-excel{
                display: inline-block;
                background-image:url(resources/images/excel.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-excel span{
                opacity: 0;
            }
            .buttons-pdf{
                display: inline-block;
                background-image:url(resources/images/pdf.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-pdf span{
                opacity: 0;
            }
            .buttons-print{
                display: inline-block;
                background-image:url(resources/images/print.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-print span{
                opacity: 0;
            }

            div.dt-buttons {
                float: right;
                margin-left:10px;
            }
        </style>
    </body>
</html>
