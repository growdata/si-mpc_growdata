/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: InstitutionService.java
 * Created on: 2016/10/19, 02:10:35 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.entity.Program;
import com.gml.simpc.api.entity.exeption.UserException;
import java.util.List;

/**
 * Interface que define el servicio de los programas
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public interface ProgramService {

    /**
     * Servicio para guardar un programa
     *
     * @param program
     */
    void save(Program program) throws UserException;
    
    /**
     * Servicio para guardar un programa
     *
     * @param program
     */
    void saveUpdate(Program program);

    /**
     * Servicio para actualizar programas
     *
     * @param program
     */
    void update(Program program);

    /**
     * Elimina un programa
     *
     * @param program
     */
    void remove(Program program);

    /**
     * Trae todos los programas
     *
     * @return
     */
    List<Program> getAll();

    /**
     * Trae los programas por filtros.
     *
     * @param program
     *
     * @return
     */
    List<Program> findByFilters(Program program);

    /**
     * Trae un programa por el id.
     *
     * @param id
     *
     * @return
     */
    Program findById(Long id);
    
    
      /**
     * Trae un programa por el codigo.
     *
     * @param code
     *
     * @return
     */
    Program findByCode(String code);
}
