/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: GraphicDao.java
 * Created on: 2016/12/27, 04:06:52 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dao;

import com.gml.simpc.api.dto.ManagementAndPlacementDto;
import java.util.Date;
import java.util.List;

/**
 * Interface que define el DAO para consultar informaci&oacute;n del portal
 * de beneficios economicos
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public interface ManagementAndPlacementDao {

    /**
     * Obtiene consulta  de gestion y colocacion
     *
     *
     * @return List<ManagementAndPlacementDto>
     */
    List<ManagementAndPlacementDto> findManagementAndPlacementResults(Date init,
       Date finish, String postulationStatus);
    
  

}
