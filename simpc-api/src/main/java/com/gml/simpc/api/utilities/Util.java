/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Utilitario.java
 * Created on: 2016/10/21, 01:36:43 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.utilities;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author <a href="mailto:julianm@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
public final class Util {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(Util.class);
    /**
     * Constante para retornar todos los valores
     */
    public static final String ALL_VALUES = "%";

    /**
     * Cadena vac&iacute;a
     */
    public static final String SPACE = " ";
    /**
     * Cadena vac&iacute;a
     */
    public static final String EMPTY_TEXT = "";
    /**
     * id constante de tipo institucion cajas de compensaci&oacute;n familiar
     */
    public static final Long INSTITUTION_TYPE_CAJA_COMPENSACION_FAMILIAR = 1L;
    /**
     * id constante de tipo institucion formaci&oacute;n para el trabajo
     */
    public static final Long INSTITUTION_TYPE_FORMACION_TRABAJO = 2L;
    /**
     * Estilo para mensajes de procesos exitosos.
     */
    public static final String SUCCESS_ALERT = "success";
    /**
     * Estilo para mensajes de procesos fallidos.
     */
    public static final String FAILED_ALERT = "danger";
    /**
     * Estilo para mensajes de procesos fallidos.
     */
    public static final String DOCUMENT_NOT_FOUND = "No se encontr� informaci�n para el tipo y n�mero de documento ingresado:";
    /**
     * Atributo para la descargar del manual de usuarios
     */
    public static final String MANUAL_PATH = "MANUAL_USUARIO_PATH";
    /**
     * Atributo para la descarga de la ayuda en l&iacute;nea
     */
    public static final String HELP_PATH = "AYUDA_EN_LINEA_PATH";
    /**
     * Atributo para la descarga de la ayuda en l&iacute;nea
     */
    public static final String ADMIN_PATH = "MANUAL_ADMINISTRADOR_PATH";
    /**
     * Nombre de la tabla de la lista de valor de grupos etnicos
     */
    public static final String ETHNIC_GROUP_TABLE_NAME = "GRUPO_ETNICO";
    /**
     * Nombre de la tabla de la lista de valor de grupos etnicos
     */
    public static final String JAR_PATH = "JAR_DESCARGABLE_PATH";

    /**
     * Usuario en sesi&oacute;n.
     */
    public static final String SESSION_USER = "session_user";
    /**
     * TECNICO LABORAL
     */
    public static final String TECHNICAL_JOB = "T�cnico Laboral en :";
    /**
     * Permisos de la sesi&oacute;n.
     */
    public static final String SESSION_PERMISSIONS = "session_permissions";
    /**
     * Lista de meses
     */
    public static final List<String> MONTHS_NAMES = new LinkedList<>();

    /**
     * Lista de estados de postulacion
     */
    public static final List<String> POSTULATION_STATUS = new LinkedList<>();
    /**
     * Tamano maximo permitido para carga web de archivos.
     */
    public static final long WEB_LOAD_MAX_SIZE = 20971520;

    /**
     * Tamano maximo permitido para carga ftp de archivos.
     */
    public static final long FTP_LOAD_MAX_SIZE = Long.MAX_VALUE;

    /**
     * Extensiones permitidas para carga web de archivos.
     */
    public static final String LOAD_VALID_EXT = "txt";
    
    /**
     * Extensiones permitidas para carga web de archivos.
     */
    public static final String LOAD_VALID_EXT_PDF = "pdf";

    /**
     * Extensi�n para archivos de error.
     */
    public static final String ERROR_FILE_EXT = "csv";

    /**
     * Sufijo para archivo de errores de estructura.
     */
    public static final String ERROR_FILE_SUFIX_STRUCTURE =
        "_errores_estructura";

    /**
     * Sufijo para archivo de errores de estructura.
     */
    public static final String ERROR_FILE_SUFIX_BUSSINESS = "_errores_negocio";

    /**
     * Mensaje post validacion de estructura de archivos.
     */
    public static final String LOAD_SUCCESS_MESSAGE =
        "Archivo validado satisfactoriamente. " +
        "Recibir� un correo electr�nico con el resultado del procesamiento.";

    /**
     * Mensaje correo cambio de contrasena
     */
    public static final String PASSWORD_RECOVERY_SUCCESFUL =
        " Se ha enviado a su correo electr�nico  un  enlace para el" +
        " cambio de la contrase�a";
    /**
     * nombre de par&aacute;metro dominio
     */
    public static final String PARAMETER_NAME_DOMAIN = "DOMINIO";
    /**
     * Par&aacute;metro para la ruta donde se encuentran los archivos de cruce.
     */
    public static final String INTERSECTION_FILES_PATH = "ARCHIVOS_CRUCE_PATH";
    /**
     * nombre de par&aacute;metro nombre firma
     */
    public static final String PARAMETER_SIGNATURE = "NOMBRE_FIRMA";
    /**
     * nombre de par&aacute;metro Para subject en la activacion de una
     * institucion
     */
    public static final String PARAMETER_ACTIVATE_INSTITUTION =
        "ACTIVATE_INSTITUTION";
    /**
     * nombre de par&aacute;metro Para subject en la Creacion de una institucion
     */
    public static final String PARAMETER_CREATE_INSTITUTION =
        "CREATE_INSTITUTION";
    /**
     * nombre de par&aacute;metro Para subject al usuario
     */
    public static final String PARAMETER_USER_MESSAGE = "USER_MESSAGE";
    /**
     * nombre de par&aacute;metro Para activacion de password
     */
    public static final String PARAMETER_PASS = "USER_PASSWORD";
    /*
     * nombre de par&aacute;metro Para subject en la aprobacion de una
     * institucion
     */
    public static final String PARAMETER_APPROVAL_INSTITUTION =
        "APPROVAL_INSTITUTION";
    
    /*
     * nombre de par&aacute;metro Para subject en la aprobacion de una
     * institucion
     */
    public static final String PARAMETER_MASS_CONSULT =
        "MASS_CONSULT";
    /*
     * nombre de par&aacute;metro Para subject en la aprobacion de una
     * institucion
     */
    public static final String PARAMETER_REJECT_INSTITUTION =
        "REJECT_INSTITUTION";
    /*
     * nombre de par&aacute;metro Para subject al momento de guardar una sede
     */
    public static final String PARAMETER_SAVE_HEADQUARTER = "SAVE_HEADQUARTER";
    /*
     * nombre de par&aacute;metro Para subject para terminacion de la cola de
     * carga
     */
    public static final String PARAMETER_LOAD_QUEUE = "LOAD_QUEUE";
    /**
     * nombre de par&aacute;metro posicion de quien firma un correo
     */
    public static final String PARAMETER_SIGNATURE_POSITION = "CARGO_FIRMA";
    /**
     * id del par&aacute;metro de la ruta para guardar los archivos
     */
    public static final Long ID_PARAMETRO_PATH_CRUCES = 7L;
    /**
     * id del par&aacute;metro de la ruta para guardar los archivos
     */
    public static final String TEMP_RUTA ="datosplanos" ;
    
    /**
     * id del tipo de programa t&eacute;cnico laboral
     */
    public static final Long ID_PROGRAM_TYPE_LABOR_TECHNICIAN = 4L;
    /**
     * id del tipo de programa M&oacute;dulo TICS
     */
    public static final Long ID_PROGRAM_TYPE_TICS_MODULE = 2L;

    /**
     * NIT Entidad Unidad Administrativa Especial del Servicio Publico
     */
    public static final String NIT_ENTIDAD_SERVICIO_PUBLICO = "900678508-4";

    /**
     * NIT Ministerio de trabajo
     */
    public static final String NIT_ENTIDAD_MINISTRY_OF_LABOR = "830115226-3";

    /**
     * Expresi&oacute;n regular que valida que el password tenga
     * al menos 8 car&aacute;cteres, al menos una letra may&uacute;scula
     * y al menos un n&uacute;mero
     */
    public static final String PASS_REG_EXP =
        "^(?=.*[0-9])(?=.*[A-Z]).{8,}$";

    static {
        MONTHS_NAMES.add("Enero");
        MONTHS_NAMES.add("Febrero");
        MONTHS_NAMES.add("Marzo");
        MONTHS_NAMES.add("Abril");
        MONTHS_NAMES.add("Mayo");
        MONTHS_NAMES.add("Junio");
        MONTHS_NAMES.add("Julio");
        MONTHS_NAMES.add("Agosto");
        MONTHS_NAMES.add("Septiembre");
        MONTHS_NAMES.add("Octubre");
        MONTHS_NAMES.add("Noviembre");
        MONTHS_NAMES.add("Diciembre");        
        
        POSTULATION_STATUS.add("Autopostulado");
        POSTULATION_STATUS.add("Autopostulado Asistido");
        POSTULATION_STATUS.add("Remitido por prestador");
        POSTULATION_STATUS.add("Preseleccionado");
        POSTULATION_STATUS.add("Descartado por prestador");
        POSTULATION_STATUS.add("No seleccionado empleador");
        POSTULATION_STATUS.add("Colocado");
        POSTULATION_STATUS.add("Declinado");
    }
    
    /**
     * Enumeraci&oacute;n para decidir los filtros de consulta.
     */
    public enum Filter {
        WITHOUT_DATES, WITH_RANGE, WITH_START_DATE, WITH_END_DATE
    }

    private Util() {
    }

    /**
     * Calcula el Hash de un string utilizando el algoritmo SHA-1
     *
     * @param texto
     *
     * @return Hash del texto utilizando el algor&icute;tmo
     */
    public static String calculateSha1(String texto) {
        LOGGER.info("Metodo [ calculateSha1 ]");

        return DigestUtils.sha1Hex(texto);
    }

    /**
     * M&eacute;todo que convierte un valor nulo o vac&iacute;o en "%"
     *
     * @param value
     *
     * @return
     */
    public static String nullValue(String value) {
        LOGGER.info("Dato que se desea evaluar" + value);
        if (!StringUtils.isBlank(value)) {
            return value;
        } else {
            return ALL_VALUES;
        }
    }

    /**
     * M&eacute;todo que convierte un valor nulo o vac&iacute;o en "%"
     *
     * @param value
     *
     * @return
     */
    public static String nullValue(Long value) {
        LOGGER.info("Dato que se desea evaluar" + value);
        if (value != null) {
            return value + "";
        } else {
            return ALL_VALUES;
        }
    }

    /**
     * M&eacute;todo que convierte un valor nulo o vac&iacute;o en "%"
     *
     * @param value
     *
     * @return
     */
    public static String likeValue(String value) {
        LOGGER.info("Dato que se desea evaluar" + value);
        if (!StringUtils.isBlank(value)) {
            return ALL_VALUES + value + ALL_VALUES;
        } else {
            return ALL_VALUES;
        }
    }

    /**
     * Selecciona el filtro adecuado a emplear teniendo en cuenta las fechas.
     *
     * @param startDate Fecha de incio.
     * @param endDate   Fecha de fin.
     *
     * @return
     */
    public static Filter selectFilter(Date startDate, Date endDate) {
        if (startDate != null && endDate != null) {
            return Filter.WITH_RANGE;
        }

        if (startDate != null) {
            return Filter.WITH_START_DATE;
        }

        if (endDate != null) {
            return Filter.WITH_END_DATE;
        }

        return Filter.WITHOUT_DATES;
    }
    /**
     * M&eacute;todo que convierte un valor 0 o vac&iacute;o en "%"
     *
     * @param value
     *
     * @return
     */
    public static String ceroValue(int value) {
        LOGGER.info("Dato que se desea evaluar" + value);
        
        return value == 0 ? ALL_VALUES : Integer.toString(value);
    }
}
