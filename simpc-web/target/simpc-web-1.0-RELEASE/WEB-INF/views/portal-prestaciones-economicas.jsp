<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Informaci�n de Prestaciones Econ�micas</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster.bundle.min.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster-sideTip-shadow.min.css" rel="stylesheet"/>
        <link href="resources/css/morris.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <style>
            in  put[type=number]::-webkit-outer-spin-button,
            input[type=number]::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            input[type=number] {
                -moz-appearance:textfield;
            }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <div class="col-sm-3" style="padding:0px;">
                        <img src="resources/images/logo_mpc.png" width="100%" alt=""/>
                    </div>
                    <div class="col-sm-8">
                        <a class="navbar-brand" href="/masterdata/index.htm">Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</a>
                    </div>
                </div>
                <div class="navbar-nav navbar-form credenciales pull-right">
                    <table>
                        <tr>
                            <td>
                                <strong>Bienvenido</strong> 
                            </td>
                            <td style="width: 120px; text-align: right">
                                <a id="" href="/masterdata/index.htm" class="btn btn-warning btn-xs">Volver al inicio</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="row row-offcanvas row-offcanvas-left">
                <h1>Informaci�n de Prestaciones Econ�micas</h1>
                <!-- main area -->
                <div class="panel panel-default">
                    <div class="panel-heading"><h2>Filtros de B�squeda</h2></div>
                    <div class="panel-body">
                        <ul class="nav nav-tabs">
                            <li><a data-toggle="tab" href="#consultaConsolidadoPostulantes" id="tabConsolidadoPostulantes" onclick="borraGrafico()">Consulta Consolidada Postulantes</a></li>
                            <li><a data-toggle="tab" href="#consultaDetalladaPostulantes"  id="tabDetalladaPostulantes" onclick="borraGrafico()">Consulta Detallada Postulantes</a></li>
                            <li><a data-toggle="tab" href="#consultaConsolidadaBeneficiarios" id="tabConsolidadaBeneficiarios" onclick="borraGrafico()">Consulta Consolidada Beneficiarios</a></li>
                            <li><a data-toggle="tab" href="#consultaDetalladaBeneficiarios" id="tabDetalladaBeneficiarios" onclick="borraGrafico()">Consulta Detallada Beneficiarios</a></li>
                        </ul>
                        <div id="tabs" class="tab-content">
                            <div id="alert-area" ></div>
                            <div id="consultaConsolidadoPostulantes" class="tab-pane fade in ">
                                <div class="panel-body">
                                    <legend>
                                        <form:form method="get" action="buscarPrestacionesEconomicas.htm">
                                            <input type="hidden" id="activateBtn" value="${activateBtn}">
                                            <input type="hidden" id="tipoConsulta" name="tipoConsulta" value="1" path="tipoConsulta"/>
                                            <div id="filtrosConsultaTipoUno">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="ccf" class=" control-label">Caja de Compensaci�n</label>
                                                        <select class="form-control" id="ccf" path="ccf" name="ccf">
                                                            <option label="Todas las cajas de compensaci�n" value="">Todas las cajas de compensaci�n</option>
                                                            <c:if test="${not empty foundations}">
                                                                <c:forEach var="item" items="${foundations}">
                                                                    <c:if test="${item.isCCF == 'V'}">
                                                                        <option <c:if test="${item.code eq ccfSelected}">selected="selected"</c:if> label="${item.shortName}" value="${item.code}">${item.shortName}</option>
                                                                    </c:if>
                                                                </c:forEach>
                                                            </c:if>
                                                        </select>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="ano" class=" control-label">A�o</label>
                                                        <select class="form-control" id="ano" path="ano" required="required" name="ano">
                                                            <option label="Seleccione un a�o" value="">Seleccione un a�o</option>
                                                            <c:if test="${not empty years}">
                                                                <c:forEach var="item" items="${years}">
                                                                    <option <c:if test="${item eq yearSelected}">selected="selected"</c:if> label="${item}" value="${item}">${item}</option>
                                                                </c:forEach>
                                                            </c:if>
                                                        </select>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="mes" class=" control-label">Mes</label>
                                                        <input type="hidden" id="mes" path="mes" name="mes" value="">
                                                        <select disabled="disabled" class="form-control" id="mes" path="mes" name="mes">
                                                            <option label="Todos los meses">Todos los meses</option>
                                                        </select>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-sm-offset-4 col-sm-8" style="text-align:right">
                                                <button type="submit" class="btn btn-success">Buscar</button>
                                            </div>
                                        </form:form>
                                    </legend>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel panel-default" style="width: 100%; margin: 0 auto;">
                                    <div class="panel-heading"><h2>Resultados de la B�squeda</h2></div>
                                    <div class="panel-body">
                                        <div class=" table-responsive col-md-12">
                                            <center>
                                                <button type="button" class="btn btn-default desableBtn"
                                                        data-toggle="modal" data-target="#graphicModal">  
                                                    <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                                                    Ver gr&aacute;fico
                                                </button>
                                            </center>
                                            <table id="resultsConsolidPostulants" class="table table-striped table-bordered" 
                                                   cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th class ="orden" ></th>
                                                        <th>Mes</th>
                                                        <th>Postulantes al subsidio</th>
                                                        <th>Solicitudes aprobadas</th>
                                                        <th>Solicitudes denegadas</th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr>
                                                        <th class ="orden" ></th>
                                                        <th>Mes</th>
                                                        <th>Postulantes al subsidio</th>
                                                        <th>Solicitudes aprobadas</th>
                                                        <th>Solicitudes denegadas</th>
                                                    </tr>
                                                </tfoot>
                                                <tbody>
                                                    <c:if test="${not empty resultList1}">
                                                        <c:forEach var="item" items="${resultList1}" begin="0">
                                                            <tr>
                                                                <td class="orden">${item.order}</td>
                                                                <td>${item.month}</td>
                                                                <td align="right">${item.postulants}</td>
                                                                <td align="right">${item.approvedApplications}</td>
                                                                <td align="right">${item.requestsDenied}</td>
                                                            </tr>
                                                        </c:forEach>
                                                    </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <div id="consultaDetalladaPostulantes" class="tab-pane fade ">
                                <div class="panel-body">
                                    <legend>
                                        <form:form method="get" action="buscarPrestacionesEconomicas.htm">
                                            <input type="hidden" id="tipoConsulta" name="tipoConsulta" value="2" path="tipoConsulta"/>
                                            <div id="filtrosConsultaTipoUno">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="ccf" class=" control-label">Caja de Compensaci�n</label>
                                                        <input type="hidden" id="ccf" path="ccf" name="ccf" value="">
                                                        <select disabled="disabled" class="form-control" id="ccf" path="ccf" name="ccf">
                                                            <option label="Todas las cajas de compensaci�n">Todas las cajas de compensaci�n</option>
                                                        </select>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="ano" class=" control-label">A�o</label>
                                                        <select class="form-control" id="ano" path="ano" required="required" name="ano">
                                                            <option label="Seleccione un a�o" value="">Seleccione un a�o</option>
                                                            <c:if test="${not empty years}">
                                                                <c:forEach var="item" items="${years}">
                                                                    <option <c:if test="${item eq yearSelected}">selected="selected"</c:if> label="${item}" value="${item}">${item}</option>
                                                                </c:forEach>
                                                            </c:if>
                                                        </select>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="mes" class=" control-label">Mes</label>
                                                        <select class="form-control" id="mes" path="mes" name="mes">
                                                            <option label="Todos los meses" value="">Todos los meses</option>
                                                            <option <c:if test="${monthsSelected == '01'}">selected="selected"</c:if> value="01">Enero</option>
                                                            <option <c:if test="${monthsSelected == '02'}">selected="selected"</c:if> value="02">Febrero</option>
                                                            <option <c:if test="${monthsSelected == '03'}">selected="selected"</c:if> value="03">Marzo</option>
                                                            <option <c:if test="${monthsSelected == '04'}">selected="selected"</c:if> value="04">Abril</option>
                                                            <option <c:if test="${monthsSelected == '05'}">selected="selected"</c:if> value="05">Mayo</option>
                                                            <option <c:if test="${monthsSelected == '06'}">selected="selected"</c:if> value="06">Junio</option>
                                                            <option <c:if test="${monthsSelected == '07'}">selected="selected"</c:if> value="07">Julio</option>
                                                            <option <c:if test="${monthsSelected == '08'}">selected="selected"</c:if> value="08">Agosto</option>
                                                            <option <c:if test="${monthsSelected == '09'}">selected="selected"</c:if> value="09">Septiembre</option>
                                                            <option <c:if test="${monthsSelected == '10'}">selected="selected"</c:if> value="10">Octubre</option>
                                                            <option <c:if test="${monthsSelected == '11'}">selected="selected"</c:if> value="11">Noviembre</option>
                                                            <option <c:if test="${monthsSelected == '12'}">selected="selected"</c:if> value="12">Diciembre</option>
                                                        </select>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-sm-offset-4 col-sm-8" style="text-align:right">
                                                <button type="submit" class="btn btn-success">Buscar</button>
                                            </div>
                                        </form:form>
                                    </legend>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel panel-default" style="width: 100%; margin: 0 auto;">
                                    <div class="panel-heading"><h2>Resultados de la B�squeda</h2></div>
                                    <div class="panel-body">
                                        <div class=" table-responsive col-md-12">
                                            <center>
                                                <button type="button" class="btn btn-default desableBtn"
                                                        data-toggle="modal" data-target="#graphicModal">  
                                                    <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                                                    Ver gr&aacute;fico
                                                </button>
                                            </center>
                                            <table id="resultsDetailPostulants" class="table table-striped table-bordered" 
                                                   cellspacing="0" width="100%" >
                                                <thead>
                                                    <tr>
                                                         <th class="orden"></th>
                                                        <th>Nombre de la CCF</th>
                                                        <th>Postulantes al subsidio</th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr>
                                                        <th class="orden"></th>
                                                        <th>Nombre de la CCF</th>
                                                        <th>Postulantes al subsidio</th>
                                                    </tr>
                                                </tfoot>
                                                <tbody>
                                                    <c:if test="${not empty resultList2}">
                                                        <c:forEach var="item" items="${resultList2}">
                                                            <tr>
                                                                <td class="orden">${item.order}</td>
                                                                <td>${item.month}</td>
                                                                <td align="right">${item.postulants}</td>
                                                            </tr>
                                                        </c:forEach>
                                                    </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <div id="consultaConsolidadaBeneficiarios" class="tab-pane fade ">
                                <div class="panel-body">
                                    <legend>
                                        <form:form method="get" action="buscarPrestacionesEconomicas.htm">
                                            <input type="hidden" id="tipoConsulta" name="tipoConsulta" value="3" path="tipoConsulta"/>
                                            <div id="filtrosConsultaTipoUno">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="ccf" class=" control-label">Caja de Compensaci�n</label>
                                                        <select class="form-control" id="ccf" path="ccf" name="ccf">
                                                            <option label="Todas las cajas de compensaci�n" value="">Todas las cajas de compensaci�n</option>
                                                            <c:if test="${not empty foundations}">
                                                                <c:forEach var="item" items="${foundations}">
                                                                    <c:if test="${item.isCCF == 'V'}">
                                                                        <option <c:if test="${item.code eq ccfSelected}">selected="selected"</c:if> label="${item.shortName}" value="${item.code}">${item.shortName}</option>
                                                                    </c:if>
                                                                </c:forEach>
                                                            </c:if>
                                                        </select>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="ano" class=" control-label">A�o</label>
                                                        <select class="form-control" id="ano" path="ano" required="required" name="ano">
                                                            <option label="Seleccione un a�o" value="">Seleccione un a�o</option>
                                                            <c:if test="${not empty years}">
                                                                <c:forEach var="item" items="${years}">
                                                                    <option <c:if test="${item eq yearSelected}">selected="selected"</c:if> label="${item}" value="${item}">${item}</option>
                                                                </c:forEach>
                                                            </c:if>
                                                        </select>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="mes" class=" control-label">Mes</label>
                                                        <input type="hidden" id="mes" path="mes" name="mes" value="">
                                                        <select disabled="disabled" class="form-control" id="mes" path="mes" name="mes">
                                                            <option label="Todos los meses">Todos los meses</option>
                                                        </select>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-sm-offset-4 col-sm-8" style="text-align:right">
                                                <button type="submit" class="btn btn-success">Buscar</button>
                                            </div>
                                        </form:form>
                                    </legend>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel panel-default" style="width: 100%; margin: 0 auto;">
                                    <div class="panel-heading"><h2>Resultados de la B�squeda</h2></div>
                                    <div class="panel-body">
                                        <div class=" table-responsive col-md-12">
                                            <center>
                                                <button type="button" class="btn btn-default desableBtn"
                                                        data-toggle="modal" data-target="#graphicModal">  
                                                    <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                                                    Ver gr&aacute;fico
                                                </button>
                                            </center>
                                            <table id="resultsConsolidatedBenefits" class="table table-striped table-bordered" 
                                                   cellspacing="0" width="100%" >
                                                <thead>
                                                    <tr>
                                                        <th class="orden"></th>
                                                        <th>Mes</th>
                                                        <th>Beneficiarios del Subsidio</th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr>
                                                        <th class="orden"></th>
                                                        <th>Mes</th>
                                                        <th>Beneficiarios del Subsidio</th>
                                                    </tr>
                                                </tfoot>
                                                <tbody>
                                                    <c:if test="${not empty resultList3}">
                                                        <c:forEach var="item" items="${resultList3}">
                                                            <tr>
                                                                 <td class="orden">${item.order}</td>
                                                                <td>${item.month}</td>
                                                                <td align="right">${item.postulants}</td>
                                                            </tr>
                                                        </c:forEach>
                                                    </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <div id="consultaDetalladaBeneficiarios" class="tab-pane fade ">
                                <div class="panel-body">
                                    <legend>
                                        <form:form method="get" action="buscarPrestacionesEconomicas.htm">
                                            <input type="hidden" id="tipoConsulta" name="tipoConsulta" value="4" path="tipoConsulta"/>
                                            <div id="filtrosConsultaTipoUno">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="ccf" class=" control-label">Caja de Compensaci�n</label>
                                                        <input type="hidden" id="ccf" path="ccf" name="ccf" value="">
                                                        <select disabled="disabled" class="form-control">
                                                            <option label="Todas las cajas de compensaci�n">Todas las cajas de compensaci�n</option>
                                                        </select>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="ano" class=" control-label">A�o</label>
                                                        <select class="form-control" id="ano" path="ano" required="required" name="ano">
                                                            <option label="Seleccione un a�o" value="">Seleccione un a�o</option>
                                                            <c:if test="${not empty years}">
                                                                <c:forEach var="item" items="${years}">
                                                                    <option <c:if test="${item eq yearSelected}">selected="selected"</c:if> label="${item}" value="${item}">${item}</option>
                                                                </c:forEach>
                                                            </c:if>
                                                        </select>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="mes" class=" control-label">Mes</label>
                                                        <select class="form-control" id="mes" path="mes" name="mes">
                                                            <option label="Todos los meses" value="">Todos los meses</option>
                                                            <option <c:if test="${monthsSelected == '01'}">selected="selected"</c:if> value="01">Enero</option>
                                                            <option <c:if test="${monthsSelected == '02'}">selected="selected"</c:if> value="02">Febrero</option>
                                                            <option <c:if test="${monthsSelected == '03'}">selected="selected"</c:if> value="03">Marzo</option>
                                                            <option <c:if test="${monthsSelected == '04'}">selected="selected"</c:if> value="04">Abril</option>
                                                            <option <c:if test="${monthsSelected == '05'}">selected="selected"</c:if> value="05">Mayo</option>
                                                            <option <c:if test="${monthsSelected == '06'}">selected="selected"</c:if> value="06">Junio</option>
                                                            <option <c:if test="${monthsSelected == '07'}">selected="selected"</c:if> value="07">Julio</option>
                                                            <option <c:if test="${monthsSelected == '08'}">selected="selected"</c:if> value="08">Agosto</option>
                                                            <option <c:if test="${monthsSelected == '09'}">selected="selected"</c:if> value="09">Septiembre</option>
                                                            <option <c:if test="${monthsSelected == '10'}">selected="selected"</c:if> value="10">Octubre</option>
                                                            <option <c:if test="${monthsSelected == '11'}">selected="selected"</c:if> value="11">Noviembre</option>
                                                            <option <c:if test="${monthsSelected == '12'}">selected="selected"</c:if> value="12">Diciembre</option>
                                                        </select>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-sm-offset-4 col-sm-8" style="text-align:right">
                                                <button type="submit" class="btn btn-success">Buscar</button>
                                            </div>
                                        </form:form>
                                    </legend>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel panel-default" style="width: 100%; margin: 0 auto;">
                                    <div class="panel-heading"><h2>Resultados de la B�squeda</h2></div>
                                    <div class="panel-body">
                                        <div class=" table-responsive col-md-12">
                                            <center>
                                                <button type="button" class="btn btn-default desableBtn"
                                                        data-toggle="modal" data-target="#graphicModal">  
                                                    <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                                                    Ver gr&aacute;fico
                                                </button>
                                            </center>
                                            <table id="resultsDetailedBenefits" class="table table-striped table-bordered" 
                                                   cellspacing="0" width="100%" >
                                                <thead>
                                                    <tr>
                                                        <th class="orden"></th>
                                                        <th>Nombre de la CCF</th>
                                                        <th>Beneficiarios del Subsidio</th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr>
                                                        <th class="orden"></th>
                                                        <th>Nombre de la CCF</th>
                                                        <th>Beneficiarios del Subsidio</th>
                                                    </tr>
                                                </tfoot>
                                                <tbody>
                                                    <c:if test="${not empty resultList4}">
                                                        <c:forEach var="item" items="${resultList4}">
                                                            <tr>
                                                                <td class="orden">${item.order}</td>
                                                                <td>${item.month}</td>
                                                                <td align="right">${item.postulants}</td>
                                                            </tr>
                                                        </c:forEach>
                                                    </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="clearfix"></div> 

        <div class="modal fade" id="graphicModal" tabindex="-1" role="dialog" aria-labelledby="graphicModalLabel">
            <div class="modal-dialog modal-lg" style="width: 1000px;" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12  " id="graphicContainer2">
                                <h4 class="modal-title" id="graphicModalLabel"><Strong><center>${titulo}</center></Strong></h4>
                                <div id="graphicContainer" ></div>
                                <br>


                                <div id="legend" class="graphic-legend inline-block"></div>
                                <br> <br> <br>
                                <Strong> ${titulografica}</Strong>
                                <div class=" table-responsive col-md-12">
                                    <table id="tablag" class=" table-striped table-bordered " cellspacing="0" width="100%">
                                        <thead>

                                            <tr>
                                                 <th class="orden"></th>
                                                <c:if test="${not empty titleList}">
                                                    <c:forEach items="${titleList}" var="info" begin="0">

                                                        <th>${info}</th>

                                                    </c:forEach>  
                                                </c:if>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            
                                            <c:if test="${not empty graphicList}">
                                                <c:forEach items="${graphicList}" var="item" begin="0">

                                                    <tr> 
                                                         <td class="orden">${item.order}</td>
                                                        <td>${item.month}</td>
                                                        <td align="right">${item.postulants}</td>
                                                        <td align="right">${item.approvedApplications}</td>
                                                        <td align="right">${item.requestsDenied}</td>

                                                    </tr>   
                                                </c:forEach>  
                                            </c:if>
                                            <c:if test="${not empty graphicList2}">
                                                <c:forEach items="${graphicList2}" var="item" begin="0">

                                                    <tr> 
                                                         <td class="orden">${item.order}</td>
                                                        <td>${item.month}</td>
                                                        <td align="right">${item.postulants}</td>



                                                    </tr>   
                                                </c:forEach>  
                                            </c:if>
                                            <c:if test="${not empty graphicList3}">
                                                <c:forEach items="${graphicList3}" var="item" begin="0">

                                                    <tr> 
                                                        <td class="orden">${item.order}</td>
                                                        <td>${item.month}</td>
                                                        <td align="right">${item.postulants}</td>



                                                    </tr>   
                                                </c:forEach>  
                                            </c:if>
                                            <c:if test="${not empty graphicList4}">
                                                <c:forEach items="${graphicList4}" var="item" begin="0">

                                                    <tr> 
                                                         <td class="orden">${item.order}</td>
                                                        <td>${item.month}</td>
                                                        <td align="right">${item.postulants}</td>

                                                    </tr>   
                                                </c:forEach>  
                                            </c:if>
                                        </tbody> 
                                    </table>
                                </div>

                            </div>

                        </div>
                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" onclick="printGraphic()">Exportar PDF</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <!--Footer -->
        <div class="footer">
            <img src="resources/images/bg_banderin.jpg" width="100%"/> 
            <div class="clearfix"></div> 
        </div>
        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.buttons.min.js"></script>
        <script src="resources/js/buttons.flash.min.js"></script>
        <script src="resources/js/jszip.min.js"></script>
        <script src="resources/js/pdfmake.min.js"></script>
        <script src="resources/js/vfs_fonts.js"></script>
        <script src="resources/js/buttons.html5.min.js"></script>
        <script src="resources/js/buttons.print.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/bootstrap-confirmation.js"></script>
        <script src="resources/js/jquery.validate.min.js"></script>
        <script src="resources/js/tooltipster.bundle.js"></script>
        <script src="resources/js/custom-functionalities.js"></script>
        <script src="resources/js/raphael.min.js"></script>
        <script src="resources/js/morris.js"></script>

        <script>

                            $(document).ready(function () {
                                
                                if($("#activateBtn").val() === ""){
                                    $(".desableBtn").attr("disabled", "desabled");
                                } else {
                                    $(".desableBtn").removeAttr("disabled");
                                }
                                
                                $('.orden').hide();
                                
                                activaTab('consultaConsolidadoPostulantes');
                                if ('${tipoConsulta}' == 1) {
                                    activaTab('consultaConsolidadoPostulantes');
                                }
                                if ('${tipoConsulta}' == 2) {
                                    activaTab('consultaDetalladaPostulantes');
                                }
                                if ('${tipoConsulta}' == 3) {
                                    activaTab('consultaConsolidadaBeneficiarios');
                                }
                                if ('${tipoConsulta}' == 4) {
                                    activaTab('consultaDetalladaBeneficiarios');
                                }
                                
                                if ('${tipoConsulta}' == 1) {
                                                                    $('.table').DataTable({
                                    "language": {
                                        "url": "resources/js/Spanish.json"
                                    },
                                    "initComplete": function (settings, json) {
                                        $(".dt-buttons").each(function (index) {
                                            console.log(index + ": " + $(this).text());
                                            if ($(this).find('.exportar').length === 0) {
                                                $(this).prepend('<label class="exportar">Exportar a:&nbsp;&nbsp;</label>');
                                            }
                                        });
                                    },
                                    "bFilter": true,
                                    "bInfo": false,
                                    dom: 'Blfrtip',
                                    buttons: [
                                        {
                                            extend: 'csv',
                                            fieldBoundary: '',
                                            text: '<i class="fa fa-file-text-o"></i>',
                                            titleAttr: 'CSV',
                                            exportOptions: {
                                                columns: [1, 2, 3, 4]
                                            }

                                        },
                                        {
                                            extend: 'excel',
                                            text: '<i class="fa fa-file-excel-o"></i>',
                                            titleAttr: 'EXCEL',
                                            exportOptions: {
                                                columns: [1, 2, 3, 4]
                                            }
                                        },
                                        {
                                            extend: 'pdf',
                                            text: '<i class="fa fa-file-pdf-o"></i>',
                                            titleAttr: 'PDF',
                                            exportOptions: {
                                                columns: [1, 2, 3, 4]
                                            }
                                        },
                                        {
                                            extend: 'print',
                                            text: '<i class="fa fa-files-o"></i>',
                                            titleAttr: 'COPY',
                                            exportOptions: {
                                                columns: [1, 2, 3, 4]
                                            }
                                        }
                                    ]

                                });
                                    
                                }
                                if ('${tipoConsulta}' == 2) {
                                                                    $('.table').DataTable({
                                    "language": {
                                        "url": "resources/js/Spanish.json"
                                    },
                                    "initComplete": function (settings, json) {
                                        $(".dt-buttons").each(function (index) {
                                            console.log(index + ": " + $(this).text());
                                            if ($(this).find('.exportar').length === 0) {
                                                $(this).prepend('<label class="exportar">Exportar a:&nbsp;&nbsp;</label>');
                                            }
                                        });
                                    },
                                    "bFilter": true,
                                    "bInfo": false,
                                    dom: 'Blfrtip',
                                    buttons: [
                                        {
                                            extend: 'csv',
                                            fieldBoundary: '',
                                            text: '<i class="fa fa-file-text-o"></i>',
                                            titleAttr: 'CSV',
                                            exportOptions: {
                                                columns: [1, 2]
                                            }

                                        },
                                        {
                                            extend: 'excel',
                                            text: '<i class="fa fa-file-excel-o"></i>',
                                            titleAttr: 'EXCEL',
                                            exportOptions: {
                                                columns: [1, 2]
                                            }
                                        },
                                        {
                                            extend: 'pdf',
                                            text: '<i class="fa fa-file-pdf-o"></i>',
                                            titleAttr: 'PDF',
                                            exportOptions: {
                                                columns: [1, 2]
                                            }
                                        },
                                        {
                                            extend: 'print',
                                            text: '<i class="fa fa-files-o"></i>',
                                            titleAttr: 'COPY',
                                            exportOptions: {
                                                columns: [1, 2]
                                            }
                                        }
                                    ]

                                });
                                    
                                }
                                if ('${tipoConsulta}' == 3) {
                                                                    $('.table').DataTable({
                                    "language": {
                                        "url": "resources/js/Spanish.json"
                                    },
                                    "initComplete": function (settings, json) {
                                        $(".dt-buttons").each(function (index) {
                                            console.log(index + ": " + $(this).text());
                                            if ($(this).find('.exportar').length === 0) {
                                                $(this).prepend('<label class="exportar">Exportar a:&nbsp;&nbsp;</label>');
                                            }
                                        });
                                    },
                                    "bFilter": true,
                                    "bInfo": false,
                                    dom: 'Blfrtip',
                                    buttons: [
                                        {
                                            extend: 'csv',
                                            fieldBoundary: '',
                                            text: '<i class="fa fa-file-text-o"></i>',
                                            titleAttr: 'CSV',
                                            exportOptions: {
                                                columns: [1, 2]
                                            }
                                        },
                                        {
                                            extend: 'excel',
                                            text: '<i class="fa fa-file-excel-o"></i>',
                                            titleAttr: 'EXCEL',
                                            exportOptions: {
                                                columns: [1, 2]
                                            }
                                        },
                                        {
                                            extend: 'pdf',
                                            text: '<i class="fa fa-file-pdf-o"></i>',
                                            titleAttr: 'PDF',
                                            exportOptions: {
                                                columns: [1, 2]
                                            }
                                        },
                                        {
                                            extend: 'print',
                                            text: '<i class="fa fa-files-o"></i>',
                                            titleAttr: 'COPY',
                                            exportOptions: {
                                                columns: [1, 2]
                                            }
                                        }
                                    ]

                                });
                                    
                                }
                                if ('${tipoConsulta}' == 4) {
                                                                    $('.table').DataTable({
                                    "language": {
                                        "url": "resources/js/Spanish.json"
                                    },
                                    "initComplete": function (settings, json) {
                                        $(".dt-buttons").each(function (index) {
                                            console.log(index + ": " + $(this).text());
                                            if ($(this).find('.exportar').length === 0) {
                                                $(this).prepend('<label class="exportar">Exportar a:&nbsp;&nbsp;</label>');
                                            }
                                        });
                                    },
                                    "bFilter": true,
                                    "bInfo": false,
                                    dom: 'Blfrtip',
                                    buttons: [
                                        {
                                            extend: 'csv',
                                            fieldBoundary: '',
                                            text: '<i class="fa fa-file-text-o"></i>',
                                            titleAttr: 'CSV',
                                            exportOptions: {
                                                columns: [1, 2]
                                            }
                                        },
                                        {
                                            extend: 'excel',
                                            text: '<i class="fa fa-file-excel-o"></i>',
                                            titleAttr: 'EXCEL',
                                            exportOptions: {
                                                columns: [1, 2]
                                            }
                                        },
                                        {
                                            extend: 'pdf',
                                            text: '<i class="fa fa-file-pdf-o"></i>',
                                            titleAttr: 'PDF',
                                            exportOptions: {
                                                columns: [1, 2]
                                            }
                                        },
                                        {
                                            extend: 'print',
                                            text: '<i class="fa fa-files-o"></i>',
                                            titleAttr: 'COPY',
                                            exportOptions: {
                                                columns: [1, 2]
                                            }
                                        }
                                    ]

                                });
                                    
                                }

                                $('#tablag').DataTable({
                                    "language": {
                                        "url": "resources/js/Spanish.json"
                                    },
                                    dom: 'lt'
                                });
            <c:if test="${not empty internalMsg}">
                                $('#creationModal').modal('show');
            </c:if>


                                $('#graphicModal').on('shown.bs.modal', function () {

                                    $(function () {

                                        var json = '<c:out value="${graphicData}" escapeXml="false"/>';
                                        var type = '<c:out value="${graphic.type}" escapeXml="false"/>';
                                        var highest = Math.max.apply(this,$.map($.parseJSON(json), function(o){ return o.postulants; })) + 10;                        
                                        var ykeys = [];
                                        var labels = [];
                                        var lineColors = [];
                                        var barColors = [];
            <c:forEach items="${yKeys}" var="yKey" begin="0">
                                        ykeys.push('<c:out value="${yKey}" escapeXml="false"/>');
            </c:forEach>
            <c:forEach items="${labels}" var="labels" begin="0">
                                        labels.push('<c:out value="${labels}" escapeXml="false"/>');
            </c:forEach>
            <c:forEach items="${lineColors}" var="lineColors" begin="0">
                                        lineColors.push('<c:out value="${lineColors}" escapeXml="false"/>');
            </c:forEach>
            <c:forEach items="${barColors}" var="barColors" begin="0">
                                        barColors.push('<c:out value="${barColors}" escapeXml="false"/>');
            </c:forEach>


                                        $("#graphicContainer").empty();
                                        $('#legend').empty();
                                        if (type === 'B') {
                                            var browsersChart = Morris.Bar({
                                                element: 'graphicContainer',
                                                data: jQuery.parseJSON(json),
                                                xkey: '<c:out value="${xKey}" escapeXml="false"/>',
                                                ykeys: ykeys,
                                                ymax: highest,
                                                labels: labels,
                                                stacked: true,
                                                hideHover: 'auto',
                                                barGap: 2,
                                                barColors: barColors,
                                                barSizeRatio: 0.75,
                                                xLabelAngle: 60,
                                                gridTextSize: 10,
                                                resize: true

                                            });
                                            browsersChart.options.data.forEach(function (labels, i) {
                                                var legendItem = $('<span></span>').text(browsersChart.options.labels[i]).prepend('<br><span>&nbsp;</span>');
                                                legendItem.find('span')
                                                        .css('backgroundColor', browsersChart.options.barColors[i])
                                                        .css('width', '20px')
                                                        .css('display', 'inline-block')
                                                        .css('margin', '5px');
                                                $('#legend').append(legendItem)
                                            });
                                        } else if (type === 'L') {
                                            var browsersChart = Morris.Line({
                                                element: 'graphicContainer',
                                                lineColors: lineColors,
                                                data: jQuery.parseJSON(json),
                                                xkey: '<c:out value="${xKey}" escapeXml="false"/>',
                                                ykeys: ykeys,
                                                ymax: 'auto',
                                                labels: labels,
                                                stacked: true,
                                                hideHover: 'auto',
                                                barGap: 4,
                                                barSizeRatio: 0.55,
                                                resize: true,
                                                xLabelAngle: 20,
                                                parseTime: false
                                            });
                                            browsersChart.options.data.forEach(function (labels, i) {
                                                var legendItem = $('<span></span>').text(browsersChart.options.labels[i]).prepend('<br><span>&nbsp;</span>');
                                                legendItem.find('span')
                                                        .css('backgroundColor', browsersChart.options.lineColors[i])
                                                        .css('width', '20px')
                                                        .css('display', 'inline-block')
                                                        .css('margin', '5px');
                                                $('#legend').append(legendItem);
                                            });
                                        } else {
                                            Morris.Donut({
                                                element: 'graphicContainer',
                                                data: jQuery.parseJSON(json),
                                                stacked: true,
                                                hideHover: 'auto',
                                                barGap: 4,
                                                barSizeRatio: 0.55,
                                                resize: true
                                            });
                                        }

                                    });
                                });
                            });
                            function activaTab(tab) {
                                $('.nav-tabs a[href="#' + tab + '"]').tab('show');
                            }
                            ;
                            //**  * Imprime el grafico en un pdf.

                            function printGraphic() {
                                var objeto = document.getElementById('graphicContainer2'); //obtenemos el objeto a imprimir
                                var ventana = window.open('', '_blank'); //abrimos una ventana vac�a nueva
                                ventana.document.write(objeto.innerHTML); //imprimimos el HTML del objeto en la nueva ventana
                                ventana.document.close(); //cerramos el documento
                                ventana.print(); //imprimimos la ventana
                                ventana.close(); //cerramos la ventana

                            }

                            function borraGrafico() {
                                $("#graphicContainer2").empty();
                                $(".desableBtn").attr("disabled", "desabled");
                            }


        </script>       
        <style>
            .buttons-csv{
                display: inline-block;
                background-image:url(resources/images/csv.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;

            }
            .buttons-csv span{
                opacity: 0;
            }
            .buttons-excel{
                display: inline-block;
                background-image:url(resources/images/excel.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;

            }
            .buttons-excel span{
                opacity: 0;
            }
            .buttons-pdf{
                display: inline-block;
                background-image:url(resources/images/pdf.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;

            }
            .buttons-pdf span{
                opacity: 0;
            }
            .buttons-print{
                display: inline-block;
                background-image:url(resources/images/print.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;


            }
            .buttons-print span{
                opacity: 0;

            }
            div.dt-buttons {
                float: right;
                margin-left:10px;
            }
            .graphic-legend > span {
                display: inline-block;
                margin-right: 25px;
                margin-bottom: 10px;
                font-size: 13px;


            }
            .graphic-legend > span:last-child {
                margin-right: 0;
            }

            #graphicContainer {
                max-height: 280px;
                margin-top: 20px;
                margin-bottom: 20px;


            }


            .graphic-legend > span > i {
                display: inline-block;
                width: 15px;
                height: 15px;
                margin-right: 7px;
                margin-top: -3px;
                vertical-align: middle;
                border-radius: 1px;
            }

            #browsers_chart {
                max-height: 280px;
                margin-top: 20px;
                margin-bottom: 20px;
            }

        </style>
    </body>
</html>
