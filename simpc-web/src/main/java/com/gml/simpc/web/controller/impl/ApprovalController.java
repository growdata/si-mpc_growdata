
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 **
 * Document: UserController.java
 * Created on: 2016/10/19, 02:14:58 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.gml.simpc.api.dto.TemplateDto;
import com.gml.simpc.api.entity.Approval;
import com.gml.simpc.api.entity.Institution;
import com.gml.simpc.api.entity.Session;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.entity.exeption.SystemException;
import com.gml.simpc.api.enums.ApprovalInstitutionStatus;
import com.gml.simpc.api.services.ApprovalService;
import com.gml.simpc.api.services.HeadquarterService;
import com.gml.simpc.api.services.InstitutionService;
import com.gml.simpc.api.services.UserService;
import com.gml.simpc.commons.annotations.FunctionalityInterceptor;
import com.gml.simpc.commons.mail.sender.Notificator;
import com.gml.simpc.commons.storage.velocity.VelocityExecutor;
import com.gml.simpc.web.controller.ControllerDefinition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_001_UNKNOW;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_015_TEMPLATE_FAIL;
import static com.gml.simpc.api.utilities.Util.FAILED_ALERT;
import static com.gml.simpc.api.utilities.Util.PARAMETER_APPROVAL_INSTITUTION;
import static com.gml.simpc.api.utilities.Util.PARAMETER_REJECT_INSTITUTION;
import static com.gml.simpc.api.utilities.Util.SESSION_USER;
import static com.gml.simpc.api.utilities.Util.SUCCESS_ALERT;

/**
 * Controlador para la ventana de manejo de Aprobacion de instituciones.
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Quilindo</a>
 */
@FunctionalityInterceptor(name = "aprobacion-instituciones.htm")
@Controller
public class ApprovalController implements ControllerDefinition {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(ApprovalController.class);

    /**
     * Formateador de fechas.
     */
    private final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

    /**
     * Servicio para aprobacion de instituciones; Servicio
     * de instituciones intermediario para la comunicaci&oacute;n con la
     * persistencia.
     */
    @Autowired
    private InstitutionService institutionService;
    /**
     * Servicio para adminisraci&oacute;n de sedes; Servicio de sedes
     * intermediario para la comunicaci&oacute;n con la persistencia.
     */
    @Autowired
    private HeadquarterService headquarterService;
    /**
     * Servicio para adminisraci&oacute;n de aprobaciones; Servicio de
     * aprobaciones
     * intermediario para la comunicaci&oacute;n con la persistencia.
     */
    @Autowired
    private ApprovalService approvalService;

    /**
     * Servicio de usuarios.
     */
    @Autowired
    private UserService userService;

    @Autowired
    private Notificator notificator;

    /**
     * M&eacute;todo que se ejecuta al abrir la ventana de index; Trae listas
     * necesarias para el funcionamiento de la ventana.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "aprobacion-instituciones",
        method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response) {
        LOGGER.info("Ejecutando metodo en aprobacion de instituciones ");
        ModelAndView modelAndView = new ModelAndView("aprobacion-instituciones");

        try {
            modelAndView.addObject("IntitutionList", institutionService.
                findByApprovalStatus(ApprovalInstitutionStatus.N));

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("finally de consultar en AprobacionController");
        }

        init(modelAndView, false);

        return modelAndView;
    }

    /**
     * M&eacute;todo que se ejecuta al abrir la venttana de index; Trae el
     * detalle necesario para mostrar en la ventana.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping("detalle-institucion-aprobar")
    public ModelAndView detail(HttpServletRequest request,
        HttpServletResponse response) {

        Long id = Long.parseLong(request.getParameter("id"));
        Institution parent;

        ModelAndView modelAndView =
            new ModelAndView("detalle-institucion-aprobar");

        try {
            parent = this.institutionService.findById(id);
            if (parent.getQualityCertificateExpiration() != null) {
                parent.setDateToShow(format.format(parent.
                    getQualityCertificateExpiration()));
            }
            modelAndView.addObject("parentInstitution", parent);
            modelAndView.addObject("Headquarter",
                this.headquarterService.findByInstitution(
                    parent));
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("finally de display Aprobacion");
        }

        init(modelAndView, false);

        return modelAndView;

    }

    /**
     * Necesario para actualizar la institucion.
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("aprobacion")
    public ModelAndView approveIntitution(HttpServletRequest request,
        HttpServletResponse response) {

        ModelAndView modelAndView = new ModelAndView("aprobacion-instituciones");
        Session session = (Session) request.getSession().getAttribute(
            SESSION_USER);
        try {

            User userSession = session.getUser();
            Long idInstitution = Long.
                parseLong(request.getParameter("valorId"));
            String institutionEmail=request.getParameter("email");
             String iName=request.getParameter("name");

            this.institutionService.activateInstitution("A", "A", userSession.
                getId(), new Date(), idInstitution);

            modelAndView.addObject("IntitutionList", institutionService.
                findByApprovalStatus(ApprovalInstitutionStatus.N));
            String MENSAJE_APROBACION_INSTITUCION = getProcessedMessage(
                idInstitution + "",userSession.getFirstName().concat("  ").concat(userSession.getFirstSurname()));

            
            for (User usuario : this.userService.findByAdmin(true)) {
                notificator.sendNotification(usuario.getEmail(),
                    MENSAJE_APROBACION_INSTITUCION,usuario.getFirstName().
                    concat(" ").concat(usuario.getFirstSurname()),
                    PARAMETER_APPROVAL_INSTITUTION);

            }
            notificator.sendNotification(userSession.getEmail(),
                MENSAJE_APROBACION_INSTITUCION,userSession.getFirstName().
                concat(" ").concat(userSession.getFirstSurname()),
                PARAMETER_APPROVAL_INSTITUTION);

            notificator.sendNotification(institutionEmail,
                MENSAJE_APROBACION_INSTITUCION,iName,PARAMETER_APPROVAL_INSTITUTION);

            modelAndView.addObject("msg", "Se ha actualizado la institución " +
                "exitosamente. Se ha enviado CORREO de confirmacion");
            modelAndView.addObject("msgType", SUCCESS_ALERT);
        } catch (SystemException t) {
            LOGGER.error("Error ", t);
            modelAndView.addObject("msg",
                "No se puedo enviar el correo de confirmacion,Esto puede" +
                "deberse en la configuracion de la plantilla. Por favor" +
                "contacte al administrador");
            modelAndView.addObject("msgType", FAILED_ALERT);
        } catch (Exception uEx) {
            LOGGER.error("Error ", uEx);
            modelAndView.addObject("internalMsg", uEx.getMessage());
            modelAndView.addObject("msgType", FAILED_ALERT);
        } finally {
            LOGGER.info("finally save in ApprovalController");
        }

        init(modelAndView, false);
        return modelAndView;

    }

    /**
     * M&eacute;todo que permite rechazar una institucion.
     * 
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = "rechazar",
        method = RequestMethod.POST)
    public ModelAndView refuse(HttpServletRequest request,
        HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView("aprobacion-instituciones");

        Session session = (Session) request.getSession().getAttribute(
            SESSION_USER);
        try {

            User userSession = session.getUser();
            Long idInstitution =Long.parseLong(request.getParameter("valorId"));
            String iName=request.getParameter("name");
            String institutionEmail=request.getParameter("email");

            this.institutionService.updateApprovalState('R', userSession.getId(),
                        new Date(), Long.parseLong(request.getParameter("valorId")));
            String rejection = request.getParameter("message-text");
            Approval approval = new Approval(idInstitution,userSession,
                new Date(), rejection);

            this.approvalService.save(approval);

            modelAndView.addObject("msg", "Se ha  RECHAZADO la  institución " +
                         iName+". Se ha enviado un CORREO de informacion");
            modelAndView.addObject("msgType", SUCCESS_ALERT);
            modelAndView.addObject("IntitutionList", institutionService.
                findByApprovalStatus(ApprovalInstitutionStatus.N));

            String MENSAJE_RECHAZO_INSTITUCION = getProcessedMessage(
             idInstitution.toString(),userSession.getFirstName().concat("  ").concat(userSession.getFirstSurname()));
            
            for (User usuario : this.userService.findByAdmin(true)) {
            notificator.sendNotification(usuario.getEmail(),
                MENSAJE_RECHAZO_INSTITUCION,usuario.getFirstName().concat(" ").
                concat(usuario.getFirstSurname()),PARAMETER_REJECT_INSTITUTION);
            }
            notificator.sendNotification(userSession.getEmail(),
                MENSAJE_RECHAZO_INSTITUCION,userSession.getFirstName().concat(" ").
                concat(userSession.getFirstSurname()),PARAMETER_REJECT_INSTITUTION);
            notificator.sendNotification(institutionEmail,
            MENSAJE_RECHAZO_INSTITUCION,iName,PARAMETER_REJECT_INSTITUTION);
        } catch (Exception uEx) {
            LOGGER.error("Error ", uEx);
            modelAndView.addObject("msg",
                "Error en la creacion del correo con la plantilla configurada");
            modelAndView.addObject("msgType", FAILED_ALERT);
        } finally {
            LOGGER.info("finally save in ApprovalController");
        }

        return modelAndView;
    }

    /**
     * Recibe los datos de configuracion del mensaje.
     * 
     * @param idInstitution
     * @param iname
     * @return 
     */
    private String getProcessedMessage(String idInstitution,String iname) {
        String message;

        Institution theInstitution = institutionService.
            findById(Long.parseLong(idInstitution));

        String causal = "";
        String template;

        if (theInstitution.getApprovalStatus().getCode() == 'R') {
            template = approvalService.findTemplate().getRefuseTemplate();
            List cases = this.approvalService.findByInstitution(
                theInstitution);
            Approval lastcase = (Approval) cases.get(cases.size() - 1);
            causal = lastcase.getRejectionCause();

        } else {
            template = approvalService.findTemplate().getApprovalTemplate();
        }

        Map<String, Object> params = new HashMap<>();
        TemplateDto information = new TemplateDto(theInstitution, causal,
            this.headquarterService.findByInstitution(theInstitution));
        params.put("datos", information);
        
        params.put("aprobadopor",iname);
        
        
        try {
            message = VelocityExecutor.applyTemplate(template, params);
        } catch (Exception e) {
            LOGGER.error("Error", e);

            throw new SystemException(ERR_015_TEMPLATE_FAIL);
        }
        
        return message;
    }

    /**
     * Inicia los objetos para el controler.
     * 
     * @param modelAndView
     * @param creationError 
     */
    private void init(ModelAndView modelAndView, boolean creationError) {
        if (creationError) {
            modelAndView.addObject("listaIntituciones",
                "Error al cargar las Instituciones");
        }
        LOGGER.info(" INIT aprobacion de instituciones ");
    }
}
