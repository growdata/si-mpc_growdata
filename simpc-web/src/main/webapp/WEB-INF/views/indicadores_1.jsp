<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css/morris.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
    </head>
    <body>
        <c:if test="${not empty msg}">
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        <h1>Indicadores</h1>
        
        <c:if test="${not empty indicatorList}">    
            <div class="panel panel-default">
                <div class="panel-heading"><h2>B&uacute;squeda</h2></div>
                <div class="panel-body">
                    <legend>
                        <form method="get" action="/masterdata/filtrosIndicadores.htm"> 
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="indicador" class=" control-label">Indicador</label>
                                    <select class="form-control" id="indicador" onchange="onSelectIndicator()">
                                        <c:if test="${not empty indicatorList}">
                                            <c:forEach var="item" items="${indicatorList}">
                                                <option value="${item.id}">${item.name}</option>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="tipoGrafico" class="control-label">Tipo de Gr&aacute;fico</label>
                                    <select class="form-control" id="tipoGrafico" onchange="onSelectGraphicType()">
                                        <c:if test="${not empty indicatorList}">
                                            <c:forEach var="item" items="${indicatorList}" varStatus="iter">
                                                <c:if test="${iter.index == 0}">
                                                    <c:forEach var="item2" items="${item.types}"
                                                               varStatus="iter2">
                                                        <c:if test="${iter2.index == 0}">
                                                            <option value="${item2}" selected="true">${item2.label}</option>
                                                        </c:if>
                                                        <c:if test="${iter2.index != 0}">
                                                            <option value="${item2}">${item2.label}</option>
                                                        </c:if>
                                                    </c:forEach>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="grafico" class="control-label">Gr&aacute;fico</label>
                                    <select class="form-control" id="grafico" name="grafico">
                                        <c:if test="${not empty indicatorList}">
                                            <c:forEach var="item" items="${indicatorList}" varStatus="iter">
                                                <c:if test="${iter.index == 0}">
                                                    <c:forEach var="item2" items="${item.types}"
                                                               varStatus="iter2">
                                                        <c:if test="${iter2.index == 0}">
                                                            <c:forEach var="item3" items="${item.graphics}">
                                                                <c:if test="${item3.type == item2}">
                                                                    <option value="${item3.id}">${item3.name}</option>
                                                                </c:if>
                                                            </c:forEach>
                                                        </c:if>
                                                    </c:forEach>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="col-sm-offset-4 col-sm-8" style="text-align:right">
                                <button type="submit" class="btn btn-success">Iniciar</button>
                            </div>
                        </form>
                    </legend>
                    <div class="clearfix"></div>
                </div>
            </div>
        </c:if>
        
        <c:if test="${not empty graphic}">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Filtros</h2></div>
                <div class="panel-body">
                    <legend>
                        <form:form method="get" modelAttribute="periodDto" action="/masterdata/obtenerInfoIndicador.htm">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="hidden" name="grafico" value="${graphic.id}" />
                                    <label class="control-label">Indicador</label>
                                    <p class="form-control-static">${graphic.indicator.name}</p>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Gr&aacute;fico</label>
                                    <p class="form-control-static">${graphic.name}</p>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="anoInicio" class="control-label">A&ntilde;o Inicio</label>
                                    <form:select class="form-control" id="anoInicio" path="startYear">
                                        <c:forEach var="item" items="${years}">
                                            <option value="${item}">${item}</option>
                                        </c:forEach>
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="mesInicio" class="control-label">Mes Inicio</label>
                                    <form:select class="form-control" id="grafico" path="startMonth">
                                        <option value="1">Enero</option>
                                        <option value="2">Febrero</option>
                                        <option value="3">Marzo</option>
                                        <option value="4">Abril</option>
                                        <option value="5">Mayo</option>
                                        <option value="6">Junio</option>
                                        <option value="7">Julio</option>
                                        <option value="8">Agosto</option>
                                        <option value="9">Septiembre</option>
                                        <option value="10">Octubre</option>
                                        <option value="11">Noviembre</option>
                                        <option value="12">Diciembre</option>
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <c:if test="${graphic.numberOfPeriods > 1}">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="anoFin" class="control-label">A&ntilde;o Fin</label>
                                        <form:select class="form-control" id="anoFin" path="endYear">
                                            <c:forEach var="item" items="${years}">
                                                <option value="${item}">${item}</option>
                                            </c:forEach>
                                        </form:select>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="mesFin" class="control-label">Mes Fin</label>
                                        <form:select class="form-control" id="grafico" path="endMonth">
                                            <option value="1">Enero</option>
                                            <option value="2">Febrero</option>
                                            <option value="3">Marzo</option>
                                            <option value="4">Abril</option>
                                            <option value="5">Mayo</option>
                                            <option value="6">Junio</option>
                                            <option value="7">Julio</option>
                                            <option value="8">Agosto</option>
                                            <option value="9">Septiembre</option>
                                            <option value="10">Octubre</option>
                                            <option value="11">Noviembre</option>
                                            <option value="12">Diciembre</option>
                                        </form:select>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </c:if>
                            
                            <div class="clearfix"></div>

                            <div class="col-sm-offset-4 col-sm-8" style="text-align:right">
                                <button type="submit" class="btn btn-success">Buscar</button>
                            </div>
                        </form:form>
                    </legend>
                </div>
            </div>
        </c:if>
        
        <c:if test="${not empty tableData}">
            <br/>
            <div class="clearfix"></div>
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Informaci&oacute;n del indicador</h2></div>
                <div class="panel-body">
                    <button type="button" class="btn btn-primary btn-sm pull-right"
                            data-toggle="modal" data-target="#graphicModal">  Ver gr&aacute;ficos</button>
                    <br/>
                    <br/>
                    <div class=" table-responsive">
                        <table id="institutions" class="table table-striped table-bordered" 
                               cellspacing="0">
                            <thead>
                                <tr>
                                    <c:forEach var="item" items="${filter.titles}">
                                        <th>${item}</th>
                                    </c:forEach>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <c:forEach var="item" items="${filter.titles}">
                                        <th>${item}</th>
                                    </c:forEach>
                                </tr>
                            </tfoot>
                            <tbody>
                                <c:forEach var="item" items="${tableData}">
                                    <tr>
                                        <c:forEach var="item2" items="${filter.titles}" varStatus="iter">
                                            <td>${item.getColumn(iter.index)}</td>
                                        </c:forEach>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </c:if>
            
            <div class="modal fade" id="graphicModal" tabindex="-1" role="dialog" aria-labelledby="graphicModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="graphicModalLabel">${graphic.indicator.name}</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="graphicContainer" style="height: 100%; width: 100%"></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" onclick="printGraphic()">Exportar PDF</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
            
        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
		<script src="resources/js/dataTables.buttons.min.js"></script>
        <script src="resources/js/buttons.flash.min.js"></script>
        <script src="resources/js/jszip.min.js"></script>
        <script src="resources/js/pdfmake.min.js"></script>
        <script src="resources/js/vfs_fonts.js"></script>
        <script src="resources/js/buttons.html5.min.js"></script>
        <script src="resources/js/buttons.print.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/raphael.min.js"></script>
        <script src="resources/js/morris.js"></script>
        <script src="resources/js/xepOnline.js"></script>
        <script>
            $(document).ready(function () {
                $('#institutions').DataTable({
                    "language": {
                        "url": "resources/js/Spanish.json"
                    }, "initComplete": function (settings, json) {
                        $(".dt-buttons").each(function (index) {
                            console.log(index + ": " + $(this).text());
                            if ($(this).find('.exportar').length === 0) {
                                $(this).prepend('<label class="exportar">Exportar a:&nbsp;&nbsp;</label>');
                            }
                        });
                    },
                    "bFilter": false,
                    "bInfo": false,
					dom: 'Bfrtip',
					buttons: [
						  {
                extend:    'print',
                text:      '<i class="fa fa-files-o"></i>',
                titleAttr: 'COPY'
            },
            {
                extend:    'excel',
                text:      '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'EXCEL'
            },
            {
                extend:    'csv',
                text:      '<i class="fa fa-file-text-o"></i>',
                titleAttr: 'CSV'
            },
            {
                extend:    'pdf',
                text:      '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF'
            }
					]
                });
                
                $('#graphicModal').on('shown.bs.modal', function () {
                    $(function () {
                        alert('<c:out value="${graphicData}" escapeXml="false"/>');
                        var json = '<c:out value="${graphicData}" escapeXml="false"/>';
                        var type = '<c:out value="${graphic.type}" escapeXml="false"/>';

                        $("#graphicContainer").empty();

                        if (type === 'B') {
                            Morris.Bar({
                                element: 'graphicContainer',
                                data: jQuery.parseJSON(json),
                                xkey: 'axisX',
                                ykeys: ['axisY'],
                                labels: ['A�os'],
                                stacked: true, 
                                resize: true
                            });
                        } else if (type === 'L') {
                            Morris.Line({
                                element: 'graphicContainer',
                                data: jQuery.parseJSON(json),
                                xkey: 'axisX',
                                ykeys: ['axisY'],
                                labels: ['A�os'],
                                stacked: true, 
                                resize: true
                            });
                        } else {
                            Morris.Donut({
                                element: 'graphicContainer',
                                data: jQuery.parseJSON(json),
                                stacked: true, 
                                resize: true
                            });
                        }
                    });
                });
            });
            
            /**
            * Imprime el grafico en un pdf.
            */
            function printGraphic() {
                xepOnline.Formatter.Format('graphicContainer',{
                    render: 'download', 
                    srctype: 'svg',
                    pageWidth: '9.5in'
                });
            }
            
            /**
             * Se ejecuta cuando se selecciona un indicador.
             * @returns {undefined}
             */
            function onSelectIndicator() {
                jQuery("#tipoGrafico").find('option').remove();
                
                <c:if test="${not empty indicatorList}">
                    <c:forEach var="item" items="${indicatorList}">
                        var id = "<c:out value="${item.id}"/>";
                        
                        if (id === $('#indicador').val()) {
                            <c:forEach var="item2" items="${item.types}"
                                       varStatus="iter2">
                                <c:if test="${iter2.index == 0}">
                                    jQuery("<option>").attr("value", '${item2}').
                                        text('${item2.label}').attr("selected", "true").
                                        appendTo("#tipoGrafico");
                                </c:if>
                                <c:if test="${iter2.index != 0}">
                                    jQuery("<option>").attr("value", '${item2}').
                                        text('${item2.label}').appendTo("#tipoGrafico");
                                </c:if>
                            </c:forEach>
                        }
                    </c:forEach>
                </c:if>
                    
                onSelectGraphicType();
            }
            
            /**
             * Se ejecuta cuando se selecciona un tipo de gr&aacute;fica.
             * @returns {undefined}
             */
            function onSelectGraphicType() {
                jQuery("#grafico").find('option').remove();
                
                <c:if test="${not empty indicatorList}">
                    <c:forEach var="item" items="${indicatorList}" varStatus="iter">
                        var id = "<c:out value="${item.id}"/>";
                        
                        if (id === $('#indicador').val()) {
                            <c:forEach var="item2" items="${item.types}"
                                       varStatus="iter2">
                                var gId = "<c:out value="${item2}"/>";
                        
                                if (gId === $('#tipoGrafico').val()) {
                                    <c:forEach var="item3" items="${item.graphics}">
                                        <c:if test="${item3.type == item2}">
                                            jQuery("<option>").attr("value", '${item3.id}').
                                                text('${item3.name}').appendTo("#grafico");
                                        </c:if>
                                    </c:forEach>
                                }
                            </c:forEach>
                        }
                    </c:forEach>
                </c:if>
            }
        </script>
        <style>
            .buttons-csv{
                display: inline-block;
                background-image:url(resources/images/csv.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-csv span{
                opacity: 0;
            }
            .buttons-excel{
                display: inline-block;
                background-image:url(resources/images/excel.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-excel span{
                opacity: 0;
            }
            .buttons-pdf{
                display: inline-block;
                background-image:url(resources/images/pdf.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-pdf span{
                opacity: 0;
            }
            .buttons-print{
                display: inline-block;
                background-image:url(resources/images/print.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-print span{
                opacity: 0;
            }
            
            div.dt-buttons {
                float: right;
                margin-left:10px;
            }
        </style>
    </body>
</html>
