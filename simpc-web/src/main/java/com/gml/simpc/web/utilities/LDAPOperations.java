/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: LDAPOperations.java
 * Created on: 2017/02/14, 11:06:49 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.utilities;

import java.util.List;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.AuthenticationException;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.stereotype.Service;

import static org.springframework.ldap.query.LdapQueryBuilder.query;

/**
 * Clase contenedora de operaciones con el LDAP.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Service
public class LDAPOperations {

    /**
     * Logger de la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(LDAPOperations.class);
    /**
     * Plantilla de Spring para LDAP.
     */
    @Autowired
    private LdapTemplate ldapTemplate;

    /**
     * Verifica si un usuario existe en el directorio activo.
     *
     * @param username
     *
     * @return
     */
    public final boolean userExist(String username) {
        List<String> userList = this.ldapTemplate.search(query().
            where("sAMAccountName").is(username), new AttributesMapper<String>() {

            @Override
            public String mapFromAttributes(Attributes attrs)
                throws NamingException {
                Attribute attr = attrs.get("uid");

                return attr.get().toString();
            }
        });

        return !userList.isEmpty();
    }

    /**
     * Realiza autenticaci&oacute;n contra el directorio activo.
     *
     * @param username
     * @param password
     *
     * @return
     */
    public final boolean authenticate(String username, String password) {
        try {
            this.ldapTemplate.authenticate(query().where("sAMAccountName").
                is(username), password);

            return true;
        } catch (AuthenticationException ex) {
            LOGGER.info("CREDENCIALES INVALIDAS.", ex);
        } catch (Exception ex) {
            LOGGER.error("Error", ex);
        }

        return false;
    }
}
