
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: LoadResultDto.java
 * Created on: 2017/12/23, 04:21:39 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * Objective: to provide actions before and after the execution of batch jobs.
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.listener;
/**
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica Jim&eacute;nez Rozo</a>
 */
import org.apache.log4j.Logger;
import org.springframework.batch.core.SkipListener;
import org.springframework.batch.item.file.FlatFileParseException;

public class CustomSkipListener implements SkipListener<Object, Object> {
    
     /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(CustomSkipListener.class);

    @Override
    public void onSkipInRead(Throwable t) {
        StringBuilder message = new StringBuilder("ERROR en LECTURA: ");

        if (t instanceof FlatFileParseException) {
            message.append("Registro ")
                    .append(((FlatFileParseException)t).getLineNumber())
                    .append(": Error de formato para la siguiente entrada: ")
                    .append(((FlatFileParseException)t).getInput());
        } else {
            message.append(t.getMessage());
        }
        
        LOGGER.info(message.toString());
    }

    @Override
    public void onSkipInWrite(Object item, Throwable t) {
        StringBuilder message = new StringBuilder("ERROR en ESCRITURA: ").append(t.getMessage());
        
        LOGGER.info(message.toString());

    }

    @Override
    public void onSkipInProcess(Object item, Throwable t) {
        StringBuilder message = new StringBuilder("ERROR en PROCESADO: ").append(t.getMessage());
        
        LOGGER.info(message.toString());
    }
}
