/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ValidateCommand.java
 * Created on: 2017/01/18, 04:28:57 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.validator;

import com.gml.simpc.loader.dto.ValidationErrorDto;
import java.util.List;

/**
 * Clase para implementar commando de validacion de negocio en carges de
 * información
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica
 * Jim&eacute;nez Rozo</a>
 */
public interface ValidateCommand {

    /**
     * Realiza la validación.
     *
     * @param loadId
     *
     * @return
     */
    List<ValidationErrorDto> validate(Long loadId);

    /**
     * Verifica si es del tipo que le corresponde.
     *
     * @param loadType
     *
     * @return
     */
    boolean verify(String loadType);
}
