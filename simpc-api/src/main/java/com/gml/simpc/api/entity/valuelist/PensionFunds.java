/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: PensionFunds.java
 * Created on: 2017/03/01, 01:47:01 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity.valuelist;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
@Table(name = "FONDOSPENSIONES")
@Entity
public class PensionFunds implements Serializable {
    
    /**
     * Identificador de la entidad.
     */
    @Id
    @Column(name = "ID", length = 20)
    private Long id;
    /**
     * C&oacute;digo.
     */
    @Column(name = "CODIGO", length = 20)
        private String code;
    /**
     * Descripci&oacute;n
     */
    @Column(name = "NIT", length = 250)
    private String nit;

    /**
     * Descripci&oacute;n
     */
    @Column(name = "VALOR", length = 250)
    private String value;

    public PensionFunds(long id, String code, String nit, String value) {
        this.id = id;
        this.code = code;
        this.nit = nit;
        this.value = value;
    }

    public PensionFunds() {
    }


    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getNit() {
        return nit;
    }

    public String getValue() {
        return value;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public void setValue(String value) {
        this.value = value;
    }

    
    
}
