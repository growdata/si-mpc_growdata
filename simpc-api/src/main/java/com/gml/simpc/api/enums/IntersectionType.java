/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IntersectionType.java
 * Created on: 2017/02/08, 10:38:38 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.enums;

/**
 * Enumeraci&oacute;n que define los tipos de cruces.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public enum IntersectionType {

    /**
     * Cruces individuales y masivos.
     */
    I('I', "Individual"),
    M('M', "Masivo");

    /**
     * C&oacute;digo del estado.
     */
    private final char code;
    /**
     * Texto del estado.
     */
    private final String label;

    private IntersectionType(char code, String label) {
        this.code = code;
        this.label = label;
    }

    public char getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }

    /**
     * Devuelve el status por c&oacute;digo.
     *
     * @param code
     *
     * @return
     */
    public static IntersectionType getByCode(char code) {
        IntersectionType[] allValues = values();

        for (IntersectionType status : allValues) {
            if (status.getCode() == (code)) {
                return status;
            }
        }

        throw new IllegalArgumentException("C�digo [ " + code +
            " ] no se encuentra registrado.");
    }

    @Override
    public String toString() {
        return label;
    }

}
