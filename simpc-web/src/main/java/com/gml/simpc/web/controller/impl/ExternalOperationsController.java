/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ExternalOperationsController.java
 * Created on: 2017/02/21, 04:11:44 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gml.simpc.api.dto.ExternalLoginDto;
import com.gml.simpc.api.dto.ResponseDto;
import com.gml.simpc.api.entity.DesktopSession;
import com.gml.simpc.api.entity.HistoryAccess;
import com.gml.simpc.api.entity.LoadProcess;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.entity.exeption.UserException;
import com.gml.simpc.api.entity.exeption.code.ErrorCode;
import com.gml.simpc.api.enums.ProcessStatus;
import com.gml.simpc.api.enums.ProfileStatus;
import com.gml.simpc.api.enums.RequestOrigin;
import com.gml.simpc.api.services.DesktopSessionService;
import com.gml.simpc.api.services.HistoryAccessService;
import com.gml.simpc.api.services.UserService;
import com.gml.simpc.api.utilities.Util;
import com.gml.simpc.web.batch.LoadService;
import com.gml.simpc.web.utilities.LDAPOperations;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static com.gml.simpc.api.utilities.Util.LOAD_SUCCESS_MESSAGE;

/**
 * Controlador para operaciones externas.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@RestController
public class ExternalOperationsController {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(ExternalOperationsController.class);
    /**
     * Nit del ministerio de trabajo.
     */
    private static final String NIT_MINTRABAJO = "830115226";
    /**
     * Nit del ministerio de trabajo con d&iacute;gito de verificaci&oacute;n.
     */
    private static final String NIT_MINTRABAJO_AND_DV = NIT_MINTRABAJO + "-3";
    /**
     * Servicio para adminisraci&oacute;n de usuarios.
     */
    @Autowired
    private UserService userService;
    /**
     * Servicio de carga.
     */
    @Autowired
    private LoadService loadService;
    /**
     * Plantilla LDAP.
     */
    @Autowired
    private LDAPOperations ldapOperations;
    /**
     * Servicio de log de accesos.
     */
    @Autowired
    private HistoryAccessService historyAccessService;
    /**
     * Servicio de sesiones de escritorio.
     */
    @Autowired
    private DesktopSessionService desktopSessionService;
    /**
     * Formateador de fechas.
     */
    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
    /**
     * Formateador timestamp.
     */
    private final SimpleDateFormat timestampFormatter = 
        new SimpleDateFormat("yyyyMMddHHmmssSSSSSS");

    /**
     * M&eacute;todo para realizar Login desde afuera de la aplicaci&oacute;n.
     *
     * @param user
     * @return 
     */
    @RequestMapping(value = "/login-externo", method = RequestMethod.POST,
        headers = "Accept=*/*")
    public String externalLogin(
        @RequestBody ExternalLoginDto user
    ) {
        LOGGER.info("Ejecutando metodo [ externalLogin ]");

        ResponseDto dto = new ResponseDto();

        try {
            User userdb = this.userService.
                findByUsernameOrEmail(user.getUsername(),
                    user.getUsername());

            boolean userExist;

            if (userdb != null &&
                (NIT_MINTRABAJO.equals(userdb.getCcf().getNit()) ||
                NIT_MINTRABAJO_AND_DV.equals(userdb.getCcf().getNit()))) {
                userExist = this.ldapOperations.
                   authenticate(user.getUsername(), user.getPassword());
                LOGGER.info("CUAL USUARIO EVALUA "+user.getUsername());
            } else {
                userdb = this.userService.searchUserLogin(user.getUsername(),
                    user.getUsername(), user.getPassword(), 
                    ProfileStatus.A.name());

                userExist = (userdb != null);
            }

            if (userExist) {
                LOGGER.info("Usuario existe");

                saveAccessLog(userdb);
                DesktopSession session = createSession(userdb);
                dto.setOk(true);
                dto.setSession(session);
            } else {
                dto.setMessage("Inicio de sesi�n fallido. Las credenciales no" +
                    " coinciden.");
            }

            LOGGER.info("Usuario no existe");
            
        } catch (Exception ex) {
            LOGGER.info("Error [ externalLogin ]", ex);

            dto.setMessage(ErrorCode.ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("Finaliza el metodo executeLogin");
        }

        return getJsonObject(dto);
    }

    /**
     * M&eacute;todo para hacer una carga desde escritorio.
     *
     * @param token
     * @param key
     * @param inLoadProcess
     *
     * @return
     */
    @RequestMapping(value = "/carga-externa/{token}/{key}",
        method = RequestMethod.POST
    )
    public String externalCreation(
        @PathVariable("token") String token, @PathVariable("key") String key,
        @RequestBody LoadProcess inLoadProcess
    ) {
        LOGGER.info("Ejecutando metodo [ externalCreation ] para archivo " +
            inLoadProcess.toString());
        ResponseDto dto = new ResponseDto();

        try {
            LoadProcess loadProcess = inLoadProcess;
            DesktopSession session = this.desktopSessionService.
                findByUserToken(token);
            validateTokenAndKey(key, session);
            loadProcess.setConsultant(session.getUser());
            loadProcess = this.loadService.createProcess(loadProcess);

            LOGGER.info("Process created, id " + loadProcess.getId());

            if (loadProcess.getStatus() == ProcessStatus.ESTRUCTURA_VALIDADA) {
                loadProcess.setDetail(LOAD_SUCCESS_MESSAGE);
            }
            
            dto.setMessage(loadProcess.getDetail());
            dto.setOk(true);
        } catch (UserException ex) {
            LOGGER.info("Error [ externalCreation ] 1", ex);
            dto.setMessage(ex.getMessage());
        } catch (Exception ex) {
            LOGGER.info("Error [ externalCreation ] 2", ex);

            dto.setMessage(ErrorCode.ERR_001_UNKNOW.getMessage());
        }
        
        return getJsonObject(dto);
    }

    /**
     * Convierte un ResponseDto a Json.
     *
     * @param dto
     *
     * @return
     */
    private String getJsonObject(ResponseDto dto) {
        try {
            ObjectMapper mapper = new ObjectMapper();

            return mapper.writeValueAsString(dto);
        } catch (JsonProcessingException ex) {
            LOGGER.error("Error al convertir el objeto a json:", ex);
            
            return null;
        }
    }

    /**
     * Verifica si el token y el key son validos.
     *
     * @param key     Se construye con la fecha del d&iacute; de la
     *                petici&oacute;n y el id del usuario; se valida contra el
     *                d&icute;a de hoy y el de ayer por si hubo cambio de
     *                d&iaute;a.
     * @param session
     *
     * @throws UserException
     */
    private void validateTokenAndKey(String key, DesktopSession session)
        throws UserException {
        if (session == null) {
            throw new UserException(ErrorCode.ERR_044_SESSION_ERROR);
        }

        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        cal.add(Calendar.DATE, -1);
        Date yesterday = cal.getTime();

        String todayResult = getFormattedDate(today);
        String yesterdayResult = getFormattedDate(yesterday);

        String key1 = Util.calculateSha1(todayResult + "-" + session.getId());
        String key2 = Util.calculateSha1(yesterdayResult + "-" +
            session.getId());

        if (!key.equals(key1) && !key.equals(key2)) {
            throw new UserException(ErrorCode.ERR_044_SESSION_ERROR);
        }
    }

    /**
     * M&eacute;todo para guardar el log del acceso.
     */
    private void saveAccessLog(User user) {
        HistoryAccess historyAccess = new HistoryAccess();
        historyAccess.setUser(user);
        historyAccess.setAccessDate(new Date());
        historyAccess.setOrigin(RequestOrigin.DESKTOP);

        this.historyAccessService.save(historyAccess);
    }

    /**
     * Retorna la fecha formateada.
     *
     * @param date
     *
     * @return
     */
    private String getFormattedDate(Date date) {
        return this.formatter.format(date);
    }

    /**
     * Crea la sesi&oacute;n del usuario.
     *
     * @param userdb
     *
     * @return
     */
    private DesktopSession createSession(User userdb) {
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();

        String todayResult = this.timestampFormatter.format(today);
        String token = Util.calculateSha1(todayResult + "-" +
            userdb.getId());

        DesktopSession session = new DesktopSession();
        session.setUser(userdb);
        session.setUserDate(new Date());
        session.setUserToken(token);

        this.desktopSessionService.save(session);

        return session;
    }
}
