/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: BatchJobNames.java
 * Created on: 2016/10/26, 10:08:56 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.utilities;

import java.util.HashMap;
import java.util.Map;

/**
 * Constantes para la web en cuanto a nombres de jobs de carges de información.
 *
 * @author <a href="mailto:julianm@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
public class BatchJobNames {
    
    /**
     * Mapa de nombres para jobs de carga de información
     */
    private final Map<String, String> loadJobNames;
    
    /**
     * Mapa de nombres para jobs de carga a stage
     */
    private final Map<String, String> stageJobNames;
    
    /**
     * Mapa de nombres para jobs de carga a master data
     */
    private final Map<String, String> masterJobNames;
    
    
    private final Map<String,String>stageJobTableNames;

    public BatchJobNames() {
        
        this.loadJobNames = new HashMap<>();
        this.loadJobNames.put("CAP-1", "trainingImportFileJob");
        this.loadJobNames.put("CAP-2", "trainingAttendanceImportFileJob");
        this.loadJobNames.put("ORI-1", "orientationImportFileJob");
        this.loadJobNames.put("ORI-2", "orientationAttendanceImportFileJob");
        this.loadJobNames.put("PES-1", "fosfecImportFileJob");
        this.loadJobNames.put("PES-2", "dependentImportFileJob");
        this.loadJobNames.put("HDV-1", "basicInformationImportFileJob");
        this.loadJobNames.put("HDV-2", "educationLevelImportFileJob");
        this.loadJobNames.put("HDV-3", "informalEducationImportFileJob");
        this.loadJobNames.put("HDV-4", "workExperienceImportFileJob");
        this.loadJobNames.put("HDV-5", "languageImportFileJob");
        this.loadJobNames.put("HDV-6", "otherKnowledgeImportFileJob");
        this.loadJobNames.put("HDV-7", "intermediationImportFileJob");
        
        this.stageJobNames = new HashMap<>();
        this.stageJobNames.put("CAP-1", "trainingStageJob");
        this.stageJobNames.put("CAP-2", "trainingAttendanceStageJob");
        this.stageJobNames.put("ORI-1", "orientationStageJob");
        this.stageJobNames.put("ORI-2", "orientationAttendanceStageJob");
        this.stageJobNames.put("PES-1", "fosfecStageJob");
        this.stageJobNames.put("PES-2", "dependentStageJob");
        this.stageJobNames.put("HDV-1", "basicInformationStageJob");
        this.stageJobNames.put("HDV-2", "educationLevelStageJob");
        this.stageJobNames.put("HDV-3", "informalEducationStageJob");
        this.stageJobNames.put("HDV-4", "workExperienceStageJob");
        this.stageJobNames.put("HDV-5", "languageStageJob");
        this.stageJobNames.put("HDV-6", "otherKnowledgeStageJob");
        this.stageJobNames.put("HDV-7", "intermediationStageJob");
        
        this.masterJobNames = new HashMap<>();
        this.masterJobNames.put("CAP-1", "trainingMasterJob");
        this.masterJobNames.put("CAP-2", "trainingAttendanceMasterJob");
        this.masterJobNames.put("ORI-1", "orientationMasterJob");
        this.masterJobNames.put("ORI-2", "orientationAttendanceMasterJob");
        this.masterJobNames.put("PES-1", "fosfecMasterJob");
        this.masterJobNames.put("PES-2", "dependentMasterJob");
        this.masterJobNames.put("HDV-1", "basicInformationMasterJob");
        this.masterJobNames.put("HDV-2", "educationLevelMasterJob");
        this.masterJobNames.put("HDV-3", "informalEducationMasterJob");
        this.masterJobNames.put("HDV-4", "workExperienceMasterJob");
        this.masterJobNames.put("HDV-5", "languageMasterJob");
        this.masterJobNames.put("HDV-6", "otherKnowledgeMasterJob");
        this.masterJobNames.put("HDV-7", "intermediationMasterJob");
        
        this.stageJobTableNames = new HashMap<>();
        this.stageJobTableNames.put("CAP-1", "ST_CAP_PROGRAMA_MODULO");
        this.stageJobTableNames.put("CAP-2", "ST_PROGRAMA_DETALLE");
        this.stageJobTableNames.put("ORI-1", "ST_ORI_PROGRAMA_MODULO");
        this.stageJobTableNames.put("ORI-2", "ST_PROGRAMA_DETALLE");
        this.stageJobTableNames.put("PES-1", "ST_FOSFEC");
        this.stageJobTableNames.put("PES-2", "ST_DEPENDIENTES");
        this.stageJobTableNames.put("HDV-1", "ST_DATOS_BASICOS");
        this.stageJobTableNames.put("HDV-2", "ST_NIVEL_EDUCATIVO");
        this.stageJobTableNames.put("HDV-3", "ST_EDUCACION_INFORMAL");
        this.stageJobTableNames.put("HDV-4", "ST_EXPERIENCIA_LABORAL");
        this.stageJobTableNames.put("HDV-5", "ST_IDIOMAS");
        this.stageJobTableNames.put("HDV-6", "ST_OTROS_CONOCIMIENTOS");
        this.stageJobTableNames.put("HDV-7", "ST_GESTION_INTERMEDIACION");
        
    }

    public Map<String, String> getLoadJobNames() {
        return loadJobNames;
    }

    public Map<String, String> getStageJobNames() {
        return stageJobNames;
    }
    
    public Map<String, String> getMasterJobNames() {
        return masterJobNames;
    }

    public Map<String, String> getStageJobTableNames() {
        return stageJobTableNames;
    }
    
    
}
