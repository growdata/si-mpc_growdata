/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: GraphicDao.java
 * Created on: 2016/12/27, 04:06:52 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dao;

import com.gml.simpc.api.dto.GraphicDataDto;
import com.gml.simpc.api.dto.IndicatorFilterDto;
import com.gml.simpc.api.dto.TableDataDto;
import java.util.List;

/**
 * Interface que define el DAO para consultar informaci&oacute;n de los
 * indicadores.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface GraphicDao {

    /**
     * Obtiene la informaci&oacute;n para pintar los gr&aacute;ficos.
     *
     * @param filter
     *
     * @return
     */
    List<GraphicDataDto> getGraphicData(IndicatorFilterDto filter);

    /**
     * Obtiene la informaci&oacute;n para llenar las tablas que acompa�an los
     * gr&aacute;ficos.
     *
     * @param filter
     *
     * @return
     */
    List<TableDataDto> getTableData(IndicatorFilterDto filter);
}
