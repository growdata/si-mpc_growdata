<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "Master Data"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster.bundle.min.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster-sideTip-shadow.min.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
        
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
        <link rel="bootstrap.bundle.js">
    </head>
    <body>
        <c:if test="${not empty msg}">
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        <h1>Administraci�n de Perfiles</h1>
        <div class="panel panel-default">
            <div class="panel-heading"><h2>B�squeda</h2></div>
            <div class="panel-body">
                <legend>
                    <form:form method="get" action="/masterdata/buscarPerfiles.htm" modelAttribute="searchProfile">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="nombrePerfil" class="control-label">Nombre del Perfil</label>
                                <form:input type="text" class="form-control" id="nameC" 
                                            placeholder="Nombre del Perfil" maxlength="50"
                                            path="name"/>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="estado" class="control-label">Estado</label>
                                <form:select class="form-control" id="statusC" path="status">
                                    <option value="">Seleccione...</option>
                                    <c:if test="${not empty profileStatusList}">
                                        <c:forEach var="item" items="${profileStatusList}">
                                            <option value="${item}">${item.label}</option>
                                        </c:forEach>
                                    </c:if>
                                </form:select>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="estado" class="control-label">Funcionalidad</label>
                                <select class="form-control" id="functionalityId" name="functionalityId">
                                    <option value="">Seleccione...</option>
                                    <option value="14" >Portal de Cargas</option>
                                    <option value="15" >Cruces de Informaci�n</option>
                                    <option value="16" data-toggle="tooltip" data-placement="right" title="Registra las instituciones oferentes de capacitaci�n, asi como la creaci�n de sus respctivas sedes">Administraci�n de Instituciones</option>
                                    <option value="17" data-toggle="tooltip" data-placement="right" title="Aprueba instituciones con estado &quot;No Aprobada&quot;">Aprobaci�n de Instituciones</option>
                                    <option value="18" data-toggle="tooltip" data-placement="right" title="Muestra el listado de las instituciones registradas con estado &quot;Activa&quot;">Banco de Oferentes</option>
                                    <option value="19" data-toggle="tooltip" data-placement="right" title="Registra y consulta programas de capacitaci�n para la inserci�n y reinserci�n laboral">Administraci�n de Programas</option>
                                    <option value="20" data-toggle="tooltip" data-placement="right" title="Muestra el listado de accesos al sistema, por cada usuario registrado">Historial de Accesos</option>
                                    <option value="21" data-toggle="tooltip" data-placement="right" title="Muestra el listado de acciones ejecutadas en el sistema, por cada usuario registrado">Historial de Acciones Realizadas</option>
                                    <option value="22" data-toggle="tooltip" data-placement="right" title="Muestra el listado de programas y m�dulos cargados">Programas/M�dulos Registrados</option>
                                    <option value="23" data-toggle="tooltip" data-placement="right" title="">Reporte de Cargas</option>
                                    <option value="24" data-toggle="tooltip" data-placement="right" title="Genera indicadores, muestra informaci�n de indicador de acuerdo a su selecci�n">Indicadores</option>
                                    <option value="25" data-toggle="tooltip" data-placement="right" title="Almacena los archivos resultados de las consultas masivas e individuales">Historial de Cruces</option>
                                    <option value="26" data-toggle="tooltip" data-placement="right" title="Explica paso a paso como usar el aplicativo , explica las funciones de acceso para los usuarios">Manual de Usuario</option>
                                    <option value="27" data-toggle="tooltip" data-placement="right" title="Explica paso a paso como usar el aplicativo , explica las funciones todas las funciones del sistema">Manual de Administrador</option>
                                    <option value="28" data-toggle="tooltip" data-placement="right" title="Resuelve dudas de conceptos b�sicos del sistema, operaci�n b�sica de m�dulos implementados, y muestra respuestas a preguntas frecuentes">Ayudas en L�nea</option>
                                    <option value="29" data-toggle="tooltip" data-placement="right" title="Descarga archivo &quot;descargaJar.jar&quot;, para porterior ejecuci�n y cargue de archivos">Descargar MD Loader</option>
                                </select>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-offset-4 col-sm-8" style="text-align:right">
                            <button onclick="reinicioMsn()" type="button" 
                            class="btn btn-success">Nuevo</button>
                            <button type="submit" class="btn btn-success">Buscar</button>
                        </div>
                    </form:form>
                </legend>
                <div class="clearfix"></div>
            </div>
        </div>

        <br/>
        <div class="panel panel-default">
            <div class="panel-heading"><h2>Detalle Consulta Perfiles</h2></div>
            <div class="panel-body">
                <div class=" table-respontablesive">
                    <table id="perfiles" class="table table-striped table-bordered" 
                           cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nombre del Perfil</th>
                                <th>Descripci�n del Perfil</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Nombre del Perfil</th>
                                <th>Descripci�n del Perfil</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:if test="${not empty profilesList}">
                                <c:forEach var="item" items="${profilesList}">
                                    <tr>
                                        <td>${item.name}</td>
                                        <td>${item.description}</td>
                                        <td>${item.status.label}</td>
                                        <td>
                                            <c:if test="${item.adminStatus == 'F'}">
                                                <div align="center">
                                                    <div class="btn-group" role="group">
                                                        <button type="button" class="btn btn-info btn-xs"
                                                                onclick="loadProfileDetail(${item.id})">
                                                            <i class="fa fa-id-card-o" aria-hidden="true"></i> Ver
                                                        </button>
                                                    </div>
                                                    <div class="btn-group" role="group">
                                                        <button type="button" class="btn btn-warning btn-xs" onclick="editProfile(${item.id})">
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> 
                                                            Editar
                                                        </button>
                                                    </div>
                                                    <div class="btn-group" role="group">
                                                        <form:form method="post" action="/masterdata/borrarPerfil.htm"> 
                                                            <input type="hidden" name="id" value="${item.id}"/>
                                                            <button class="btn btn-danger btn-xs elimPerf" type="submit"
                                                                    data-btn-ok-label="Confirmar" data-btn-cancel-label="Cancelar"
                                                                    data-title="Est� seguro?" 
                                                                    data-toggle="confirmation" data-placement="top" 
                                                                    confirmation-callback  >
                                                                <i class="fa fa-times-circle-o" aria-hidden="true"></i>Eliminar
                                                            </button>
                                                        </form:form>
                                                    </div>
                                                </div>
                                            </c:if>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:if>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>         

        <div class="modal fade" id="creationModal" role="dialog" 
             aria-labelledby="creationModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="creationModalLabel">Registro de Perfil</h4>
                    </div>
                    <div class="modal-body">
                        <c:if test="${not empty internalMsg}">
                            <div id="internal" class="alert alert-${msgType} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-label="Close">
                                    <span aria-hidden="true">�</span>
                                </button>
                                <strong>${internalMsg}</strong>
                            </div>
                        </c:if>
                        <form:form id="creationProfile" modelAttribute="creationProfile" method="post" action="/masterdata/guardar-perfil.htm">
                            <div class="container">
                                <div class="alert alert-warning alert-dismissible" id="myAlert">
                                    <strong>�Atenci�n!</strong> Los usuarios que tengan asignado este perfil, no podr�n iniciar sesi�n.
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <h1>Datos de Perfil </h1>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="hidden" id="id" name="id" path="id"/>
                                    <label for="nombrePerfil" class="control-label">Nombre del Perfil(*)</label>
                                    <form:input type="text" class="form-control" id="name" maxlength="50"
                                                placeholder="Nombre del Perfil" required="required"
                                                path="name"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="estadoPerfil" class=" control-label">Estado(*)</label>
                                    <form:select onchange="cambioEstado()" class="form-control estadoPerfil" id="status" 
                                                 path="status" required="required">
                                        <option value="">Seleccione...</option>
                                        <c:if test="${not empty profileStatusList}">
                                            <c:forEach var="item" items="${profileStatusList}">
                                                <option value="${item}">${item.label}</option>
                                            </c:forEach>
                                        </c:if>
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="descripci�nPerfil" class="control-label">Descripci�n(*)</label>
                                    <form:input type="text" class="form-control" id="description" maxlength="80"
                                                placeholder="Descripci�n del Perfil" required="required"
                                                path="description"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="col-sm-12" style="text-align:right">
                                    <br/>
                                    <button id="saveButton" class="btn btn-success" type="button"
                                            data-btn-ok-label="Confirmar" data-btn-cancel-label="Cancelar"
                                            data-title="Est� seguro?" 
                                            data-toggle="confirmation" data-placement="top" 
                                            confirmation-callback >Guardar
                                    </button>
                                    <button type="button" onclick="resetForm()" 
                                            class="btn btn-danger" data-dismiss="modal">Cancelar
                                    </button>
                                </div>
                            </div>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="detailModal" role="dialog" 
             aria-labelledby="creationModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="creationModalLabel">Detalle de Perfil</h4>
                    </div>
                    <div class="modal-body" style="height: 80vh">
                        <iframe id="detailArea" width="100%" height="100%" style="border: 0">
                        </iframe>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.buttons.min.js"></script>
        <script src="resources/js/buttons.flash.min.js"></script>
        <script src="resources/js/jszip.min.js"></script>
        <script src="resources/js/pdfmake.min.js"></script>
        <script src="resources/js/vfs_fonts.js"></script>
        <script src="resources/js/buttons.html5.min.js"></script>
        <script src="resources/js/buttons.print.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/bootstrap-confirmation.js"></script>
        <script src="resources/js/jquery.validate.min.js"></script>
        <script src="resources/js/tooltipster.bundle.js"></script>
        <script src="resources/js/custom-functionalities.js"></script>
        <script>
            $(document).ready(function () {
                
                $(".elimPerf").each(function () {
                    $(this).confirmation();
                });
                

                $('#perfiles').DataTable({
                    "language": {
                        "url": "resources/js/Spanish.json"
                    }, "initComplete": function (settings, json) {
                        $(".dt-buttons").each(function (index) {
                            console.log(index + ": " + $(this).text());
                            if ($(this).find('.exportar').length === 0) {
                                $(this).prepend('<label class="exportar">Exportar a:&nbsp;&nbsp;</label>');
                            }
                        });
                    },
                    "bFilter": false,
                    "bInfo": false,
                    dom: 'Blfrtip',
                    buttons: [
                        {
                            extend: 'csv',
                            fieldBoundary: '',
                            footer: false,
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'CSV',
                            exportOptions: {
                                columns: [0, 1, 2]
                            }
                        },
                        {
                            extend: 'excel',
                            footer: false,
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'EXCEL',
                            exportOptions: {
                                columns: [0, 1, 2]
                            }
                        },
                        {
                            extend: 'pdf',
                            footer: false,
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'PDF',
                            exportOptions: {
                                columns: [0, 1, 2]
                            }
                        },
                        {
                            extend: 'print',
                            footer: false,
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'COPY',
                            exportOptions: {
                                columns: [0, 1, 2]
                            }
                        }
                    ]
                });

            <c:if test="${not empty internalMsg}">
                <c:if test="${not empty jsonProfile}">
                                            var json = jQuery.parseJSON('<c:out value="${jsonProfile}" escapeXml="false"/>');
                                            foundComponents(json, '');
                </c:if>
                                            $('#creationModal').modal('show');
                                            $('.container').hide();
            </c:if>
            <c:if test="${not empty creationProfile.id}">
                                            $('#creationModal').modal('show');
            </c:if>

                                        });

                                        function loadProfileDetail(id) {
                                            
                                            $('#detailModal').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                            
                                            $('#detailArea').attr("src",
                                                    'detalle-consulta-perfiles.htm?id=' + id);
                                        }

                                        function reinicioMsn() {
                                            
                                            $('#creationModal').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                            
                                            $('.container').hide();
                                            $('#internal').hide();

                                        }

                                        function cambioEstado() {
                                            valor = $(".estadoPerfil").val();
                                            if (valor === "I") {
                                                $('.container').show("fast");
                                            } else {
                                                $('.container').hide("fast");
                                            }
                                        }

                                        function resetForm() {
                                            creationProfile.reset();
                                            $("#id").val("");
                                        }

                                        function editProfile(id) {
                                            
                                            $('#creationModal').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                            
                                            $('.container').hide();
                                            onEdit("/masterdata/obtener-perfil.htm?id=" + id,
                                                    "creationModal");
                                        }

                                        $('#saveButton').confirmation({
                                            onConfirm: function (event, element) {
                                                if (validateForm("creationProfile")) {
                                                    $('#creationProfile').submit();
                                                }
                                            }
                                        });
        </script>      
        <style>
            .buttons-csv{
                display: inline-block;
                background-image:url(resources/images/csv.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-csv span{
                opacity: 0;
            }
            .buttons-excel{
                display: inline-block;
                background-image:url(resources/images/excel.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-excel span{
                opacity: 0;
            }
            .buttons-pdf{
                display: inline-block;
                background-image:url(resources/images/pdf.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-pdf span{
                opacity: 0;
            }
            .buttons-print{
                display: inline-block;
                background-image:url(resources/images/print.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-print span{
                opacity: 0;
            }

            div.dt-buttons {
                float: right;
                margin-left:10px;
            }
        </style>
    </body>
</html>
