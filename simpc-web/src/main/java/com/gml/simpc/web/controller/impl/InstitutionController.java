/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserController.java
 * Created on: 2016/10/19, 02:14:58 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.gml.simpc.api.entity.Approval;
import com.gml.simpc.api.entity.Headquarter;
import com.gml.simpc.api.entity.Institution;
import com.gml.simpc.api.entity.Session;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.entity.exeption.SystemException;
import com.gml.simpc.api.entity.exeption.UserException;
import com.gml.simpc.api.entity.exeption.code.ErrorCode;
import com.gml.simpc.api.entity.valuelist.City;
import com.gml.simpc.api.entity.valuelist.State;
import com.gml.simpc.api.enums.ApprovalInstitutionStatus;
import com.gml.simpc.api.enums.InstitutionStatus;
import com.gml.simpc.api.enums.LegalNature;
import com.gml.simpc.api.enums.Origin;
import com.gml.simpc.api.services.ApprovalService;
import com.gml.simpc.api.services.InstitutionService;
import com.gml.simpc.api.services.UserService;
import com.gml.simpc.api.utilities.Util;
import com.gml.simpc.commons.annotations.AnnotationInterceptor;
import com.gml.simpc.commons.annotations.FunctionalityInterceptor;
import com.gml.simpc.commons.mail.sender.Notificator;
import com.gml.simpc.web.controller.ControllerDefinition;
import com.gml.simpc.web.utilities.ValueListService;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gml.simpc.api.entity.exeption.FileException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_001_UNKNOW;
import static com.gml.simpc.api.entity.exeption.code.FileErrorCode.FILE_ERR_015_FILE_TYPE_INVALID;
import static com.gml.simpc.api.utilities.Util.FAILED_ALERT;
import static com.gml.simpc.api.utilities.Util.PARAMETER_ACTIVATE_INSTITUTION;
import static com.gml.simpc.api.utilities.Util.PARAMETER_CREATE_INSTITUTION;
import static com.gml.simpc.api.utilities.Util.SESSION_USER;
import static com.gml.simpc.api.utilities.Util.SUCCESS_ALERT;
import com.gml.simpc.loader.file.FileValidator;
import java.io.File;
import java.util.Calendar;

/**
 * Controlador para la ventana de manejo de instituciones oferentes.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@FunctionalityInterceptor(name = "instituciones-oferentes.htm")
@Controller
public class InstitutionController implements ControllerDefinition {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER
            = Logger.getLogger(InstitutionController.class);
    /**
     * Mensaje de creaci�n de instituciones a los usuarios administradores.
     */
    private static final String MENSAJE_CREACION_INSTITUCION = "Se ha creado "
            + "la instituci�n %s exitosamente. Se encuentra lista para revisi�n de "
            + "aprobaci�n ";
    /**
     * Mensaje de edicion de una institucion.
     */
    private static final String MENSAJE_EDITAR_INSTITUCION
            = "Se ha modificado la instituci�n %s exitosamente. La Causa fue : ";
    /**
     * Servicio para adminisraci&oacute;n de instituciones oferentes; Servicio
     * de instutuciones intermediario para la comunicaci&oacute;n con la
     * persistencia.
     */
    @Autowired
    private InstitutionService institutionService;
    /**
     * Servicio para listas de valores.
     */
    @Autowired
    private ValueListService valueListService;

    @Autowired
    private Notificator notificator;
    /**
     * Servicio de usuarios.
     */
    @Autowired
    private UserService userService;
    /**
     * Servicio para adminisraci&oacute;n de instituciones oferentes; Servicio
     * de instutuciones intermediario para la comunicaci&oacute;n con la
     * persistencia.
     */
    @Autowired
    private ApprovalService approvalService;

    /**
     * Inicializa el binder para formatear fechas.
     *
     * @param binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class,
                new CustomDateEditor(sdf, true));
    }

    /**
     * M&eacute;todo que se ejecuta al abrir la ventana de index; Trae listas
     * necesarias para el funcionamiento de la ventana.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "instituciones-oferentes",
            method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
            HttpServletResponse response) {
        LOGGER.info("Ejecutando metodo [ onDisplay InstitutionController ]");
        ModelAndView modelAndView
                = new ModelAndView("instituciones-oferentes");

        init(modelAndView, false);

        return modelAndView;
    }

    /**
     * M&eacute;etodo para obener una instituci&oacute;n.
     * 
     * @param request
     * @param response
     * @return
     * @throws UserException 
     */
    @RequestMapping(value = "obtener-institucion",
            method = RequestMethod.GET)
    public @ResponseBody
    String onEditInstitution(HttpServletRequest request,
            HttpServletResponse response) throws UserException {
        LOGGER.info("Ejecutando metodo [ onEditInstitution ]");

        try {
            Institution institution = this.institutionService.
                    findById(Long.parseLong(request.getParameter("id")));
            ObjectMapper mapper = new ObjectMapper();

            return mapper.writeValueAsString(institution);
        } catch (Exception ex) {
            LOGGER.error("Error", ex);

            throw new UserException(ERR_001_UNKNOW);
        }
    }

    /**
     * M&eacute;todo que se ejecuta al abrir la ventana de index; Trae listas
     * necesarias para el funcionamiento de la ventana.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "instituciones-aprobadas",
            method = RequestMethod.GET)
    public ModelAndView onDisplayApproved(HttpServletRequest request,
            HttpServletResponse response) {
        LOGGER.info("Ejecutando metodo [ onDisplay InstitutionController ]");

        ModelAndView modelAndView
                = new ModelAndView("instituciones-aprobadas");
        modelAndView.addObject("institutionList", new ArrayList<Institution>());
        init(modelAndView, false);
        return modelAndView;
    }

    /**
     * M&eacute;todo que borra una instituci&oacute;n
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "borrarInstitucion",
            method = RequestMethod.POST)
    public ModelAndView delete(HttpServletRequest request,
            HttpServletResponse response) {
        LOGGER.info("entra a borrar en el controller de instituciones");
        ModelAndView modelAndView
                = new ModelAndView("instituciones-oferentes");
        try {
            if (request.getParameter("id") != null) {
                Long institucionId = Long.parseLong(request.getParameter("id"));
                this.institutionService.
                        remove(new Institution(institucionId));
            }
            modelAndView.addObject("msg",
                    "Se ha eliminado la instituci�n exitosamente.");
            modelAndView.addObject("msgType", SUCCESS_ALERT);
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msgType", FAILED_ALERT);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("finally de eliminar instituciones");
        }
        init(modelAndView, false);
        return modelAndView;
    }

    /**
     * M&eacute;todo que consulta las instituciones oferentes de acuerdo a lo
     * diligenciado en los filtros de formulario de consulta.
     *
     * @param request
     * @param response
     * @param searchInstitution
     *
     * @return
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
            afterExecutionValue = "DESPUES", methodName = "BUSCARPRUEBA",
            entityTableName = "INSTITUCIONES")
    @RequestMapping(value = "buscarInstituciones",
            method = RequestMethod.POST)
    public ModelAndView search(HttpServletRequest request,
            HttpServletResponse response,
            @ModelAttribute("searchInstitution") Institution searchInstitution) {
        LOGGER.info("entra a consultar en el controller de instituciones");

        ModelAndView modelAndView
                = new ModelAndView("instituciones-oferentes");

        try {
            List<Institution> institutionList = this.institutionService.
                    findByFilters(searchInstitution);
            modelAndView.addObject("institutionList", institutionList);
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msgType", FAILED_ALERT);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("finally de consultar en InstitutionsController");
        }

        init(modelAndView, false);

        return modelAndView;
    }

    /**
     * M&eacute;todo que consulta las instituciones aprobadas de acuerdo a lo
     * diligenciado en los filtros de formulario de consulta.
     *
     * @param request
     * @param response
     * @param approDateIni
     * @param approDateFin
     * @param regisDateIni
     * @param regisDateFin
     *
     * @return
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
            afterExecutionValue = "DESPUES", methodName = "BUSCARPRUEBA",
            entityTableName = "INSTITUCIONES")
    @RequestMapping(value = "buscarInstitucionesAprobadas",
            method = RequestMethod.GET)
    public ModelAndView searchApprovedInstitutions(HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(name = "fechaIniApro") Date approDateIni,
            @RequestParam(name = "fechaFinApro") Date approDateFin,
            @RequestParam(name = "fechaIniReg") Date regisDateIni,
            @RequestParam(name = "fechaFinReg") Date regisDateFin) {
        LOGGER.info(
                "entra a searchApprovedInstitutions en el controller de instituciones");

        Date approDateFinC = null;
        Date regisDateFinC = null;

        if (approDateFin != null) {
            Calendar calendarF1 = Calendar.getInstance();
            calendarF1.setTime(approDateFin);
            calendarF1.add(Calendar.DAY_OF_YEAR, 1);
            approDateFinC = calendarF1.getTime();
        }
        if (regisDateFin != null) {
            Calendar calendarF2 = Calendar.getInstance();
            calendarF2.setTime(regisDateFin);
            calendarF2.add(Calendar.DAY_OF_YEAR, 1);
            regisDateFinC = calendarF2.getTime();
        }

        ModelAndView modelAndView
                = new ModelAndView("instituciones-aprobadas");

        String institutionName = Util.likeValue(request.
                getParameter("institutionName"));
        String institutionType = Util.likeValue(request.
                getParameter("tipoInstitucion"));
        String regisCcf = Util.likeValue(request.
                getParameter("ccfRegistra"));

        try {

            List<Institution> institutionList = this.institutionService.
                    findApprovedInstitutions(institutionName, institutionType,
                            regisCcf, approDateIni, approDateFinC,
                            regisDateIni, regisDateFinC);
            modelAndView.addObject("institutionList", institutionList);
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("finally de consultar en InstitutionsController");
        }

        init(modelAndView, false);

        return modelAndView;
    }

    /**
     * M&eacute;todo que activa una instituci&oacute;n.
     * 
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = "activarInstitucion",
            method = RequestMethod.POST)
    public ModelAndView activateInstitution(HttpServletRequest request,
            HttpServletResponse response) {

        Session session = (Session) request.getSession().getAttribute(
                SESSION_USER);
        User userSession = session.getUser();

        ModelAndView modelAndView
                = new ModelAndView("instituciones-oferentes");
        boolean creationError = false;

        try {

            Long idInstitution = Long.parseLong(request.getParameter("id"));
            String var = request.getParameter("status");

            Institution i = this.institutionService.
                    activateInstitution(var, "N",
                            userSession.getId(), new Date(), idInstitution);

            String modificationCause = this.sendMessage(i,
                    MENSAJE_EDITAR_INSTITUCION.
                            concat(request.getParameter("message-text")),
                    PARAMETER_ACTIVATE_INSTITUTION);

            Approval approval = new Approval(idInstitution, userSession,
                    new Date(), modificationCause);

            this.approvalService.save(approval);

            modelAndView.addObject("msg", "Se ha activado la instituci�n"
                    + " exitosamente.");
            modelAndView.addObject("msgType", SUCCESS_ALERT);
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
            modelAndView.addObject("msgType", FAILED_ALERT);
        }

        init(modelAndView, creationError);

        return modelAndView;

    }

    /**
     * M&eacute;todo que guarda una instituci&oacute;n.
     *
     * @param request
     * @param response
     * @param creationInstitution
     * @return
     */
    @RequestMapping(value = "guardarInstitucion",
            method = RequestMethod.POST)
    public ModelAndView save(HttpServletRequest request,
            HttpServletResponse response, @Valid @ModelAttribute(
                    "creationInstitution"
            ) Institution creationInstitution) throws FileException {

        Session session = (Session) request.getSession().
                getAttribute(SESSION_USER);
        User sessionUser = session.getUser();

        ModelAndView modelAndView = new ModelAndView("instituciones-oferentes");
        boolean creationError = false;

        if ("".equals(request.getParameter("id"))) {
            modelAndView.addObject("tipoformulario", "nuevo");
        } else {
            modelAndView.addObject("tipoformulario", "editar");
        }

        try {
            String name = ((MultipartFile) creationInstitution.getFile()).
                    getOriginalFilename();

            if (!name.equals(request.getParameter("certAntigua"))) {

                String ext = name.substring(name.lastIndexOf(".") + 1);
                FileValidator.validateExtentionPDF(ext);
            }
            
            validateDV(creationInstitution);
            boolean isCreation = true;

            creationInstitution.setModificationDate(new Date());
            creationInstitution.setModificationUserId(sessionUser);
            creationInstitution.setApprovalStatus(ApprovalInstitutionStatus.N);

            byte[] file;

            if (creationInstitution.getFile() != null) {
                LOGGER.info("Llego archivo desde la ventana.");

                file = ((MultipartFile) creationInstitution.getFile()).
                        getBytes();
                creationInstitution.setQualityCertificate(file);
            }

            if (creationInstitution.getId() == null) {
                creationInstitution.setCreationDate(new Date());
                creationInstitution.setCreationUserId(sessionUser);
                /*
                 * Guardamos el programa
                 */

                this.institutionService.save(creationInstitution);
            } else {
                this.institutionService.update(creationInstitution);
                isCreation = false;
            }

            String message;

            if (isCreation) {
                message = this.sendMessage(creationInstitution,
                        MENSAJE_CREACION_INSTITUCION, PARAMETER_CREATE_INSTITUTION);
            } else {
                message = this.sendMessage(creationInstitution,
                        MENSAJE_EDITAR_INSTITUCION.
                                concat(request.getParameter("message-text")),
                        PARAMETER_ACTIVATE_INSTITUTION);
            }

            Approval approval = new Approval(creationInstitution.getId(),
                    sessionUser, new Date(), message);

            this.approvalService.save(approval);

            modelAndView.addObject("msg", "Se ha guardado la instituci�n "
                    + creationInstitution.getInstitutionName() + " exitosamente.");
            modelAndView.addObject("msgType", SUCCESS_ALERT);
            modelAndView.addObject("oldCert", name);
        } catch (SystemException ex) {
            creationError = true;

            try {
                LOGGER.error("Error ", ex);
                modelAndView.addObject("internalMsg", ex.getMessage());
                ObjectMapper om = new ObjectMapper();
                modelAndView.addObject("jsonInstitution",
                        om.writeValueAsString(creationInstitution));
                modelAndView.addObject("msgType", FAILED_ALERT);
            } catch (IOException ex1) {
                LOGGER.error("Error", ex1);
                modelAndView.addObject("internalMsg", ERR_001_UNKNOW.
                        getMessage());
                modelAndView.addObject("msgType", FAILED_ALERT);
            }
        } catch (FileException ex) {
            creationError = true;

            try {
                LOGGER.error("Error ", ex);
                modelAndView.addObject("internalMsg", ex.getMessage());
                ObjectMapper om = new ObjectMapper();
                modelAndView.addObject("jsonInstitution",
                        om.writeValueAsString(creationInstitution));
                modelAndView.addObject("msgType", FAILED_ALERT);
            } catch (IOException ex1) {
                LOGGER.error("Error", ex1);
                modelAndView.addObject("internalMsg", ERR_001_UNKNOW.
                        getMessage());
                modelAndView.addObject("msgType", FAILED_ALERT);
            }
        } catch (Exception ex) {
            creationError = true;
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
            modelAndView.addObject("msgType", FAILED_ALERT);
        } finally {
            LOGGER.info("finally save in InstitutionsController");
        }

        init(modelAndView, creationError);

        return modelAndView;
    }

    /**
     * M&Eacute;todo para descargar la certificaci&oacute;n de 
     * una instituci&oacute;m
     * 
     * @param request
     * @param response 
     */
    @RequestMapping(value = "descargarCertificado", method = RequestMethod.POST)
    public void getFile(HttpServletRequest request,
            HttpServletResponse response) {

        try {
            Institution institution = this.institutionService.
                    findById(Long.parseLong(request.getParameter("id")));

            if (institution.getQualityCertificate() != null) {
                InputStream is
                        = new ByteArrayInputStream(institution.getQualityCertificate());

                IOUtils.copy(is, response.getOutputStream());
                response.setContentType("application/pdf");
                response.flushBuffer();
            }
        } catch (IOException ex) {
            LOGGER.info("Error al descargar archivo.", ex);
        }
    }

    /**
     * Inicia los objetos para el controler.
     * 
     * @param modelAndView
     * @param creationError 
     */
    private void init(ModelAndView modelAndView, boolean creationError) {
        LOGGER.info("Ejecutando metodo [ init ]");

        modelAndView.addObject("institutionStatusList", InstitutionStatus.
                values());
        modelAndView.addObject("approvalInstitutionStatusList",
                ApprovalInstitutionStatus.values());
        modelAndView.addObject("legalNatureList", LegalNature.values());
        modelAndView.addObject("originList", Origin.values());
        modelAndView.addObject("searchInstitution", new Institution());

        if (!creationError) {
            modelAndView.addObject("creationInstitution",
                    new Institution(new Headquarter(new City(), new State())));
        }

        modelAndView.addObject("institutionTypes",
                this.valueListService.getInstitutionTypeList());
        modelAndView.addObject("documentTypes",
                this.valueListService.getDocumentTypeList());
        modelAndView.addObject("stateList",
                this.valueListService.getStateList());
        modelAndView.addObject("citiesList",
                this.valueListService.getCityList());
        modelAndView.
                addObject("ccfs", this.valueListService.getFoundationList());
        modelAndView.addObject("typeQualityCertificate",
                this.valueListService.getTypeQualityCertificate());
    }

    /**
     * Se crea mensaje para enviar correo electr&oacute;nico relacionado con una
     * instituci&oacute;n
     * 
     * @param institution
     * @param message
     * @param subjet
     * @return 
     */
    private String sendMessage(Institution institution, String message,
            String subjet) {
        String fullMessage = String.format(message, institution.
                getInstitutionName());

        for (User usuario : this.userService.findByAdmin(true)) {
            notificator.sendNotification(usuario.getEmail(), fullMessage,
                    usuario.getFirstName().concat(" ").concat(usuario.
                            getFirstSurname()), subjet);
        }
        return fullMessage;
    }

    /**
     * M�todo para validar el digito de verificaci�n de un NIT o RUT ingresado
     * al momento de crear una instituci�n.
     *
     * @param institution
     *
     * @throws UserException
     */
    private void validateDV(Institution institution) throws UserException {
        int[] nums = {
            3, 7, 13, 17, 19, 23, 29, 37, 41, 43, 47, 53, 59, 67, 71
        };

        int sum = 0;

        String str = String.valueOf(institution.getNit());
        for (int i = str.length() - 1, j = 0; i >= 0; i--, j++) {
            sum += Character.digit(str.charAt(i), 10) * nums[j];
        }

        Integer dv = (int) ((sum % 11) > 1 ? (11 - (sum % 11)) : (sum % 11));

        if (!Objects.equals(dv, institution.getDv())
                && (institution.getDocumentType().getId() == 1
                || institution.getDocumentType().getId() == 4)) {
            throw new SystemException(ErrorCode.ERR_030_VALIDATION_ERROR);
        }
    }
}
