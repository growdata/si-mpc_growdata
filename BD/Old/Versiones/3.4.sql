DROP TABLE IND_VARIABLES;
DROP TABLE IND_FUENTES;
DROP TABLE IND_INDICADORES;

CREATE TABLE IND_FUENTES 
   (	
    ID NUMBER(19,0), 
    NOMBRE VARCHAR2(255), 
    TABLA VARCHAR2(255)
   );
ALTER TABLE IND_FUENTES ADD PRIMARY KEY (ID);

CREATE TABLE IND_VARIABLES 
   (	
    IND_FUENTE_ID NUMBER(19,0),
    ID NUMBER(19,0), 
    CAMPO VARCHAR2(255),
    DESCRIPCION VARCHAR2(255),
    TIPO VARCHAR2(255),
    DETALLE VARCHAR2(255)
   );
ALTER TABLE IND_VARIABLES ADD PRIMARY KEY (ID);
ALTER TABLE IND_VARIABLES ADD CONSTRAINT FK_IND_VARIABLES 
    FOREIGN KEY (IND_FUENTE_ID) REFERENCES IND_FUENTES (ID); 

CREATE TABLE IND_INDICADORES 
   (	
    ID NUMBER(19,0), 
    NOMBRE VARCHAR2(255),
    DESCRIPCION VARCHAR2(255),
    IND_FUENTE_ID NUMBER(19,0),
    TIPO_GRAFICO VARCHAR2(50), 
    IND_VARIABLE_X_ID NUMBER(19,0),
    IND_VARIABLE_SERIE_ID NUMBER(19,0),
    IND_VARIABLE_Y_ID NUMBER(19,0),
    FUNCION VARCHAR2(50) ,
 CONDICION clob
   );
ALTER TABLE IND_INDICADORES ADD PRIMARY KEY (ID);

Insert into IND_FUENTES (ID,NOMBRE,TABLA) values ('1','Prestaciones Economicas','MD_F_FOSFEC');
Insert into IND_FUENTES (ID,NOMBRE,TABLA) values ('2','Capacitación','MD_F_CAP_PROGRAMA_MODULO');
Insert into IND_FUENTES (ID,NOMBRE,TABLA) values ('3','Detalle de capacitación','MD_VIEW_PORTAL_CAP');
Insert into IND_FUENTES (ID,NOMBRE,TABLA) values ('4','Valores percapita en salud y pension','VW_PER_CAPITA_SALUD_PENSION');
Insert into IND_FUENTES (ID,NOMBRE,TABLA) values ('5','Valores percapita en alimentacion','VW_PERCAPITA_ALIMENTACION');
Insert into IND_FUENTES (ID,NOMBRE,TABLA) values ('6','Valores percapita ahorro de cesantias','VW_AHORRO_CESANTIAS_PERCAPITA');
Insert into IND_FUENTES (ID,NOMBRE,TABLA) values ('7','Valores percapita cuota monetaria','VW_CUOTA_MON_PERCAPITA');
Insert into IND_FUENTES (ID,NOMBRE,TABLA) values ('8','Valores percapita prestaciones economicas','VW_PRESTACIONES_PERCAPITA');
Insert into IND_FUENTES (ID,NOMBRE,TABLA) values ('9','Valores percapita por cuota monetaria por dependientes ','VW_CUOTA_DEPENDIETES_PERCAPITA');
--------------------------------------------------------

commit;


Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('261','Valor Per-capita de Salud  (Barras x CCF)','1','B','7','6','11','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('72','Beneficiarios de Liquidados (salud y pension)(Lineas x Periodo)','1','L','25','7','13','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('82','Beneficiarios cuota monetaria  (Lineas Periodo)','1','L','25','7','10','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('271','Valor Per-capita de  Pensión  (Barras x CCF)','1','B','7','6','11','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('331','Capacitaciones realizadas en la agce','2','B','224','221','225','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('341','Capacitaciones realizadas por tipo de Institución','2','B','226','221','225','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('351','Capacitaciones realizadas por tipo de formación','2','B','223','221','225','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('361','Capacitaciones realizadas por  cine a.c.','2','B','227','221','225','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('371','Por necesidad de formación la capacitación','2','B','228','221','225','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('381','Por tipo de certificación de programa','2','B','221','228','225','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('401','Personas ocupadas remitidas a este curso','2','B','221','228','225','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('411','Personas que se remitieron al curso','3','B','331','333','338','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('421','Personas que desertaron','3','B','331','339','338','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('431','Asistencia a la capacitación','3','B','331','340','338','AVG');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('441','Costo total de la matricula por tipo de programa','3','B','331','332','341','SUM');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('451','Costo total del subsidio de transporte por tipo de programa ','3','B','331','332','342','SUM');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('461','Otros costos asociados al curso por tipo de programa','3','B','331','332','343','SUM');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('21','Postulados No Aceptados
  (Barra x CCF)
','1','B','7','8','4','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('12','Postulados al Mecanismo (Lineas x Periodo)','1','L','1','7','3','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('31','Beneficiarios (Postulados aceptados al Mecanismo)
  (Barras x CCF)
','1','B','7','8','4','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('41','Proporción de beneficiarios sobre el total de postulados (Circulo)','1','D','1','7','4','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('51','Proporción de beneficiarios salientes del total de beneficiarios (Circulo)','1','D','8','7','26','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('71','Beneficiarios de Liquidados (salud y pension)( Barras x CCF)','1','B','7','25','13','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('81','Beneficiarios cuota monetaria ( Barras x CCF)','1','B','7','25','10','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('91','Beneficiarios de incentivo de ahorro (Barras x CCF)','1','B','7','25','11','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('101','Número de Beneficiarios de Bono de Alimentación (Barras x CCF)','1','B','7','25','12','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('111','Beneficiarios del incentivo de ahorro del total de beneficiarios del Mecanismo de Protección al Cesante.  (Circulo)','1','D','25','7','27',null);
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('121','Proporción Beneficiarios de cuota monetaria ( Circulo)','1','D','25','7','15','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('201','Total liquidado en Salud (Barras x CCF)','1','B','7','25','13','SUM');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('211','Total Liquidado en Pensión  (Barras x CCF)','1','B','7','25','14','SUM');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('221','Total Liquidado en Cuota Monetaria  (Barras x CCF)','1','B','7','25','10','SUM');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('231','Total Liquidado en Bono de Alimentación  (Barras x CCF)','1','B','7','25','12','SUM');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('241','Total Liquidado Por Ahorro de Cesantías  (Barras x CCF)','1','B','7','25','11','SUM');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('251','Total Prestaciones Económicas  (Barras x CCF)','1','B','7','25','27','SUM');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('281','Valor Per-capita de  Cuota    (Barras x CCF)','1','B','7','6','11','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('291','Valor Per-capita de  Bono de Alimentación  (Barras x CCF)','1','B','7','6','11','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('301','Valor Per-capita Liquidación  Ahorro de Cesantías  (Barras x CCF)','1','B','7','6','11','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('311','Valor Per-capita Prestaciones Económicas  (Barras x CCF)','1','B','7','6','11','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('321','Valor Per-capita de Cuota Monetaria por dependientes económicos   (Barras x CCF)','1','B','7','6','11','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('11','Postulados al Mecanismo (Barra x CCF)','1','B','7','1','3','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('22','Postulados No Aceptados
  (Lineas x Periodo)

','1','L','8','7','4','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('32','Beneficiarios (Postulados aceptados al Mecanismo)  (Linea x Periodo)','1','L','8','7','4','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('92','Beneficiarios de incentivo de ahorro (Lineas x Periodo)','1','L','25','7','11','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('102','Número de Beneficiarios de Bono de Alimentación  (Lineas x Periodo)','1','L','25','7','12','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('131','Porcentaje de ahorro para cesantías (Circulo)','1','D','25','7','15','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('141','Beneficiarios de las Agencias de Gestión y Colocación (todos los inscritos a agencia)','1','D','7','11','15','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('151','Beneficiarios de capacitación (si una persona esta en 3 se reporta una sola vez por persona)','1','D','7','11','15','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('161','Proporción de beneficiarios de incentivo sobre el total de beneficiarios','1','D','7','11','15','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('171','Beneficiarios de Capacitación del total de beneficiarios','1','D','7','11','15','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('181','Beneficiarios por tipo de curso de capacitación','1','D','7','11','15','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('191','Beneficiarios de capacitación que terminaron','1','D','7','11','15','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('202','Total liquidado en Salud (Lineas x Periodo)','1','L','25','7','13','SUM');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('212','Total Liquidado en Pensión (Lineas x Periodo)','1','L','25','7','14','SUM');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('222','Total Liquidado en Cuota Monetaria (Lineas x Periodo)','1','L','25','7','10','SUM');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('232','Total Liquidado en Bono de Alimentación (Lineas x Periodo)','1','L','25','7','12','SUM');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('242','Total Liquidado Por Ahorro de Cesantías  (Lineas x Periodo)','1','L','25','7','11','SUM');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('252','Total Prestaciones Económicas (Lineas x Periodo)','1','L','25','7','27','SUM');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('262','Valor Per-capita de Salud  (Lineas x Periodo)','1','B','7','6','11','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('272','Valor Per-capita de  Pensión  (Lineas x Periodo)','1','L','6','7','10','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('282','Valor Per-capita de  Cuota Monetaria (Lineas x Periodo)','1','L','6','7','10','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('292','Valor Per-capita de  Bono de Alimentación','1','L','6','7','10','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('302','Valor Per-capita Liquidación  Ahorro de Cesantías','1','L','6','7','10','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('312','Valor Per-capita Prestaciones Económicas','1','L','6','7','10','COUNT');
Insert into SYSTEM.IND_INDICADORES (ID,NOMBRE,IND_FUENTE_ID,TIPO_GRAFICO,IND_VARIABLE_X_ID,IND_VARIABLE_SERIE_ID,IND_VARIABLE_Y_ID,FUNCION) values ('322','Valor Per-capita de Cuota Monetaria por dependientes económicos ','1','L','6','7','10','COUNT');
--------------------------------------------------------



-------------

truncate table IND_VARIABLES;

Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('1','21','campo',null,'TIPO_IDENTIFICACION');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('1','22','campo',null,'numero_identificacion');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('2','221','periodo','yyyy-MM','fecha_inicio');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('2','222','periodo','yyyy-MM','fecha_fin');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('2','223','tabla','tipos_programa','tipo_curso');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('2','224','tabla','vw_cajas_compensacion','codigo_ccf');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('2','225','campo',null,'codigo_programa');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('2','226','tabla','vw_tipos_institucion','codigo_inst');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('4','18','campo',null,'periodo');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('4','19','campo',null,'valor_percapita_salud');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('4','20','campo',null,'valor_percapita_pension');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('2','227','tabla','vw_cine','cine');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('2','228','tabla','vw_necesidad','necesidad');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('3','331','periodo','yyyy-MM','fecha_fin');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('3','332','tabla','tipos_programa','tipo_cap_id');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('3','333','tabla','vw_cajas_compensacion','ccf_code');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('3','334','campo',null,'cod_programa');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('3','335','tabla','vw_tipos_institucion','tipo_inst_id');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('3','336','tabla','vw_cine','cine');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('3','337','tabla','vw_necesidad','necesidad');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('3','338','campo',null,'documento');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('3','339','tabla','vw_estados_asistencia_cap','estado');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('3','340','campo',null,'porcentaje_asistencia');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('3','341','campo',null,'costo_matricula');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('3','342','campo',null,'COSTO_TRANSPORTE');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('3','343','campo',null,'OTROS_COSTOS');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('1','1','periodo','yyyy-MM','fecha_postulacion_mpc');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values (null,'3','campo',null,'TIPO_IDENTIFICACION || numero_identificacion ');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('1','4','campo',null,'verificacion_requisitos');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('1','5','tabla','vw_estados_beneficio','terminacion_beneficio');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('1','6','campo',null,'periodo_beneficio_liquidado');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('1','7','tabla','vw_cajas_compensacion','codigo_ccf');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('1','8','periodo','yyyy-MM','fecha_verificacion_postu');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('1','9','periodo','yyyy-MM','fecha_terminacion_beneficio');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('1','10','campo',null,'monto_liquidado_cuota');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('1','11','campo',null,'Monto_Liquidado_Cesantias');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('1','12','campo',null,'Monto_Liquidado_Alimentacion');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('1','13','campo',null,'monto_liquidado_salud');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('1','14','campo',null,'monto_liquidado_pension');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('1','15','campo',null,'valor_total_prestaciones');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('1','16','campo',null,'beneficiario_del_mecanismo');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('1','23','campo',null,'inscripcion_servicio_empleo');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('1','24','campo',null,'tipo_vinculacion_ccf');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('1','25','periodo','yyyy-MM','fecha_liquidacion_beneficio');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('1','26','campo',null,'TERMINACION_BENEFICIO');
Insert into SYSTEM.IND_VARIABLES (IND_FUENTE_ID,ID,TIPO,DETALLE,CAMPO) values ('1','27','campo',null,'valor_total_prestaciones');
--Creación de vista para cajas de compensacion
create or replace view vw_cajas_compensacion as
    select codigo id,nombre_corto descripcion
    from fundaciones 
    where es_ccf = 'V'
;   
commit;
--Creación de vista para tipos de institucion
create or replace view vw_tipos_institucion as
    select 
    t.id,t.DESCRIPCION
    from MD_F_CAP_PROGRAMA_MODULO md, INSTITUCIONES i,TIPOS_INSTITUCION t
    where md.codigo_inst = i.id and t.id = i.TIPO_INSTITUCION_ID
    group by t.id,t.DESCRIPCION
;

commit;




--Creación de vista para codigos cine
create or replace view vw_cine as
    select id,valor descripcion
    from cine
;  
commit;
--Creacio de vista para necesidad
create or replace view vw_necesidad as
select 
necesidad id,
case when necesidad = '1' then 'Necesidad de la empresa.'
 when necesidad = '2' then 'Necesidad del buscador.'
 when necesidad = '3' then 'Necesidad del sector productivo.'
 when necesidad = '4' then 'Ninguna de las anteriores'
end descripcion
from MD_F_CAP_PROGRAMA_MODULO
group by necesidad;
commit;
--Creación de vista para estado de aprobacion en fosfec
create or replace view vw_estados_aprobacion as
    select 
    verificacion_requisitos id,
    case when verificacion_requisitos =1 then  'APROBADO'
    when  verificacion_requisitos =2 then 'DENEGADO'
    when verificacion_requisitos=3 then 'DESISTIDO'
    when  verificacion_requisitos =4 then 'CANCELADO'
    when  verificacion_requisitos =5 then 'EN PROCESO DE VERIFICACIÓN'
    end descripcion
    from MD_F_FOSFEC group by verificacion_requisitos;
  commit;  
--Creación de vista para estado de beneficio en fosfec
create or replace view vw_estados_beneficio as
    select 
    terminacion_beneficio id,
    case when terminacion_beneficio =1 then  'FINALIZADO'
    when  terminacion_beneficio =2 then 'NO FINALIZADO'
    end descripcion
    from MD_F_FOSFEC group by terminacion_beneficio;    
commit;
--Creación de vista para estado de asistencia a capacitacion
create or replace view vw_estados_asistencia_cap as
select estado id,
case when estado = '1' then 'Finalizado'
 when estado = '2' then 'En curso'
 when estado = '3' then 'Desertado'
 when estado = '4' then 'Inscrito'
 end descripcion
 from MD_F_PROGRAMA_DETALLE
 group by estado;

commit;
