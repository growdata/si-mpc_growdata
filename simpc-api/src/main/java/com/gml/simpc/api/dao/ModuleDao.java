/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ModuleDao.java
 * Created on: 2016/12/14, 03:28:20 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dao;

import com.gml.simpc.api.dto.ModuleDto;
import java.util.List;

/**
 * DAO para consultar modulos.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface ModuleDao {

    /**
     * Consulta Modulos por CCF.
     *
     * @param startDate
     * @param endDate
     * @param programName
     * @param programCode
     * @param moduloName
     * @param ccfCode
     * @param institution
     * @param fomationType
     *
     * @return
     */
    
    /**
     * Consulta programas modulos sin  filtros fecha
     */
    List<ModuleDto> findModulesByFilters(ModuleDto searchModules);
    /**
     * Consulta programas modulos con fecha inicio
     */
    List<ModuleDto> findModulesByFiltersStartDate(ModuleDto searchModules);
     /**
     * Consulta programas modulos con fecha fin
     */
    List<ModuleDto> findModulesByFiltersEndDate(ModuleDto searchModules);
     /**
     * Consulta programas modulos con rango de fechas
     * 
     */
    List<ModuleDto> findModulesByFiltersRange(ModuleDto searchModules);

    /**
     * Consulta Detalle de los modulos
     *
     * @param id
     * @return
     */
    List<ModuleDto> findModules(String id);

    
}
