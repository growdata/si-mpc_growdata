/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Profile.java
 * Created on: 2016/10/21, 11:05:46 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import com.gml.simpc.api.enums.AdminStatus;
import com.gml.simpc.api.enums.ProfileStatus;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Entidad para manejo de perfiles.
 *
 * @author <a href="mailto:jonathanp@gmlsoftware.com">Jonathan Ponton</a>
 */
@Table(name = "PERFILES")
@Entity
public class Profile implements Serializable {

    /**
     * ID de del perfil.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator =
        "profiles_seq")
    @SequenceGenerator(name = "profiles_seq", sequenceName =
        "PROFILES_SEQ", allocationSize = 1, initialValue= 1)
    private Long id;

    /**
     * Descripci&oacute;n del perfil.
     */
    @Column(name = "DESCRIPCION")
    private String description;
    /**
     * Nombre del perfil.
     */
    @NotNull(message =
        "El campo Nombre de Perfil ya se encuentra registrado, por favor intente con otro.")
    @Column(name = "NOMBRE", unique = true, nullable = false)
    private String name;

    /**
     * Estado del perfil.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "ESTADO", columnDefinition = "char(1) default 'A'",
        nullable = false)
    private ProfileStatus status = ProfileStatus.A;

    /**
     * Estado del perfil.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "ADMINISTRADOR", columnDefinition = "char(1) default 'F'",
        nullable = false)
    private AdminStatus adminStatus = AdminStatus.F;

    public AdminStatus getAdminStatus() {
        return adminStatus;
    }

    public void setAdminStatus(AdminStatus adminStatus) {
        this.adminStatus = adminStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProfileStatus getStatus() {
        return status;
    }

    public void setStatus(ProfileStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Perfil{" + "id=" + id + ", Descripción=" + description +
            ", Nombre=" + name + ", Estado=" + status + ", Es Administrador=" +
            adminStatus + '}';
    }

}
