/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Light.java
 * Created on: 2017/01/18, 04:30:10 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.smpc.web.validator;

import com.gml.simpc.loader.dto.ValidationErrorDto;
import com.gml.simpc.loader.validator.ValidateCommand;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

/**
 * Clase para implementar validaciones particulares de negocio en carges de
 * información
 * en cuanto a Dependientes
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
@Service(value = "dependentValidateService")
public class DependentValidateImp implements ValidateCommand {

    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(
        DependentValidateImp.class);
    
     /**
     * Atributo para almacenar la lista de errores
     */
    List<ValidationErrorDto> errorsList;
    
    
     private static final String RULE_QUERY =
         "SELECT st.linea, st.TIPO_IDENTIFICACION||st.NUMERO_IDENTIFICACION||'El numero de dependientes no coincide con el numero informado en el archivo Maestro FOSFEC' mensaje  " +
         "  from  st_DEPENDIENTES st inner join  (select numerop,tipop,numero_identificacion from" +
         "    (select DISTINCT NUMERO_IDENTIFICACION numerop,TIPO_IDENTIFICACION tipop, " +
         "ORDEN_DEPENDIENTE dependiente,fecha_RADICACION_mpc fechapostu, count(d.TIPO_ID_PERSONA_CARGO||d.NUMERO_IDENTIFICACION_CARGO) cantidad  " +
         "  from  st_DEPENDIENTES d where d.carga_id= ? " +
         "group by NUMERO_IDENTIFICACION ,TIPO_IDENTIFICACION ,ORDEN_DEPENDIENTE ,fecha_RADICACION_mpc ) dependientes  " +
         "inner join " +
         "(SELECT  DISTINCT numero_identificacion,tipo_identificacion, fecha_postulacion_mpc fecha,numero_personas_cargo FROM  md_f_fosfec " +
         " GROUP BY numero_identificacion, tipo_identificacion,fecha_postulacion_mpc,numero_personas_cargo ) f  " +
         " on (dependientes.numerop=f.numero_identificacion and  dependientes.tipop=f.tipo_identificacion and dependientes.fechapostu=f.fecha) " +
         "  where cantidad<>numero_personas_cargo)t" +
         "  on (st.NUMERO_IDENTIFICACION=numerop and st.TIPO_IDENTIFICACION=tipop) where  st.carga_id=?";

    /**
     * Consulta reglas de negocio
     */
    private static final String RULE_QUERY2 =
        "  SELECT st.linea,   st.TIPO_IDENTIFICACION||st.NUMERO_IDENTIFICACION||'El número de indentificación del postulante no se encuenta en el  Maestro FOSFEC para esa fecha de postulacion' mensaje  " +
"          from  st_DEPENDIENTES st inner join  (select numerop,tipop,numero_identificacion from ( " +
"           select DISTINCT NUMERO_IDENTIFICACION numerop,TIPO_IDENTIFICACION tipop, " +
"          ORDEN_DEPENDIENTE dependiente,fecha_RADICACION_mpc fechapostu  " +
"          from  st_DEPENDIENTES d " +
"           where d.carga_id= ?   " +
"         group by NUMERO_IDENTIFICACION ,TIPO_IDENTIFICACION ,ORDEN_DEPENDIENTE ,fecha_RADICACION_mpc ) dependientes   " +
"           left join  "+
"          (SELECT  DISTINCT numero_identificacion,tipo_identificacion, fecha_postulacion_mpc fecha,numero_personas_cargo FROM  md_f_fosfec  " +
"           GROUP BY numero_identificacion, tipo_identificacion,fecha_postulacion_mpc,numero_personas_cargo ) f   " +
"         on (dependientes.numerop=f.numero_identificacion and  dependientes.tipop=f.tipo_identificacion and dependientes.fechapostu=f.fecha)  " +
"            where numero_identificacion is null)t  " +
"          on (st.NUMERO_IDENTIFICACION=numerop and st.TIPO_IDENTIFICACION=tipop) where  st.carga_id= ?  ";
    
    /**
     * Consulta reglas de negocio
     */
    private static final String RULE_QUERY3 =
    "  SELECT st.linea,  st.TIPO_IDENTIFICACION||st.NUMERO_IDENTIFICACION||'El número de orden no coincide con los beneficiarios reportados' mensaje " +
        "   from  st_DEPENDIENTES st inner join  " +
        " ( select numerop,tipop,numero from " +
        " ( select DISTINCT NUMERO_IDENTIFICACION numerop,TIPO_IDENTIFICACION tipop," +
        "   ORDEN_DEPENDIENTE dependientep,fecha_RADICACION_mpc fechapostu  " +
        "  from  st_DEPENDIENTES d " +
        "  where d.carga_id= ?  " +
        "  group by NUMERO_IDENTIFICACION ,TIPO_IDENTIFICACION ,ORDEN_DEPENDIENTE ,fecha_RADICACION_mpc ) dependientes  " +
        "   left join" +
        "  (select DISTINCT NUMERO_IDENTIFICACION numero,TIPO_IDENTIFICACION tipo," +
        "  ORDEN_DEPENDIENTE dependiente,fecha_RADICACION_mpc fechapostu ," +
        "  count( DISTINCT d.TIPO_ID_PERSONA_CARGO||d.NUMERO_IDENTIFICACION_CARGO) cantidad " +
        "  from  st_DEPENDIENTES d " +
        "  where d.carga_id= ? " +
        "  group by NUMERO_IDENTIFICACION ,TIPO_IDENTIFICACION ,ORDEN_DEPENDIENTE ,fecha_RADICACION_mpc )t " +
        "  on (numerop= NUMERO and tipop=TIPO) " +
        "  WHERE dependientep <> cantidad) " +
        " on (st.NUMERO_IDENTIFICACION=numerop and st.TIPO_IDENTIFICACION=tipop) where  st.carga_id= ?  ";

    /**
     * Plantilla JDBC para consultar las propiedades.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

   /**
     * Realiza la validación.
     *
     * @param loadId
     *
     * @return
     */
    @Override
    public List<ValidationErrorDto> validate(Long loadId) {

        LOGGER.
            info("Validating training bussiness rules for load ... " + loadId);
        this.errorsList = new ArrayList<>();
        validationQuery( RULE_QUERY, new Long[]{loadId,loadId});
        validationQuery( RULE_QUERY2, new Long[]{loadId,loadId});
        validationQuery( RULE_QUERY3, new Long[]{loadId,loadId,loadId});
        LOGGER.info("Final errors: " + this.errorsList.size());
        return this.errorsList;
    }

    private void validationQuery( String query, Long[] params) {
        try {
            List<ValidationErrorDto> resultList =
                this.jdbcTemplate.query(query, params, new RowMapper() {
                    @Override
                    public ValidationErrorDto mapRow(ResultSet rs, int rownumber)
                        throws SQLException {
                        ValidationErrorDto e = new ValidationErrorDto(
                            Integer.parseInt(rs.getString("LINEA")),
                            rs.getString("MENSAJE"));

                        return e;
                    }
                });
            
            addErrors(resultList);
        } catch (Exception e) {
            
            LOGGER.error("Validation query  " + query + " params " + e);
        }

    }

    private void addErrors(List<ValidationErrorDto> errorsList) {
        for (ValidationErrorDto validationErrorDto : errorsList) {
            this.errorsList.add(validationErrorDto);
        }
    }
    /**
     * Verifica si es del tipo que le corresponde.
     *
     * @param loadType
     *
     * @return
     */
    @Override
    public boolean verify(String loadType) {
        return "PES-2".equals(loadType);
    }
}
