/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: MasterWriterLocator.java
 * Created on: 2017/02/01, 09:56:28 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.writer;

import org.springframework.batch.item.support.AbstractItemStreamItemWriter;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Clase para localizar un Item Writer.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class MasterWriterLocator implements ApplicationContextAware, FactoryBean {

    /**
     * Nombre del bean a localizar.
     */
    private String name;
    /**
     * Contexto de la aplicaci&oacute;n.
     */
    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext ac)
        throws BeansException {
        this.applicationContext = ac;
    }

    @Override
    public Object getObject() throws Exception {
        return this.name == null ? null :
            this.applicationContext.getBean(this.name);
    }

    @Override
    public Class getObjectType() {
        return AbstractItemStreamItemWriter.class;
    }

    @Override
    public boolean isSingleton() {
        return false;
    }

    public final void setName(String name) {
        this.name = name;
    }
}
