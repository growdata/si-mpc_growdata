/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: GraphicDataDto.java
 * Created on: 2017/01/04, 09:19:57 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

/**
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class GraphicDataDto {

    /**
     * Valor para el eje X.
     */
    private String axisX;
    /**
     * Valor para el eje Y.
     */
    private int axisY;
    
  
    /**
     * Valor para el eje Z.
     */
    private int axisZ;
    
    /**
     * Valor para campo de agrupación.
     */
    private String groupField;
    

    public String getAxisX() {
        return axisX;
    }

    public void setAxisX(String axisX) {
        this.axisX = axisX;
    }

    public int getAxisY() {
        return axisY;
    }

    public void setAxisY(int axisY) {
        this.axisY = axisY;
    }

    public int getAxisZ() {
        return axisZ;
    }

    public void setAxisZ(int axisZ) {
        this.axisZ = axisZ;
    }

    public String getGroupField() {
        return groupField;
    }

    public void setGroupField(String groupField) {
        this.groupField = groupField;
    }
    
    
}
