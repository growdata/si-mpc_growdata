/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserController.java
 * Created on: 2016/10/19, 02:14:58 PM
 * Project: MPC Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.gml.simpc.api.entity.LoadProcess;
import com.gml.simpc.api.entity.Parameter;
import com.gml.simpc.api.entity.Session;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.enums.ProcessStatus;
import com.gml.simpc.api.services.LoadProcessService;
import com.gml.simpc.api.services.ParameterService;
import com.gml.simpc.api.utilities.Util;
import com.gml.simpc.commons.annotations.FunctionalityInterceptor;
import com.gml.simpc.loader.file.FileUploader;
import com.gml.simpc.web.batch.LoadService;
import com.gml.simpc.web.controller.ControllerDefinition;
import com.gml.simpc.web.utilities.ValueListService;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_001_UNKNOW;
import static com.gml.simpc.api.utilities.Util.FAILED_ALERT;
import static com.gml.simpc.api.utilities.Util.LOAD_SUCCESS_MESSAGE;
import static com.gml.simpc.api.utilities.Util.SESSION_USER;

/**
 * Controlador para cargas de archivos.
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@FunctionalityInterceptor(name = "cargar-archivo.htm")
@Controller
public class LoadController implements ControllerDefinition {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(LoadController.class);

    /**
     * Servicio para gesti&oacute;n de las listas de valor.
     */
    @Autowired
    private ValueListService valueListService;
    /**
     * Servicio para gesti&oacute;n del detalle de cargas.
     */
    @Autowired
    private LoadProcessService loadProcessService;
    /**
     * Servicio para gesti&oacute;n de parametros.
     */
    @Autowired
    private ParameterService parameterService;
    /**
     * Servicio para gesti&oacute;n cargas.
     */
    @Autowired
    private LoadService loadService;

    /**
     * M&eacute;todo por defecto que permite la recepci&oacute;n de un archivo.
     * 
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = "cargar-archivo", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response) {
        LOGGER.info("Ejecutando metodo [ onDisplay load ]");
        ModelAndView modelAndView = new ModelAndView("cargar-archivo");
        LoadProcess loadProcess = new LoadProcess();
        loadProcess.setLoadType("");
        loadProcess.setInitDate(new Date());
        modelAndView.addObject("loadProcess", loadProcess);

        try {
            modelAndView.addObject("loadTypeList",
                this.valueListService.getValueList().getLoadTypes());
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg",
                "Error al realizar la consulta. " +
                "Por favor comuniquese con el administrador");
        } finally {
            LOGGER.info("Finaliza la invocacion de [ init].");
        }

        return modelAndView;
    }

    /**
     * M&eacute;todo para listar procesos de carga actuales por Rango.
     *
     * @param request
     * @param response
     * @param startDate
     * @param finalDate
     *
     * @return
     */
    @RequestMapping(value = "detalle-rango", method = RequestMethod.GET)
    public ModelAndView rangeDetail(HttpServletRequest request,
        HttpServletResponse response,
        @RequestParam(name = "fechaInicio") Date startDate,
        @RequestParam(name = "fechaFin") Date finalDate
    ) {
        ModelAndView modelAndView = new ModelAndView("info-cargue");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(finalDate);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date endDate = calendar.getTime();
        String fileId = Util.likeValue(request.getParameter("file"));
        Session session = (Session) request.getSession().getAttribute(
            SESSION_USER);
        User userSession = session.getUser();
        String ccfCode = Util.nullValue(userSession.getCcf().getCode());
        if (userSession.isAdmin()) {
            modelAndView.addObject("isAdmin", 'y');
            ccfCode = "%";
        }

        try {

            modelAndView.addObject("loadProcessList",
                this.loadProcessService.findByDateAndFile(startDate,
                    endDate, fileId, ccfCode));

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg",
                "Error al realizar la consulta. " +
                "Por favor comuniquese con el administrador");
        } finally {
            LOGGER.info("Finaliza la invocacion de [ listProcess ].");
        }
        init(modelAndView);
        return modelAndView;

    }

    /**
     * M&eacute;todo para consultar cargues.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "consulta-cargas", method = RequestMethod.GET)
    public ModelAndView loadDetail(HttpServletRequest request,
        HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView("info-cargue");

        init(modelAndView);
        return modelAndView;

    }

    /**
     * M&eacute;todo para detalle cargues.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "detalle-cargue", method = RequestMethod.GET)
    public ModelAndView details(HttpServletRequest request,
        HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView("detalle-cargue");

        try {
            Long id = Long.parseLong(request.getParameter("id"));
            LoadProcess loadProcess = loadProcessService.findById(id);
            modelAndView.addObject("loadProcess", loadProcess);
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg",
                "Error al realizar la consulta. " +
                "Por favor comuniquese con el administrador");
        } finally {
            LOGGER.info("Finaliza la invocacion de [ listProcess ].");
        }
        return modelAndView;

    }

    /**
     * M&eacute;todo para listar procesos de carga actuales por periodo.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "detalle-periodo", method = RequestMethod.GET)
    public ModelAndView periodDetail(HttpServletRequest request,
        HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView("info-cargue");
        Session session = (Session) request.getSession().
            getAttribute(SESSION_USER);
        User userSession = session.getUser();
        String ccfCode = Util.nullValue(userSession.getCcf().getCode());
        if (userSession.isAdmin()) {
            modelAndView.addObject("isAdmin", 'y');
            ccfCode = "%";
        }

        try {
            String fileId = Util.nullValue(request.getParameter("file2"));
            int year = Integer.parseInt(request.getParameter("anio"));
            String month = Util.nullValue(request.getParameter("mes"));
            LOGGER.info(
                "*** file" + fileId + "***** a�o" + year + "*****" + month);

            modelAndView.addObject("loadProcessList", this.loadProcessService.
                findByperiodAndFile(year, month, fileId, ccfCode));
            modelAndView.addObject("tab", "tabPeriodo");
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg",
                "Error en la consulta.Comuniquese con el administrador");
        } finally {
            LOGGER.info("Finaliza la invocacion de [ listProcess ].");
        }
        init(modelAndView);
        return modelAndView;

    }

    /**
     * M&eacute;todo que envia el archivo al servidor y dispara inicio de flujo de
     * carga.
     *
     * @param request
     * @param response
     * @param loadProcess
     *
     * @return
     */
    @RequestMapping(value = "generar-carga", method = RequestMethod.POST)
    public ModelAndView save(HttpServletRequest request,
        HttpServletResponse response,
        @ModelAttribute("loadProcess") LoadProcess loadProcess
    ) {
        ModelAndView modelAndView;
        modelAndView = onDisplay(request, response);
        Session session = (Session) request.getSession().
            getAttribute(SESSION_USER);
        User userSession = session.getUser();
        try {
            Parameter loadPathParameter =
                parameterService.findByName("ARCHIVOS_CARGA_PATH");

            loadProcess.setConsultant(userSession);
            loadProcess.setNotifyEmail(userSession.getEmail());
            loadProcess.setSendType("web");
            loadProcess.setNewFileRoot(loadPathParameter.getValue());
            if (loadProcess.getFile() != null) {
                MultipartFile multFile = ((MultipartFile) loadProcess.getFile());
                loadProcess.setDetail(LOAD_SUCCESS_MESSAGE);

                FileUploader uploader = new FileUploader();
                uploader.sendFile(multFile, loadProcess, new HashMap<>());
                
                if (loadProcess.getFileSended() == true) {
                    loadProcess.setStatus(ProcessStatus.ARCHIVO_CARGADO);
                    this.loadService.createProcess(loadProcess);
                    modelAndView = new ModelAndView("carga-archivo-resultado");
                    modelAndView.addObject("loadProcess", loadProcess);
                } else {
                    modelAndView.addObject("msgType", FAILED_ALERT);
                    modelAndView.addObject("msg", loadProcess.getErrorCode().
                        getMessage());
                }
            }
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg",
                "Error en la consulta.Comuniquese con el administrador");
        } finally {
            LOGGER.info("Finaliza generar carga");
        }
        return modelAndView;
    }

    /**
     * M&eacute;todo que genera el archivo con errores para su descarga.
     * 
     * @param request
     * @param response
     * @param type
     * @param loadId
     * @throws IOException 
     */
    @RequestMapping(value = "descargar-errores", method = RequestMethod.GET)
    public void errorsDownload(HttpServletRequest request,
        HttpServletResponse response,
        @RequestParam(name = "type") String type,
        @RequestParam(name = "loadId") Long loadId) throws IOException {

        File file = null;

        LoadProcess loadProcess = this.loadProcessService.findById(loadId);
        if ("estructura".equalsIgnoreCase(type)) {
            file = new File(loadProcess.getNewFileRoot() + 
                loadProcess.getNewFilePath() + 
                loadProcess.getStructureErrorFileName());
        }
        if ("negocio".equalsIgnoreCase(type)) {
            file = new File(loadProcess.getNewFileRoot() + 
                loadProcess.getNewFilePath() +
                loadProcess.getBussinessErrorFileName());
        }

        if (file != null) {
            String mimeType = URLConnection.
                guessContentTypeFromName(file.getName());
            if (mimeType == null) {
                LOGGER.error("mimetype is not detectable, will take default");
                mimeType = "application/octet-stream";
            }
            
            response.setContentType(mimeType);
            response.setHeader("Content-Disposition", "inline; filename=\"" + 
                file.getName() + "\"");

            LOGGER.info("Size: " + file.length());
            LOGGER.info("Size: " + ((int) file.length()));
            
            response.setContentLength((int) file.length());
            try (FileInputStream fis = new FileInputStream(file)) {
                InputStream inputStream = new BufferedInputStream(fis);
                FileCopyUtils.copy(inputStream, response.getOutputStream());
            }
        }
    }

    /**
     * M&eacute;todo para instanciar las fechas.
     * 
     * @param modelAndView 
     */
    private void init(ModelAndView modelAndView) {
        List<Integer> years = new ArrayList<>();
        Calendar c1 = Calendar.getInstance();
        for (int i = 2014; i <= c1.get(Calendar.YEAR); i++) {
            years.add(i);
        }
        try {

            modelAndView.addObject("fileType", this.valueListService
                .getValueList().getLoadTypeList());

            modelAndView.addObject("anios", years);
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msgType", FAILED_ALERT);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("finally ");
        }

    }
}
