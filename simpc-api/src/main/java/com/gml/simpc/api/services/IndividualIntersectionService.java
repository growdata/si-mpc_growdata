/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IndividualIntersectionService.java
 * Created on: 2016/11/15, 10:46:55 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.dto.BasicInformationDto;
import com.gml.simpc.api.dto.DependentDto;
import com.gml.simpc.api.dto.EducationLevelDto;
import com.gml.simpc.api.dto.InformalEducationDto;
import com.gml.simpc.api.dto.IntermediationDto;
import com.gml.simpc.api.dto.LanguageDto;
import com.gml.simpc.api.dto.OtherKnowledgeDto;
import com.gml.simpc.api.dto.PilaDto;
import com.gml.simpc.api.dto.PilaEmploymentServiceDto;
import com.gml.simpc.api.dto.ProfitDto;
import com.gml.simpc.api.dto.ProgramDto;
import com.gml.simpc.api.dto.WorkExperienceDto;
import java.util.List;

/**
 * Servicio para obtener los registros de la tabla MD_F_PILA
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public interface IndividualIntersectionService {

    /**
     * Obtiene todos los registros de pila por tipo y n&uacute;mero de
     * documento.
     *
     * @return
     */
    List<PilaDto> getPilaByDocumentAndNumberType(String documentType,
        String numberIdentification);

    /**
     * Obtiene todos los registros de dependientes por tipo y n&uacute;mero de
     * documento.
     *
     * @return
     */
    List<DependentDto> getDependentsByDocumentAndNumberType(String documentType,
        String numberIdentification);

    /**
     * Obtiene todos los registros de orientaci&oacute;n y capacitaci&oacute;n
     * por tipo y n&uacute;mero de documento.
     *
     * @return
     */
    List<ProgramDto> getTrainingsByDocumentAndNumberType(String documentType,
        String numberIdentification);

    /**
     * Obtiene todos los registros de pila por tipo y n&uacute;mero de
     * documento para usuarios del servicio publico de empleo.
     *
     * @return
     */
    List<PilaEmploymentServiceDto>
        getPilaByDocumentAndNumberTypePublicEmploymentService(
            String documentType,
            String numberIdentification);

    /**
     * Obtiene todos los registros de fosfec por tipo y n&uacute;mero de
     * documento.
     *
     * @return
     */
    List<ProfitDto> getFosfecByDocumentAndNumberType(String documentType,
        String numberIdentification);

    /**
     * Obtiene todos los registros de intermediacion por tipo y n&uacute;mero
     * de documento.
     *
     * @return
     */
    List<IntermediationDto> getIntermediationByDocumentAndNumberType(
        String documentType, String numberIdentification);

    /**
     * Obtiene todos los registros de basicInformation por tipo y n&uacute;mero
     * de documento.
     *
     * @return
     */
    List<BasicInformationDto> getBasicInformationByDocumentAndNumberType(
        String documentType, String numberIdentification);

    /**
     * Obtiene todos los registros de educationLevel por tipo y n&uacute;mero
     * de documento.
     *
     * @return
     */
    List<EducationLevelDto> getEducationLevelByDocumentAndNumberType(
        String documentType, String numberIdentification);

    /**
     * Obtiene todos los registros de workExperience por tipo y n&uacute;mero
     * de documento.
     *
     * @return
     */
    List<WorkExperienceDto> getWorkExperienceByDocumentAndNumberType(
        String documentType, String numberIdentification);

    /**
     * Obtiene todos los registros de InformalEducationDto por tipo y
     * n&uacute;mero de documento.
     *
     * @return
     */
    List<InformalEducationDto> getInformalEducationByDocumentAndNumberType(
        String documentType, String numberIdentification);

    /**
     * Obtiene todos los registros de LanguageDto por tipo y n&uacute;mero
     * de documento.
     *
     * @return
     */
    List<LanguageDto> getLanguageByDocumentAndNumberType(
        String documentType, String numberIdentification);

    /**
     * Obtiene todos los registros de LanguageDto por tipo y n&uacute;mero
     * de documento.
     *
     * @return
     */
    List<OtherKnowledgeDto> getOtherKnowledgeByDocumentAndNumberType(
        String documentType, String numberIdentification);
}
