/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Functionality.java
 * Created on: 2016/10/21, 10:57:53 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity.valuelist;

import com.gml.simpc.api.enums.CCFMode;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad que representa la caja de compensaci&ocute;n
 *
 * @author <a href="mailto:julianm@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
@Table(name = "FUNDACIONES")
@Entity
public class Foundation implements Serializable {

    /**
     * ID de la entidad.
     */
    @Id
    @Column(name = "CODIGO", nullable = false)
    private String code;
    /**
     * NIT de la caja de compensaci&ocute;n
     */
    @Column(name = "NIT")
    private String nit;

    /**
     * Nombre de la entidad
     */
    @Column(name = "NOMBRE")
    private String name;
    
    /**
     * Nombre corto de la entidad
     */
    @Column(name = "NOMBRE_CORTO")
    private String shortName;
    /**
     * Indica si es CCF o no la entidad.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "ES_CCF", columnDefinition = "char(1) default 'V'",
        nullable = false)
    private CCFMode isCCF = CCFMode.V;

    /**
     * Constructor por defecto.
     */
    public Foundation() {
        this.code = null;
    }

    /**
     * Constructor por nombre.
     *
     * @param foundationName
     */
    public Foundation(String foundationName) {
        this.name = foundationName;
    }
    
    /**
     * Constructor por nombre.
     *
     * @param foundationName
     */
    public Foundation(String code, String name) {
        this.code = code;
        this.name = name;
    }
    
      /**
     * Constructor por nombre.
     *
     * @param foundationName
     */
    public Foundation(String code, String name, String nit) {
        this.code = code;
        this.name = name;
        this.nit=nit;
    }
    

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public CCFMode getIsCCF() {
        return isCCF;
    }

    public void setIsCCF(CCFMode isCCF) {
        this.isCCF = isCCF;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.code);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Foundation other = (Foundation) obj;

        return Objects.equals(this.name, other.name);
    }
}
