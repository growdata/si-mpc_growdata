/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: TrainingType.java
 * Created on: 2017/01/23, 02:41:11 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity.valuelist;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad que representa un tipo de capacitaci&oacute;n.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Table(name = "TIPOS_CAPACITACION")
@Entity
public class TrainingType implements Serializable {

    /**
     * Identificador de la entidad.
     */
    @Id
    @Column(name = "ID", length = 20)
    private Long id;
    /**
     * C&oacute;digo.
     */
    @Column(name = "CODIGO", length = 20)
    private String code;

    /**
     * Descripci&oacute;n
     */
    @Column(name = "VALOR", length = 250)
    private String value;

    /**
     * Constructor por defecto.
     */
    public TrainingType() {
        this.id = null;
    }
    
    /**
     * Short Nname
     */
    @Column(name = "NOMBRE", length = 20)
    private String name;

    /**
     * Constructor por nombre.
     *
     * @param name
     */
    public TrainingType(String name) {
        this.code = name;
    }

    /**
     * Constructor por codigoy valor.
     *
     * @param code
     * @param value
     *
     */
    public TrainingType(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
