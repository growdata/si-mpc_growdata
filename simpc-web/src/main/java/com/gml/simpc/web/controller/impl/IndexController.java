/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 **
 * Document: IndexController.java
 * Created on: 2016/10/27, 05:07:24 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.gml.simpc.api.entity.Functionality;
import com.gml.simpc.api.entity.Session;
import com.gml.simpc.api.entity.exeption.code.ErrorCode;
import com.gml.simpc.api.services.FunctionalityService;
import com.gml.simpc.web.controller.ControllerDefinition;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import static com.gml.simpc.api.utilities.Util.SESSION_PERMISSIONS;
import static com.gml.simpc.api.utilities.Util.SESSION_USER;

/**
 * Controlador que administra la pantalla del inicio.
 *
 * @author <a href="mailto:julianm@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
@Controller
public class IndexController implements ControllerDefinition {

    /**
     * Logger de la clase <code>IndexController</code>
     */
    private static final Logger LOGGER = Logger.getLogger(IndexController.class);
    /**
     * Servicio para obtener funcionalidades.
     */
    @Autowired
    private FunctionalityService functionalityService;

    /**
     * M&eacute;todo que se ejecuta al abrir la ventana de index.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response) {
        LOGGER.info("Ejecutando metodo [ onDisplay IndexController ]");

        try {
            Session session = (Session) request.getSession().
                getAttribute(SESSION_USER);

            ArrayList<Functionality> functionalities  =
                (ArrayList<Functionality>) this.functionalityService.
                getFunctionalitiesByUsername(session.getUser().
                    getEmail());

            request.getSession().setAttribute(SESSION_PERMISSIONS,
                functionalities);

            ModelAndView modelAndView = new ModelAndView("index",
                "funtionalities", functionalities);

            String nameUser = session.getUser().getFirstName() + " " +
                session.getUser().getFirstSurname();

            modelAndView.addObject("userNames", nameUser);

            return modelAndView;
        } catch (Exception e) {
            LOGGER.info("Error", e);

            return new ModelAndView("index", "msg",
                ErrorCode.ERR_001_UNKNOW.getMessage());
        }
    }
}
