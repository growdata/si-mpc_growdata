/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IntersectionRepository.java
 * Created on: 2017/02/07, 04:46:02 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository;

import com.gml.simpc.api.entity.Intersection;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Repositorio de cruces.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface IntersectionRepository 
    extends JpaRepository<Intersection, Long>  {
    
    /**
     * Busca las cargas realizadas en un rango de fechas.
     *
     * @param start
     * @param end
     *
     * @return
     */
    @Query(value = "SELECT * FROM cruces c INNER JOIN usuarios u on u.id=c.usuario " +
        "WHERE FECHA_FIN BETWEEN ?1 and ?2   AND tipo_cruce  LIKE ?3 " +
        "AND u.caja_compensacion LIKE ?4",
        nativeQuery = true)
    List<Intersection> findByEndDateRange(Date startDate,Date endDate,
        String searchType,String ccf );

}
