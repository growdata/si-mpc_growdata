<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <style>
            input[type=number]::-webkit-outer-spin-button,
            input[type=number]::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            input[type=number] {
                -moz-appearance:textfield;
            }
        </style>
    </head>
    <body>
        <c:if test="${not empty msg}">
            <div class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        <div class="col-sm-12">
            <h1>Datos del parámetro</h1>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label for="name" class=" control-label">Nombre</label>
                <p class="form-control-static">${parameter.name}</p>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label for="description" class=" control-label">Descripci&oacute;n</label>
                <p class="form-control-static">${parameter.description}</p>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label for="value" class=" control-label">Valor</label>
                <p class="form-control-static">${parameter.value}</p>
            </div>
        </div>
        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/jquery.ui.widget.js"></script>
        <script src="resources/js/jquery.iframe-transport.js"></script>
        <script src="resources/js/jquery.fileupload.js"></script>
        <script>
            $(document).ready(function () {
                $('#institutions').DataTable({
                    "language": {
                        "url": "resources/js/Spanish.json"
                    },
                    "bFilter": false,
                    "bInfo": false
                });

                $('#fechaVencimiento').datepicker();

                loadStates();
                <c:if test="${not empty internalMsg}">
                     $('#creationModal').modal('show');
                </c:if>
            });
        </script>                                           
    </body>
</html>
