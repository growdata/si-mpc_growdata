/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: TrainingPreparedStatementSetter.java
 * Created on: 2017/01/20, 12:16:58 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.writer;

import com.gml.simpc.api.dto.WorkExperienceDto;
import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.springframework.batch.item.database.ItemPreparedStatementSetter;

/**
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica
 * Jim&eacute;nez Rozo</a>
 */
public class WorkExperiencePreparedStatementSetter
    implements ItemPreparedStatementSetter<WorkExperienceDto> {

    @Override
    public void setValues(WorkExperienceDto otherKnowledgeDto,
        PreparedStatement ps)
        throws SQLException {
        ThreadLocalJob.setProcesed();
        JobResultDto jobResult = ThreadLocalJob.getResult();
        int line = jobResult.getProcesedLines();
        ps.setString(1, otherKnowledgeDto.getDocumentType());
        ps.setString(2, otherKnowledgeDto.getNumberIdentification());
        ps.setString(3, otherKnowledgeDto.getWorkExperienceType());
        ps.setString(4, otherKnowledgeDto.getProduct());
        ps.setString(5, otherKnowledgeDto.getPeopleAmount());
        ps.setString(6, otherKnowledgeDto.getCompanyName());
        ps.setString(7, otherKnowledgeDto.getSector());
        ps.setString(8, otherKnowledgeDto.getCompanyPhone());
        ps.setString(9, otherKnowledgeDto.getCountry());
        ps.setString(10, otherKnowledgeDto.getPosition());
        ps.setString(11, otherKnowledgeDto.getEquivalentPosition());
        ps.setString(12, otherKnowledgeDto.getAdmisionDate());
        ps.setString(13, otherKnowledgeDto.getCurrentlyWorking());
        ps.setString(14, otherKnowledgeDto.getRetirementDate());
        ps.setString(15, Integer.toString(line));
    }

}
