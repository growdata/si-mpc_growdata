/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: LoadProcessDto.java
 * Created on: 2017/02/08, 09:58:17 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

import java.io.Serializable;

/**
 *
 * @author angie
 */
public class LoadProcessDto implements Serializable {

    private Long id;

    /**
     * Tipo de carga
     */
    private String loadType;

    /**
     * Tipo de carga
     */
    private String sendType;

    /**
     * Nombre original del archivo cargado
     */
    private String originalFileName;

    /**
     * Ruta del archivo en disco
     */
    private String newFileRoot;

    /**
     * Ruta del archivo en disco
     */
    private String newFilePath;

    /**
     * Nombre del archivo en disco
     */
    private String newFileName;

    /**
     * Nombre del archivo de errores de estructura en disco
     */
    private String structureErrorFileName;

    /**
     * Nombre del archivo de errores de negocio en disco
     */
    private String bussinessErrorFileName;

    /**
     * Tama�o del archivo
     */
    private Long size;

    /**
     * Consultor que realiza la carga
     */
    private String consultant;

    /**
     * Tipo de escritura de la informacion
     */
    private String writeType;

    /**
     * Usuario que ejecuta la carga
     */
    private String notifyEmail;

    public LoadProcessDto() {
    }

    public LoadProcessDto(Long id, String loadType, String sendType,
        String originalFileName, String newFileRoot, String newFilePath,
        String newFileName, String structureErrorFileName,
        String bussinessErrorFileName, Long size, String consultant,
        String writeType, String notifyEmail) {
        this.id = id;
        this.loadType = loadType;
        this.sendType = sendType;
        this.originalFileName = originalFileName;
        this.newFileRoot = newFileRoot;
        this.newFilePath = newFilePath;
        this.newFileName = newFileName;
        this.structureErrorFileName = structureErrorFileName;
        this.bussinessErrorFileName = bussinessErrorFileName;
        this.size = size;
        this.consultant = consultant;
        this.writeType = writeType;
        this.notifyEmail = notifyEmail;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLoadType() {
        return loadType;
    }

    public void setLoadType(String loadType) {
        this.loadType = loadType;
    }

    public String getSendType() {
        return sendType;
    }

    public void setSendType(String sendType) {
        this.sendType = sendType;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public String getNewFileRoot() {
        return newFileRoot;
    }

    public void setNewFileRoot(String newFileRoot) {
        this.newFileRoot = newFileRoot;
    }

    public String getNewFilePath() {
        return newFilePath;
    }

    public void setNewFilePath(String newFilePath) {
        this.newFilePath = newFilePath;
    }

    public String getNewFileName() {
        return newFileName;
    }

    public void setNewFileName(String newFileName) {
        this.newFileName = newFileName;
    }

    public String getStructureErrorFileName() {
        return structureErrorFileName;
    }

    public void setStructureErrorFileName(String structureErrorFileName) {
        this.structureErrorFileName = structureErrorFileName;
    }

    public String getBussinessErrorFileName() {
        return bussinessErrorFileName;
    }

    public void setBussinessErrorFileName(String bussinessErrorFileName) {
        this.bussinessErrorFileName = bussinessErrorFileName;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getConsultant() {
        return consultant;
    }

    public void setConsultant(String consultant) {
        this.consultant = consultant;
    }

    public String getWriteType() {
        return writeType;
    }

    public void setWriteType(String writeType) {
        this.writeType = writeType;
    }

    public String getNotifyEmail() {
        return notifyEmail;
    }

    public void setNotifyEmail(String notifyEmail) {
        this.notifyEmail = notifyEmail;
    }

    @Override
    public String toString() {
        return "LoadProcessDao{" + "id=" + id + ", loadType=" + loadType +
            ", sendType=" + sendType + ", originalFileName=" + originalFileName +
            ", newFileRoot=" + newFileRoot + ", newFilePath=" + newFilePath +
            ", newFileName=" + newFileName + ", structureErrorFileName=" +
            structureErrorFileName + ", bussinessErrorFileName=" +
            bussinessErrorFileName + ", size=" + size + ", consultant=" +
            consultant + ", writeType=" + writeType + ", notifyEmail=" +
            notifyEmail + '}';
    }

}
