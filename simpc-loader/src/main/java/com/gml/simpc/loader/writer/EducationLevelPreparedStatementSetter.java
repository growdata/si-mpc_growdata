/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: TrainingPreparedStatementSetter.java
 * Created on: 2017/01/20, 12:16:58 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.writer;

import com.gml.simpc.api.dto.EducationLevelDto;
import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.springframework.batch.item.database.ItemPreparedStatementSetter;

/**
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica Jim&eacute;nez Rozo</a>
 */
public class EducationLevelPreparedStatementSetter 
        implements ItemPreparedStatementSetter<EducationLevelDto> {
 
    @Override
    public void setValues(EducationLevelDto educationLevelDto, 
        PreparedStatement ps) 
            throws SQLException {
        ThreadLocalJob.setProcesed();
        JobResultDto jobResult = ThreadLocalJob.getResult();
        int line = jobResult.getProcesedLines();
        ps.setString(1, educationLevelDto.getDocumentType());
        ps.setString(2, educationLevelDto.getNumberIdentification());
        ps.setString(3, educationLevelDto.getEducationLevel());
        ps.setString(4, educationLevelDto.getPerformanceArea());
        ps.setString(5, educationLevelDto.getNucleusKnowledge());
        ps.setString(6, educationLevelDto.getDegreeObtained());
        ps.setString(7, educationLevelDto.getDegreeHomologated());
        ps.setString(8, educationLevelDto.getDegreeCountry());
        ps.setString(9, educationLevelDto.getUniversity());
        ps.setString(10, educationLevelDto.getStatus());
        ps.setString(11, educationLevelDto.getFinishDate());
        ps.setString(12, educationLevelDto.getProfessionalCard());
        ps.setString(13, educationLevelDto.getProfessionalCardNumber());
        ps.setString(14, educationLevelDto.getProfessionalCardExpeditionDate());
        ps.setString(15, educationLevelDto.getObservations());
        ps.setString(16, educationLevelDto.getPractice());
        ps.setString(17, Integer.toString(line));
    }
 
}
