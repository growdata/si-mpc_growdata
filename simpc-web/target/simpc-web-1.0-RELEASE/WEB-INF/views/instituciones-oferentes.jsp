<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster.bundle.min.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster-sideTip-shadow.min.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
    </head>

    <body>
        <c:if test="${not empty msg}">
            <div id="alert-${msgType}" class="alert alert-${msgType} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>


        <h1>Administraci&oacute;n de Instituciones</h1>
        <div class="panel panel-default">
            <div class="panel-heading"><h2>B&uacute;squeda</h2></div>
            <div class="panel-body">
                <legend>
                    <form:form method="POST" action="/masterdata/buscarInstituciones.htm" modelAttribute="searchInstitution"> 
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="tipoInsitucion" class=" control-label">Tipo de Instituci&oacute;n</label>
                                <form:select class="form-control" id="tipoInsitucionC" path="type.id">
                                    <option value="">Seleccione...</option>
                                    <c:if test="${not empty institutionTypes}">
                                        <c:forEach var="item" items="${institutionTypes}">
                                            <option label="${item.name}" value="${item.id}">${item.name}</option>
                                        </c:forEach>
                                    </c:if>
                                </form:select>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="nombreInstitucion" class="control-label">Nombre de la Instituci&oacute;n</label>
                                <form:input type="text" class="form-control" id="nombreInstitucionC" 
                                            placeholder="Nombre de la Instituci�n" maxlength="50"
                                            path="institutionName" name="institutionName"/>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="estado" class="control-label">Estado de Aprobaci�n</label>
                                <form:select class="form-control" id="estadoAprobacionC" path="approvalStatus">
                                    <option value="">Seleccione...</option>
                                    <c:if test="${not empty approvalInstitutionStatusList}">
                                        <c:forEach var="item" items="${approvalInstitutionStatusList}">
                                            <option value="${item}">${item.label}</option>
                                        </c:forEach>
                                    </c:if>
                                </form:select>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-offset-4 col-sm-8" style="text-align:right">
                            <button id="nuevoBtn" type="button" class="btn btn-success" onclick="alistaForm()">Nuevo</button>
                            <button id="buscarBtn" type="submit" class="btn btn-success">Buscar</button>
                        </div>
                    </form:form>
                </legend>
                <div class="clearfix"></div>
            </div>
        </div>

        <br/>
        <div class="panel panel-default">
            <div class="panel-heading"><h2>Detalle Consulta Instituciones Oferentes</h2></div>
            <div class="panel-body">
                <div class=" table-responsive">
                    <table id="institutions" class="table table-striped table-bordered" 
                           cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nombre de la Instituci&oacute;n</th>
                                <th>Tipo de Instituci&oacute;n</th>
                                <th>Estado de Activaci�n</th>
                                <th>Estado de Aprobaci�n</th>
                                <th style="width: 150px !important; text-align: center">Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Nombre de la Instituci&oacute;n</th>
                                <th>Tipo de Instituci&oacute;n</th>
                                <th>Estado de Activaci�n</th>
                                <th>Estado de Aprobaci�n</th>
                                <th style="width: 150px !important; text-align: center">Acciones</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:if test="${not empty institutionList}">
                                <c:forEach var="item" items="${institutionList}">
                                    <tr>
                                        <td>${item.institutionName}</td>
                                        <td>${item.type}</td>
                                        <td>${item.status.label}</td>
                                        <td>${item.approvalStatus.label}</td>
                                        <td>
                                <center>
                                    <div class="btn-group" role="group" >
                                        <table>
                                            <tr>
                                                <td>
                                                    <button type="button" class="btn btn-info btn-xs"
                                                            onclick="loadInstitutionDetail(${item.id})">
                                                        <i class="fa fa-id-card-o" aria-hidden="true"></i> 
                                                        Ver
                                                    </button>
                                                </td>
                                                <td>
                                                    <input type="hidden" name="id" value="${item.id}"/>
                                                   
                                                    <button id="editBtn${item.nit}" type="submit" class="btn btn-warning btn-xs"
                                                            onclick="editInstitution(${item.id})">
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> 
                                                        Editar
                                                    </button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </center>
                                </td>
                                </tr>
                            </c:forEach>
                        </c:if>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="modal fade" id="creationModal" role="dialog" 
             aria-labelledby="creationModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button onclick="resetForm()" type="button" class="close"data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="creationModalLabel">Registro de Instituciones</h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-sm-12">
                            <h1>Datos de la Instituci�n</h1>
                            <br/> 
                        </div>
                        <div class="clearfix"></div>
                        <c:if test="${not empty internalMsg}">
                            <div id="internal" class="alert alert-${msgType} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-label="Close">
                                    <span aria-hidden="true">�</span>
                                </button>
                                <strong>${internalMsg}</strong>
                            </div>
                        </c:if>
                        <form:form id="creationInstitutionForm" modelAttribute="creationInstitution" 
                                   enctype="multipart/form-data" method="post" action="/masterdata/guardarInstitucion.htm">

                            <form:input type="hidden" path="id" name="id" id="id" value="${creationInstitution.id}"/>
                            <input type="hidden" id="tipoFormulario" value="${tipoformulario}"/>
                            <input type="hidden" id="certAntigua" name="certAntigua" value="${oldCert}"/>
                        
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="tipoInsitucion" class="control-label">Tipo de Instituci&oacute;n(*)</label>
                                    <form:select class="form-control istitutionOff" id="typeid" path="type.id" required="required">
                                        <option value="">Seleccione...</option>
                                        <c:if test="${not empty institutionTypes}">
                                            <c:forEach var="item" items="${institutionTypes}">
                                                <option label="${item.name}" value="${item.id}">
                                                    ${item.name}
                                                </option>
                                            </c:forEach>
                                        </c:if>
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nombreInstitucion" class="control-label">Nombre de la Instituci&oacute;n (*)</label>
                                    <form:input type="text" class="form-control istitutionOff" id="institutionName" maxlength="50"
                                                placeholder="Nombre de la Instituci�n" required="required"
                                                path="institutionName"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="tipoDocumento" class=" control-label">Tipo de Documento(*)</label>
                                    <form:select onchange="activacionDigito()" class="form-control istitutionOff" 
                                                 id="documentTypeid" path="documentType.id" required="required">
                                        <option value="">Seleccione...</option>
                                            <c:if test="${not empty documentTypes}">
                                                <c:forEach var="item" items="${documentTypes}">
                                                    <c:if test="${item.id == '1' || item.id == '2' || item.id == '3' || item.id == '4' || item.id == '5'}">
                                                        <option label="${item.name}" value="${item.id}">${item.name}</option>
                                                    </c:if>
                                                </c:forEach>
                                            </c:if>   
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nit" class="control-label">N�mero de Documento(*)</label>
                                    <form:input data-rule-intrule="true" type="text" class="form-control istitutionOff" id="nit" 
                                                placeholder="N�mero de Documento" required="required" 
                                                onkeypress="return isNumberKey(event)"
                                                path="nit" maxlength="10" minlength="8" />
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 digito">
                                <div class="form-group">
                                    <label for="dv" class="control-label">D&iacute;gito de Verificaci&oacute;n</label>
                                    <form:input disable="true" type="text" class="form-control disable istitutionOff" id="dv" 
                                                placeholder="D�gito de Verificaci�n" path="dv" 
                                                onkeypress="return isNumberKey(event)"
                                                maxlength="1" disabled="disabled" />
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="origen" class=" control-label">Origen(*)</label>
                                    <form:select class="form-control istitutionOff" id="origin" path="origin" required="required">
                                        <option class="selectedOn" value="">Seleccione...</option>
                                        <c:if test="${not empty originList}">
                                            <c:forEach var="item" items="${originList}">
                                                <c:if test="${item == creationInstitution.origin}">
                                                    <option class="selectedOff" label="${item.label}" value="${item}" selected="selected">
                                                        ${item.label}
                                                    </option>
                                                    
                                                </c:if>
                                                <c:if test="${item != creationInstitution.origin}">
                                                    <option label="${item.label}" value="${item}">
                                                        ${item.label}
                                                    </option>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="naturalezaLegal" class=" control-label">Naturaleza Jur&iacute;dica(*)</label>
                                    <form:select class="form-control istitutionOff" id="legalNature" 
                                                 path="legalNature" required="required">
                                        <option class="selectedOn" value="">Seleccione...</option>
                                        <c:if test="${not empty legalNatureList}">
                                            <c:forEach var="item" items="${legalNatureList}">
                                                <c:if test="${item == creationInstitution.legalNature}">
                                                    <option class="selectedOff" label="${item.label}" value="${item}" selected="selected">
                                                        ${item.label}
                                                    </option>
                                                </c:if>
                                                <c:if test="${item != creationInstitution.legalNature}">
                                                    <option label="${item.label}" value="${item}">
                                                        ${item.label}
                                                    </option>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="telefonoContacto" class="control-label">Tel&eacute;fono de Contacto(*)</label>
                                    <form:input data-rule-celrule="true" type="text" class="form-control istitutionOff" id="contactPhone" required="required"
                                                placeholder="Tel�fono Contacto" onkeypress="return isNumberKey(event)"
                                                path="contactPhone" minlength="7" maxlength="10"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="emailContacto" class="control-label">E-Mail Contacto(*)</label>
                                    <form:input data-rule-emailtld="true" class="form-control istitutionOff" id="contactEmail" 
                                                placeholder="E-Mail Contacto" required="required"
                                                path="contactEmail" autocomplete="off" maxlength="50"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                    
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="tipoCertificado" class=" control-label">Tipo de Certificaci�n Emitida(*)</label>
                                    <form:select class="form-control istitutionOff" 
                                                 id="typeQualityCertificateid" path="typeQualityCertificate.id" required="required">
                                        <option value="">Seleccione...</option>
                                            <c:if test="${not empty typeQualityCertificate}">
                                                <c:forEach var="item" items="${typeQualityCertificate}">
                                                        <option label="${item.code}" value="${item.id}">${item.code}</option>
                                                </c:forEach>
                                            </c:if>   
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                    
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <table>
                                        <label for="certificadoCalidad" class="control-label">Certificaci&oacute;n de Calidad</label>
                                        <tr>
                                            <td>
                                                <form:input onchange="fechaObligatoria()" type="file" id="certificadoCalidad"
                                                            class="form-control certificadoObligatorio istitutionOff"  name="file"
                                                            path="file" accept="application/pdf" required="required"/>
                                            </td>
                                            <td class="downloadCert">                                                
                                                <button type="button" class="btn btn-success"
                                                        onclick="fileForm.submit()">Descargar
                                                </button>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 fechaCert">
                                <div class="form-group">
                                    <label for="fechaVencimiento" class="control-label">Fecha de Vencimiento de la Certificaci&oacute;n</label>
                                    <form:input id="qualityCertificateExpiration" class="form-control datepicker istitutionOff" 
                                                data-date-format="dd-mm-yyyy" path="qualityCertificateExpiration"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 desactivarEstado">
                                <div class="form-group">
                                    <label for="naturalezaLegal" class=" control-label">Estado(*)</label>
                                    <form:select class="form-control" id="status" path="status" required="required" name="status">
                                        <c:if test="${not empty institutionStatusList}">
                                            <c:forEach var="item" items="${institutionStatusList}">
                                                <option  label="${item.label}" value="${item}">
                                                    ${item.label}
                                                </option>
                                            </c:forEach>
                                        </c:if>
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="hiddendiv">        
                                <div class="col-sm-12">
                                    <h1>Datos de la sede</h1>
                                    <br/> 
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nombreSede" class="control-label">Nombre de la Sede(*)</label>
                                        <form:input type="text" class="form-control" id="headquartername" 
                                                    placeholder="Nombre de la Sede" required="required"
                                                    path="headquarter.name" maxlength="50"/>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="direccionSede" class="control-label">Direcci&oacute;n de la Sede(*)</label>
                                        <form:input type="text" class="form-control" id="headquarteraddress" 
                                                    placeholder="Direcci�n de la sede" required="required"
                                                    path="headquarter.address" maxlength="200"/>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="departamento" class=" control-label">Departamento(*)</label>
                                        <form:select onchange="activaMunicipio()" class="form-control" id="headquarterstateid" 
                                                     path="headquarter.state.id" required="required">
                                            <option value="">Seleccione...</option>
                                        </form:select>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group activaMunicipio">
                                        <label for="municipio" class="control-label">Municipio(*)</label>
                                        <form:select class="form-control" id="headquartercityid" 
                                                     path="headquarter.city.id" required="required">
                                        </form:select>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <form:checkbox id="headquarterprincipal" class="control-label"
                                                       path="headquarter.principal" value="true"/>
                                        <label for="nombreSede" class="control-label">Es principal</label>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="modal-body msn">
                                <div class="form-group">
                                    <label for="message-text" class="form-control-label msnLabel">Causa de la modificaci�n(*)</label>
                                    <textarea class="form-control" id="message-text" name="message-text" required="required" minlength="1"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-12" style="text-align:right">
                                <br/>
                                <button id="saveButton" class="btn btn-success envioForm"
                                        data-btn-ok-label="Confirmar" data-btn-cancel-label="Cancelar"
                                        data-title="Est� seguro?" 
                                        data-toggle="confirmation" data-placement="top" 
                                        confirmation-callback>Guardar</button>
                                <button type="button" onclick="resetForm()" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                            </div>
                        </form:form>
                        <form method="post" action="/masterdata/descargarCertificado.htm" target="_blank" id="fileForm">
                            <input type="hidden" name="id" id="id2"/>
                        </form> 
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="detailModal" role="dialog" 
             aria-labelledby="creationModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="creationModalLabel">Detalle de Instituci&oacute;n</h4>
                    </div>

                    <div class="modal-body" style="height: 100vh">
                        <iframe id="detailArea" width="100%" height="100%" style="border: 0">
                        </iframe>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>    

        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/dataTables.buttons.min.js"></script>
        <script src="resources/js/buttons.flash.min.js"></script>
        <script src="resources/js/jszip.min.js"></script>
        <script src="resources/js/pdfmake.min.js"></script>
        <script src="resources/js/vfs_fonts.js"></script>
        <script src="resources/js/buttons.html5.min.js"></script>
        <script src="resources/js/buttons.print.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/jquery.ui.widget.js"></script>
        <script src="resources/js/jquery.iframe-transport.js"></script>
        <script src="resources/js/bootstrap-confirmation.js"></script>
        <script src="resources/js/jquery.fileupload.js"></script>
        <script src="resources/js/jquery.validate.min.js"></script>
        <script src="resources/js/tooltipster.bundle.js"></script>
        <script src="resources/js/custom-functionalities.js"></script>
        <script>

            $(document).ready(function () {
                             
                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Mi�rcoles", "Jueves", "Viernes", "S�bado", "Domingo"],
                    daysShort: ["Dom", "Lun", "Mar", "Mi�", "Jue", "Vie", "S�b", "Dom"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Hoy"
                };

                $('.datepicker').datepicker({
                    language: "es",
                    autoclose: true,
                    startDate: new Date()
                });

                $('#institutions').DataTable({
                    "language": {
                        "url": "resources/js/Spanish.json"
                    }, "initComplete": function (settings, json) {
                        $(".dt-buttons").each(function (index) {
                            console.log(index + ": " + $(this).text());
                            if ($(this).find('.exportar').length === 0) {
                                $(this).prepend('<label class="exportar">Exportar a:&nbsp;&nbsp;</label>');
                            }
                        });
                    },
                    "bFilter": false,
                    "bInfo": false,
                    dom: 'Blfrtip',
                    buttons: [
                        {
                            extend: 'csv',
                            fieldBoundary: '',
                            footer: false,
                             text: '<i class="fa fa-files-o"></i>',
                                                        titleAttr: 'CSV',
                            exportOptions: {
                                columns: [0, 1, 2, 3]
                            }
                        },
                        {
                            extend: 'excel',
                            footer: false,
                             text: '<i class="fa fa-files-o"></i>',
                                                        titleAttr: 'EXCEL',
                            exportOptions: {
                                columns: [0, 1, 2, 3]
                            }
                        },
                        {
                            extend: 'pdf',
                            footer: false,
                             text: '<i class="fa fa-files-o"></i>',
                                                        titleAttr: 'PDF',
                            exportOptions: {
                                columns: [0, 1, 2, 3]
                            }
                        },
                        {
                            extend: 'print',
                            footer: false,
                             text: '<i class="fa fa-files-o"></i>',
                                                        titleAttr: 'COPY',
                            exportOptions: {
                                columns: [0, 1, 2, 3]
                            }
                        }
                    ]
                });

                loadStates("headquarterstateid", "headquartercityid");
                loadCitiesByState("headquarterstateid", "headquartercityid");

                <c:if test="${not empty internalMsg}">
                        
                    $('.downloadCert').hide();

                    <c:if test="${not empty jsonInstitution}">
                        var json = jQuery.parseJSON('<c:out value="${jsonInstitution}" escapeXml="false"/>');
                        foundComponents(json, '');
                        loadCitiesJson();
                        foundComponent(json, '', 'headquarterstateid');
                        $(".istitutionOff").removeAttr("disabled");
                    </c:if>

                    if ($('#tipoFormulario').val() === "nuevo") {
                        $(".desactivarEstado").hide();
                        $(".msn").hide();
                        $('#id').val('');
                    } else {
                        $(".hiddendiv").hide();
                        $(".msn").show();
                    }

                    if ($("#documentTypeid").val() === '2' || $("#documentTypeid").val() === '6') {
                        $(".disable").attr("disabled", "disabled");
                        $(".disable").removeAttr("required");
                        $("#nit").attr("data-rule-intrule", "true");
                        $("#nit").attr("data-rule-alpharule", "false");
                        $("#nit").attr("onkeypress", "return isNumberKey(event)");
                    } else if($("#documentTypeid").val() === '5' || $("#documentTypeid").val() === '3'){
                        $(".disable").attr("disabled", "disabled");
                        $(".disable").removeAttr("required");
                        $("#nit").attr("data-rule-intrule", "false");
                        $("#nit").attr("data-rule-alpharule", "true");
                        $("#nit").removeAttr("onkeypress");
                    } else {
                        $(".disable").removeAttr("disabled");
                        $(".disable").attr("required", "required");
                        $("#nit").attr("data-rule-intrule", "true");
                        $("#nit").attr("data-rule-alpharule", "false");
                        $("#nit").attr("onkeypress", "return isNumberKey(event)");
                    }

                    $('#creationModal').modal('show');
                    
                </c:if>

                <c:if test="${not empty creationInstitution.id}">
                    $('#creationModal').modal('show');
                </c:if>
                <c:if test="${empty creationInstitution.id}">
                    $('.selectedOff').removeAttr("selected");
                    $('.selectedOn').attr("selected", "selected");
                </c:if>
                    
            });

            function CalidadObligatoria() {
                if ($("#typeid").val() === '1' && $('#id').val() === "" || $("#typeid").val() === '2' && $('#id').val() === "") {
                    $(".certificadoObligatorio").attr("required", "required");
                    $("#qualityCertificateExpiration").attr("required", "required");
                } else {
                    $(".certificadoObligatorio").removeAttr("required");
                    $("#qualityCertificateExpiration").removeAttr("required");
                }
            }

            function activacionDigito() {
                if ($("#documentTypeid").val() === '2' || $("#documentTypeid").val() === '6') {
                    $(".disable").attr("disabled", "disabled");
                    $(".disable").removeAttr("required");
                    $("#dv").val("");
                    $("#nit").attr("data-rule-intrule", "true");
                    $("#nit").attr("data-rule-alpharule", "false");
                    $("#nit").attr("onkeypress", "return isNumberKey(event)");
                    $("#nit").val("");
                } else if($("#documentTypeid").val() === '5' || $("#documentTypeid").val() === '3'){
                    $(".disable").attr("disabled", "disabled");
                    $(".disable").removeAttr("required");
                    $("#dv").val("");
                    $("#nit").attr("data-rule-intrule", "false");
                    $("#nit").attr("data-rule-alpharule", "true");
                    $("#nit").removeAttr("onkeypress");
                    $("#nit").val("");
                } else {
                    $(".disable").removeAttr("disabled");
                    $(".disable").attr("required", "required");
                    $("#nit").attr("data-rule-intrule", "true");
                    $("#nit").attr("data-rule-alpharule", "false");
                    $("#nit").attr("onkeypress", "return isNumberKey(event)");
                    $("#nit").val("");
                }
            }

            function loadCreationForm(id) {
                if (id !== undefined) {
                    $('#creationModal').attr("src",
                            'instituciones-oferentes.htm?id=' + id);
                }
            }

            function loadStates(deptoId, mnpoId){

                <c:if test="${not empty stateList}">
                    <c:forEach var="item" items="${stateList}" varStatus="iter">
                        <c:if test="${not empty creationInstitution.headquarter.state.id}">
                            <c:if test="${item.id == creationInstitution.headquarter.state.id}">
                                            jQuery("<option>").attr("value", '${item.id}').
                                                    text('${item.name}').appendTo("#" + deptoId);

                                            loadCities("${item.id}", mnpoId);
                            </c:if>
                            <c:if test="${item.id != creationInstitution.headquarter.state.id}">
                                            jQuery("<option>").attr("value", '${item.id}').
                                                    text('${item.name}').appendTo("#" + deptoId);
                            </c:if>
                        </c:if>
                        <c:if test="${empty creationInstitution.headquarter.state.id}">
                            <c:if test="${iter.index == 0}">
                                            jQuery("<option>").attr("value", '${item.id}').
                                                    text('${item.name}').appendTo("#" + deptoId);

                                            loadCities("${item.id}", mnpoId);
                            </c:if>
                            <c:if test="${iter.index > 0}">
                                            jQuery("<option>").attr("value", '${item.id}').
                                                    text('${item.name}').appendTo("#" + deptoId);
                            </c:if>
                        </c:if>
                    </c:forEach>
                </c:if>
            }

            function loadCitiesJson() {

                jQuery("#headquartercityid").html('');
                var stateCode = jQuery("#headquarterstateid").val();

                loadCities(stateCode, "headquartercityid");
            }

            function loadCitiesByState(deptoId, mnpoId) {
                jQuery("#" + deptoId).change(function () {
                    jQuery("#" + mnpoId).html('');
                    var stateCode = jQuery("#" + deptoId).val();

                    loadCities(stateCode, mnpoId);
                });
            }

            function loadCities(stateCode, mnpoId) {
                jQuery("#" + mnpoId).find('option').remove();
                jQuery("<option>").attr("value", '').
                        text('Seleccione...').appendTo("#" + mnpoId);        
                <c:if test="${not empty citiesList}">
                    <c:forEach items="${citiesList}" var="item" >
                        var code = "<c:out value="${item.state.id}"/>";
                        if (code === stateCode) {
                        <c:if test="${not empty creationInstitution.headquarter.city.id}">
                            <c:if test="${item.id == creationInstitution.headquarter.city.id}">
                                jQuery("<option>").attr("value", '${item.id}').text('${item.name}').
                                        appendTo("#" + mnpoId);
                            </c:if>
                            <c:if test="${item.id != creationInstitution.headquarter.city.id}">
                                jQuery("<option>").attr("value", '${item.id}').
                                        text('${item.name}').appendTo("#" + mnpoId);
                            </c:if>
                        </c:if>
                        <c:if test="${empty creationInstitution.headquarter.city.id}">
                                jQuery("<option>").attr("value", '${item.id}').
                                        text('${item.name}').appendTo("#" + mnpoId);
                        </c:if>
                                            }
                    </c:forEach>
                </c:if>
            }

            function fechaObligatoria() {

                if ($("#qualityCertificateExpiration").value === undefined) {
                    $("#qualityCertificateExpiration").attr("required", "required");
                    $(".fechaCert").show();
                } else {
                    $("#qualityCertificateExpiration").removeAttr("required");
                    $(".fechaCert").hide();
                }

            }

            function loadInstitutionDetail(id) {
                
                $('#detailModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                
                $('#detailArea').attr("src",
                        'detalle-institucion-oferente.htm?id=' + id);
            }

            $('#saveButton').confirmation({
                onConfirm: function (event, element) {
                    if (validateForm("creationInstitutionForm")) {
                        $('#creationInstitutionForm').submit();
                    }
                }
            });

            function resetForm() {
                creationInstitutionForm.reset();
                $('#id').val('');
                $('#internal').hide();
            }

            function alistaForm() {
                
                $('#creationModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                
                creationInstitutionForm.reset();
                $("#creationInstitutionForm").attr("action", "/masterdata/guardarInstitucion.htm");
                $(".istitutionOff").removeAttr("disabled");
                $("#dv").attr("disabled", "true");
                $(".desactivarEstado").hide();
                $(".hiddendiv").show();
                $(".fechaCert").hide();
                $(".msn").hide();
                $('#id').val('');
                $('.activaMunicipio').hide();
                $('.downloadCert').hide();
                $("#selectOpt").hide("selected", "selected");
            }

            function activaMunicipio() {

                if ($("#headquarterstateid").val() !== "") {

                    $('.activaMunicipio').show();

                }
            }

            function editInstitution(id,name) {
                
                $('#creationModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                
                 $(".istitutionOff").removeAttr("disabled");
                onEdit('/masterdata/obtener-institucion.htm?id=' + id,
                        'creationModal', afterEdit);
            }

            function afterEdit() {
                
                if ($("#documentTypeid").val() === '2' || $("#documentTypeid").val() === '6') {
                    $(".disable").attr("disabled", "disabled");
                    $(".disable").removeAttr("required");
                    $("#nit").attr("data-rule-intrule", "true");
                    $("#nit").attr("data-rule-alpharule", "false");
                    $("#nit").attr("onkeypress", "return isNumberKey(event)");
                } else if($("#documentTypeid").val() === '5' || $("#documentTypeid").val() === '3'){
                    $(".disable").attr("disabled", "disabled");
                    $(".disable").removeAttr("required");
                    $("#nit").attr("data-rule-intrule", "false");
                    $("#nit").attr("data-rule-alpharule", "true");
                    $("#nit").removeAttr("onkeypress");
                } else {
                    $(".disable").removeAttr("disabled");
                    $(".disable").attr("required", "required");
                    $("#nit").attr("data-rule-intrule", "true");
                    $("#nit").attr("data-rule-alpharule", "false");
                    $("#nit").attr("onkeypress", "return isNumberKey(event)");
                }
                
                if ($('#status').val() === 'I') {
                    $("#creationInstitutionForm").attr("action", "/masterdata/activarInstitucion.htm");
                    $(".istitutionOff").attr("disabled", "true");
                } else {
                    $("#creationInstitutionForm").attr("action", "/masterdata/guardarInstitucion.htm");
                    $(".istitutionOff").removeAttr("disabled");
                }

                if ($("#documentTypeid").val() === '2' || $("#documentTypeid").val() === '3' || $("#documentTypeid").val() === '5' || $("#documentTypeid").val() === '6' || $("#dv").val() !== "" && $('#status').val() === 'I') {
                    $(".disable").attr("disabled", "disabled");
                } else {
                    $(".disable").removeAttr("disabled");
                }

                $(".certificadoObligatorio").removeAttr("required");
                $("#qualityCertificateExpiration").removeAttr("required");
                
                $("#id2").val($("#id").val());
                $(".msn").show();
                $(".desactivarEstado").show();
                $('#tipoFormulario').val("editar");
                
                if ($('#qualityCertificateExpiration').val() !== undefined &&
                        $('#qualityCertificateExpiration').val() !== '') {
                    $('.downloadCert').show();
                    $(".fechaCert").show();
                    $("#qualityCertificateExpiration").attr("required", 
                        "required");
                } else {
                    $('.downloadCert').hide();
                    $(".fechaCert").hide();
                }
            }

            $.validator.addMethod('emailtld', function (val, elem) {
                var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

                if (!filter.test(val)) {
                    return false;
                } else {
                    return true;
                }
            }, 'Por favor, escriba una direcci�n de correo v�lida.');
            
            $.validator.addMethod('intrule', function (val, elem) {
                var filter = /^([0-9]{8,10})*$/;

                if (!filter.test(val)) {
                    return false;
                } else {
                    return true;
                }
            }, 'Por favor, escriba un numero de documento v�lido.');
            
            $.validator.addMethod('celrule', function (val, elem) {
                var filter = /^([0-9]{7,10})*$/;

                if (!filter.test(val)) {
                    return false;
                } else {
                    return true;
                }
            }, 'Por favor, escriba un tel�fono de contacto v�lido.');
            
            $.validator.addMethod('alpharule', function (val, elem) {
                var filter = /^([0-9a-zA-Z]{4,10})*$/;

                if (!filter.test(val)) {
                    return false;
                } else {
                    return true;
                }
            }, 'Por favor, escriba un numero de identificaci�n v�lido.');

        </script>
        <style>
            .buttons-csv{
                display: inline-block;
                background-image:url(resources/images/csv.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-csv span{
                opacity: 0;
            }
            .buttons-excel{
                display: inline-block;
                background-image:url(resources/images/excel.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-excel span{
                opacity: 0;
            }
            .buttons-pdf{
                display: inline-block;
                background-image:url(resources/images/pdf.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-pdf span{
                opacity: 0;
            }
            .buttons-print{
                display: inline-block;
                background-image:url(resources/images/print.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-print span{
                opacity: 0;
            }

            div.dt-buttons {
                float: right;
                margin-left:10px;
            }
        </style>
    </body>
</html>
