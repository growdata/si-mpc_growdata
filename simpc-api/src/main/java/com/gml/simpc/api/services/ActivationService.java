/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ActivationService.java
 * Created on: 2016/10/19, 02:10:35 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.entity.Activate;
import com.gml.simpc.api.entity.exeption.UserException;
import com.gml.simpc.api.enums.LinkReason;
import java.util.List;

/**
 * Interface que define el servicio de los tokens de activaci&oacute;n.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
public interface ActivationService {

    /**
     * Obtiene todos los usuarios creados.
     *
     * @return
     */
    List<Activate> getAll();

    /**
     * Servicio para guardar usuarios.
     *
     * @param activate
     */
    void save(Activate activate);

    /**
     * Servicio para actualizar activaciones.
     *
     * @param activate
     */
    void update(Activate activate);

    /**
     * Elimina una activaci&oacute;n.
     *
     * @param activate
     */
    void remove(Activate activate);

    /**
     * Busca si el token ingresado al activar el usuario coincide con el que
     * esta en base tras el registro
     *
     * @param token Token generado en el momento del registro
     *
     * @return Entidad de activaci&ocute;n
     */
    Activate findByUserToken(String token);

    /**
     * Eliminar los token de acuerdo al parametro hora.
     */
    void deleteExpiratedTokens();

    /**
     * Env&iacute;a el enlace de activaci&oacute;n al usuario.
     *
     * @param email
     * @param linkReason
     * @return
     *
     * @throws UserException
     */
    boolean sendPasswordLink(String email, LinkReason linkReason)
        throws UserException;

    /**
     * Elimina todos los tokens de un usuario.
     *
     * @param userId
     */
    void deleteTokensByUser(Long userId);
}
