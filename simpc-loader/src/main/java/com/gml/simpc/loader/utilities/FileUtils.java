/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: FileUtils.java
 * Created on: 2017/01/24, 10:09:49 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.utilities;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

/**
 * Clase de utilidades relacionadas con archivos
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica
 * Jim&eacute;nez Rozo</a>
 */
public class FileUtils {

    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(FileUtils.class);

    /**
     * M�todo para convertir MultiPartFile to File
     *
     * @param multipart
     * @param loadPath
     *
     * @return
     *
     * @throws IllegalStateException
     */
    public static File multipartToFile(MultipartFile multipart, String loadPath) {

        File directory = new File(loadPath + "tmp" + System.getProperty(
            "file.separator"));
        directory.mkdirs();
        String tempFileName = directory.getAbsolutePath() +
            System.getProperty("file.separator") + multipart.
            getOriginalFilename();
        File convFile = new File(tempFileName);
        try {
            multipart.transferTo(convFile);
        } catch (IOException ex) {
            LOGGER.error("IO Error converting multipart to file " + ex);
        } catch (IllegalStateException ex) {
            LOGGER.error("Ilegal Error converting multipart to file " + ex);
        }
        return convFile;
    }

    /**
     * M�todo que lee un archivo para ver que exista e imprime su contenido
     *
     * @param file
     */
    public static void readFile(File file) {

        StringBuilder result = new StringBuilder("Reading file " + file.
            getPath() +
             "Size: " + file.length() + ": ");
        try (Scanner scanner = new Scanner(file)) {

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                result.append("\n").append(line);
            }

            scanner.close();

        } catch (IOException e) {
            LOGGER.error("Error", e);
        }
        LOGGER.debug(result);
    }

}
