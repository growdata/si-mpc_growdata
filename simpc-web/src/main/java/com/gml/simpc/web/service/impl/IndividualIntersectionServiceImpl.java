/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ActionLogServiceImpl.java
 * Created on: 2016/11/15, 10:50:35 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.dao.IndividualIntersectionDao;
import com.gml.simpc.api.dto.BasicInformationDto;
import com.gml.simpc.api.dto.DependentDto;
import com.gml.simpc.api.dto.EducationLevelDto;
import com.gml.simpc.api.dto.InformalEducationDto;
import com.gml.simpc.api.dto.IntermediationDto;
import com.gml.simpc.api.dto.LanguageDto;
import com.gml.simpc.api.dto.OtherKnowledgeDto;
import com.gml.simpc.api.dto.PilaDto;
import com.gml.simpc.api.dto.PilaEmploymentServiceDto;
import com.gml.simpc.api.dto.ProfitDto;
import com.gml.simpc.api.dto.ProgramDto;
import com.gml.simpc.api.dto.WorkExperienceDto;
import com.gml.simpc.api.services.IndividualIntersectionService;
import com.gml.simpc.commons.annotations.AnnotationInterceptor;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Service(value = "individualIntersectionService")
public class IndividualIntersectionServiceImpl implements
    IndividualIntersectionService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(IndividualIntersectionServiceImpl.class);
    /**
     * Repositorio para log de acciones.
     */
    @Autowired
    private IndividualIntersectionDao individualIntersectionDao;

    /**
     * Genera lista de pila seg&ucute;n tipo y n&ucute;mero de documento.
     * 
     * @param documentType
     * @param numberIdentification
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", entityTableName = "MD_F_PILA",
        methodName = "Consultar PILA por tipo y n�mero de documento")
    @Override
    public List<PilaDto> getPilaByDocumentAndNumberType(String documentType,
        String numberIdentification) {
        LOGGER.info("Ejecutando metodo [ getPilaByDocumentAndNumberType ] de " +
            "IndividualIntersectionServiceImpl");
        return this.individualIntersectionDao.
            findPilaByDocumentAndNumberType(documentType, numberIdentification);
    }

    /**
     * Genera lista de dependientes seg&ucute;n tipo y n&ucute;mero de docuumento.
     * 
     * @param documentType
     * @param numberIdentification
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", entityTableName = "MD_F_DEPENDIENTES",
        methodName = "Consultar DEPENDIENTES por tipo y n�mero de documento")
    @Override
    public List<DependentDto> getDependentsByDocumentAndNumberType(
        String documentType, String numberIdentification) {
        LOGGER.info("Ejecutando metodo [ " +
            "getDependentsByDocumentAndNumberType ] de " +
            "IndividualIntersectionServiceImpl");
        return this.individualIntersectionDao.
            findDependentsByDocumentAndNumberType(documentType,
                numberIdentification);
    }

    /**
     * Genera lista de capacitaciones segun tipo y n&ucute;mero de documento.
     * 
     * @param documentType
     * @param numberIdentification
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES",
        entityTableName = "MD_F_CAP_PROGRAMA_MODULO",
        methodName = "Consultar Capacitaci�n y Orientaci�n por " +
        "tipo y n�mero de documento")
    @Override
    public List<ProgramDto> getTrainingsByDocumentAndNumberType(
        String documentType, String numberIdentification) {
        LOGGER.info("Ejecutando metodo [ " +
            "getTrainingsByDocumentAndNumberType ] de " +
            "IndividualIntersectionServiceImpl");
        return this.individualIntersectionDao.
            findTrainingsByDocumentAndNumberType(documentType,
                numberIdentification);
    }

    /**
     * Genera lista de pila seg&ucute;n el tipo y numero de documento 
     * espec&icute;fico para los integrantes del servicio p&ucute;blico de empleo.
     * 
     * @param documentType
     * @param numberIdentification
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", entityTableName = "MD_F_PILA",
        methodName = "Consultar Pila por tipo y n�mero de documento")
    @Override
    public List<PilaEmploymentServiceDto>
        getPilaByDocumentAndNumberTypePublicEmploymentService(
            String documentType, String numberIdentification) {
        LOGGER.info("Ejecutando metodo [" +
            " getPilaByDocumentAndNumberTypePublicEmploymentService ] de " +
            "IndividualIntersectionServiceImpl");
        return this.individualIntersectionDao.
            findPilaByDocumentAndNumberTypeEmploymentService(documentType,
                numberIdentification);
    }

        /**
         * Genera lista de fosfec seg&ucute;n el tipo y n&ucute;mero de documento.
         * 
         * @param documentType
         * @param numberIdentification
         * @return 
         */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", entityTableName = "MD_F_FOSFEC",
        methodName = "Consultar Fosfec por tipo y n�mero de documento")
    @Override
    public List<ProfitDto> getFosfecByDocumentAndNumberType(String documentType,
        String numberIdentification) {
        LOGGER.info("Ejecutando metodo [ getFosfecByDocumentAndNumberType ] " +
            "de IndividualIntersectionServiceImpl");
        return this.individualIntersectionDao.
            findFosfecByDocumentAndNumberType(documentType, numberIdentification);
    }

    /**
     * Genera lista de intermediaci&icute;n seg&ucute;n el tipo y n&ucute;mero 
     * de documento.
     * 
     * @param documentType
     * @param numberIdentification
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES",
        entityTableName = "MD_F_GESTION_INTERMEDIACION",
        methodName = "Consultar Intermediaci�n por tipo y n�mero de documento")
    @Override
    public List<IntermediationDto> getIntermediationByDocumentAndNumberType(
        String documentType, String numberIdentification) {
        LOGGER.info(
            "Ejecutando metodo [ getIntermediationByDocumentAndNumberType ] de " +
            "IndividualIntersectionServiceImpl");
        return this.individualIntersectionDao.
            findIntermediationByDocumentAndNumberType(documentType,
                numberIdentification);
    }

    /**
     * Genera lista de informaci&ocute;n b&acute;sica seg&ucute;n el tipo
     * y n&ucute;mero de documento.
     * 
     * @param documentType
     * @param numberIdentification
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", entityTableName = "MD_F_DATOS_BASICOS",
        methodName = "Consultar Datos B�sicos por tipo y n�mero de documento")
    @Override
    public List<BasicInformationDto> getBasicInformationByDocumentAndNumberType(
        String documentType, String numberIdentification) {
        LOGGER.info("Ejecutando metodo [ " +
            "getBasicInformationByDocumentAndNumberType]" +
            " de IndividualIntersectionServiceImpl");
        try {
            return this.individualIntersectionDao.
                findBasicInformationByDocumentAndNumberType(documentType,
                    numberIdentification);
        } catch (Exception ex) {
            LOGGER.error("Error", ex);
            return null;
        }
    }

    /**
     * Genera lista de nivel educativo seg&ucute;n el tipo
     * y n&ucute;mero de documento.
     * 
     * @param documentType
     * @param numberIdentification
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES",
        entityTableName = "MD_F_NIVEL_EDUCATIVO",
        methodName = "Consultar Nivel Educativo por tipo y n�mero de documento")
    @Override
    public List<EducationLevelDto> getEducationLevelByDocumentAndNumberType(
        String documentType, String numberIdentification) {
        LOGGER.info("Ejecutando metodo [ " +
            "getEducationLevelByDocumentAndNumberType ]" +
            " de IndividualIntersectionServiceImpl");
        try {
            return this.individualIntersectionDao.
                findEducationLevelByDocumentAndNumberType(documentType,
                    numberIdentification);
        } catch (Exception ex) {
            LOGGER.error("Error", ex);
            return null;
        }
    }

    /**
     * Genera lista de experiencia laboral seg&ucute;n el tipo
     * y n&ucute;mero de documento.
     * 
     * @param documentType
     * @param numberIdentification
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES",
        entityTableName = "MD_F_EXPERIENCIA_LABORAL",
        methodName = "Consultar Experiencia Laboral por tipo y n�mero de " +
        "documento")
    @Override
    public List<WorkExperienceDto> getWorkExperienceByDocumentAndNumberType(
        String documentType, String numberIdentification) {
        LOGGER.info("Ejecutando metodo [ " +
            "getWorkExperienceByDocumentAndNumberType ]" +
            " de IndividualIntersectionServiceImpl");
        try {
            return this.individualIntersectionDao.
                findWorkExperienceByDocumentAndNumberType(documentType,
                    numberIdentification);
        } catch (Exception ex) {
            LOGGER.error("Error", ex);
            return null;
        }
    }

    /**
     * Genera lista de educaci&ocute;n informal seg&ucute;n el tipo
     * y n&ucute;mero de documento.
     * 
     * @param documentType
     * @param numberIdentification
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", entityTableName =
        "MD_F_EDUCACION_INFORMAL", methodName = "Consultar Educaci�n " +
        "Informal por tipo y n�mero de documento")
    @Override
    public List<InformalEducationDto> getInformalEducationByDocumentAndNumberType(
        String documentType, String numberIdentification) {
        LOGGER.info(
            "Ejecutando metodo [ getInformalEducationByDocumentAndNumberType]" +
            " de IndividualIntersectionServiceImpl");
        try {
            return this.individualIntersectionDao.
                findInformalEducationByDocumentAndNumberType(documentType,
                    numberIdentification);
        } catch (Exception ex) {
            LOGGER.error("Error", ex);
            return null;
        }
    }

    /**
     * Genera lista de idiomas seg&ucute;n el tipo y n&ucute;mero de documento.
     * 
     * @param documentType
     * @param numberIdentification
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", entityTableName =
        "MD_F_IDIOMAS", methodName = "Consultar Idiomas por tipo y n�mero de " +
        "documento")
    @Override
    public List<LanguageDto> getLanguageByDocumentAndNumberType(
        String documentType, String numberIdentification) {
        LOGGER.info(
            "Ejecutando metodo [ getLanguageByDocumentAndNumberType]" +
            " de IndividualIntersectionServiceImpl");
        try {
            return this.individualIntersectionDao.
                findLanguageByDocumentAndNumberType(documentType,
                    numberIdentification);
        } catch (Exception ex) {
            LOGGER.error("Error", ex);
            return null;
        }
    }

    /**
     * Genera lista de otros conocimientos seg&ucute;n el tipo y n&ucute;mero
     * de documento.
     * 
     * @param documentType
     * @param numberIdentification
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", entityTableName =
        "MD_F_OTROS_CONOCIMIENTOS", methodName = "Consultar Otros " +
        "Conocimientos por tipo y n�mero de documento")
    @Override
    public List<OtherKnowledgeDto> getOtherKnowledgeByDocumentAndNumberType(
        String documentType, String numberIdentification) {
        LOGGER.info(
            "Ejecutando metodo [ getOtherKnowledgeByDocumentAndNumberType]" +
            " de IndividualIntersectionServiceImpl");
        try {
            return this.individualIntersectionDao.
                findOtherKnowledgeByDocumentAndNumberType(documentType,
                    numberIdentification);
        } catch (Exception ex) {
            LOGGER.error("Error", ex);
            return null;
        }
    }
}
