/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ResourcesServiceImpl.java
 * Created on: 2016/10/26, 12:28:33 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.entity.Resource;
import com.gml.simpc.api.services.ResourcesService;
import com.gml.simpc.commons.annotations.AnnotationInterceptor;
import com.gml.simpc.web.repository.ResourceRepository;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author <a href="mailto:julianm@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
@Service(value = "resourcesService")
public class ResourcesServiceImpl implements ResourcesService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(
        ResourcesServiceImpl.class);

    /**
     * Repositorio para usuarios.
     */
    @Autowired
    private ResourceRepository resourceRepository;

    /**
     * Genera lista de todos los recursos.
     * 
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "getAll",
        entityTableName = "RECURSOS")
    @Override
    public List<Resource> getAll() {
        LOGGER.info("Ejecutando metodo [ getAll ]");
        return this.resourceRepository.findAll();
    }

    /**
     * Guarda recursos.
     * 
     * @param resource 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "save",
        entityTableName = "RECURSOS")
    @Override
    public void save(Resource resource) {
        LOGGER.info("Ejecutando metodo [ save ]");
        this.resourceRepository.saveAndFlush(resource);
    }

    /**
     * Actualiza recursos.
     * 
     * @param resource 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "update",
        entityTableName = "RECURSOS")
    @Override
    public void update(Resource resource) {
        LOGGER.info("Ejecutando metodo [ update ]");
        this.resourceRepository.saveAndFlush(resource);
    }

    /**
     * Elimina recursos.
     * 
     * @param resource 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "remove",
        entityTableName = "RECURSOS")
    @Override
    public void remove(Resource resource) {
        LOGGER.info("Ejecutando metodo [ remove ]");
        this.resourceRepository.delete(resource);
    }

    /**
     * Busca y devuelve lista de recursos seg&ucute;n el id de un perfil
     * especi&icute;fico.
     * 
     * @param idProfile
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "findByIdProfile",
        entityTableName = "RECURSOS")
    @Override
    public List<Resource> findByIdProfile(Long idProfile) {
        LOGGER.info("Ejecutando metodo [ findByIdProfile ]");
        return this.resourceRepository.findByIdProfile(idProfile);
    }

    /**
     * Elimina los recursos de un perfil especifico seg&ucute;n su id.
     * 
     * @param profileID 
     */
    @Override
    public void deleteByProfileId(Long profileID) {
        this.resourceRepository.deleteByProfileId(profileID);
    }

}
