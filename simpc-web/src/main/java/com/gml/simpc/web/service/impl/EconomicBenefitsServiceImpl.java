/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ActionLogServiceImpl.java
 * Created on: 2016/11/15, 10:50:35 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.dao.EconomicBenefitsDao;
import com.gml.simpc.api.dto.ConsolidantPostulantDto;
import com.gml.simpc.api.services.EconomicBenefitsService;
import com.gml.simpc.commons.annotations.AnnotationInterceptor;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Service(value = "EconomicBenefitsService")
public class EconomicBenefitsServiceImpl implements EconomicBenefitsService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(
        EconomicBenefitsServiceImpl.class);
    /**
     * Repositorio para usuarios.
     */
    @Autowired
    private EconomicBenefitsDao economicBenefitsDao;

    /**
     * Genera la consulta consolidada de postulantes.
     * 
     * @param idCcf
     * @param year
     * @param month
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar Consolidado " +
        "Postulantes", entityTableName = "MD_F_FOSFEC")
    @Override
    public List<ConsolidantPostulantDto> getConsolidatedPostulants(String idCcf,
        String year, String month) {
        List<ConsolidantPostulantDto> consolidantPostulantList =
            economicBenefitsDao.getConsolidatedPostulants(idCcf, year, month);
        
        return consolidantPostulantList;
    }

    /**
     * Genera la consulta detallada de postulantes.
     * 
     * @param idCcf
     * @param year
     * @param month
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar Detallado " +
        "Postulantes", entityTableName = "MD_F_FOSFEC")
    @Override
    public List<ConsolidantPostulantDto> getDetailedPostulants(String idCcf,
        String year, String month) {
        List<ConsolidantPostulantDto> consolidantPostulantList =
            economicBenefitsDao.getDetailedPostulants(idCcf, year, month);
        
        return consolidantPostulantList;
    }

    /**
     * Genera la consulta consolidada de beneficiarios.
     * 
     * @param idCcf
     * @param year
     * @param month
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar Consolidado " +
        "Beneficiarios", entityTableName = "MD_F_FOSFEC")
    @Override
    public List<ConsolidantPostulantDto> getConsolidatedBenefits(String idCcf,
        String year, String month) {
        List<ConsolidantPostulantDto> consolidantPostulantList =
            economicBenefitsDao.getConsolidatedBenefits(idCcf, year, month);
        
        return consolidantPostulantList;
    }

    /**
     * Genera la consulta detallada de beneficiarios.
     * 
     * @param idCcf
     * @param year
     * @param month
     * @return 
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar Detallado " +
        "Beneficiarios", entityTableName = "MD_F_FOSFEC")
    @Override
    public List<ConsolidantPostulantDto> getDetailedBenefits(String idCcf,
        String year, String month) {
        List<ConsolidantPostulantDto> consolidantPostulantList =
            economicBenefitsDao.getDetailedBenefits(idCcf, year, month);
        
        return consolidantPostulantList;
    }

}
