/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: MailHistory.java
 * Created on: 2017/01/13, 02:38:31 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entidad para guardar el historial de Emails.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Table(name = "HISTORICO_CORREOS")
@Entity
public class MailHistory implements Serializable {

    /**
     * ID de la entidad.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO, 
        generator = "mail_history_seq_gen")
    @SequenceGenerator(name = "mail_history_seq_gen", 
        sequenceName = "MAIL_HISTORY_SEQ", allocationSize = 1, initialValue= 1)
    private Long id;
    /**
     * Correo desde el que se env&iacute;a.
     */
    @Column(name = "DE", nullable = false)
    private String from;
    /**
     * Correo al que se env&iacute;a.
     */
    @Column(name = "PARA", nullable = false)
    private String to;
    /**
     * Mensaje que se env&iacute;a.
     */
    @Column(name = "TEXTO", nullable = false, columnDefinition = "CLOB")
    private String text;
    /**
     * Fecha de env&iacute;o.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_ENVIO", nullable = false)
    private Date creationDate;

    public MailHistory() {
    }

    public MailHistory(String from, String to, String text) {
        this.from = from;
        this.to = to;
        this.text = text;
        
        this.creationDate = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
