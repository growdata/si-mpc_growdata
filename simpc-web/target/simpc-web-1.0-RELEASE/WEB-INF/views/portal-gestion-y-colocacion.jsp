<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Informaci�n de Gesti�n y Colocaci�n</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster.bundle.min.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster-sideTip-shadow.min.css" rel="stylesheet"/>
        <link href="resources/css/morris.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <style>
            in  put[type=number]::-webkit-outer-spin-button,
            input[type=number]::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            input[type=number] {
                -moz-appearance:textfield;
            }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <div class="col-sm-3" style="padding:0px;">
                        <img src="resources/images/logo_mpc.png" width="100%" alt=""/>
                    </div>
                    <div class="col-sm-8">
                        <a class="navbar-brand" href="/masterdata/index.htm">Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</a>
                    </div>
                </div>
                <div class="navbar-nav navbar-form credenciales pull-right">
                    <table>
                        <tr>
                            <td>
                                <strong>Bienvenido</strong> 
                            </td>
                            <td style="width: 120px; text-align: right">
                                <a id="" href="/masterdata/index.htm" class="btn btn-warning btn-xs">Volver al inicio</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="row row-offcanvas row-offcanvas-left">
                <h1>Informaci�n de Gesti�n y Colocaci�n</h1>
                <!-- main area -->
                <div class="panel panel-default">
                    <div class="panel-heading"><h2>Filtros de B�squeda</h2></div>
                    <div class="panel-body">
                        <div class="containerMsn">
                            <div class="alert alert-warning alert-dismissible" id="myAlert">
                                <font SIZE=3>(*Si va a elegir un tipo de fecha aseg�rese de diligenciar los campos 'inicio' y 'fin' de manera correcta)</font>
                            </div>
                        </div>
                        <div id="alert-area" ></div>
                        <div class="panel-body">
                            <legend>
                                <form:form id="buscarIgc" method="get" action="buscarGestionYColocacion.htm">
                                    <input type="hidden" id="activateBtn" value="${activateBtn}">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="fechaInicioBegin" class="control-label">Fecha de Postulaci�n - Inicio</label>
                                            <input id="fechaInicioBegin" class="form-control datepicker"
                                                   data-date-format="dd-mm-yyyy" path="initDate" name ="initDate" 
                                                   autocomplete="off" required="required" value="${initDateSelected}"/>
                                            <fmt:formatDate value="${startDate}" pattern="dd-MM-yyyy" />
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>        
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="fechaInicioEnd" class="control-label">Fecha de Postulaci�n - Fin</label>
                                            <input id="fechaInicioEnd" class="form-control datepicker" name="finishDate"
                                                   data-date-format="dd-mm-yyyy" path="finishDate"  
                                                   autocomplete="off" required="required" value="${endDateSelected}"/>
                                            <fmt:formatDate value="${endDate}" pattern="dd-MM-yyyy" />
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>     
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="mes" class=" control-label">Estado de la Postulaci�n</label>
                                            <select class="form-control" id="statusPostulation" path="statusPostulation" name="statusPostulation">
                                                <option <c:if test="${statusPostSelected == ''}">selected="selected"</c:if> label="Todos los estados" value="">Todos los estados</option>
                                                <option <c:if test="${statusPostSelected == '1'}">selected="selected"</c:if> label="Autopostulado" value="1">Autopostulado</option>
                                                <option <c:if test="${statusPostSelected == '2'}">selected="selected"</c:if> label="Autopostulado Asistido" value="2">Autopostulado Asistido</option>
                                                <option <c:if test="${statusPostSelected == '3'}">selected="selected"</c:if> label="Remitido por prestador" value="3">Remitido por prestador</option>
                                                <option <c:if test="${statusPostSelected == '4'}">selected="selected"</c:if> label="Preseleccionado" value="4">Preseleccionado</option>
                                                <option <c:if test="${statusPostSelected == '5'}">selected="selected"</c:if> label="Descartado por prestador" value="5">Descartado por prestador</option>
                                                <option <c:if test="${statusPostSelected == '6'}">selected="selected"</c:if> label="No seleccionado empleador" value="6">No seleccionado empleador</option>
                                                <option <c:if test="${statusPostSelected == '7'}">selected="selected"</c:if> label="Colocado" value="7">Colocado</option>
                                                <option <c:if test="${statusPostSelected == '8'}">selected="selected"</c:if> label="Declinado" value="8">Declinado</option>
                                            </select>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-offset-4 col-sm-8" style="text-align:right">
                                        <input onclick="verificarForm()" type="button" value="Buscar" class="btn btn-success">
                                    </div>
                                </form:form>         
                        </div>
                        <div class="clearfix"></div>

                        <div class="panel panel-default" style="width: 100%; margin: 0 auto;">
                            <div class="panel-heading"><h2>Resultados de la B�squeda</h2></div>
                            <div class="panel-body">
                                <div class=" table-responsive col-md-12">
                                    <center>
                                        <button type="button" class="btn btn-default desableBtn"
                                                data-toggle="modal" data-target="#graphicModal">  
                                            <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                                            Ver gr&aacute;fico
                                        </button>
                                    </center>
                                    <table id="resultsConsolidPostulants" class="table table-striped table-bordered" 
                                           cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Estado Postulante</th>
                                                <th>Mujeres</th>
                                                <th>Hombres</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Estado Postulante</th>
                                                <th>Mujeres</th>
                                                <th>Hombres</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <c:if test="${not empty resultList}">
                                                <c:forEach var="item" items="${resultList}">
                                                    <tr>
                                                        <td>${item.postulationStatus}</td>
                                                        <td align="right">${item.women}</td>
                                                        <td align="right">${item.men}</td>
                                                    </tr>
                                                </c:forEach>
                                            </c:if>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <br/>


        <div class="modal fade" id="graphicModal" tabindex="-1" role="dialog" aria-labelledby="graphicModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <input type="hidden" id="newGraphic" val=""/>
                            <div class="col-md-12  " id="graphicContainer2">
                                <h4 class="modal-title" id="graphicModalLabel"><Strong><center>${titulo}</center></Strong></h4>
                                <div id="graphicContainer" ></div>
                                <br>


                                <div id="legend" class="graphic-legend inline-block"></div>
                                <br> <br> <br>
                                <Strong> ${titulografica}</Strong>
                                <div class=" table-responsive col-md-12">
                                    <table id="tablag" class=" table-striped table-bordered " cellspacing="0" width="100%">
                                        <thead>

                                            <tr>
                                                <c:if test="${not empty titleList}">
                                                    <c:forEach items="${titleList}" var="info" begin="0">

                                                        <th>${info}</th>

                                                    </c:forEach>  
                                                </c:if>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <c:if test="${not empty graphicList}">
                                                <c:forEach items="${graphicList}" var="item" begin="0">

                                                    <tr> 
                                                        
                                                        <td>${item.postulationStatus}</td>
                                                        <td align="right">${item.women}</td>
                                                        <td align="right">${item.men}</td>


                                                    </tr>   
                                                </c:forEach>  
                                            </c:if>

                                        </tbody> 
                                    </table>
                                </div>

                            </div>

                        </div>
                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" onclick="printGraphic()">Exportar PDF</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <!--Footer -->
        <div class="footer">
            <img src="resources/images/bg_banderin.jpg" width="100%"/> 
            <div class="clearfix"></div> 
        </div>
        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.buttons.min.js"></script>
        <script src="resources/js/buttons.flash.min.js"></script>
        <script src="resources/js/jszip.min.js"></script>
        <script src="resources/js/pdfmake.min.js"></script>
        <script src="resources/js/vfs_fonts.js"></script>
        <script src="resources/js/buttons.html5.min.js"></script>
        <script src="resources/js/buttons.print.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/bootstrap-confirmation.js"></script>
        <script src="resources/js/jquery.validate.min.js"></script>
        <script src="resources/js/tooltipster.bundle.js"></script>
        <script src="resources/js/custom-functionalities.js"></script>
        <script src="resources/js/raphael.min.js"></script>
        <script src="resources/js/morris.js"></script>
         <script src="resources/js/moment.js"></script>
        <script>

            $(document).ready(function () {
                
                if($("#activateBtn").val() === ""){
                    $(".desableBtn").attr("disabled", "desabled");
                } else {
                    $(".desableBtn").removeAttr("disabled");
                }
                
                $('.containerMsn').hide();

                $('#resultsConsolidPostulants').DataTable({
                    
                    "language": {
                        "url": "resources/js/Spanish.json"
                    },
                    "initComplete": function (settings, json) {
                        $(".dt-buttons").each(function (index) {
                            console.log(index + ": " + $(this).text());
                            if ($(this).find('.exportar').length === 0) {
                                $(this).prepend('<label class="exportar">Exportar a:&nbsp;&nbsp;</label>');
                            }
                        });
                    },
                    "bFilter": true,
                    "bInfo": false,
                    dom: 'Blfrtip',
                    buttons: [
                        {
                            extend: 'csv',
                            fieldBoundary: '',
                            text: '<i class="fa fa-file-text-o"></i>',
                            titleAttr: 'CSV'

                        },
                        {
                            extend: 'excel',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            titleAttr: 'EXCEL'
                        },
                        {
                            extend: 'pdf',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            titleAttr: 'PDF'

                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'COPY'
                        }
                    ]

                });

                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Mi�rcoles", "Jueves", "Viernes", "S�bado", "Domingo"],
                    daysShort: ["Dom", "Lun", "Mar", "Mi�", "Jue", "Vie", "S�b", "Dom"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Hoy"
                };

                                $('#fechaInicioBegin').datepicker({language: "es", autoclose: true});
                                $('#fechaInicioEnd').datepicker({language: "es", autoclose: true});

                            });

                            $('#graphicModal').on('shown.bs.modal', function () {
                                $(function () {
                                    var json = '<c:out value="${graphicData}" escapeXml="false"/>';
                                    var type = '<c:out value="${graphic.type}" escapeXml="false"/>';
                                     var ykeys = [];
                                        var labels = [];
                                        var lineColors = [];
                                        var barColors = [];
            <c:forEach items="${yKeys}" var="yKey" begin="0">
                                        ykeys.push('<c:out value="${yKey}" escapeXml="false"/>');
            </c:forEach>
            <c:forEach items="${labels}" var="labels" begin="0">
                                        labels.push('<c:out value="${labels}" escapeXml="false"/>');
            </c:forEach>
           
            <c:forEach items="${barColors}" var="barColors" begin="0">
                                        barColors.push('<c:out value="${barColors}" escapeXml="false"/>');
            </c:forEach>
                                    
                                    $("#graphicContainer").empty();
                                    if (type === 'B') {
                                      var browsersChart =    Morris.Bar({
                                            element: 'graphicContainer',
                                            data: jQuery.parseJSON(json),
                                             xkey: '<c:out value="${xKey}" escapeXml="false"/>',
                                                ykeys: ykeys,
                                                labels: labels,
                                                stacked: false,
                                                hideHover: 'auto',
                                                barGap: 4,
                                                barColors: barColors,
                                                barSizeRatio: 0.55,
                                                xLabelAngle: 45,
                                                resize: true
                                        });
                                        if($('#newGraphic').val() === ""){
                                            browsersChart.options.data.forEach(function (labels, i) {
                                                    var legendItem = $('<span></span>').text(browsersChart.options.labels[i]).prepend('<br><span>&nbsp;</span>');
                                                    legendItem.find('span')
                                                            .css('backgroundColor', browsersChart.options.barColors[i])
                                                            .css('width', '20px')
                                                            .css('display', 'inline-block')
                                                            .css('margin', '5px');
                                                    $('#newGraphic').val("NO");
                                                    $('#legend').append(legendItem)
                                                });
                                        }
                                    } else if (type === 'L') {
                                        Morris.Line({
                                            element: 'graphicContainer',
                                            data: jQuery.parseJSON(json),
                                            xkey: 'month',
                                            ykeys: ['postulants', 'approvedApplications', 'requestsDenied'],
                                            labels: ['Postulantes al subsidio', 'Solicitudes aprobadas', 'Solicitudes Denegadas'],
                                            stacked: false,
                                            resize: true,
                                            parseTime: false
                                        });
                                    } else {
                                        Morris.Donut({
                                            element: 'graphicContainer',
                                            data: jQuery.parseJSON(json),
                                            stacked: true,
                                            resize: true
                                        });
                                    }
                                });
                            });

                            /**
                             * Imprime el grafico en un pdf.
                             */
                            function printGraphic() {
                                var objeto = document.getElementById('graphicContainer2'); //obtenemos el objeto a imprimir
                                var ventana = window.open('', '_blank'); //abrimos una ventana vac�a nueva
                                ventana.document.write(objeto.innerHTML); //imprimimos el HTML del objeto en la nueva ventana
                                ventana.document.close(); //cerramos el documento
                                ventana.print(); //imprimimos la ventana
                                ventana.close(); //cerramos la ventana

                            }

                            function verificarForm() {
                                if ($('#fechaInicioBegin').val() !== "" && $('#fechaInicioEnd').val() === "" ||
                                        $('#fechaInicioBegin').val() === "" && $('#fechaInicioEnd').val() !== "") {
                                    $('.containerMsn').hide("fast");
                                    $('.containerMsn').show("fast");
                                } else if (moment($("#fechaInicioBegin").val(),"DD-MM-YYYY") > moment($("#fechaInicioEnd").val(),"DD-MM-YYYY")) {
                                    $('.containerMsn').hide("fast");
                                    $('.containerMsn').show("fast");
                                } else {
                                    
                                    if (validateForm("buscarIgc")) {
                        $('#buscarIgc').submit();
                    }
                                    
//                                    $('#buscarIgc').submit();
                                }
                            }
        </script>       
        <style>
            .buttons-csv{
                display: inline-block;
                background-image:url(resources/images/csv.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;

            }
            .buttons-csv span{
                opacity: 0;
            }
            .buttons-excel{
                display: inline-block;
                background-image:url(resources/images/excel.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;

            }
            .buttons-excel span{
                opacity: 0;
            }
            .buttons-pdf{
                display: inline-block;
                background-image:url(resources/images/pdf.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;

            }
            .buttons-pdf span{
                opacity: 0;
            }
            .buttons-print{
                display: inline-block;
                background-image:url(resources/images/print.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;


            }
            .buttons-print span{
                opacity: 0;

            }
            div.dt-buttons {
                float: right;
                margin-left:10px;
            }
.graphic-legend > span {
                display: inline-block;
                margin-right: 25px;
                margin-bottom: 10px;
                font-size: 13px;


            }
            .graphic-legend > span:last-child {
                margin-right: 0;
            }

            #graphicContainer {
                max-height: 280px;
                margin-top: 20px;
                margin-bottom: 20px;


            }


            .graphic-legend > span > i {
                display: inline-block;
                width: 15px;
                height: 15px;
                margin-right: 7px;
                margin-top: -3px;
                vertical-align: middle;
                border-radius: 1px;
            }

            #browsers_chart {
                max-height: 280px;
                margin-top: 20px;
                margin-bottom: 20px;
            }

        </style>
    </body>
</html>
