/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: PersonDto.java
 * Created on: 2017/02/07, 10:58:46 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.intersection.dto;

/**
 * DTO para identificar a las personas.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class PersonDto {

    /*
     * Tipo de identificaci&oacute;n.
     */
    private String identificationType;
    /**
     * N&uacute;mero de identificaci&oacute;n.
     */
    private String identificationNumber;
    /**
     * C&oacute;digo de caja de compensaci&oacute;n.
     */
    private String ccfCode;

    public String getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(String identificationType) {
        this.identificationType = identificationType;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getCcfCode() {
        return ccfCode;
    }

    public void setCcfCode(String ccfCode) {
        this.ccfCode = ccfCode;
    }
}