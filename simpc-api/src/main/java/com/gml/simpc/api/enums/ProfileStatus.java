/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserStatus.java
 * Created on: 2016/10/21, 02:56:30 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.enums;

/**
 * Enum que maneja los estados de los usuarios
 *
 * @author <a href="mailto:jonathanp@gmlsoftware.com">Jonathan Pont�n</a>
 */
public enum ProfileStatus {

    A('A', "Activo"),
    I('I', "Inactivo");

    /**
     * C&oacute;digo del estado.
     */
    private final char code;
    /**
     * Texto del estado.
     */
    private final String label;

    private ProfileStatus(char code, String label) {
        this.code = code;
        this.label = label;
    }

    public char getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }

    /**
     * Devuelve el status por c&oacute;digo.
     *
     * @param code
     *
     * @return
     */
    public static ProfileStatus getByCode(char code) {
        ProfileStatus[] allValues = values();

        for (ProfileStatus status : allValues) {
            if (status.getCode()==(code)) {
                return status;
            }
        }

        throw new IllegalArgumentException("C&oacute;digo [ " + code +
            " ] no se encuentra registrado.");
    }
    @Override
    public String toString(){
        return label;
    }
}