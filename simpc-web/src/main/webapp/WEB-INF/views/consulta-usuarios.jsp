<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>MECANISMO DE PROTECCI�N AL CESANTE "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster.bundle.min.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster-sideTip-shadow.min.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <style>
            input[type=number]::-webkit-outer-spin-button,
            input[type=number]::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            input[type=number] {
                -moz-appearance:textfield;
            }
        </style>
    </head>

    <body>
        <br/>
        <c:if test="${not empty msg}">
            <div id="alert-${msgType}" class="alert alert-${msgType} alert-dismissible" role="alert">
                <button onclick="resetForm()" type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        <h1>Administraci�n de Usuarios</h1>
        <div class="panel panel-default">
            <div class="panel-heading"><h2>B&uacute;squeda</h2></div>
            <div class="panel-body">
                <legend>
                    <form:form method="get" action="/masterdata/buscarUsuarios.htm" modelAttribute="searchUser"> 
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="nombre-usuario-busqueda" class="control-label">Nombre - Apellido</label>
                                <form:input type="text" class="form-control" id="firstNameC" 
                                            placeholder="Nombre del Consultor"
                                            path="firstName" maxlength="50"/>
                                <div class="clearfix"></div>
                            </div>
                        </div>  
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="numero-documento" class="control-label">N�mero de Documento</label>
                                <form:input type="text" class="form-control" id="identificationNumberC" 
                                            maxlength="10" minlength="4" placeholder="N�mero de Documento"
                                            path="identificationNumber"/>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="institucion-busqueda" class=" control-label">Entidad</label>
                                <form:select class="form-control" id="ccfC" path="ccf.code">
                                    <option value="">Seleccione...</option>
                                    <c:if test="${not empty foundationsList}">
                                        <c:forEach var="item" items="${foundationsList}">
                                            <c:if test="${item.code == creationUser.ccf.code}">
                                                <option label="${item.shortName}" value="${item.code}" selected="true"/>
                                            </c:if>
                                            <c:if test="${item.code != creationUser.ccf.code}">
                                                <option label="${item.shortName}" value="${item.code}"/>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                                </form:select>
                                <div class="clearfix"></div>
                            </div>
                        </div>         
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="estado" class="control-label">Estado de Usuario</label>
                                <form:select class="form-control" id="statusC" path="status" name="status">
                                    <option value="">Seleccione...</option>
                                    <c:if test="${not empty userStatusList}">
                                        <c:forEach var="item" items="${userStatusList}">
                                            <option value="${item.code}">${item.label}</option>
                                        </c:forEach>
                                    </c:if>
                                </form:select>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="perfil-usuario" class="control-label">Perfil</label>
                                <form:select class="form-control" id="profileidC" path="profile.id">
                                    <option value="">Seleccione...</option>
                                    <c:if test="${not empty userProfileList}">
                                        <c:forEach var="item" items="${userProfileList}">
                                            <c:if test="${item.adminStatus == 'F' && item.status == 'A'}">
                                                <c:if test="${item.id == creationUser.profile.id}">
                                                    <option label="${item.name}" value="${item.id}" selected="true"/>
                                                </c:if>
                                                <c:if test="${item.id != creationUser.profile.id}">
                                                    <option label="${item.name}" value="${item.id}"/>
                                                </c:if>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                                </form:select>                                    
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-offset-4 col-sm-8" style="text-align:right">
                            <button id="nuevoBtn" onclick="reinicioMsn()" type="button" class="btn btn-success">Nuevo</button>
                            <button type="submit" class="btn btn-success">Buscar</button>
                        </div>
                    </form:form>
                </legend>    
            </div>
        </div>

        <br/>
        <div class="panel panel-default">
            <div class="panel-heading"><h2>Detalle Consulta Usuarios</h2></div>
            <div class="panel-body">
                <div class=" table-responsive">
                    <table id="usuarios" class="table table-striped table-bordered" 
                           cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Primer Apellido</th>
                                <th>Primer Nombre</th>
                                <th>Segundo Nombre</th>
                                <th>N�mero de Identificaci�n</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>    
                        <tfoot>
                            <tr>
                                <th>Primer Apellido</th>
                                <th>Primer Nombre</th>
                                <th>Segundo Nombre</th>
                                <th>N�mero de Identificaci�n</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:if test="${not empty usersList}">
                                <c:forEach var="item" items="${usersList}">
                                    <c:if test="${item.admin == '0'}">
                                        <tr>
                                            <td>${item.firstSurname}</td>
                                            <td>${item.firstName}</td>
                                            <td>${item.secondName}</td> 
                                            <td>${item.identificationNumber}</td>
                                            <td>${item.status.label}</td>
                                            <td>
                                                <div align="center">
                                                    <div class="btn-group" role="group">
                                                        <button type="button" class="btn btn-info btn-xs"
                                                                data-toggle="modal" data-target="#detailModal"
                                                                onclick="loadUserDetail(${item.id})">
                                                            <i class="fa fa-id-card-o" aria-hidden="true"></i> Ver
                                                        </button>
                                                    </div>
                                                    <div class="btn-group" role="group">
                                                        <button id="edit${item.identificationNumber}" type="button" class="btn btn-warning btn-xs" onclick="editUser(${item.id})">
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> 
                                                            Editar
                                                        </button>
                                                    </div>
                                                       <div class="btn-group" role="group">
                                                        <c:if test="${item.status.label=='Por procesar'}">    
                                                            <form id="passwordUser" method="post" action="sendPassword.htm" >  
                                                                <button id="activate${item.identificationNumber}" type="submit" class="btn btn-success btn-xs" onclick="sendPassword()" >
                                                                    <input type="hidden" name="passw" value="${item.id}"/>
                                                                  <span class="glyphicon glyphicon-envelope"></span>
                                                                    Activar
                                                                </button>
                                                            </form>
                                                        </c:if> 
                                                    </div>   
                                                </div>
                                            </td>
                                        </tr>
                                    </c:if>
                                </c:forEach>
                            </c:if>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="modal fade" id="creationModal" role="dialog"
             aria-labelledby="creationModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                                onclick="resetForm()">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="creationModalLabel">Registro Usuario</h4>
                    </div>                                        
                    <div class="modal-body">
                        <c:if test="${not empty internalMsg}">
                            <div id="alert-${msgType}" class="alert alert-${msgType} alert-dismissible container2" role="alert">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-label="Close" onclick="resetForm()">
                                    <span aria-hidden="true">�</span>
                                </button>
                                <strong>${internalMsg}</strong>
                            </div>
                        </c:if>
                        <div class="container">
                            <div class="alert alert-warning alert-dismissible" id="myAlert">
                                <strong>�Atenci�n!</strong> Al inactivar el usuario no podr� iniciar sesi�n.
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <h1>Datos del Usuario</h1>
                        </div>
                        <form:form id="creationUser" method="post" action="/masterdata/guardar-usuario.htm" modelAttribute="creationUser">   
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="hidden" id="id" name="id" path="id"/>
                                    <input type="hidden" id="tipoFormulario" value="${tipoformulario}"/>
                                    <label for="institucion-busqueda" class=" control-label">Entidad(*)</label>
                                    <form:select class="form-control" id="ccfcode" path="ccf.code" required="required">
                                        <c:if test="${not empty foundationsList}">
                                            <option value="">Seleccione...</option>
                                            <c:forEach var="item" items="${foundationsList}">
                                                <c:if test="${item.code == creationUser.ccf.code}">
                                                    <form:option label="${item.shortName}" value="${item.code}" selected="true"/>
                                                </c:if>
                                                <c:if test="${item.code != creationUser.ccf.code}">
                                                    <form:option label="${item.shortName}" value="${item.code}"/>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="tipoDeDocumento" class=" control-label">Tipo de Documento(*)</label>
                                    <form:select onchange="tipoDeCaracter()" class="form-control istitutionOff" 
                                                 id="documentTypeid" path="documentType.id" required="required">
                                        <option value="">Seleccione...</option>
                                            <c:if test="${not empty documentTypes}">
                                                <c:forEach var="item" items="${documentTypes}">
                                                    <c:if test="${item.id == '2' || item.id == '3' || item.id == '5'}">
                                                        <form:option label="${item.name}" value="${item.id}"/>
                                                    </c:if>
                                                </c:forEach>
                                            </c:if>           
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="numeroDeDocumento" class="control-label">N�mero de Documento(*)</label>
                                    <form:input data-rule-intrule="true" type="text" class="form-control istitutionOff" id="identificationNumber" 
                                                placeholder="N�mero de Documento" required="required" 
                                                onkeypress="return isNumberKey(event)"
                                                path="identificationNumber" minlength="4" maxlength="10"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="pnombreusuario" class="control-label">Primer Nombre(*)</label>
                                    <form:input type="text" class="form-control" id="firstName" 
                                                placeholder="Primer Nombre" required="required"
                                                maxlength="30" path="firstName"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="snombreusuario" class="control-label">Segundo Nombre</label>
                                    <form:input type="text" class="form-control" id="secondName" 
                                                placeholder="Segundo Nombre"
                                                maxlength="30" path="secondName"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="papellidousuario" class="control-label">Primer Apellido(*)</label>
                                    <form:input type="text" class="form-control" id="firstSurname" 
                                                placeholder="Primer Apellido" required="required"
                                                maxlength="30" path="firstSurname"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="sapellidousuario" class="control-label">Segundo Apellido</label>
                                    <form:input type="text" class="form-control" id="secondSurname" 
                                                placeholder="Segundo Apellido"
                                                maxlength="30" path="secondSurname"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="direccion-predio" class="control-label">Direcci�n(*)</label>
                                    <form:input type="text" class="form-control" id="address" maxlength="100" 
                                                placeholder="Direcci�n Predio" path="address" required="required"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="departamento" class="control-label">Departamento(*)</label>
                                    <form:select class="form-control disable" id="stateid" 
                                                 path="state.id" required="required">
                                        <option id="selectedDep" value="">Seleccione...</option>
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="municipio" class="control-label">Municipio(*)</label>
                                    <form:hidden id="transCityId" path="transCityId" />
                                    <form:select class="form-control disable" id="cityid"  onchange="setTransCityId();"
                                                 path="city.id" required="required">
                                    </form:select>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="telefonousuario" class="control-label">Tel�fono de Contacto(*)</label>
                                    <form:input data-rule-celrule="true" type="text" class="form-control" id="phoneNumber"
                                                placeholder="Tel�fono Contacto" required="required"
                                                path="phoneNumber" minlength="7" maxlength="10" onkeypress="return isNumberKey(event)"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="emailusuario" class="control-label">E-mail de Contacto(*)</label>
                                    <form:input data-rule-emailtld="true" class="form-control" id="email" 
                                                placeholder="E-Mail Contacto" required="required"
                                                path="email" autocomplete="off" maxlength="50"/>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <c:if test="${not empty userMinistryOfLabor}">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="user-name-usuario" class="control-label">Nombre de Usuario(*)</label>
                                        <form:input readonly="true" type="text" class="form-control" id="username" 
                                                    placeholder="Nombre de Usuario" required="required"
                                                    path="username" autocomplete="off" />
                                        <label class="control-label"><font size="2" color="blue">*S�lo aplica para los usuarios de Ministerio</font></label>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </c:if>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="perfil-usuario" class="control-label">Perfil de Usuario(*)</label>
                                    <form:select class="form-control" id="profileid" path="profile.id" required="required">
                                        <option value="">Seleccione...</option>
                                        <c:if test="${not empty userProfileList}">
                                            <c:forEach var="item" items="${userProfileList}">
                                                <c:if test="${item.adminStatus == 'F' && item.status == 'A'}">
                                                    <c:if test="${item.id == creationUser.profile.id}">
                                                        <option label="${item.name}" value="${item.id}" selected="true">
                                                            ${item.name}	
                                                        </option>                                                        
                                                    </c:if>
                                                    <c:if test="${item.id != creationUser.profile.id}">
                                                        <option label="${item.name}" value="${item.id}">
                                                            ${item.name}	
                                                        </option>                                                      
                                                    </c:if>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                    </form:select>                                    
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 estadoUsuario">
                                <div class="form-group">
                                    <div>
                                        <label for="estadoUsuario" class="control-label">Estado de Usuario(*)  </label>
                                        <form:select onchange="cambioEstado()" class="form-control valorPerfil" id="status" name="status" path="status">
                                            <c:if test="${not empty userStatusList}">
                                                <c:forEach var="item" items="${userStatusList}">
                                                    <c:if test="${item.code != 'T'}">
                                                        <c:if test="${item == creationUser.status}">
                                                            <form:option label="${item.label}" value="${item}" selected="true"/>
                                                        </c:if>
                                                        <c:if test="${item != creationUser.status}">
                                                            <form:option label="${item.label}" value="${item}"/>
                                                        </c:if>
                                                    </c:if>
                                                </c:forEach>
                                            </c:if>
                                        </form:select>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 estadoUsuarioT">
                                <div class="form-group">
                                    <div>
                                        <label for="estadoUsuario" class=" control-label">Estado de Usuario(*)</label>
                                        <input type="text" value="Por procesar" class="form-control valorPerfil" id="statusT" readonly="readonly"/>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-12" style="text-align:right">
                                <br/>
                                <button id="saveButton" class="btn btn-success" type="submit"
                                        data-btn-ok-label="Confirmar" data-btn-cancel-label="Cancelar"
                                        data-title="Est� seguro?" 
                                        data-toggle="confirmation" data-placement="top" 
                                        confirmation-callback>Guardar   
                                </button>
                                <button onclick="resetForm()" type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                            </div> 
                        </form:form>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="detailModal" role="dialog" 
             aria-labelledby="creationModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                                onclick="resetForm()">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="creationModalLabel">Detalles de Usuario</h4>
                    </div>
                    <div class="modal-body" style="height: 80vh">
                        <iframe id="detailArea" width="100%" height="100%" style="border: 0">
                        </iframe>
                    </div>
                    <div class="modal-footer">
                        <button onclick="resetForm()" type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>

        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/jquery.dataTables.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/dataTables.buttons.min.js"></script>
        <script src="resources/js/buttons.flash.min.js"></script>
        <script src="resources/js/jszip.min.js"></script>
        <script src="resources/js/pdfmake.min.js"></script>
        <script src="resources/js/vfs_fonts.js"></script>
        <script src="resources/js/buttons.html5.min.js"></script>
        <script src="resources/js/buttons.print.min.js"></script>
        <script src="resources/js/dataTables.bootstrap.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>
        <script src="resources/js/bootstrap-confirmation.js"></script>
        <script src="resources/js/jquery.validate.min.js"></script>
        <script src="resources/js/tooltipster.bundle.js"></script>
        <script src="resources/js/custom-functionalities.js"></script>
        <script>

            $(document).ready(function () {

                $('.container').hide();

                $('#usuarios').DataTable({
                    "language": {
                        "url": "resources/js/Spanish.json"
                    },
                    "initComplete": function (settings, json) {
                        $(".dt-buttons").each(function (index) {
                            console.log(index + ": " + $(this).text());
                            if ($(this).find('.exportar').length === 0) {
                                $(this).prepend('<label class="exportar">Exportar a:&nbsp;&nbsp;</label>');
                            }
                        });
                    },
                    "bFilter": false,
                    "bInfo": false,
                    dom: 'Blfrtip',
                    buttons: [
                        {
                            extend: 'csv',
                            fieldBoundary: '',
                            footer: false,
                            titleAttr: 'CSV',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            }
                        },
                        {
                            extend: 'excel',
                            footer: false,
                            titleAttr: 'EXCEL',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            }
                        },
                        {
                            extend: 'pdf',
                            footer: false,
                            titleAttr: 'PDF',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            }
                        },
                        {
                            extend: 'print',
                            footer: false,
                            titleAttr: 'COPY',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4]
                            }
                        }
                    ]
                });
                
                loadStates("stateid", "cityid");
                loadCitiesByState("stateid", "cityid");
                
                <c:if test="${not empty internalMsg}">
                    <c:if test="${not empty jsonUser}">
                        var json = jQuery.parseJSON('<c:out value="${jsonUser}" escapeXml="false"/>');
                        foundComponents(json, '');
                        loadCitiesAndSelect($("#stateid").val(),"stateid", "cityid","transCityId");
                    </c:if>
                        
                    if($("#status").val() === null){
                        $('.estadoUsuarioT').show();
                        $('.estadoUsuario').hide();
                    }else{
                        $('.estadoUsuarioT').hide();
                        $('.estadoUsuario').show();
                    }

                    if ($('#tipoFormulario').val() === "nuevo") {
                        $('.estadoUsuario').hide();
                        $('.estadoUsuarioT').hide();
                        $('#estadoUsuario').hide();
                        $('#id').val('');
                    } else {
                        $('#estadoUsuario').show();
                    }
                    
                    if ($("#documentTypeid").val() === '2') {
                        $("#identificationNumber").attr("data-rule-alpharule", "false");
                        $("#identificationNumber").attr("data-rule-intrule", "true");
                        $("#identificationNumber").attr("onkeypress", "return isNumberKey(event)");
                    } else if($("#documentTypeid").val() === '5' || $("#documentTypeid").val() === '3'){
                        $("#identificationNumber").attr("data-rule-intrule", "false");
                        $("#identificationNumber").attr("data-rule-alpharule", "true");
                        $("#identificationNumber").removeAttr("onkeypress");
                    } else {
                        $("#identificationNumber").val("");
                    }

                    $('#creationModal').modal('show');

                </c:if>
                <c:if test="${not empty creationUser.id}">
                    $('#creationModal').modal('show');
                </c:if>
                    
            });

            $("#email").keyup(function () {
                $("#username").val($("#email").val().split("@")[0]);
            });
            
            function setTransCityId(){
                
                $("#transCityId").val($("#cityid").val());
            }

            function reinicioMsn() {
                
                $('#creationModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                
                creationUser.reset();
                $('#id').val('');
                $('#tipoFormulario').val("nuevo");
                $('.container').hide();
                $('.estadoUsuario').hide();
                $('.estadoUsuarioT').hide();
                $('#internal').hide();
                $('.container2').hide();
                loadStates("stateid", "cityid");
                loadCitiesByState("stateid", "cityid");
            }

            function loadUserDetail(id) {
                $('#detailArea').attr("src",
                        'detalle-consulta-usuarios.htm?id=' + id);
            }

            function cambioEstado() {

                valor = $(".valorPerfil").val();

                if (valor === "I") {
                    $('.container').show("fast");
                } else {
                    $('.container').hide("fast");
                }
            }

            function resetForm() {
                creationUser.reset();
                $("#id").val("");
            }
            
            function editUser(id) {
                
                $('#creationModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                
                onEdit("/masterdata/obtener-usuario.htm?id=" + id,
                        "creationModal", afterEdit);
            }

            function afterEdit() {
                
                if($("#status").val() === null){
                    $('.estadoUsuarioT').show();
                    $('.estadoUsuario').hide();
                }else{
                    $('.estadoUsuarioT').hide();
                    $('.estadoUsuario').show();
                }

                if ($("#documentTypeid").val() === '2') {
                    $("#identificationNumber").attr("data-rule-alpharule", "false");
                    $("#identificationNumber").attr("data-rule-intrule", "true");
                    $("#identificationNumber").attr("onkeypress", "return isNumberKey(event)");
                } else if($("#documentTypeid").val() === '5' || $("#documentTypeid").val() === '3'){
                    $("#identificationNumber").attr("data-rule-intrule", "false");
                    $("#identificationNumber").attr("data-rule-alpharule", "true");
                    $("#identificationNumber").removeAttr("onkeypress");
                } else {
                    $("#identificationNumber").val("");
                }
                
                $('.container').hide();
                $('#tipoFormulario').val("editar");
                $('#estadoUsuario').show();
                $('.container2').hide();
                
                loadCitiesAndSelect($("#stateid").val(),"stateid", "cityid","transCityId");
            }
            
            function tipoDeCaracter() {
                
                if ($("#documentTypeid").val() === '2') {
                    $("#identificationNumber").attr("data-rule-alpharule", "false");
                    $("#identificationNumber").attr("data-rule-intrule", "true");
                    $("#identificationNumber").attr("onkeypress", "return isNumberKey(event)");
                    $("#identificationNumber").val("");
                } else if($("#documentTypeid").val() === '5' || $("#documentTypeid").val() === '3'){
                    $("#identificationNumber").attr("data-rule-intrule", "false");
                    $("#identificationNumber").attr("data-rule-alpharule", "true");
                    $("#identificationNumber").removeAttr("onkeypress");
                    $("#identificationNumber").val("");
                } else {
                    $("#identificationNumber").val("");
                }
            }
          
            $('#saveButton').confirmation({
                onConfirm: function (event, element) {
                    if (validateForm("creationUser")) {
                        $('#creationUser').submit();
                    }
                }
            });

            $.validator.addMethod('emailtld', function (val, elem) {
                var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

                if (!filter.test(val)) {
                    return false;
                } else {
                    return true;
                }
            }, 'Por favor, escriba una direcci�n de correo v�lida.');
            
            $.validator.addMethod('intrule', function (val, elem) {
                var filter = /^([0-9]{4,10})*$/;

                if (!filter.test(val)) {
                    return false;
                } else {
                    return true;
                }
            }, 'Por favor, escriba un numero de identificaci�n v�lido.');
            
            $.validator.addMethod('celrule', function (val, elem) {
                var filter = /^([0-9]{7,10})*$/;

                if (!filter.test(val)) {
                    return false;
                } else {
                    return true;
                }
            }, 'Por favor, escriba un tel�fono de contacto v�lido.');
            
            $.validator.addMethod('alpharule', function (val, elem) {
                var filter = /^([0-9a-zA-Z]{4,10})*$/;

                if (!filter.test(val)) {
                    return false;
                } else {
                    return true;
                }
            }, 'Por favor, escriba un numero de identificaci�n v�lido.');
            
            function loadStates(deptoId, mnpoId) {
                <c:if test="${not empty stateList}">
                    <c:forEach var="item" items="${stateList}" varStatus="iter">
                        <c:if test="${iter.index == 0}">
                            jQuery("<option>").attr("value", '${item.id}').
                                    text('${item.name}').
                                    appendTo("#" + deptoId);
                            loadCities("${item.id}", deptoId, mnpoId);
                        </c:if>
                        <c:if test="${iter.index > 0}">
                            jQuery("<option>").attr("value", '${item.id}').
                                    text('${item.name}').appendTo("#" + deptoId);
                        </c:if>
                    </c:forEach>
                </c:if>
            }

            function loadCitiesByState(deptoId, mnpoId) {
                jQuery("#" + deptoId).change(function () {
                    jQuery("#" + mnpoId).html('');
                    var stateCode = jQuery("#" + deptoId).val();
                    loadCities(stateCode, deptoId, mnpoId);
                });
            }

            function loadCitiesAndSelect(stateCode, deptoId, mnpoId,transId){
                var transValue = $("#"+transId).val();
                jQuery("#" + mnpoId).find('option').remove();
                jQuery("<option>").attr("value", '').
                        text('Seleccione...').appendTo("#" + mnpoId);
                <c:if test="${not empty citiesList}">
                    <c:forEach items="${citiesList}" var="item" >
                        var code = "<c:out value="${item.state.id}"/>";
                        if (code === stateCode) {
                            if ('${item.id}' === transValue) {
                               jQuery("<option>").attr("value", '${item.id}').
                                    attr("selected", "false").
                                    text('${item.name}').appendTo("#" + mnpoId);
                            }  
                            if ('${item.id}' != transValue) {
                               jQuery("<option>").attr("value", '${item.id}').
                                    text('${item.name}').appendTo("#" + mnpoId);
                            }  
                            
                        }
                    </c:forEach>
                </c:if>
            }
            
            function loadCities(stateCode, deptoId, mnpoId) {
                jQuery("#" + mnpoId).find('option').remove();
                jQuery("<option>").attr("value", '').
                        text('Seleccione...').appendTo("#" + mnpoId);
                <c:if test="${not empty citiesList}">
                    <c:forEach items="${citiesList}" var="item" >
                        var code = "<c:out value="${item.state.id}"/>";
                        if (code === stateCode) {
                            jQuery("<option>").attr("value", '${item.id}').
                                    text('${item.name}').appendTo("#" + mnpoId);
                        }
                    </c:forEach>
                </c:if>
            }
            
            function loadCitiesJson() {

                jQuery("#cityid").html('');
                var stateCode = jQuery("#stateid").val();
                loadCities(stateCode, "cityid");
            }
        </script>
        <style>
            .buttons-csv{
                display: inline-block;
                background-image:url(resources/images/csv.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-csv span{
                opacity: 0;
            }
            .buttons-excel{
                display: inline-block;
                background-image:url(resources/images/excel.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-excel span{
                opacity: 0;
            }
            .buttons-pdf{
                display: inline-block;
                background-image:url(resources/images/pdf.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-pdf span{
                opacity: 0;
            }
            .buttons-print{
                display: inline-block;
                background-image:url(resources/images/print.png);
                cursor:pointer !important;
                width: 32px !important;
                height: 32px !important;
                border: none !important;
            }
            .buttons-print span{
                opacity: 0;
            }

            div.dt-buttons {
                float: right;
                margin-left:10px;
            }
        </style>
    </body>
</html>
