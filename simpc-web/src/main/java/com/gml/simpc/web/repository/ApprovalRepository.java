/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ApprovalRepository.java
 * Created on: 2016/11/30, 01:17:41 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository;

import com.gml.simpc.api.entity.Approval;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repositorio de aprobaciones.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface ApprovalRepository extends JpaRepository<Approval, Long> {

    /**
     * Este query trae los sedes dada una instituci&oacute;n
     *
     * @param institution
     *
     * @return
     */
    List<Approval> findByInstitution(Long institution);
}
