<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Informaci�n de Recursos FOSFEC</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/css/datepicker.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster.bundle.min.css" rel="stylesheet"/>
        <link href="resources/css/tooltipster-sideTip-shadow.min.css" rel="stylesheet"/>
        <link href="resources/css/morris.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <div class="col-sm-3" style="padding:0px;">
                        <img src="resources/images/logo_mpc.png" width="100%" alt=""/>
                    </div>
                    <div class="col-sm-8">
                        <a class="navbar-brand" href="/masterdata/index.htm">Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</a>
                    </div>
                </div>
                <div class="navbar-nav navbar-form credenciales pull-right">
                    <table>
                        <tr>
                            <td>
                                <strong>Bienvenido</strong> 
                            </td>
                            <td style="width: 120px; text-align: right">
                                <a id="" href="/masterdata/index.htm" class="btn btn-warning btn-xs">Volver al inicio</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </nav>
        <div class="container">
            <h1>Informaci&oacute;n de Recursos FOSFEC</h1>
            <!-- main area -->
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Filtros de B&uacute;squeda</h2></div>
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li>
                            <a data-toggle="tab" href="#recursosCcf" onclick="borraGrafico()" id="tabRecursos">Consulta consolidada ejecuci&oacute;n de recurso</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#detalleCcf" onclick="borraGrafico()"  id="tabDetalle">Consulta detallada ejecuci&oacute;n de recursos</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="alert-area" ></div>
                        <div id="recursosCcf" class="tab-pane fade in ">
                            <div class="panel-body">
                                <form  action="/masterdata/recursosFosfec.htm" method="GET"  >
                                    <input type="hidden" id="activateBtn" value="${activateBtn}">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="ccf" class=" control-label">Caja de Compensaci�n</label>
                                            <select class="form-control" id="ccf" path="ccfCode" name="ccf">
                                                <option label="Todas las cajas de compensaci�n" value="">Todas las cajas de compensaci�n</option>
                                                <c:if test="${not empty ccfs}">
                                                    <c:forEach var="item" items="${ccfs}">
                                                        <c:if test="${item.isCCF == 'V'}">
                                                            <option <c:if test="${item.code eq ccfSelected}">selected="selected"</c:if> value="${item.code}">${item.shortName}</option>
                                                        </c:if>
                                                    </c:forEach>
                                                </c:if>
                                            </select>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 ">
                                        <div class="form-group">
                                            <label for="year" class="control-label">A�o</label>
                                            <select class="form-control"  id="anio" name="anio" required="true">
                                                <option label="Seleccione..."  value="">Seleccione...</option>
                                                <c:if test="${not empty anios}">
                                                    <c:forEach var="item" items="${anios}">
                                                        <option <c:if test="${item eq yearSelected}">selected="selected"</c:if>  value="${item}" >${item}</option>
                                                    </c:forEach>
                                                </c:if>
                                            </select>
                                        </div>
                                    </div> 
                                    <div class="col-sm-4 ">
                                        <div class="form-group">
                                            <label for="month" class="control-label">Mes</label>
                                            <input type="hidden" id="mes" name="mes" value="">
                                            <select disabled="disabled" class="form-control">
                                                <option label="Todos los meses">Todos los meses</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12" style="text-align:right">
                                        <button type="submit" class="btn btn-success">Buscar</button>    
                                    </div>
                                </form>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading"><h2>Resultado de la B&uacute;squeda</h2></div>
                                <div class="panel-body">
                                    <div class=" table-responsive col-md-12">
                                        <center>
                                            <button type="button" class="btn btn-default desableBtn"
                                                    data-toggle="modal" data-target="#graphicModal">  
                                                <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                                                Ver gr&aacute;fico
                                            </button>
                                        </center>
                                        <table id="tabla1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>MES</th>
                                                    <th>SALUD</th>
                                                    <th>PENSI&Oacute;N</th>
                                                    <th>CUOTA MONETARIA</th>
                                                    <th>BONO</th>
                                                    <th>TOTAL</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>MES</th>
                                                    <th>SALUD</th>
                                                    <th>PENSI&Oacute;N</th>
                                                    <th>CUOTA MONETARIA</th>
                                                    <th>BONO</th>
                                                    <th>TOTAL</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <c:if test="${not empty informationList}">
                                                    <c:forEach items="${informationList}" var="info" begin="0">
                                                        <tr> 
                                                            <td class="val">${info.getColumn(0)}</td>
                                                            <td class="val" align="right">${info.getColumn(1)}</td>
                                                            <td class="val" align="right">${info.getColumn(2)}</td>
                                                            <td class="val" align="right">${info.getColumn(3)}</td>
                                                            <td class="val" align="right">${info.getColumn(4)}</td>
                                                            <td class="val" align="right">${info.getColumn(5)}</td>
                                                        </tr>   
                                                    </c:forEach>  
                                                </c:if>
                                            </tbody> 
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="detalleCcf" class="tab-pane fade ">
                            <div class="panel-body">
                                <form:form  action="/masterdata/detalleCcf.htm" method ="get" >
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="ccfDetailLabel" class="control-label">Caja de compensaci&oacute;n</label>
                                            <input type="hidden"  name="ccftab2" id="ccftab2" value="">
                                            <select disabled="disabled" class="form-control"> 
                                                <option  label="Todas las Cajas">Todas las Cajas</option>
                                            </select>   
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 ">
                                        <div class="form-group">
                                            <label for="year" class="control-label">A�o</label>
                                            <select class="form-control"  id="aniotab2" name="aniotab2" required="true">
                                                <option  label="Seleccione..."  value="" ></option>

                                                <c:if test="${not empty anios}">
                                                    <c:forEach var="item" items="${anios}">
                                                        <option <c:if test="${item eq yearSelected2}">selected="selected"</c:if> value="${item}" >${item}</option>
                                                    </c:forEach>
                                                </c:if>
                                            </select>
                                        </div>
                                    </div> 
                                    <div class="col-sm-4 ">
                                        <div class="form-group">
                                            <label for="monthtab2" class="control-label">Mes</label>
                                            <select class="form-control" id="mestab2" name="mestab2"  >
                                                <option label="Todos los Meses" value=""></option>
                                                <option <c:if test="${monthSelected2 == '01'}">selected="selected"</c:if> value="01">Enero</option>
                                                <option <c:if test="${monthSelected2 == '02'}">selected="selected"</c:if> value="02">Febrero</option>
                                                <option <c:if test="${monthSelected2 == '03'}">selected="selected"</c:if> value="03">Marzo</option>
                                                <option <c:if test="${monthSelected2 == '04'}">selected="selected"</c:if> value="04">Abril</option>
                                                <option <c:if test="${monthSelected2 == '05'}">selected="selected"</c:if> value="05">Mayo</option>
                                                <option <c:if test="${monthSelected2 == '06'}">selected="selected"</c:if> value="06">Junio</option>
                                                <option <c:if test="${monthSelected2 == '07'}">selected="selected"</c:if> value="07">Julio</option>
                                                <option <c:if test="${monthSelected2 == '08'}">selected="selected"</c:if> value="08">Agosto</option>
                                                <option <c:if test="${monthSelected2 == '09'}">selected="selected"</c:if> value="09">Septiembre</option>
                                                <option <c:if test="${monthSelected2 == '10'}">selected="selected"</c:if> value="10">Octubre</option>
                                                <option <c:if test="${monthSelected2 == '11'}">selected="selected"</c:if> value="11">Noviembre</option>
                                                <option <c:if test="${monthSelected2 == '12'}">selected="selected"</c:if> value="12">Diciembre</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12" style="text-align:right">
                                        <button type="submit" class="btn btn-success">Buscar</button>    
                                    </div>
                                </form:form>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading"><h2>Resultado de la B&uacute;squeda</h2></div>
                                <div class="panel-body">
                                    <div class=" table-responsive col-md-12">
                                        <center>
                                            <button type="button" class="btn btn-default desableBtn"
                                                    data-toggle="modal" data-target="#graphicModal">  
                                                <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                                                Ver gr&aacute;fico
                                            </button>
                                        </center>
                                        <table id="tabla2" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>CAJA DE COMPENSACI&Oacute;N</th>
                                                    <th>SALUD</th>
                                                    <th>PENSI&Oacute;N</th>
                                                    <th>CUOTA MONETARIA</th>
                                                    <th>BONO</th>
                                                    <th>TOTAL</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>CAJA DE COMPENSACI&Oacute;N</th>
                                                    <th>SALUD</th>
                                                    <th>PENSI&Oacute;N</th>
                                                    <th>CUOTA MONETARIA</th>
                                                    <th>BONO</th>
                                                    <th>TOTAL</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <c:if test="${not empty detalleList}">
                                                    <c:forEach items="${detalleList}" var="detalle" begin="0">
                                                        <tr> 
                                                            <td class="mesFos">${detalle.getColumn(0)}</td>
                                                            <td class="val" align="right">${detalle.getColumn(1)}</td>
                                                            <td class="val" align="right">${detalle.getColumn(2)}</td>
                                                            <td class="val" align="right">${detalle.getColumn(3)}</td>
                                                            <td class="val" align="right">${detalle.getColumn(4)}</td>
                                                            <td class="val" align="right">${detalle.getColumn(5)}</td>
                                                        </tr>   
                                                    </c:forEach>  
                                                </c:if>
                                            </tbody> 
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        </div><!-- /.col-xs-12 main -->
    </div><!--/.container-->
    <div class="clearfix"></div> 
    <div class="modal fade" id="graphicModal" tabindex="-1" role="dialog" aria-labelledby="graphicModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12  " id="graphicContainer2">
                            <h4 class="modal-title" id="graphicModalLabel"><Strong><center>${titulo}</center></Strong></h4>
                            <div id="graphicContainer" ></div>
                            <br>
                            <div id="legend" class="graphic-legend inline-block"></div>
                            <br> <br> <br>
                             <Strong> ${titulografica}</Strong>
                            <div class=" table-responsive col-md-12">
                                <table id="tablag" class=" table-striped table-bordered " cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <c:if test="${not empty titleList}">
                                                <c:forEach items="${titleList}" var="info" begin="0">
                                                    <th>${info}</th>
                                                </c:forEach>  
                                            </c:if>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:if test="${not empty informationList}">
                                            <c:forEach items="${informationList}" var="info" begin="0">
                                                <tr> 
                                                    <td class="val" id="text">${info.getColumn(0)}</td>
                                                    <td class="val" align="right">${info.getColumn(1)}</td>
                                                    <td class="val" align="right">${info.getColumn(2)}</td>
                                                    <td class="val" align="right">${info.getColumn(3)}</td>
                                                    <td class="val" align="right">${info.getColumn(4)}</td>
                                                </tr>   
                                            </c:forEach>  
                                        </c:if>
                                                <c:if test="${not empty graphicList2}">
                                            <c:forEach items="${graphicList2}" var="info" begin="0">
                                                <tr> 
                                                    <td>${info.ccf}</td>
                                                    <td class="val" align="right">${info.total}</td>
                                                </tr>   
                                            </c:forEach>  
                                        </c:if>
                                    </tbody> 
                                </table>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="printGraphic()">Exportar PDF</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <!--Footer -->
    <div class="footer">
        <img src="resources/images/bg_banderin.jpg" width="100%"/> 
        <div class="clearfix"></div> 
    </div>
    <!--Ends footer -->

    <!-- jQuery -->
    <script src="resources/js/jquery-1.12.3.js"></script>
    <script src="resources/js/jquery.dataTables.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/dataTables.buttons.min.js"></script>
    <script src="resources/js/buttons.flash.min.js"></script>
    <script src="resources/js/jszip.min.js"></script>
    <script src="resources/js/pdfmake.min.js"></script>
    <script src="resources/js/vfs_fonts.js"></script>
    <script src="resources/js/buttons.html5.min.js"></script>
    <script src="resources/js/buttons.print.min.js"></script>
    <script src="resources/js/dataTables.bootstrap.min.js"></script>
    <script src="resources/js/bootstrap-datepicker.js"></script>
    <script src="resources/js/bootstrap-confirmation.js"></script>
    <script src="resources/js/jquery.validate.min.js"></script>
    <script src="resources/js/tooltipster.bundle.js"></script>
    <script src="resources/js/custom-functionalities.js"></script>
    <script src="resources/js/raphael.min.js"></script>
    <script src="resources/js/morris.js"></script>
    <script>

        $(document).ready(function () {
            
            if($("#activateBtn").val() === ""){
                $(".desableBtn").attr("disabled", "desabled");
            } else {
                $(".desableBtn").removeAttr("disabled");
            }
            
            $(${tab}).tab('show');

            $('.table').DataTable({
                "language": {
                    "url": "resources/js/Spanish.json"
                },
                "initComplete": function (settings, json) {
                    $(".dt-buttons").each(function (index) {
                        console.log(index + ": " + $(this).text());
                        if ($(this).find('.exportar').length === 0) {
                            $(this).prepend('<label class="exportar">Exportar a:&nbsp;&nbsp;</label>');
                        }
                    });
                },
                "bFilter": true,
                "bInfo": false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'csv',
                        fieldBoundary: '',
                        text: '<i class="fa fa-file-text-o"></i>',
                        titleAttr: 'CSV'

                    },
                    {
                        extend: 'excel',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        titleAttr: 'EXCEL'
                    },
                    {
                        extend: 'pdf',
                        text: '<i class="fa fa-file-pdf-o"></i>',
                        titleAttr: 'PDF'

                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-files-o"></i>',
                        titleAttr: 'COPY'
                    }
                ]

            });


            $('#tablag').DataTable({
                "language": {
                    "url": "resources/js/Spanish.json"
                },
//                                "bFilter": false,
//                                "bInfo": false,
                dom: 'lt',
            });
            
            <c:if test="${not empty internalMsg}">
                                $('#creationModal').modal('show');
            </c:if>

            $('#graphicModal').on('shown.bs.modal', function () {

                $(function () {

                    var json = '<c:out value="${graphicData}" escapeXml="false"/>';
                    var type = '<c:out value="${graphicType}" escapeXml="false"/>';

                    $("#graphicContainer").empty();
                    $('#legend').empty();

                    if (type === 'B') {

                        var browsersChart = Morris.Bar({
                            element: 'graphicContainer',
                            data: jQuery.parseJSON(json),
                            xkey: 'ccf',
                            ykeys: ['total'],
                            labels: ['Total'],
                            stacked: true,
                            hideHover: 'auto',
                            barGap: 4,
                            barColors: ['blue'],
                            barSizeRatio: 0.55,
                            xLabelAngle: 70,
                            resize: true
                        });

                        browsersChart.options.data.forEach(function (labels, i) {
                            var legendItem = $('<span></span>').text(browsersChart.options.labels[i]).prepend('<br><span>&nbsp;</span>');
                            legendItem.find('span')
                                    .css('backgroundColor', browsersChart.options.barColors[i])
                                    .css('width', '20px')
                                    .css('display', 'inline-block')
                                    .css('margin', '5px');
                            $('#legend').append(legendItem)
                        });
                        
                    } else if (type === 'L') {
                        var browsersChart = Morris.Line({
                            element: 'graphicContainer',
                            lineColors: ['#a6d000', '#0070e7', '#e700b5', '#ffab17'],
                            data: jQuery.parseJSON(json),
                            xkey: 'month',
                            ykeys: ['health', 'pension', 'monetaryQuota', 'bonds'],
                            labels: ['Salud', 'Pension', 'Cuota Monetaria', 'Bono'],
                            stacked: true,
                            hideHover: 'auto',
                            barGap: 4,
                            barSizeRatio: 0.55,
                            resize: true,
                            xLabelAngle: 20,
                            parseTime: false
                        });

                        browsersChart.options.data.forEach(function (labels, i) {
                            var legendItem = $('<span></span>').text(browsersChart.options.labels[i]).prepend('<br><span>&nbsp;</span>');
                            legendItem.find('span')
                                    .css('backgroundColor', browsersChart.options.lineColors[i])
                                    .css('width', '20px')
                                    .css('display', 'inline-block')
                                    .css('margin', '5px');
                            $('#legend').append(legendItem)
                        });

                    } else {
                        var browsersChart = Morris.Donut({
                            element: 'graphicContainer',
                            data: jQuery.parseJSON(json),
                            stacked: true,
                            resize: true
                        });
                    }
                });
            });
            
            $(".val").each(function(){
                if($.trim($(this).text()) === "01"){
                    $(this).text("Enero");
                }   
                else if ($.trim($(this).text()) === "02"){
                    $(this).text("Febrero");
                }   
                else if ($.trim($(this).text()) === "03"){
                    $(this).text("Marzo");
                }   
                else if ($.trim($(this).text()) === "04"){
                    $(this).text("Abril");
                }   
                else if ($.trim($(this).text()) === "05"){
                    $(this).text("Mayo");
                }   
                else if ($.trim($(this).text()) === "06"){
                    $(this).text("Junio");
                }   
                else if ($.trim($(this).text()) === "07"){
                    $(this).text("Julio");
                }   
                else if ($.trim($(this).text()) === "08"){
                    $(this).text("Agosto");
                }   
                else if ($.trim($(this).text()) === "09"){
                    $(this).text("Septiembre");
                }   
                else if ($.trim($(this).text()) === "10"){
                    $(this).text("Octubre");
                }   
                else if ($.trim($(this).text()) === "11"){
                    $(this).text("Noviembre");
                }
                else if ($.trim($(this).text()) === "12"){
                    $(this).text("Diciembre");
                }
                else {
                    $(this).text("$" + number_format($.trim($(this).text())));
                }   
            });
        });

        function borraGrafico() {
            $("#graphicContainer2").empty();
            $(".desableBtn").attr("disabled", "desabled");
        }

        function printGraphic() {
            var objeto = document.getElementById('graphicContainer2');
            var ventana = window.open('', '_blank');
            ventana.document.write(objeto.innerHTML);
            ventana.document.close(); 
            ventana.print();
            ventana.close();
        }

        function number_format(amount, decimals) {

            amount += '';
            amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));

            decimals = decimals || 0;

            if (isNaN(amount) || amount === 0){ 
                return parseFloat(0).toFixed(decimals);
            }

            amount = '' + amount.toFixed(decimals);

            var amount_parts = amount.split('.'),
                regexp = /(\d+)(\d{3})/;

            while (regexp.test(amount_parts[0])){
                amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
            }
            return amount_parts.join('.');
        }

    </script>
    <style>
        .buttons-csv{
            display: inline-block;
            background-image:url(resources/images/csv.png);
            cursor:pointer !important;
            width: 32px !important;
            height: 32px !important;
            border: none !important;
        }
        .buttons-csv span{
            opacity: 0;
        }
        .buttons-excel{
            display: inline-block;
            background-image:url(resources/images/excel.png);
            cursor:pointer !important;
            width: 32px !important;
            height: 32px !important;
            border: none !important;
        }
        .buttons-excel span{
            opacity: 0;
        }
        .buttons-pdf{
            display: inline-block;
            background-image:url(resources/images/pdf.png);
            cursor:pointer !important;
            width: 32px !important;
            height: 32px !important;
            border: none !important;
        }
        .buttons-pdf span{
            opacity: 0;
        }
        .buttons-print{
            display: inline-block;
            background-image:url(resources/images/print.png);
            cursor:pointer !important;
            width: 32px !important;
            height: 32px !important;
            border: none !important;
        }
        .buttons-print span{
            opacity: 0;
        }
        div.dt-buttons {
            float: right;
            margin-left:10px;
        }
        .graphic-legend > span {
            display: inline-block;
            margin-right: 25px;
            margin-bottom: 10px;
            font-size: 13px;
        }
        .graphic-legend > span:last-child {
            margin-right: 0;
        }
        #graphicContainer {
            max-height: 280px;
            margin-top: 20px;
            margin-bottom: 20px;
        }
        .mbox {   
            display: inline-block;
            width: 10px;
            height: 10px;
            margin: 10px 55px 10px 25px;
            padding-left: 4px;
        }

    </style>
</body>
</html>
