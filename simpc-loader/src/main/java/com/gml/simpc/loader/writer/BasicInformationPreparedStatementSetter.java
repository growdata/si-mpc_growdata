/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: TrainingPreparedStatementSetter.java
 * Created on: 2017/01/20, 12:16:58 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.writer;

import com.gml.simpc.api.dto.BasicInformationDto;
import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.springframework.batch.item.database.ItemPreparedStatementSetter;

/**
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica Jim&eacute;nez Rozo</a>
 */
public class BasicInformationPreparedStatementSetter 
        implements ItemPreparedStatementSetter<BasicInformationDto> {
 
    @Override
    public void setValues(BasicInformationDto basicInformationDto, 
        PreparedStatement ps) 
            throws SQLException {
        ThreadLocalJob.setProcesed();
        JobResultDto jobResult = ThreadLocalJob.getResult();
        int line = jobResult.getProcesedLines();
        ps.setString(1, basicInformationDto.getCcfCode());
        ps.setString(2, basicInformationDto.getAgencyCode());
        ps.setString(3, basicInformationDto.getRegisterDate());
        ps.setString(4, basicInformationDto.getLastModificationDate());
        ps.setString(5, basicInformationDto.getDocumentType());
        ps.setString(6, basicInformationDto.getNumberIdentification());
        ps.setString(7, basicInformationDto.getEmail());
        ps.setString(8, basicInformationDto.getFirstName());
        ps.setString(9, basicInformationDto.getSecondName());
        ps.setString(10, basicInformationDto.getFirstSurName());
        ps.setString(11, basicInformationDto.getSecondSurName());
        ps.setString(12, basicInformationDto.getBirthDate());
        ps.setString(13, basicInformationDto.getGender());
        ps.setString(14, basicInformationDto.getMilitaryCard());
        ps.setString(15, basicInformationDto.getMilitaryCardType());
        ps.setString(16, basicInformationDto.getMilitaryCardNumber());
        ps.setString(17, basicInformationDto.getPhoneContact());
        ps.setString(18, basicInformationDto.getCivilStatus());
        ps.setString(19, basicInformationDto.getCountry());
        ps.setString(20, basicInformationDto.getNationality());
        ps.setString(21, basicInformationDto.getStateDivipola());
        ps.setString(22, basicInformationDto.getCityDivipola());
        ps.setString(23, basicInformationDto.getHouseBoss());
        ps.setString(24, basicInformationDto.getTargetPopulation());
        ps.setString(25, basicInformationDto.getEthnicGroup());
        ps.setString(26, basicInformationDto.getPopulationType());
        ps.setString(27, basicInformationDto.getDisability());
        ps.setString(28, basicInformationDto.getDisabilityType());
        ps.setString(29, basicInformationDto.getResidenceCountry());
        ps.setString(30, basicInformationDto.getResidenceState());
        ps.setString(31, basicInformationDto.getResidenceCity());
        ps.setString(32, basicInformationDto.getAddres());
        ps.setString(33, basicInformationDto.getNeighborhood());
        ps.setString(34, basicInformationDto.getOtherPhone());
        ps.setString(35, basicInformationDto.getObservations());
        ps.setString(36, basicInformationDto.getWageAspiration());
        ps.setString(37, basicInformationDto.getTravelPossibility());
        ps.setString(38, basicInformationDto.getMovePossibility());
        ps.setString(39, basicInformationDto.getDistanceJobOffersInterest());
        ps.setString(40, basicInformationDto.getEmploymentStatus());
        ps.setString(41, basicInformationDto.getVehicleOwner());
        ps.setString(42, basicInformationDto.getDrivingLicenseCar());
        ps.setString(43, basicInformationDto.getCategoryLicenseCar());
        ps.setString(44, basicInformationDto.getDrivingLicenseMotorcycle());
        ps.setString(45, basicInformationDto.getCategoryLicenseMotorcycle());
        ps.setString(46, Integer.toString(line));
    }
 
}
