/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: TemplateController.java
 * Created on: 2016/12/07, 09:00:32 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.gml.simpc.api.entity.OptionsTemplate;
import com.gml.simpc.api.entity.exeption.SystemException;
import com.gml.simpc.api.services.ApprovalService;
import static com.gml.simpc.api.utilities.Util.FAILED_ALERT;
import static com.gml.simpc.api.utilities.Util.SUCCESS_ALERT;
import com.gml.simpc.commons.storage.velocity.TestTemplate;
import com.gml.simpc.web.controller.ControllerDefinition;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controlador encargado de las plantillas para enviar correos.
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
@Controller
public class TemplateController implements ControllerDefinition {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(ApprovalController.class);

    /**
     * Servicio para aprobacion de instituciones; Servicio
     * de instutuciones intermediario para la comunicaci&oacute;n con la
     * persistencia.
     */
    @Autowired
    private ApprovalService approvalService;
    
    
    
    /**
     * Inicializa el binder para formatear fechas.
     *
     * @param binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class,
            new CustomDateEditor(sdf, true));
    }

    /**
     * Permite configurar el mesaje de aprobacion.
     * 
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = "mensaje-aprobacion")
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView("mensaje-aprobacion");
        init(modelAndView);
        return modelAndView;
    }

    /**
     * Permite configurar el mesaje de aprobacion.
     * 
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = "mensaje-rechazo")
    public ModelAndView refuseConfigure(HttpServletRequest request,
        HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView("mensaje-rechazo");
        init(modelAndView);
        return modelAndView;
    }

    /**
     * M&eacute;todo que guarda mensajes de aprobaci&oacute;n y rechazo.
     *
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = "guardar-configuracion-template",
        method = RequestMethod.POST)
    public ModelAndView save(HttpServletRequest request,
        HttpServletResponse response) {

        String template = request.getParameter("htmlValor");
        OptionsTemplate theTemplate = this.approvalService.findTemplate();
        String type = request.getParameter("tipo");
        String newWindow = "mensaje-aprobacion";

        if (template != null) {
            template = template.replaceAll("\t", "").replaceAll("\n", "").
                replaceAll("\r", "");
        }

        if ("A".equals(type)) {
            theTemplate.setApprovalTemplate(template);
        } else {
            theTemplate.setRefuseTemplate(template);
            newWindow = "mensaje-rechazo";
        }

        ModelAndView modelAndView = new ModelAndView(newWindow);

        try {
            TestTemplate tester = new TestTemplate();
            tester.testTemplate(template);
            approvalService.saveTemplate(theTemplate);
            modelAndView.addObject("msg",
                "Se ha guardado la plantilla con exito");
            modelAndView.addObject("msgType", SUCCESS_ALERT);

        } catch (SystemException ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg",
                "Error en el formato de la plantilla, Por favor consulte al adminsitrador");
            modelAndView.addObject("msgType", FAILED_ALERT);
        } catch (Exception uEx) {
            LOGGER.error("Error ", uEx);
            modelAndView.addObject("msg",uEx);
            modelAndView.addObject("msgType", FAILED_ALERT);
        } finally {
            LOGGER.info("Guardar plantilla");
        }
        init(modelAndView);
        return modelAndView;
    }

    /**
     * Inicia los objetos para el controler.
     *
     * @param modelAndView
     */
    private void init(ModelAndView modelAndView) {
        try {

            modelAndView.addObject("configure", approvalService.findTemplate());
        } catch (Exception uEx) {
            LOGGER.error("Error ", uEx);
            modelAndView.addObject("msg",
                "Error al Mostrar la plantilla, Por consulte al adminsitrador");
            modelAndView.addObject("msgType", FAILED_ALERT);
        } finally {
            LOGGER.info("Mostrar plantilla");
        }
    }
}
