/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ValueListService.java
 * Created on: 2017/01/16, 02:45:08 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.utilities;

import com.gml.simpc.api.entity.valuelist.CarLicenseCategory;
import com.gml.simpc.api.entity.valuelist.Cine;
import com.gml.simpc.api.entity.valuelist.City;
import com.gml.simpc.api.entity.valuelist.CivilStatus;
import com.gml.simpc.api.entity.valuelist.Degree;
import com.gml.simpc.api.entity.valuelist.DisabilityType;
import com.gml.simpc.api.entity.valuelist.DocumentType;
import com.gml.simpc.api.entity.valuelist.EducationalLevel;
import com.gml.simpc.api.entity.valuelist.EmploymentExperience;
import com.gml.simpc.api.entity.valuelist.EmploymentStatus;
import com.gml.simpc.api.entity.valuelist.Eps;
import com.gml.simpc.api.entity.valuelist.EquivalentPosition;
import com.gml.simpc.api.entity.valuelist.EthnicGroup;
import com.gml.simpc.api.entity.valuelist.Foundation;
import com.gml.simpc.api.entity.valuelist.KnowledgeCore;
import com.gml.simpc.api.entity.valuelist.Language;
import com.gml.simpc.api.entity.valuelist.MotorcycleLicenseCategory;
import com.gml.simpc.api.entity.valuelist.OrientationType;
import com.gml.simpc.api.entity.valuelist.OtherKnowledge;
import com.gml.simpc.api.entity.valuelist.PensionFunds;
import com.gml.simpc.api.entity.valuelist.PerformanceArea;
import com.gml.simpc.api.entity.valuelist.ProgramType;
import com.gml.simpc.api.entity.valuelist.Sector;
import com.gml.simpc.api.entity.valuelist.SeveranceFund;
import com.gml.simpc.api.entity.valuelist.State;
import com.gml.simpc.api.entity.valuelist.TrainingType;
import com.gml.simpc.api.utilities.ValueList;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica
 * Jim&eacute;nez Rozo</a>
 */
public class ValueListTestService {

    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(ValueListTestService.class);
    /*
     * -----------------------------------------------------------------------
     */

    private ValueList valueList;

    /*
     * -----------------------------------------------------------------------
     */
    public ValueList getValueList() {
        return valueList;
    }

    /*
     * -----------------------------------------------------------------------
     */
    private void setLoadTypes() {
        LOGGER.info("... updating test LoadTypes");

    }

    private void setProgramTypes() {
        LOGGER.info("... updating test ProgramTypes");
        Map<String, ProgramType> map = new HashMap<>();
        map.put("1", new ProgramType(1L,
            "Competencias claves y transversales"));
        map.put("2", new ProgramType(2L, "M�dulo TICS"));
        map.put("3", new ProgramType(3L, "Validaci�n bachillerato"));
        map.put("4", new ProgramType(4L, "T�cnico laboral"));
        map.put("5", new ProgramType(5L,
            "Certificaci�n de competencias"));
        map.put("6", new ProgramType(6L, "Reentrenamiento t�cnico"));
        valueList.setProgramTypes(map);
    }

    private void setOrientationTypes() {
        LOGGER.info("... updating test OrientantioTypes");
        Map<String, OrientationType> map = new HashMap<>();
        map.put("1", new OrientationType(1L,
            "Prueba Psicotecnica"));
        map.put("2", new OrientationType(2L, "Entrevista"));
        map.
            put("3", new OrientationType(3L, "Talleres"));
        map.put("4", new OrientationType(4L, "Curso/Diplomado/Otro  "));
        map.put("5", new OrientationType(5L,
            "Certificaci�n de competencias laborales"));
        map.
            put("6", new OrientationType(6L, "Servicios Adicionales"));
        valueList.setOrientationTypes(map);
    }

    private void setFoundations() {
        LOGGER.info("... updating rwar Foundations");
        Map<String, Foundation> map = new HashMap<>();
        map.put("CCF02", new Foundation("CCF02", "Camacol"));
        map.put("CCF03", new Foundation("CCF03", "Confenalco"));
        map.put("CCF04", new Foundation("CCF04", "Comfama"));
        map.put("CCF05", new Foundation("CCF05", "Cajacopi"));
        map.put("CCF06", new Foundation("CCF06", "Combarranquilla"));
        map.put("CCF11", new Foundation("CCF11", "Combarranquilla"));
        valueList.setFoundations(map);
    }

    private void setDocumentTypes() {
        LOGGER.info("... updating test documentTypes");
        Map<String, DocumentType> map = new HashMap<>();
        map.put("CC", new DocumentType("CC", "C�dula de Ciudadan�a"));
        map.put("CE", new DocumentType("CE", "C�dula de Extranjeria"));
        map.put("NIT", new DocumentType("NIT", "NIT"));
        map.put("PA", new DocumentType("PA", "Pasaporte"));
        map.put("PI", new DocumentType("TI", "Tarjeta de Identidad"));
        valueList.setDocumentTypeFull(map);
    }

    private void setStates() {
        LOGGER.info("... updating States");
        Map<String, State> map = new HashMap<>();
        map.put("91", new State("91", "Amazonas"));
        map.put("05", new State("05", "Antioquia"));
        map.put("17", new State("05", "Antioquia"));
        valueList.setStates(map);
    }

    private void setCities() {
        LOGGER.info("... updating Cities");
        Map<String, City> map = new HashMap<>();
        map.put("91001", new City("91001", "Leticia"));
        map.put("91263", new City("91263", "El encanto"));
        valueList.setCities(map);
    }

    private void setCivilStatus() {
        LOGGER.info("... updating civil status");

        Map<String, CivilStatus> map = new HashMap<>();
        map.put("1", new CivilStatus("1", "Soltero"));
        map.put("2", new CivilStatus("2", "Casado"));
        valueList.setCivilStatusMap(map);
    }

    private void setFountations() {
        LOGGER.info("... updating foundations");
        Map<String, Foundation> map = new HashMap<>();
        map.put("CCF02", new Foundation("CCF02", "CAJA DE COMPENSACION " +
            "FAMILIAR CAMACOL COMFAMILIAR CAMACOL", "890900840-1"));
        map.put("CCF03", new Foundation("CCF03", "CAJA DE COMPENSACION " +
            "FAMILIAR COMFENALCO ANTIOQUIA", "890900842-6"));
        map.put("CCF04", new Foundation("CCF04", "CAJA DE COMPENSACION " +
            "FAMILIAR DE ANTIOQUIA COMFAMA", "890900841-9"));
        map.put("CCF05", new Foundation("CCF05", "CAJA DE COMPENSACION " +
            "FAMILIAR CAJACOPI ATLANTICO", "890102044-1"));
        map.put("CCF11", new Foundation("CCF11", "CAJA DE COMPENSACION " +
            "FAMILIAR CAJACOPI ATLANTICO", "890102044-1"));
        valueList.setFoundations(map);
    }

    private void setEducationalLevels() {
        LOGGER.info("... updating educationalLevels");
        Map<String, EducationalLevel> map = new HashMap<>();
        map.put("BS", new EducationalLevel("BS", "B�sica Secundaria(6-9)"));
        map.put("D", new EducationalLevel("D", "Doctorado"));
        map.put("E", new EducationalLevel("E", "Especializaci�n"));
        map.put("M", new EducationalLevel("M", "Maestr�a"));
        valueList.setEducationalLevelMap(map);
    }

    private void setPerformanceAreas() {
        LOGGER.info("... updating performanceAreas");
        Map<String, PerformanceArea> map = new HashMap<>();
        map.put("1", new PerformanceArea("1", "FINANZAS  Y  ADMINISTRACI�N"));
        map.put("2", new PerformanceArea("2",
            "CIENCIAS  NATURALES,  APLICADAS" +
            "  Y  RELACIONADAS"));
        map.put("3", new PerformanceArea("3", "SALUD"));
        map.put("4", new PerformanceArea("4", "CIENCIAS SOCIALES, EDUCACI�N, " +
            "SERVICIOS  GUBERNAMENTALES Y RELIGI�N"));
        valueList.setPerformanceAreaMap(map);
    }

    private void setKnowledgeCores() {
        LOGGER.info("... updating knowledgeCores");
        Map<String, KnowledgeCore> map = new HashMap<>();
        map.put("TLA1", new KnowledgeCore("TLA1", "AGRONOM�A"));
        map.put("TLA10", new KnowledgeCore("TLA10", "EDUCACI�N"));
        map.put("TLA11", new KnowledgeCore("TLA11", "BACTERIOLOG�A"));
        map.put("TLA12", new KnowledgeCore("TLA12", "ENFERMER�A"));

        valueList.setKnowledgeCoreMap(map);
    }

    private void setObtainedTitles() {
        LOGGER.info("... updating obtainedTitles");
        Map<String, Degree> map = new HashMap<>();
        map.put("AGROPECUAR", new Degree("AGROPECUAR", "Agropecuario"));
        map.put("ARTISTICO", new Degree("ARTISTICO", "Art�stico"));
        map.put("BASICASEC", new Degree("BASICASEC", "Basica " +
            "Secundaria"));
        map.put("BILING�E", new Degree("BILING�E", "Biling�e"));

        valueList.setObtainedTitleMap(map);
    }

    private void setExperienceTypes() {
        LOGGER.info("... updating experienceTypes");
        Map<String, EmploymentExperience> map = new HashMap<>();
        map.put("1", new EmploymentExperience("1", "Asalariado"));
        map.put("2", new EmploymentExperience("2", "Independiente"));
        map.put("3", new EmploymentExperience("3", "Pasant�a o Pr�ctica " +
            "Laboral"));

        valueList.setEmploymentExperienceMap(map);
    }

    private void setSectors() {
        LOGGER.info("... updating sectors");
        Map<String, Sector> map = new HashMap<>();
        map.put("160", new Sector("160", "Actividades de empleo"));
        map.put("161", new Sector("161", "Actividades de las agencias de " +
            "viajes, operadores tur�sticos, servicios de reserva y " +
            "actividades relacionadas"));
        map.put("162", new Sector("162", "Actividades de seguridad e " +
            "investigaci�n privada"));
        map.put("163", new Sector("163", "Actividades de servicios a " +
            "edificios y paisajismo (jardines, zonas verdes)"));

        valueList.setSectorMap(map);
    }

    private void setEquivalentPositions() {
        LOGGER.info("... updating equivalent Positions");
        Map<String, EquivalentPosition> map = new HashMap<>();
        map.put("0", new EquivalentPosition("0", "Por Definir"));
        map.put("1", new EquivalentPosition("1", "Abad"));
        map.put("10", new EquivalentPosition("10", "Abogado de contralor�a"));
        map.put("100", new EquivalentPosition("100", "Agente de polic�a"));
        valueList.setEquivalentPositionMap(map);
    }

    private void setTrainingTypes() {
        LOGGER.info("... updating TrainingTypes");
        Map<String, TrainingType> map = new HashMap<>();
        map.put("1",
            new TrainingType("1", "Competencias claves y transversales"));
        map.put("2", new TrainingType("2", "TICS"));
        map.put("3", new TrainingType("3", "Alfabetizacion o bachillerato"));
        map.put("4", new TrainingType("4", "Entrenamiento - Reentrenamiento"));
        map.put("5", new TrainingType("5", "Tecnico laboral"));

        valueList.setTrainingTypeMap(map);
    }

    private void setLanguages() {
        LOGGER.info("... updating languages");
        Map<String, Language> map = new HashMap<>();
        map.put("1", new Language("1", "Alem�n"));
        map.put("2", new Language("2", "Arabe"));
        map.put("3", new Language("3", "B�lgaro"));
        map.put("4", new Language("4", "Catal�n"));

        valueList.setLanguageMap(map);
    }

    private void setOtherKnowledges() {
        LOGGER.info("... updating ther languages");
        Map<String, OtherKnowledge> map = new HashMap<>();
        map.put("23", new OtherKnowledge("23", "Procesador de texto (Ejemplo:" +
            " Word)"));
        map.put("24", new OtherKnowledge("24", "Hoja de c�lculo (Ejemplo:" +
            " Excel)"));
        map.put("25", new OtherKnowledge("25", "Multimedia (Ejemplo:" +
            " PowerPoint)"));
        map.put("26", new OtherKnowledge("26", "Otros"));

        valueList.setOtherKnowledgeMap(map);
    }

    private void setEthnicGroups() {
        LOGGER.info("... updating EthnicGroups");
        Map<String, EthnicGroup> map = new HashMap<>();
        map.put("1", new EthnicGroup("1", "Indigenas"));
        map.put("2", new EthnicGroup("2", "Negros)"));
        map.put("3", new EthnicGroup("3", "Raizales"));
        valueList.setEthnicGroupMap(map);
    }

    private void setDisabilityTypes() {
        LOGGER.info("... updating DisabilityTypes");
        Map<String, DisabilityType> map = new HashMap<>();
        map.put("7", new DisabilityType("7", "Visual"));
        map.put("8", new DisabilityType("8", "Auditiva"));
        map.put("9", new DisabilityType("9", "Fisica"));
        valueList.setDisabilityTypeMap(map);
    }

    private void setEmploymentStatus() {
        LOGGER.info("... updating EmploymentStatus");
        Map<String, EmploymentStatus> map = new HashMap<>();
        map.put("1", new EmploymentStatus("1", "Primer empleo"));
        map.put("2", new EmploymentStatus("2", "Desempleado"));
        map.put("3", new EmploymentStatus("3", "Empleado"));
        valueList.setEmploymentStatusMap(map);
    }

    private void setCarLicenseCategories() {
        LOGGER.info("... updating CarLicenseCategories");
        Map<String, CarLicenseCategory> map = new HashMap<>();
        map.put("B1", new CarLicenseCategory("B1", "B1"));
        map.put("B2", new CarLicenseCategory("B2", "B2"));
        map.put("B3", new CarLicenseCategory("B3", "B3"));
        map.put("C1", new CarLicenseCategory("C1", "C1"));
        map.put("C2", new CarLicenseCategory("C2", "C2"));
        map.put("C3", new CarLicenseCategory("C3", "C3"));
        valueList.setCarLicenseCategoryMap(map);
    }

    private void setMotorcycleLicenseCategories() {
        LOGGER.info("... updating CarLicenseCategories");
        Map<String, MotorcycleLicenseCategory> map = new HashMap<>();
        map.put("A1", new MotorcycleLicenseCategory("A1", "A1"));
        map.put("A2", new MotorcycleLicenseCategory("A2", "A2"));

        valueList.setMotorcycleLicenseCategoryMap(map);
    }

    private void setCine() {
        LOGGER.info("... updating CarLicenseCategories");
        Map<String, Cine> map = new HashMap<>();
        map.put("A1", new Cine(3L, "3", "ewew"));
        map.put("A2", new Cine(4L, "4", "ewew"));

        valueList.setCineMap(map);
    }

    private void setEps() {
        LOGGER.info("... updating EPS");
        Map<String, Eps> map = new HashMap<>();
        map.put("CCFC15", new Eps(3L, "CCFC15", "ewew",
            "eee"));
        map.put("A2", new Eps(4L, "4", "ewew", "dasds"));

        valueList.setEpsMap(map);
    }

    private void setPensionFund() {
        LOGGER.info("... updating pension");
        Map<String, PensionFunds> map = new HashMap<>();
        map.put("A1", new PensionFunds(3L, "isd", "ewew",
            "eee"));
        map.put("230301", new PensionFunds(4L, "230301",
            "ewew", "dasds"));

        valueList.setPensionFundsMap(map);
    }

    private void setSeverange() {
        LOGGER.info("... updating censantias");
        Map<String, SeveranceFund> map = new HashMap<>();
        map.put("A1", new SeveranceFund(3L, "A1", "A1"));
        map.put("230301", new SeveranceFund(4L, "230301",
            "ewew"));

        valueList.setSeveranceFundMap(map);
    }

    /*
     * -----------------------------------------------------------------------
     */
    public void updateAll() {
        this.valueList = new ValueList();
        LOGGER.info("Updating value lists from database new service... ");
        setLoadTypes();
        setProgramTypes();
        setOrientationTypes();
        setFoundations();
        setStates();
        setCities();
        setDocumentTypes();
        setCivilStatus();
        setFountations();
        setEducationalLevels();
        setPerformanceAreas();
        setKnowledgeCores();
        setObtainedTitles();
        setExperienceTypes();
        setSectors();
        setEquivalentPositions();
        setTrainingTypes();
        setLanguages();
        setOtherKnowledges();
        setEthnicGroups();
        setDisabilityTypes();
        setEmploymentStatus();
        setCarLicenseCategories();
        setMotorcycleLicenseCategories();
        setCine();
        setEps();
        setPensionFund();
        setSeverange();
    }

}
