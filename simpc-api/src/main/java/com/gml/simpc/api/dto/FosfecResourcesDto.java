/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: FosfecResourcesDto.java
 * Created on: 2017/02/02, 04:43:51 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

/**
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
public class FosfecResourcesDto {
    
    /**
     * Valor para  consolidado de salud
     */
    private String health;
    /**
     * Valor para  el consolidado de pension
     */
    private String pension;
    
     /**
     * Valor para  el consolidado de bonos
     */
    private String bonds;
    /**
     * Valor para  la ccf  consultada
     */
    private String ccf;
    /**
     * Valor para  el  mes consultado
     */
    private String month;
    /**
     * Valor para  el   consolidado de la cuota moneraria
     */
    
    private String monetaryQuota;
    /**
     * Valor para  el  total consolidado
     */
    private String total;

   

    public String getPension() {
        return pension;
    }

    public String getBonds() {
        return bonds;
    }

    public String getCcf() {
        return ccf;
    }

    public String getMonetaryQuota() {
        return monetaryQuota;
    }

    public String getTotal() {
        return total;
    }

    public String getHealth() {
        return health;
    }

    public void setHealth(String health) {
        this.health = health;
    }
    

    public void setPension(String pension) {
        this.pension = pension;
    }

    public void setBonds(String bonds) {
        this.bonds = bonds;
    }

    public void setCcf(String ccf) {
        this.ccf = ccf;
    }

    public void setMonetaryQuota(String monetaryQuota) {
        this.monetaryQuota = monetaryQuota;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }
    
    
    

}
