/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Intersection.java
 * Created on: 2017/02/07, 04:21:06 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import com.gml.simpc.api.enums.IntersectionStatus;
import com.gml.simpc.api.enums.IntersectionType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Table(name = "CRUCES")
@Entity
public class Intersection implements Serializable {

    /**
     * Id de la entidad.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator =
        "intersection_seq_gen")
    @SequenceGenerator(name = "intersection_seq_gen", sequenceName =
        "INTERSECTION_SEQ", allocationSize = 1, initialValue= 1)
    private Long id;
    /**
     * Usuario que realiza el cruce.
     */
    @ManyToOne
    @JoinColumn(name = "USUARIO", nullable = false)
    private User user;
    /**
     * N&uacute;mero de registros en el archivo.
     */
    @Column(name = "TOTAL_REGISTROS")
    private int totalOfRecors;
    /**
     * N&uacute;mero de filas exitosas.
     */
    @Column(name = "REGISTROS_EXITOSOS")
    private int successRecords;
    /**
     * N&uacute;mero de filas fallidas.
     */
    @Column(name = "REGISTROS_FALLIDOS")
    private int failedRecords;
    /**
     * Estado en que se encuentra el cruce.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "ESTADO")
    private IntersectionStatus status;
    /**
     * Path y nombre del archivo cargado,con el nombre asignado por el sistema
     */
    @Column(name = "ARCHIVO_CARGADO")
    private String loadFile;
    /**
     * Carpeta donde se encuentran los archivos resultado.
     */
    @Column(name = "DIRECTORIO_RESULTADOS")
    private String resultPath;
    /**
     * Tipo de cruce.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "TIPO_CRUCE", columnDefinition = "char(1) default 'M'")
    private IntersectionType intersectionType = IntersectionType.M;
    /**
     * Detalle del cruce.
     */
    @Column(name = "DETALLE", nullable = true, columnDefinition = "CLOB")
    private String detail;
    /**
     * Fecha de creaci&oacute;n del cruce.
     */
    @Column(name = "FECHA_CREACION", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date creationDate;
    /**
     * Fecha de finalizaci&oacute;n del cruce.
     */
    @Column(name = "FECHA_FIN")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    
    /**
     * Path y nombre del archivo cargado,con el nombre cargado
     */
    @Column(name = "ARCHIVO_NOMBRE")
    private String fileName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public IntersectionStatus getStatus() {
        return status;
    }

    public void setStatus(IntersectionStatus status) {
        this.status = status;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getLoadFile() {
        return loadFile;
    }

    public void setLoadFile(String loadFile) {
        this.loadFile = loadFile;
    }

    public String getResultPath() {
        return resultPath;
    }

    public void setResultPath(String resultPath) {
        this.resultPath = resultPath;
    }

    public IntersectionType getIntersectionType() {
        return intersectionType;
    }

    public void setIntersectionType(IntersectionType intersectionType) {
        this.intersectionType = intersectionType;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getTotalOfRecors() {
        return totalOfRecors;
    }

    public void setTotalOfRecors(int totalOfRecors) {
        this.totalOfRecors = totalOfRecors;
    }

    public int getSuccessRecords() {
        return successRecords;
    }

    public void setSuccessRecords(int successRecords) {
        this.successRecords = successRecords;
    }

    public int getFailedRecords() {
        return failedRecords;
    }

    public void setFailedRecords(int failedRecords) {
        this.failedRecords = failedRecords;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

   
    
}
