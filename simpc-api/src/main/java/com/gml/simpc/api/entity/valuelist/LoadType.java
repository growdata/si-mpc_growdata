/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: LoadType.java
 * Created on: 2016/12/10, 02:58:40 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity.valuelist;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad que representa los posibles tipos de carga de archivos
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Table(name = "WF_TIPOS_CARGA")
@Entity
public class LoadType implements Serializable {

    /**
     * ID del tipo de carga.
     */
    @Id
    @Column(name = "ID", length = 10)
    private Long id;
    /**
     * nombre del tipo de carga
     */
    @Column(name = "NOMBRE", length = 50)
    private String name;

    /**
     * descripci&oacute;n
     */
    @Column(name = "DESCRIPCION", length = 200)
    private String description;

    /**
     * Constructor por defecto.
     */
    public LoadType() {
        this.id = null;
    }

    /**
     * Constructor por nombre.
     *
     * @param name
     */
    public LoadType(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LoadType other = (LoadType) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return name;
    }
}
