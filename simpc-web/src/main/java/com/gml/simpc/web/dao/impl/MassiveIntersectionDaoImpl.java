/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: MassiveIntersectionDaoImpl.java
 * Created on: 2017/02/13, 03:10:09 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.dao.impl;

import com.gml.simpc.api.dao.MassiveIntersectionDao;
import com.gml.simpc.api.dto.BasicInformationDto;
import com.gml.simpc.api.dto.EducationLevelDto;
import com.gml.simpc.api.dto.InformalEducationDto;
import com.gml.simpc.api.dto.IntermediationDto;
import com.gml.simpc.api.dto.LanguageDto;
import com.gml.simpc.api.dto.OtherKnowledgeDto;
import com.gml.simpc.api.dto.PilaDto;
import com.gml.simpc.api.dto.PilaEmploymentServiceDto;
import com.gml.simpc.api.dto.ProfitDto;
import com.gml.simpc.api.dto.ProgramDto;
import com.gml.simpc.api.dto.WorkExperienceDto;
import com.gml.simpc.web.dao.mapper.BasicInformationDtoRowMapper;
import com.gml.simpc.web.dao.mapper.EducationLevelDtoRowMapper;
import com.gml.simpc.web.dao.mapper.InformalEducationDtoRowMapper;
import com.gml.simpc.web.dao.mapper.IntermediationDtoRowMapper;
import com.gml.simpc.web.dao.mapper.LanguageDtoRowMapper;
import com.gml.simpc.web.dao.mapper.OtherKnowledgeDtoRowMapper;
import com.gml.simpc.web.dao.mapper.PilaDtoRowMapper;
import com.gml.simpc.web.dao.mapper.PilaEmploymentServiceDtoRowMapper;
import com.gml.simpc.web.dao.mapper.ProfitDtoRowMapper;
import com.gml.simpc.web.dao.mapper.ProgramDtoRowMapper;
import com.gml.simpc.web.dao.mapper.WorkExperienceDtoRowMapper;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Dao para cruces masivos.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Repository
public class MassiveIntersectionDaoImpl implements MassiveIntersectionDao {

    /**
     * Logger de la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(MassiveIntersectionDaoImpl.class);
    /**
     * Plantilla Jdbc para operar la base de datos.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    /**
     * Consulta para encontrar registros de la tabla MD_F_CAP_PROGRAMA_MODULO y
     * MD_F_ORI_PROGRAMA_MODULO filtrando por ID.
     */
    private static final String FIND_TRAINING = "SELECT CAP.CARGA_ID, " +
            "CAP.CODIGO_PROGRAMA ,CAP.NOMBRE_PROGRAMA ,CAP.TIPO_PROGRAMA , " +
            "CAP.TIPO_CURSO ,CAP.CODIGO_CCF  ,CAP.CODIGO_INST ,CAP.CODIGO_SEDE , " +
            "CAP.CODIGO_DEPARTAMENTO ,CAP.CODIGO_MUNICIPIO ,CAP.CERTIFICADA , " +
            "CAP.NOMBRE_CERTIFICACION ,CAP.OTRO_NOMBRE_CERT ,CAP.CERTIFICACION_EMITIDA , " +
            "CAP.CINE, CAP.NECESIDAD,CAP.CODIGO_MODULO, " +
            "CAP.NOMBRE_MODULO ,CAP.DURACION ,CAP.TIPO_HORARIO , " +
            "CAP.TIPO_COSTO_MATRICULA ,CAP.COSTO_MATRICULA,CAP.TIPO_OTROS_COSTOS, " +
            "CAP.OTROS_COSTOS ,CAP.FECHA_INICIO ,CAP.FECHA_FIN, 'N/A' AS " +
            "AREA_DESEMPENO " +
          "FROM MD_F_CAP_PROGRAMA_MODULO CAP INNER JOIN " +
            "MD_F_PROGRAMA_DETALLE DETA ON (CAP.CODIGO_MODULO=DETA.CODIGO_MODULO) " +
            "INNER JOIN MD_F_DATOS_BASICOS B ON DETA.TIPO_DOCUMENTO = " +
            "B.TIPO_DOCUMENTO AND DETA.DOCUMENTO = B.NUMERO_DOCUMENTO " +
            "INNER JOIN DATOS_CRUCE CR ON B.TIPO_DOCUMENTO = " +
            "CR.TIPO_IDENTIFICACION AND B.NUMERO_DOCUMENTO = CR.NUMERO_IDENTIFICACION " +
          "WHERE CR.ID = ? " +

          "UNION " + 

          "SELECT ORI.CARGA_ID, ORI.CODIGO_PROGRAMA ,ORI.NOMBRE_PROGRAMA , " +
            "ORI.TIPO_PROGRAMA ,ORI.TIPO_CURSO ,ORI.CODIGO_CCF  ,ORI.INSTITUCION " +
            "AS CODIGO_INST , 'N/A' AS CODIGO_SEDE ,ORI.CODIGO_DEPARTAMENTO , " +
            "ORI.CODIGO_MUNICIPIO ,'N/A' AS CERTIFICADA ,'N/A' AS " +
            "NOMBRE_CERTIFICACION ,'N/A' AS OTRO_NOMBRE_CERT , " +
            "'N/A' AS CERTIFICACION_EMITIDA ,'N/A' AS CINE ,'N/A' AS NECESIDAD , " +
            "ORI.CODIGO_MODULO ,ORI.NOMBRE_MODULO ,ORI.DURACION,ORI.TIPO_HORARIO, " +
            "'N/A' AS TIPO_COSTO_MATRICULA ,'N/A' AS COSTO_MATRICULA ,'N/A' AS " +
            "TIPO_OTROS_COSTOS ,'N/A' AS OTROS_COSTOS ,ORI.FECHA_INICIO , " +
            "ORI.FECHA_FIN, ORI. AREA_DESEMPENO " +
          "FROM MD_F_ORI_PROGRAMA_MODULO ORI INNER JOIN MD_F_PROGRAMA_DETALLE DETA2 ON (ORI.CODIGO_MODULO= " +
            "DETA2.CODIGO_MODULO) " +
            "INNER JOIN md_f_datos_basicos b ON " +
            "DETA2.TIPO_DOCUMENTO = B.TIPO_DOCUMENTO AND " +
            "DETA2.DOCUMENTO = B.NUMERO_DOCUMENTO " +
            "INNER JOIN DATOS_CRUCE CR2 ON B.TIPO_DOCUMENTO = " +
            "CR2.TIPO_IDENTIFICACION AND B.NUMERO_DOCUMENTO = CR2.NUMERO_IDENTIFICACION " +
          "WHERE CR2.ID = ?";

    /**
     * Consulta para encontrar registros de la tabla MD_F_PILA
     * filtrando por id de carga.
     */
    private static final String FIND_PILA = "SELECT a.* FROM md_f_pila a " +
        "INNER JOIN md_f_datos_basicos c ON a.tipodocumento = c.tipo_documento " +
        "AND a.numerodocumento = c.numero_documento INNER JOIN datos_cruce b " +
        "ON a.tipodocumento = b.tipo_identificacion AND a.numerodocumento = " +
        "b.numero_identificacion AND c.codigo_ccf = b.caja_compensacion " +
        "WHERE b.id = ?";
    /**
     * Consulta para encontrar registros de la tabla MD_F_PILA
     * filtrando por id de carga para servicio p&uacute;blico de empleo.
     */
    private static final String FIND_PILA_PS = "SELECT id_carga, " +
        "tipodocumento, numerodocumento, tipoidaportanteu, numidaportanteu, " +
        "nombreaportanteu, maxfechau, primerperiodou FROM md_f_pila a " +
        "INNER JOIN md_f_datos_basicos c ON a.tipodocumento = c.tipo_documento " +
        "AND a.numerodocumento = c.numero_documento INNER JOIN datos_cruce b " +
        "ON a.tipodocumento = b.tipo_identificacion AND a.numerodocumento = " +
        "b.numero_identificacion AND c.codigo_ccf = b.caja_compensacion " +
        "WHERE b.id = ?";
    /**
     * Consulta para encontrar registros de la tabla MD_F_GESTION_INTERMEDIACION
     * filtrando por id de carga.
     */
    private static final String FIND_INTERMEDIATION = "SELECT a.* FROM " +
        "md_f_gestion_intermediacion a INNER JOIN md_f_datos_basicos c ON " +
        "a.tipo_documento = c.tipo_documento AND a.numero_documento = " +
        "c.numero_documento INNER JOIN datos_cruce b ON a.tipo_documento = " +
        "b.tipo_identificacion AND a.numero_documento = b.numero_identificacion " +
        "AND c.codigo_ccf = b.caja_compensacion WHERE b.id = ?";

    /**
     * Consulta para encontrar registros de la tabla MD_F_DATOS_BASICOS
     * filtrando por id de carga.
     */
    private static final String FIND_BASIC_INFORMATION = "SELECT c.* FROM " +
        "md_f_datos_basicos c INNER JOIN datos_cruce b ON c.tipo_documento = " +
        "b.tipo_identificacion AND c.numero_documento = b.numero_identificacion " +
        "AND c.codigo_ccf = b.caja_compensacion WHERE b.id = ?";

    /**
     * Consulta para encontrar registros de la tabla MD_F_NIVEL_EDUCATIVO
     * filtrando por id de carga.
     */
    private static final String FIND_EDUCATION_LEVEL = "SELECT a.* FROM " +
        "md_f_nivel_educativo a INNER JOIN md_f_datos_basicos c ON " +
        "a.tipo_documento = c.tipo_documento AND a.numero_documento = " +
        "c.numero_documento INNER JOIN datos_cruce b ON a.tipo_documento = " +
        "b.tipo_identificacion AND a.numero_documento = b.numero_identificacion " +
        "AND c.codigo_ccf = b.caja_compensacion WHERE b.id = ?";

    /**
     * Consulta para encontrar registros de la tabla MD_F_EXPERIENCIA_LABORAL
     * filtrando por id de carga.
     */
    private static final String FIND_WORK_EXPERIENCE = "SELECT a.* FROM " +
        "md_f_experiencia_laboral a INNER JOIN md_f_datos_basicos c ON " +
        "a.tipo_documento = c.tipo_documento AND a.numero_documento = " +
        "c.numero_documento INNER JOIN datos_cruce b ON a.tipo_documento = " +
        "b.tipo_identificacion AND a.numero_documento = b.numero_identificacion " +
        "AND c.codigo_ccf = b.caja_compensacion WHERE b.id = ?";

    /**
     * Consulta para encontrar registros de la tabla MD_F_EDUCACION_INFORMAL
     * filtrando por id de carga.
     */
    private static final String FIND_INFORMAL_EDUCATION = "SELECT a.* FROM " +
        "md_f_educacion_informal a INNER JOIN md_f_datos_basicos c ON " +
        "a.tipo_documento = c.tipo_documento AND a.numero_documento = " +
        "c.numero_documento INNER JOIN datos_cruce b ON a.tipo_documento = " +
        "b.tipo_identificacion AND a.numero_documento = b.numero_identificacion " +
        "AND c.codigo_ccf = b.caja_compensacion WHERE b.id = ?";

    /**
     * Consulta para encontrar registros de la tabla MD_F_IDIOMAS
     * filtrando por id de carga.
     */
    private static final String FIND_LANGUAGES = "SELECT a.* FROM " +
        "md_f_idiomas a INNER JOIN md_f_datos_basicos c ON " +
        "a.tipo_documento = c.tipo_documento AND a.numero_documento = " +
        "c.numero_documento INNER JOIN datos_cruce b ON a.tipo_documento = " +
        "b.tipo_identificacion AND a.numero_documento = b.numero_identificacion " +
        "AND c.codigo_ccf = b.caja_compensacion WHERE b.id = ?";

    /**
     * Consulta para encontrar registros de la tabla MD_F_OTROS_CONOCIMIENTOS
     * filtrando por id de carga.
     */
    private static final String FIND_OTHER_KNOWLEDGES = "SELECT a.* FROM " +
        "md_f_otros_conocimientos a INNER JOIN md_f_datos_basicos c ON " +
        "a.tipo_documento = c.tipo_documento AND a.numero_documento = " +
        "c.numero_documento INNER JOIN datos_cruce b ON a.tipo_documento = " +
        "b.tipo_identificacion AND a.numero_documento = b.numero_identificacion " +
        "AND c.codigo_ccf = b.caja_compensacion WHERE b.id = ?";
    
    /**
     * Consulta para encontrar registros de la tabla MD_F_FOSFEC
     * filtrando por tipo y n&uacute;mero de documento.
     */
    private static final String FIND_FOSFEC = "SELECT * FROM MD_F_FOSFEC f " +
        "INNER JOIN md_f_datos_basicos c ON f.TIPO_IDENTIFICACION = " +
        "c.tipo_documento AND f.NUMERO_IDENTIFICACION = c.numero_documento " +
        "INNER JOIN datos_cruce b ON f.TIPO_IDENTIFICACION = " +
        "b.tipo_identificacion AND f.NUMERO_IDENTIFICACION = b.numero_identificacion " +
        "WHERE b.id = ?";

    @Override
    public List<PilaDto> findPila(long intersectionId) {
        LOGGER.info("Consultando información de PilaDto.");

        return this.jdbcTemplate.query(FIND_PILA, new PilaDtoRowMapper(),
            intersectionId);
    }

    @Override
    public List<PilaEmploymentServiceDto> findPilaES(long intersectionId) {
        LOGGER.info("Consultando información de PilaEmploymentServiceDto.");

        return this.jdbcTemplate.query(FIND_PILA_PS, 
            new PilaEmploymentServiceDtoRowMapper(), intersectionId);
    }

    @Override
    public List<IntermediationDto> findIntermediation(long intersectionId) {
        LOGGER.info("Consultando información de IntermediationDto.");

        return this.jdbcTemplate.query(FIND_INTERMEDIATION,
            new IntermediationDtoRowMapper(), intersectionId);
    }

    @Override
    public List<BasicInformationDto> findBasicInformation(long intersectionId) {
        LOGGER.info("Consultando información de BasicInformationDto.");

        return this.jdbcTemplate.query(FIND_BASIC_INFORMATION,
            new BasicInformationDtoRowMapper(), intersectionId);
    }

    @Override
    public List<EducationLevelDto> findEducationLevel(long intersectionId) {
        LOGGER.info("Consultando información de EducationLevelDto.");

        return this.jdbcTemplate.query(FIND_EDUCATION_LEVEL,
            new EducationLevelDtoRowMapper(), intersectionId);
    }

    @Override
    public List<WorkExperienceDto> findWorkExperience(long intersectionId) {
        LOGGER.info("Consultando información de WorkExperienceDto.");

        return this.jdbcTemplate.query(FIND_WORK_EXPERIENCE,
            new WorkExperienceDtoRowMapper(), intersectionId);
    }

    @Override
    public List<InformalEducationDto> findInformalEducation(long intersectionId) {
        LOGGER.info("Consultando información de InformalEducationDto.");

        return this.jdbcTemplate.query(FIND_INFORMAL_EDUCATION,
            new InformalEducationDtoRowMapper(), intersectionId);
    }

    @Override
    public List<LanguageDto> findLanguage(long intersectionId) {
        LOGGER.info("Consultando información de LanguageDto.");

        return this.jdbcTemplate.query(FIND_LANGUAGES,
            new LanguageDtoRowMapper(), intersectionId);
    }

    @Override
    public List<OtherKnowledgeDto> findOtherKnowledge(long intersectionId) {
        LOGGER.info("Consultando información de OtherKnowledgeDto.");

        return this.jdbcTemplate.query(FIND_OTHER_KNOWLEDGES,
            new OtherKnowledgeDtoRowMapper(), intersectionId);
    }

    @Override
    public List<ProfitDto> findFosfec(long intersectionId) {
        return this.jdbcTemplate.query(FIND_FOSFEC, new ProfitDtoRowMapper(), 
            intersectionId);
    }
    
    @Override
    public List<ProgramDto> findTraining(long intersectionId) {
        LOGGER.info("Consultando información de PilaDto.");

        return this.jdbcTemplate.query(FIND_TRAINING, new ProgramDtoRowMapper(),
            intersectionId, intersectionId);
    }
}
