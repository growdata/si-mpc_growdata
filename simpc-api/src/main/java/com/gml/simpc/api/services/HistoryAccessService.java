/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ActionLogService.java
 * Created on: 2016/11/15, 10:46:55 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.entity.HistoryAccess;
import com.gml.simpc.api.entity.User;
import java.util.Date;
import java.util.List;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public interface HistoryAccessService {

    /**
     * Obtiene todos los hist&oacute;ricos de accesos
     *
     * @return
     */
    List<HistoryAccess> getAll();

    /**
     * Servicio para guardar un hist&oacute;rico de acceso
     *
     * @param actionLog
     */
    void save(HistoryAccess historyAccess);

    /**
     * Servicio para actualizar un hist&oacute;rico de acceso.
     *
     * @param historyAccess
     */
    void update(HistoryAccess historyAccess);

    /**
     * Elimina un hist&oacute;rico de acceso.
     *
     * @param historyAccess
     */
    void remove(HistoryAccess historyAccess);

    
    /**
     * Consulta de historial de accesos
     *
     * @param historyAccess
     */
    List<HistoryAccess> findHistoricAccessUser(User user, Date initDate,
        Date finishDate);

}
