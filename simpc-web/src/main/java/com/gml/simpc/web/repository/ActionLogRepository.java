/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ActionLogRepository.java
 * Created on: 2016/11/15, 10:52:58 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository;

import com.gml.simpc.api.entity.ActionLog;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public interface ActionLogRepository extends JpaRepository<ActionLog, String> {

    /**
     * Consulta de historial de acciones
     * @param numberIdentification
     * @param initDate
     * @param finishDate
     * @return 
     */
    @Query(value =
        "Select LA.id, LA.FECHA_MODIFICACION, LA.usuario, LA.ACCION, " +
        "LA.PARAMETROS, LA.TABLA_ASOCIADA from ACCIONES_LOG " +
        "LA inner join usuarios us on (us.ID=LA.usuario) where " +
        "us.NUMERO_IDENTIFICACION like ?1 and LA.FECHA_MODIFICACION " +
        "BETWEEN ?2 and ?3",
        nativeQuery = true)
    List<ActionLog> getActionsLog(String numberIdentification, Date initDate,
        Date finishDate);

    /**
     * Consulta de historial de acciones
     * @param numberIdentification
     * @return 
     */
    @Query(value =
        "Select LA.id, LA.FECHA_MODIFICACION, LA.usuario, LA.ACCION, " +
        "LA.PARAMETROS, LA.TABLA_ASOCIADA from ACCIONES_LOG " +
        "LA inner join usuarios us on (us.ID=LA.usuario) where " +
        "us.NUMERO_IDENTIFICACION like ?1",
        nativeQuery = true)
    List<ActionLog> getActionsLog(String numberIdentification);

}
