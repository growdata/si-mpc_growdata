/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Interceptor.java
 * Created on: 2016/11/04, 03:27:52 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.commons.interceptor;

import com.gml.simpc.api.entity.ActionLog;
import com.gml.simpc.api.entity.Session;
import com.gml.simpc.api.services.ActionLogService;
import com.gml.simpc.commons.annotations.AnnotationInterceptor;
import com.gml.simpc.commons.storage.SessionStorage;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
/**
 * Clase que realizar&aacute; la intercepci&oacute;n sobre la anotaci&oacute;n.
 *
 * @author Javier Rocha
 */
@Component
@Aspect
public class Interceptor {

    /**
     * Logger de la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(Interceptor.class);
    /**
     * servicio de funcionalidades
     */
    @Autowired
    private ActionLogService actionLogService;

    /**
     * Se ejecuta antes de ingresar a un m&eacute;todo anotado.
     *
     * @param joinPoint  Punto de intercepci&oacute;n.
     * @param annotationInterceptor Anotaci&oacute;n.
     */
    @Before("@annotation(annotationInterceptor)")
    public void beforeExecution(final JoinPoint joinPoint,
        final AnnotationInterceptor annotationInterceptor) {
        
        try {
            final Session session = SessionStorage.getValue();

            ExecutorService executor = Executors.newSingleThreadExecutor();

            executor.execute(new Runnable() {
                @Override
                public void run() {
                    StringBuilder parameters = new StringBuilder();
                    for (Object object : joinPoint.getArgs()) {
                        parameters = parameters.append(object.toString()).
                            append(", \\n ");
                    }
                    ActionLog actionLog = new ActionLog();
                    actionLog.setAction(annotationInterceptor.methodName());
                    actionLog.setEntity(annotationInterceptor.entityTableName());
                    actionLog.setFecha(new Date());
                    actionLog.setParameters(parameters.toString());
                    actionLog.setUser(session.getUser());

                    actionLogService.save(actionLog);
                }
            });

            executor.shutdown();
        } catch (Exception ex) {
            LOGGER.error("Error", ex);
        } finally {
            LOGGER.info("finally en beforeExecution en Interceptor");
        }

    }

    /**
     * Se ejecuta despues de ingresar a un m&eacute;todo anotado.
     *
     * @param joinPoint  Punto de intercepci&oacute;n.
     * @param annotation Anotaci&oacute;n.
     */
    @After("@annotation(annotationInterceptor)")
    public void afterExecution(JoinPoint joinPoint,
        AnnotationInterceptor annotationInterceptor) {

        LOGGER.info("Ejecutando intercepcion despues de la ejecucion. [ " +
            annotationInterceptor.afterExecutionValue() + " ]");
    }
}
