    
    CREATE OR REPLACE VIEW VW_COSTO_PROM_MATRIC (CODIGO_MODULO, FECHA_INICIO, TIPO_CURSO, XKEY, VALUE) AS 
      SELECT CAP.CODIGO_MODULO,
            CAP.FECHA_INICIO FECHA_INICIO,
            CAP.TIPO_CURSO,
            TP.DESCRIPCION XKEY,
            SUM(CAP.COSTO_MATRICULA) VALUE
          FROM MD_F_CAP_PROGRAMA_MODULO CAP  
            INNER JOIN TIPOS_PROGRAMA TP ON (CAP.TIPO_CURSO=TP.ID) 
            INNER JOIN MD_F_PROGRAMA_DETALLE DET ON CAP.CODIGO_MODULO = DET.CODIGO_MODULO
          WHERE CAP.TIPO_OTROS_COSTOS = 1
          GROUP BY CAP.CODIGO_MODULO, CAP.FECHA_INICIO, CAP.TIPO_CURSO, TP.DESCRIPCION
    UNION ALL
     SELECT CAP.CODIGO_MODULO,
            CAP.FECHA_INICIO FECHA_INICIO,
            CAP.TIPO_CURSO,
            TP.DESCRIPCION XKEY,
            SUM(CAP.COSTO_MATRICULA) VALUE
          FROM MD_F_CAP_PROGRAMA_MODULO CAP  
            INNER JOIN TIPOS_PROGRAMA TP ON (CAP.TIPO_CURSO=TP.ID) 
          WHERE CAP.TIPO_OTROS_COSTOS = 2
          GROUP BY CAP.CODIGO_MODULO, CAP.FECHA_INICIO, CAP.TIPO_CURSO, TP.DESCRIPCION;
    COMMIT;

--------------------------------------------------

create or replace view VW_PIE_CAPACITACIONES as
SELECT  distinct CODIGO_CCF , fecha_liquidacion_beneficio,detalle.documento,f.NUMERO_IDENTIFICACION,
nvl2(detalle.documento,1,0) Capacita
FROM MD_F_FOSFEC f 
left  join  Md_f_programa_detalle detalle
on (f.TIPO_IDENTIFICACION=detalle.tipo_documento and f.NUMERO_IDENTIFICACION=detalle.documento)
where verificacion_requisitos=1;
commit;

------------------------------------------------------------------------------------------------------------

create or replace view ASISTENTES_CAPACITACIONES as
select  distinct
capacitaciones."CODIGO_PROGRAMA",capacitaciones."NOMBRE_PROGRAMA",
capacitaciones."TIPO_CURSO",
capacitaciones."CODIGO_CCF",capacitaciones."CODIGO_INST",
capacitaciones."CERTIFICADA",capacitaciones."NOMBRE_CERTIFICACION",capacitaciones."CINE",
capacitaciones."NECESIDAD",capacitaciones."CODIGO_MODULO",capacitaciones."NOMBRE_MODULO",
capacitaciones."TIPO_COSTO_MATRICULA",capacitaciones."COSTO_MATRICULA",capacitaciones."TIPO_OTROS_COSTOS",capacitaciones."OTROS_COSTOS",
capacitaciones."FECHA_INICIO",
capacitaciones."FECHA_FIN",
detalle.TIPO_DOCUMENTO
        ,detalle.DOCUMENTO
        ,detalle.ESTADO
        ,detalle.COSTO_TRANSPORTE
       
  from 
    MD_F_PROGRAMA_DETALLE detalle   inner join   MD_F_CAP_PROGRAMA_MODULO  capacitaciones 
    on capacitaciones.CODIGO_MODULO = detalle.CODIGO_MODULO;
commit;

----------------------------------------------------------------------------------------------------------------
--Creaci�n de vista para tipos de institucion
create or replace view VW_PIE_CESANTIAS as
with data  as (



SELECT md.CODIGO_CCF ,  md.fecha_liquidacion_beneficio,
count (  distinct MONTO_LIQUIDADO_CESANTIAS ||md.fecha_liquidacion_beneficio || 
NUMERO_IDENTIFICACION ||TIPO_IDENTIFICACION) BENEFICIARIOS_CESANTIAS,
null as otros 
FROM MD_F_FOSFEC md
WHERE  md.MONTO_LIQUIDADO_CESANTIAS <> 0
group by CODIGO_CCF ,  fecha_liquidacion_beneficio


union

SELECT  p.CODIGO_CCF ,  p.fecha_liquidacion_beneficio,
null as BENEFICIARIOS_CESANTIAS,
count (  distinct p.fecha_liquidacion_beneficio || NUMERO_IDENTIFICACION ||TIPO_IDENTIFICACION) OTROS

FROM MD_F_FOSFEC p
WHERE 
p.MONTO_LIQUIDADO_CESANTIAS = 0
group by p.CODIGO_CCF ,  p.fecha_liquidacion_beneficio

)
SELECT fecha_liquidacion_beneficio,CODIGO_CCF , "DESCRIPCION_MONTO","VALUE" FROM data
unpivot
(
  value

    for DESCRIPCION_MONTO in ("OTROS","BENEFICIARIOS_CESANTIAS"))
commit;

-----------------------------------------------------------------------

create or replace view VW_PIE_CUOTA as
with data  as (


SELECT md.CODIGO_CCF ,  md.fecha_liquidacion_beneficio,
count (MONTO_LIQUIDADO_CUOTA) CUOTA_MONETARIA,
null as otros 
FROM MD_F_FOSFEC md
WHERE  md.MONTO_LIQUIDADO_CUOTA <> 0
group by CODIGO_CCF ,  fecha_liquidacion_beneficio


union

SELECT  p.CODIGO_CCF ,  p.fecha_liquidacion_beneficio,
null as CUOTA_MONETARIA,
count(p.MONTO_LIQUIDADO_SALUD) OTROS 
FROM MD_F_FOSFEC p WHERE  p.MONTO_LIQUIDADO_CUOTA = 0
group by p.CODIGO_CCF ,  p.fecha_liquidacion_beneficio

)
SELECT fecha_liquidacion_beneficio,CODIGO_CCF , "DESCRIPCION_MONTO","VALUE" FROM data
unpivot
(
  value
    for DESCRIPCION_MONTO in ("OTROS","CUOTA_MONETARIA"))
;
commit;

------------------------------------------------------------------------------------------------

create or replace view VW_TIPOS_INSTITUCION_CARGADAS as
 select codigo_modulo,codigo_inst,fecha_inicio,tipo_institucion_id from MD_F_CAP_PROGRAMA_MODULO
 inner join instituciones i on i.id=codigo_inst

;
commit;

----------------------------------------------------------------------------------------------

create or replace view VW_TIPOS_CERTIFICACION as
SELECT 
                c.id, c.codigo DESCRIPCION 
FROM   certificaciones  c

;
commit;





-------------------------------------------------------------------------------------------------------

    CREATE OR REPLACE VIEW VW_FINALIZACION_BENEFICIO (CODIGO_CCF, TIPO_IDENTIFICACION, NUMERO_IDENTIFICACION, TERMINACION_BENEFICIO, FECHA_LIQUIDACION_BENEFICIO) AS 
      WITH CTE AS (
      SELECT L.CODIGO_CCF, L.TIPO_IDENTIFICACION, L.NUMERO_IDENTIFICACION, L.TERMINACION_BENEFICIO, L.FECHA_LIQUIDACION_BENEFICIO,
      LAG(TERMINACION_BENEFICIO,1,'NA') OVER (PARTITION BY L.CODIGO_CCF, L.TIPO_IDENTIFICACION, L.NUMERO_IDENTIFICACION ORDER BY L.FECHA_LIQUIDACION_BENEFICIO DESC) ROW_NUMBER
      FROM MD_F_FOSFEC L 
      WHERE L.TERMINACION_BENEFICIO = 2
      ORDER BY  L.FECHA_LIQUIDACION_BENEFICIO)
      SELECT CODIGO_CCF, TIPO_IDENTIFICACION, NUMERO_IDENTIFICACION, TERMINACION_BENEFICIO, FECHA_LIQUIDACION_BENEFICIO 
      FROM CTE
      WHERE ROW_NUMBER = 'NA'
    UNION ALL
      SELECT L.CODIGO_CCF, L.TIPO_IDENTIFICACION, L.NUMERO_IDENTIFICACION, L.TERMINACION_BENEFICIO, L.FECHA_LIQUIDACION_BENEFICIO
      FROM MD_F_FOSFEC L 
      WHERE L.TERMINACION_BENEFICIO = 1;
    COMMIT;

----------------------------------------------------------------------------------------------

create or replace view VW_TIPOS_CERTIFICACION as
SELECT 
                c.id, c.codigo DESCRIPCION 
FROM   certificaciones  c

;
commit;

------------------------------------------------------------------------------------------