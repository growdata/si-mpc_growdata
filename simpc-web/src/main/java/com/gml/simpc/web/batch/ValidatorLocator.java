/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: ValidatorLocator.java
 * Created on: 2017/01/20, 08:52:43 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.batch;

import com.gml.simpc.loader.dto.ValidationErrorDto;
import com.gml.simpc.loader.validator.ValidateCommand;
import com.gml.simpc.web.listeners.impl.LoadQueueServiceImpl;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Clase para encontrar el validador de negocio adecuado por tipo de carga
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica Jim&eacute;nez Rozo</a>
 */
@Service(value = "validatorLocatorService")
public class ValidatorLocator {
    
    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER = 
        Logger.getLogger(LoadQueueServiceImpl.class);    
    
    /**
     * Lista de implementaciones.
     */
    @Autowired 
    private List<ValidateCommand> implementations;
    
    /**
     * Almacena la lista de errores.
     *
     */
    List <ValidationErrorDto> errorsList;
 
    /**
     * M&eacute;todo que selecciona el validador adecuado y ejecuta la validacion
     * mensaje.
     * @param loadType
     * @param loadId
     * @return
     */
    public final List<ValidationErrorDto> evaluate(String loadType,Long loadId) {
        
        this.errorsList = new ArrayList<>();

        for (ValidateCommand valCmd : implementations) {

            if (valCmd.verify(loadType)) {
                this.errorsList= valCmd.validate(loadId);
            }
        }
        return this.errorsList;
    }
}
