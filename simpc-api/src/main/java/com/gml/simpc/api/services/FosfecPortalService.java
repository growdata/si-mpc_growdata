/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: FosfecPortalService.java
 * Created on: 2017/01/31, 04:19:20 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.dto.FosfecResourcesDto;
import com.gml.simpc.api.dto.TableDataDto;
import java.util.List;

/**
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface FosfecPortalService {
    
    /**
     * Obtiene los datos para los recursos de Fosfec
     *
     * @return
     */
    List<TableDataDto> getFosfecResources(String ccf,
        String year, String month);

    /**
     * Obtiene los datos para el grafico segun el filtro
     *
     * @return
     */
    List<FosfecResourcesDto> graphicFosfecResources(String ccf,
        String year, String month);
    
    /**
     * Obtiene los datos para mostrar detalle recursos de Fosfec
     *
     * @return
     */
    List<TableDataDto> getFosfecResourcesDetail(String ccf,
        String year, String month);

    /**
     * Obtiene los datos para el grafico segun el filtro para el detalle
     *
     * @return
     */
     List<FosfecResourcesDto> graphicFosfecResourcesDetail(String ccf,
        String year, String month);


}
