/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: BasicInformationDeleteWriter.java
 * Created on: 2017/09/14, 03:14:59 PM
 * Project: D&eacute;bito Autom&aacute;tico.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.writer;

import com.gml.simpc.api.dto.BasicInformationDto;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.batch.item.ItemWriter;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public class BasicInformationDeleteWriter
    implements ItemWriter<BasicInformationDto> {

    /**
     * Plantilla jdbc.
     */
    private JdbcTemplate jdbcTemplate;
    /**
     * DataSource.
     */
    private DataSource dataSource;
    /**
     * SQL.
     */
    private String sql;

    @PostConstruct
    public void init() {
        this.jdbcTemplate = new JdbcTemplate(this.dataSource);
    }

    @Override
    public void write(List<? extends BasicInformationDto> list)
        throws Exception {
        for (BasicInformationDto item : list) {
            try {
                this.jdbcTemplate.update(this.sql, item.getDocumentType(),
                    item.getNumberIdentification());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }
}
