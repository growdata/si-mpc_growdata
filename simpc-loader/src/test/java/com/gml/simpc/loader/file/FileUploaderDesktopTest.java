/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: FileUploaderDesktopTest.java
 * Created on: 2017/02/08, 09:58:17 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.file;

import com.gml.simpc.api.entity.LoadProcess;
import java.io.File;
import java.util.HashMap;
import org.junit.Ignore;
import org.junit.Test;

import static com.gml.simpc.api.entity.exeption.code.FileErrorCode.FILE_ERR_001_EMPTY_FILE;
import static com.gml.simpc.api.entity.exeption.code.FileErrorCode.FILE_ERR_012_FTP_CONNECTION;
import static org.junit.Assert.assertTrue;

/**
 * Pruebas para carga escritorio de archivos
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica</a>
 */
@Ignore
public class FileUploaderDesktopTest {

    /**
     * Logeer para la clase.
     */
    private static final org.apache.log4j.Logger LOGGER =
        org.apache.log4j.Logger.getLogger(FileUploaderDesktopTest.class);

    public FileUploaderDesktopTest() {
    }

    /**
     * Caso de prueba para archivos que no existen
     */
    @Test
    public void testDesktopUpLoadNoFoundFileCase() {
        FileUploader uploader = new FileUploader();
        String fileName = "loadTestFiles/program/training/non-exists.txt";
        String loadPath = "C:/SIMPC/UPLOADS/";
        File file = new File(fileName);
        HashMap<Object, Object> params = new HashMap<>();
        params.put("server", "localhost");
        params.put("port", "21");
        params.put("user", "simpc");
        params.put("password", "1234567890");
        LoadProcess loadProcess = new LoadProcess(loadPath, "ftp", "CAP-1", "A");
        uploader.sendFile(file, loadProcess, params);
        LOGGER.info(loadProcess.toString());
        assertTrue(loadProcess.getFileSended() == false);
        assertTrue(loadProcess.getErrorCode() == FILE_ERR_001_EMPTY_FILE);

    }

    /**
     * Caso de prueba para archivos correctos
     */
    @Test
    public void testDesktopBadConnSuccessCase() {

        LOGGER.info(
            "Running FileUploaderWebTest - testDesktopBadConnSuccessCase");
        FileUploader uploader = new FileUploader();
        String fileName =
            "loadTestFiles/program/training/program-code-error.txt";
        String loadPath = "C:/SIMPC/UPLOADS/";
        File file = new File(fileName);
        HashMap<Object, Object> params = new HashMap<>();
        params.put("server", "localhost87");
        params.put("port", "21");
        params.put("user", "simpc");
        params.put("password", "1234567890");
        LoadProcess loadProcess = new LoadProcess(loadPath, "ftp", "CAP-1", "A");
        uploader.sendFile(file, loadProcess, params);
        LOGGER.info(loadProcess.toString());
        assertTrue(loadProcess.getFileSended() == false);
        assertTrue(loadProcess.getErrorCode() == FILE_ERR_012_FTP_CONNECTION);

    }

}
