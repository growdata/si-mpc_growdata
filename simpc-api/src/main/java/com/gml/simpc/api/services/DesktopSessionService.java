/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: DesktopSessionService.java
 * Created on: 2016/10/19, 02:10:35 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.entity.DesktopSession;

/**
 * Interface que define el servicio de las sesiones de escritorio.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface DesktopSessionService {

    /**
     * Busca si el token ingresado coincide con el que esta en base tras el
     * registro.
     *
     * @param token Token generado en el momento del registro
     *
     * @return Entidad de activaci&ocute;n
     */
    DesktopSession findByUserToken(String token);

    /**
     * Eliminar los token expirados.
     */
    void deleteExpiratedTokens();
    
    /**
     * Servicio para guardar sesiones.
     *
     * @param desktopSession
     */
    void save(DesktopSession desktopSession);
}
