/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CommonsValidator.java
 * Created on: 2016/12/11, 12:33:45 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 * Objetive: Test program massive upload: orientation records
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.stage;

/**
 * Transfiere la informacion de los Beneficios Economicos (Archivo Fosfec)
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-batch.xml",
    "classpath:batch_jobs/program/orientation/orientation-attendance-stage-job.xml",
    "classpath:database.xml"})
@Ignore
public class OrientationAttendanceStageJobTest {

    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(OrientationAttendanceStageJobTest.class);

    /**
     * Servicio para ejecutar jobs en modo pruebas
     */
    @Autowired
    JobLauncherTestUtils jobLauncherTestUtils;

    @Test
    public void successCaseJob() throws Exception {

        LOGGER.info("Running OrientationStageJobTest - successCaseJob");

        //Inicializamos local thread
        ThreadLocalJob.init();

        // Inicializamos el job.
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath",
            "file:loadTestFiles/program/orientation/attendance/success.txt");
        jobParametersBuilder.addString("loadId", "43");

        JobExecution jobExecution = jobLauncherTestUtils.
            launchJob(jobParametersBuilder.toJobParameters());

        for (Throwable t : jobExecution.getAllFailureExceptions()) {
            t.printStackTrace();
        }

        JobResultDto jobResult = ThreadLocalJob.getResult();
        LOGGER.info(jobResult.toString());

        if (!jobResult.getErrorList().isEmpty()) {
            LOGGER.info(jobResult.getErrorsString());
        }

        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().
            getExitCode());
        Assert.assertTrue(jobResult.getErrorList().isEmpty());

        ThreadLocalJob.delete();
    }

}
