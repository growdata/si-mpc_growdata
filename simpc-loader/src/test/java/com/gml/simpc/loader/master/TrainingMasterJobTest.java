/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CommonsValidator.java
 * Created on: 2016/12/11, 12:33:45 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 * Objetive: Test program massive upload: training records
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.master;

/**
 * Unit Tests for load to master from training load.
 * Requires set correct value in property name="password", 
 * resources/default package/database.xml.
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-batch.xml", 
    "classpath:batch_jobs/program/training/training-master-job.xml",
    "classpath:database.xml"})
@Ignore
public class TrainingMasterJobTest {
    
    /**
     * Logeer para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(TrainingMasterJobTest.class);
    
    /**
     * Servicio para ejecutar jobs en modo pruebas
    */
    @Autowired
    JobLauncherTestUtils jobLauncherTestUtils;
    
    
    /**
     * Case for success writting for writing type Nuevo
     * Requires set correct value in property name="password", 
     * resources/default package/database.xml.
     * Please comment @Ignore anotation only for local tests. Uncomment again after pull request.
     * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
    */
    @Test
    public void successCaseNewJob() throws Exception {
        
        System.out.println("Running TrainingMasterJobTest - successCaseNewJob");

       //Inicializamos local thread
        ThreadLocalJob.init();
 
        // Inicializamos el job.
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath", 
            "file:loadTestFiles/program/training/success.txt"); 
        jobParametersBuilder.addString("loadId", "10");
        jobParametersBuilder.addString("writeType", "N");
        
        JobExecution jobExecution = jobLauncherTestUtils.
            launchJob(jobParametersBuilder.toJobParameters());
        
        for (Throwable t : jobExecution.getAllFailureExceptions()) {
            t.printStackTrace();
        }
        
        JobResultDto jobResult = ThreadLocalJob.getResult();
        LOGGER.info(jobResult.toString());
        
        if (!jobResult.getErrorList().isEmpty()){
            LOGGER.info(jobResult.getErrorsString());
        }
        
        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().getExitCode());
        Assert.assertTrue(jobResult.getErrorList().isEmpty());
        
        ThreadLocalJob.delete();
    }

    /**
     * Case for success writting for writing type Actualizacion
     * Requires set correct value in property name="password", 
     * resources/default package/database.xml.
     * Please comment @Ignore anotation only for local tests. Uncomment again after pull request.
     * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
    */
    @Test
    public void successCaseUpdateJob() throws Exception {
        
        System.out.println("Running TrainingMasterJobTest - successCaseUpdateJob");

       //Inicializamos local thread
        ThreadLocalJob.init();
 
        // Inicializamos el job.
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("filePath", 
            "file:loadTestFiles/program/training/success.txt"); 
        jobParametersBuilder.addString("loadId", "20");
        jobParametersBuilder.addString("writeType", "A");
        
        JobExecution jobExecution = jobLauncherTestUtils.
            launchJob(jobParametersBuilder.toJobParameters());
        
        for (Throwable t : jobExecution.getAllFailureExceptions()) {
            t.printStackTrace();
        }
        
        JobResultDto jobResult = ThreadLocalJob.getResult();
        LOGGER.info(jobResult.toString());
        
        if (!jobResult.getErrorList().isEmpty()){
            LOGGER.info(jobResult.getErrorsString());
        }
        
        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().getExitCode());
        Assert.assertTrue(jobResult.getErrorList().isEmpty());
        
        ThreadLocalJob.delete();
    }
 }
