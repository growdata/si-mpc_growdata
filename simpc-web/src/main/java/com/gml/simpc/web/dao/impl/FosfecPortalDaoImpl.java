/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: FosfecPortalDaoImpl.java
 * Created on: 2017/02/01, 09:18:03 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.dao.impl;

import com.gml.simpc.api.dao.FosfecPortalDao;
import com.gml.simpc.api.dto.FosfecResourcesDto;
import com.gml.simpc.api.dto.TableDataDto;
import com.gml.simpc.api.utilities.Util;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
@Repository
public class FosfecPortalDaoImpl implements FosfecPortalDao {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(FosfecPortalDaoImpl.class);
    
 

    /**
     * Plantilla JDBC para consultar las propiedades.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Consulta para hallar los valores por mes de los montos salud,pension
     * y beneficios segun el filtro
     */
    private static final String FIND_FOSFEC_RESOURCES =
        "SELECT  periodo_beneficio_liquidado,mes," +
        "SUM(monto_liquidado_salud) salud,SUM (monto_liquidado_pension) pension, " +
        "SUM(monto_liquidado_cuota) cuota_monetaria, SUM (monto_liquidado_alimentacion) bono, " +
        "SUM(monto_liquidado_salud+monto_liquidado_pension+monto_liquidado_cuota+monto_liquidado_alimentacion) total " +
        "FROM  (SELECT  distinct  periodo_beneficio_liquidado,numero_identificacion," +
        "TO_CHAR(fecha_liquidacion_beneficio,'MM' )MES ,monto_liquidado_salud, " +
        "monto_liquidado_pension,monto_liquidado_cuota,monto_liquidado_alimentacion " +
        "FROM MD_F_FOSFEC  where codigo_ccf like ? AND TO_CHAR(fecha_liquidacion_beneficio,'MM' )like ? " +
        "AND TO_CHAR(fecha_liquidacion_beneficio,'yyyy' )like ?) " +
        "GROUP BY periodo_beneficio_liquidado,mes ORDER BY mes";

    private static final String FIND_FOSFEC_DETAIL = "SELECT ccf ,SUM(monto_liquidado_salud) salud," +
        "SUM (monto_liquidado_pension) pension,SUM(monto_liquidado_cuota) cuota_monetaria," +
        "SUM (monto_liquidado_alimentacion) bono , SUM(monto_liquidado_salud+monto_liquidado_pension+monto_liquidado_cuota+monto_liquidado_alimentacion) total FROM " +
        "(SELECT  distinct  f.nombre_corto ccf,periodo_beneficio_liquidado,numero_identificacion,TO_CHAR(fecha_liquidacion_beneficio,'MM' )MES ," +
        "monto_liquidado_salud, monto_liquidado_pension,monto_liquidado_cuota,monto_liquidado_alimentacion " +
        "FROM MD_F_FOSFEC mdf INNER JOIN FUNDACIONES f ON mdf.codigo_ccf=f.codigo " +
        " where UPPER(codigo_ccf) like UPPER(?) AND TO_CHAR(fecha_liquidacion_beneficio,'MM' )like ? AND TO_CHAR(fecha_liquidacion_beneficio,'yyyy' )like ?) GROUP BY ccf"; 

    @Override
    public List<TableDataDto> getFosfecResources(String ccf, String year,
        String month) {

        return this.jdbcTemplate.query(FIND_FOSFEC_RESOURCES, new RowMapper() {
            @Override
            public TableDataDto mapRow(ResultSet rs, int rownumber)
                throws SQLException {
                TableDataDto dto = new TableDataDto();

                dto.addColumn(rs.getString("MES"));
                dto.addColumn(rs.getString("SALUD"));
                dto.addColumn(rs.getString("PENSION"));
                dto.addColumn(rs.getString("CUOTA_MONETARIA"));
                dto.addColumn(rs.getString("BONO"));
                dto.addColumn(rs.getString("TOTAL"));

                return dto;
            }
        }, ccf, month, year);

    }

    @Override
    public List<FosfecResourcesDto> graphicFosfecResources(String ccf,
        String year,
        String month) {

        return this.jdbcTemplate.query(FIND_FOSFEC_RESOURCES, new RowMapper() {
            @Override
            public FosfecResourcesDto mapRow(ResultSet rs, int rownumber)
                throws SQLException {
                FosfecResourcesDto dto = new FosfecResourcesDto();

                dto.setMonth(Util.MONTHS_NAMES.get(rs.getInt("MES") - 1));
                dto.setHealth(rs.getString("SALUD"));
                dto.setPension(rs.getString("PENSION"));
                dto.setMonetaryQuota(rs.getString("CUOTA_MONETARIA"));
                dto.setBonds(rs.getString("BONO"));
                dto.setTotal(rs.getString("TOTAL"));

                return dto;
            }
        }, ccf, month, year);
    }

    @Override
    public List<TableDataDto> getFosfecResourcesDetail(String ccf, String year,
        String month) {

        return this.jdbcTemplate.query(FIND_FOSFEC_DETAIL, new RowMapper() {
            @Override
            public TableDataDto mapRow(ResultSet rs, int rownumber)
                throws SQLException {
                TableDataDto dto = new TableDataDto();

                dto.addColumn(rs.getString("CCF"));
                dto.addColumn(rs.getString("SALUD"));
                dto.addColumn(rs.getString("PENSION"));
                dto.addColumn(rs.getString("CUOTA_MONETARIA"));
                dto.addColumn(rs.getString("BONO"));
                dto.addColumn(rs.getString("TOTAL"));

                return dto;
            }
        }, ccf, month, year);
    }

    @Override
    public List<FosfecResourcesDto> graphicFosfecResourcesDetail(String ccf,
        String year, String month){

        return this.jdbcTemplate.query( FIND_FOSFEC_DETAIL, new RowMapper() {
            @Override
            public FosfecResourcesDto mapRow(ResultSet rs, int rownumber)
                throws SQLException {
                FosfecResourcesDto dto = new FosfecResourcesDto();

                dto.setCcf(rs.getString("CCF"));
                dto.setTotal(rs.getString("TOTAL"));

                return dto;
            }
        }, ccf, month, year);
    }

}
