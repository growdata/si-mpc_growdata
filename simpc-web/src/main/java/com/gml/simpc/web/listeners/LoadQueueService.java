/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ExpirationService.java
 * Created on: 2016/11/23, 12:00:20 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.listeners;

/**
 * Definici&ocute;n del listener de procesamiento de cargas
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
public interface LoadQueueService {

    /**
     * Expira los token en la tabla de acticaci&ocute;n de acuerdo al tiempo
     * establecido
     */
    void init();
}
