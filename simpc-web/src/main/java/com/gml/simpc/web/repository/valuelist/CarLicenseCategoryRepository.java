/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: KnowledgeCoreRepository.java
 * Created on: 2017/01/23, 02:28:09 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository.valuelist;

import com.gml.simpc.api.entity.valuelist.CarLicenseCategory;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public interface CarLicenseCategoryRepository 
    extends JpaRepository<CarLicenseCategory, Long> {

}