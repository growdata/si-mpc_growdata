/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: ValidationErrorDto.java
 * Created on: 2017/01/06, 10:41:19 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.dto;

/**
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica Jim&eacute;nez Rozo</a>
 */
public class ValidationErrorDto {
    
     /**
     * L�nea en la que se presenta el error
     */
    private int line;
    
     /**
     * Lista de mensajes de error
     */
    private String message;
    
    /*-----------------------------------------------------------------------*/

    public ValidationErrorDto(int line,String message) {
        this.line=line;
        this.message=message;
    }

    public ValidationErrorDto() {
    }

    /*-----------------------------------------------------------------------*/

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    
    
}
