/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: FosfecPortalServiceImpl.java
 * Created on: 2017/01/31, 04:28:18 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;
import com.gml.simpc.api.dao.FosfecPortalDao;
import com.gml.simpc.api.dto.FosfecResourcesDto;
import com.gml.simpc.api.dto.TableDataDto;
import com.gml.simpc.api.services.FosfecPortalService;
import com.gml.simpc.commons.annotations.AnnotationInterceptor;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
@Service
public class FosfecPortalServiceImpl implements FosfecPortalService{
    
    /**
     * Dao para manejar la persistencia.
     */
    @Autowired
    private FosfecPortalDao fosfecPortalDao;
    
    /**
     * Genera la consulta de recursos fosfec.
     * 
     * @param ccf
     * @param year
     * @param month
     * @return 
     */
     @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar Recursos " +
        "Fosfec", entityTableName = "MD_F_FOSFEC")
    @Override
    public List<TableDataDto> getFosfecResources(String ccf, String year,
        String month) {
       return fosfecPortalDao.getFosfecResources(ccf, year, month);
    }
    
    /**
     * Genera la grafica asociada a los recursos fosfec.
     * 
     * @param ccf
     * @param year
     * @param month
     * @return 
     */
     @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar Graficas " +
        "de Recursos Fosfec", entityTableName = "MD_F_FOSFEC")
    @Override
    public List<FosfecResourcesDto> graphicFosfecResources(String ccf, String year,
        String month) {
        return fosfecPortalDao.graphicFosfecResources(ccf, year, month);
    }
    
    /**
     * Genera la consulta detallada de recursos fosfec.
     * 
     * @param ccf
     * @param year
     * @param month
     * @return 
     */
     @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar Detallado " +
        "de Recursos Fosfec", entityTableName = "MD_F_FOSFEC")
    @Override
    public List<TableDataDto> getFosfecResourcesDetail(String ccf, String year,
        String month) {
        return fosfecPortalDao.getFosfecResourcesDetail(ccf, year, month);
    }

    /**
     * Genera la grafica asociada al detalle de recursos fosfec.
     * 
     * @param ccf
     * @param year
     * @param month
     * @return 
     */
     @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName = "Consultar Gr�ficas " +
        "de Detallado de Recursos Fosfec", entityTableName = "MD_F_FOSFEC")
    @Override
    public  List<FosfecResourcesDto> graphicFosfecResourcesDetail(String ccf,
        String year, String month){
        return fosfecPortalDao.graphicFosfecResourcesDetail(ccf, year, month);
    }

}
