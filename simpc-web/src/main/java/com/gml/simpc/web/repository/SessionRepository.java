/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserRepository.java
 * Created on: 2016/10/19, 02:03:51 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository;

import com.gml.simpc.api.entity.Session;
import com.gml.simpc.api.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repositorio para instituciones.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Transactional
public interface SessionRepository extends JpaRepository<Session, Long> {

    /**
     * Actualiza la sesi&oacute;n.
     *
     * @param userId
     *
     * @return
     */
    @Modifying
    @Query(value = "UPDATE SESIONES SET ULTIMA_OPERACION = SYSTIMESTAMP " +
        "WHERE ULTIMA_OPERACION >(SYSTIMESTAMP - (.000694 * (SELECT " +
        "TO_NUMBER(VALOR) FROM PARAMETROS WHERE ID = 1))) AND USUARIO = ?1",
        nativeQuery = true)
    Integer updateSession(Long userId);

    /**
     * M&eacute;todo para verificar la existencia de la sesi&oacute;n
     *
     * @param user Nombre del usuario suministrado por el administrador
     *
     * @return
     *
     */
    @Query(nativeQuery = true, value = "SELECT * FROM SESIONES WHERE " +
        "ULTIMA_OPERACION > (SYSTIMESTAMP - (.000694 * (SELECT " +
        "TO_NUMBER(VALOR) FROM PARAMETROS WHERE ID = 1))) AND USUARIO = ?1")
    Session findByUser(User user);

    /**
     * Expira las sesiones que tiene un tiempo de inactividad determinado
     * por parametros del sistema. La fracci&ocute;n indica el tiempo de
     * vencimiento
     * de la sesi&ocute;n. Para pruebas se utiliza un minuto
     *
     * @return
     *         Entero que indica el borrado
     */
    @Modifying
    @Query(value = "DELETE FROM SESIONES " + 
        "WHERE (ULTIMA_OPERACION < " +
        "      (SYSTIMESTAMP - (.000694 * (SELECT TO_NUMBER(VALOR) FROM PARAMETROS " +
        "         WHERE NOMBRE = 'VENCIMIENTO_SESION')))) "+
        "OR "+ 
        "(extract(minute from (SYSTIMESTAMP - CAST(ultimo_ping as timestamp))) >=1) "
        , nativeQuery = true)
    Integer expireSessions();
    
    /**
     * Actualiza la sesi&oacute;n.
     *
     * @param sessionId
     *
     * @return
     */
    @Modifying
    @Query(value = "UPDATE SESIONES SET ULTIMO_PING = SYSTIMESTAMP " +
        "WHERE ID = ?1",
        nativeQuery = true)
    Integer sessionPing(Long sessionId);
}
