/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: User.java
 * Created on: 2016/10/19, 01:47:45 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gml.simpc.api.entity.valuelist.City;
import com.gml.simpc.api.entity.valuelist.DocumentType;
import com.gml.simpc.api.entity.valuelist.Foundation;
import com.gml.simpc.api.entity.valuelist.State;
import com.gml.simpc.api.enums.UserStatus;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.hibernate.validator.constraints.Email;

/**
 * Entidad que representa el usuario.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Table(name = "USUARIOS", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"NOMBRE_USUARIO"},
        name = User.UQ_USUARIOS_USUARIO),
    @UniqueConstraint(columnNames = {"CORREO_ELECTRONICO"},
        name = User.UQ_USUARIOS_CORREO),
    @UniqueConstraint(columnNames = {"TIPO_DOCUMENTO", "NUMERO_IDENTIFICACION"},
        name = User.UQ_USUARIOS_IDENTIFICACION)})
@Entity
public class User implements Serializable {

    public static final String UQ_USUARIOS_IDENTIFICACION =
        "UQ_USUARIOS_IDENTIFICACION";
    public static final String UQ_USUARIOS_USUARIO = "UQ_USUARIOS_USUARIO";
    public static final String UQ_USUARIOS_CORREO = "UQ_USUARIOS_CORREO";
    /**
     * ID de la entidad.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator =
        "user_seq_gen")
    @SequenceGenerator(name = "user_seq_gen", sequenceName =
        "USER_SEQ", allocationSize = 1, initialValue= 1)
    private Long id;
    /**
     * tipo de documento
     */
    @ManyToOne
    @JoinColumn(name = "TIPO_DOCUMENTO", nullable = false)
    private DocumentType documentType;
    /**
     * N�mero de documento del usuario
     */
    @Column(name = "NUMERO_IDENTIFICACION", nullable = false)
    private String identificationNumber;
    /**
     * UserName del consultor.
     */
    @Column(name = "NOMBRE_USUARIO")
    private String username;
    /**
     * Password del consultor.
     */
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "PASSWORD_USUARIO")
    private String password;
    /**
     * perfil asignado al consultor.
     */
    @ManyToOne
    @JoinColumn(name = "PERFIL", nullable = false)
    private Profile profile;
    /**
     * estado de consultor (Activo o Inactivo).
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "ESTADO", columnDefinition = "char(1) default 'T'",
        nullable = false)
    private UserStatus status;
    /**
     * Caja de Compensacion Familiar a la que pertenece el consultor.
     */
    @ManyToOne
    @JoinColumn(name = "CAJA_COMPENSACION", nullable = false)
    private Foundation ccf;
    /**
     * Primer nombre del consultor.
     */
    @Column(name = "PRIMER_NOMBRE", nullable = false)
    private String firstName;
    /**
     * Segundo nombre del consultor.
     */
    @Column(name = "SEGUNDO_NOMBRE")
    private String secondName;
    /**
     * Primer apellido del consultor.
     */
    @Column(name = "PRIMER_APELLIDO", nullable = false)
    private String firstSurname;
    /**
     * Segundo apellido del consultor.
     */
    @Column(name = "SEGUNDO_APELLIDO")
    private String secondSurname;
    /**
     * Tel&eacute;fono del consultor.
     */
    @Column(name = "NUMERO_TELEFONO", nullable = false)
    private String phoneNumber;
    /**
     * Correo electr&oacute;nico del consultor.
     */
    @Email
    @Column(name = "CORREO_ELECTRONICO", nullable = false)
    private String email;
    /**
     * Correo electr&oacute;nico del consultor.
     */
    @Column(name = "DIRECCION", nullable = false)
    private String address;
    /**
     * Booleano que determina si el usuario es administrador o no
     */
    @Column(name = "ADMINISTRADOR", nullable = false,
        columnDefinition = "NUMBER(1) DEFAULT 0")
    private boolean admin;

    /**
     * Id de Usuario de creaci&oacute:n. Campo de Auditor&iacute;a.
     */
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "ID_USUARIO_CREACION")
    private User creationUserId;
    /**
     * fecha de creaci&oacute:n. Campo de Auditor&iacute;a.
     */
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    /**
     * Id de Usuario de modificaci&oacute;n.Campo de Auditor&iacute;a.
     */
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "ID_USUARIO_MODIFICACION")
    private User modificationUserId;
    /**
     * fecha de modificaci&oacute:n. Campo de Auditor&iacute;a.
     */
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modificationDate;
    /**
     * fecha de modificaci&oacute:n. Campo de Auditor&iacute;a.
     */
    @Column(name = "ULTIMO_CAMBIO_CONTRASENA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastPasswordChange;
    
    /**
     * departamento al cual pertenece el usuario.
     */
    @ManyToOne
    @JoinColumn(name = "ID_MUNICIPIO", nullable = false)
    private City city;
    
    /**
     * campo transiente para mantener id de ciudad en vista
     */
    @Transient
    private Long transCityId;

    /**
     * departamento al cual pertenece el usuario.
     */
    @ManyToOne
    @JoinColumn(name = "ID_DEPARTAMENTO", nullable = false)
    private State state;
    
    
    public User() {
        this.id = null;
    }

    public User(Long id) {
        this.id = id;
    }

    public User(Foundation ccf) {
        this.ccf = ccf;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public Foundation getCcf() {
        return ccf;
    }

    public void setCcf(Foundation ccf) {
        this.ccf = ccf;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getFirstSurname() {
        return firstSurname;
    }

    public void setFirstSurname(String firstSurname) {
        this.firstSurname = firstSurname;
    }

    public String getSecondSurname() {
        return secondSurname;
    }

    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public User getCreationUserId() {
        return creationUserId;
    }

    public void setCreationUserId(User creationUserId) {
        this.creationUserId = creationUserId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public User getModificationUserId() {
        return modificationUserId;
    }

    public void setModificationUserId(User modificationUserId) {
        this.modificationUserId = modificationUserId;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Date getLastPasswordChange() {
        return lastPasswordChange;
    }

    public void setLastPasswordChange(Date lastPasswordChange) {
        this.lastPasswordChange = lastPasswordChange;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }
    
        public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Long getTransCityId() {
        return transCityId;
    }

    public void setTransCityId(Long transCityId) {
        this.transCityId = transCityId;
    }
    
    

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Usuario{").append("id=").append(id).
            append(",N�mero de Identificaci�on=").append(identificationNumber).
            append(", Nombre de Usuario=").append(username).
            append(", Perfil=").append(profile.getName()).
            append(", Estado=").append(status.getLabel()).
            append(", Caja de Compensaci�n=").append(ccf.getName()).
            append(", Primer Nombre=").append(firstName).
            append(", Segundo Nombre=").append(secondName).
            append(", Primer Apellido=").append(firstSurname).
            append(", Segundo Apellido=").append(secondSurname).
            append(", N�mero de Celular=").append(phoneNumber).
            append(", Correo Electr�nico=").append(email).
            append(", Direcci�n=").append(address).
            append(", Departamento=").append(state).
            append(", Municipio=").append(city).
            append(", Es Administrador=").append(admin).append('}');
        return stringBuilder.toString();
    }
}
