/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: PilaDtoRowMapper.java
 * Created on: 2017/02/13, 11:30:02 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.dao.mapper;

import com.gml.simpc.api.dto.DependentDto;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 * Row Mapper para PILA.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public class TrainingDtoRowMapper implements RowMapper<DependentDto> {
    @Override
    public DependentDto mapRow(ResultSet rs, int i) throws SQLException {
       DependentDto e = new DependentDto();
       e.setCargaId(rs.getLong("CARGA_ID"));
       e.setBirthday(rs.getString("FECHA_NACIMIENTO"));
       e.setDependentOrder(rs.getString("ORDEN_DEPENDIENTE"));
       e.setEventType(rs.getString("TIPO_NOVEDAD"));
       e.setFirstName(rs.getString("FECHA_NACIMIENTO"));
       e.setFirstSurname(rs.getString("FECHA_NACIMIENTO"));
       e.setGender(rs.getString("FECHA_NACIMIENTO"));
       e.setIdType(rs.getString("TIPO_IDENTIFICACION"));
       e.setNumberId(rs.getString("NUMERO_IDENTIFICACION"));
       e.setPersonInChargeNumber(rs.getString("NUMERO_IDENTIFICACION_CARGO"));
       e.setPersonInChargeRelationship(rs.getString("PARENTESCO_PERSONAS_CARGO"));
       e.setPersonInChargeStudiesCurrently(rs.getString("PERSONA_CARGO_ESTUDIA"));
       e.setPersonInChargeTypeId(rs.getString("TIPO_ID_PERSONA_CARGO"));
       e.setSecondName(rs.getString("SEGUNDO_NOMBRE"));
       e.setSecondSurname(rs.getString("SEGUNDO_APELLIDO"));
        return e;
    }
}
