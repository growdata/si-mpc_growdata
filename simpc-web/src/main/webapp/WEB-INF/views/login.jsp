    <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="net.tanesha.recaptcha.ReCaptcha" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mecanismo de Protecci&oacute;n al Cesante "SI-MPC"</title>
        <link href="resources/css/bootstrap.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
        <link href="resources/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"/>
        <link href="resources/css/index.css" rel="stylesheet"/>
        <script src="resources/js/jquery-1.12.3.js"></script>
        <script src="resources/js/bootstrap.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script>
            $(document).ready(function () {
                
                if (window.self !== window.top) {
                    window.top.location.reload();
                }
            });
                                                                
        </script>
    </head>
    <body>
        <c:if test="${not empty msg}"> 
            <c:if test="${empty msgType}">
                <c:set var="altype" value="danger" />
            </c:if>
            <c:if test="${not empty msgType}">
                <c:set var="altype" value="${msgType}" />
            </c:if>
            <div id="alert-${altype}" class="alert alert-${altype} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>
        <div class="container header" id="content">
            <div class="row" >
                <div class="col-sm-12 logo-image" >
                    <img src="resources/images/logo_mpc.png" style="width: 20%"/> 
                </div> 
            </div>    
        </div>    
        <div class="container content" id="content">
            <div class="row">
                <div class="col-sm-8 content-public">
                    <h1>Bienvenido </h1> 
                    <p>
                        El Sistema de Protecci�n Social en Colombia, est� regido y controlado por la Ley 100 de 1993 y 
                        re�ne de manera coordinada un conjunto de entidades, normas y procedimientos a los cuales podr�n 
                        tener acceso las personas y la comunidad.
                    </p> 
                    <br>

                    <div >

                        <caption>A continuaci�n encontrar� una serie de portales con informaci�n dirigida a todo p�blico.</caption>
                        <table class="table col-md-offset-2 table-responsive" style="width:80%">
                            <tbody>
                                <tr>
                                    <td style="border-top: none;">
                                        <br/>
                                        <a href="banco-oferentes-portal.htm"  class="btn btn-default btn-block">Banco de Oferentes</a>
                                    </td>    
                                    <td  style="border-top: none;">
                                        <p>Consultar 
                                            todas las instituciones y programas que se encuentran aprobados como 
                                            parte de la capacitaci�n para la inserci�n y reinserci�n laboral.</p>
                                    </td>
                                </tr> 
                                <tr>
                                    <td  style="border-top: none;">
                                        <a href="portal-gestion-y-colocacion.htm" type="button" class="btn btn-default btn-block">Gesti�n y Colocaci�n</a>
                                    </td>    
                                    <td  style="border-top: none;">
                                         <p>Consultar informaci�n estad�stica socioecon�mica y
                                            laboral a partir de hojas de vida.</p>
                                    </td>
                                </tr> 
                                <tr>
                                    <td  style="border-top: none;">
                                        <a href="portal-prestaciones-economicas.htm" type="button" class="btn btn-default btn-block">Prestaciones Econ�micas</a>
                                    </td>    
                                    <td  style="border-top: none;">
                                         <p>Consultar 
                                            informaci�n estad�stica de los beneficios del Mecanismo de Protecci�n al
                                            Cesante a partir de las prestaciones econ�micas.</p>
                                    </td>
                                </tr> 
                                <tr>
                                    <td  style="border-top: none;">
                                        <a href="portal-capacitacion.htm" type="button" class="btn btn-default btn-block">Capacitaci�n Inserci�n y<br> Reinserci�n Laboral</a>
                                    </td>    
                                    <td  style="border-top: none;">
                                        <p>Consultar 
                                            informaci�n estad�stica de las instituciones y programas de capacitaci�n
                                            del Mecanismo de Protecci�n al Cesante a partir de las capacitaciones 
                                            para la inserci�n y reinserci�n laboral</p>
                                    </td>
                                </tr> 
                                <tr>
                                    <td  style="border-top: none;">
                                        <a href="banco-fosfec-portal.htm" type="button" class="btn btn-default btn-block">Recursos FOSFEC</a>
                                    </td>    
                                    <td  style="border-top: none;">
                                        <p >Consultar 
                                            informaci�n estad�stica de la gesti�n de recursos del Fondo de 
                                            Solidaridad del Fomento al Empleo y Protecci�n al Cesante - FOSFEC.</p>
                                    </td>
                                </tr> 
                                <tr>
                                    <td  style="border-top: none;">
                                        <a href="portal-recursos-capacitacion.htm" type="button" class="btn btn-default btn-block">Recursos De Capacitaci�n</a>
                                    </td>    
                                    <td  style="border-top: none;">
                                         <p>Consultar 
                                            informaci�n estad�stica de recursos de capacitaci�n.</p>
                                    </td>
                                </tr> 
                            </tbody></table>
                    </div>
                </div>
                <div class="col-sm-4 content-login">
                    <form:form method="post" action="/masterdata/login.htm" 
                               autocomplete="off"
                               modelAttribute="user" id="formulario-login">
                        <div class="container loggin">
                            <h3>Ingrese al sistema</h3><br/>
                            <div class="form-group">
                                <form:label for="username" class="sr-only" path="email">Correo o Usuario </form:label>
                                <form:input id="username" class="form-control" placeholder="Correo o Usuario" path="email" required="required" />
                            </div>
                            <div class="form-group">
                                <form:label for="password" class="sr-only" path="password">Contrase�a</form:label>
                                <form:input type="password" id="password" class="form-control" path="password" placeholder="Contrase�a" required="required"/>
                            </div>
                            <div class="form-group">
                               <!-- <div class="g-recaptcha" data-sitekey="${captchaKey}"></div> -->
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <label> 
                                        <a href="#" onclick="lockedModal()" style="color: rgba(252,178,20,1.10); text-decoration:none; font-weight: bold;">Olvid� mi contrase�a</a>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <button id="loginBtn" class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
                            </div>
                        </div>            
                    </form:form>
                </div>
            </div>

            <div id="formRecuperacion" class="modal fade" role="dialog" aria-labelledby="creationModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="creationModalLabel">Recuperaci&oacute;n de contrase&ntilde;a</h4>
                        </div>
                        <div class="modal-body">
                            <div class="container">
                                <div class="alert alert-warning alert-dismissible" id="myAlert">
                                    <font><strong>�Atenci�n!</strong> Si usted es usuario del Ministerio, no podr� recuperar su contrase�a por esta opci�n, por favor comun�quese con el �rea de Tecnolog�a del Ministerio. </font>
                                </div>
                            </div>
                            <form method="get" action="/masterdata/obtenerClave.htm">
                                <div class="form-group">
                                    <label for="username" class="sr-only">Correo o Usuario </label>
                                    <input id="username" name="username" class="form-control" placeholder="Correo o Usuario" required="required" />
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-lg btn-primary btn-block" type="submit">Recuperar Contrase&ntilde;a</button>
                                </div>           
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container footer" id="footer">
            <div class="row" >
                <div class="col-sm-12 logo-image" >
                    <img src="resources/images/bg_banderin.jpg" style="width: 100%"/>  
                </div> 
           
        </div>
    </body>
    <script src="resources/js/jquery-1.12.3.js"></script>
    <script src="resources/js/jquery.dataTables.min.js"></script>
    <script src="resources/js/dataTables.buttons.min.js"></script>
    <script src="resources/js/buttons.flash.min.js"></script>
    <script src="resources/js/jszip.min.js"></script>
    <script src="resources/js/pdfmake.min.js"></script>
    <script src="resources/js/vfs_fonts.js"></script>
    <script src="resources/js/buttons.html5.min.js"></script>
    <script src="resources/js/buttons.print.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/dataTables.bootstrap.min.js"></script>
    <script src="resources/js/bootstrap-datepicker.js"></script>
    <script src="resources/js/jquery.ui.widget.js"></script>
    <script src="resources/js/jquery.iframe-transport.js"></script>
    <script src="resources/js/bootstrap-confirmation.js"></script>
    <script src="resources/js/jquery.fileupload.js"></script>
    <script src="resources/js/jquery.validate.min.js"></script>
    <script src="resources/js/tooltipster.bundle.js"></script>
    <script src="resources/js/custom-functionalities.js"></script>
    <script>
        function lockedModal(){
            
            $('#formRecuperacion').modal({
                backdrop: 'static',
                keyboard: false
            });            
        }
    </script>
</html>    
