/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ModuleDto.java
 * Created on: 2016/12/13, 11:24:22 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

import java.util.Date;

/**
 * Carga los datos de los modulos
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
public class ModuleDto {

    private String id;
    private String programName;
    private String loadId;
    private Integer programCode;
    private String moduleCode;
    private String intitutionHqCode;
    private String moduleName;
    private String deparmet;
    private String municipality;
    private int totalHours;
    private Date startDate;
    private Date endDate;
    private String schedule;
    private double enrollmentCost;
    private double otherCost;
    private String trainingType;
    private String institution;
    private String percentageRequire;
    private String codigo_ccf;

    public ModuleDto() {
//
//        startDate = null;
//        endDate = null;
    }

    public String getId() {
        return id;
    }

    public void setPercentageRequire(String percentageRequire) {
        this.percentageRequire = percentageRequire;
    }

    public String getPercentageRequire() {
        return percentageRequire;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public void setTrainingType(String trainingType) {
        this.trainingType = trainingType;
    }

    public String getTrainingType() {
        return trainingType;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getProgramName() {
        return programName;
    }

    public String getLoadId() {
        return loadId;
    }

    public void setCodigo_ccf(String codigo_ccf) {
        this.codigo_ccf = codigo_ccf;
    }

    public String getCodigo_ccf() {
        return codigo_ccf;
    }

    public void setLoadId(String loadId) {
        this.loadId = loadId;
    }

    public void setProgramCode(Integer programCode) {
        this.programCode = programCode;
    }

    public void setModuleCode(String moduleCode) {
        this.moduleCode = moduleCode;
    }

    public void setIntitutionHqCode(String intitutionHqCode) {
        this.intitutionHqCode = intitutionHqCode;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public void setDeparmet(String deparmet) {
        this.deparmet = deparmet;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public void setTotalHours(int totalHours) {
        this.totalHours = totalHours;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public void setEnrollmentCost(double enrollmentCost) {
        this.enrollmentCost = enrollmentCost;
    }

    public void setOtherCost(double otherCost) {
        this.otherCost = otherCost;
    }

    public Integer getProgramCode() {
        return programCode;
    }

    public String getModuleCode() {
        return moduleCode;
    }

    public String getIntitutionHqCode() {
        return intitutionHqCode;
    }

    public String getModuleName() {
        return moduleName;
    }

    public String getDeparmet() {
        return deparmet;
    }

    public String getMunicipality() {
        return municipality;
    }

    public int getTotalHours() {
        return totalHours;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public String getSchedule() {
        return schedule;
    }

    public double getEnrollmentCost() {
        return enrollmentCost;
    }

    public double getOtherCost() {
        return otherCost;
    }

}
