/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: TrainingResourcesDao.java
 * Created on: 2017/04/11, 02:44:51 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dao;

import com.gml.simpc.api.dto.TrainingDto;
import java.util.List;

/**
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
public interface TrainingResourcesDao {
    
    
    
    /**
     * Obtiene los datos de  costos por CCF
     *
     * @return
     */
    List<TrainingDto> getCostsByCcf(String ccf,String year,String month);
    /**
     * Obtiene los datos de  tab periodo
     *
     * @return
     */
   List<TrainingDto> getCostsByPeriod(String ccf, String year,
        String month);
   

    
}
