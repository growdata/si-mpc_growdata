/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: PilaDtoRowMapper.java
 * Created on: 2017/02/13, 11:30:02 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.dao.mapper;

import com.gml.simpc.api.dto.ProgramDto;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 * Row Mapper para PILA.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public class ProgramDtoRowMapper implements RowMapper<ProgramDto> {
    @Override
    public ProgramDto mapRow(ResultSet rs, int i) throws SQLException {
       ProgramDto e = new ProgramDto();
       e.setCargaId(rs.getLong("CARGA_ID"));
       e.setProgramCode(rs.getString("CODIGO_PROGRAMA"));
       e.setProgramName(rs.getString("NOMBRE_PROGRAMA"));
       e.setProgramType(rs.getString("TIPO_PROGRAMA"));
       e.setCourseType(rs.getString("TIPO_CURSO"));
       e.setCcfCode(rs.getString("CODIGO_CCF"));
       e.setInstCode(rs.getString("CODIGO_INST"));
       e.setHeadqCode(rs.getString("CODIGO_SEDE"));
       e.setStateCode(rs.getString("CODIGO_DEPARTAMENTO"));
       e.setCityCode(rs.getString("CODIGO_MUNICIPIO"));
       e.setCertified(rs.getString("CERTIFICADA"));
       e.setCertName(rs.getString("NOMBRE_CERTIFICACION"));
       e.setOtherCertName(rs.getString("OTRO_NOMBRE_CERT"));
       e.setIssuedCertName(rs.getString("CERTIFICACION_EMITIDA"));
       e.setCine(rs.getString("CINE"));
       e.setNecesity(rs.getString("NECESIDAD"));
       e.setModuleCode(rs.getString("CODIGO_MODULO"));
       e.setModuleName(rs.getString("NOMBRE_MODULO"));
       e.setTotalHours(rs.getString("DURACION"));
       e.setSchedule(rs.getString("TIPO_HORARIO"));
       e.setEnrollmentCostType(rs.getString("TIPO_COSTO_MATRICULA"));
       e.setEnrollmentCost(rs.getString("COSTO_MATRICULA"));
       e.setOtherCostType(rs.getString("TIPO_OTROS_COSTOS"));
       e.setOtherCost(rs.getString("OTROS_COSTOS"));
       e.setEndDate(rs.getString("FECHA_INICIO"));
       e.setStartDate(rs.getString("FECHA_FIN"));
       e.setPerformance(rs.getString("AREA_DESEMPENO"));
        return e;
    }
}
