/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: VelocityExecutor.java
 * Created on: 2016/12/12, 11:35:19 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.commons.storage.velocity;

import com.gml.simpc.api.entity.exeption.SystemException;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_015_TEMPLATE_FAIL;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.PostConstruct;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.RuntimeSingleton;
import org.apache.velocity.runtime.parser.node.SimpleNode;
import org.springframework.stereotype.Service;

/**
 * Ejecuta y procesa las plantillas Velocity.
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
@Service
public final class VelocityExecutor {

    /**
     * Logger de la clase.
     */
    private static final Logger LOGGER = 
        Logger.getLogger(VelocityExecutor.class);
    /**
     * Engine de Velocity.
     */
    private VelocityEngine ve;

    /**
     * Constructor privado para impedir la creaci&oacute;n de instancias.
     */
    private VelocityExecutor() {

    }

    @PostConstruct
    public void init() {
        // Creamos una instancia del motor y la iniciamos.
        this.ve = new VelocityEngine();
        this.ve.init();
    }

    /**
     * Aplica una plantilla Velocity.
     *
     * @param template
     * @param params
     *
     * @return
     */
    public static String applyTemplate(String template,
        Map<String, Object> params) {

        if (template == null || params == null) {
            throw new IllegalArgumentException("Parámetros nulos.");
        }

        try {
            // Creamos una instancia del contexto.
            VelocityContext context = new VelocityContext();

            for (Entry<String, Object> key : params.entrySet()) {
                context.put(key.getKey(), key.getValue());
            }
            //creamos la plantilla
            RuntimeServices runtimeServices = RuntimeSingleton.
                getRuntimeServices();
            StringReader reader = new StringReader(scapeCharacters(template));
            SimpleNode node = runtimeServices.parse(reader, "Template name");
            Template thetemplate = new Template();
            thetemplate.setRuntimeServices(runtimeServices);
            thetemplate.setData(node);
            thetemplate.initDocument();
            // Aplicamos la plantilla a nuestro destinatario.
            StringWriter writer = new StringWriter();
            thetemplate.merge(context, writer);
            return originalText(writer.toString());
        } catch (Exception ex) {
            LOGGER.error("Error", ex);
            
            throw new SystemException(ERR_015_TEMPLATE_FAIL);
        }
    }

    /**
     * Escapa el & para no confundir a velocity.
     *
     * @param text
     *
     * @return
     */
    private static String scapeCharacters(String text) {
        String finalText = text.replace("&", "#[[&]]#");

        return finalText;
    }

    /**
     * Recupera el & escapado.
     *
     * @param text
     *
     * @return
     */
    private static String originalText(String text) {
        String finalText = text.replace("#[[&]]#", "&");

        return finalText;
    }
}
