/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: FileUpload.java
 * Created on: 2016/12/8, 16:06:20 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.loader;

import org.springframework.web.multipart.MultipartFile;


/**
 * Definici&ocute;n de modelo para carga de archivo
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
public class FileUpload {
    MultipartFile file;
    
    
    public MultipartFile getValue() {
        return file;
    }

    public void setValue(MultipartFile file) {
        this.file = file;
    } 
}
