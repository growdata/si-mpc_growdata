
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserRepository.java
 * Created on: 2016/10/19, 02:03:51 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository;

import com.gml.simpc.api.entity.LoadProcess;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Repositorio para procesos de carga
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public interface LoadProcessRepository extends JpaRepository<LoadProcess, Long> {

    /**
     * Busca las cargas realizadas en un rango de fechas , tipo de archivo
     * cargado y ccf
     */
    @Query(value =
        "select * from WF_CARGA_PROCESO wf INNER JOIN usuarios u on u.id=wf.consultor " +
        " where TIPO_CARGA like ?3 AND  FECHA_INICIO BETWEEN ?1 and ?2  " +
        "AND u.CAJA_COMPENSACION LIKE ?4 ", nativeQuery = true)
    List<LoadProcess> findByDateAndFile(Date start, Date end, String fileId,
        String ccf);

    /**
     * Busca las cargas realizadas en un Perido mes/anio d
     */
    @Query(value =
        "SELECT * FROM wf_carga_proceso wf INNER JOIN usuarios u on u.id=wf.consultor " +
        "WHERE EXTRACT(Year from FECHA_INICIO)= ?1 AND  EXTRACT(MONTH FROM fecha_inicio) " +
        "like ?2  AND tipo_carga LIKE ?3  AND u.CAJA_COMPENSACION LIKE ?4 ",
        nativeQuery = true)
    List<LoadProcess> findByperiodAndFile(int year, String month, String file,
        String consultant);

    /**
     * Busca las primera carga en estado estructura_validada
     */
    @Query(value = "SELECT *  FROM (SELECT * FROM WF_CARGA_PROCESO WHERE " +
        " estado = 'ESTRUCTURA_VALIDADA' ORDER BY FECHA_INICIO ) " +
        " WHERE ROWNUM = 1", nativeQuery = true)
    LoadProcess findFirstValidated();

    /**
     * Busca un cargue exitoso en el d�a con la misma cabecera.
     *
     * @param header
     *
     * @return
     */
    @Query(nativeQuery = true, value = "SELECT * FROM WF_CARGA_PROCESO " +
        "WHERE fecha_inicio > (TRUNC(SYSDATE) - INTERVAL '1' SECOND) " +
        "AND cabecera = ?1")
    LoadProcess findByHeader(String header);
}
