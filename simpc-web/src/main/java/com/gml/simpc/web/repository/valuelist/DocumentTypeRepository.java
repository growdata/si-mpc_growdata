/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: DocumentTypeRepository.java
 * Created on: 2017/01/11, 03:59:37 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.repository.valuelist;

import com.gml.simpc.api.entity.valuelist.DocumentType;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author <a href="mailto:jonathanp@gmlsoftware.com">Jonathan Pont�n</a>
 */
public interface DocumentTypeRepository 
    extends JpaRepository<DocumentType, String> {

}
