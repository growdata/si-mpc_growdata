/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ActionLogService.java
 * Created on: 2016/11/15, 10:46:55 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.dto.ConsolidantPostulantDto;
import java.util.List;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
public interface EconomicBenefitsService {

    /**
     * Consulta Consolidada Postulantes
     * 
     * @param idCcf
     * @param year
     * @param month
     * @return
     * 
     */
    List<ConsolidantPostulantDto> getConsolidatedPostulants(String idCcf,
        String year, String month);

    /**
     * Consulta Detallada Postulantes
     * 
     * @param idCcf
     * @param year
     * @param month
     * @return 
     */
    List<ConsolidantPostulantDto> getDetailedPostulants(String idCcf,
        String year, String month);

    /**
     * Consulta Consolidada Beneficiarios
     * 
     * @param idCcf
     * @param year
     * @param month
     * @return 
     */
    List<ConsolidantPostulantDto> getConsolidatedBenefits(String idCcf,
        String year, String month);

    /**
     * Consulta Detallada Beneficiarios
     * 
     * @param idCcf
     * @param year
     * @param month
     * @return 
     */
    List<ConsolidantPostulantDto> getDetailedBenefits(String idCcf,
        String year, String month);

}
