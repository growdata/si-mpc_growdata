/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: TemplateDto.java
 * Created on: 2016/12/16, 10:00:35 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.dto;

import com.gml.simpc.api.entity.Headquarter;
import com.gml.simpc.api.entity.Institution;
import java.util.List;

/**
 * Transfiere la informacion de consultor, institucion para ser enviada por
 * correo
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
public class TemplateDto {

    private final String tipoInstitucion;
    private final String nitInstitucion;
    private final Integer digitoVerificacion;
    private final String nombreInstitucion;
    private final String origenInstitucion;
    private final String estadoInstitucion;
    private final String naturalezaLegal;
    private final String fechaExpiracionCertificado;
    private final String telefonoInstitucion;
    private final String emailInstitucion;
    private final List<Headquarter> conjuntoSedes;
    private final String idConsultor;
    private final String nombreConsultor;
    private final String apellidoConsultor;
    private final String estadoConsultor;
    private final String telefonoConsultor;
    private final String emailConsultor;
    private final String nombreCcf;
    private final String nitCcf;
    private final String causaRechazo;
    private final String idCcf;

    /**
     * Constructor de creacion del objeto de informacion para
     * mostrar en la plantilla de aprobacion_instituciones.
     *
     * @param i
     * @param causal
     * @param conjuntoSedes
     */
    public TemplateDto(Institution i, String causal,
        List<Headquarter> conjuntoSedes) {
        this.tipoInstitucion = i.getType().getName();
        this.nitInstitucion = i.getNit();
        this.digitoVerificacion = i.getDv();
        this.nombreInstitucion = i.getInstitutionName();
        this.origenInstitucion = i.getOrigin().name();
        this.estadoInstitucion = i.getStatus().name();
        this.naturalezaLegal = i.getLegalNature().name();
        this.fechaExpiracionCertificado =
            i.getQualityCertificateExpiration() == null ? "" :
                i.getQualityCertificateExpiration().toString();
        this.telefonoInstitucion = i.getContactPhone();
        this.emailInstitucion = i.getContactEmail();
        this.conjuntoSedes = conjuntoSedes;
        this.idConsultor = i.getModificationUserId().getIdentificationNumber();
        this.nombreConsultor = i.getModificationUserId().getFirstName();
        this.apellidoConsultor = i.getModificationUserId().getFirstSurname();
        this.estadoConsultor = i.getModificationUserId().getStatus().name();
        this.telefonoConsultor = i.getModificationUserId().getPhoneNumber();
        this.emailConsultor = i.getModificationUserId().getEmail();
        this.nombreCcf = i.getModificationUserId().getCcf().getName();
        this.nitCcf = i.getModificationUserId().getCcf().getNit();
        this.causaRechazo = causal;
        this.idCcf = i.getModificationUserId().getCcf().getCode();
    }

    public String getTipoInstitucion() {
        return tipoInstitucion;
    }

    public String getNitInstitucion() {
        return nitInstitucion;
    }

    public Integer getDigitoVerificacion() {
        return digitoVerificacion;
    }

    public String getNombreInstitucion() {
        return nombreInstitucion;
    }

    public String getOrigenInstitucion() {
        return origenInstitucion;
    }

    public String getEstadoInstitucion() {
        return estadoInstitucion;
    }

    public String getNaturalezaLegal() {
        return naturalezaLegal;
    }

    public String getFechaExpiracionCertificado() {
        return fechaExpiracionCertificado;
    }

    public String getTelefonoInstitucion() {
        return telefonoInstitucion;
    }

    public String getEmailInstitucion() {
        return emailInstitucion;
    }

    public List<Headquarter> getConjuntoSedes() {
        return conjuntoSedes;
    }

    public String getIdConsultor() {
        return idConsultor;
    }

    public String getNombreConsultor() {
        return nombreConsultor;
    }

    public String getApellidoConsultor() {
        return apellidoConsultor;
    }

    public String getEstadoConsultor() {
        return estadoConsultor;
    }

    public String getTelefonoConsultor() {
        return telefonoConsultor;
    }

    public String getEmailConsultor() {
        return emailConsultor;
    }

    public String getNombreCcf() {
        return nombreCcf;
    }

    public String getNitCcf() {
        return nitCcf;
    }

    public String getCausaRechazo() {
        return causaRechazo;
    }

    public String getIdCcf() {
        return idCcf;
    }
}
