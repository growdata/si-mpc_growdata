
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserController.java
 * Created on: 2016/10/19, 02:14:58 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gml.simpc.api.dto.ManagementAndPlacementDto;
import com.gml.simpc.api.entity.IndicatorGraphic;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_001_UNKNOW;
import com.gml.simpc.api.enums.GraphicType;
import com.gml.simpc.api.services.ManagementAndPlacementService;
import com.gml.simpc.web.controller.ControllerDefinition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controlador para la ventana de manejo de portal de beneficios economicos
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Controller
public class ManagementAndPlacementPortalController 
    implements ControllerDefinition {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER
            = Logger.getLogger(ManagementAndPlacementPortalController.class);

    /**
     * Anio 2014, se usa para los datos mostrados en los filtros de la pagina.
     */
    public static final int YEAR2014 = 2014;

    /**
     * servicio de Beneficios economicos.
     */
    @Autowired
    private ManagementAndPlacementService managementAndPlacementService;

    /**
     * Inicializa el binder para formatear fechas.
     *
     * @param binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class,
                new CustomDateEditor(sdf, true));
    }

    /**
     * M&eacute;todo para pintar portal de beneficios economicos.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "portal-gestion-y-colocacion",
            method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
            HttpServletResponse response) {
        LOGGER.info(
                "Ejecutando metodo [ onDisplay portal-gestion-y-colocacion ]");
        ModelAndView modelAndView
                = new ModelAndView("portal-gestion-y-colocacion");
        try {

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("Finaliza la invocacion de [ onDisplay ].");
        }

        return modelAndView;
    }

    /**
     * M&eacute;todo para buscar registros de gesti&oacute;n y colocaci&oacute;n.
     * 
     * @param request
     * @param response
     * @param startDate
     * @param endDate
     * @return 
     */
    @RequestMapping(value = "buscarGestionYColocacion",
            method = RequestMethod.GET)
    public ModelAndView search(HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(name = "initDate") Date startDate, @RequestParam(name
                    = "finishDate") Date endDate) {
        LOGGER.info(
                "entra a search en el controller de ManagementAndPlacement"
                + "PortalController");
        ModelAndView modelAndView
                = new ModelAndView("portal-gestion-y-colocacion");
        String idStatusPostulation = null;

        try {

            String initDateSel = request.getParameter("initDate");
            String endDateSel = request.getParameter("finishDate");
            String statusPostulation = request.getParameter("statusPostulation");

            if (request.getParameter("statusPostulation") != null && 
                !"".equals(request.getParameter("statusPostulation"))) {
                idStatusPostulation = request.getParameter("statusPostulation");
            }
            
            LOGGER.info(" fecha inicio " + startDate);
            LOGGER.info(" fecha fin " + endDate);
            LOGGER.info(" estado postulacion " + idStatusPostulation);
            List<ManagementAndPlacementDto> listaHombresYMujeresInter = 
                this.managementAndPlacementService.
                    findManagementAndPlacementResults(startDate, endDate,
                            idStatusPostulation);

            modelAndView.addObject("initDateSelected", initDateSel);
            modelAndView.addObject("endDateSelected", endDateSel);
            modelAndView.addObject("statusPostSelected", statusPostulation);
            modelAndView.addObject("resultList", listaHombresYMujeresInter);
            ObjectMapper mapper = new ObjectMapper();
            modelAndView.addObject("graphicData",
                    mapper.writeValueAsString(listaHombresYMujeresInter));
            IndicatorGraphic graphic = new IndicatorGraphic();
            graphic.setType(GraphicType.B);
            modelAndView.addObject("graphic", graphic);

            String xKey = "postulationStatus";
            List<String> yKeys = new ArrayList();
            List<String> labels = new ArrayList();
            List<String> tableg = new ArrayList();
            List<String> barColors = new ArrayList();

            yKeys.add("women");
            yKeys.add("men");
            labels.add("Mujeres");
            labels.add("Hombres");
            barColors.add("pink");
             barColors.add("blue");

            tableg.add("ESTADO DE POSTULACION");
            tableg.add("CANTIDAD MUJERES");
            tableg.add("CANTIDAD DE HOMBRES");

           
            modelAndView.addObject("graphicList", listaHombresYMujeresInter);
            modelAndView.addObject("xKey", xKey);
            modelAndView.addObject("yKeys", yKeys);
            modelAndView.addObject("labels", labels);
            modelAndView.addObject("titleList", tableg);
            modelAndView.addObject("barColors", barColors);
            modelAndView.addObject("titulografica", "CANTIDAD DE POSTULANTES POR ESTADO");

            modelAndView.addObject("titulo",
                "ESTADO DE POSTULANTE");
            modelAndView.addObject("activateBtn", true);

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info(
                "Finaliza la invocacion de [ buscarGestionYColocacion ].");
        }

        return modelAndView;
    }

}
