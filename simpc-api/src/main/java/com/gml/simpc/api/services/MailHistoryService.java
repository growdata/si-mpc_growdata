/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: MailHistoryService.java
 * Created on: 2017/01/13, 02:59:04 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.services;

import com.gml.simpc.api.entity.MailHistory;

/**
 * Interface que define el servicio de historial de email.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
public interface MailHistoryService {

    /**
     * Guarda un mail al historico.
     *
     * @param mailHistory
     */
    void save(MailHistory mailHistory);
}
