/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: TrainingPreparedStatementSetter.java
 * Created on: 2017/01/20, 12:16:58 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.loader.writer;

import com.gml.simpc.api.dto.InformalEducationDto;
import com.gml.simpc.loader.dto.JobResultDto;
import com.gml.simpc.loader.thread.ThreadLocalJob;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.springframework.batch.item.database.ItemPreparedStatementSetter;

/**
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica Jim&eacute;nez Rozo</a>
 */
public class InformalEducationPreparedStatementSetter 
        implements ItemPreparedStatementSetter<InformalEducationDto> {
 
    @Override
    public void setValues(InformalEducationDto informalEducationDto, 
        PreparedStatement ps) 
            throws SQLException {
        ThreadLocalJob.setProcesed();
        JobResultDto jobResult = ThreadLocalJob.getResult();
        int line = jobResult.getProcesedLines();
        ps.setString(1, informalEducationDto.getDocumentType());
        ps.setString(2, informalEducationDto.getNumberIdentification());
        ps.setString(3, informalEducationDto.getTrainingType());
        ps.setString(4, informalEducationDto.getInstitution());
        ps.setString(5, informalEducationDto.getStatus());
        ps.setString(6, informalEducationDto.getCertificationDate());
        ps.setString(7, informalEducationDto.getProgramName());
        ps.setString(8, informalEducationDto.getCountry());
        ps.setString(9, informalEducationDto.getHours());
        ps.setString(10, Integer.toString(line));
    }
 
}
