/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ActionLogServiceImpl.java
 * Created on: 2016/11/15, 10:50:35 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.service.impl;

import com.gml.simpc.api.entity.ActionLog;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.services.ActionLogService;
import com.gml.simpc.api.utilities.Util;
import com.gml.simpc.web.repository.ActionLogRepository;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Service(value = "logActionService")
public class ActionLogServiceImpl implements ActionLogService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(
            ActionLogServiceImpl.class);
    /**
     * Repositorio para log de acciones.
     */
    @Autowired
    private ActionLogRepository actionLogRepository;

    /**
     * M&ecute;todo de consulta para generar el log del sistema.
     * 
     * @return 
     */
    @Override
    public List<ActionLog> getAll() {
        LOGGER.info("Ejecutando metodo [ getAll ] de ActionLogServiceImpl");
        return this.actionLogRepository.findAll();
    }

    /**
     * M&ecute;todo que guarda el log del sistema.
     * 
     * @param actionLog 
     */
    @Override
    public void save(ActionLog actionLog) {
        LOGGER.info("Ejecutando metodo [ save ] de ActionLogServiceImpl");
        this.actionLogRepository.save(actionLog);
    }

    /**
     * M&ecute;etodo que actuliza el log del sistema.
     * 
     * @param actionLog 
     */
    @Override
    public void update(ActionLog actionLog) {
        LOGGER.info("Ejecutando metodo [ update ] de ActionLogServiceImpl");
        this.actionLogRepository.saveAndFlush(actionLog);
    }

    /**
     * M&ecute;todo que elimina cualquier log del sistema.
     * 
     * @param actionLog 
     */
    @Override
    public void remove(ActionLog actionLog) {
        LOGGER.info("Ejecutando metodo [ remove ] de ActionLogServiceImpl");
        this.actionLogRepository.delete(actionLog);
    }

    /**
     * M&ecute;todo que devuelve un log del sistema.
     * 
     * @return 
     */
    public ActionLogRepository getActionLogRepository() {
        return actionLogRepository;
    }

    /**
     * M&ecute;todo que setea un log del sistema.
     * 
     * @param actionLogRepository 
     */
    public void setActionLogRepository(ActionLogRepository actionLogRepository) {
        this.actionLogRepository = actionLogRepository;
    }

    /**
     * M&ecute;todo que busca un log del sistema segun el usuarios y un periodo
     * de fechas.
     * 
     * @param user
     * @param initDate
     * @param finishDate
     * @return 
     */
    @Override
    public List<ActionLog> getActionsByFilters(User user, Date initDate,
            Date finishDate) {

        LOGGER.info("Ejecutando metodo [ getActionsByFilters ] in "
                + "ActionLogServiceImpl");

        LOGGER.info(" las fechas son " + initDate + "," + finishDate);
        if (initDate == null && finishDate == null) {
            return this.actionLogRepository.getActionsLog(user.
                    getIdentificationNumber());
        } else {
            return this.actionLogRepository.getActionsLog(
                    Util.nullValue(user.getIdentificationNumber()), initDate,
                    finishDate);
        }
    }

}
