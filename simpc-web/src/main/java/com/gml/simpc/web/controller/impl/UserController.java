/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserController.java
 * Created on: 2016/10/19, 02:14:58 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.controller.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gml.simpc.api.dto.PasswordChangeDto;
import com.gml.simpc.api.entity.ActionLog;
import com.gml.simpc.api.entity.HistoryAccess;
import com.gml.simpc.api.entity.Profile;
import com.gml.simpc.api.entity.Session;
import com.gml.simpc.api.entity.User;
import com.gml.simpc.api.entity.exeption.SystemException;
import com.gml.simpc.api.entity.exeption.UserException;
import com.gml.simpc.api.entity.exeption.code.ErrorCode;
import com.gml.simpc.api.entity.valuelist.Foundation;
import com.gml.simpc.api.enums.LinkReason;
import com.gml.simpc.api.enums.UserFields;
import com.gml.simpc.api.enums.UserStatus;
import com.gml.simpc.api.services.ActionLogService;
import com.gml.simpc.api.services.ActivationService;
import com.gml.simpc.api.services.HistoryAccessService;
import com.gml.simpc.api.services.ProfileService;
import com.gml.simpc.api.services.UserService;
import com.gml.simpc.api.utilities.Util;
import com.gml.simpc.commons.annotations.AnnotationInterceptor;
import com.gml.simpc.commons.annotations.FunctionalityInterceptor;
import com.gml.simpc.commons.captcha.VerifyRecaptcha;
import com.gml.simpc.commons.mail.sender.Notificator;
import com.gml.simpc.web.controller.ControllerDefinition;
import com.gml.simpc.web.utilities.LDAPOperations;
import com.gml.simpc.web.utilities.ValueListService;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_001_UNKNOW;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_006_USER_DONT_EXIST;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_016_PASSWORD_DIFERENT_TO_CONFIRMATION;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_018_PASSWORD_MUST_BE_DIFERENT;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_032_ACTIVE_DIRECTORY_USED;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_033_LDAP_AUTHENTICATION_FAILED;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_042_ERROR_INVALID_PASSWORD;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_046_ERROR_PROGRAM_NAME_EXIST;
import static com.gml.simpc.api.entity.exeption.code.ErrorCode.ERR_056_ERROR_ACTIVATE_MINTRABUSER;
import static com.gml.simpc.api.utilities.Util.FAILED_ALERT;
import static com.gml.simpc.api.utilities.Util.PARAMETER_USER_MESSAGE;
import static com.gml.simpc.api.utilities.Util.SUCCESS_ALERT;
import static com.gml.simpc.api.utilities.Util.nullValue;

/**
 * Controlador para la ventana de maestro de usuario.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@FunctionalityInterceptor(name = {
    "consulta-usuarios.htm", "editInformationUser.htm"
})
@Controller
public class UserController implements ControllerDefinition {

    /**
     * Registro de log en la aplicaci&ocute;n.
     */
    private static final Logger LOGGER = Logger.getLogger(UserController.class);
    /**
     * Mensaje para cambio de contrase�a exitoso.
     */
    public static final String PASS_CHANGE_SUCCESS_MESSAGE =
        "Se ha cambiado la contrase�a exitosamente";

    /**
     * Mensaje para cambio datos b&aacute;sicos de usuario.
     */
    public static final String USER_UPDATE_SUCCESS_MESSAGE =
        "Se han modificado los datos exitosamente";
    /**
     * Identificador del usuario en sesi&oacute;n.
     */
    public static final String SESSION_USER = "session_user";
    /**
     * Servicio para adminisraci&oacute;n de usuarios; Servicio de usuario
     * intermediario para la comunicaci&ocute;n con la persistencia.
     */
    @Autowired
    private UserService userService;

    /**
     * Servicio para listas de valores.
     */
    @Autowired
    private ValueListService valueListService;
    /**
     * Servicio de activaci&oacute;n.
     */
    @Autowired
    private ActivationService activationService;

    /**
     * Servicio para adminisraci&oacute;n de <code>Profile</code> Servicio de
     * <code>Profile</code> intermediario para la comunicaci&ocute;n con la
     * persistencia.
     */
    @Autowired
    private ProfileService profileService;

    /**
     * Servicio para adminisraci&oacute;n de historicos de accesos.
     */
    @Autowired
    private HistoryAccessService historyAccessService;
    /**
     * Servicio para adminisraci&oacute;n de historicos de acciones.
     */
    @Autowired
    private ActionLogService actionLogService;
    /**
     * Servicio para consulta LDAP.
     */
    @Autowired
    private LDAPOperations ldapOperationsService;
    /**
     * Servicio de notificaci&oacute;n.
     */
    @Autowired
    private Notificator notificator;
    /**
     * Servicio para verificaci&oacute;n de Captcha.
     */
    @Autowired
    private VerifyRecaptcha verifyRecaptcha;

    /**
     * M&eacute;todo para obtener un usuario.
     * 
     * @param request
     * @param response
     * @return
     * @throws UserException 
     */
    @RequestMapping(value = "obtener-usuario", method = RequestMethod.GET)
    public @ResponseBody
    String onEditUser(HttpServletRequest request,
        HttpServletResponse response) throws UserException {
        LOGGER.info("Ejecutando metodo [ onEditUser ]");

        try {
            User user = this.userService.
                findById(Long.parseLong(request.getParameter("id")));
            user.setTransCityId(user.getCity().getId());
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(user);
        } catch (Exception ex) {
            LOGGER.error("Error", ex);

            throw new UserException(ERR_001_UNKNOW);
        }
    }

    /**
     * M&eacute;todo que se ejecuta al abrir la ventana de index; Trae listas
     * necesarias para el funcionamiento de la ventana.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "consulta-usuarios",
        method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response) {
        LOGGER.info("Ejecutando metodo [ onDisplay UserController ]");
        ModelAndView modelAndView =
            new ModelAndView("consulta-usuarios");
        boolean sw = false;

        try {

            modelAndView.addObject("usersList", this.userService.getAll());
            if (isUserOfMinistryOfLabor(request)) {
                modelAndView.addObject("userMinistryOfLabor", 1);
            }
            if (request.getParameter("id") != null) {
                modelAndView.addObject("creationUser", new User());

                sw = true;
            }

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("Finaliza la invocacion de [ onDisplay ].");
        }

        init(modelAndView, sw);

        return modelAndView;
    }

    /**
     * M&eacute;todo para editar la informaci&oacute;n de un usuario.
     * 
     * @param request
     * @param response
     * @return 
     */
    @RequestMapping(value = "editInformationUser",
        method = RequestMethod.GET)
    public ModelAndView onDisplayEditUser(HttpServletRequest request,
        HttpServletResponse response) {
        LOGGER.info("Ejecutando metodo [ onDisplayEditUser UserController ]");
        ModelAndView modelAndView =
            new ModelAndView("editarInformacionBasicaUsuario");
        initEdit(modelAndView, request);

        Session session = (Session) request.getSession().getAttribute(
            SESSION_USER);
        User user = session.getUser();

        boolean MintrabUser = "2".equals(user.getCcf().getCode());

        modelAndView.addObject("mintrabUser", MintrabUser);
        modelAndView.addObject("userInfo",
            this.userService.findById(user.getId()));
        modelAndView.addObject("stateList",
            this.valueListService.getStateList());
        modelAndView.addObject("citiesList",
            this.valueListService.getCityList());

        return modelAndView;
    }

    /**
     * m&eacute;todo que consulta usuarios de acuerdo a lo diligenciado en los
     * filtros de formulario de consulta.
     *
     * @param request
     * @param response
     * @param searchUser
     *
     * @return
     */
    @RequestMapping(value = "buscarUsuarios",
        method = RequestMethod.GET)
    public ModelAndView search(HttpServletRequest request,
        HttpServletResponse response,
        @ModelAttribute("searchUser") User searchUser) {
        LOGGER.info("entra a consultar en el controller de usuarios");

        ModelAndView modelAndView =
            new ModelAndView("consulta-usuarios");

        try {
            List<User> usersList = this.userService.
                findByFilters(searchUser);
            modelAndView.addObject("usersList", usersList);
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            init(modelAndView, false);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("Finaliza la invocacion de [ buscarUsuarios].");
        }

        init(modelAndView, false);

        return modelAndView;
    }

    /**
     * Enviar solicitud de constrase�a.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "sendPassword",
        method = RequestMethod.POST)
    public ModelAndView sendPassword(HttpServletRequest request,
        HttpServletResponse response) {
        ModelAndView modelAndView = onDisplay(request, response);

        try {
            String id = request.getParameter("passw");
            User user = this.userService.findById(Long.parseLong(id));
            LOGGER.info("****" + id);
            this.activationService.sendPasswordLink(user.getEmail(),
                LinkReason.CREATION);
            modelAndView.addObject("msg",
                "Se envi� confirmaci�n de contrase�a con " +
                "�xito a " + user.getEmail());
            modelAndView.addObject("msgType", SUCCESS_ALERT);
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
            modelAndView.addObject("msgType", FAILED_ALERT);
        } finally {
            LOGGER.info("finally sendPassword");
        }
        return modelAndView;
    }

    /**
     * M&eacute;todo que guarda un usuario.
     *
     * @param request
     * @param response
     * @param creationUser
     *
     * @return
     */
    @RequestMapping(value = "guardar-usuario",
        method = RequestMethod.POST)
    public ModelAndView save(HttpServletRequest request,
        HttpServletResponse response, @Valid @ModelAttribute(
            "creationUser"
        ) User creationUser) {

        ModelAndView modelAndView = new ModelAndView("consulta-usuarios");
        boolean creationError = false;
        boolean isNew = creationUser.getId() == null;

        if ("".equals(request.getParameter("id"))) {
            modelAndView.addObject("tipoformulario", "nuevo");
        } else {
            modelAndView.addObject("tipoformulario", "editar");
        }

        modelAndView.addObject("cityUser", creationUser.getCity().getId());
        LOGGER.info("GUARDAR USUARIO AQUI" + creationUser.getCity().getId());

        try {
            User oldUser = this.userService.findById(creationUser.getId());
            if (isNew) {
                creationUser.setStatus(UserStatus.T);
                creationUser.setCreationDate(new Date());
                Session session = (Session) request.getSession().getAttribute(
                    SESSION_USER);
                User user = session.getUser();
                creationUser.setCreationUserId(user);

                Foundation ccf = this.valueListService.getFoundations().
                    get(creationUser.getCcf().getCode());

                if (ccf.getNit().equals(Util.NIT_ENTIDAD_MINISTRY_OF_LABOR)) {
                    LOGGER.info("El usuario pertenece al ministerio.");

                    String username = creationUser.getEmail().split("@")[0];
                    creationUser.setUsername(username);
                }

                this.userService.save(creationUser);

                this.activationService.sendPasswordLink(creationUser.getEmail(),
                    LinkReason.CREATION);
            } else {

                if (request.getParameter("status") == null) {
                    creationUser.setStatus(UserStatus.T);
                }

                creationUser.setModificationDate(new Date());
                Session session = (Session) request.getSession().getAttribute(
                    SESSION_USER);
                User user = session.getUser();
                creationUser.setModificationUserId(user);
                creationUser.setCreationUserId(oldUser.getCreationUserId());
                creationUser.setUsername(oldUser.getUsername());
                creationUser.setCreationDate(oldUser.getCreationDate());
                creationUser.setPassword(oldUser.getPassword());
                creationUser.setLastPasswordChange(oldUser.
                    getLastPasswordChange());

                this.userService.save(creationUser);
                String message = getChanges(oldUser, creationUser);
                notificator.sendNotification(creationUser.getEmail(),
                    "Su cuenta ha sido modificada con exito.\n" + message,
                    creationUser.
                        getFirstName().concat(" ").
                        concat(creationUser.getFirstSurname()),
                    PARAMETER_USER_MESSAGE);
            }
            modelAndView.addObject("msg", "Se ha guardado el usuario " +
                creationUser.getFirstName() + " exitosamente.");
            modelAndView.addObject("msgType", SUCCESS_ALERT);
            modelAndView.addObject("usersList", this.userService.getAll());
        } catch (SystemException ex) {
            creationError = true;

            try {
                LOGGER.error("Error ", ex);
                modelAndView.addObject("internalMsg", ex.getMessage());
                ObjectMapper om = new ObjectMapper();
                modelAndView.addObject("jsonUser",
                    om.writeValueAsString(creationUser));
                modelAndView.addObject("msgType", FAILED_ALERT);
            } catch (IOException ex1) {
                LOGGER.error("Error", ex1);
                modelAndView.addObject("internalMsg", ERR_001_UNKNOW.
                    getMessage());
                modelAndView.addObject("msgType", FAILED_ALERT);
            }
        } catch (UserException ex) {
            try {
                creationError = true;
                LOGGER.error("Error ", ex);
                modelAndView.addObject("internalMsg", ex.getMessage());
                ObjectMapper om = new ObjectMapper();
                modelAndView.addObject("jsonUser",
                    om.writeValueAsString(creationUser));
                modelAndView.addObject("usersList", this.userService.getAll());
                modelAndView.addObject("msgType", FAILED_ALERT);
            } catch (IOException ex1) {
                creationError = true;
                LOGGER.error("Error", ex1);
                modelAndView.addObject("internalMsg", ERR_001_UNKNOW.
                    getMessage());
                modelAndView.addObject("msgType", FAILED_ALERT);
            }
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
            modelAndView.addObject("msgType", FAILED_ALERT);

        } finally {
            LOGGER.info("finally save in UserController");
        }

        init(modelAndView, creationError);

        return modelAndView;
    }

    /**
     * M&eacute;todo que compara campos para saber que se modifico.
     * 
     * @param oldUser
     * @param creationUser
     * @return 
     */
    private String getChanges(User oldUser, User creationUser) {
        LOGGER.info("INFO ENTRANTE" + creationUser.getCreationUserId() + " " +
            creationUser.getCreationDate() + " " + creationUser.
            getLastPasswordChange());

        StringBuilder message = new StringBuilder("Campos:\n");

        if (!(nullValue(oldUser.getAddress()).
            equals(creationUser.getAddress()))) {
            message.append(UserFields.DIRECCION);
            message.append(",\n");
        }
        if (!nullValue(oldUser.getCcf().getCode()).
            equals(creationUser.getCcf().getCode())) {
            message.append(UserFields.ENTIDAD);
            message.append(",\n");
        }
        if (!nullValue(oldUser.getFirstName()).
            equals(creationUser.getFirstName())) {
            message.append(UserFields.NOMBRE);
            message.append(",\n");
        }
        if (!nullValue(oldUser.getSecondName()).
            equals(creationUser.getSecondName())) {
            message.append(UserFields.SEGUNDO_NOMBRE);
            message.append(",\n");
        }
        if (!nullValue(oldUser.getFirstSurname()).
            equals(creationUser.getFirstSurname())) {
            message.append(UserFields.APELLIDO);
            message.append(",\n");
        }
        if (!nullValue(oldUser.getPhoneNumber()).
            equals(creationUser.getPhoneNumber())) {
            message.append(UserFields.TELEFONO);
            message.append(",\n");
        }
        if (!nullValue(oldUser.getEmail()).
            equals(creationUser.getEmail())) {
            message.append(UserFields.EMAIL);
            message.append(",\n");
        }
        if (!nullValue(oldUser.getUsername()).
            equals(creationUser.getUsername())) {
            message.append(UserFields.NOMBREUSUARIO);
            message.append(",\n");
        }
        if (!oldUser.getProfile().getId().
            equals(creationUser.getProfile().getId())) {
            message.append(UserFields.PERFIL);
            message.append(",\n");
        }
        if (!nullValue(oldUser.getStatus().getLabel()).
            equals(creationUser.getStatus().getLabel())) {
            message.append(UserFields.ESTADO);
            message.append(".\n");
        }

        return message.toString();
    }

    /**
     * M&eacute;todo que guarda un usuario.
     *
     * @param request
     * @param response
     * @param creationUser
     *
     * @return
     */
    @RequestMapping(value = "guardar-usuario-base",
        method = RequestMethod.POST)
    public ModelAndView saveBasicInfo(HttpServletRequest request,
        HttpServletResponse response, @Valid @ModelAttribute(
            "creationUser"
        ) User creationUser) {

        ModelAndView modelAndView =
            new ModelAndView("editarInformacionBasicaUsuario");
        modelAndView.addObject("mintrabUser",
            request.getParameter("mintrabUser"));
        modelAndView.addObject("documentTypes",
            this.valueListService.getDocumentTypeList());

        boolean creationError = false;

        try {
            creationUser.setModificationDate(new Date());
            Session session = (Session) request.getSession().getAttribute(
                SESSION_USER);
            User user = session.getUser();
            creationUser.setId(user.getId());
            creationUser.setModificationUserId(user);
            creationUser.setStatus(UserStatus.A);
            this.userService.updateUser(creationUser);
            modelAndView.addObject("msg", "Se ha guardado el usuario " +
                creationUser.getFirstName() + " exitosamente. " +
                " Cierre e inicie la sesi�n nuevamente para ver los cambios.");
            modelAndView.addObject("msgType", SUCCESS_ALERT);
            modelAndView.addObject("creationUser", creationUser);
            modelAndView.addObject("userInfo",
                this.userService.findById(user.getId()));
            creationError = true;
        } catch (SystemException ex) {
            creationError = true;

            try {
                LOGGER.error("Error ", ex);
                modelAndView.addObject("msg", ex.getMessage());
                ObjectMapper om = new ObjectMapper();
                modelAndView.addObject("jsonUser",
                    om.writeValueAsString(creationUser));
                modelAndView.addObject("msgType", FAILED_ALERT);
            } catch (IOException ex1) {
                LOGGER.error("Error", ex1);
                modelAndView.addObject("msg", ERR_001_UNKNOW.
                    getMessage());
                modelAndView.addObject("msgType", FAILED_ALERT);
            }
        } catch (UserException ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg", ex.getMessage());
            initEdit(modelAndView, request);
            modelAndView.addObject("msgType", FAILED_ALERT);
            creationError = true;
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
            modelAndView.addObject("msgType", FAILED_ALERT);
        } finally {
            LOGGER.info("finally saveBasicInfo in UserController");
        }

        modelAndView.addObject("editionPassword", new PasswordChangeDto());
        init(modelAndView, creationError);

        return modelAndView;
    }

    /**
     * M&eacute;todo para editar un usuario.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "edicion-usuario",
        method = RequestMethod.GET)
    public ModelAndView editUser(HttpServletRequest request,
        HttpServletResponse response) {

        ModelAndView modelAndView =
            new ModelAndView("edicion-usuario");
        boolean creationError = false;

        Long id = Long.parseLong(request.getParameter("id"));
        User parent = null;

        try {
            parent = this.userService.findById(id);

            ObjectMapper om = new ObjectMapper();
            modelAndView.addObject("jsonUser",
                om.writeValueAsString(parent));

            creationError = true;
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        }

        modelAndView.addObject("creationUser", parent);
        modelAndView.addObject("editionUser", parent);

        init(modelAndView, creationError);

        return modelAndView;
    }

    /**
     * M&eacute;todo que guarda una instituci&oacute;n.
     *
     * @param request
     * @param passwordChange
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "modificarClave",
        method = RequestMethod.POST)
    public ModelAndView recoverPassword(HttpServletRequest request,
        HttpServletResponse response,
        @ModelAttribute("editionPassword") PasswordChangeDto passwordChange) {
        ModelAndView modelAndViewError = new ModelAndView("activar-usuario");
        ModelAndView modelAndViewSuccesfull =
            new ModelAndView("login");

        try {
            boolean isOld = Boolean.valueOf(request.getParameter("older"));
            boolean mintrabUser = Boolean.valueOf(request.
                getParameter("mintrab"));
            modelAndViewError.addObject("mintrabUser", mintrabUser);

            if (isOld || request.getParameter("userName") != null ||
                passwordChange.getUserName() != null) {
                String userName = request.getParameter("userName") != null ?
                    request.getParameter("userName") : passwordChange.
                    getUserName();
                Long userId = Long.parseLong(request.getParameter("userId"));
                LOGGER.info(" el username que llega es " + userName);
                User user = userService.findById(userId);

                if (user.getLastPasswordChange() != null) {
                    modelAndViewError.addObject("isOldUser", true);
                }

                return validatePasswod(passwordChange, user, userName,
                    modelAndViewSuccesfull, modelAndViewError, userId,
                    mintrabUser, isOld);

            } else {
                modelAndViewError.addObject("msg",
                    ERR_006_USER_DONT_EXIST.getMessage());
                modelAndViewError.addObject("msgType", FAILED_ALERT);
                modelAndViewError.addObject("editionPassword",
                    new PasswordChangeDto());

                return modelAndViewError;
            }

        } catch (DataIntegrityViolationException ex) {
            Long userId = Long.parseLong(request.getParameter("userId"));
            User user = userService.findById(userId);
            LOGGER.error("Error ", ex);
            modelAndViewError.addObject("msg",
                ERR_046_ERROR_PROGRAM_NAME_EXIST.getMessage());
            modelAndViewError.addObject("msgType", FAILED_ALERT);
            modelAndViewError.addObject("user", user);
            modelAndViewError.addObject("editionPassword",
                new PasswordChangeDto());
        } catch (SystemException ex) {
            LOGGER.error("Error ", ex);

            modelAndViewError.addObject("msg", ex.getMessage());
            modelAndViewError.addObject("msgType", FAILED_ALERT);
            modelAndViewError.addObject("user", new User());
            modelAndViewError.addObject("editionPassword",
                new PasswordChangeDto());
        } catch (ConstraintViolationException ex) {
            LOGGER.error("Error ", ex);
            StringBuilder builder = new StringBuilder("Alertas: ");

            for (ConstraintViolation error : ex.getConstraintViolations()) {
                builder.append(error.getMessage());
                builder.append("\n");
            }
            modelAndViewError.addObject("msg", builder.toString());
            modelAndViewError.addObject("msgType", FAILED_ALERT);
            modelAndViewError.addObject("user", new User());
            modelAndViewError.addObject("editionPassword",
                new PasswordChangeDto());
        } catch (Exception e) {
            LOGGER.error("ERROR", e);
            modelAndViewError.addObject("msg", ERR_001_UNKNOW.getMessage());
            modelAndViewError.addObject("msgType", FAILED_ALERT);
            modelAndViewError.addObject("user", new User());
            modelAndViewError.addObject("editionPassword",
                new PasswordChangeDto());
        }

        return modelAndViewError;
    }

    /**
     * M&eacute;todo que valida si la contrase�a es v&aacute;lida.
     * 
     * @param password
     * @return 
     */
    private Boolean validPassword(String password) {
        Pattern p = Pattern.compile(Util.PASS_REG_EXP);
        Matcher m = p.matcher(password);
        return m.find();
    }

    /**
     * M&eacute;todo que actualiza una contrase�a.
     *
     * @param request
     * @param response
     * @param passwordChange
     *
     * @return
     */
    @RequestMapping(value = "cambiarContrasenia",
        method = RequestMethod.POST)
    public ModelAndView savePassword(HttpServletRequest request,
        HttpServletResponse response,
        @ModelAttribute("editionPassword") PasswordChangeDto passwordChange) {
        ModelAndView modelAndView = new ModelAndView(
            "editarInformacionBasicaUsuario");
        try {
            HttpSession session = request.getSession();
            Session sessionUser = (Session) session.getAttribute(SESSION_USER);

            boolean valPass1 = validatePass(passwordChange.getNewPassword());
            boolean valPass2 = validatePass(passwordChange.
                getConfirmationPassword());

            if (valPass1 && valPass2) {

                if (sessionUser.getUser() != null) {
                    modelAndView.addObject("editionUser", sessionUser.getUser());

                    if (passwordChange.getConfirmationPassword() != null &&
                        passwordChange.getNewPassword() != null &&
                        passwordChange.getLastPassword() != null) {
                        if (sessionUser.getUser().getPassword().equals(Util.
                            calculateSha1(passwordChange.getLastPassword()))) {
                            if (passwordChange.getConfirmationPassword().
                                equals(passwordChange.getLastPassword())) {
                                modelAndView.addObject("msg",
                                    ERR_018_PASSWORD_MUST_BE_DIFERENT.
                                        getMessage());
                                modelAndView.addObject("msgType", FAILED_ALERT);
                                initEdit(modelAndView, request);
                                modelAndView.addObject("passMode", true);
                                return modelAndView;
                            }
                            if (passwordChange.getNewPassword().
                                equals(passwordChange.getConfirmationPassword())) {
                                sessionUser.getUser().setPassword(Util.
                                    calculateSha1(passwordChange.
                                        getConfirmationPassword()));
                                userService.update(sessionUser.getUser());
                                modelAndView.addObject("msg",
                                    PASS_CHANGE_SUCCESS_MESSAGE);
                                modelAndView.addObject("msgType", SUCCESS_ALERT);
                                modelAndView.addObject("passMode", true);
                            } else {
                                modelAndView.addObject("msg",
                                    ERR_016_PASSWORD_DIFERENT_TO_CONFIRMATION.
                                        getMessage());
                                modelAndView.addObject("msgType", FAILED_ALERT);
                                modelAndView.addObject("passMode", true);
                            }
                        } else {
                            modelAndView.addObject("msg",
                                ErrorCode.ERR_017_CREDENTIALS_FAILED.
                                    getMessage());
                            modelAndView.addObject("msgType", FAILED_ALERT);
                            modelAndView.addObject("passMode", true);
                        }
                    }
                } else {
                    modelAndView.addObject("msg",
                        "No se encontr� el usuario registrado");
                    modelAndView.addObject("msgType", FAILED_ALERT);
                    modelAndView.addObject("passMode", true);
                }
            } else {
                modelAndView.addObject("msg",
                    "Por favor, ingrese las contrase�a seg�n los par�metros indicados");
                modelAndView.addObject("msgType", FAILED_ALERT);
                modelAndView.addObject("passMode", true);
            }
        } catch (ConstraintViolationException ex) {
            LOGGER.error("Error ", ex);
            StringBuilder builder = new StringBuilder("Alertas: ");

            for (ConstraintViolation error : ex.getConstraintViolations()) {
                builder.append(error.getMessage());
                builder.append("\n");
            }
            modelAndView.addObject("msg", builder.toString());
            modelAndView.addObject("msgType", FAILED_ALERT);
            modelAndView.addObject("passMode", true);
        } catch (Exception e) {
            LOGGER.error("ERROR", e);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
            modelAndView.addObject("msgType", FAILED_ALERT);
            modelAndView.addObject("passMode", true);
        }
        initEdit(modelAndView, request);
        return modelAndView;
    }

    /**
     * M&eacute;todo para abrir el detalle de un usuario.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping("detalle-consulta-usuarios")
    public ModelAndView detailProfile(HttpServletRequest request,
        HttpServletResponse response) {
        Long id = Long.parseLong(request.getParameter("id"));
        User parent;
        ModelAndView modelAndView =
            new ModelAndView("detalle-consulta-usuarios");
        try {
            LOGGER.info("Se busca la instucion con este ID" + id);
            parent = this.userService.findById(id);

            modelAndView.addObject("parentUser", parent);

        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("finally de display Aprobacion");
        }
        init(modelAndView, false);
        return modelAndView;
    }

    /**
     * Inicia los objetos para el controler.
     * 
     * @param modelAndView
     * @param request 
     */
    private void initEdit(ModelAndView modelAndView, HttpServletRequest request) {
        try {
            LOGGER.info("entrando a initEdit en UserCOntroller");
            /*
             * seteamos el estado del usuario de consulta en alguno de la lista
             * por defecto
             */
            HttpSession session = request.getSession();
            Session sessionUser = (Session) session.getAttribute(SESSION_USER);

            User userInSession = this.userService.
                findById(sessionUser.getUser().getId());

            modelAndView.addObject("documentTypes",
                this.valueListService.getDocumentTypeList());
            modelAndView.addObject("foundationsList",
                this.valueListService.getAllFoundations());
            modelAndView.addObject("stateList",
                this.valueListService.getStateList());
            modelAndView.addObject("citiesList",
                this.valueListService.getCityList());

            modelAndView.addObject("editionPassword", new PasswordChangeDto());

            if (sessionUser.getUser() != null) {
                modelAndView.addObject("creationUser", userInSession);
            } else {
                modelAndView.addObject("creationUser", new User());
                modelAndView.addObject("msgType", FAILED_ALERT);
                modelAndView.addObject("msg",
                    "No se encontr� el usuario registrado");
            }
        } catch (Exception ex) {
            LOGGER.error("ERROR", ex);
            modelAndView.addObject("msgType", FAILED_ALERT);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("finally init in UserController");
        }
    }

    /**
     * Inicia los objetos para el controler.
     * 
     * @param modelAndView
     * @param creationError 
     */
    private void init(ModelAndView modelAndView, boolean creationError) {
        LOGGER.info("Ejecutando metodo [ init ]");
        List<Profile> listProfiles = this.profileService.getAll();
        modelAndView.addObject("userStatusList", UserStatus.values());
        modelAndView.addObject("userProfileList", listProfiles);
        modelAndView.addObject("foundationsList",
            this.valueListService.getAllFoundations());
        modelAndView.addObject("profileList", listProfiles);
        modelAndView.addObject("searchUser", new User());
        modelAndView.addObject("documentTypes",
            this.valueListService.getDocumentTypeList());
        modelAndView.addObject("stateList",
            this.valueListService.getStateList());
        modelAndView.addObject("citiesList",
            this.valueListService.getCityList());

        if (!creationError) {
            modelAndView.addObject("creationUser", new User());
            modelAndView.addObject("editionUser", new User());
        }

        LOGGER.info("Finaliza el metodo init en UserController");
    }

    /**
     * M&eacute;todo que se ejecuta al abrir la ventana de consulta de historial
     * de accesos.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "consulta-historial-accesos",
        method = RequestMethod.GET)
    public ModelAndView onDisplayAccessHistory(HttpServletRequest request,
        HttpServletResponse response) {

        LOGGER.info(
            "Ejecutando metodo [ onDisplayAccessHistory UserController ]");
        ModelAndView modelAndView =
            new ModelAndView("consulta-historial-accesos");
        try {
            modelAndView.addObject("cajasCompensacionList",
                this.valueListService.getFoundationList());
            // today    
            Calendar date = new GregorianCalendar();
            // reset hour, minutes, seconds and millis
            date.set(Calendar.HOUR_OF_DAY, 0);
            date.set(Calendar.MINUTE, 0);
            date.set(Calendar.SECOND, 0);
            date.set(Calendar.MILLISECOND, 0);
            modelAndView.addObject("historicAccessList",
                this.historyAccessService.
                    findHistoricAccessUser(new User(), date.getTime(),
                        new Date()));
        } catch (Exception ex) {
            LOGGER.error("Error", ex);
        } finally {
            LOGGER.info("finally de onDisplayAccessHistory en UserController");
        }
        return modelAndView;
    }

    /**
     * M&eacute;todo que se ejecuta al abrir la ventana de consulta de historial
     * de acciones.
     *
     * @param request
     * @param response
     *
     * @return
     */
    @RequestMapping(value = "consulta-historial-acciones",
        method = RequestMethod.GET)
    public ModelAndView onDisplayActionsHistory(HttpServletRequest request,
        HttpServletResponse response) {
        LOGGER.info(
            "Ejecutando metodo [ onDisplayAccessHistory UserController ]");
        ModelAndView modelAndView =
            new ModelAndView("consulta-historial-acciones");
        try {

            // today    
            Calendar date = new GregorianCalendar();
            // reset hour, minutes, seconds and millis
            date.set(Calendar.HOUR_OF_DAY, 0);
            date.set(Calendar.MINUTE, 0);
            date.set(Calendar.SECOND, 0);
            date.set(Calendar.MILLISECOND, 0);
            modelAndView.addObject("historicActionsList", this.actionLogService.
                getActionsByFilters(new User(), date.getTime(), new Date()));

        } catch (Exception ex) {
            LOGGER.error("Error", ex);
        }

        return modelAndView;
    }

    /**
     * M&eacute;todo para buscar en el historial de accesos segus los filtros
     * seleccionados.
     *
     * @param request
     * @param response
     * @param startDate
     * @param finalDate
     *
     * @return
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName =
        "CONSULTAR-HISTORIAL-ACCESOS", entityTableName = "HISTORICO_ACCESOS")
    @RequestMapping(value = "buscarHistorialAccesos",
        method = RequestMethod.GET)
    public ModelAndView searchHistoricAccess(HttpServletRequest request,
        HttpServletResponse response,
        @RequestParam(name = "initialDate") Date startDate,
        @RequestParam(name = "finishialDate") Date finalDate) {
        LOGGER.info("entra a searchHistoricAccess en el controller de " +
            "userCOntroller");
        ModelAndView modelAndView =
            new ModelAndView("consulta-historial-accesos");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(finalDate);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date endDate = calendar.getTime();
        try {
            List<HistoryAccess> historicosAccesos;
            String numeroDocumento = "";
            String nombreUsuario = "";
            String idCajaCompensacion = "";
            if (request.getParameter("foundation") != null && !"".equals(
                request.
                    getParameter("foundation"))) {
                idCajaCompensacion = request.getParameter("foundation");
            }
            if (request.getParameter("nombre") != null && !"".equals(request.
                getParameter("nombre"))) {
                nombreUsuario = request.getParameter("nombre");
            }
            if (request.getParameter("documento") != null && !"".equals(request.
                getParameter("documento"))) {
                numeroDocumento = request.getParameter("documento");
            }
            User user = new User();
            if (!"".equals(idCajaCompensacion)) {
                user.setCcf(new Foundation());
                user.getCcf().setCode(idCajaCompensacion);
            }
            if (!"".equals(numeroDocumento)) {
                user.setIdentificationNumber(numeroDocumento);
            }
            user.setUsername(nombreUsuario);
            LOGGER.info("Fecha inicio" + startDate);
            LOGGER.info("Fecha fin" + endDate);
            historicosAccesos = this.historyAccessService.
                findHistoricAccessUser(user, startDate, endDate);
            modelAndView.addObject("historicAccessList", historicosAccesos);
            modelAndView.addObject("cajasCompensacionList",
                this.valueListService.getFoundationList());
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);
            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("finally de searchHistoricAccess en userController");
        }

        return modelAndView;
    }

    /**
     * M&eacute;todo para buscar en el historial de acci&oacute;es segus los 
     * filtros seleccionados.
     *
     * @param request
     * @param response
     * @param startDate
     * @param finalDate
     *
     * @return
     */
    @AnnotationInterceptor(beforeExecutionValue = "ANTES",
        afterExecutionValue = "DESPUES", methodName =
        "CONSULTAR-HISTORIAL-ACCIONES", entityTableName = "HISTORICO_ACCIONES")
    @RequestMapping(value = "buscarHistorialAcciones",
        method = RequestMethod.GET)
    public ModelAndView searchHistoricActions(HttpServletRequest request,
        HttpServletResponse response,
        @RequestParam(name = "initialDate") Date startDate,
        @RequestParam(name = "finishialDate") Date finalDate) {
        LOGGER.info("entra a searchHistoricActions en el controller de " +
            "userCOntroller");
        ModelAndView modelAndView =
            new ModelAndView("consulta-historial-acciones");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(finalDate);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date endDate = calendar.getTime();

        try {
            List<ActionLog> actionLogs;
            String numeroDocumento = "";

            if (request.getParameter("documento") != null && !"".equals(request.
                getParameter("documento"))) {
                numeroDocumento = request.getParameter("documento");
            }
            User user = new User();
            if (!"".equals(numeroDocumento)) {
                user.setIdentificationNumber(numeroDocumento);
            }
            actionLogs = this.actionLogService.
                getActionsByFilters(user, startDate, endDate);
            modelAndView.addObject("historicActionsList", actionLogs);
        } catch (Exception ex) {
            LOGGER.error("Error ", ex);

            modelAndView.addObject("msg", ERR_001_UNKNOW.getMessage());
        } finally {
            LOGGER.info("finally de searchHistoricActions en userController");
        }

        return modelAndView;
    }

    /**
     * Inicializa el binder para formatear fechas.
     *
     * @param binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class,
            new CustomDateEditor(sdf, true));
    }

    /**
     * M&eacute;todo que valida si es usuario del ministerio de trabajo.
     *
     * @param rq
     *
     * @return
     */
    private Boolean isUserOfMinistryOfLabor(HttpServletRequest rq) {
        Session session = (Session) rq.getSession().getAttribute(
            SESSION_USER);
        User user = session.getUser();
        return user.getCcf().getNit().equals(Util.NIT_ENTIDAD_MINISTRY_OF_LABOR);
    }

    /**
     * M&eacute;todo que valida si es usuario del ministerio de trabajo.
     *
     * @param user
     *
     * @return
     */
    private Boolean isUserOfMinistryOfLabor(User user) {
        return user.getCcf().getNit().equals(Util.NIT_ENTIDAD_MINISTRY_OF_LABOR);
    }

    /**
     * Activa el usuario dependiendo si es o no del ministerio.
     *
     * @param user
     * @param userName
     * @param passwordChange
     * @param userId
     *
     * @throws UserException
     * @throws SystemException
     */
    private void activateUser(User user, String userName,
        PasswordChangeDto passwordChange, long userId, boolean oldUser)
        throws UserException, SystemException {

        user.setUsername(userName);
        user.setPassword(Util.calculateSha1(
            passwordChange.
                getConfirmationPassword()));
        user.setLastPasswordChange(new Date());

        if (oldUser) {
            user.setStatus(UserStatus.A);
            this.userService.updateUserOld(user);
        } else {
            this.userService.update(user);
        }
        this.activationService.deleteTokensByUser(userId);
    }

    /**
     * M&eacute;todo que valida si la contrasenia es valida.
     * 
     * @param pass
     * @return 
     */
    public Boolean validatePass(String pass) {

        boolean Appro;

        boolean key1;
        boolean key2;
        boolean key3;
        boolean key4;

        Pattern k1 = Pattern.compile(".*[A-Z].*");
        Matcher m1 = k1.matcher(pass);
        if (m1.matches()) {
            key1 = true;
        } else {
            key1 = false;
        }
        Pattern k2 = Pattern.compile(".*[a-z].*");
        Matcher m2 = k2.matcher(pass);
        if (m2.matches()) {
            key2 = true;
        } else {
            key2 = false;
        }
        Pattern k3 = Pattern.compile(".*[0-9].*");
        Matcher m3 = k3.matcher(pass);
        if (m3.matches()) {
            key3 = true;
        } else {
            key3 = false;
        }
        Pattern k4 = Pattern.compile(".*[^a-zA-Z0-9].*");
        Matcher m4 = k4.matcher(pass);
        if (m4.matches()) {
            key4 = true;
        } else {
            key4 = false;
        }

        if (key1 && key2 && key3 && key4) {
            Appro = true;
        } else {
            Appro = false;
        }
        
        return Appro;

    }

    /**
     * Verifica la contrase&ntilde;a e inicia el proceso de activaci&oacute;n
     * del usuario.
     *
     * @param passwordChange
     * @param user
     * @param userName
     * @param modelAndViewSuccesfull
     * @param modelAndViewError
     * @param userId
     *
     * @return
     *
     * @throws UserException
     * @throws SystemException
     */
    private ModelAndView validatePasswod(PasswordChangeDto passwordChange,
        User user, String userName, ModelAndView modelAndViewSuccesfull,
        ModelAndView modelAndViewError, long userId, boolean mintrabUser,
        boolean oldUser)
        throws UserException, SystemException {

        if (passwordChange.getNewPassword().
            equals(passwordChange.getConfirmationPassword()) && !mintrabUser) {
            if (validPassword(passwordChange.getConfirmationPassword())) {
                if (user.getPassword() == null || user.
                    getLastPasswordChange() == null) {
                    user.setStatus(UserStatus.A);
                }

                activateUser(user, userName, passwordChange, userId, oldUser);

                modelAndViewSuccesfull.addObject("user", new User());
                modelAndViewSuccesfull.addObject("msg",
                    PASS_CHANGE_SUCCESS_MESSAGE);
                modelAndViewSuccesfull.addObject("msgType",
                    SUCCESS_ALERT);
                modelAndViewSuccesfull.addObject("captchaKey",
                    this.verifyRecaptcha.getKey());

                return modelAndViewSuccesfull;

            } else {
                modelAndViewError.addObject("msg",
                    ERR_042_ERROR_INVALID_PASSWORD.getMessage());
                modelAndViewError.addObject("msgType", FAILED_ALERT);
                LOGGER.info(" user id es  igual a " + user.getId());
                modelAndViewError.addObject("user", user);
                modelAndViewError.addObject("editionPassword",
                    new PasswordChangeDto());

                return modelAndViewError;
            }
        } else if (mintrabUser) {
            try {
                if (isUserOfMinistryOfLabor(user)) {
                    if (!this.ldapOperationsService.authenticate(userName,
                        passwordChange.getNewPassword())) {
                        throw new SystemException(
                            ERR_033_LDAP_AUTHENTICATION_FAILED);
                    } else {
                        user.setStatus(UserStatus.A);
                        user.setUsername(userName);
                        this.activationService.deleteTokensByUser(userId);
                        userService.updateUser(user);
                    }
                } else {
                    if (this.ldapOperationsService.userExist(userName)) {
                        throw new SystemException(
                            ERR_032_ACTIVE_DIRECTORY_USED, userName);
                    }
                }
            } catch (SystemException e) {
                modelAndViewError.addObject("msg",
                    ERR_056_ERROR_ACTIVATE_MINTRABUSER.getMessage());
                modelAndViewError.addObject("user", user);
                modelAndViewError.addObject("editionPassword",
                    new PasswordChangeDto());
                return modelAndViewError;
            }

            modelAndViewSuccesfull.addObject("user", new User());
            modelAndViewSuccesfull.addObject("msg",
                PASS_CHANGE_SUCCESS_MESSAGE);
            modelAndViewSuccesfull.addObject("msgType",
                SUCCESS_ALERT);
            modelAndViewSuccesfull.addObject("captchaKey",
                this.verifyRecaptcha.getKey());

            return modelAndViewSuccesfull;
        } else {
            modelAndViewError.addObject("msg",
                ERR_016_PASSWORD_DIFERENT_TO_CONFIRMATION.getMessage());
            modelAndViewError.addObject("msgType", FAILED_ALERT);
            LOGGER.info(" user id es  igual a " + user.getId());
            modelAndViewError.addObject("user", user);
            modelAndViewError.addObject("editionPassword",
                new PasswordChangeDto());
            return modelAndViewError;
        }
    }
}
