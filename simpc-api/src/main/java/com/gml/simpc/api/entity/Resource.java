/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Resource.java
 * Created on: 2016/10/26, 11:50:06 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entidad para manejo de recursos.
 *
 * @author <a href="mailto:julianm@gmlsoftware.com">Juli&acute;n Misnaza</a>
 */
@Table(name = "RECURSOS")
@Entity
public class Resource implements Serializable {

    /**
     * ID del recurso.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "profiles_seq")
    @SequenceGenerator(name = "profiles_seq", sequenceName =
        "RESOURCES_SEQ", allocationSize = 1, initialValue= 1)
    private Long id;

    @Column(name = "PERFIL_ID")
    private Long idProfile;

    @Column(name = "FUNCIONALIDAD_ID")
    private Long idFunctionality;

    /**
     * Constructor por perfil y funcionalidad.
     *
     * @param idProfile
     * @param idFunctionality
     */
    public Resource(Long idProfile, Long idFunctionality) {
        this.idProfile = idProfile;
        this.idFunctionality = idFunctionality;
    }

    /**
     * Constructor por defecto.
     */
    public Resource() {
        this.id = null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdProfile() {
        return idProfile;
    }

    public void setIdProfile(Long idProfile) {
        this.idProfile = idProfile;
    }

    public Long getIdFunctionality() {
        return idFunctionality;
    }

    public void setIdFunctionality(Long idFunctionality) {
        this.idFunctionality = idFunctionality;
    }

    @Override
    public String toString() {
        return "Recurso{" + "id=" + id + ", Id Del Perfil=" + idProfile +
            ", Id de la Funcionalidad=" + idFunctionality + '}';
    }
    
}
