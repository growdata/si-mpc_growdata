/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ValueListService.java
 * Created on: 2017/01/16, 02:45:08 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.web.utilities;

import com.gml.simpc.api.entity.IndicatorConfiguration;
import com.gml.simpc.api.entity.valuelist.CarLicenseCategory;
import com.gml.simpc.api.entity.valuelist.Cine;
import com.gml.simpc.api.entity.valuelist.City;
import com.gml.simpc.api.entity.valuelist.CivilStatus;
import com.gml.simpc.api.entity.valuelist.Degree;
import com.gml.simpc.api.entity.valuelist.DisabilityType;
import com.gml.simpc.api.entity.valuelist.DocumentType;
import com.gml.simpc.api.entity.valuelist.EducationalLevel;
import com.gml.simpc.api.entity.valuelist.EmploymentStatus;
import com.gml.simpc.api.entity.valuelist.EquivalentPosition;
import com.gml.simpc.api.entity.valuelist.EthnicGroup;
import com.gml.simpc.api.entity.valuelist.Foundation;
import com.gml.simpc.api.entity.valuelist.InstitutionType;
import com.gml.simpc.api.entity.valuelist.Certifications;
import com.gml.simpc.api.entity.valuelist.DeliveredCertification;
import com.gml.simpc.api.entity.valuelist.KnowledgeCore;
import com.gml.simpc.api.entity.valuelist.Language;
import com.gml.simpc.api.entity.valuelist.LoadType;
import com.gml.simpc.api.entity.valuelist.MotorcycleLicenseCategory;
import com.gml.simpc.api.entity.valuelist.OrientationType;
import com.gml.simpc.api.entity.valuelist.OtherKnowledge;
import com.gml.simpc.api.entity.valuelist.PerformanceArea;
import com.gml.simpc.api.entity.valuelist.PerformanceAreaOrientation;
import com.gml.simpc.api.entity.valuelist.Population;
import com.gml.simpc.api.entity.valuelist.ProgramType;
import com.gml.simpc.api.entity.valuelist.Sector;
import com.gml.simpc.api.entity.valuelist.State;
import com.gml.simpc.api.entity.valuelist.TrainingType;
import com.gml.simpc.api.entity.valuelist.TypeQualityCertificate;
import com.gml.simpc.api.utilities.ValueList;
import com.gml.simpc.web.repository.IndicatorConfigurationRepository;
import com.gml.simpc.web.repository.UserRepository;
import com.gml.simpc.web.repository.valuelist.CarLicenseCategoryRepository;
import com.gml.simpc.web.repository.valuelist.CineRepository;
import com.gml.simpc.web.repository.valuelist.CityRepository;
import com.gml.simpc.web.repository.valuelist.CivilStatusRepository;
import com.gml.simpc.web.repository.valuelist.DegreeRepository;
import com.gml.simpc.web.repository.valuelist.DisabilityTypeRepository;
import com.gml.simpc.web.repository.valuelist.DocumentTypeRepository;
import com.gml.simpc.web.repository.valuelist.EducationalLevelRepository;
import com.gml.simpc.web.repository.valuelist.EmploymentExperienceRepository;
import com.gml.simpc.web.repository.valuelist.EmploymentStatusRepository;
import com.gml.simpc.web.repository.valuelist.EpsRepository;
import com.gml.simpc.web.repository.valuelist.EquivalentPositionRepository;
import com.gml.simpc.web.repository.valuelist.EthnicGroupRepository;
import com.gml.simpc.web.repository.valuelist.FoundationRepository;
import com.gml.simpc.web.repository.valuelist.InstitutionTypeRepository;
import com.gml.simpc.web.repository.valuelist.KnowledgeCoreRepository;
import com.gml.simpc.web.repository.valuelist.LanguageRepository;
import com.gml.simpc.web.repository.valuelist.LoadTypeRepository;
import com.gml.simpc.web.repository.valuelist.MotorcycleLicenseCategoryRepository;
import com.gml.simpc.web.repository.valuelist.ObtainedTitleRepository;
import com.gml.simpc.web.repository.valuelist.OrientationTypeRepository;
import com.gml.simpc.web.repository.valuelist.OtherKnowledgeRepository;
import com.gml.simpc.web.repository.valuelist.PensionFundRepository;
import com.gml.simpc.web.repository.valuelist.PerformanceAreaOrientationRepository;
import com.gml.simpc.web.repository.valuelist.PerformanceAreaRepository;
import com.gml.simpc.web.repository.valuelist.PopulationRepository;
import com.gml.simpc.web.repository.valuelist.ProgramTypeRepository;
import com.gml.simpc.web.repository.valuelist.SectorRepository;
import com.gml.simpc.web.repository.valuelist.SeveranceFundRepository;
import com.gml.simpc.web.repository.valuelist.StateRepository;
import com.gml.simpc.web.repository.valuelist.TrainingTypeRepository;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.gml.simpc.web.repository.valuelist.CertificationsRepository;
import com.gml.simpc.web.repository.valuelist.DeliveredCertificationRepository;
import com.gml.simpc.web.repository.valuelist.TypeQualityCertificateRepository;

/**
 * Componente para administrar las listas de valor.
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica
 * Jim&eacute;nez Rozo</a>
 */
@Service("valueListService")
public class ValueListService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(ValueListService.class);

    /**
     * Servicio para acceder a los tipos de carga desde el repositorio jpa.
     */
    @Autowired
    private LoadTypeRepository loadTypeRepository;
    /**
     * Servicio para acceder a los tipos de programa desde el repositorio jpa.
     */
    @Autowired
    private ProgramTypeRepository programTypeRepository;
    /**
     * Servicio para acceder a las cajas de compensaci?n desde el repositorio
     * jpa.
     */
    @Autowired
    private FoundationRepository foundationRepository;
    /**
     * Servicio para acceder a departamentos desde el repositorio jpa.
     */
    @Autowired
    private StateRepository stateRepository;
    /**
     * Servicio para acceder a los municipios desde el repositorio jpa.
     */
    @Autowired
    private CityRepository cityRepository;
    /**
     * Servicio para acceder a los niveles Cine desde el repositorio jpa.
     */
    @Autowired
    private CineRepository cineRepository;
    /**
     * Servicio de tipos de documentos.
     */
    @Autowired
    private DocumentTypeRepository docTypeRepository;
    /**
     * Servicio de tipos de institucion.
     */
    @Autowired
    private InstitutionTypeRepository insTypeRepo;

    /**
     * Servicio para orientationType.
     */
    @Autowired
    private OrientationTypeRepository oriTypeRepo;

    /*
     * -----------------------------------------------------------------------
     */
    @Autowired
    private CivilStatusRepository civilStatusRepository;

    /*
     * Repositorio para titulo obtenido
     */
    @Autowired
    private ObtainedTitleRepository obTitleRepo;
    /**
     * Repositorio para grados.
     */
    @Autowired
    private DegreeRepository degreeRepository;
    /**
     * Repositorio para tipos de incapacidad.
     */
    @Autowired
    private DisabilityTypeRepository disTypeRepository;
    /**
     * Repositorio para tipos de incapacidad.
     */
    @Autowired
    private EducationalLevelRepository eduLevelRepo;
    /**
     * Repositorio para tipos de experiencia laboral.
     */
    @Autowired
    private EmploymentExperienceRepository emplExpeRepo;
    /**
     * Repositorio para tipos de estados laborales.
     */
    @Autowired
    private EmploymentStatusRepository emplstatRepo;
    /**
     * Repositorio para tipos de posiciones equivalentes.
     */
    @Autowired
    private EquivalentPositionRepository eqPosRepo;
    /**
     * Repositorio para tipos de grupos Etnicos.
     */
    @Autowired
    private EthnicGroupRepository ethnicGroupRepo;
    /**
     * Repositorio para tipos de nucleos de conocimiento.
     */
    @Autowired
    private KnowledgeCoreRepository knowledgeCoreRepo;
    /**
     * Repositorio para tipos de idiomas.
     */
    @Autowired
    private LanguageRepository languageRepository;
    /**
     * Repositorio para otros conocimientos.
     */
    @Autowired
    private OtherKnowledgeRepository otherKnowledgeRepo;
    /**
     * Repositorio para areas de desempe&ntilde;o. nivel de educacion
     */
    @Autowired
    private PerformanceAreaRepository performAreaRepo;
    /**
     * Repositorio para areas de desempe&ntilde;o. orientacion
     */
    @Autowired
    private PerformanceAreaOrientationRepository performAreaOrientationRepo;
    /**
     * Repositorio para sectores.
     */
    @Autowired
    private SectorRepository sectorRepository;
    /**
     * Repositorio para tipos de capacitaci&oacute;n.
     */
    @Autowired
    private TrainingTypeRepository trainingTypeRepo;
    /**
     * Repositorio para tipos de capacitaci&oacute;n.
     */
    @Autowired
    private UserRepository userRepository;

    /**
     * Repositorio para categorias de licencias de carro
     */
    @Autowired
    private CarLicenseCategoryRepository clr;

    /**
     * Repositorio para categorias de licencias de moto
     */
    @Autowired
    private MotorcycleLicenseCategoryRepository mlr;
    
    /**
     * Repositorio para los tipos de poblacion
     */
    @Autowired
    private  PopulationRepository population;
    /**
     * Repositorio para administradoras de salud
     */
    @Autowired
    private  EpsRepository eps;
    /**
     * Repositorio para administradoras de pension
     */
    @Autowired
    private  PensionFundRepository pension;
    /**
     * Repositorio para administradoras de cesantias
     */
    @Autowired
    private  SeveranceFundRepository severance;
    /**
     * Repositorio para administrar los indicadores
     */
    @Autowired
    private  IndicatorConfigurationRepository indicators;
    /**
     * Repositorio para administrar las certificaciones
     */
    @Autowired
    private  CertificationsRepository certifications;
    /**
     * Repositorio para administrar las certificaciones emitidas
     */
    @Autowired
    private  DeliveredCertificationRepository deliveredCertification;
    /**
     * Repositorio para administrar los tipos de certificacion
     */
    @Autowired
    private  TypeQualityCertificateRepository typeQualityCertificate;

    /**
     * Atributo para almacenar las listas de valores.
     */
    private ValueList valueList;

    /**
     * Actualiza periodicamente las listas de valores desde la base de datos.
     */
    @PostConstruct
    @Scheduled(cron = "0 0/5 * * * ?")
    public void updateAll() {
        LOGGER.info("Updating value lists from database new service... ");
        this.valueList = new ValueList();

        try {
            this.valueList.setCarLicenseCategoryList(clr.findAll());
            this.valueList.setCine(this.cineRepository.findAll());
            this.valueList.setCities(this.cityRepository.findAll());
            this.valueList.setCivilStatus(this.civilStatusRepository.findAll());
            this.valueList.setDegrees(this.degreeRepository.findAll());
            this.valueList.setDisabilityTypes(this.disTypeRepository.findAll());
            this.valueList.setDocumentTypes(this.docTypeRepository.findAll());
            this.valueList.setEducationalLevels(this.eduLevelRepo.findAll());
            this.valueList.setEmploymentExperience(this.emplExpeRepo.findAll());
            this.valueList.setEmploymentStatus(this.emplstatRepo.findAll());
            this.valueList.setEquivalentPositions(this.eqPosRepo.findAll());
            this.valueList.setEthnicGroups(this.ethnicGroupRepo.findAll());
            this.valueList.setFoundations(this.foundationRepository.findAll());
            this.valueList.setInstitutionTypes(this.insTypeRepo.findAll());
            this.valueList.setKnowledgeCores(this.knowledgeCoreRepo.findAll());
            this.valueList.setLanguages(this.languageRepository.findAll());
            this.valueList.setLoadTypes(this.loadTypeRepository.findAll());
            this.valueList.setMotorcycleLicenseCategoryList(this.mlr.findAll());
            this.valueList.setObtainedTitleList(this.obTitleRepo.findAll());
            this.valueList.setOtherKnowledges(this.otherKnowledgeRepo.findAll());
            this.valueList.setOrientationType(this.oriTypeRepo.findAll());
            this.valueList.setPerformanceAreas(this.performAreaRepo.findAll());
            this.valueList.setPerformanceAreasOrientation(this.performAreaOrientationRepo.findAll());
            this.valueList.setProgramTypes(this.programTypeRepository.findAll());
            this.valueList.setSectors(this.sectorRepository.findAll());
            this.valueList.setStates(this.stateRepository.findAll());
            this.valueList.setTrainingTypes(this.trainingTypeRepo.findAll());
            this.valueList.setPopulation(this.population.findAll());
            this.valueList.setSeveranceFund(this.severance.findAll());
            this.valueList.setEps(this.eps.findAll());
            this.valueList.setPensionFunds(this.pension.findAll());
            this.valueList.setInticators(this.indicators.findAll());
            this.valueList.setCertifications(this.certifications.findAll());
            this.valueList.setDeliveredCertification(this.deliveredCertification.findAll());
            this.valueList.setTypeQualityCertificate(this.typeQualityCertificate.findAll());
        } catch (Exception ex) {
            LOGGER.error("Error", ex);
        }
    }

    
    public Map<String, Cine> getCine() {
        return this.valueList.getCineMap();
    }

    public List<Cine> getCineList() {
        return this.valueList.getCineList();
    }
    
    public List<Certifications> getCertifications() {
        return this.valueList.getCertifications();
    }
    
    public Map<String, DeliveredCertification> getDeliveredCertificationMap() {
        return this.valueList.getDeliveredCertificationMap();
    }
    
    public List<DeliveredCertification> getDeliveredCertificationList() {
        return this.valueList.getDeliveredCertification();
    }
    
    public List<TypeQualityCertificate> getTypeQualityCertificate() {
        return this.valueList.getTypeQualityCertificate();
    }
    
     public Map<String, Population> getPopulation() {
        return this.valueList.getPopulation();
    }

    public List<Population> getPopulationList() {
        return this.valueList.getPopulationList();
    }
    
    public Map<String, LoadType> getLoadTypes() {
        return this.valueList.getLoadTypes();
    }

    public List<LoadType> getLoadTypeList() {
        return this.valueList.getLoadTypeList();
    }

    public Map<String, ProgramType> getProgramTypes() {
        return this.valueList.getProgramTypes();
    }

    public List<ProgramType> getProgramTypeList() {
        return this.valueList.getProgramTypeList();
    }

    public List<OrientationType> getOrientationTypeList() {
        return this.valueList.getOrientationTypeList();
    }

    public Map<String, Foundation> getFoundations() {
        return this.valueList.getFoundations();
    }

    public List<Foundation> getFoundationList() {
        return this.valueList.getFoundationList();
    }

    public Map<String, State> getStates() {
        return this.valueList.getStates();
    }

    public List<State> getStateList() {
        return this.valueList.getStateList();
    }

    public Map<String, City> getCities() {
        return this.valueList.getCities();
    }

    public List<City> getCityList() {
        return this.valueList.getCityList();
    }
    
    public Map<String, DocumentType> getDocumentTypes() {
        return this.valueList.getDocumentTypes();
    }

    public List<DocumentType> getDocumentTypeList() {
        return this.valueList.getDocumentTypeList();
    }

    public Map<String, InstitutionType> getInstitutionTypes() {
        return this.valueList.getInstitutionTypes();
    }

    public List<InstitutionType> getInstitutionTypeList() {
        return this.valueList.getInstitutionTypeList();
    }

    public List<CivilStatus> getCivilStatusList() {
        return this.valueList.getCivilStatusList();
    }

    public Map<String, CivilStatus> getCivilStatusMap() {
        return this.valueList.getCivilStatusMap();
    }

    public List<Degree> getDegreeList() {
        return this.valueList.getDegreeList();
    }

    public Map<String, Degree> getDegreeMap() {
        return this.valueList.getDegreeMap();
    }

    public List<DisabilityType> getDisabilityTypeList() {
        return this.valueList.getDisabilityTypeList();
    }

    public Map<String, DisabilityType> getDisabilityTypeMap() {
        return this.valueList.getDisabilityTypeMap();
    }

    public List<EducationalLevel> getEducationalLevelList() {
        return this.valueList.getEducationalLevelList();
    }

    public Map<String, EducationalLevel> getEducationalLevelMap() {
        return this.valueList.getEducationalLevelMap();
    }

    public List<EmploymentStatus> getEmploymentStatusList() {
        return this.valueList.getEmploymentStatusList();
    }

    public Map<String, EmploymentStatus> getEmploymentStatusMap() {
        return this.valueList.getEmploymentStatusMap();
    }

    public List<EquivalentPosition> getEquivalentPositionList() {
        return this.valueList.getEquivalentPositionList();
    }

    public Map<String, EquivalentPosition> getEquivalentPositionMap() {
        return this.valueList.getEquivalentPositionMap();
    }

    public List<EthnicGroup> getEthnicGroupList() {
        return this.valueList.getEthnicGroupList();
    }

    public Map<String, EthnicGroup> getEthnicGroupMap() {
        return this.valueList.getEthnicGroupMap();
    }

    public List<KnowledgeCore> getKnowledgeCoreList() {
        return this.valueList.getKnowledgeCoreList();
    }

    public Map<String, KnowledgeCore> getKnowledgeCoreMap() {
        return this.valueList.getKnowledgeCoreMap();
    }

    public List<Language> getLanguageList() {
        return this.valueList.getLanguageList();
    }

    public Map<String, Language> getLanguageMap() {
        return this.valueList.getLanguageMap();
    }

    public List<OtherKnowledge> getOtherKnowledgeList() {
        return this.valueList.getOtherKnowledgeList();
    }

    public Map<String, OtherKnowledge> getOtherKnowledgeMap() {
        return this.valueList.getOtherKnowledgeMap();
    }

    public List<PerformanceArea> getPerformanceAreaList() {
        return this.valueList.getPerformanceAreaList();
    }
    public List<PerformanceAreaOrientation> getPerformanceAreaOrientationList() {
        return this.valueList.getPerformanceAreaOrientationList();
    }

    public Map<String, PerformanceArea> getPerformanceAreaMap() {
        return this.valueList.getPerformanceAreaMap();
    }
    public Map<String, PerformanceAreaOrientation> getPerformanceAreaOrientationMap() {
        return this.valueList.getPerformanceAreaOrientationMap();
    }
    public List<Sector> getSectorList() {
        return this.valueList.getSectorList();
    }

    public Map<String, Sector> getSectorMap() {
        return this.valueList.getSectorMap();
    }

    public List<TrainingType> getTrainingTypeList() {
        return this.valueList.getTrainingTypeList();
    }

    public Map<String, TrainingType> getTrainingTypeMap() {
        return this.valueList.getTrainingTypeMap();
    }
    
    public Map<Long, City> getCitiesById() {
        return this.valueList.getCitiesById();
    }

    public List<CarLicenseCategory> getCarLicenseCategoryList() {
        return this.valueList.getCarLicenseCategoryList();
    }

    public Map<String, CarLicenseCategory> getCarLicenseCategoryMap() {
        return this.valueList.getCarLicenseCategoryMap();
    }

    public List<MotorcycleLicenseCategory> getMotorcycleLicenseCategoryList() {
        return this.valueList.getMotorcycleLicenseCategoryList();
    }

    public Map<String, MotorcycleLicenseCategory> getMotorcycleLicenseCategoryMap() {
        return this.valueList.getMotorcycleLicenseCategoryMap();
    }
    
    public List<Foundation> getAllFoundations() {
        return this.valueList.getAllFoundations();
    }
    public Map<String, IndicatorConfiguration> getIndicatorsMap() {
        return this.valueList.getIndicatorsMap();
    }

    public List<IndicatorConfiguration> getIndicatorsList() {
        return this.valueList.getIndicatorsList();
    }

    /**
     * M?todo p?blico para obtener las listas de valores
     */
    public ValueList getValueList() {
        return valueList;
    }
}
