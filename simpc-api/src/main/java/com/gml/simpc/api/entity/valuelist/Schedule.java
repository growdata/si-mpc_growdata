/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: Schedule.java
 * Created on: 2017/02/18, 10:22:30 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.simpc.api.entity.valuelist;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author <a href="mailto:luzq@gmlsoftware.com">Luz Amanda Quilindo</a>
 */
@Table(name = "HORARIOS")
@Entity
public class Schedule implements Serializable {
    /**
     * ID del tipo de instituci&oacute;n.
     */
    @Id
    @Column(name = "ID", length = 10)
    private Long id;
    /**
     * nombre del tupo de instituci&oacute;n.
     */
    @Column(name = "NOMBRE", length = 50)
    private String name;

    /**
     * descripci&oacute;n
     */
    @Column(name = "CODIGO", length = 200)
    private String code;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    
    

}
